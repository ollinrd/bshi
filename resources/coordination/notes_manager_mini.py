#!pythonw
# -*- coding: UTF-8 -*-
import os
import sys
import platform
import json
import subprocess
from datetime import datetime
from PySide import QtCore, QtGui
import openpyxl
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml
from shotgun_api3 import Shotgun

from pprint import pprint as pp


class Ui_Dialog(QtGui.QDialog):

    def __init__(self):
        super(Ui_Dialog, self).__init__()
        self.e_subject = False
        self.e_date = False
        self.e_file = False    

        self.sg = None   

        config_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))        

        project_settings = self.get_settings(config_path)        
        sys.path.append(project_settings.get("python_utils_path"))
        from pythonTools.shotgunUtils import dbConnect

        self.sg, self.sg_connect = self.connect_to_shotgun(project_settings, dbConnect)        

    def get_settings(self, config_path):
      #settings_path = config_path + "/config/resources/project/project_specs.yml"    
      settings_path = "/nfs/ovfxToolkit/Prod/Projects/MIN2" + "/config/resources/project/project_specs.yml"    
      print settings_path
      stream = open(settings_path, "r")
      settings = dict(yaml.load(stream))    
      return settings

    def connect_to_shotgun(self, proj_settings, dbConnect):

      api_namekey = proj_settings.get("notesmanager_apikeyname")
      sg_connect = dbConnect.sgConnect()
      sg = sg_connect.connect(api_namekey)
      print 'sg: ', sg     

      return sg, sg_connect

    def setupUi(self, notesManagerUi):
        notesManagerUi.setObjectName("notesManagerUi")
        notesManagerUi.resize(400, 760)
        notesManagerUi.setMinimumSize(QtCore.QSize(400, 760))
        notesManagerUi.setMaximumSize(QtCore.QSize(400, 760))
        self.gridLayout = QtGui.QGridLayout(notesManagerUi)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setContentsMargins(10, 10, 10, 10)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.verticalLayout_7 = QtGui.QVBoxLayout()
        self.verticalLayout_7.setContentsMargins(-1, 0, -1, 0)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.groupBox = QtGui.QGroupBox(notesManagerUi)
        self.groupBox.setMinimumSize(QtCore.QSize(0, 320))
        self.groupBox.setMaximumSize(QtCore.QSize(16777215, 320))
        self.groupBox.setFlat(False)
        self.groupBox.setCheckable(False)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox_2 = QtGui.QGroupBox(self.groupBox)
        self.groupBox_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.groupBox_2.setFlat(False)
        self.groupBox_2.setObjectName("groupBox_2")
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox_2)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.email_subject = QtGui.QLineEdit(self.groupBox_2)
        self.email_subject.setMinimumSize(QtCore.QSize(0, 26))
        self.email_subject.setMaximumSize(QtCore.QSize(16777215, 26))
        self.email_subject.setObjectName("email_subject")
        self.gridLayout_3.addWidget(self.email_subject, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.groupBox_2)
        self.groupBox_4 = QtGui.QGroupBox(self.groupBox)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(250)
        sizePolicy.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy)
        self.groupBox_4.setMinimumSize(QtCore.QSize(0, 200))
        self.groupBox_4.setFlat(False)
        self.groupBox_4.setObjectName("groupBox_4")
        self.gridLayout_5 = QtGui.QGridLayout(self.groupBox_4)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.gridLayout_11 = QtGui.QGridLayout()
        self.gridLayout_11.setContentsMargins(-1, 0, -1, -1)
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.calendarWidget = QtGui.QCalendarWidget(self.groupBox_4)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.calendarWidget.sizePolicy().hasHeightForWidth())
        self.calendarWidget.setSizePolicy(sizePolicy)
        self.calendarWidget.setObjectName("calendarWidget")
        self.gridLayout_11.addWidget(self.calendarWidget, 0, 0, 1, 1)
        self.timeEdit_2 = QtGui.QTimeEdit(self.groupBox_4)
        self.timeEdit_2.setObjectName("timeEdit_2")
        self.gridLayout_11.addWidget(self.timeEdit_2, 1, 0, 1, 1)
        self.gridLayout_5.addLayout(self.gridLayout_11, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.groupBox_4)
        self.verticalLayout_6.addLayout(self.verticalLayout)
        self.verticalLayout_7.addWidget(self.groupBox)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.groupBox_3 = QtGui.QGroupBox(notesManagerUi)
        self.groupBox_3.setFlat(False)
        self.groupBox_3.setObjectName("groupBox_3")
        self.gridLayout_4 = QtGui.QGridLayout(self.groupBox_3)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.notes_type = QtGui.QComboBox(self.groupBox_3)
        self.notes_type.setMinimumSize(QtCore.QSize(0, 26))
        self.notes_type.setMaximumSize(QtCore.QSize(16777215, 26))
        self.notes_type.setObjectName("notes_type")
        self.notes_type.addItem("")
        #
        self.gridLayout_4.addWidget(self.notes_type, 0, 0, 1, 1)
        self.horizontalLayout.addWidget(self.groupBox_3)
        self.groupBox_5 = QtGui.QGroupBox(notesManagerUi)
        self.groupBox_5.setObjectName("groupBox_5")
        self.gridLayout_6 = QtGui.QGridLayout(self.groupBox_5)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.select_file = QtGui.QPushButton(self.groupBox_5)
        self.select_file.setObjectName("select_file")
        self.gridLayout_6.addWidget(self.select_file, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.groupBox_5)
        self.verticalLayout_7.addLayout(self.horizontalLayout)
        self.filepath = QtGui.QLabel(notesManagerUi)
        self.filepath.setText("")
        self.filepath.setObjectName("filepath")
        self.verticalLayout_7.addWidget(self.filepath)
        self.gridLayout_10 = QtGui.QGridLayout()
        self.gridLayout_10.setContentsMargins(-1, 0, -1, -1)
        self.gridLayout_10.setObjectName("gridLayout_10")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_10.addItem(spacerItem, 0, 0, 1, 1)
        self.extract = QtGui.QPushButton(notesManagerUi)
        self.extract.setObjectName("extract")
        self.gridLayout_10.addWidget(self.extract, 0, 1, 1, 1)
        self.verticalLayout_7.addLayout(self.gridLayout_10)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setContentsMargins(-1, 0, -1, -1)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.extraction_group = QtGui.QGroupBox(notesManagerUi)
        self.extraction_group.setMinimumSize(QtCore.QSize(0, 100))
        self.extraction_group.setObjectName("extraction_group")
        self.gridLayout_8 = QtGui.QGridLayout(self.extraction_group)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.scrollArea = QtGui.QScrollArea(self.extraction_group)
        self.scrollArea.setFrameShape(QtGui.QFrame.WinPanel)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 350, 59))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.gridLayout_9 = QtGui.QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_9.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_9.setSpacing(0)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.extraction_result = QtGui.QTextEdit(self.scrollAreaWidgetContents)
        self.extraction_result.setFrameShape(QtGui.QFrame.NoFrame)
        self.extraction_result.setFrameShadow(QtGui.QFrame.Plain)
        self.extraction_result.setObjectName("extraction_result")
        self.gridLayout_9.addWidget(self.extraction_result, 0, 0, 1, 1)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout_8.addWidget(self.scrollArea, 0, 0, 1, 1)
        self.verticalLayout_3.addWidget(self.extraction_group)
        self.verticalLayout_7.addLayout(self.verticalLayout_3)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.close = QtGui.QPushButton(notesManagerUi)
        self.close.setObjectName("close")
        self.horizontalLayout_2.addWidget(self.close)
        self.verticalLayout_7.addLayout(self.horizontalLayout_2)
        self.verticalLayout_4.addLayout(self.verticalLayout_7)
        self.gridLayout.addLayout(self.verticalLayout_4, 1, 0, 1, 1)
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setContentsMargins(-1, 0, -1, -1)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label = QtGui.QLabel(notesManagerUi)
        self.label.setMinimumSize(QtCore.QSize(400, 100))
        self.label.setMaximumSize(QtCore.QSize(400, 16777215))
        self.label.setAutoFillBackground(False)
        self.label.setStyleSheet("background-color: rgb(91, 0, 122);\n"
"background-image: url(:/images/header.jpg);")
        self.label.setText("")
        self.label.setObjectName("label")

        if platform.system() == 'Linux':
            self.imagepix = QtGui.QPixmap("/nfs/ovfxToolkit/Prod/Projects/OSOY/config/resources/coordination/header.jpg")        
        else:
            self.imagepix = QtGui.QPixmap("T:/Prod/Projects/OSOY/config/resources/coordination/header.jpg")

        self.label.setPixmap(self.imagepix)

        self.verticalLayout_5.addWidget(self.label)
        self.gridLayout.addLayout(self.verticalLayout_5, 0, 0, 1, 1)

        self.retranslateUi(notesManagerUi)
        QtCore.QMetaObject.connectSlotsByName(notesManagerUi)

        # Connects ui
        self.close.pressed.connect(self.closePressed)        
        self.email_subject.textEdited.connect(self.validate_inputs)
        self.email_subject.textChanged.connect(self.validate_inputs)     
        self.select_file.released.connect(self.get_xlsx_file)
        self.extract.released.connect(self.extract_xlsx)

        #
        self.calendarWidget.showToday()



    def retranslateUi(self, notesManagerUi):
        """notesManagerUi.setWindowTitle(QtGui.QApplication.translate("notesManagerUi", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.extraction_group.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes for extraction: ", None, QtGui.QApplication.UnicodeUTF8))
        self.extract.setText(QtGui.QApplication.translate("notesManagerUi", "Extract", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes reference info: ", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes Email subject: ", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_4.setTitle(QtGui.QApplication.translate("notesManagerUi", "Email date and hour: (MM/DD/YYYY  HH:MM:XX)", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes type:", None, QtGui.QApplication.UnicodeUTF8))
        #self.notes_type.setItemText(0, QtGui.QApplication.translate("notesManagerUi", "first item", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_5.setTitle(QtGui.QApplication.translate("notesManagerUi", "xlsx notes file: ", None, QtGui.QApplication.UnicodeUTF8))
        self.select_file.setText(QtGui.QApplication.translate("notesManagerUi", "Select file...", None, QtGui.QApplication.UnicodeUTF8))
        self.close.setText(QtGui.QApplication.translate("notesManagerUi", "Close", None, QtGui.QApplication.UnicodeUTF8))"""

        notesManagerUi.setWindowTitle(QtGui.QApplication.translate("notesManagerUi", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.extraction_group.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes for extraction: ", None, QtGui.QApplication.UnicodeUTF8))
        self.extract.setText(QtGui.QApplication.translate("notesManagerUi", "Extract", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes reference info: ", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes Email subject: ", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_4.setTitle(QtGui.QApplication.translate("notesManagerUi", "Email date and hour: (MM/DD/YYYY  HH:MM XX)", None, QtGui.QApplication.UnicodeUTF8))
        #self.label.setText(QtGui.QApplication.translate("notesManagerUi", "/", None, QtGui.QApplication.UnicodeUTF8))
        #self.label_2.setText(QtGui.QApplication.translate("notesManagerUi", "/", None, QtGui.QApplication.UnicodeUTF8))
        #self.label_3.setText(QtGui.QApplication.translate("notesManagerUi", ":", None, QtGui.QApplication.UnicodeUTF8))        
        self.filepath.setText(QtGui.QApplication.translate("notesManagerUi", "", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("notesManagerUi", "Notes type:", None, QtGui.QApplication.UnicodeUTF8))
        print 1, self.notes_type.count()
        self.notes_type.addItem("VFX Supervisor")
        self.notes_type.addItem("VFX Producer")
        self.notes_type.addItem("VFX Coordinator")
        self.notes_type.addItem("CineSync")
        self.notes_type.addItem("KickOff")        
        
        print 2, self.notes_type.count()
        self.groupBox_5.setTitle(QtGui.QApplication.translate("notesManagerUi", "xlsx notes file: ", None, QtGui.QApplication.UnicodeUTF8))
        self.select_file.setText(QtGui.QApplication.translate("notesManagerUi", "Select file...", None, QtGui.QApplication.UnicodeUTF8))
        self.close.setText(QtGui.QApplication.translate("notesManagerUi", "Close", None, QtGui.QApplication.UnicodeUTF8))
        #
        self.extraction_group.setEnabled(False)
        self.extraction_result.setEnabled(False)
        self.extract.setEnabled(False)
        #
        newfont = QtGui.QFont("SansSerif", 8, QtGui.QFont.Bold) 
        self.filepath.setFont(newfont)


    def closePressed(self):
    # Close app
        QtCore.QCoreApplication.instance().quit()

    def get_xlsx_file(self):

        # Browse for the xlsx file 
        xlsx_file, file_type = QtGui.QFileDialog.getOpenFileName(self, 'Single File', '~/Desktop/', '*.xlsx')        

        self.xlsx_selected = xlsx_file
        self.filepath.setText(self.xlsx_selected)

        # Validate ui elements
        self.validate_inputs()
        

    def validate_inputs(self):

        subject = self.email_subject.text().replace(' ', '')        
        filepath = self.filepath.text().replace(' ', '')
                
        if subject and filepath:            
            self.extract.setEnabled(True)
        else:
            self.extract.setEnabled(False)

    def define_project(self):

        project = {'type': 'Project', 'id': 341}
        
        return project

    def get_ppl_groups(self):
        filters = []
        fields = ['code', 'sg_type', 'users', 'sg_link']
        groups = self.sg.find('Group', filters, fields)
        
        return groups

    def extract_data_from_file(self):
        # Get xlsx information
        book = openpyxl.load_workbook(self.xlsx_selected)
        sheet = book.active

        return sheet

    def get_coordinator_and_lead(self, user, groups, types=[]):
        
        user_list = []
        # Fetch for lead    
        for group in groups:    
            # Get the lead for the user
            if 'Lead' in types:
                if group['sg_type'] == 'Lead':
                    if user in group['users']:
                        user_list.append(group['sg_link'])
                        break
        # Fetch for coordinator
        for group in groups:                        
            # Get the coordinator for the Lead                  
            if 'Coordinator' in types and user_list:            
                if group['sg_type'] == 'Coordinator':               
                    if user_list[0] in group['users']:
                        user_list.append(group['sg_link'])
                        break
        #
        return user_list

    def get_context_from_client_version(self, project_dict, client_version_name, sg_groups):
        # Get client Version        
        print '\nClient version: {0}'.format(client_version_name)

        fields = ['sg_task', 'entity', 'sg_client_version_number', 'code', 'entity']
        filters = [['client_code', 'contains', client_version_name],
                    ['sg_status_list', 'is_not', 'na'], 
                    ['project', 'is', project_dict],                     
                    ['sg_version_type', 'in', ['Client', 'Client Delivery']]]        
        client_versions = self.sg.find('Version', filters, fields)
        print 'client_versions', len(client_versions), client_versions
        # internal version
        filters = [['client_code', 'is', client_version_name],
                    ['sg_status_list', 'is_not', 'na'], 
                    ['project', 'is', project_dict],
                    ['sg_version_type', 'is', 'For Lead']]
        #                    
        internal_version = self.sg.find_one('Version', filters, fields)


        if internal_version:
            version_number = internal_version['sg_client_version_number']  
            entity = self.sg.find_one(internal_version['entity']['type'], [['id', 'is', internal_version['entity']['id']]], ['code', 'sg_client_version_number'])            
        else:
            version_number = 0
            print 'Internal version for client: {0}, not found.'.format(client_version_name)
            entity = None
               
        task = None
        user_list = []
        users_to_link = []

        #Get task
        if entity:            
            fields = ['task_assignees']
            task = self.sg.find_one('Task', [['id', 'is', internal_version['sg_task']['id']]], fields)            
            user_list = task['task_assignees']

            for user in user_list:          
                users_to_link.extend(self.get_coordinator_and_lead(user, sg_groups, ['Lead', 'Coordinator']))

        result_dict = {
            'version_number': version_number,
            'client_versions': client_versions,            
            'entity': entity,
            'task': task,
            'to': user_list,
            'cc': users_to_link,
            'version_name': client_version_name            
        }

        if internal_version:
            result_dict['internal_version']= internal_version

        return result_dict

    def build_notes_data(self, sg_project, sg_groups, xlsx_sheet):
        notes_data = []

        for row in range(xlsx_sheet.min_row , xlsx_sheet.max_row + 1):                        
            #for column in range(a1.min_column, a1.max_column):
            client_version_name_list = xlsx_sheet.cell(row, 1).value                        
            print 'client_version_name_list: ', client_version_name_list
            if client_version_name_list is not None:
                for client_version_name in client_version_name_list.replace(' ', '').split(','):                
                    #print '--> ', client_version_name, len(client_version_name)
                    note_body = xlsx_sheet.cell(row, 4).value
                    note_data = self.get_context_from_client_version(sg_project, client_version_name, sg_groups)                             
                    note_data['body'] = note_body                    
                    notes_data.append(note_data)
                    #print 'x-- ', note_data
        
        return notes_data

    def create_notes_playlist(self, app_user, mail_desc, date_data, list_of_notes, sg_project):        
        
        batch_queue = []
        print 'mail_desc: ', mail_desc
        playlist_name, ext = os.path.splitext(os.path.basename(self.xlsx_selected))

        print 'Playlisto to create: ', playlist_name.replace(' ', '_')
        print 'Elements in playlist: ', len(list_of_notes)

        version_playlist = []
        for note in list_of_notes:
            if 'internal_version' in note:
                if note['internal_version']:
                    #print 'xxx: ', note['internal_version']
                    version_playlist.append(note['internal_version'])

        print '{0} version added in playlist.'.format(len(version_playlist))

        n_date = self.calendarWidget.selectedDate()
        n_time = self.timeEdit_2.time() 
        
        # Collect date and time
        month = n_date.month()
        day = n_date.day()
        year = n_date.year()
        #
        hour = n_time.hour()
        minu = n_time.minute()
        #        
        mode = 'AM'
        print 'hour: ', hour
        if hour >= 12:
            mode = 'PM'
            if hour > 12:
                hour = hour-12

        if hour == 0:
            hour = 12                
        

        str_date = '{0} {1} {2}  {3}:{4}{5}'.format(str(month).zfill(2), str(day).zfill(2), year, str(hour).zfill(2), str(minu).zfill(2), mode)
        print 'str_date: ', str_date
        datetime_object = datetime.strptime(str_date, '%m %d %Y %I:%M%p')        

        #datetime_object = datetime.strptime('{0} {1} {2}  {3}:{4}{5}'.format(month, day, year, hour, minu, mode), '%m %d %Y %I:%M%p')
        print 'datetime_object', datetime_object

        playlist_data = {
            'code': playlist_name,
            #'sg_date_and_time':date_data,
            'description': mail_desc, 
            'sg_type': 'Client Notes',
            'versions': version_playlist,
            'project': sg_project,     
            'created_by': app_user,
            'sg_date_and_time': datetime_object

             }

        notes_playlist = self.sg.find_one('Playlist', [['code', 'is', playlist_name], ['sg_type', 'is', 'Client Notes']], ['code'])       
        print 'Playlist found: ', notes_playlist
        

        if notes_playlist:
            #sgDB.update('Playlist', notes_playlist['id'], playlist_data)
            print 'Playlist already Created!!!  id: ', notes_playlist['id']            
        else:
            #print '\n','*'*50
            #print playlist_data
            #print '*'*50, '\n'
            batch_data = {"request_type": "create", "entity_type": "Playlist", "data": playlist_data}
            notes_playlist = self.sg.create('Playlist', playlist_data)            
            notes_playlist = self.sg.find_one('Playlist', [['id', 'is', notes_playlist['id']]], ['code'])
            print 'Playlist CREATED: ', notes_playlist#, notes_playlist['id']
            self.sg.update('Playlist', notes_playlist['id'], {'locked': True})
            print 'Playlist Locked!!!'
            #print playlist_data            


        return notes_playlist


    def get_subject(self, s_type, shot_code): 
        subject_text = '{0}'

        if s_type == 'VFX Supervisor':
            subject_text = 'VFXSup: {0}'
        elif s_type == 'VFX Producer':
            subject_text = 'VFXProd: {0}'
        elif s_type == 'VFX Coordinator':
            subject_text = 'VFXCoord: {0}'
        elif s_type == 'CineSync':
            subject_text = 'CineSync: {0}'
        elif s_type == 'Kickoff':
            subject_text = 'VFXSup: {0}'    
  
        return subject_text.format(shot_code)


    def get_header(self, s_type): 
        subject_text = ''

        if s_type == 'VFX Supervisor':
            subject_text = 'Per StephanF,'
        elif s_type == 'VFX Producer':
            subject_text = 'Per ShalenaO,'
        elif s_type == 'VFX Coordinator':
            subject_text = 'Per KatG,'
        elif s_type == 'CineSync':
            subject_text = 'Per StephanF during CineSync session,'
        elif s_type == 'Kickoff':
            subject_text = 'Per Guillaume during Kickoff session,'  

        return subject_text


    def get_author(self):

        import getpass
        user = getpass.getuser()
        
        author_user = self.sg.find_one("HumanUser", [['login', 'is', user]], [])

        if author_user is None:            
            author_user = {'type': 'HumanUser', 'id': 332}
            if user == 'mdives':            
                author_user = {'type': 'HumanUser', 'id': 432}            
        
        return author_user
        

    def create_notes(self, subject_type, ref_header, notes_list, sg_playlist, sg_project):

        last_try = None
        not_founds = ''
        try:
            created = 'None'
            if sg_playlist:
                created = sg_playlist['code']
            info_data = 'Playlist created: {0}\n\nNotes created: {1}\n\n'.format(created, len(notes_list))

            author_user = self.get_author()            

            for note in notes_list:
                print '\n','-'*50
                print 'Building Note for version: ', note['version_name']

                last_try = note['version_name']

                # Note content generation
                pre_header = self.get_header(subject_type)
                header = '{0}, {1}'.format(pre_header, ref_header)
                print header
                                
                subject = self.get_subject(subject_type, note['entity']['code'])
                if note['body'] == None or note['body'] == '':
                    note['body'] == ' '

                body = '_{0}_\n*{2}*\n\n{1}\n'.format(header.encode("utf8"), note['body'].encode("utf8"), note['version_name'])
                #print body

                print 'cv', note['client_versions']
                print 'iv', note['internal_version']
                #body_info = '{0}\n\n{1}\n{2}\n'.format(header.encode("utf8"), header2.encode("utf8"), note['body'].encode("utf8"))
                # Linked entities for the note
                links = []
                if 'internal_version' in note:
                    if not note['internal_version']:                        
                        not_founds += 'xClient version name: {0}, internal version not found in SG.\n'                
                    else:
                        links = [{'type': note['internal_version']['type'], 'id':note['internal_version']['id']}]
                else:
                    print 'xClient version name: {0}, version not found in SG.'  .format(note['version_name'])                                  
                links.extend(note['client_versions'])
                links.extend([{'type': note['entity']['type'], 'id':note['entity']['id']}])

                if sg_playlist:
                    links.extend([{'type': sg_playlist['type'], 'id':sg_playlist['id']}])

                # Notes create data
                notes_data = {
                    'project': sg_project,
                    'subject': subject,
                    'addressings_to': note['to'],
                    'addressings_cc': note['cc'],
                    'user': author_user,
                    'note_links': links,
                    'content': body,
                    'tasks': [note['task']],
                    'sg_note_type': '01. Client'
                }
                print 'note: ', notes_data

                
                try:
                    note_created = self.sg.create('Note', notes_data)
                    print '\nId: {1},  Note created for: {0}'.format(note['version_name'], note_created['id'])
                    info_data += '\n'+ '-'*20 + subject 
                    #info_data += '\nNote: {0}\n'.format(subject)
                    info_data += '\nId: {1},  Note created for: {0}'.format(note['version_name'], note_created['id'])
                except Exception, e: 
                    import traceback

                    print 'ERROOOR !!! ERROOOR !!! ERROOOR !!! ERROOOR !!! ERROOOR !!! ERROOOR !!! '
                    error = traceback.format_exc()                                                  
                    print error
                
                #print '-'*54
                #print 'Note name: {0}\n'.format(subject)
                #print body

        except Exception, e: 
            import traceback

            error = traceback.format_exc()           
            info_data = last_try + '\n\n' + error + '\n\n' + '*'*25 + '\n' + info_data
            print e
            print info_data


        info_data = not_founds + '\n' +  info_data
        return info_data
            

    def extract_xlsx(self):

        author = self.get_author()

        self.extraction_result.clear()
        # Get sshotgun connection
        #self.sg_connect()
        # Get the project for the file
        project = self.define_project()
        # Get shotgun groups 
        groups = self.get_ppl_groups()

        mail_description = self.email_subject.text()

        n_date = self.calendarWidget.selectedDate()
        n_time = self.timeEdit_2.time() 
        
        # Collect date and time
        month = n_date.month()
        day = n_date.day()
        year = n_date.year()
        #
        hour = n_time.hour()
        minu = n_time.minute()
        #        
        mode = 'AM'
        print 'hour: ', hour
        if hour >= 12:
            mode = 'PM'
            if hour > 12:
                hour = hour-12

        if hour == 0:
            hour = 12                
        

        str_date = '{0} {1} {2}  {3}:{4}{5}'.format(str(month).zfill(2), str(day).zfill(2), year, str(hour).zfill(2), str(minu).zfill(2), mode)
        print 'str_date: ', str_date
        mail_date = datetime.strptime(str_date, '%m %d %Y %I:%M%p')        
        print 'mail_date: ', mail_date

        mail_date_header = str(mail_date)[11:]
        print 'print mail_date_header: ', mail_date_header


        notes_sb_type = self.notes_type.currentText()

        # Get data from file
        sheet = self.extract_data_from_file()

        # Create notes data dict list
        notes_to_create = self.build_notes_data(project, groups, sheet)
        #for notex in notes_to_create:
        #    print '\n- ', notex
            

        # Generate header reference text
        header_reference = '{0} :: {1}'.format(mail_description, mail_date_header) 
        print 'header_reference: ', header_reference
        
        #create playlist from notes to create   
        playlist = self.create_notes_playlist(author, header_reference, mail_date, notes_to_create, project)
        

        # Create notes with Linking
        log_result = self.create_notes(notes_sb_type, header_reference, notes_to_create,  playlist, project)

        if log_result:
            self.extraction_group.setEnabled(True)
            self.extraction_result.setEnabled(True)            
        else:
            self.extraction_group.setEnabled(False)
            self.extraction_result.setEnabled(False)

        self.extraction_result.setText(log_result)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    user = ui.get_author()
    if user:
        ui.setupUi(Dialog)
        Dialog.show()
    else:
        print 'Invalid user: ', ui.get_author()

    #ui.sgConnect()
    sys.exit(app.exec_())
