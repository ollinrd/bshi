#!/usr/bin/python
# -*- coding: utf-8
# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import sgtk
import os
import sys
import threading
import os

# by importing QT from sgtk rather than directly, we ensure that
# the code will be compatible with both PySide and PyQt.
from sgtk.platform.qt import QtCore, QtGui
from .ui.dialog import Ui_Dialog

# standard toolkit logger
logger = sgtk.platform.get_logger(__name__)

import hou

class DigitalAssets(object):
    """
    Main publish handler
    """

    def __init__(self, app):
        self._app = app

    def validate_selection(self, type='nodes'):



        selection = hou.selectedNodes()

        if type == 'nodes':
            if len(selection) and len(selection) == 1:
                if hou.selectedNodes()[0].type().name() == 'subnet':
                    return True

        elif type == 'hda':
            if selection and len(selection) == 1:
                if hou.selectedNodes()[0].type().definition() != None:
                    return True

        return False

    def create_from_nodes(self):

        try:
            if self.validate_selection():
                self._app.engine.show_dialog("Crate hda from nodes...", self._app, AppDialog1)
            else:
                hou.ui.displayMessage('Error creating HDA!!!\n\n\n'
                                      'Magic works when you select only one node. \n'
                                      'The node must be a subnetwork type to cast the spell!', buttons=('OK',))
        except:
            import traceback
            print traceback.format_exc()


    def create_from_hda(self):

        if self.validate_selection('hda'):
            self._app.engine.show_dialog("Create from hda...", self._app, AppDialog2)
        else:
            hou.ui.displayMessage('Error creating HDA!!!\n\n\n'
                                  'Magic works when you select only one node. \n'
                                  'The node must be a subnetwork type to cast the spell!', buttons=('OK',))

    def create_from_file(self):

        self._app.engine.show_dialog("Create from file...", self._app, AppDialog3)


    def do_publish_item(self, definition, work_path, publish_path, source, source_publish, fields):

        # Copy lib item to publish
        self._app.ensure_folder_exists(os.path.dirname(publish_path))
        cmd = 'cp {} {}'.format(work_path, publish_path)
        print 'copying: ', cmd
        os.system(cmd)

        name = definition.nodeTypeName()
        operator = definition.nodeType().description()
        print'name: ', name
        print 'operator: ', operator

        prefix, h_type, i_name, version = operator.split('_')
        print prefix, h_type, i_name, version
        print prefix['boom']
        publish_version = fields['version']

        tool_name = name.split('_')[1]
        print 'tool_name: ', tool_name
        #create tool entity if not found
        filters = [['code', 'is', tool_name], ['sg_environment', 'is', 'Houdini'], ['sg_category', 'is', 'HDA']]
        fields = []
        lib_entity = self._app.engine.shotgun.find_one('CustomNonProjectEntity09', filters, fields)
        print 'lib entity? : ', lib_entity
        #
        if not lib_entity:
            data = {
                'code': tool_name,
                'description': '{} Tool'.format(tool_name),
                'sg_environment': 'Houdini',
                'sg_category': 'HDA',
                'sg_type': 'hda',
                'sg_status_list': 'avlble'
            }
            lib_entity = self._app.engine.shotgun.create('CustomNonProjectEntity09', data)


        self._app.execute_hook("hook_publish_path", progress_cb=None, operator=operator, name=name, path=publish_path, version=publish_version, entity=lib_entity)

        """
        # Process source files if required
        if source_publish:
            # Copy source work to publish
            os.system('cp {0} {1}'.format(source, source_publish))
            # Register source publish
            publish_name = os.path.splitext(os.path.basename(source_publish))[0]
            publish_type = 'Source library item'


            # Register lib iten into shotgun
            publish_data = {
                "tk": tk,
                "context": self._app.engine.context,
                "comment": "Source file for the corresponding HDA.\n" + definition.comment(),
                "path": publish_path,
                "name": publish_name,
                "version_number": publish_version,
                "published_file_type": publish_type,
            }
            publish = sgtk.util.register_publish(**publish_data)
            dependencies.append(source_publish)

            print 'Source lirary item published!!!', publish

        # creamos hda en ruta
        hda_node = definition.copyToHDAFile(publish_path, new_name=name, new_menu_name=operator)

        publish_name = os.path.splitext(os.path.basename(publish_path))[0]
        publish_type = 'Library item'

        # Register lib item into shotgun
        publish_data = {
            "code": os.path.basename(publish_path),
            "entity": self._app.engine.context.entity,
            "version_number": publish_version,
            "description": definition.comment(),
            "published_file_type":  publish_type,
            "path": publish_path,
            "name": publish_name,
        }
        #publish = sgtk.util.register_publish(**publish_data)
        self._app.engine.shotgun.create("PublishedFile", publish_data)
        """

        print 'Lirary item published!!!'

    def publish_hda(self):

        try:
            # Verificamos seleccion
            hda_node = hou.selectedNodes()

            if hda_node and len(hda_node) == 1:
                # Verificamos definicion
                hda_def = hda_node[0].type().definition()

                # Verificamos existencia del archivo / overwrite
                entity_type = self._app.engine.context.entity['type'].lower()
                lib_work_template = self._app.engine.get_template_by_name('{}_library_item'.format(entity_type))
                print hda_def.libraryFilePath()

                lib_source_work_template = self._app.engine.get_template_by_name('{}_source_library_item'.format(entity_type))

                if lib_work_template.validate(hda_def.libraryFilePath()):
                    fields = lib_work_template.get_fields(hda_def.libraryFilePath())
                    fields['software_name'] = 'houdini'
                    # Creamos ruta de publish
                    lib_pub_template = self._app.engine.get_template_by_name('library_item'.format(entity_type))
                    print '\n', 1, lib_pub_template
                    lib_pub_path = lib_pub_template.apply_fields(fields)
                    print '\n', 2, lib_pub_path


                    # Source files if exists
                    fields['ext'] = 'py'
                    source_work = lib_source_work_template.apply_fields(fields)
                    print 'source_work: ', source_work
                    if os.path.exists(source_work):
                        source_pub_template = self._app.engine.get_template_by_name('source_library_item'.format(entity_type))
                        souce_path = source_pub_template.apply_fields(fields)
                    else:
                        souce_path = None


                    # Publicar el elemento
                    self.do_publish_item(hda_def, hda_def.libraryFilePath(), lib_pub_path, source_work, souce_path, fields)
                else:
                    hou.ui.displayMessage('Error Publishing HDA!!!\n\n\n'
                                          'This node cant be published. \n'
                                          'Please create HDA in workarea first!', buttons=('OK',))
            else:
                hou.ui.displayMessage('Error Publishing HDA!!!\n\n\n'
                                      'You need a node selected. \n'
                                      'The node must be an HDA created in workarea!', buttons=('OK',))
        except:
            import traceback
            print traceback.format_exc()



class AppDialog1(QtGui.QWidget):
    """
    Main application dialog window
    """
    
    def __init__(self):
        """
        Constructor
        """
        # first, call the base class and let it do its thing.
        QtGui.QWidget.__init__(self)
        
        # now load in the UI that was created in the UI designer
        self.ui = Ui_Dialog()
        self.ui.setupUi_from_nodes(self)

        self.ui.hda_tool.clicked.connect(self.update_names)
        self.ui.hda_asset.clicked.connect(self.update_names)
        self.ui.new_hda_name.textChanged.connect(self.update_names)
        self.ui.create.pressed.connect(self.create_hda)

        # most of the useful accessors are available through the Application class instance
        # it is often handy to keep a reference to this. You can get it via the following method:
        self._app = sgtk.platform.current_bundle()

        self.current_hda_type = 'Tool'

        self.project = self._app.shotgun.find_one('Project',
                                             [['id', 'is', self._app.engine.context.project['id']]],
                                             ['code'])


        entity_type = self._app.engine.context.entity['type']
        entity_id = self._app.engine.context.entity['id']

        self.step = self._app.shotgun.find_one('Step', [['id', 'is', self._app.engine.context.step['id']]], ['short_name'])

        if entity_type == 'Asset':
            fields = ['sg_asset_type']
        else:
            fields = []
            
        self.entity = self._app.shotgun.find_one(entity_type, [['id', 'is', entity_id]], fields)


        # logging happens via a standard toolkit logger
        logger.info("Crate hda from nodes...")
        
        # via the self._app handle we can for example access:
        # - The engine, via self._app.engine
        # - A Shotgun API instance, via self._app.shotgun
        # - An Sgtk API instance, via self._app.sgtk 
        
        # lastly, set up our very basic UI
        #self.ui.context.setText("Current Context: %s" % self._app.context)

        self.context_hda_files = self.get_context_files()

        self.ui.create.setEnabled(False)

    def get_context_files(self):

        entity_type = self._app.engine.context.entity['type'].lower()
        lib_template = self._app.engine.get_template_by_name('{}_library_item'.format(entity_type))

        fields = {
            'category': 'tool',
            'software_name': 'houdini',
            'item_type': 'hda',
            'project': self.project['code'],
            'ext': 'hda',
            'Step': self.step['short_name'],
            'Asset': self._app.engine.context.entity['name'],
            'sg_asset_type': self.entity['sg_asset_type']
        }

        version_paths = self._app.tank.paths_from_template(lib_template, fields, ['version', 'name', ])
        version_paths.sort(reverse=True)

        context_files = {}
        for i in version_paths:
            fields = lib_template.get_fields(i)
            if fields['name'] not in context_files:
                context_files[fields['name']] = [fields['version']]
            else:
                context_files[fields['name']].append(fields['version'])

        return context_files


    def generate_operator(self):
        special_chars = [' ', '~', ':', "'", '+', '[', '\\', '@', '^', '{', '%', '(', '-', '"', '*', '|', ',', '&',
                         '<', '`', '}', '.', '_', '=', ']', '!', '>', ';', '?', '#', '$', ')', '/']

        text = self.ui.new_hda_name.text()

        new_text = ''
        make_upper = False

        for i in text:
            if i in [u'\xf1', u'\xd1']:
                # reset upper flag
                make_upper = True
            elif i.isalpha():
                # toggle case if required
                if not make_upper:
                    new_text += i
                else:
                    new_text += i.upper()
                # Brake if Max text length is less than 32 chars
                if len(new_text) >= 32:
                    break
                # reset upper flag
                make_upper = False
            elif i.isdigit():
                new_text += i
                # Brake if Max text length is less than 32 chars
                if len(new_text) >= 32:
                    break
                # reset upper flag
                make_upper = False
            elif i in special_chars:
                # set upper flag true
                make_upper = True

        return new_text

    def update_operator_name(self, text):

        if text:
            text = 'OVFX_{}'.format(text)
        else:
            text = 'OVFX_{}_<newOperatorName>'.format(self.current_hda_type.lower())

        self.ui.new_operator_name.setText(text)

    def get_file_name(self, name, version):
        version_fix = version.zfill(3)
        file_name = '{}_libary_tool_hda_{}_v{}.hda'.format(self.project['code'], name, version_fix)

        return file_name

    def update_names(self):

        operator_name = self.generate_operator()

        version = 1
        if operator_name in self.context_hda_files:
            version = int(self.context_hda_files[operator_name][0]) + 1

        self.current_hda_type = self.ui.hda_tool.text() if self.ui.hda_tool.isChecked() else self.ui.hda_asset.text()

        self.ui.create.setEnabled(True)
        if not operator_name:
            operator_name = '<newOperatorName>'
            self.ui.create.setEnabled(False)

        new_operator_name = '{}_{}_v{}'.format(self.current_hda_type.lower(), operator_name, str(version).zfill(3))

        self.update_operator_name(new_operator_name)
        #self.update_file_name(operator_name)

    def do_create_hda(self, name, operator, hda_path):
        try:
            # Get the current subnet selected
            subnet_node = hou.selectedNodes()[0]
        except:
            hou.ui.displayMessage('Error creating HDA!!!\n\n\n'
                                  'Magic works when you select only one node. \n'
                                  'The node must be a subnetwork type to cast the spell!', buttons=('OK',))
            return False

        # Ensure folders are created
        self._app.ensure_folder_exists(os.path.dirname(hda_path))

        print 1, name
        print 2, hda_path
        print 3, operator

        # Create new digital asset from temp node
        hda_node = subnet_node.createDigitalAsset(name=name, hda_file_name=hda_path, description=operator,
                                                  min_num_inputs=0, max_num_inputs=0)

        print 'hda_node: ', hda_node
        #hda_node.setName(name)

        # Get HDA definition
        hda_def = hda_node.type().definition()

        # Update and save new HDA
        hda_options = hda_def.options()
        hda_options.setSaveInitialParmsAndContents(True)
        hda_def.setOptions(hda_options)
        hda_def.save(hda_def.libraryFilePath(), hda_node, hda_options)

        self.ui.new_hda_name.setEnabled(False)

        return True

    def get_last_version(self, template, path_list):

        version_list = []
        for p in path_list:
            version_list.append(template.get_fields(p)['version'])

        print 'version_list: ', version_list

        if version_list:
            return version_list[0] + 1
        else:
            return 1

    def create_source(self, destiny):

        try:
            hda_raw_node = hou.selectedNodes()[0]
            code = hda_raw_node.asCode(False, True)

            # Write to the file
            node_file = open(destiny, "w")
            node_file.write(code)
            node_file.close()

            return True, None
        except:
            import traceback
            print traceback.format_exc()

            return False, traceback.format_exc()



    def create_hda(self):

        # Disable the create button
        self.ui.create.setVisible(False)

        hda_name = self.ui.new_hda_name.text()
        operator_name = self.ui.new_operator_name.text()
        name = operator_name.replace('OVFX_', '')
        entity_type = self._app.engine.context.entity['type'].lower()
        lib_template = self._app.engine.get_template_by_name('{}_library_item'.format(entity_type))
        lib_source_template = self._app.engine.get_template_by_name('{}_source_library_item'.format(entity_type))

        type, simple_name, version = name.split('_')
        version_number = int(version.replace('v', ''))

        fields = {
        'category': 'hda',
        'software_name': 'houdini',
        'item_type': self.current_hda_type.lower(),
        'name': simple_name,
        'project': self.project['code'],
        'ext': 'hda',
        'version': version_number,
        'Step': self.step['short_name'],
        'Asset': self._app.engine.context.entity['name'],
        'sg_asset_type': self.entity['sg_asset_type']
        }

        version_paths = self._app.tank.paths_from_template(lib_template, fields, ['version'])
        version_paths.sort(reverse=True)
        fields['version'] = self.get_last_version(lib_template, version_paths)
        new_hda_path = lib_template.apply_fields(fields)
        print new_hda_path
        print operator_name

        fields['ext'] = 'py'
        new_hda_source_path = lib_source_template.apply_fields(fields)

        if os.path.exists(new_hda_path):
            hou.ui.displayMessage(
                'Error creating HDA!!!\n\n\nA previous magic item is already created. '
                '\nType a diferent name to create a new one!',
                buttons=('OK',))
        else:
            # create the node
            if self.do_create_hda(operator_name, operator_name, new_hda_path):
                txt = 'Source created: \n{}\n'.format(new_hda_source_path)
                txt += 'HDA created: \n{}'.format(new_hda_path)

                # Return green message
                self.ui.output_msg.setText(txt)
                self.ui.output_msg.setStyleSheet("QLabel { color: rgb(0, 255, 0);}")
            else:
                # return red error message
                self.ui.output_msg.setText("Error creating HDA!!!")
                self.ui.output_msg.setStyleSheet("QLabel { color: rgb(255, 0, 0);}")


class AppDialog2(QtGui.QWidget):
    """
    Main application dialog window
    """

    def __init__(self):
        """
        Constructor
        """
        # first, call the base class and let it do its thing.
        QtGui.QWidget.__init__(self)

        # now load in the UI that was created in the UI designer
        self.ui = Ui_Dialog()
        self.ui.setupUi_from_hda(self)

        self.ui.new_hda_name.textChanged.connect(self.update_names)
        self.ui.create.pressed.connect(self.create_hda)

        # most of the useful accessors are available through the Application class instance
        # it is often handy to keep a reference to this. You can get it via the following method:
        self._app = sgtk.platform.current_bundle()
        self.project = self._app.shotgun.find_one('Project',
                                                  [['id', 'is', self._app.engine.context.project['id']]],
                                                  ['code'])
        entity_type = self._app.engine.context.entity['type']
        entity_id = self._app.engine.context.entity['id']
        self.step = self._app.shotgun.find_one('Step', [['id', 'is', self._app.engine.context.step['id']]],
                                               ['short_name'])
        if entity_type == 'Asset':
            fields = ['sg_asset_type']
        else:
            fields = []
        self.entity = self._app.shotgun.find_one(entity_type, [['id', 'is', entity_id]], fields)

        # logging happens via a standard toolkit logger
        logger.info("Crate hda from nodes...")

        # via the self._app handle we can for example access:
        # - The engine, via self._app.engine
        # - A Shotgun API instance, via self._app.shotgun
        # - An Sgtk API instance, via self._app.sgtk

        # lastly, set up our very basic UI
        # self.ui.context.setText("Current Context: %s" % self._app.context)

        self.context_hda_files = self.get_context_files()


    def get_context_files(self):

        entity_type = self._app.engine.context.entity['type'].lower()
        lib_template = self._app.engine.get_template_by_name('{}_library_item'.format(entity_type))

        fields = {
            'category': 'tool',
            'software_name': 'houdini',
            'item_type': 'hda',
            'project': self.project['code'],
            'ext': 'hda',
            'Step': self.step['short_name'],
            'Asset': self._app.engine.context.entity['name'],
            'sg_asset_type': self.entity['sg_asset_type']
        }

        version_paths = self._app.tank.paths_from_template(lib_template, fields, ['version', 'name', ])
        version_paths.sort(reverse=True)

        context_files = {}
        for i in version_paths:
            fields = lib_template.get_fields(i)
            if fields['name'] not in context_files:
                context_files[fields['name']] = [fields['version']]
            else:
                context_files[fields['name']].append(fields['version'])

        return context_files

    
    def generate_operator(self):
        special_chars = [' ', '~', ':', "'", '+', '[', '\\', '@', '^', '{', '%', '(', '-', '"', '*', '|', ',', '&',
                         '<', '`', '}', '.', '_', '=', ']', '!', '>', ';', '?', '#', '$', ')', '/']
        text = self.ui.new_hda_name.text()
        new_text = ''
        make_upper = False

        for i in text:
            if i in [u'\xf1', u'\xd1']:
                # reset upper flag
                make_upper = True
            elif i.isalpha():
                # toggle case if required
                if not make_upper:
                    new_text += i
                else:
                    new_text += i.upper()
                # Brake if Max text length is less than 32 chars
                if len(new_text) >= 32:
                    break
                # reset upper flag
                make_upper = False

            elif i.isdigit():
                new_text += i
                # Brake if Max text length is less than 32 chars
                if len(new_text) >= 32:
                    break
                # reset upper flag
                make_upper = False
            elif i in special_chars:
                # set upper flag true
                make_upper = True

        return new_text

    def update_operator_name(self, text):

        if text:
            text = 'OVFX_{}'.format(text)
        else:
            text = 'OVFX_<newOperatorName>'

        self.ui.new_operator_name.setText(text)

    def get_file_name(self, name, version):
        version_fix = version.zfill(3)
        file_name = '{}_libary_tool_hda_{}_v{}.hda'.format(self.project['code'], name, version_fix)

        return file_name

    def update_names(self):

        operator_name = self.generate_operator()

        version = 1
        if operator_name in self.context_hda_files:
            version = int(self.context_hda_files[operator_name][0]) + 1
        operator_name = '{}_v{}'.format(operator_name, str(version).zfill(3))

        self.update_operator_name(operator_name)

    def do_create_hda(self, name, operator, hda_path):
        try:
            # Get the current subnet selected
            current_hda_node = hou.selectedNodes()[0]
        except:
            hou.ui.displayMessage('Error creating HDA!!!\n\n\n'
                                  'Magic works when you select only one node. \n'
                                  'The node must be a subnetwork type to cast the spell!', buttons=('OK',))
            return False

        node = hou.selectedNodes()[0]
        hda_def = hou.selectedNodes()[0].type().definition()

        # Ensure folders are created
        self._app.ensure_folder_exists(os.path.dirname(hda_path))

        # duplicate file


        # Create new digital asset from temp node
        #hda_node = hda_def.save(hda_path, node)
        node.allowEditingOfContents()
        def1 = node.type().definition()
        newDefinition = def1.copyToHDAFile(hda_def.libraryFilePath(), new_name=name, new_menu_name=operator)
        print newDefinition
        #def1.setDescription("kuryNode")
        node.changeNodeType(name, keep_network_contents=False)
        hda_def.updateFromNode(node)
        node.matchCurrentDefinition()

        hda_node = hda_def.save(hda_path, node)

        self.ui.new_hda_name.setEnabled(False)

        return True

    def get_last_version(self, template, path_list):

        version_list = []
        for p in path_list:
            version_list.append(template.get_fields(p)['version'])

        print 'version_list: ', version_list

        if version_list:
            return version_list[0] + 1
        else:
            return 1

    def create_hda(self):

        # Disable the create button
        self.ui.create.setVisible(False)

        hda_name = self.ui.new_hda_name.text()
        operator_name = self.ui.new_operator_name.text()
        x, name, version = operator_name.split('_')
        version = int(version.replace('v', ''))
        entity_type = self._app.engine.context.entity['type'].lower()
        lib_template = self._app.engine.get_template_by_name('{}_library_item'.format(entity_type))

        print name
        #print name['boom']

        fields = {
            'category': 'tool',
            'software_name': 'houdini',
            'item_type': 'hda',
            'name': name,
            'project': self.project['code'],
            'ext': 'hda',
            'version': version,
            'Step': self.step['short_name'],
            'Asset': self._app.engine.context.entity['name'],
            'sg_asset_type': self.entity['sg_asset_type']
        }

        version_paths = self._app.tank.paths_from_template(lib_template, fields, ['version'])
        version_paths.sort(reverse=True)
        fields['version'] = self.get_last_version(lib_template, version_paths)
        new_hda_path = lib_template.apply_fields(fields)
        fields['ext'] = 'py'

        if os.path.exists(new_hda_path):
            hou.ui.displayMessage(
                'Error creating HDA!!!\n\n\nA previous magic item is already created. '
                '\nType a diferent name to create a new one!',
                buttons=('OK',))
        else:
            # create the node
            if self.do_create_hda(name, operator_name, new_hda_path):
                txt = 'HDA created: \n{}'.format(new_hda_path)

                # Return green message
                self.ui.output_msg.setText(txt)
                self.ui.output_msg.setStyleSheet("QLabel { color: rgb(0, 255, 0);}")
            else:
                # return red error message
                self.ui.output_msg.setText("Error creating HDA!!!")
                self.ui.output_msg.setStyleSheet("QLabel { color: rgb(255, 0, 0);}")


class AppDialog3(QtGui.QWidget):
    """
    Main application dialog window
    """

    def __init__(self):
        """
        Constructor
        """

        self.fname = None

        # first, call the base class and let it do its thing.
        QtGui.QWidget.__init__(self)

        # now load in the UI that was created in the UI designer
        self.ui = Ui_Dialog()
        self.ui.setupUi_from_file(self)

        self.ui.new_hda_name.textChanged.connect(self.update_names)
        self.ui.create.pressed.connect(self.create_hda)
        self.ui.browser.pressed.connect(self.browse_file)
        # most of the useful accessors are available through the Application class instance
        # it is often handy to keep a reference to this. You can get it via the following method:
        self._app = sgtk.platform.current_bundle()
        self.project = self._app.shotgun.find_one('Project',
                                                  [['id', 'is', self._app.engine.context.project['id']]],
                                                  ['code'])
        entity_type = self._app.engine.context.entity['type']
        entity_id = self._app.engine.context.entity['id']
        self.step = self._app.shotgun.find_one('Step', [['id', 'is', self._app.engine.context.step['id']]],
                                               ['short_name'])
        if entity_type == 'Asset':
            fields = ['sg_asset_type']
        else:
            fields = []
        self.entity = self._app.shotgun.find_one(entity_type, [['id', 'is', entity_id]], fields)

        # logging happens via a standard toolkit logger
        logger.info("Crate hda from nodes...")

        # via the self._app handle we can for example access:
        # - The engine, via self._app.engine
        # - A Shotgun API instance, via self._app.shotgun
        # - An Sgtk API instance, via self._app.sgtk

        # lastly, set up our very basic UI
        # self.ui.context.setText("Current Context: %s" % self._app.context)

        self.context_hda_files = self.get_context_files()

    def get_context_files(self):

        entity_type = self._app.engine.context.entity['type'].lower()
        lib_template = self._app.engine.get_template_by_name('{}_library_item'.format(entity_type))

        fields = {
            'category': 'tool',
            'software_name': 'houdini',
            'item_type': 'hda',
            'project': self.project['code'],
            'ext': 'hda',
            'Step': self.step['short_name'],
            'Asset': self._app.engine.context.entity['name'],
            'sg_asset_type': self.entity['sg_asset_type']
        }

        version_paths = self._app.tank.paths_from_template(lib_template, fields, ['version', 'name', ])
        version_paths.sort(reverse=True)

        context_files = {}
        for i in version_paths:
            fields = lib_template.get_fields(i)
            if fields['name'] not in context_files:
                context_files[fields['name']] = [fields['version']]
            else:
                context_files[fields['name']].append(fields['version'])

        return context_files


    def browse_file(self):
        print 'Browse file...', os.path.expanduser("~")

        self.fname = QtGui.QFileDialog.getOpenFileName(dir=os.path.expanduser("~"), filter='*.hda')
        print self.fname

        self.ui.label_6.setText(self.fname[0])
        self.ui.label_6.setStyleSheet("QLabel { color: #5aabf6; font-size: 10px;}")


    def generate_operator(self):
        special_chars = [' ', '~', ':', "'", '+', '[', '\\', '@', '^', '{', '%', '(', '-', '"', '*', '|', ',', '&',
                         '<', '`', '}', '.', '_', '=', ']', '!', '>', ';', '?', '#', '$', ')', '/']
        text = self.ui.new_hda_name.text()
        new_text = ''
        make_upper = False

        for i in text:
            if i in [u'\xf1', u'\xd1']:
                # reset upper flag
                make_upper = True
            elif i.isalpha():
                # toggle case if required
                if not make_upper:
                    new_text += i
                else:
                    new_text += i.upper()
                # Brake if Max text length is less than 32 chars
                if len(new_text) >= 32:
                    break
                # reset upper flag
                make_upper = False

            elif i.isdigit():
                new_text += i
                # Brake if Max text length is less than 32 chars
                if len(new_text) >= 32:
                    break
                # reset upper flag
                make_upper = False
            elif i in special_chars:
                # set upper flag true
                make_upper = True

        return new_text

    def update_operator_name(self, text):

        if text:
            text = 'OVFX_{}'.format(text)
        else:
            text = 'OVFX_<newOperatorName>'

        self.ui.new_operator_name.setText(text)

    def get_file_name(self, name, version):
        version_fix = version.zfill(3)
        file_name = '{}_libary_tool_hda_{}_v{}.hda'.format(self.project['code'], name, version_fix)

        return file_name

    def update_names(self):

        operator_name = self.generate_operator()

        version = 1
        if operator_name in self.context_hda_files:
            version = int(self.context_hda_files[operator_name][0]) + 1
        operator_name = '{}_v{}'.format(operator_name, str(version).zfill(3))

        self.update_operator_name(operator_name)
        # self.update_file_name(operator_name)

    def do_create_hda(self, name, operator, hda_path):
        #
        hda_def = hou.hda.definitionsInFile(self.fname[0])[0]

        # Ensure folders are created
        self._app.ensure_folder_exists(os.path.dirname(hda_path))

        # Create new digital asset from temp node
        hda_node = hda_def.copyToHDAFile(hda_path, new_name=name, new_menu_name=operator)
        return True


        print 'new_hda_node: ', hda_node

        self.ui.new_hda_name.setEnabled(False)

        return True

    def get_last_version(self, template, path_list):

        version_list = []
        for p in path_list:
            version_list.append(template.get_fields(p)['version'])

        print 'version_list: ', version_list

        if version_list:
            return version_list[0] + 1
        else:
            return 1

    def create_hda(self):

        # Disable the create button
        self.ui.create.setVisible(False)

        hda_name = self.ui.new_hda_name.text()
        operator_name = self.ui.new_operator_name.text()
        name = operator_name.replace('OVFX_', '')
        entity_type = self._app.engine.context.entity['type'].lower()
        lib_template = self._app.engine.get_template_by_name('{}_library_item'.format(entity_type))
        lib_source_template = self._app.engine.get_template_by_name('{}_source_library_item'.format(entity_type))

        fields = {
            'category': 'tool',
            'software_name': 'houdini',
            'item_type': 'hda',
            'name': self.ui.new_hda_name.text(),
            'project': self.project['code'],
            'ext': 'hda',
            'version': 1,
            'Step': self.step['short_name'],
            'Asset': self._app.engine.context.entity['name'],
            'sg_asset_type': self.entity['sg_asset_type']
        }

        version_paths = self._app.tank.paths_from_template(lib_template, fields, ['version'])
        version_paths.sort(reverse=True)
        fields['version'] = self.get_last_version(lib_template, version_paths)
        new_hda_path = lib_template.apply_fields(fields)
        print new_hda_path

        fields['ext'] = 'py'
        new_hda_source_path = lib_source_template.apply_fields(fields)

        if os.path.exists(new_hda_path):
            hou.ui.displayMessage(
                'Error creating HDA!!!\n\n\nA previous magic item is already created. '
                '\nType a diferent name to create a new one!',
                buttons=('OK',))
        else:

            # create the node
            if self.do_create_hda(name, operator_name, new_hda_path):
                txt = 'HDA created: \n{}'.format(new_hda_path)

                # Return green message
                self.ui.output_msg.setText(txt)
                self.ui.output_msg.setStyleSheet("QLabel { color: rgb(0, 255, 0);}")
            else:
                # return red error message
                self.ui.output_msg.setText("Error creating HDA!!!")
                self.ui.output_msg.setStyleSheet("QLabel { color: rgb(255, 0, 0);}")
