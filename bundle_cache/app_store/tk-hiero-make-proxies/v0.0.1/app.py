# This example adds a right-click Menu to the Timeline View for getting the current Shot Selection.
# After running this action, 'hiero.selectedShots' will return the TrackItems selected in the timeline
from hiero.core import *
import hiero.ui

from hiero.ui import mainWindow # Need this to work around OS X 'Start Dictation' Bug
from PySide2 import QtGui
from PySide2 import QtCore
from PySide2 import QtUiTools
from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2 import QtWidgets
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import threading
import sgtk
import shutil
import tempfile
import subprocess
import os
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml
from sgtk.platform import Application

class Menu_ollin(Application,QAction):
    def init_app(self):
        self.UI_ = self.import_module("tk_hiero_version_zero")
        #self.engine = sgtk.platform.current_engine()
        QAction.__init__(self, "Get Selected Shots", None)
        self.triggered.connect(self.getShotSelection)
        hiero.core.events.registerInterest("kShowContextMenu/kTimeline",
                                           self.eventHandler)

    def getShotSelection(self):
        """Get the shot selection and stuff it in: hiero.selectedShots"""
        warning = QMessageBox()
        warning.setText('Create Proxies of selected clips?')
        warning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        sure = warning.exec_()
        if sure == QMessageBox.Ok:
            engine = sgtk.platform.current_engine()
            self.sg_project = self.engine.context.project
            tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
            self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
            p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                          "project_specs.yml")
            with open(p_specs_path, 'r') as stream:
                try:
                    self.specs = yaml.load(stream)
                except yaml.YAMLError as exc:
                    print(exc)
            # self.engine = sgtk.platform.current_engine()
            # self.config_path = self.engine.context.tank.pipeline_configuration.get_path()
            self.sg = self.engine.shotgun
            self.sg_project = self.sg.find_one('Project',[['id','is',self.sg_project['id']]],['code'])
            self.scans_template = self.engine.get_template_by_name(self.specs['scan_template'])
            os.system("USER=$(whoami)")
            self.user = os.environ["USER"]
            selection = self._selection
            self.shot_dict = []
            if len(selection) == 1:
                clip = selection[0]
                contin = self.validations(clip)
                if contin == 'bad':
                    return
                path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                seq = clip.name().split('_')[0]

                self.set_fields_and_path(clip, seq, path_source)
            else:
                for clip in selection:
                    contin = self.validations(clip)
                    if contin == 'bad':
                        return
                path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                seq = clip.name().split('_')[0]
                self.set_fields_and_path(clip, seq, path_source)
            warning = QMessageBox()
            warning.setText('Proxies job is now in Deadline!')
            warning.setStandardButtons(QMessageBox.Ok)
            warning.exec_()


    # generate paths and Cut
    def send_job(self, python_script, name, dict_):
        nuke_version = self.specs['main_nuke_path']
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        bndl = self.engine.context.tank.pipeline_configuration.get_path()
        nuke_cmd += ' "-t {0} {1} {2}" '.format(python_script,dict_, bndl)
        nuke_cmd += ' -name "{0}" -pool editorial'.format(name)
        nuke_cmd += ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        deadline = self.specs['deadline_command_path']
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd

    def set_fields_and_path(self, clip, seq, path_source):
        self.shot_d = {}
        seq = clip.parent().parent().name()
        track = clip.parent()
        fields = self.scans_template.get_fields(path_source)
        SHOT = self.sg.find_one('Shot',[['project', 'is', self.sg_project], ['code', 'is', str(clip.name())]])
        if SHOT:
            self.shot_d['id'] = SHOT['id']
            self.shot_d['fields'] = fields
            self.shot_d['clip_name'] = str(clip.name())
            self.shot_d['artist'] = self.user
            self.shot_d['seq'] = str(seq)
            self.shot_d['project'] = self.sg_project
            self.shot_d['clip_plate_path'] = path_source
            try:
                FR = float(clip.source().mediaSource().metadata()['media.input.frame_rate'])
            except:
                FR = self.specs['frame_rate']
            self.shot_d['framerate'] = FR
            prefixx = '/nfs/ovfxToolkit/TMP/'
            (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "proxies_dict_", suffix='.txt')
            file = open(dictPath, 'w')
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell = True)
            file.write(str(self.shot_d))
            file.close()
            scriptPath = self.config_path +'/hooks/Hiero/ovfx_make_proxies.py'
            self.send_job(scriptPath,'Proxies_' + str(self.sg_project['code']) + str(clip.name()),dictPath)

    def validations(self, clip):
        path_source = clip.source().mediaSource().fileinfos()[0].filename()
        print 'PATH ORIGINAL:', path_source
        if ' 'in path_source:
            self.error_window(1, '')
            return 'bad'
        elif '.mov' in path_source:
            self.error_window(2, path_source)
            return 'bad'
        seq1 = clip.parent().parent().name()
        #seq = clip.name().split('_')[0]
        seq = clip.parent().parent().name()
        filters = [['code', 'is', seq], ['project', 'is', self.sg_project]]
        secuencia = self.sg.find_one('Sequence', filters)
        if not secuencia:
            self.error_window(3, seq)
            return 'bad'
        filters = [['code', 'is', seq1], ['project', 'is', self.sg_project]]
        secuencia1 = self.sg.find_one('Sequence', filters)
        if not secuencia1:
            self.error_window(3, seq1)
            return 'bad'
        if len(clip.parent().tags()) == 0:
            warning = QMessageBox()
            warning.setText('There is no tag in this track do you want to continue?')
            warning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            sure = warning.exec_()
            if not sure == QMessageBox.Ok:
                return 'bad'
        track_name = clip.parent().name()
        if ' ' in track_name or '_' in track_name:
            self.error_window(4, track_name)
            return 'bad'

        filters_sh = [['code', 'is', clip.name()], ['project', 'is', self.sg_project]]
        SHT = self.sg.find_one('Shot', filters_sh, ['sg_head_in', 'sg_tail_out',
                                                    'sg_external_id', 'sg_offset_frame',
                                                    'sg_cut_in', 'sg_cut_out'])
        if not SHT:
            self.error_window(6, clip.name())
            return 'bad'

        sh_err = self.shot_fields_error(SHT)
        if not sh_err == 'no error':
            self.error_window(5, sh_err)
            return 'bad'

        return 'good'

    def shot_fields_error(self, SHT):
        print SHT
        if not None in [SHT['sg_head_in'], SHT['sg_tail_out'], SHT['sg_external_id'], SHT['sg_offset_frame'], SHT['sg_cut_in'], SHT['sg_cut_out'] ]:
            err = 'no error'
        else:
            err = '''Revisa los valores en la base de datos en shotgun no debe haber un "None":\n
                   head in: {0}
                   tail out: {1}
                   external id: {2}
                   offset frame: {3} 
                   cut in: {4}
                   cut out: {5}
                  '''.format(SHT['sg_head_in'], SHT['sg_tail_out'],
                             SHT['sg_external_id'], SHT['sg_offset_frame'],
                             SHT['sg_cut_in'], SHT['sg_cut_out'])
        print 'ERRR', err
        return err

    def error_window(self, op, text_):
        if op == 1:
            error = 'Error:\nRevisa que la ruta no contenga espacios'
        if op == 2:
            error = 'Error:\nEstas tratando de ingestar un archivo .mov\n{0}'.format(text_)
        if op == 3:
            error = 'Error:\nNo existe la secuencia:{0}'.format(text_)
        if op == 4:
            error = 'Error:\nRevisa el nombre del track:{0}\nRecuerda:no debe tener espacios ni guiones'.format(text_)
        if op == 5:
            error = '{0}'.format(text_)
        if op == 6:
            error = 'No existe el shot: {0}'.format(text_)
        warning = QMessageBox()
        warning.setText(error)
        warning.setStandardButtons(QMessageBox.Ok)
        sure = warning.exec_()
        return sure

    def eventHandler(self, event):
        self._selection = None
        if hasattr(event.sender, 'getSelection') and event.sender.getSelection() is not None and len( event.sender.getSelection() ) != 0:
            selection = event.sender.getSelection() # Here, you could also use: hiero.ui.activeView().selection()
            
            self._selection = [shot for shot in selection if isinstance(shot,hiero.core.TrackItem)] # Filter out just TrackItems
            if len(selection)>0:
                title = "OLLIN Create Proxies Of Selected Shots"
                self.setText(title)
                menu = event.menu 
                for i in event.menu.actions():
                    if str(i.text()) == title:
                        menu.removeAction(i)
                menu.addAction(self)