# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'incoming.ui'
#
# Created: Tue Sep  4 13:49:01 2018
#      by: pyside-uic 0.2.13 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui
from PySide2 import QtWidgets

class Valid_Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(306, 252)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.groupBoxff = QtWidgets.QGroupBox(Form)
        self.groupBoxff.setObjectName("groupBoxff")
        self.verticalLayout_ff = QtWidgets.QVBoxLayout(self.groupBoxff)
        self.verticalLayout_ff.setObjectName("verticalLayout_ff")
        self.inputff = QtWidgets.QLineEdit(self.groupBoxff)
        self.inputff.setObjectName("inputff")
        self.verticalLayout_ff.addWidget(self.inputff)
        self.verticalLayout.addWidget(self.groupBoxff)

        self.groupBoxlf = QtWidgets.QGroupBox(Form)
        self.groupBoxlf.setObjectName("groupBoxlf")
        self.verticalLayout_lf = QtWidgets.QVBoxLayout(self.groupBoxlf)
        self.verticalLayout_lf.setObjectName("verticalLayout_lf")
        self.inputlf = QtWidgets.QLineEdit(self.groupBoxlf)
        self.inputlf.setObjectName("inputlf")
        self.verticalLayout_lf.addWidget(self.inputlf)
        self.verticalLayout.addWidget(self.groupBoxlf)

        self.buttonBox = QtWidgets.QDialogButtonBox(Form)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtWidgets.QApplication.translate("Form", "Ingest Options"))
        self.groupBoxff.setTitle(QtWidgets.QApplication.translate("Form", "First Frame"))
        self.groupBoxlf.setTitle(QtWidgets.QApplication.translate("Form", "last Frame"))

