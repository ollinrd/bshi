# Copyright (c) 2017 Shotgun Software Inc.
# ''
# CONFIDENTIAL AND PROPRIETARY
# ''
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import glob
import os
import maya.cmds as cmds
import pymel.core as pm
import sgtk

HookBaseClass = sgtk.get_hook_baseclass()



class MayaSessionCollector(HookBaseClass):
    """
    Collector that operates on the maya session. Should inherit from the basic
    collector hook.
    """

    @property
    def settings(self):
        """
        Dictionary defining the settings that this collector expects to receive
        through the settings parameter in the process_current_session and
        process_file methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts as
        part of its environment configuration.
        """

        # grab any base class settings
        collector_settings = super(MayaSessionCollector, self).settings or {}

        # settings specific to this collector
        maya_session_settings = {
            "Work Template": {
                "type": "template",
                "default": None,
                "description": "Template path for artist work files. Should "
                               "correspond to a template defined in "
                               "templates.yml. If configured, is made "
                               "available to publish plugins via the "
                               "collected item's properties. ",
            },
        }

        # update the base settings with these settings
        collector_settings.update(maya_session_settings)

        return collector_settings

    def process_current_session(self, settings, parent_item):
        """
        Analyzes the current session open in Maya and parents a subtree of
        items under the parent_item passed in.

        :param dict settings: Configured settings for this collector
        :param parent_item: Root item instance

        """
        print('process_current_session!!')

        # create an item representing the current maya session
        item = self.collect_current_maya_session(settings, parent_item)
        project_root = item.properties["project_root"]

        # Asset structure validation

        context = self.parent.engine.context
        current_entity = context.entity
        entity_name = current_entity['name']
        entity_type = current_entity['type'].lower()
        entity_step = context.step['name'].lower()

        valid_session_geometry = [
            'animation','character fx', 'fx', 'layout', 'look dev', 'matchmove', 'model',
            'rnd','scene assembly']

        valid_step_cams = [
            'layout','rnd', 'matchmove']

        valid_session_notification = [
            'character fx','rig', 'rnd']

        valid_session_cfx = [
            'character fx','fx']

        #
        print 'caaaaaaaaaaameras....'
        valid_session_shaders = [
            'rnd', 'model', 'look dev']

        if  entity_step in valid_session_notification:
            print 'Colect notification item ....'
            self._collect_session_notification(item)

        if entity_type == 'asset' and entity_step in valid_session_shaders:
            self._collect_session_shaders(item)

        elif entity_type == 'shot':

            print 'shot............'
            if entity_step in valid_step_cams:
                print 'Colect camera item............'
                self._collect_session_cameras(item)




        if entity_step in valid_session_geometry:
            if cmds.ls(geometry=True, noIntermediate=True):
                result = [str(mset) for mset in cmds.listSets(allSets=True) if 'set_sgpublish' == str(mset).lower()]
                if result:
                    #print 'colecting session geometry...'
                    geo = self._collect_session_geometry(item)
                    print ('Collect Geoooo')

        if entity_step in valid_session_cfx:
            self._collect_characterFX(item)
            print 'Colecting session CFX'


    
        #project_root = item.properties["project_root"]

    def collect_current_maya_session(self, settings, parent_item):
        """
        Creates an item that represents the current maya session.

        :param parent_item: Parent Item instance

        :returns: Item of type maya.session
        """
        print('collect_current_maya_session!!')
        publisher = self.parent

        # get the path to the current file
        path = cmds.file(query=True, sn=True)

        # determine the display name for the item
        if path:
            file_info = publisher.util.get_file_path_components(path)
            display_name = file_info["filename"]
        else:
            display_name = "Current Maya Session"

        # create the session item for the publish hierarchy
        session_item = parent_item.create_item(
            "maya.session",
            "Maya Session",
            display_name
        )

        # get the icon path to display for this item
        icon_path = os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "maya.png"
        )
        session_item.set_icon_from_path(icon_path)

        # discover the project root which helps in discovery of other
        # publishable items
        project_root = cmds.workspace(q=True, rootDirectory=True)
        session_item.properties["project_root"] = project_root

        # if a work template is defined, add it to the item properties so
        # that it can be used by attached publish plugins
        work_template_setting = settings.get("Work Template")
        if work_template_setting:

            work_template = publisher.engine.get_template_by_name(
                work_template_setting.value)

            # store the template on the item for use by publish plugins. we
            # can't evaluate the fields here because there's no guarantee the
            # current session path won't change once the item has been created.
            # the attached publish plugins will need to resolve the fields at
            # execution time.
            session_item.properties["work_template"] = work_template
            self.logger.debug("Work template defined for Maya collection.")

        self.logger.info("Collected current Maya scene")

        return session_item

    def collect_alembic_caches(self, parent_item, project_root):
        """
        Creates items for alembic caches

        Looks for a 'project_root' property on the parent item, and if such
        exists, look for alembic caches in a 'cache/alembic' subfolder.

        :param parent_item: Parent Item instance
        :param str project_root: The maya project root to search for alembics
        """

        # ensure the alembic cache dir exists
        cache_dir = os.path.join(project_root, "cache", "alembic")
        if not os.path.exists(cache_dir):
            return

        self.logger.info(
            "Processing alembic cache folder: %s" % (cache_dir,),
            extra={
                "action_show_folder": {
                    "path": cache_dir
                }
            }
        )

        # look for alembic files in the cache folder
        for filename in os.listdir(cache_dir):
            cache_path = os.path.join(cache_dir, filename)

            # do some early pre-processing to ensure the file is of the right
            # type. use the base class item info method to see what the item
            # type would be.
            item_info = self._get_item_info(filename)
            if item_info["item_type"] != "file.alembic":
                continue

            # allow the base class to collect and create the item. it knows how
            # to handle alembic files
            super(MayaSessionCollector, self)._collect_file(
                parent_item,
                cache_path
            )

    def _collect_session_geometry(self, parent_item):
        """
        Creates items for session geometry to be exported.

        :param parent_item: Parent Item instance
        """
       
        engine_ = self.parent.engine
        e_type = engine_.context.entity['type'].lower()
        e_step = engine_.context.step['name'].lower()

        geo_item = parent_item.create_item(
            "maya.session.geometry",
            "Geometry",
            "All Session Geometry"
        )

        # get the icon path to display for this item
        icon_path = os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "geometry.png"
        )

        geo_item.set_icon_from_path(icon_path)
        work_path = cmds.file(query=True, sn=True)

        eng_getTemp = self.parent.engine.get_template_by_name
        work_template = eng_getTemp('maya_{0}_work'.format(e_type))
        fields = work_template.get_fields(work_path)

        # General settings
        geo_item.properties["publish_type"] = "Session Geometry"
        publish_name = os.path.basename(work_path).split('.')[0][:-5]
        geo_item.properties["publish_name"] = publish_name

        # --------------------Alembic--------------------------------------
        # Abc geometry export for:
        #      model (Asset)
        #      animation (Shot)
        #      fx (Shot)
        #      rnd (Shot and Asset)
        abc_posible_steps = ['model', 'animation', 'character fx', 'fx', 'rnd', 'matchmove',
                             'layout', 'look dev']

        if e_step in abc_posible_steps:

            # Info
            alembic_template = eng_getTemp('{0}_alembic_cache'.format(e_type))
            alembic_path = alembic_template.apply_fields(fields)

            # Modify item
            geo_item.properties["abc_publish_type"] = "Alembic Geo"
            abc_temp = "{0}_alembic_cache".format(e_type)
            geo_item.properties["abc_template_name"] = abc_temp
            geo_item.properties["abc_publish_path"] = alembic_path

        # --------------------Obj-----------------------------------------
        # Obj geometry export for:
        #      model (Asset)
        #      rnd (Shot and Asset)
        obj_posible_steps = ['look dev', 'rnd', 'model', 'character fx', 'fx']
        if e_step in obj_posible_steps:
            # Info
            obj_template = eng_getTemp('{0}_obj_cache'.format(e_type))
            obj_path = obj_template.apply_fields(fields)
            obj_temp = "{0}_obj_cache".format(e_type)

            # Modify item
            geo_item.properties["obj_publish_type"] = "Obj Cache" 
            geo_item.properties["obj_template_name"] = obj_temp
            geo_item.properties["obj_publish_path"] = obj_path

        # --------------------ASS-----------------------------------------
        # ASS geometry export for:
        #      fx (Shot)
        #      look dev(Asset)
        #      rnd (Shot and Asset)
        ass_posible_steps = ['fx', 'look dev', 'rnd', 'model']

        if e_step in ass_posible_steps:
            # Info
            # ASS geometry export only for ASSET
            print "aaaaaaaaaaaaaaaaaaaaa"
            if e_type== 'shot':
                ass_template = eng_getTemp('maya_work_shot_ass')
                ass_path = ass_template.apply_fields(fields)             

            else:
                ass_template = eng_getTemp('maya_geoass_publish')
                ass_path = ass_template.apply_fields(fields)

            # Modify item
            geo_item.properties["ass_publish_type"] = "Geometry Ass"
            geo_item.properties["ass_template_name"] = "maya_geoass_publish"
            geo_item.properties["ass_publish_path"] = ass_path

        geo_item.properties["fields"] = fields

        geo_item.properties["work_path"] = work_path

        print "work path:", geo_item.properties["work_path"]


        publish_name = os.path.basename(work_path).split('.')[0][:-5]
        geo_item.properties["publish_name"] = publish_name
        return geo_item

    def _collect_session_shaders(self, parent_item):
        """
        Creates items for session geometry to be exported.

        :param parent_item: Parent Item instance
        """

        print '_collect_session_shaders!!'

        shader_item = parent_item.create_item(
            "maya.session.shaders",
            "Shader files",
            "Asset shader files"
        )

        # get the icon path to display for this item
        icon_path = os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "shader_item_v001.png"
        )

        shader_item.set_icon_from_path(icon_path)

        work_path = cmds.file(query=True, sn=True)

        eng_getTemp = self.parent.engine.get_template_by_name

        work_template = eng_getTemp('maya_asset_work')
        fields = work_template.get_fields(work_path)

        shaders_file_template = eng_getTemp('asset_shaders_files')
        shaders_file_path = shaders_file_template.apply_fields(fields)

        shaders_relation_template = eng_getTemp('asset_shaders_relation')
        shaders_relation_path = shaders_relation_template.apply_fields(fields)

        # Shader attributes
        shader_item.properties["shaders_material_path"] = shaders_file_path
        shader_item.properties["shaders_material_type"] = "Shader file"

        shader_item.properties["shaders_relation_path"] = shaders_relation_path
        shader_item.properties["shaders_relation_type"] = "Shader relations"

        shader_item.properties["fields"] = fields
        shader_item.properties["work_path"] = work_path
        shader_item.properties["publish_type"] = "Shader file"


         #Texture attrs

        texture_file_template = eng_getTemp ('asset_textures_files')
        #textures_file_path= texture_file_template.apply_fields(fields)


        shader_item.properties["texture_file_template"] =  texture_file_template 


        publish_name = os.path.basename(work_path).split('.')[0][:-5]

        engine_ = self.parent.engine
        e_step = engine_.context.step['name'].lower()

        if e_step == "model":
            publish_name = publish_name + "_previs"
        shader_item.properties["publish_name"] = publish_name

        print publish_name

        return shader_item

    def _collect_session_cameras(self, parent_item):
        """
        Creates items for session geometry to be exported.

        :param parent_item: Parent Item instance
        """

        cam_item = parent_item.create_item(
            "maya.session.cameras",
            "Cameras",
            "All Session Cameras"
        )

        # get the icon path to display for this item
        icon_path = os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "camera.png"
        )

        cam_item.set_icon_from_path(icon_path)

        cam_item.properties["publish_type"] = "Maya Camera"

        print 'Camera item collected..................'
        return cam_item


    def _collect_characterFX(self, parent_item):
        '''
        creates items for character FX '''

        engine_ = self.parent.engine
        e_type = engine_.context.entity['type'].lower()
        e_step = engine_.context.step['name'].lower()


        creature_fx_item= parent_item.create_item(
            "maya.session.characterFX",
            "Creature FX Collector",
            "Creature FX"
        )
        #icon
        icon_path = os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            'creature_icon.png'
        )

        creature_fx_item.set_icon_from_path(icon_path)
        
        #info
        work_path= cmds.file(q=1, sn=True)

        eng_getTemp= self.parent.engine.get_template_by_name

        #asset/shot
        work_template= eng_getTemp('maya_{0}_work'.format(e_type.lower()))
        fields= work_template.get_fields(work_path)

        #shot
        if e_type.lower()== 'shot':
            ziva_abc_template= eng_getTemp('ziva_shot_alembic_geo')
            ziva_abc_path = ziva_abc_template.apply_fields(fields)

        #asset/shot
        ornatrix_ass_template= eng_getTemp('Ornatrix_{0}_ass'.format(e_type.lower()))
        ornatrix_ass_path= ornatrix_ass_template.apply_fields(fields)


        publish_name = os.path.basename(work_path).split('.')[0][:-5]

        creature_fx_item.properties["publish_name"] = publish_name

        #Ziva ABC Attrs
        if e_type.lower()== 'shot':
            creature_fx_item.properties["ziva_abc_path"] = ziva_abc_path
            creature_fx_item.properties["ziva_publish_type"] = "Ziva Alembic"

        #Ornatrix ASS Attrs
        creature_fx_item.properties["ornatrix_ass_path"]= ornatrix_ass_path
        creature_fx_item.properties["ornatrix_publish_type"]= 'Ornatrix Ass'


        # General Attrs
        creature_fx_item.properties["work_path"] = work_path
        creature_fx_item.properties["fields"] = fields
        creature_fx_item.properties["publish_type"] = 'Ziva Alembic'

        print 'CFX item collected ................'
        return creature_fx_item
        



    def collect_playblasts(self, parent_item, project_root):
        """
        Creates items for quicktime playblasts.

        Looks for a 'project_root' property on the parent item, and if such
        exists, look for movie files in a 'movies' subfolder.

        :param parent_item: Parent Item instance
        :param str project_root: The maya project root to search for playblasts
        """

        movie_dir_name = None

        # try to query the file rule folder name for movies. This will give
        # us the directory name set for the project where movies will be
        # written
        '''
        if "movie" in cmds.workspace(fileRuleList=True):
            # this could return an empty string
            movie_dir_name = cmds.workspace(fileRuleEntry='movie')

        if not movie_dir_name:
            # fall back to the default
            movie_dir_name = "movies"

        # ensure the movies dir exists
        movies_dir = os.path.join(project_root, movie_dir_name)
        print 'movies_dir: ', movies_dir

        if not os.path.exists(movies_dir):
            return

        self.logger.info(
            "Processing movies folder: %s" % (movies_dir,),
            extra={
                "action_show_folder": {
                    "path": movies_dir
                }
            }
        )

        # look for movie files in the movies folder
        for filename in os.listdir(movies_dir):

            # do some early pre-processing to ensure the file is of the right
            # type. use the base class item info method to see what the item
            # type would be.
            item_info = self._get_item_info(filename)
            if item_info["item_type"] != "file.video":
                continue

            movie_path = os.path.join(movies_dir, filename)

            # allow the base class to collect and create the item. it knows how
            # to handle movie files
            item = super(MayaSessionCollector, self)._collect_file(
                parent_item,
                movie_path
            )

            # the item has been created. update the display name to include
            # the an indication of what it is and why it was collected
            item.name = "%s (%s)" % (item.name, "playblast")
        '''
        cam_item = parent_item.create_item(
            "maya.session.playblast",
            "Playblast",
            "Playblast Movie"
        )

        # get the icon path to display for this item
        icon_path = os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "camera.png"
        )

        cam_item.set_icon_from_path(icon_path)

        cam_item.properties["publish_type"] = "Maya Playblast"

        return cam_item


    def collect_rendered_images(self, parent_item):
        """
        Creates items for any rendered images that can be identified by
        render layers in the file.

        :param parent_item: Parent Item instance
        :return:
        """

        # iterate over defined render layers and query the render settings for
        # information about a potential render
        for layer in cmds.ls(type="renderLayer"):

            self.logger.info("Processing render layer: %s" % (layer,))

            # use the render settings api to get a path where the frame number
            # spec is replaced with a '*' which we can use to glob
            (frame_glob,) = cmds.renderSettings(
                genericFrameImageName="*",
                fullPath=True,
                layer=layer
            )

            # see if there are any files on disk that match this pattern
            rendered_paths = glob.glob(frame_glob)

            if rendered_paths:
                # we only need one path to publish, so take the first one and
                # let the base class collector handle it
                item = super(MayaSessionCollector, self)._collect_file(
                    parent_item,
                    rendered_paths[0],
                    frame_sequence=True
                )

                # the item has been created. update the display name to include
                # the an indication of what it is and why it was collected
                item.name = "%s (Render Layer: %s)" % (item.name, layer)
 
    def _collect_session_notification(self, parent_item):

        print '_collect_sesion_notification!!'

        note_item = parent_item.create_item(
            "maya.session.notification",
            "Notification",
            'Publish notification'
        )

        # get the icon path to display for this item
        icon_path = os.path.join(
            self.disk_location,
            os.pardir, "icons", 
            "publish.png"
        )
        note_item.set_icon_from_path(icon_path)

        note_item.properties["publish_type"] = "Maya scene"

        print 'note item collected'
        print 20*"*"
        print note_item
        print 20*"*"
        return note_item

