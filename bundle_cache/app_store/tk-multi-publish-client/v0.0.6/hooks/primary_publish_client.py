# Copyright (c) 2016 ollin vfx
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import sgtk
import glob
import shutil

from tank import Hook
from tank import TankError


class PrimaryPublishClientHook(Hook):

    def execute(self, progress_cb, **kwargs):
        """
        Main hook entry point
        :param path:         the path where frames should be found.
        :param first_frame: Start frame for the output movie
        :param last_frame:  End frame for the output movie
        :param shot:                shotgun shot
        """
        if self.parent.engine.name == "tk-maya":
            self._publish_in_maya(progress_cb)
        elif self.parent.engine.name == "tk-nuke":
            self._publish_in_nuke(progress_cb)
        else:
            raise TankError("Unable to perform publish for unhandled engine %s" % self.parent.engine.name)

    def _publish_in_maya(self, progress_cb):
        import maya.cmds as cmds
        tk_maya = self.parent.engine

        shot = self.parent.shotgun.find_one("Shot",
                                            [["id", "is",
                                              tk_maya.context.entity["id"]]],
                                            ["sg_external_id"])

        work_template = tk_maya.get_template_by_name("maya_shot_work")

        delivery_maya_template = tk_maya.get_template_by_name("maya_shot_delivery")
        delivery_nuke_template = tk_maya.get_template_by_name("nuke_shot_delivery")
        scene_path = os.path.abspath(cmds.file(query=True, sn=True))

        fields = work_template.get_fields(scene_path)

        fields_copy = fields.copy()
        fields_copy["task_name"] = "lensDist"
        fields_copy["version_two"] = self._compute_client_version(delivery_nuke_template, fields_copy, 0)

        fields_copy["task_name"] = "mp"

        fields_copy["external_id"] = shot["sg_external_id"]
        fields_copy["version_four"] = fields_copy["version_two"]

        delivery_path = delivery_maya_template.apply_fields(fields_copy)
        self.parent.ensure_folder_exists(os.path.dirname(delivery_path))
        shutil.copy(scene_path, delivery_path)

    def _publish_in_nuke(self, progress_cb):
        """
        """
        import nuke

        tk_nuke = self.parent.engine

        # get scene path
        script_path = nuke.root().name().replace("/", os.path.sep)
        if script_path == "Root":
            script_path = ""
        script_path = os.path.abspath(script_path)

        # Get the publish render path
        if progress_cb:
            progress_cb(10.0, "Defining paths to publish")

        tasks = self.parent.shotgun.find("Task",
                                         [["entity", "is",
                                          tk_nuke.context.entity]],
                                         ["content"])
        shot = self.parent.shotgun.find_one("Shot",
                                            [["id", "is",
                                              tk_nuke.context.entity["id"]]],
                                            ["sg_external_id"])

        work_template = tk_nuke.get_template_by_name("nuke_shot_work")
        delivery_template = tk_nuke.get_template_by_name("nuke_shot_delivery")

        preview_delivery_template = tk_nuke.get_template_by_name("nuke_shot_preview_delivery")
        preview_delivery_template_exr = tk_nuke.get_template_by_name("nuke_shot_preview_delivery_exr")

        delivery_plates = tk_nuke.get_template_by_name("nuke_shot_delivery_plates")
        undist_delivery_template = tk_nuke.get_template_by_name("nuke_shot_undist_delivery")

        publish_template = tk_nuke.get_template_by_name("nuke_shot_render_jpeg")

        allow_tasks_content = ["cones", "geo", "perspective", "ud"]

        paths_dict = {}
        progress = 20.0
        
        for task in tasks:
            if task["content"] in allow_tasks_content:
                fields = work_template.get_fields(script_path)

                fields["task_name"] = task["content"]
                fields["version"] = self._compute_template_last_version(publish_template, fields, "version")
                
                publish_path = publish_template.apply_fields(fields)
                
                fields_copy = fields.copy()
                fields_copy["task_name"] = "lensDist"
                
                fields_copy["external_id"] = shot["sg_external_id"]
                fields_copy["version_two"] = self._compute_client_version(delivery_template, fields_copy)
                fields_copy["task_name"] = task["content"]
                fields_copy["version_four"] = fields_copy["version_two"]

                if task["content"] == "ud":
                    delivery_path = undist_delivery_template.apply_fields(fields_copy)
                else:
                    delivery_path = preview_delivery_template.apply_fields(fields_copy)
                
                if progress_cb:
                    progress_cb(progress, "Coping images")
                paths_dict[publish_path] = delivery_path
                self.parent.ensure_folder_exists(os.path.dirname(delivery_path))
                self._copy_images_from_template(publish_template, fields,
                                                "version", delivery_path)
                progress += 10.0

        self._read_nodes_from_maya(paths_dict,
                                   preview_delivery_template_exr,
                                   delivery_plates,
                                   fields_copy)

        fields_copy["task_name"] = "lensDist"
        script_delivery_path = delivery_template.apply_fields(fields_copy)
        self.parent.ensure_folder_exists(os.path.dirname(script_delivery_path))
        self._copy_file(script_path, script_delivery_path, paths_dict)

        #self._register_delivery(tk_nuke.context.project, script_delivery_path,
        #                        shot)

    def _read_nodes_from_maya(self, paths_dict, preview_delivery_template,
                              delivery_plates, fields_copy):
        import nuke
        read_nodes = nuke.allNodes('Read')
        count = 1
        for read_node in read_nodes:
            if "maya/images" in read_node["file"].value():
                fields_copy["task_name"] = "objectOkja{0}".format(count)
                delivery_path = preview_delivery_template.apply_fields(fields_copy)
                self.parent.ensure_folder_exists(os.path.dirname(delivery_path))
                self._copy_images(read_node["file"].value(), delivery_path)
                paths_dict[read_node["file"].value()] = delivery_path
                count += 1
            elif "scans/layer" in read_node["file"].value():
                delivery_path = delivery_plates.apply_fields(fields_copy)            
                paths_dict[read_node["file"].value()] = delivery_path

    def _register_delivery(self, project, script_path, shot):

        target_folder = os.path.dirname(os.path.dirname(script_path))
        external = os.path.basename(os.path.dirname(os.path.dirname(script_path)))

        delivery_type = "Client Delivery"
        folder_link = {'local_path': target_folder,
                       'content_type': None,
                       'link_type': 'local',
                       'name': delivery_type}

        context = self.parent.context
        title = "{0} submitted a publish {1} for {2}".format(context.user["name"],
                                                             delivery_type,
                                                          context.entity["name"])

        version = self.parent.shotgun.find("Version",
                                           [['entity', 'is',
                                             self.parent.context.entity]],
                                           ["code"],
                                           [{'field_name': 'created_at',
                                             'direction': 'desc'}],
                                           limit=1)[0]

        contents = "The publish {0} is {1}\n".format(delivery_type, external)
        contents += "The internal version is {0}".format(version["code"])
        data = {'project': project,
                'title': title,
                "description": contents,
                "sg_version": version,
                'sg_external_id': external,
                "sg_folder": folder_link}

        delivery = self.parent.shotgun.create('Delivery', data)
        self.parent.shotgun.share_thumbnail(entities=[delivery],
                                            source_entity=shot)

    def _copy_file(self, script_path, delivery_path, paths_dict):
        import nuke
        read_nodes = nuke.allNodes('Read')

        for read_node in read_nodes:
            if paths_dict.has_key(read_node["file"].value()):
                new_file = paths_dict[read_node["file"].value()]
                read_node["file"].setValue(new_file.replace("jpeg", "jpg"))

        nuke.scriptSave()
        shutil.copy(script_path, delivery_path)
        paths_dict_inv = {}

        for k, v in paths_dict.iteritems():
            paths_dict_inv[v] = k

        for read_node in read_nodes:
            if paths_dict_inv.has_key(read_node["file"].value()):
                new_file = paths_dict_inv[read_node["file"].value()]
                read_node["file"].setValue(new_file)

        nuke.scriptSave()

    def _copy_images_from_template(self, template, fields, key, delivery_path):
        images = self.parent.tank.paths_from_template(template, fields, [key])

        for image in images:
            basename = image.split(".")[0]
            delivery_basename = delivery_path.split(".")[0]
            shutil.copy(image, image.replace(basename, delivery_basename))

    def _copy_images(self, publish_path, delivery_path):

        splitext = os.path.splitext(publish_path)
        wildcard = splitext[0].replace("%04d", "")

        publish_images = sorted(glob.glob(wildcard + "*" + splitext[1]))
        for index in range(0, len(publish_images)):
          
            basename = publish_images[index].split(".")[0]
            delivery_basename = delivery_path.split(".")[0]
            
            print (publish_images[index],
                   publish_images[index].replace(basename,
                                                 delivery_basename))
            shutil.copy(publish_images[index],
                        publish_images[index].replace(basename,
                                                      delivery_basename))

    def _compute_template_last_version(self, template, fields, key,
                                       increment=0):
        existing_versions = self.parent.tank.paths_from_template(template,
                                                                 fields,
                                                                 [key])

        print existing_versions, "asasas"
        version_numbers = [template.get_fields(v).get(key) for v in existing_versions]

        if version_numbers:
            return max(version_numbers) + increment

        return 1

    def _compute_client_version(self, template, fields, add=1):
        return self._compute_template_last_version(template, fields,
                                                   "version_two",
                                                   add)
