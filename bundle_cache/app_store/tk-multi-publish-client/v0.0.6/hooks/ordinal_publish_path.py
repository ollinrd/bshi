import tank
from tank import Hook

from subprocess import Popen, PIPE


class PublishPaths(Hook):
    """
    Single hook that implements publish of the primary task
    """    
    def execute(self, primary_path):
        """
        """
        if primary_path:
            return self._get_primary_publish_path()
        else:
            return self._get_secondary_publish_path()

    def _get_secondary_publish_path(self):
        tk_engine = tank.platform.current_engine()
        publish_template = tk_engine.get_template_by_name(self._app.get_setting("secondary_publish_path_template"))

        work_template = tk_engine.get_template_by_name("nuke_shot_work")
        fields = work_template.get_fields(self._get_dcc_file_path())

        shot = self._app.shotgun.find_one("Shot",
                                          [["id", "is", 
                                            self._app.context.entity["id"]]],
                                          ["sg_external_id"])

        if self._app.get_setting("lower_external_id"):
            fields["external_id"] = shot["sg_external_id"].lower()
        else:
            fields["external_id"] = shot["sg_external_id"]
        
        maximum = self._get_version_max(publish_template, fields)

        fields["version"] =  maximum

        print maximum, "secondary", fields
       
        return maximum, publish_template.apply_fields(fields)

    def _get_primary_publish_path(self, publish_template):
        tk_engine = tank.platform.current_engine()
        secondary_template = tk_engine.get_template_by_name(self._app.get_setting("secondary_publish_path_template"))
        publish_template = tk_engine.get_template_by_name(self._app.get_setting("primary_publish_path_template"))
        
        work_template = tk_engine.get_template_by_name("nuke_shot_work")
        fields = work_template.get_fields(self._get_dcc_file_path())
        shot = self._app.shotgun.find_one("Shot",
                                          [["id", "is", 
                                            self._app.context.entity["id"]]],
                                          ["sg_external_id"])

        fields["external_id"] = shot["sg_external_id"]

        maximum = self._get_version_max(secondary_template, fields)
        if maximum!=1:
            maximum-=1
            
        fields["version"] =  maximum
        
        return maximum, publish_template.apply_fields(fields)