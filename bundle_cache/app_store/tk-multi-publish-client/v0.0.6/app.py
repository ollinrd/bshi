# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

"""
Quick versions to shotgun from Nuke

"""
import tank

from tank import TankError
from tank.platform.qt import QtCore, QtGui


class PublishClient(tank.platform.Application):

    def init_app(self, init_ui=True):

        pass

        
    # @property
    def __get_version(self):
        return self._version

    # @version.setter
    def __set_version(self, value):
        self._version = value
    version = property(__get_version, __set_version)

    # @property
    def __get_checkboxes_values(self):
        return self._checkboxes_values

    # @checkboxes_values.setter
    def __set_checkboxes_values(self, value):
        self._checkboxes_values = value
    checkboxes_values = property(__get_checkboxes_values, __set_checkboxes_values)

    def primary_publish(self, checkboxes_values, version):
        self.checkboxes_values = checkboxes_values
        self.version = version 
        self.execute_hook("hook_primary_publish_client",
                          progress_cb=None)

    def secondary_publish(self, checkboxes_values, version):
        self.checkboxes_values = checkboxes_values
        self.version = version

        print self.checkboxes_values, "app"
        self.execute_hook("hook_secondary_publish_client",
                          progress_cb=None)

    