# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

from tank.platform.qt import QtCore, QtGui
import tank
from .output import PublishOutput
from .item import Item
from .task import Task
from tank import TankError

import os


class PublishForm(QtGui.QWidget):
    """
    Implementation of the main publish UI
    """

    # signals
    publish = QtCore.Signal()
    
    def __init__(self, app, handler, parent=None):
        """
        Construction
        """
        QtGui.QWidget.__init__(self, parent)
        self._app = app
        
        # TODO: shouldn't need the handler
        self._handler = handler
    
        self._primary_task = None
        self._tasks = []
        
        # set up the UI
        from .ui.publish_form import Ui_PublishForm
        self._ui = Ui_PublishForm() 
        self._ui.setupUi(self)

        self._ui.publish_settings.publish.connect(self._on_publish)
        self._ui.publish_settings.cancel.connect(self.on_close)
        self._ui.publish_result.close.connect(self.on_close)
            
        # always start with the details page:
        self._set_primary_task()
        self._initialize()

    @property
    def selected_tasks(self):
        """
        The currently selected tasks
        """
        return self._get_selected_tasks()

    @property
    def sync(self):
        """
        The sync boolean
        """
        return self._ui.publish_settings.sync

    @property
    def make_first_video(self):
        """
        The second_video boolean
        """
        return self._ui.publish_settings.make_first_video

    @property
    def make_second_video(self):
        """
        The first_video boolean
        """
        return self._ui.publish_settings.make_second_video

    @property
    def get_version_to_publish(self):
        """
        The first_video boolean
        """
        return self._ui.publish_settings.version_spb

    @property
    def get_frame_range(self):
        return (int(self._ui.publish_settings.actual_start_frame),
                int(self._ui.publish_settings.actual_end_frame))

    def set_frame_range(self, first_frame, last_frame):
        self._ui.publish_settings.actual_start_frame = first_frame
        self._ui.publish_settings.actual_end_frame = last_frame

    def set_secondary_visible(self, value):
        self._ui.publish_settings.secondaryVisible(value)

    def show_publish_details(self):
        self._ui.pages.setCurrentWidget(self._ui.publish_details)

    def show_publish_progress(self, title):
        self._ui.pages.setCurrentWidget(self._ui.publish_progress)
        self._ui.publish_progress.title = title

    def set_progress_reporter(self, reporter):
        self._ui.publish_progress.set_reporter(reporter)

    def show_publish_result(self, success, errors):
        """
        Show the result of the publish in the UI
        """
        # show page:
        self._ui.pages.setCurrentWidget(self._ui.publish_result)
        self._ui.publish_result.status = success
        self._ui.publish_result.errors = errors

    def set_primary_path(self):
        maximum, publish_path = self._app.execute_hook("hook_ordinal_publish_path",
                                                       primary = True)
        path_list = publish_path.split("/")[4:]

        self._ui.publish_settings.set_version_range(maximum)

        self._ui.publish_settings.publish_path_label = os.path.dirname("/".join(path_list))
        self._ui.publish_settings.file_name_label = os.path.basename(publish_path)

    def _initialize(self):
        """
        Initialize UI with information provided
        """

        engine = self._app.engine.name

        if engine == "tk-maya":
            import maya.cmds as cmds
            current_in = cmds.playbackOptions(query=True, minTime=True)
            current_out = cmds.playbackOptions(query=True, maxTime=True)

        elif engine == "tk-nuke":
            import nuke
            current_in = int(nuke.root()["first_frame"].value())
            current_out = int(nuke.root()["last_frame"].value())

        # pull initial data from handler:
        self._ui.publish_settings.actual_start_frame = str(current_in)
        self._ui.publish_settings.actual_end_frame = str(current_out)

        (new_in, new_out) = self._get_frame_range_from_shotgun()

        self._ui.publish_settings.sg_start_frame = str(new_in)
        self._ui.publish_settings.sg_end_frame = str(new_out)

        first_name = self._app.get_setting("secondary_first_delivery_name")
        second_name = self._app.get_setting("secondary_second_delivery_name")

        maximum, publish_path = self._app.execute_hook("hook_ordinal_publish_path",
                                                       primary = False)
        self._ui.publish_settings.set_version_range(maximum)

        path_list = publish_path.split("/")[4:]

        self._ui.publish_settings.publish_path_label = os.path.dirname("/".join(path_list))
        self._ui.publish_settings.file_name_label = os.path.basename(publish_path)

        if first_name:
            self._ui.publish_settings.make_first_video_text = "Make {0} video".format(first_name)
        else:
            self._ui.publish_settings.make_first_video_text = ""

        if second_name:
            self._ui.publish_settings.make_second_video_text = "Make {0} video".format(second_name)
        else:
            self._ui.publish_settings.make_second_video_text = ""

        self._ui.publish_result.set_change_details(self._app.get_setting("change_details"))

    def _get_frame_range_from_shotgun(self):
        """
        Returns (in, out) frames from shotgun.
        """
        # we know that this exists now (checked in init)
        entity = self._app.context.entity

        sg_entity_type = self._app.context.entity["type"]
        sg_filters = [["id", "is", entity["id"]]]

        sg_in_field = "sg_cut_in"
        sg_out_field = "sg_cut_out"
        fields = [sg_in_field, sg_out_field]

        data = self._app.shotgun.find_one(sg_entity_type, filters=sg_filters,
                                          fields=fields)

        # check if fields exist!
        if sg_in_field not in data:
            raise tank.TankError("Configuration error: Your current context is connected to a Shotgun "
                                 "%s. This entity type does not have a "
                                 "field %s.%s!" % (sg_entity_type, sg_entity_type, sg_in_field))

        if sg_out_field not in data:
            raise tank.TankError("Configuration error: Your current context is connected to a Shotgun "
                                 "%s. This entity type does not have a "
                                 "field %s.%s!" % (sg_entity_type, sg_entity_type, sg_out_field))

        return ( data[sg_in_field], data[sg_out_field] )

    def _get_selected_tasks(self):
        """
        Get a list of the selected tasks that 
        should be published
        """
        
        # always publish primary task:
        selected_tasks = [self._primary_task]
        
        # get secondary tasks from details form:
        
        return selected_tasks
        
    def _set_primary_task(self):
        """
        Set the primary task and update the UI accordingly
        """
        primary_output_dict = {}
        primary_output_dict["scene_item_type"] = self._app.get_setting("primary_scene_item_type")
        primary_output_dict["display_name"] = self._app.get_setting("primary_display_name")
        primary_output_dict["description"] = self._app.get_setting("primary_description")
        primary_output_dict["icon"] = self._app.get_setting("primary_icon")
        primary_output_dict["tank_type"] = self._app.get_setting("primary_tank_type")
        primary_output_dict["publish_template"] = self._app.get_setting("primary_publish_template")
        primary_output = PublishOutput(self._app, primary_output_dict, name=PublishOutput.PRIMARY_NAME, selected=True, required=True)
        
        if self._app.engine.name == "tk-maya":
            import maya.cmds as cmds
            script_name = os.path.abspath(cmds.file(query=True, sn=True))
        elif self._app.engine.name == "tk-nuke":
            import nuke
            script_name = nuke.root().name()

        if script_name == "Root":
            raise TankError("Please Save your file before Publishing")
        
        script_file = script_name.replace("/", os.path.sep)
        script_name = os.path.basename(script_file)

        item = Item({"type": "work_file", "name": script_name})
        self._primary_task = Task(item, primary_output)

        # connect to the primary tasks modified signal so that we can
        # update the UI if something changes.
        self._primary_task.modified.connect(self._on_primary_task_modified)
        
        
        # build details text and set:            
        lines = []
        name_str = self._primary_task.output.display_name
        if self._primary_task.item.name:
            name_str = "%s - %s" % (name_str, self._primary_task.item.name)
        lines.append("<span style='font-size: 16px'}><b>%s</b></span><span style='font-size: 12px'}>" % (name_str))
        if self._primary_task.output.description:
            lines.append("%s" % self._primary_task.output.description)
        if self._primary_task.item.description:
            lines.append("%s" % self._primary_task.item.description)
       
       
    def _on_primary_task_modified(self):
        """
        Called when the primary task has been modified, e.g. there are new errors to report
        """
        # update the errors display for the primary publish
        self.__update_primary_errors()
        
    def _on_publish(self):
        """
        Slot called when the publish button in the dialog is clicked
        """
        self.publish.emit()

    def on_close(self):
        """
        Slot called when the cancel or close signals in the dialog 
        are recieved
        """
        self.close()


