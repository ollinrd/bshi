import os
import nuke
import tempfile
import subprocess
import psutil

from tank import TankError
from tank.platform.qt import QtGui

from .progress import TaskProgressReporter


class PublishHandler(object):
    """
    Main publish handler
    """

    def __init__(self, app):
        """
        Construction
        """
        self._app = app
        self._publish_type = "primary"

    def show_publish_dlg(self, publish_type):
        """
        Displays the publish dialog
        """

        self.publish_type = publish_type

        try:
            # create new multi-publish dialog instance
            from .publish_form import PublishForm
            display_name = self._app.get_setting("display_name")
            publish_form = self._app.engine.show_dialog(display_name,
                                                        self._app,
                                                        PublishForm,
                                                        self._app, self)
            if self.publish_type == "primary":
                publish_form.set_secondary_visible(False)
                publish_form.set_primary_path()            

            publish_form.publish.connect(lambda f=publish_form: self._on_publish(f))

        except TankError, e:
            QtGui.QMessageBox.information(None, "Unable To Publish!", "%s" % e)

        except Exception, e:
            self._app.log_exception("Unable to publish")

    # @property
    def __get_publish_type(self):
        return self._publish_type

    # @publish_type.setter
    def __set_publish_type(self, value):
        self._publish_type = value
    publish_type = property(__get_publish_type, __set_publish_type)

    def _on_publish(self, publish_form):
        publish_errors = []
        try:
            
            self._check_version(publish_form)

            selected_tasks = publish_form.selected_tasks

            # create progress reporter and connect to UI:
            progress = TaskProgressReporter(selected_tasks)
            publish_form.set_progress_reporter(progress)

            # show pre-publish progress:
            publish_form.show_publish_progress("Publishing to client")
            progress.reset()

            self._get_version()
            if publish_form.sync:
                multi_setframerange = self._app.engine.apps.get("tk-multi-setframerange")

                if multi_setframerange:
                    multi_setframerange.run_app()

                publish_form.set_frame_range(str(int(nuke.root()["first_frame"].value())),
                                             str(int(nuke.root()["last_frame"].value())))

            script_path = nuke.root().name().replace("/", os.path.sep)

            checkboxes_values = {"make_fist_video": publish_form.make_first_video,
                                 "make_second_video": publish_form.make_second_video,
                                 "frame_range": publish_form.get_frame_range,
                                 "version_to_publish": publish_form.get_version_to_publish,
                                 "script_path": script_path }
            self._app.checkboxes_values = checkboxes_values

            nuke_cmd = self._construct_nuke_cmd(checkboxes_values, self._app.version)

            deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
            subprocess.Popen(deadline + " " + nuke_cmd, shell=True)

            self._show_deadline()

        except TankError, e:
            publish_errors.append("%s" % e)

        publish_form.show_publish_progress("Registering into shotgun")
        progress.report(50.0, "Creating new entry")

        # show publish result:
        publish_form.show_publish_result(not publish_errors,
                                         publish_errors)

    def _show_deadline(self):
        renderman_path = "/opt/Thinkbox/Deadline10/bin/deadlinemonitor"
        for pid in psutil.pids():
            p = psutil.Process(pid)
            if "deadlinemonitor" in p.name():
                break
        else:
            subprocess.Popen((renderman_path), shell=True)

    def _construct_nuke_cmd(self, checkboxes_values, version):
        script_path = nuke.root().name().replace("/", os.path.sep)
        script_name = os.path.basename(script_path)
        prefix = os.path.dirname(script_path) + "/."

        (fd, python_script) = tempfile.mkstemp(suffix='.py',
                                               prefix=prefix)
        open_python_file = open(python_script, "w")

        script = nuke.root().name().replace("/", os.path.sep)

        config_path = self._app.engine.context.tank.pipeline_configuration.get_path()
        python_code = """
import os
import sys
sys.path.append('{0}/install/core/python')
import sgtk

try:
   for arg in sys.argv:
       if arg.endswith(".nk") and os.path.exists(arg):
           path_or_id = arg
           break
   print path_or_id
   
   from tank_vendor.shotgun_authentication import ShotgunAuthenticator
   cdm = sgtk.util.CoreDefaultsManager()
   authenticator = ShotgunAuthenticator(cdm)
   user = authenticator.get_user()   
   sgtk.set_authenticated_user(user)

   if not path_or_id.isdigit():
       tk = sgtk.sgtk_from_path(path_or_id)
   else:
       tk = sgtk.sgtk_from_entity("Task", int(path_or_id))

   tk.synchronize_filesystem_structure()
   if not path_or_id.isdigit():
       ctx = tk.context_from_path(path_or_id)

   if sgtk.platform.current_engine():
       sgtk.platform.current_engine().destroy()
   # Start engine with context    
   sgtk.platform.start_engine("tk-nuke", tk, ctx)
except Exception, e:
   print "Failed to start Toolkit Engine - %s" % e

tk_nuke = sgtk.platform.current_engine()

publish_client_app = tk_nuke.apps.get("tk-multi-publish-client", False)
""".format(config_path)

        if self.publish_type != "primary":
            ptype = "review"
            python_code += "publish_client_app.secondary_publish({0}, {1})"
        else:
            ptype = "delivery"
            python_code += "publish_client_app.primary_publish({0}, {1})"

        print python_code.format(checkboxes_values, version)
        open_python_file.write(python_code.format(checkboxes_values, version))
        open_python_file.close()

        #current_version = nuke.NUKE_VERSION_STRING
        # Proyect must use this Nuke version to work and render
        nuke_version = "Nuke11.3v4/Nuke11.3"

        batch_name = os.path.splitext(script_name)[0]

        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " /usr/local/{0} -arguments ".format(nuke_version)
        nuke_cmd += ' "-t {0} {1}" '.format(python_script, script)
        nuke_cmd += ' -frames 1 -pool editorial'
        nuke_cmd += ' -name "Publish client {0}" '.format(ptype)
        nuke_cmd += " -prop BatchName={0}".format(batch_name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True)

        return nuke_cmd

    def _check_version(self, publish_form):
        if publish_form.get_version_to_publish == 0:
            raise TankError("Please selected a valid version to publish")

    def _get_version(self):
        # get scene path
        engine_name = self._app.engine.name
        # depending on engine:
        if engine_name == "tk-maya":
            import maya.cmds as cmds
            file_path = os.path.abspath(cmds.file(query=True, sn=True))
        elif engine_name == "tk-nuke":
            import nuke
            file_path = nuke.root().name().replace("/", os.path.sep)
            if file_path == "Root":
                file_path = ""
            file_path = os.path.abspath(file_path)

        elif engine_name == "tk-houdini":
            import hou
            file_path = os.path.abspath(hou.hipFile.name())

        elif engine_name == "tk-photoshop":
            import photoshop

            doc = photoshop.app.activeDocument
            if doc is None:
                raise TankError("There is no currently active document!")

            # get scene path
            file_path = doc.fullName.nativePath

        basename = os.path.basename(os.path.splitext(file_path)[0])
        basename = basename.replace(".", "_")
        version = self._app.shotgun.find_one("Version",
                                             [["code", "contains", basename]],
                                             ['sg_path_to_frames',
                                              "sg_first_frame",
                                              "sg_last_frame",
                                              "description", "entity",
                                              "project", "code", "created_by",
                                              "sg_notes"])

        if not version:
            raise TankError("There is not a publish version for this file. Open a version that has a published version")

        self._app.version = version

    def do_primary_publish(self, progress_cb=None,
                           publish_errors=[]):
        """
        :param version_id:    A integer with the id of the version .
        """
        try:
            self._app.execute_hook("hook_primary_publish_client",
                                   progress_cb=progress_cb,)
        except TankError, e:
            publish_errors.append("%s" % e)

    def do_secondary_publish(self, progress_cb=None,
                             publish_errors=[]):
        """
        :param version_id:    A integer with the id of the version .
        """

        try:
            self._app.execute_hook("hook_secondary_publish_client",
                                   progress_cb=progress_cb)
        except TankError, e:
            QtGui.QMessageBox.information(None, "Unable To Publish!", "%s" % e)
            publish_errors.append("%s" % e)
