# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'publish_form.ui'
#
# Created: Tue Mar  7 12:54:22 2017
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui

class Ui_PublishForm(object):
    def setupUi(self, PublishForm):
        PublishForm.setObjectName("PublishForm")
        PublishForm.resize(450, 250)
        PublishForm.setAutoFillBackground(False)
        self.verticalLayout = QtGui.QVBoxLayout(PublishForm)
        self.verticalLayout.setSpacing(20)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pages = QtGui.QStackedWidget(PublishForm)
        self.pages.setObjectName("pages")
        self.publish_settings = PublishSettingsForm()
        self.publish_settings.setObjectName("publish_settings")
        self.pages.addWidget(self.publish_settings)
        self.publish_progress = PublishProgressForm()
        self.publish_progress.setObjectName("publish_progress")
        self.pages.addWidget(self.publish_progress)
        self.publish_result = PublishResultForm()
        self.publish_result.setObjectName("publish_result")
        self.pages.addWidget(self.publish_result)
        self.verticalLayout.addWidget(self.pages)

        self.retranslateUi(PublishForm)
        self.pages.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(PublishForm)

    def retranslateUi(self, PublishForm):
        PublishForm.setWindowTitle(QtGui.QApplication.translate("PublishForm", "Form", None, QtGui.QApplication.UnicodeUTF8))

from ..publish_settings_form import PublishSettingsForm
from ..publish_result_form import PublishResultForm
from ..publish_progress_form import PublishProgressForm
import resources_rc
