# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import tank
from tank.platform.qt import QtCore, QtGui
 
class PublishSettingsForm(QtGui.QWidget):
    """
    Implementation of the main publish UI
    """
    publish = QtCore.Signal()
    cancel = QtCore.Signal()
    
    def __init__(self, parent=None):
        """
        Construction
        """
        QtGui.QWidget.__init__(self, parent)
    
        self._status = True
        self._errors = []
    
        # set up the UI
        from .ui.publish_settings_form import Ui_PublishSettingsForm
        self._ui = Ui_PublishSettingsForm() 
        self._ui.setupUi(self)
        
        # hook up buttons
        self._ui.publish_btn.clicked.connect(self._on_publish)
        self._ui.cancel_btn.clicked.connect(self._on_cancel)

    # @property
    def __get_actual_start_frame(self):
        return self._safe_to_string(self._ui.actual_first_frame_line.text()).strip()

    # @actual_start_frame.setter
    def __set_actual_start_frame(self, value):
        self._ui.actual_first_frame_line.setText(value)
    actual_start_frame = property(__get_actual_start_frame, __set_actual_start_frame)

    # @property
    def __get_actual_end_frame(self):
        return self._safe_to_string(self._ui.actual_last_frame_line.text()).strip()

    # @end_frame.setter
    def __set_actual_end_frame(self, value):
        self._ui.actual_last_frame_line.setText(value)
    actual_end_frame = property(__get_actual_end_frame, __set_actual_end_frame)

    # @property
    def __get_sg_start_frame(self):
        return self._safe_to_string(self._ui.sg_first_frame_label.text()).strip()

    # @sg_start_frame.setter
    def __set_sg_start_frame(self, value):
        self._ui.sg_first_frame_label.setText(value)
    sg_start_frame = property(__get_sg_start_frame, __set_sg_start_frame)

    # @property
    def __get_sg_end_frame(self):
        return self._safe_to_string(self._ui.sg_first_frame_label.text()).strip()

    # @sg_end_frame.setter
    def __set_sg_end_frame(self, value):
        self._ui.sg_last_frame_label.setText(value)
    sg_end_frame = property(__get_sg_end_frame, __set_sg_end_frame)

    # @property
    def __get_sync(self):
        return self._ui.sync_cb.isChecked()

    # @sync.setter
    def __set_sync(self, value):
        self._ui.sync_cb.setChecked(value)
    sync = property(__get_sync, __set_sync)

    # @property
    def __get_make_first_video(self):
        return self._ui.first_video_cb.isChecked()

    # @make_first_video.setter
    def __set_make_first_video(self, value):
        self._ui.first_video_cb.setChecked(value)
    make_first_video = property(__get_make_first_video, __set_make_first_video)

    # @property
    def __get_make_first_video_text(self):
        return self._ui.first_video_cb.text()

    # @make_first_video_text.setter
    def __set_make_first_video_text(self, value):
        if not value:
            self._ui.first_video_cb.setVisible(False)
        self._ui.first_video_cb.setText(value)      
    make_first_video_text = property(__get_make_first_video_text,
                                     __set_make_first_video_text)

    # @property
    def __get_make_second_video(self):
        return self._ui.second_video_cb.isChecked()

    # @make_second_video.setter
    def __set_make_second_video(self, value):
        self._ui.second_video_cb.setChecked(value)
    make_second_video = property(__get_make_second_video, __set_make_second_video)

    # @property
    def __get_make_second_video_text(self):
        return self._ui.second_video_cb.text()

    # @make_second_video_text.setter
    def __set_make_second_video_text(self, value):
        if not value:
            self._ui.second_video_cb.setVisible(False)
        self._ui.second_video_cb.setText(value)
    make_second_video_text = property(__get_make_second_video_text,
                                      __set_make_second_video_text)

    # @property
    def __get_publish_path_label(self):
        return self._ui.publish_path_label.text()
        
    # @publish_path_label.setter
    def __set_publish_path_label(self, value):
        self._ui.publish_path_label.setText(value)
    publish_path_label = property(__get_publish_path_label, __set_publish_path_label)

    # @property
    def __get_file_name_label(self):
        return self._ui.file_name_label.text()
        
    # @file_name_label.setter
    def __set_file_name_label(self, value):
        self._ui.file_name_label.setText(value)
    file_name_label = property(__get_file_name_label, __set_file_name_label)

    # @make_second_video.setter
    def __set_make_second_video(self, value):
        self._ui.second_video_cb.setChecked(value)
    make_second_video = property(__get_make_second_video, __set_make_second_video)

    # @property
    def __get_version(self):
        return self._ui.version_spb.value()

    # @version.setter
    def __set_version(self, value):
        self._ui.version_spb.setValue()
    version_spb = property(__get_version, __set_version)

    def set_version_range(self, max_value):
        self._ui.version_spb.setRange(0, max_value)

    def secondaryVisible(self, value):
        self._ui.label_6.setVisible(value)
        self._ui.line.setVisible(value)
        self._ui.first_video_cb.setVisible(value)
        self._ui.second_video_cb.setVisible(value)

    def _on_publish(self):
        self.publish.emit()
        
    def _on_cancel(self):
        self.cancel.emit()

    def _safe_to_string(self, value):
        """
        safely convert the value to a string - handles
        QtCore.QString if usign PyQt
        """
        #
        if isinstance(value, basestring):
            # it's a string anyway so just return
            return value
        
        if hasattr(QtCore, "QString"):
            # running PyQt!
            if isinstance(value, QtCore.QString):
                # QtCore.QString inherits from str but supports 
                # unicode, go figure!  Lets play safe and return
                # a utf-8 string
                return str(value.toUtf8())
        
        # For everything else, just return as string
        return str(value)