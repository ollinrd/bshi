
class layout_cam():

    def __init__(self):
        pass

    def groupCams(self, *agrs):
        import maya.cmds as cmds
        import sgtk
        import sys
        # group name fields
        engine= sgtk.platform.current_engine()
        Longproject = engine.context.project['name']
        projectSpace = Longproject.split(':')[0]
        project = projectSpace.strip()
        shot = engine.context.entity['name']
        # List  Cameras
        sceanecams= cmds.listCameras(p=1)
        selectedNodes=cmds.ls(sl=1)

        if selectedNodes:
            layoutCameras_grp= cmds.group(em= 1,n='{0}_{1}_layout_cams'.format(project,shot))
            print selectedNodes
            for node in selectedNodes:
                if node in sceanecams:
                    cmds.parent(node,layoutCameras_grp)
        else:
            cmds.confirmDialog(t='Layout Camera Fails', m='Seleccione la camara a publicar')

