from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import QtWidgets
import maya.OpenMayaUI as OMUI
import pymel.core as pm 
import shiboken2
import os 
import sys
from  ollin_tools_maya import ollin_tools_maya

class setOverscan():

	def __init__(self, parent=None):
		#Init class
		self.core_process= ollin_tools_maya()


	def initUI(self,parent=None):
		encoding = sys.getfilesystemencoding()
		self.loc= os.path.dirname(unicode(__file__, encoding))
		resources=self.loc + "/setOverscan_resources"
		self.uiFileName = resources + '/setOverscan.ui'
		self._init_maya_UI(self.uiFileName, parent)
		self._config_UI_()
		self.show()


	def _init_maya_UI(self, uiFileName, parent=None):
		from pymel import versions
		if versions.current() < 201700:
			from PySide import QtUiTools
			self.loader = QtUiTools.QUiLoader()
		else:
			from PySide2 import QtUiTools
			self.loader = QtUiTools.QUiLoader()

		uifile = QtCore.QFile(self.uiFileName)
		uifile.open(QtCore.QFile.ReadOnly)

		#Maya main window
		self.mayaWin = OMUI.MQtUtil.mainWindow() 
		self.mayaWin = shiboken2.wrapInstance(long(self.mayaWin), QtWidgets.QWidget)
		self.ui = self.loader.load(uifile, self.mayaWin)
		uifile.close()
		
	def _config_UI_(self):
		self.sg_mode= 0
		self.ui.setOverscan_button.clicked.connect(self.setOverscan)
		self.ui.sg_rb.toggled.connect(self.sgMode)
		self.ui.prompt_rb.toggled.connect(self.promptMode)


	def setOverscan(self):
		overscanPercent= self.ui.overscan_txtBox.text()
		if self.sg_mode == 1:
			self.core_process.setOverscan(self.sg_mode, None)
		else:
			self.core_process.setOverscan(self.sg_mode, overscanPercent)


	def sgMode(self):
		self.ui.overscan_txtBox.setDisabled(True)
		self.sg_mode= 1
	

	def promptMode(self):
		print"promptMode"
		self.ui.overscan_txtBox.setDisabled(False)
		self.sg_mode= 0
		



	def show(self):
		self.ui.show()
