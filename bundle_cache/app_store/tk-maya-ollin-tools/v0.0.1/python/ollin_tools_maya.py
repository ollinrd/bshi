import traceback
import sgtk
import os
import pymel.core as pm
import maya.cmds as cmds

 
class ollin_tools_maya():
    def shotgun_sets(self, app):
        all_n = cmds.ls()
        flag = False
        for i in all_n:
            if str(i) == 'set_sgPublish':
                flag = True
                break
        if flag is False:
            selected = cmds.ls(sl=True)
            if selected:
                cmds.sets(n='set_sgPublish')
            else:
                cmds.confirmDialog(title='ERROR', message='Nothing is selected!!!!!')
        else:
            cmds.confirmDialog(title='ERROR', message='The set "set_sgPublish" already exists!!!!!')

    #[Character Set]
    def character_sets(self,app):
        all_n=cmds.ls()
        flag=False
        for i in all_n:
            if str(i) == 'set_sgCharacter':
                flag=True
                break

        if flag is False:
            selected=cmds.ls(sl=True)
            if selected:
                cmds.sets(n='set_sgCharacter')
            else:
                cmds.confirmDialog(title='ERROR', message='Nothing is selected!!!!!')
        else:
            cmds.confirmDialog(title='ERROR', message='The set "set_sgPublish" already exists!!!!!')
            

    def resetOverscan(*Args):
        # Find shot name and resolution through shotgun
        try:
            toolkit = sgtk.platform.current_engine()
            sg_shot_raw = toolkit.context.entity
            print 'Project: ', toolkit.context.project['name']
            sgProject = sgShot = toolkit.shotgun.find_one('Project',[['name', 'is', toolkit.context.project['name']]], ['code'])

            sgShot = toolkit.shotgun.find_one(
                "Shot",
                [
                    ["id", "is", sg_shot_raw["id"]]
                ],
                [
                    "code", "sg_resx", "sg_resy", "sg_cg_overscan",
                ],
            )

        except:
            cmds.error(
                "Shotgun probably doesn't have a 'Shot' context set. Make sure you've created a"
                " '+ New File' through the shogun File Open window for the shot that you want to work on"
                ", or have an existing shot scene open.")

        # Find first camera that matches correct camera name, and if none set empty string
        shotCam = cmds.ls('*{0}_'.format(sgProject['code']) + sgShot['code'] + '_cam', r=True)
        overscanPercent = 'off'
        if shotCam:
            shotCam = shotCam[0]
        else:
            shotCam = ""
        sgResWidth = float(sgShot['sg_resx']) ##sgResWidth = float(sgShot['sg_resx'])
        sgResHeight = float(sgShot['sg_resy'])  ##sgResHeight = float(sgShot['sg_resy'])

        if (
            (int(round(sgResWidth / 2)) == cmds.getAttr("defaultResolution.width")) and
            (int(round(sgResHeight / 2)) == cmds.getAttr("defaultResolution.height"))
        ):
            cmds.warning(
                "It looks like you're working at half resolution."
                "Will calculate overscan based off this.")
            sgResWidth = cmds.getAttr('defaultResolution.width')
            sgResHeight = cmds.getAttr('defaultResolution.height')
            
            # Check whether shot cam exists

        if not cmds.objExists(shotCam):
            cmds.error(
                "Can't find shot camera!!!"
                " Check that camera is in scene and that it follows naming conventions")

        # Check for 'off' value as overscanPercent, and if so, turn off overscan (return to normal)

        if overscanPercent == 'off':
            print "TURNING OFF OVERSCAN (RESETTING RESOLUTION AND CAMERA POSTSCALE)..."
            # Reset resolution to original (set in shotgun)
            cmds.setAttr("defaultResolution.width", sgResWidth)
            cmds.setAttr("defaultResolution.height", sgResHeight)
            cmds.setAttr("defaultResolution.deviceAspectRatio", (float(sgResWidth) / float(sgResHeight)))
            # Reset shotCam post scale to 1.0
            cmds.setAttr(shotCam + '.postScale', 1.0)
            print "...done"
            return
        else: 
            cmds.warning('No encontramos la camara ')

    def setOverscan(self, mode, percent):
        # Find shot name and resolution through shotgun
        try:
            toolkit = sgtk.platform.current_engine()
            sg_shot_raw = toolkit.context.entity
            sg = toolkit.shotgun
            filts = [['name', 'is', toolkit.context.project['name']]]
            sgProject = sgShot = sg.find_one('Project', filts, ['code'])
            sgShot = toolkit.shotgun.find_one(
                "Shot",
                [
                    ["id", "is", sg_shot_raw["id"]]
                ],
                [
                    "code", "sg_resx", "sg_resy", "sg_cg_overscan",
                ],
            )

        except:
            cmds.error(
                "Shotgun probably doesn't have a 'Shot' context set. Make sure you've created a"
                " '+ New File' through the shogun File Open window for the shot that you want to work on"
                ", or have an existing shot scene open.")

        # Find first camera that matches correct camera name, and if none set empty string
        shotCam = cmds.ls('*{0}_'.format(sgProject['code']) + sgShot['code'] + '_cam', r=True)
        if shotCam:
            shotCam = shotCam[0]
        else:
            shotCam = ""
        sgResWidth = float(sgShot['sg_resx']) ##sgResWidth = float(sgShot['sg_resx'])
        sgResHeight = float(sgShot['sg_resy'])  ##sgResHeight = float(sgShot['sg_resy'])
        if sgShot['sg_cg_overscan']:
            overscanPercent = float(sgShot['sg_cg_overscan'])
        else:
            overscanPercent = 'off' 
            cmds.warning(
                "There are no Overscan set in Shotgun"
                )

        if (
            (int(round(sgResWidth / 2)) == cmds.getAttr("defaultResolution.width")) and
            (int(round(sgResHeight / 2)) == cmds.getAttr("defaultResolution.height"))
        ):
            cmds.warning(
                "It looks like you're working at half resolution."
                "Will calculate overscan based off this.")
            sgResWidth = cmds.getAttr('defaultResolution.width')
            sgResHeight = cmds.getAttr('defaultResolution.height')

        if mode == 0:
            overscanPercent= float(percent)
            

        # Check whether shot cam exists

        if not cmds.objExists(shotCam):
            cmds.error(
                "Can't find shot camera!!!"
                " Check that camera is in scene and that it follows naming conventions")

        # Check for 'off' value as overscanPercent, and if so, turn off overscan (return to normal)

        if overscanPercent == 'off':
            print "TURNING OFF OVERSCAN (RESETTING RESOLUTION AND CAMERA POSTSCALE)..."
            # Reset resolution to original (set in shotgun)
            cmds.setAttr("defaultResolution.width", sgResWidth)
            cmds.setAttr("defaultResolution.height", sgResHeight)
            cmds.setAttr("defaultResolution.deviceAspectRatio", (float(sgResWidth) / float(sgResHeight)))
            # Reset shotCam post scale to 1.0
            cmds.setAttr(shotCam + '.postScale', 1.0)
            print "...done"
            return

        # Work out the scale and reciprocal from the 'overscanPercent' argument value

        overscanScale = 1.0 + (overscanPercent / 100.0)
        overscanReciprocal = 1 / overscanScale

        # Get resolution and calculate new resolution with added percent of pixels

        currResWidth = cmds.getAttr('defaultResolution.width')
        currResHeight = cmds.getAttr('defaultResolution.height')
        newResWidth = sgResWidth * overscanScale
        newResHeight = sgResHeight * overscanScale

        # Give a warning if scene resolution before running script doesn't match sg resoltion

        if (currResWidth != sgResWidth) or (currResHeight != sgResHeight):
            cmds.warning(
                "Scene resolution before running setOverscan didn't match resolution in shotgun."
                " Scene resolution will be ignored"
                " and overscan will be calculated from values given in shotgun.")

        # Give a warning if shot camera post scale before running script isn't 1.0

        if cmds.getAttr(shotCam + '.postScale') != 1.0:
            cmds.warning(
                "The shot camera's post scale before running setOverscan wasn't set to 1.0."
                " Overscan will be calculated from assumed original post scale of 1.0.")

        # Give a warning if the new resolution with overscan isn't a whole number
        # (We can't render half pixels)
        if (not newResWidth % 1 == 0) or (not newResHeight % 1 == 0):
            cmds.warning(
                "This percent value of overscan gives fraction pixel values."
                " Will round to nearest whole pixel.")
            newResWidth = int(round(newResWidth))
            newResHeight = int(round(newResHeight))

        # If number supplied, start applying overscan:

        print "SETTING OVERSCAN OF %s%% extra..." % (overscanPercent)

        # Change the post scale on the shot cam

        cmds.setAttr(shotCam + '.postScale', overscanReciprocal)

        # Change the resolution to new values with extra pixels (and preserve pixel aspect of 1.0)

        cmds.setAttr("defaultResolution.width", newResWidth)
        cmds.setAttr("defaultResolution.height", newResHeight)
        cmds.setAttr("defaultResolution.deviceAspectRatio", (float(newResWidth) / float(newResHeight)))

        print "Overscan Done!"

    def config_renderSettings(*args):
        toolkit = sgtk.platform.current_engine()
        context = toolkit.context
        sg = toolkit.shotgun

        fields = []
        e_type = context.entity['type']
        if e_type == 'Asset':
            fields = []
        elif e_type == 'Shot':
            fields = ['sg_tail_out', 'sg_head_in', 'sg_resy', 'sg_resx']

        e_id = context.entity['id']
        #print('Entity type:', e_type)
        #print('Entity id:', e_id)
        #print('fields:', fields)
        sg_entity = sg.find_one(e_type, [['id', 'is', e_id]], fields)

        # Variables and plugins validation
        print 'Config render settings'
        arnold_loaded = False
        try:
            if not cmds.pluginInfo('mtoa', q=True, loaded=True):
                cmds.loadPlugin('mtoa')
            arnold_loaded = True
        except:
            pass

        ##########################################################
        ####################      Common      ####################
        ##########################################################
        #### [Fie output] ####
        # Clear name prefix
        at_ = 'defaultRenderGlobals.imageFilePrefix'
        cmds.setAttr(at_, '', type='string')

        if arnold_loaded:
            # Current renderer
            at_ = "defaultRenderGlobals.currentRenderer"
            cmds.setAttr(at_, l=False)

            at_ = "defaultRenderGlobals.currentRenderer"
            cmds.setAttr(at_, "arnold", type="string")

            # Image format
            at_ = 'defaultArnoldDriver.ai_translator'
            cmds.setAttr(at_, 'exr', type='string')

            # Compression zips
            at_ = 'defaultArnoldDriver.exrCompression'
            cmds.setAttr(at_, 2)

            # Half presicion
            at_ = 'defaultArnoldDriver.halfPrecision'
            cmds.setAttr(at_, 1)

            # Autocrop
            val_ = toolkit.context.entity['type'] == 'Shot'
            at_ = 'defaultArnoldDriver.autocrop'
            cmds.setAttr(at_, val_)

        #### [Metadata] ####
        # Frame/animation
        cmds.setAttr('defaultRenderGlobals.outFormatControl', 0)
        cmds.setAttr('defaultRenderGlobals.animation', 1)
        cmds.setAttr('defaultRenderGlobals.putFrameBeforeExt', 1)

        # Frame padding
        cmds.setAttr('defaultRenderGlobals.extensionPadding', 4)

        if context.step['name'].lower() == "matchmove":
            return

        #### [Frame Range] #####
        start_f = 1001
        end_f = 1096
        if toolkit.context.entity['type'] == 'Shot':
            start_f_tmp = sg_entity['sg_head_in']
            if start_f_tmp is not None:
                start_f = start_f_tmp
            end_f_tmp = sg_entity['sg_tail_out']
            if end_f_tmp is not None:
                end_f = end_f_tmp

        # Start frame
        cmds.setAttr('defaultRenderGlobals.startFrame', start_f)
        # End frame
        cmds.setAttr('defaultRenderGlobals.endFrame', end_f)

        #### [Image Size] ####
        width_ = 1920
        height_ = 1080
        if toolkit.context.entity['type'] == 'Shot':
            # Width
            width_tmp = sg_entity['sg_resx']
            if width_tmp is not None:
                width_ = width_tmp
            # Height
            height_tmp = sg_entity['sg_resy']
            if height_tmp is not None:
                height_ = height_tmp

        cmds.setAttr('defaultResolution.width', width_)
        cmds.setAttr('defaultResolution.height', height_)

        ### [Motion blur] ###
        entityT= context.entity["type"]

        if entityT.lower()== "shot":
            filter_= [["id", "is", e_id]]
            fields= ["sg_shutter_angle"]
            shot_info= sg.find_one(entityT, filter_, fields)

            if shot_info["sg_shutter_angle"]:
                angle= float(shot_info["sg_shutter_angle"])/360.00
                pm.setAttr("defaultArnoldRenderOptions.motion_blur_enable",1)
                pm.setAttr("defaultArnoldRenderOptions.motion_frames",angle)

        ### [Abort on Lisence Fail] ###
        pm.setAttr("defaultArnoldRenderOptions.abortOnLicenseFail",1)

        ##########################################################
        ##################   Arnold Renderer    ##################
        ##########################################################

        #### [Textures] ####
        if arnold_loaded:
            # Auto conver Textures to TX
            at_ = 'defaultArnoldRenderOptions.autotx'
            cmds.setAttr(at_, 0)

            # Use existing TX Textures
            at_ = 'defaultArnoldRenderOptions.use_existing_tiled_textures'
            cmds.setAttr(at_, 0)
