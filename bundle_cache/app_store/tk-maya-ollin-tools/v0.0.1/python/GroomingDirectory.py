import maya.cmds as cmds
import os
import sys
import subprocess
import sgtk
from sgtk.util.filesystem import ensure_folder_exists


class OpenDir():

	def Open(self,app,parent= None):
		currentFile=cmds.file(exn=1,q=1)
		mayaFolder= os.path.dirname(os.path.dirname(currentFile))
		engine= sgtk.platform.current_engine()
		step= engine.context.step['name']
		

		Dirs=mayaFolder.split('/')
		DummyPath= ''
		for d in Dirs:
			if d == Dirs[8]:
				DummyPath += 'fx'+'/'
			else:
				DummyPath+=d+'/'
			

		path=(DummyPath +'/ASS_Files')
		ensure_folder_exists(path)


		os.system("xdg-open " + "{0}".format(path))
		sys.stdout.write(path)
