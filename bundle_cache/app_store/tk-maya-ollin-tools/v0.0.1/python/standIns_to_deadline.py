from sgtk.util.filesystem import ensure_folder_exists
import os
import sys
import time
import shutil
import subprocess
from PySide2 import QtCore
#Update

class createStandIns():

    def __init__(self):
        
        self.loc= os.path.dirname(os.path.abspath(__file__))
        self.uiFileName= self.loc + '/StandIns_Resources/AssFromSelection.ui'
        self.renderSettings= True

    def _init_maya_ui(self, uiFileName, parent= None):
        from pymel import versions
        if versions.current() < 201700:
            from Pyside import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        uifile = QtCore.QFile(self.uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)

        self.ui = self.loader.load(uifile, parent)
        uifile.close()


    def showUi(self,app, parent= None):
        self._init_maya_ui(self.uiFileName, parent)
        self.loadUI()
        self.show()

    def loadUI(self):
        
        self.ui.startTime_edit.setDisabled(True)
        self.ui.EndTime_edit.setDisabled(True)
        self.ui.rs_radial.toggled.connect(self._FrameRange_RenderSettings)

        #Propmpt
        self.ui.promp_radial.toggled.connect(self._FrameRange_Prompt)

        #Connect process
        self.ui.process_Button.clicked.connect(self.processAss)

        #Connect cancel
        self.ui.cancel_Button.clicked.connect(self.cancel)


    def _FrameRange_RenderSettings(self):
        ui = self.ui
        ui.startTime_edit.setDisabled(True)
        ui.EndTime_edit.setDisabled(True)
        self.renderSettings= True
       

    def _FrameRange_Prompt(self):
        ui = self.ui
        ui.startTime_edit.setDisabled(False)
        ui.EndTime_edit.setDisabled(False)
        self.renderSettings= False


    def show(self):
        self.ui.show()


    def cancel(self):
        self.ui.hide()


    def processAss(self):
        import maya.cmds as cmds
        ui = self.ui
        if self.renderSettings == True:
            _start= int(cmds.getAttr('defaultRenderGlobals.startFrame'))
            _end= int(cmds.getAttr('defaultRenderGlobals.endFrame'))
        else:
            _start= int(self.ui.startTime_edit.text())
            _end= int(self.ui.EndTime_edit.text())

        self.Execute(_start, _end)



    def Execute(self,start_frame, end_frame ):
       
        copy_maya = self._CreateCopy()
        command= self.CreateKickass(copy_maya, start_frame, end_frame)
        out = self._send_job(command)
        print 'OUT job: ', out
        job_id = out.split("JobID=")[-1].split("The job")[0].strip()

        # Clear tmp file created for deadline ass files generation.
        self._clear_maya_temp(job_id, copy_maya)

    def _getSelection(self):
        import maya.cmds as cmds
        import maya.mel as mel

        selection=cmds.ls(sl=1)

        print selection

        melCmd= 'select -r '

        for sel in selection:
            melCmd+=  sel + ' '
        melCmd+= ';'
            
        print 'Mel Selection command', melCmd

        return melCmd


    def CreateKickass(self, maya_copy, start_frame, end_frame):
        import maya.cmds as cmds
        import maya.mel as mel
        import sgtk

        #Get Frames
        start_frame = start_frame
        end_frame = end_frame
        #by_frame = int(cmds.getAttr('defaultRenderGlobals.byFrame'))

        #Get Scene path
        original_file_path = os.path.abspath(cmds.file(query=True, sn=True))
        file_path = os.path.abspath(cmds.file(query=True, sn=True))
        scene_folder = os.path.dirname(file_path)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])

        #Get Version
        filePath= os.path.basename(cmds.file(exn=1, q=1))
        version= (filePath.split('_')[-1]).split('.')[0]

        #Set the Ass storage folder
        scene_f_base= os.path.dirname( scene_folder)
        ass_folder = os.path.join(scene_f_base, "ASS_Grooming_Files", version)


        print 'ASS FOLDER: ', ass_folder

        script_name = os.path.splitext(os.path.basename(file_path))[0]

        #Ensure folders exist
        engine=sgtk.platform.current_engine()
        ensure_folder_exists(ass_folder)

        #Multiple Layers
        multipleLayer= False
        currentRL= 'masterLayer'

        #Current layer name selected
        sCurrentRL= cmds.editRenderLayerGlobals(q=1, currentRenderLayer=True)

        if len (cmds.ls(type= 'renderlayer')) >= 2:
            multipleLayer = True
            if sCurrentRL != 'defaultRenderLayer':
                currentRL = str(sCurrentRL.replace('rs_', ''))

        if multipleLayer:
            ass_folder = ass_folder + '/{0}'.format(currentRL)

        #Create ass path
        ass_file_path= os.path.join(ass_folder,scene_name + ".ass")

        pool= self._define_pool()

        e_type = 'shot'

        engine=sgtk.platform.current_engine()
        if engine.context.entity['type'] == 'Asset':
            e_type = 'asset'

        melSelection= self._getSelection()
        ornatrixCmd= "OxSetIsRendering(true);"

        #deadline Command
        f_dir = os.path.dirname(file_path)
        flags = ' "-proj {0} -batch -file {1}'.format(f_dir, maya_copy)
        flags += " -command  '{0} {1} arnoldExportAss".format(melSelection, ornatrixCmd) 
        flags += " -f <QUOTE>{0}<QUOTE>".format(ass_file_path)
        flags += " -s"
        flags += " -bb"
        flags += " -startFrame <STARTFRAME%4>"
        flags += " -endFrame <ENDFRAME%4>"
        flags += "'"
        flags += '"'

        maya_path = '/usr/autodesk/maya2018/bin/maya'
        if '2019' in cmds.about(version=True):
            maya_path = '/usr/autodesk/maya2019/bin/maya'

        chunkN = int((end_frame-start_frame)/10)
        if chunkN < 2:
            chunkN = 2
        if chunkN > 10:
            chunkN = 10

        batch_cmd = " -SubmitCommandLineJob "
        batch_cmd += " -executable {0} ".format(maya_path)
        batch_cmd += " -arguments {0}".format(flags)
        batch_cmd += ' -frames {0}-{1}'.format(start_frame, end_frame)
        batch_cmd += ' -pool {0} '.format(pool)
        batch_cmd += ' -chunksize {0}'.format(chunkN)
        batch_cmd += ' -name "{0}"'.format('{0} ASS render'.format(e_type))
        batch_cmd += ' -prop ConcurrentTasks=1'
        batch_cmd += ' -prop BatchName={0}'.format(script_name)
        batch_cmd += ' -prop OutputDirectory0={0}'.format(ass_folder)

        batch_cmd += self._construct_environment_varibles()

        print 'ASS batch_cmd: ', batch_cmd
        return batch_cmd

        #Mandar a Deadline


    def _define_pool(self):
        matchmove_steps = [4, 176]
        light_steps= [7, 185, 28, 39, 78]
        comp_steps= [8, 174]
        import sgtk
        engine=sgtk.platform.current_engine()

        if engine.context.step["id"] in matchmove_steps:
            return "mm"
        elif engine.context.step["id"] in light_steps:
            return "light"
        elif engine.context.step["id"] in comp_steps:
            return "comp"
        else:
            return "cg"

    def _construct_environment_varibles(self):
        import maya.cmds as cmds

        evariables = " "
        arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa3202/"

        alShaders = "/ollin/ovfxToolkitSG/Resources/plugins/autodesk/"
        alShaders += "alShaders-linux-1.0.0rc19-ai4.2.12.2"
        maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'

        if '2019' in cmds.about(version=True):
            arnoldPath = '/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa401'
            maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules2019'

        evariables += " -prop EnvironmentKeyValue0=ARNOLD_PLUGIN_PATH={0}".format(arnoldPath)
        evariables += " -prop EnvironmentKeyValue1=MTOA_TEMPLATES_PATH={0}/ae".format(alShaders)
        evariables += " -prop EnvironmentKeyValue2=MAYA_CUSTOM_TEMPLATE_PATH={0}/aexml".format(alShaders)
        evariables += " -prop EnvironmentKeyValue3=MAYA_MODULE_PATH={0}".format(maya_modules)
        evariables += " -prop EnvironmentKeyValue4=MAYA_DISABLE_CIP=1"

        return evariables


    def _send_job(self, command):
        deadline = '/opt/Thinkbox/Deadline10/bin/deadlinecommand'
        ass= command
        py_cmd = ass

        #os.system(deadline + " " + py_cmd)


       
        process = subprocess.Popen(deadline + " " + py_cmd, shell=True, stdout=subprocess.PIPE)

        out, err = process.communicate()
        print 'Out: ', out
        print 'Err: ', err

        return out
        



    def _CreateCopy(self):

        import maya.cmds as cmds
        import maya.mel as mel
        from pymel import versions
        import pymel.core as pm

        folder = self._get_maya_imageFolder()

        multipleLayers = multipleAOVs = createPrefix = False
        RenLayer = RenderPass = RenderPassF = RenLayerF = ""
        filePrefix = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')

        # Render layers
        multiLayer= False
        currentRL= 'masterLayer'

        #Current layer name selected
        sCurrentRL= cmds.editRenderLayerGlobals(q=1, currentRenderLayer=True)

        if len (cmds.ls(type= 'renderlayer')) >= 2:
            multiLayer = True
            if sCurrentRL != 'defaultRenderLayer':
                currentRL = str(sCurrentRL.replace('rs_', ''))


        multipleLayers= multiLayer
        layerName = currentRL

        print 'multipleLayers: ', multipleLayers

        # Select if multiple aovs for render
        multipleAOVs = self.render_multiaovs()
        print 'multipleAOVs: ', multipleAOVs

        if multipleLayers:
            RenLayer = "<RenderLayer>/"
            RenLayerF = "_<RenderLayer>"
            # Enable multiple render layers
            if mel.eval("optionVar -q renderViewRenderAllLayers;") == 0:
                mel.eval("switchRenderAllLayers;")
        else:
            RenLayer = "masterLayer/"

        if multipleAOVs:
            RenderPass = "_<RenderPass>"
            RenderPassF = "<RenderPass>/"
        else:
            RenderPass = "_beauty"
            RenLayerF = "_masterLayer"

        fileBase = folder.split('/')[0]
        print 'fileBase: ', fileBase

        f_name = str(cmds.file(q=True, sn=True))
        outputname = os.path.splitext(os.path.basename(f_name))[0]
        newPrefix = "{4}/{0}{1}{5}{3}{2}".format(RenLayer, RenderPassF,
                                                 RenderPass, RenLayerF,
                                                 fileBase, outputname)
        cmds.setAttr('defaultRenderGlobals.imageFilePrefix',
                     newPrefix, type="string")

        # set The global pading sequence
        cmds.setAttr("defaultRenderGlobals.extensionPadding", 4)
        # set tiled
        cmds.setAttr("defaultArnoldDriver.exrTiled", 0)
        #
        cmds.setAttr("defaultArnoldDriver.preserveLayerName", 0)
        #
        cmds.setAttr("defaultArnoldDriver.append", 0)
        #
        cmds.setAttr("defaultArnoldDriver.mergeAOVs", 0)

        try:
            cmds.file(save=True)
        except:
            print "Access denied, you can not save  your changes"

        from datetime import datetime

        now_str = datetime.now()
        now_str2 = str(now_str).split('.')[0].replace(' ', '_')
        f_parts = f_name.split(".")
        f_name_copy = f_parts[0] + '_' + now_str2 + "." + f_parts[-1]

        base_name = os.path.basename(f_name_copy)

        f_name_copy = f_name_copy.replace(base_name, "." + base_name) #Crea Archivo Oculto
        if os.access(f_name_copy, os.F_OK):
            os.remove(f_name_copy)

        shutil.copy(f_name, f_name_copy)
        print ">>>>>>>>>>>>>>>>", f_name_copy
        return f_name_copy

    def _get_maya_imageFolder(self):
        import maya.cmds as cmds
        import maya.mel as mel

        scene_info = cmds.file(query=True, sn=True)
        scene_info = scene_info.split("/")[-1]
        scene_info = scene_info.split(".")[0]
        folder = scene_info.split("_")
        if len(folder) > 3:
            folder = folder[-3] + "_" + folder[-2] + "_" + folder[-1] + "/"
        else:
            folder = ""

        return folder

    def _clear_maya_temp(self, job_id, maya_path):
        import maya.cmds as cmds
        file_path = cmds.file(query=True, sn=True)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])
        deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"

        python_file = maya_path + "_tmp.py"

        code = ["import os"]
        code.append("\nif os.access('{0}', os.F_OK):".format(maya_path))
        code.append("\n    os.remove('{0}')".format(maya_path))
        code.append("\nif os.access('{0}', os.F_OK):".format(python_file))
        code.append("\n    os.remove('{0}')".format(python_file))

        with open(python_file, 'w') as f:
            for r in code:
                f.write(str(r))

        clear_cmd = " -SubmitCommandLineJob -executable"
        clear_cmd += " python -arguments {0}".format(python_file)
        clear_cmd += ' -pool cg'
        clear_cmd += ' -name "Cleaning cache file"'
        clear_cmd += ' -prop BatchName={0}'.format(scene_name)
        clear_cmd += ' -prop ExtraInfo0={0}'.format(maya_path)
        clear_cmd += ' -prop JobDependencies={0}'.format(job_id)
        clear_cmd += ' -prop ConcurrentTasks=1'
        clear_cmd += " -prop JobDependencyPercentage=100"
        clear_cmd += " -prop OutputDirectory0=/home"

        #subprocess.Popen(deadline + " " + clear_cmd, shell=True)
        #publish_errors = []
        print "Sending clean cache"
        #out = self._send_job(clear_cmd, publish_errors)
        out = self._send_job(clear_cmd)
        print out

    def render_multiaovs(self):
        import mtoa.aovs as aovs
        import maya.mel as mel

        #mel.eval('unifiedRenderGlobalsWindow;')

        AOVsList = aovs.AOVInterface().getAOVNodes(names=True)
        if len(AOVsList) >= 1:
            return True

        #mds.deleteUI("unifiedRenderGlobalsWindow")

        return False










