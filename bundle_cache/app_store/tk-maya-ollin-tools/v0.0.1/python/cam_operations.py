from .ShotMask import zshotmask_ui
from  ollin_tools_maya import ollin_tools_maya
import maya.cmds as cmds
import sgtk
import sys

class cam_operations():
    

    def __init__(self, step= None):
        self.core_process= ollin_tools_maya()
        self.win= 'window'
        self.step= step



    def TextField(self, *args):
        tarea= cmds.textField('tarea_tf', q=1, tx=1)
        if tarea == '':
            cmds.warning('Porfavor indica en que etapa de animacion te encunetras')
            tarea= 'Etapa de Animacion'
        else:
            sys.stdout.write('tu tarea es {} '.format(tarea))
            cmds.deleteUI(self.win)
            cmds.spaceLocator(n='{0}_pipLoc'.format(tarea))
            self.load_cam_mask()
            cmds.delete('{0}_pipLoc'.format(tarea))



    def createUI(self,app):
        engine= sgtk.platform.current_engine()
        user= engine.context.user['name']

        if self.step== None:
             step= engine.context.step['name']

            
        else:
             step= self.step

        print '##########',step  
      
        
        if (cmds.window(self.win,exists=True)):
            cmds.deleteUI(self.win)

        cmds.window(self.win,rtf=1, w=350, h=100, t= 'SG Camera burnins', s=1)
        cmds.columnLayout (adj=1)
        cmds.text('{0},'. format(user) + ' En que etapa de ' + ' {0}'.format(step) + ' te encunetras?')
        cmds.textField('tarea_tf',tx=' ')
        cmds.button (l='load', c=self.TextField) 
        #Launch UI
        cmds.showWindow(self.win)

    
   
    def load_cam_mask(self):
        reload(zshotmask_ui)

        ZShotMask = zshotmask_ui.ZShotMask()
        ZShotMaskUi = zshotmask_ui.ZShotMaskUi()
        ZShotMaskUi.display()
        ZShotMask.create_mask()


        import maya.cmds as cmds
        shotmask= cmds.ls(typ= "zshotmask")[0]

        cmds.setAttr("{0}.borderAlpha".format(shotmask), 0)

        #Set Overscan form shotgun Data
        sg_mode =1
        self.core_process.setOverscan(sg_mode, None)
