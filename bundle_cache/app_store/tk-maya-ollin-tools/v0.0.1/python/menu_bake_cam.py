import traceback
import sgtk
import os
import subprocess
import time
import threading
import maya.cmds as cmds


class ollin_tools_maya():
    def unlockTransforms(self):
        cameras = cmds.ls(cameras = 1)
        transforms = cmds.listRelatives(cameras, parent=1)
        newCamera = transforms[1]
        cmds.setAttr(newCamera + '.tx', lock=0)
        cmds.setAttr(newCamera + '.ty', lock=0)
        cmds.setAttr(newCamera + '.tz', lock=0)
        cmds.setAttr(newCamera + '.rx', lock=0)
        cmds.setAttr(newCamera + '.ry', lock=0)
        cmds.setAttr(newCamera + '.rz', lock=0)
        cmds.setAttr(newCamera + '.sx', lock=0)
        cmds.setAttr(newCamera + '.sy', lock=0)
        cmds.setAttr(newCamera + '.sz', lock=0)

    def errorMessage(self, sgShot, Project):
        ## Defines an error window if the camera name is not properly set
        windowError = 'myWindowID'
        errorUI = 'errorWindowUI'
        if cmds.window(windowError, exists=True):
            cmds.deleteUI(windowError)
        # Creates an error window
        cmds.window(windowError, title='ERROR', sizeable=False, resizeToFitChildren=True, widthHeight=(300, 75))
        cmds.columnLayout(adjustableColumn=True)
        cmds.separator(h=10, style='none')
        cmds.text(label='BAD CAMERA NAME')
        cmds.separator(h=1, style='none')
        cmds.text(label='example:')
        cmds.separator(h=10, style='none')
        cmds.text(label='{0}_'.format(Project) + sgShot['code'])
        cmds.separator(h=10, style='none')
        # If the window exists: deletes the old window
        def cancelCallback(self, *pArgs):
            if cmds.window(windowError, exists=True):
                cmds.deleteUI(windowError)

        cmds.button(label='OK', command=cancelCallback, align='center')
        cmds.showWindow()

    def cameraBakerUI(self,app):
    # Get some shotgun info 
        try:
            tk_maya = sgtk.platform.current_engine()
            sg_shot_raw = tk_maya.context.entity
            sgShot = tk_maya.shotgun.find_one("Shot",[["id", "is", sg_shot_raw["id"]]],["code", "sg_resx", "sg_resy", "sg_head_in", "sg_tail_out", 'project'])
            proj = tk_maya.shotgun.find_one('Project',[['id', 'is', sgShot['project']['id']]],['code'])
            Project = proj['code']
            print Project

        except:
            cmds.error(
            "Shotgun can't find some of the necessary information to set up this shot. You probably don't\n"
            "have a 'Shot' context set. Make sure you've created a '+ New File' through the shogun File Open\n"
            "window for the **shot** that you want to work on, or have an existing shot scene open.\n")
            
        # Find first camera that matches correct camera name, and if none set empty string
        shotCam = cmds.ls('*{0}_'.format(Project) + sgShot['code'] + '_camShape', r=True)
        cameraSets = [c for c in cmds.ls(cameras=True) if not cmds.camera(c, q=True, startupCamera=True)]
        cameraChecker ='[u\''+'{0}_'.format(Project) + sgShot['code']+'Shape'+'\']'
        cameraEdit = str(cameraSets)

        if ('{0}_'.format(Project) + sgShot['code']) in cameraEdit: 
            # Checks if there is only one camera on the scene 
            if len(cameraSets) == 1:
                
                for i in cameraSets: 
                    # Creates a duplicate with the name convention fron SG
                    cameraDuplicate = cmds.duplicate(i, name='*{0}_'.format(Project) + sgShot['code']+ '_cam')
                    self.unlockTransforms()  
                # Sets all the childrens of the main Camera
                children = cmds.listRelatives(cameraDuplicate, allDescendents=True, noIntermediate=True)
                childrenMeshes = cmds.ls(children, type='mesh') 
                childrenGroup = cmds.ls(childrenMeshes, '*polyCam*')
                # Deletes the polys
                cmds.delete(childrenGroup)
                cmds.parent(cameraDuplicate[0], world=True)
                cameraCreada = cameraDuplicate[0]
                cameraOriginal = cmds.listRelatives(cameraSets, parent=True)
                cameraOriTrans = cameraOriginal[0]
                # Sets the rotation of the camera
                cmds.setAttr(cameraCreada + ".ro", 0)
                cmds.pointConstraint(cameraOriTrans, cameraCreada)
                cmds.orientConstraint(cameraOriTrans, cameraCreada)
                    
                # Get resolution entries from shotgun
                # sgResWidth = float(sgShot['sg_resx'])
                # sgResHeight = float(sgShot['sg_resy'])
                
                # set shot frame range through shotgun
                sgFrameSt = sgShot["sg_head_in"]
                sgFrameEnd = sgShot["sg_tail_out"]
                
                # sets the start and end frame for the scene
                cmds.playbackOptions(minTime=sgFrameSt)
                cmds.playbackOptions(animationStartTime=sgFrameSt)
                cmds.playbackOptions(animationEndTime=sgFrameSt)
                cmds.playbackOptions(maxTime=sgFrameEnd)
                # Sets the far and near planes for the Camera 
                cmds.viewClipPlane(cameraCreada, ncp=10)
                cmds.viewClipPlane(cameraCreada, fcp=30000)
                # Sets the Scale to 1 of the Camera
                cmds.setAttr(cameraCreada + ".sx", 1)
                cmds.setAttr(cameraCreada + ".sy", 1)
                cmds.setAttr(cameraCreada + ".sz", 1)
                # Copy the transformations fron the original Camera to the new one
                cmds.bakeResults(cameraCreada, cameraSets, time=(sgFrameSt-1, sgFrameEnd+1),at=["rx","ry","rz","tx","ty","tz"], simulation=True )
                # Erase all the created constrains
                cmds.delete(cameraCreada, constraints=True)
                # Creates the Camera Group and asigns the propper name to it
                cmds.group(cameraCreada, name=cameraCreada + '_grp')
                shapeNewCamera = (str(cameraCreada) + 'Shape')
                # Sets the locator scale to 100
                cmds.setAttr(shapeNewCamera + ".locatorScale", 100)
            elif len(cameraSets) >= 2:
                cmds.warning('You have more than 2 cameras')
            
        else:
            self.errorMessage(sgShot, Project)

            #### This is the function name #### 
            #### cameraBakerUI()

    def shotgun_sets(self, app):
        selection = cmds.ls(sl = True)
        newSet1 = cmds.sets(n='set_sgPublish'