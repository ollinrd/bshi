from tank.platform import Application
import sgtk
import os
import pymel.core as pm
import maya.cmds as cmds
 

class Menu_ollin(Application):
    def init_app(self):
        # get modules
        module_camera_bake = self.import_module("maya_camera_bake")
        camera_bake_functions = module_camera_bake.camera_bake()
        ollin_tools_maya = self.import_module("ollin_tools_maya")
        ollin_tools_maya_functions = ollin_tools_maya.ollin_tools_maya()
        ollin_overscan= self.import_module("setOverscan")
        ollin_Overscan_functions = ollin_overscan.setOverscan()
        mod_cam = self.import_module("cam_operations")
        ollin_cam_ops = mod_cam.cam_operations()
        layout_cam=self.import_module("layout_camera")
        ollin_layout_cams=layout_cam.layout_cam()

        StandInns= self.import_module("standIns_to_deadline")
        ollin_standinns_to_deadline = StandInns.createStandIns()

        groomDir= self.import_module("GroomingDirectory")
        ollin_grooming_dir = groomDir.OpenDir()

        if self.engine.context.step is not None:
            tipo = self.engine.context.entity['type']
            step= self.engine.context.step['name']
        else:
            tipo= ' '
            step= ' '

        # set Menus
        MainMayaWindow = pm.language.melGlobals['gMainWindow']
        for i in sorted(pm.lsUI(menus=True)):
            if 'Ollin' in str(i):
                #pass
                cmds.deleteUI(str(i))

        customMenu = pm.menu('Ollin tools', parent=MainMayaWindow)

        if str(tipo) == 'Shot':
            pm.menuItem(label="Validate camera and Bake",
                        command=camera_bake_functions.cameraBakerUI,
                        parent=customMenu)

            pm.menuItem(label="Reset the overscan",
                        command=ollin_tools_maya_functions.resetOverscan,
                        parent=customMenu)

            pm.menuItem(label="Set overscan",
                        command=ollin_Overscan_functions.initUI,
                        parent=customMenu)

            pm.menuItem(label="Create Character Sg set",
                    command=ollin_tools_maya_functions.character_sets,
                    parent=customMenu)


            if str(step) == "layout":
                pm.menuItem(label="SG Layout Camera",
                            command=ollin_layout_cams.groupCams,
                            parent=customMenu)



        else:
            "Here what you want in Assets"

        pm.menuItem(label="Render settings SG",
                    command=ollin_tools_maya_functions.config_renderSettings,
                    parent=customMenu)

        # Tool for both entity types
        pm.menuItem(label="Create Sg set",
                    command=ollin_tools_maya_functions.shotgun_sets,
                    parent=customMenu)

        pm.menuItem(label="SG Camera burnins",
                    command=ollin_cam_ops.createUI,
                    parent=customMenu)

        # Tool for create StandInn from Selectiom (deadline)
        selectionSteps= ['character fx','fx', 'lookdev', 'light', 'set dressing']

        if str(step) in selectionSteps:
            pm.menuItem(label= "Create Grooming ASS Files from Selection ",
                        command= ollin_standinns_to_deadline.showUi,
                        parent= customMenu)

            pm.menuItem(label= "Open Grooming ASS Files Directory ",
                        command= ollin_grooming_dir.Open,
                        parent= customMenu)




