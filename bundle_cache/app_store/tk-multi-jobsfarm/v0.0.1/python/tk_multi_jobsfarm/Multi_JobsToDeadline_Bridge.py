#!/usr/bin/env pythonS
# -⁻- coding: UTF-8 -*-
import codecs
import sys
from sys import platform
import unicodedata
from sgtk.platform.qt import QtCore, QtGui
import os


class FormMain():
    def __init__(
        self, loc, engine_name, context, core, deadline, sg, engine,
        parent=None
    ):
        # self.btn_accept.clicked.connect(self.general_settings)
        uiFileName = loc + "/resources/jobsfarm_MainUI.ui"
        self.engine_name = engine_name
        self.deadline = deadline
        self.core = core
        self.sg = sg
        self.context = context
        self.engine = engine

        if engine_name == 'tk-maya':
            self._init_maya_ui(uiFileName=uiFileName)
            self.activeRenderNuke(False)
            self.activeAlembic(True)
            self._config_UI_Maya()

        elif engine_name == 'tk-nuke':
            self._init_nuke_ui(uiFileName=uiFileName)
            self.activeRenderNuke(True)
            self.activeAlembic(False)
            self._config_UI_Nuke()

        elif engine_name == 'tk-houdini':
            self._init_houdini_ui(uiFileName=uiFileName)
            self.activeRenderNuke(False)
            self.activeAlembic(True)
            self._config_UI_Houdini()

    # Methods to load UI from file !!  ---------------------------------------
    def _init_maya_ui(self, uiFileName, parent=None):
        from pymel import versions
        if versions.current() < 201700:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        uifile = QtCore.QFile(uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)
        self.ui = self.loader.load(uifile, parent)
        uifile.close()

        #self.ui.btn_accept.clicked.connect(self.general_settings)
        #self.ui.btn_refresh.clicked.connect(self.update_selection)

    def _init_nuke_ui(self, uiFileName, parent=None):
        import nuke
        cV = nuke.NUKE_VERSION_STRING
        nukeV = cV.split("v")[0]
        if float(nukeV) < 11:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        # Main window
        uifile = QtCore.QFile(uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)
        self.ui = self.loader.load(uifile, parent)
        uifile.close()

    def _init_houdini_ui(self, uiFileName, parent=None):
        # init houdini ui
        print('Houdini engine')

    # #######################################
    #                UI Methods             # --------------------------------
    # #######################################

    # Personalize UI at startup METHODS !!!-----------------------------------
    def activeRenderNuke(self, active):
        print('>>> Active Render Nuke: ', active)
        if active:
            #self.ui.Ren_frame_3D.hide()
            self.ui.Ren_frame_3D.setHidden(True)
        else:
            #self.ui.Ren_frame_Nuke.hide()
            self.ui.Ren_frame_Nuke.setHidden(True)

    def activeAlembic(self, active):
        if not active:
            self.ui.master_tabWidget.setTabEnabled(1, False)

    def _config_UI_Maya(self):

        print('UI context maya')
        #print self.context.task
        #print self.context.step

        c_step = self.context.step['name'].lower()
        start, end = self.core.getPlaybackFrames()
        # For initial active tab
        self.ui.master_tabWidget.setCurrentIndex(0)

        debug_test = True

        # #
        #   Alembic Area! --------------------------------------------------
        # #
        alembic_steps = ['rnd', 'fx', 'animation', 'matchmove', 'layout']
        if c_step in alembic_steps or debug_test:
            self.ui.master_tabWidget.setCurrentIndex(1)
            self.populate_Maya_UI_Alembic(self.ui, start, end, c_step)
            self.connect_UI_to_Maya_Alembic(self.ui)
        else:
            self.ui.master_tabWidget.setTabEnabled(1, False)

        # #
        #   Render Area! ----------------------------------------------------
        # #
        render_steps = ['rnd', 'fx', 'model', 'animation', 'matchmove', 'layout']
        if c_step in render_steps or debug_test:
            self.populate_Maya_UI_Render(self.ui, start, end)
            # Missing connect into render buttons~~~~
        else:
            self.ui.master_tabWidget.setTabEnabled(0, False)

        # #
        #   Playblast Area! -------------------------------------------------
        # #
        playblast_steps = ['fx', 'animation', 'rnd', 'matchmove', 'layout']
        if c_step in playblast_steps or debug_test:
            self.ui.master_tabWidget.setCurrentIndex(2)
            self.populate_Maya_UI_Playblast(self.ui, start, end)
            self.connect_UI_to_Maya_Playblast(self.ui)
            self.validate_current_playblast_session()
        else:
            self.ui.master_tabWidget.setTabEnabled(2, False)

    def _config_UI_Nuke(self):
        print('UI context nuke')
        # nuke UI config

    def _config_UI_Houdini(self):
        print('UI context houdini')
        # houdini UI config

    # #### Maya UI Specific #### --------------------------------------------

    def populate_Maya_UI_Alembic(self, ui, start, end, current_step):
        # Config Alembic Maya UI ------
        if current_step == 'fx':
            ui.T1_cmb_abcUse.setCurrentIndex(2)
        elif current_step == 'model':
            ui.T1_cmb_abcUse.setCurrentIndex(1)
        elif current_step == 'animation':
            ui.T1_cmb_abcUse.setCurrentIndex(0)

        ui.T1_lnEdit_First.setText(str(start))
        ui.T1_lnEdit_Last.setText(str(end))



        # Config abc path
        tag= None
        abc_path = self.core.getABC_path(tag)
        self.ui.abc_lnEdit_outputPath.setText(abc_path)

    def populate_Maya_UI_Render(self, ui, start, end):
        ui.T0_mayaRend_First.setText(str(start))
        ui.T0_mayaRend_Last.setText(str(end))
        ui.T0_mayaRend_steps.setText('1')

        chksize = (end-start)/8
        if chksize > 14:
            chksize = 14
        ui.T0_mayaRend_chunkSize.setText(str(chksize))

        ass_path = self.core.getASS_path()
        ui.T0_mayaRend_ASSPath.setText(ass_path)

    def populate_Maya_UI_Playblast(self, ui, start, end):
        """
        """
        e_type = self.context.entity['type']
        if e_type.lower() == 'asset':
            self.ui.plBlast_rBtn_shotInfo.setDisabled(True)
        else:
            self.ui.plBlast_rBtn_shotInfo.setDisabled(False)
        ui.plBlast_lnEdit_startT.setText(str(start))
        ui.plBlast_lnEdit_endT.setText(str(end))

        playblast_path = self.core.getPlayblast_path()
        ui.plBlast_lnEdit_output.setText(playblast_path)

        # Force the star and end frame from shot info
        self.__playblast_update_btnShotInfo()

    # #######################################
    #             Signal Methods            # --------------------------------
    # #######################################

    ################################
    ############ Maya ##############
    ################################

    def connect_UI_to_Maya_Alembic(self, ui):
        #Populate elements and reconnect into alembic section
        ui.btn_abc_submit.clicked.connect(self.Maya_Submit_ABC)
        ui.T1_btn_syncScene.clicked.connect(self.Maya_Sync_dataScene_ABC)
        ui.T1_btn_syncSG.clicked.connect(self.Maya_Sync_SGinfo_ABC)

    def connect_UI_to_Maya_Playblast(self, ui):
        #Populate elements and reconnect into playblast section
        ui.plBlast_btn_process.clicked.connect(self.Maya_Playblast_Process)

        # Quality slider and iput text connections
        ui.plblast_hSlider_quality.valueChanged.connect(
            self.__playblast_update_quality)
        ui.plBlast_lnEdit_quality.textChanged.connect(
            self.__playblast_update_qualitySLI)

        # Scale slider and input text connections
        ui.plblast_hSlider_scale.valueChanged.connect(
            self.__playblast_update_scale)
        ui.plBlast_lnEdit_scale.textChanged.connect(
            self.__playblast_update_scaleSLI)

        # Radial buttons FRAME RANGE
        ui.plBlast_rBtn_TimeSlider.toggled.connect(
            self.__playblast_update_btnTimeSlider)

        ui.plBlast_rBtn_StartEnd.toggled.connect(
            self.__playblast_update_btnStartEnd)

        ui.plBlast_rBtn_shotInfo.toggled.connect(
            self.__playblast_update_btnShotInfo)

        # Version check if is required
        ui.plBlast_chkVersion.stateChanged.connect(
            self.__playblast_update_chkBoxVersion)

        # Display size playblast
        ui.plBlast_cmbBox_displaySize.currentIndexChanged.connect(
            self.__playblast_update_displaySize)

    ################################
    ############ Nuke ##############
    ################################
    # TO-DO

    ################################
    ########### Houdini ############
    ################################
    # TO-DO

    # #######################################
    #             Callback Methods          # --------------------------------
    # #######################################

    ################################
    ############ Maya ##############
    ################################
    def __playblast_update_scale(self):
        # When user moves slider
        ui = self.ui
        new_val = ui.plblast_hSlider_scale.value()

        c_val = int(float(ui.plBlast_lnEdit_scale.text())*100.0)

        if new_val != c_val:
            ui.plBlast_lnEdit_scale.setText(str(float(new_val)/100.0))

    def __playblast_update_scaleSLI(self):
        # When user change text of scales
        ui = self.ui

        try:
            new_val = float(ui.plBlast_lnEdit_scale.text())
            if new_val > 1.0:
                new_val = 1.0
                ui.plBlast_lnEdit_scale.setText('1.0')

            if int(ui.plblast_hSlider_scale.value()) != float(new_val*100):
                ui.plblast_hSlider_scale.setValue(int(new_val*100.0))
        except:
            ui.plBlast_lnEdit_scale.setText('0.0')
            ui.plblast_hSlider_scale.setValue(0)

    def __playblast_update_quality(self):
        # When users move slider
        ui = self.ui

        new_val = ui.plblast_hSlider_quality.value()

        c_val = int(ui.plBlast_lnEdit_quality.text())
        if new_val != c_val:
            ui.plBlast_lnEdit_quality.setText(str(new_val))

    def __playblast_update_qualitySLI(self):
        # When users move quality text value
        ui = self.ui
        if not ui.plBlast_lnEdit_quality.text().isdigit():
            ui.plBlast_lnEdit_quality.setText('0')
            ui.plblast_hSlider_quality.setValue(0)
        else:
            new_val = int(ui.plBlast_lnEdit_quality.text())
            if new_val > 100:
                new_val = 100
                ui.plBlast_lnEdit_quality.setText('100')

            if int(ui.plblast_hSlider_quality.value()) != new_val:
                ui.plblast_hSlider_quality.setValue(new_val)

    def __playblast_update_btnTimeSlider(self):
        ""
        ui = self.ui
        if ui.plBlast_rBtn_TimeSlider.isChecked():
            "Set config from timeslider"

            start, end = self.core.getPlaybackFrames()
            ui.plBlast_lnEdit_startT.setText(str(start))
            ui.plBlast_lnEdit_endT.setText(str(end))
            ui.plBlast_lnEdit_startT.setReadOnly(True)
            ui.plBlast_lnEdit_endT.setReadOnly(True)

    def __playblast_update_btnStartEnd(self):
        ""
        ui = self.ui
        if ui.plBlast_rBtn_StartEnd.isChecked():
            "Set config from timeslider"
            # ui.plBlast_lnEdit_startT.setText(start)
            # ui.plBlast_lnEdit_endT.setText(end)
            ui.plBlast_lnEdit_startT.setReadOnly(False)
            ui.plBlast_lnEdit_endT.setReadOnly(False)

    def __playblast_update_btnShotInfo(self):
        ui = self.ui
        if ui.plBlast_rBtn_shotInfo.isChecked():
            "Set config from timeslider"
            start = 15
            end = 15

            e_id = self.context.entity['id']
            e_type = self.context.entity['type']
            fields = ['sg_tail_out', 'sg_head_in']
            if e_type.lower() == 'shot':
                query = self.sg.find_one(e_type, [['id', 'is', e_id]], fields)
                start = query['sg_head_in']
                end = query['sg_tail_out']

                ui.plBlast_lnEdit_startT.setText(str(start))
                ui.plBlast_lnEdit_endT.setText(str(end))

                ui.plBlast_lnEdit_startT.setReadOnly(True)
                ui.plBlast_lnEdit_endT.setReadOnly(True)

    def __playblast_update_chkBoxVersion(self):
        # Verify the status of check box of version create
        if (self.ui.plBlast_chkVersion.isChecked()):
            self.ui.plBlast_txtEdit_version.setDisabled(False)
        else:
            self.ui.plBlast_txtEdit_version.setDisabled(True)

    def __playblast_update_displaySize(self):
        # verify display size
        import maya.cmds as cmds

        c_text = self.ui.plBlast_cmbBox_displaySize.currentText()

        if 'From Render Settings' in c_text:
            width = cmds.getAttr('defaultResolution.width')
            height = cmds.getAttr('defaultResolution.height')
            self.ui.plBlast_lnEdit_width.setReadOnly(True)
            self.ui.plBlast_lnEdit_height.setReadOnly(True)
            self.ui.plBlast_lnEdit_width.setText(str(width))
            self.ui.plBlast_lnEdit_height.setText(str(height))

        if 'Custom' in c_text:
            self.ui.plBlast_lnEdit_width.setReadOnly(False)
            self.ui.plBlast_lnEdit_height.setReadOnly(False)

    # #######################################
    #              Core Methods             # --------------------------------
    # #######################################

    ################################
    ############ Maya ##############
    ################################

    # StandAlone Methods (local)
    def Maya_Playblast_Process(self):
        print('Processing playblast!!!')

        ui = self.ui
        first_f = str(ui.plBlast_lnEdit_startT.text())
        last_f = str(ui.plBlast_lnEdit_endT.text())

        # Use sequence time ??
        #ui.plBlast_chkBox_seqTime.isChecked()

        # ------------ From view ------------------------------------
        #  playblast visualization
        from_view = ui.plBlast_chkBox_view.isChecked()
        ornaments = ui.plBlast_chkBox_showOrnaments.isChecked()
        format_type = str(ui.plBlast_cmbBox_format.currentText())
        format_type = 'image'

        output_path = ui.plBlast_lnEdit_output.text()

        scale = float(ui.plBlast_lnEdit_scale.text())
        quality = int(ui.plBlast_lnEdit_quality.text())

        width = int(ui.plBlast_lnEdit_width.text())
        height = int(ui.plBlast_lnEdit_height.text())

        #----------- Julio Cesar ----------------------------------------
        #engine = sgtk.platform.current_engine()
        import maya.cmds as cmds
        entity = self.context.entity
        #sg = engine.shotgun

        #Mask 
        zshorMask_ls= cmds.ls(typ= "zshotmask")
        if not zshorMask_ls:
            cmds.confirmDialog(t="Burnins", m="Favor de crear los burnins y volver a intentar ")
            self.core.call_burnins()

            return False

        #Validate that the current version dosnt exist

        fP=cmds.file(query=True, sn=True)
        versionName= os.path.basename(fP).replace(".ma","_playblast")
        version=self.sg.find_one('Version', [["code", "is", str(versionName)]], [])

        if version:
            cmds.confirmDialog(t="Version existente", m="Ya se publico un Playblast de esta version, favor de subir de version y volver a intentar ")
            return False


        if entity["type"].lower()== "shot":
            filter = [["id", "is", entity['id']]]
            fields = ["sg_resx", "sg_resy", "sg_sequence"]
            shot = self.sg.find_one(entity['type'], filter, fields)

            width = shot['sg_resx']
            height = shot['sg_resy']

        else:
            width = cmds.getAttr("defaultResolution.width")
            height = cmds.getAttr("defaultResolution.height")

        compression = 'JPG'
        # ----------------------------------------------------------------

        #  playblast version info!
        comment_str = ui.plBlast_txtEdit_version.toPlainText()
        # print(comment_str)
        import maya.cmds as cmds
        val_cm = cmds.colorManagementPrefs(q=1, cmEnabled=1)
        #cmds.colorManagementPrefs(e=1, cmEnabled=0)

        output_path_modif = output_path.split(".%04d.")[0]
        self.core.getPlayblast_Command(
            start=first_f, end=last_f, format_type=format_type,
            file_path=output_path_modif, size_percent=int(scale*100.0),
            from_view=from_view, showOrnaments=ornaments, quality=quality,
            compression=compression, width=width, height=height)

        #cmds.colorManagementPrefs(e=1, cmEnabled=val_cm)

        # Get ID of publish
        if (self.ui.plBlast_chkVersion.isChecked()):
            proj = self.context.project

            import maya.cmds as cmds
            filepath = cmds.file(q=True, sn=True)
            filename = os.path.basename(filepath)
            v_name = filename.split('.')[0] + '_playblast'
            user_id = self.engine.context.user['id']

            # print (self.engine.get_env())
            # print self.engine.environment
            # print self.engine.context

            e_type = self.context.entity['type']
            e_id = self.context.entity['id']
            sg_task = self.context.task

            video_path = self.core.getPlayblast_path_video()

            data = {
                'project': {'type': 'Project', 'id': proj['id']},
                'code': v_name,
                'sg_status_list': 'rev',
                'entity': {'type': e_type, 'id': e_id},
                #'user': {'type': 'HumanUser', 'id': user_id},
                'sg_path_to_frames': output_path,
                'sg_path_to_movie': video_path,
                'sg_task': sg_task,
                'sg_version_type': 'Internal Review',
                'sg_client_task_name': 'ANIM',
                'sg_movie_has_slate': False,
                'description': comment_str}

            if e_type.lower() == 'shot':
                s_filt = [
                    ['id', 'is', e_id],
                    ['project', 'is', proj]]
                s_field = [
                    'sg_head_in', 'sg_tail_out',
                    'sg_frame_rate']
                SHOT = self.sg.find_one("Shot", s_filt, s_field)

                if SHOT is not None:
                    first_frame = int(SHOT['sg_head_in'])
                    data.update({'sg_first_frame': first_frame})


                    last_frame = int(SHOT['sg_tail_out'])
                    data.update({'sg_last_frame': last_frame})

                    data.update({'frame_range': '{0}-{1}'.format(first_frame,last_frame)})

                    fr_count = last_frame - first_frame
                    data.update({'frame_count': fr_count})

                    if SHOT['sg_frame_rate'] is not None:
                        rate = float(SHOT['sg_frame_rate'])
                        data.update({'sg_uploaded_movie_frame_rate': rate})
 
                    data.update({'sg_movie_has_slate': False})

            filters_ver = [['project', 'is', proj], ['code', 'is', v_name]]
            fields_ver = ['code', 'id']
            ver_ = self.sg.find_one("Version", filters_ver, fields_ver)

            if ver_ is not None:
                data.pop('sg_status_list', None)
                #data.pop('user', None)
                self.sg.update('Version', ver_['id'], data)
            else:
                ver_ = self.sg.create('Version', data)
            user_login = "batch"
            try:
                user_login = self.engine.context.user['login']
            except:
                pass
            #self.sg.upload(
            #    "Version", ver_["id"],
            #    output_path, "sg_uploaded_movie")

            self.deadline._seq_to_video(
                job_name=v_name+'_video_creation',
                user=user_login,
                seq_start=first_f,
                seq_path=output_path,
                video_path=video_path,
                version_id=ver_['id'],
                batch_name=v_name)
            """
            self.deadline._upload_video_to_Version(
                job_name=v_name+'_video_upload',
                user=user_login,
                video_path=video_path,
                version_info=ver_)
            """

        self.core.show_ok_dialog(msg='Process complete!',
                                 title='Playblast status')

    def validate_current_playblast_session(self):
        print(">>>>>>>>>>>>>> Playblast validation process")
        ui = self.ui  # Short access to UI
        output_path = ui.plBlast_lnEdit_output.text()
        print(output_path)

    # Sending job to Deadline
    def Maya_Submit_ABC(self):
        ui = self.ui  # Short access to UI

        ro = ui.T2_chkBx_RendereableOnly.isChecked()
        wcs = ui.T2_chkBx_WriteColorSets.isChecked()
        wfs = ui.T2_chkBx_WriteFaceSets.isChecked()
        ws = ui.T2_chkBx_WorldSpace.isChecked()
        wv = ui.T2_chkBx_WriteVisibility.isChecked()
        fer = ui.T2_chkBx_FilterEulerRotations.isChecked()
        wuvs = ui.T2_chkBx_WriteUVSets.isChecked()

        # Get text of numbers for frames
        first_f = str(ui.T1_lnEdit_First.text())
        last_f = str(ui.T1_lnEdit_Last.text())

        #Get additional fields text
        tag=str(ui.T1_lnEdit_TagAbc.text())
        step= ui.T1_lnEdit_Steps.text()

        abc_path = self.core.getABC_path(tag)
        self.ui.abc_lnEdit_outputPath.setText(abc_path)


        valid_range = True
        try:
            # Validate frame numbers
            first_f = float(first_f)
            last_f = float(last_f)
        except:
            valid_range = False

        abc_type = str(ui.T1_cmb_abcUse.currentText())
        job_name = abc_type + ' ...Alembic caching... ' + tag
        sel = ui.T1_chkBx_Selection.isChecked()
        scene_path = self.core.get_filePath()
        scene_name = os.path.basename(scene_path).split('.')[0]
        maya_location = self.core.getCurrentMayaLocation()

        if scene_path == '':
            ttl = 'Missing info'
            feed_ = 'First save your scene!'
            self.core.show_ok_dialog(feed_, ttl)
            return False

        if not valid_range:
            ttl = 'Missing info'
            feed_ = 'Fix your frame range!'
            self.core.show_ok_dialog(feed_, ttl)
            return False

        #output_abc = self.core.getABC_path()
        output_abc = str(ui.abc_lnEdit_outputPath.text())
        self.create_path(output_abc)

        maya_ver = self.core.getMayaVersion()
        self.deadline._override_maya_evars(maya_ver, True)

        abc_cmd = self.core.getABC_Command(
            rendereableOnly=ro, colorSets=wcs, faceSets=wfs, worldSpace=ws,
            visibility=wv, filterEuler=fer, uvSets=wuvs, start=first_f,
            end=last_f, selection=sel, file_path=output_abc, abc_steps=step)

        self.deadline._maya_send_alembic_job(
            scene_path=scene_path, abc_cmd=abc_cmd, job_name=job_name,
            pool='cg', output_abc=output_abc, task_name=scene_name,
            maya_location=maya_location
            )
        msg = 'Job sent! \n\n-- Details in script editor --'
        title = 'Job Status'
        self.ui.hide()
        self.core.show_ok_dialog(msg=msg, title=title)

    def Maya_Sync_dataScene_ABC(self):
        # Set info from scene as initial state does
        self._config_context_maya()

    def Maya_Sync_SGinfo_ABC(self):
        self._config_context_maya()
        ui = self.ui
        start, end = self.core.getSGFrames()
        ui.T1_lnEdit_First.setText(str(start))
        ui.T1_lnEdit_Last.setText(str(end))

    ################################
    ############ Nuke ##############
    ################################
    # TO-DO

    ################################
    ########### Houdini ############
    ################################
    # TO-DO

    ################################
    ########### General ############
    ################################

    def create_path(self, target_path):
        folder_ = os.path.dirname(target_path)
        if not os.path.exists(folder_):
            os.makedirs(folder_)

    def show(self):
        #self.ui.setWindowFlags(
        #    self.ui.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        self.ui.setWindowFlags(self.ui.windowFlags())
        self.ui.show()
