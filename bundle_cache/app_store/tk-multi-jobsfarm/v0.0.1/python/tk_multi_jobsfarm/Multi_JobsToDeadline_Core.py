

class Engine_Ctrl():
    def __init__(self, engine_name, sg, context, engine):
        """
        Define the default actions to starts
        """
        if engine_name == 'tk-maya':
            self._init_maya_core(sg, context, engine)
        elif engine_name == 'tk-nuke':
            self._init_nuke_core(sg, context, engine)
            print('nuke core')
        elif engine_name == 'tk-houdini':
            self._init_houdini_core(sg, context, engine)
            print('hodini core')
        from .python_engines.Deadline_Operations import Deadline_Ctrl

        self.deadline_process = Deadline_Ctrl()

    def _init_maya_core(self, sg, context, engine):
        from .python_engines.Maya_Operations import Maya_Ctrl
        print('Imported module succesfull')
        self.core_process = Maya_Ctrl(sg, context, engine)

    def _init_nuke_core(self, sg, context, engine):
        from .python_engines.Nuke_Operations import Nuke_Ctrl
        print('Imported module succesfull')
        self.core_process = Nuke_Ctrl(sg, context, engine)

    def _init_houdini_core(self, sg, context, engine):
        from .python_engines.Houdini_Operations import Houdini_Ctrl
        print('Imported module succesfull')
        self.core_process = Houdini_Ctrl(sg, context, engine)
