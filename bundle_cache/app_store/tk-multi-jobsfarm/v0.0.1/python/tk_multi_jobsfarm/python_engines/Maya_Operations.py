import maya.cmds as cmds


class Maya_Ctrl():
    def __init__(self, sg, context, engine):
        """
        Define the default actions to starts
        """
        self.sg = sg
        self.context = context
        self.engine = engine

    def getPlaybackFrames(self):
        start = cmds.playbackOptions(query=True, min=True)
        end = cmds.playbackOptions(query=True, max=True)
        return start, end

    def getMayaVersion(self):
        _ver = cmds.about(c=True)[:4]
        return int(_ver)

    def getSGFrames(self):
        etype = self.context.entity['type'].lower()
        if etype == 'asset':
            ttl = 'Warning SG info'
            feed_ = 'Asset enity does not have start or end info\n'
            feed_ += 'start-enf info got from scene info.'
            self.show_ok_dialog(feed_, ttl)
            return self.getPlaybackFrames()
        else:
            filter_ = ['id', 'is', self.context.entity['id']]
            fields_ = ['sg_head_in', 'sg_tail_out']
            info = self.sg.find_one('Shot', filter_, fields_)
            start = info['sg_head_in']
            end = info['sg_tail_out']

            feed_ = ' Warning!! Missing info: \n\n'
            if not start:
                feed_ += 'Start [Head in]\n'
            if not end:
                feed_ += 'End [Tail out]'
            if not start or not end:
                ttl = 'Missing SG info'
                self.show_ok_dialog(feed_, ttl)
                return self.getPlaybackFrames()
            else:
                return start, end

    def get_filePath(self):
        file_path = cmds.file(query=True, sn=True)
        return file_path

    def getABC_path(self, tag):
        current_path = self.get_filePath()
        if current_path == '':
            return 'Save scene first!!'

        etype = self.context.entity['type'].lower()

        # Get field from current scene
        work_name = 'maya_{0}_work'.format(etype)
        work_template = self.engine.get_template_by_name(work_name)

        fields = work_template.get_fields(current_path)

        

        # Apply fields
        temp_name = '{0}_work_alembic_cache'.format(etype)
        target_template = self.engine.get_template_by_name(temp_name)
        path_str = target_template.apply_fields(fields)
        #Tag
        if tag:
            ext='.'+ path_str.split(".")[-1]
            tag= tag
            tagPlus= '_'+tag+ext
            path_str= path_str.replace(ext, tagPlus)

        return path_str

    def getABC_Command(
        self, rendereableOnly, colorSets, faceSets, worldSpace, visibility,
        filterEuler, uvSets, start, end, selection, file_path, abc_steps
    ):
        #Aditional OPs
        steps= self.getAlembicSteps(abc_steps)
       
        cmd_s = " 'currentTime {0};".format(start)
        cmd_s += ' AbcExport -j <QUOTE> -frameRange {0} {1}'.format(start, end)
        # Default flags
        if steps != 1.0:
            cmd_s += ' -step {0}'.format(steps)

        cmd_s += ' -uvWrite -stripNamespaces'

        if rendereableOnly:
            cmd_s += ' -ro'
        if colorSets:
            cmd_s += ' -writeColorSets'
        if faceSets:
            cmd_s += ' -writeFaceSets'
        if worldSpace:
            cmd_s += ' -worldSpace'
        if visibility:
            cmd_s += ' -writeVisibility'
        if filterEuler:
            cmd_s += ' -eulerFilter'
        if uvSets:
            cmd_s += ' -writeUVSets'

        cmd_s += ' -dataFormat ogawa'

        if selection:
            for el in cmds.ls(sl=1, l=1):
                cmd_s += ' -root {0}'.format(el)

        cmd_s += ' -file {0}<QUOTE>;'.format(file_path)
        cmd_s += "'"

        print cmd_s

        return cmd_s

    def getAlembicSteps(self,steps):
        abc_step= steps

        if abc_step is None:
            return 1
        else:
            return 1.0/float(abc_step)


    def getASS_path(self):
        current_path = self.get_filePath()
        if current_path == '':
            return 'Save scene first!!'

        etype = self.context.entity['type'].lower()

        # Get field from current scene
        work_name = 'maya_{0}_work'.format(etype)
        work_template = self.engine.get_template_by_name(work_name)

        fields = work_template.get_fields(current_path)

        # Apply fields
        temp_name = 'maya_work_{0}_ass'.format(etype)
        target_template = self.engine.get_template_by_name(temp_name)
        path_str = target_template.apply_fields(fields)
        return path_str

    def getCurrentMayaLocation(self):
        from pymel import versions
        maya_version = str(versions.current())[:4]
        
        maya_path = '/usr/autodesk/maya{0}/bin/maya'.format(maya_version)

        return maya_path

    def getPlayblast_path(self):
        current_path = self.get_filePath()
        if current_path == '':
            return 'Save scene first!!'

        etype = self.context.entity['type'].lower()

        # Get field from current scene
        work_name = 'maya_{0}_work'.format(etype)
        work_template = self.engine.get_template_by_name(work_name)

        fields = work_template.get_fields(current_path)

        # Apply fields
        temp_name = 'maya_work_{0}_playblast_jpg'.format(etype)
        target_template = self.engine.get_template_by_name(temp_name)
        path_str = target_template.apply_fields(fields)
        return path_str

    def getPlayblast_path_video(self):
        current_path = self.get_filePath()
        if current_path == '':
            return 'Save scene first!!'

        etype = self.context.entity['type'].lower()

        # Get field from current scene
        work_name = 'maya_{0}_work'.format(etype)
        work_template = self.engine.get_template_by_name(work_name)

        fields = work_template.get_fields(current_path)

        # Apply fields
        temp_name = 'maya_work_{0}_playblast_mov'.format(etype)
        target_template = self.engine.get_template_by_name(temp_name)
        path_str = target_template.apply_fields(fields)
        return path_str

    def getPlayblast_Command(
        self, start, end, format_type, file_path, size_percent,
        from_view, showOrnaments, quality, compression, width, height
    ):
        cmd_s = " 'currentTime {0};".format(start)
        cmd_s += ' '

        cmds.playblast(
            startTime=start, endTime=end, v=0,
            format=format_type, f=file_path, percent=size_percent,
            viewer=from_view, showOrnaments=True,
            quality=quality, compression=compression, fo=1,
            clearCache=1, widthHeight=[width, height])

    def show_ok_dialog(self, msg, title):
        btns = ['Ok']
        cmds.confirmDialog(title=title, message=msg, button=btns,
                           defaultButton='Ok', dismissString='Ok')

    def call_burnins(self):
        import pymel.util
        import sgtk
        plug_path = '/nfs/ovfxToolkit/Resources/plugins/autodesk/zshotMask'
        if not plug_path in pymel.util.getEnv("MAYA_PLUG_IN_PATH"):
            pymel.util.putEnv("MAYA_PLUG_IN_PATH", [pymel.util.getEnv("MAYA_PLUG_IN_PATH"), plug_path])

        ollinTools = sgtk.platform.current_engine().apps.get("tk-maya-ollin-tools", False)
        mod_cam = ollinTools.import_module("cam_operations")
        ollin_cam_ops = mod_cam.cam_operations("step")
        cb = lambda : ollin_cam_ops.createUI("cam_operations")
        cb()

