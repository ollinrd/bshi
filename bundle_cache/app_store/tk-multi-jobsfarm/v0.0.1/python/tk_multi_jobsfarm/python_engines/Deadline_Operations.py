import subprocess
import os


class Deadline_Ctrl():
    def __init__(self):
        # Software locations! ------------------------------------------------
        self.deadline_location = '/opt/Thinkbox/Deadline10/bin/deadlinecommand'        
        self.maya_location = '/usr/autodesk/maya2018/bin/maya'

        # Maya values! -------------------------------------------------------
        #    Elements paths
        arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa3202/"
        alShaders = "/ollin/ovfxToolkitSG/Resources/plugins/autodesk/"
        alShaders += "alShaders-linux-1.0.0rc19-ai4.2.12.2"

        maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'

        maya_evars = {'ARNOLD_PLUGIN_PATH': arnoldPath,
                      'MTOA_TEMPLATES_PATH': alShaders + '/ae',
                      'MAYA_CUSTOM_TEMPLATE_PATH': alShaders + '/aexml',
                      'MAYA_MODULE_PATH': maya_modules,
                      'MAYA_DISABLE_CIP': '1',
                      }
        self.maya_evariables = self._dict_to_evar(maya_evars)

        # Nuke values! -------------------------------------------------------
        nuke_evars = {}
        self.nuke_evariables = self._dict_to_evar(nuke_evars)

        # Houdini values! ----------------------------------------------------
        houdini_evars = {}
        self.houdini_evariables = self._dict_to_evar(houdini_evars)

    def _override_maya_evars(self, maya_version, ziva_used):
        """
        """
        str_loc = '/usr/autodesk/maya{0}/bin/maya'.format(maya_version)
        self.maya_location = str_loc

        # Maya values! -------------------------------------------------------
        #    Elements paths
        arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa3202/"

        alShaders = "/ollin/ovfxToolkitSG/Resources/plugins/autodesk/"
        alShaders += "alShaders-linux-1.0.0rc19-ai4.2.12.2"

        maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'
        if maya_version != 2018:
            maya_modules += str(maya_version)
            arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/"
            arnoldPAth = "mtoa{0}_3202/".format(maya_version)

        maya_evars = {'ARNOLD_PLUGIN_PATH': arnoldPath,
                      'MTOA_TEMPLATES_PATH': alShaders + '/ae',
                      'MAYA_CUSTOM_TEMPLATE_PATH': alShaders + '/aexml',
                      'MAYA_MODULE_PATH': maya_modules,
                      'MAYA_DISABLE_CIP': '1',
                      'zivadyn_LICENSE': '5053@ovfxlicenses'
                      }

        self.maya_evariables = self._dict_to_evar(maya_evars)

    def _dict_to_evar(self, data_dict):
        evar_format = ''
        index = 0
        for evar in data_dict.keys():
            tmp_ = ' -prop EnvironmentKeyValue{0}={1}'.format(index, evar)
            tmp_ += '={0}'.format(data_dict[evar])
            evar_format += tmp_
            index = index + 1
        return evar_format

    def _send_job(self, deadline_cmd):
        try:
            deadline = self.deadline_location
            process = subprocess.Popen(deadline + " " + deadline_cmd,
                                       shell=True, stdout=subprocess.PIPE)

            out, err = process.communicate()
            print 'Out: ', out
            print 'Err: ', err

        except Exception as e:
            print(e)

    def _maya_send_alembic_job(
        self, scene_path, abc_cmd, job_name, pool, output_abc,
        task_name, maya_location
    ):

        self.maya_location = maya_location
        folder_ = os.path.dirname(scene_path)
        proj_info = '-proj {0} -batch -file "{1}"'.format(folder_, scene_path)
        maya_config = '"{0} -command {1}"'.format(proj_info, abc_cmd)

        outDir = '/' + output_abc.split('/')[-1]
        outDir = output_abc.replace(outDir, '') 

        command_str = ' -SubmitCommandLineJob'
        command_str += ' -executable {0}'.format(self.maya_location)
        command_str += ' -arguments {0}'.format(maya_config)
        command_str += ' -pool "{0}"'.format(pool)
        command_str += ' -name "{0}"'.format(job_name)
        command_str += ' -prop ConcurrentTasks=1'
        command_str += ' -prop BatchName="{0}"'.format(task_name)
        command_str += ' -prop OutputDirectory0={0} '.format(outDir)

        command_str += self.maya_evariables

        self._send_job(command_str)

        return True

    def _seq_to_video(self, job_name, user, seq_start, seq_path,
                      video_path, version_id, batch_name):
        ffmpeg_cmd = '"-y -gamma 2.2'
        ffmpeg_cmd += ' -start_number {0}'.format(seq_start)
        ffmpeg_cmd += ' -i {0}'.format(seq_path)
        ffmpeg_cmd += ' -qscale 10 -vcodec libx264 -pix_fmt yuv420p -f mov'
        ffmpeg_cmd += ' -preset slow -crf 18 -r 25 {0}"'.format(video_path)

        command_str = ' -SubmitCommandLineJob'
        command_str += ' -executable ffmpeg'
        command_str += ' -arguments {0}'.format(ffmpeg_cmd)
        command_str += ' -pool "publish"'
        #command_str += ' -username "{0}"'.format(user)
        command_str += ' -prop Priority=50'
        command_str += ' -name "{0}"'.format(job_name)
        command_str += ' -prop ConcurrentTasks=1'
        command_str += ' -prop BatchName="{0}"'.format(batch_name)
        command_str += ' -prop ExtraInfo0={0}'.format(version_id)
        command_str += ' -prop ExtraInfo1={0}'.format(video_path)

        out_dir = video_path.replace("/"+video_path.split("/")[-1], "")
        command_str += ' -prop OutputDirectory0={0} '.format(out_dir)
        self._send_job(command_str)

    def _upload_video_to_Version(self, job_name, user, video_path, version_info):
        ""
