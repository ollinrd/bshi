import sys
import sgtk
from sgtk import TankError
from sgtk.platform import Application
from sys import platform


class JobsToDeadline(Application):
    def init_app(self):
        self.engine.register_command("Jobs to Deadline", self.mainWindow)
        self.loaded = False

    def mainWindow(self):
        if not self.loaded:
            self.loaded = True
            self.eng_name = self.engine.name
            self.sg = self.engine.shotgun

            # Get functionalities
            self.tk_multi_jobsOps = self.import_module("tk_multi_jobsfarm")

            # Get instance of operations class depending on current
            # engine
            eng_ops = self.tk_multi_jobsOps.Engine_Ctrl(
                engine_name=self.eng_name, sg=self.sg,
                context=self.engine.context,
                engine=self.engine)

            self.operations = eng_ops.core_process
            self.deadline = eng_ops.deadline_process

        # Get bridge instance with UI loades in different engines
        # UI already hidded elements unnecessary according
        # context.
        self.bridge = self.tk_multi_jobsOps.FormMain(
            loc=self.disk_location, engine_name=self.eng_name,
            context=self.engine.context, core=self.operations,
            deadline=self.deadline, sg=self.sg, engine=self.engine)

            # Reload context
        self.bridge.show()
