# This example adds a right-click Menu to the Timeline View for getting the current Shot Selection.
# After running this action, 'hiero.selectedShots' will return the TrackItems selected in the timeline
from hiero.core import *
import hiero.ui

from hiero.ui import mainWindow # Need this to work around OS X 'Start Dictation' Bug
from PySide2 import QtGui
from PySide2 import QtCore
from PySide2 import QtUiTools
from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2 import QtWidgets
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import threading
import sgtk
import shutil
import tempfile
import subprocess
import os
import sys
from sgtk.platform import Application
# from incoming_ui import *
# from valid_ui import *
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml

class Menu_ollin(Application,QAction):
    def init_app(self):
        self.UI_ = self.import_module("tk_hiero_ingest")
        #self.engine = sgtk.platform.current_engine()
        QAction.__init__(self, "Get Selected Shots", None)
        self.triggered.connect(self.getShotSelection)
        hiero.core.events.registerInterest("kShowContextMenu/kTimeline",
                                           self.eventHandler)

    def error_window(self, op, text_):
        if op == 1:
            error = 'Error:\nRevisa que la ruta no contenga espacios'
        if op == 2:
            error = 'Error:\nEstas tratando de ingestar un archivo .mov\n{0}'.format(text_)
        if op == 3:
            error = 'Error:\nNo existe la secuencia:{0}'.format(text_)
        if op == 4:
            error = 'Error:\nRevisa el nombre del track:{0}\nRecuerda:no debe tener espacios ni guiones'.format(text_)
        if op == 5:
            error = '{0}'.format(text_)
        if op == 6:
            error = 'No existe el shot: {0}'.format(text_)
        warning = QMessageBox()
        warning.setText(error)
        warning.setStandardButtons(QMessageBox.Ok)
        sure = warning.exec_()
        return sure

    def shot_fields_error(self, SHT):
        print SHT
        if not None in [SHT['sg_head_in'], SHT['sg_tail_out'], SHT['sg_external_id'], SHT['sg_offset_frame'], SHT['sg_cut_in'], SHT['sg_cut_out'] ]:
          err = 'no error'
        else:
          err = '''Revisa los valores en la base de datos en shotgun no debe haber un "None":\n
                 head in: {0}
                 tail out: {1}
                 external id: {2}
                 offset frame: {3} 
                 cut in: {4}
                 cut out: {5}
                '''.format(SHT['sg_head_in'], SHT['sg_tail_out'],
                           SHT['sg_external_id'], SHT['sg_offset_frame'],
                           SHT['sg_cut_in'], SHT['sg_cut_out'])
        print 'ERRR', err
        return err

    def get_sg_templates(self):
        #self.config_path = engine.context.tank.pipeline_configuration.get_path()
        tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
        self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                 "project_specs.yml")
        with open(p_specs_path, 'r') as stream:
            try:
                self.specs = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        print 'SPECS:', self.specs
        self.plate_template = self.engine.get_template_by_name(self.specs['scan_template'])
        print 
        self.cdl_template = self.engine.get_template_by_name("cdl_shot")
        self.secondary_cdl_template = self.engine.get_template_by_name("secondary_cdl_shot")

    def getShotSelection(self):
        warning = QMessageBox()
        warning.setText('Ingest selected clips?')
        warning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        sure = warning.exec_()
        self.continue_ = False
        if sure == QMessageBox.Ok:
            self.show_ui()

    def continue_after_UI(self):
        #self.config_path = self.engine.context.tank.pipeline_configuration.get_path()
        self.sg = self.engine.shotgun
        self.sg_project = self.sg.find_one('Project',[['id','is',self.engine.context.project['id']]],['code'])
        os.system("USER=$(whoami)")
        self.user = os.environ["USER"]
        selection = self._selection
        self.shot_dict = []
        if len(selection) == 1:
            clip = selection[0]
            path_source = clip.source().mediaSource().fileinfos()[0].filename() # .split(' ')[0]
            self.get_sg_templates()
            contin = self.validations(clip)
            if contin == 'bad':
                return
            seq = clip.parent().parent().name()
            out = self.send_incoming(clip, seq, path_source)
            depend_ = out.split("JobID=")[-1].split("The job")[0].strip()
            print depend_
            if self.specs['make_proxies'] == True:
                self.send_proxies(clip, seq, self.path_plate, depend_)
            if self.incoming_ui.ui.checkBox.isChecked():
                self.send_version_0(clip, seq, self.path_plate, depend_)
            if self.incoming_ui.ui.checkBox_2.isChecked():
                self.send_version_0_client(clip, seq, self.path_plate, depend_)
        else:
            for clip in selection:
                path_source = clip.source().mediaSource().fileinfos()[0].filename()
                contin = self.validations(clip)
                if contin == 'bad':
                    return
                # seq = clip.name().split('_')[0]
                seq = clip.parent().parent().name()
                out = self.send_incoming(clip, seq, path_source)
                depend_ = out.split("JobID=")[-1].split("The job")[0].strip()
                if self.specs['make_proxies'] == True:
                    self.send_proxies(clip, seq, self.path_plate, depend_)
                if self.incoming_ui.ui.checkBox.isChecked():
                    self.send_version_0(clip, seq, self.path_plate, depend_)
                if self.incoming_ui.ui.checkBox_2.isChecked():

                    self.send_version_0_client(clip, seq, self.path_plate, depend_)
        warning = QMessageBox()
        warning.setText('INGEST job now is  in Deadline!')
        warning.setStandardButtons(QMessageBox.Ok)
        warning.exec_()

    # generate paths and Cut
    def send_incoming_job(self, python_script, name, dict_):
        nuke_version = self.specs['main_nuke_path']
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        bndl = self.engine.context.tank.pipeline_configuration.get_path()
        nuke_cmd += ' "-t {0} {1} {2}" '.format(python_script, dict_, bndl)
        nuke_cmd += ' -name "{0}" -pool editorial'.format(name)
        nuke_cmd += ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        deadline = self.specs['deadline_command_path']
        process = subprocess.Popen(deadline + " " + nuke_cmd, 
                             shell=True, stdout=subprocess.PIPE)
        print deadline + " " + nuke_cmd
        out, err = process.communicate()
        print 'PROCESS: ', out, err, process
        print out
        return str(out)

    def send_v0_job(self, python_script, name, dict_, depend_):
        nuke_version = self.specs['main_nuke_path']
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        bndl = self.engine.context.tank.pipeline_configuration.get_path()
        nuke_cmd += ' "-t {0} {1} {2}" '.format(python_script, dict_, bndl)
        nuke_cmd += ' -name "{0}" -pool editorial'.format(name)
        nuke_cmd += ' -prop JobDependencies=' + str(depend_) + ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        deadline = self.specs['deadline_command_path']
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd

    def send_v0_client_job(self, python_script, name, dict_, depend_):
        nuke_version = self.specs['main_nuke_path']
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        bndl = self.engine.context.tank.pipeline_configuration.get_path()
        nuke_cmd += ' "-t {0} {1} {2}" '.format(python_script, dict_, bndl)
        nuke_cmd += ' -name "{0}_client" -pool editorial'.format(name)
        nuke_cmd += ' -prop JobDependencies=' + str(depend_) + ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        deadline = self.specs['deadline_command_path']
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd

    # generate paths and Cut
    def send_proxies_job(self, python_script, name, dict_, depend_):
        nuke_version = self.specs['main_nuke_path']
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        bndl = self.engine.context.tank.pipeline_configuration.get_path()
        nuke_cmd += ' "-t {0} {1} {2}" '.format(python_script, dict_, bndl)
        nuke_cmd += ' -name "{0}" -pool editorial'.format(name)
        nuke_cmd += ' -prop JobDependencies=' + str(depend_) + ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        deadline = self.specs['deadline_command_path']
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd

    def send_incoming(self, clip, seq, path_source):
        self.shot_d = {}
        self.shot_d['is_cdl'] = False
        seq = clip.parent().parent().name()
        sequence = self.sg.find_one('Sequence', [['project', 'is', self.sg_project], ['code', 'is', seq]])
        print 'secuencia:', sequence
        fields = {'Shot': str(clip.name()),
                  'Sequence': str(seq),
                  'version': int(self.plate_version)}
        track = clip.parent()
        fields['width'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[0])
        fields['height'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[1])
        fields['elementName'] = track.name()
        fields['project'] = self.sg_project['code']
        SHOT = self.sg.find_one('Shot', [['project', 'is', self.sg_project], ['code', 'is', str(clip.name())]],['sg_head_in', 'sg_external_id', 'sg_lut'] )
        if SHOT:
            fields['Extension'] = path_source.split('.')[-1]
            print 30*'*'
            print 30*'*'
            print '--RnD--'
            print self.specs 
            print 15*'-'
            print self.specs['scan_template']
            print self.plate_template
            print 30*'*'
            print 30*'*'

            self.path_plate = self.plate_template.apply_fields(fields)
            fields['Extension_cdl'] = self.specs['color_format']
            if SHOT['sg_lut']:
                fields['tiempo'] = 'noche'
            else:
                fields['tiempo'] = 'dia'
            self.path_cdl = self.cdl_template.apply_fields(fields)
            self.secon_path_cdl = self.secondary_cdl_template.apply_fields(fields)
            self.shot_d['clip_name'] = str(clip.name())
            self.shot_d['artist'] = self.user
            self.shot_d['clip_cdl_path'] = self.path_cdl
            self.shot_d['clip_plate_path'] = self.path_plate
            self.shot_d['cut_origin_path'] = path_source
            self.shot_d['shot'] = str(clip.name())
            self.shot_d['seq'] = str(seq)
            self.shot_d['project'] = self.sg_project
            self.shot_d['version'] = self.plate_version
            self.shot_d['is_cdl'] = False
            self.shot_d['WB'] = False
            self.shot_d['mp_name'] = ''
            try:
                self.shot_d['aspect'] = float(clip.source().format().pixelAspect())
            except Exception as e:
                self.shot_d['aspect'] = 1
            
            media = clip.source().mediaSource()
            self.shot_d['cut_first'] = media.startTime()
            print  'FIRST FRAME: ' + str(self.shot_d['cut_first'])
            try:
                for tag in track.tags():
                    if 'MainPlate' in tag.name() or 'CDL' in tag.name():
                        self.shot_d['is_cdl'] = True
                        self.shot_d['mp_name'] = self.sg_project['code'] + '_' + SHOT['sg_external_id'] + '_element_'+ track.name()
                        print tag.name()
                    else:
                        self.shot_d['mp_name'] = ''
                        print 'No tag in track'
                    if 'WB' in tag.name():
                        self.shot_d['WB'] = True
                        print 'White balance >>>>'
                        engine = sgtk.platform.current_engine()
                        WB_template = engine.get_template_by_name("wb_shot")
                        fields = {'Sequence': self.shot_d['seq']}
                        fields['Shot'] = self.shot_d['shot']
                        fields['project'] = self.sg_project['code']
                        path = WB_template.apply_fields(fields)
                        print 'path template WB >>>>', path
                        self.shot_d['WB_Path'] = path
            except:
                pass
            if self.shot_d['is_cdl'] is False:
                self.shot_d['clip_cdl_path'] = self.secon_path_cdl

            try:
                FR = float(clip.source().mediaSource().metadata()['media.input.frame_rate'])
            except:
                FR = self.specs['frame_rate']
            self.shot_d['framerate'] = FR
            try:
                factor_ = SHOT['sg_head_in'] -1000
                print 'FACTOR {0} '.format(factor_)
                t_c = str(clip.source().mediaSource().metadata()['media.input.timecode']).split(':')
                aa = Timecode.HMSFToFrames(TimeBase.fromString(str(self.shot_d['framerate'])),True,int(t_c[0]), int(t_c[1]), int(t_c[2]), int(t_c[3]))
                tup = hiero.core.Timecode.framesToHMSF(aa + factor_ - 2, self.shot_d['framerate'] ,True)
                #tup = hiero.core.Timecode.framesToHMSF(self.shot_d['cut_first'] + factor_ -1, self.shot_d['framerate'] ,True)
                self.shot_d['timecode'] = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
                print 'timecode: ', self.shot_d['timecode']
            except Exception as e:
                print 'NO METADATA FOR TIMECODE'
                tup = hiero.core.Timecode.framesToHMSF(self.shot_d['cut_first'] -1, self.shot_d['framerate'] ,True)
                self.shot_d['timecode'] = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])

            data = {'sg_str_timecode': self.shot_d['timecode'], 'sg_external_in_frame': self.shot_d['cut_first']}
            if self.shot_d['is_cdl']:

                self.sg.update('Shot', SHOT['id'], data)
            prefixx = self.specs['tmp_folder']
            (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "incoming_dict_", suffix='.txt')
            file = open(dictPath, 'w')
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell=True)
            file.write(str(self.shot_d))
            file.close()
            tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
            config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
            incoming_hook = os.path.join(config_path, "hooks", "Hiero",
                                                     "ovfx_incoming_template.py")
            scriptPath = incoming_hook
            out = self.send_incoming_job(scriptPath, 'Incoming_{0}_{1}'.format(str(self.sg_project['code'] ), str(clip.name())),dictPath)
            return out

    def send_version_0(self, clip, seq, path_source ,depend_):
        self.shot_d = {}
        SHOT = self.sg.find_one('Shot',[['project', 'is', self.sg_project],
                                        ['code', 'is', str(clip.name())]],
                                       ['sg_head_in', 'sg_tail_out', 'code'])
        seq = clip.parent().parent().name()
        fields = {'Shot': str(clip.name()),
                  'Sequence': str(seq),
                  'version': int(self.internal_version)}
        track = clip.parent()
        fields['width'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[0])
        fields['height'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[1])
        fields['elementName'] = track.name()
        fields['project'] = self.sg_project['code']
        if SHOT:
            self.shot_d['id'] = SHOT['id']
            self.shot_d['fields'] = fields
            self.shot_d['clip_name'] = str(clip.name())
            self.shot_d['artist'] = self.user
            self.shot_d['seq'] = str(seq)
            self.shot_d['project'] = self.sg_project
            self.shot_d['version'] = self.internal_version
            self.shot_d['clip_plate_path'] = path_source
            self.clip = clip
            self.depend_ = depend_
            self.show_ui_v0(SHOT['sg_head_in'], SHOT['sg_tail_out'],
                            SHOT['code'] + ' ' + clip.parent().name())

    def post_v0_ui(self):
            self.shot_d['last'] = self.lframe
            self.shot_d['first'] = self.fframe
            try:
                FR = float(self.clip.source().mediaSource().metadata()['media.input.frame_rate'])
            except:
                FR = self.specs['frame_rate']
            print 'AAAAAAAAAAAAAAAAAAAAA', self.fframe, self.lframe
            self.shot_d['framerate'] = FR
            prefixx = self.specs['tmp_folder']
            (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "v0_dict_", suffix='.txt')
            file = open(dictPath, 'w')
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell = True)
            file.write(str(self.shot_d))
            file.close()
            scriptPath = self.config_path +'/hooks/Hiero/ovfx_version_zero.py'
            self.send_v0_job(scriptPath, 'Version0_{0}_{1}'.format(str(self.sg_project['code']), str(self.clip.name())),dictPath, self.depend_)

    def send_version_0_client(self, clip, seq, path_source, depend_):
        self.shot_d = {}
        seq = clip.parent().parent().name()
        fields = {'Shot': str(clip.name()),
                  'Sequence': str(seq),
                  'version': int(self.client_version)}
        track = clip.parent()
        fields['width'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[0])
        fields['height'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[1])
        fields['elementName'] = track.name()
        fields['project'] = self.sg_project['code']
        SHOT = self.sg.find_one('Shot',[['project', 'is', self.sg_project],
                                        ['code', 'is', str(clip.name())]],
                                        ['code', 'sg_head_in', 'sg_tail_out'])
        if SHOT:

            self.shot_d['id'] = SHOT['id']
            self.shot_d['fields'] = fields
            self.shot_d['clip_name'] = str(clip.name())
            self.shot_d['artist'] = self.user
            self.shot_d['seq'] = str(seq)
            self.shot_d['project'] = self.sg_project
            self.shot_d['version'] = self.client_version
            self.shot_d['clip_plate_path'] = path_source
            try:
                FR = float(clip.source().mediaSource().metadata()['media.input.frame_rate'])
            except:
                FR = self.specs['frame_rate']
            self.shot_d['framerate'] = FR
            prefixx = self.specs['tmp_folder']
            (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "v0_dict_", suffix='.txt')
            file = open(dictPath, 'w')
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell = True)
            file.write(str(self.shot_d))
            file.close()
            scriptPath = self.config_path + '/hooks/Hiero/ovfx_version_zero_client.py'
            self.send_v0_client_job(scriptPath, 
                                    'Version0_client_' + str(self.sg_project['code']) + '_' + str(clip.name()),
                                    dictPath, depend_)

    def send_proxies(self, clip, seq, path_source,depend_):
        self.shot_d = {}
        seq = clip.parent().parent().name()
        track = clip.parent()

        fields = self.plate_template.get_fields(path_source)
        SHOT = self.sg.find_one('Shot',[['project', 'is', self.sg_project], ['code', 'is', str(clip.name())]])
        if SHOT:
            self.shot_d['id'] = SHOT['id']
            self.shot_d['fields'] = fields
            self.shot_d['clip_name'] = str(clip.name())
            self.shot_d['artist'] = self.user
            self.shot_d['seq'] = str(seq)
            self.shot_d['project'] = self.sg_project
            self.shot_d['clip_plate_path'] = path_source
            try:
                FR = float(clip.source().mediaSource().metadata()['media.input.frame_rate'])
            except:
                if self.sg_project['code'] == 'FTHM':
                    FR = 24.00
                else:
                    FR = self.specs['frame_rate']
            self.shot_d['framerate'] = FR
            prefixx = self.specs['tmp_folder']
            (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "proxies_dict_", suffix='.txt')
            file = open(dictPath, 'w')
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell = True)
            file.write(str(self.shot_d))
            file.close()
            scriptPath = self.config_path + '/hooks/Hiero/ovfx_make_proxies.py'
            self.send_proxies_job(scriptPath,'Proxies_' + str(self.sg_project['code']) + '_' + str(clip.name()),dictPath,depend_)

    def eventHandler(self, event):
        self._selection = None
        if hasattr(event.sender, 'getSelection') and event.sender.getSelection() is not None and len( event.sender.getSelection() ) != 0:
            selection = event.sender.getSelection() # Here, you could also use: hiero.ui.activeView().selection()
            self._selection = [shot for shot in selection if isinstance(shot,hiero.core.TrackItem)] # Filter out just TrackItems
            if len(selection)>0:
                title = "OLLIN Ingest Selected Shot"
                self.setText(title)
                menu = event.menu 
                for i in event.menu.actions():
                    if str(i.text()) == "OLLIN Ingest Selected Shot":
                        menu.removeAction(i)
                menu.addAction(self)
    def cerrar(self):
        self.incoming_ui.close()
        self.continue_ = True

    def cerrar_v0(self):
        self.incoming_ui.close()
        self.continue_ = True

    def valorde_versiones(self):
        self.plate_version = self.incoming_ui.ui.spinBox_2.value()
        self.internal_version = self.incoming_ui.ui.spinBox.value()
        self.client_version = self.incoming_ui.ui.spinBox_4.value()
        print '''Plate:{0}\nInternal:{1}\nClient:{2}'''.format(self.plate_version, self.internal_version, self.client_version)
        self.incoming_ui.close()
        self.continue_ = True
        self.continue_after_UI()

    def valorde_frames(self):
        self.fframe = int(self.incoming_ui_v0.ui.inputff.text())
        self.lframe = int(self.incoming_ui_v0.ui.inputlf.text())
        self.incoming_ui_v0.close()
        self.continue_ = True
        self.post_v0_ui()

    def habilita_int(self):
        if self.incoming_ui.ui.checkBox.isChecked():
            self.incoming_ui.ui.groupBox.setEnabled(True)
        else:
            self.incoming_ui.ui.groupBox.setEnabled(False)

    def habilita_cli(self):
        if self.incoming_ui.ui.checkBox_2.isChecked():
            self.incoming_ui.ui.groupBox_2.setEnabled(True)
        else:
            self.incoming_ui.ui.groupBox_2.setEnabled(False)

    def show_ui(self):
        self.incoming_ui_class = self.UI_.dialogs.show_dialog(self)
        self.incoming_ui = self.incoming_ui_class()
        cb_int = self.incoming_ui.ui.checkBox
        cb_client = self.incoming_ui.ui.checkBox_2
        cb_int.clicked.connect(self.habilita_int)
        cb_client.clicked.connect(self.habilita_cli)
        self.incoming_ui.ui.buttonBox.accepted.connect(self.valorde_versiones)
        self.incoming_ui.ui.buttonBox.rejected.connect(self.cerrar)
        self.incoming_ui.show()

    def show_ui_v0(self, fframe, lframe, shot):
        self.incoming_ui_v0_class = self.UI_.dialogs.show_valid_dialog(self) # Valid_Ui_Form()
        self.incoming_ui_v0 = self.incoming_ui_v0_class()
        self.incoming_ui_v0.ui.inputff.setText(str(fframe))
        self.incoming_ui_v0.ui.inputlf.setText(str(lframe))
        self.incoming_ui_v0.ui.label.setText(shot)
        self.incoming_ui_v0.ui.buttonBox.accepted.connect(self.valorde_frames)
        self.incoming_ui_v0.ui.buttonBox.rejected.connect(self.cerrar_v0)
        x = self.incoming_ui_v0.show()

    def validations(self, clip):
        path_source = clip.source().mediaSource().fileinfos()[0].filename()
        print 'PATH ORIGINAL:', path_source
        if ' 'in path_source:
            self.error_window(1, '')
            return 'bad'

        elif '.mov' in path_source:
            self.error_window(2, path_source)
            return 'bad'
        seq1 = clip.parent().parent().name()
        seq = clip.parent().parent().name()
        filters = [['code', 'is', seq], ['project', 'is', self.sg_project]]
        secuencia = self.sg.find_one('Sequence', filters)
        if not secuencia:
            self.error_window(3, seq)
            return 'bad'

        filters = [['code', 'is', seq1], ['project', 'is', self.sg_project]]
        secuencia1 = self.sg.find_one('Sequence', filters)
        if not secuencia1:
            self.error_window(3, seq1)
            return 'bad'

        if len(clip.parent().tags()) == 0:
            warning = QMessageBox()
            warning.setText('There is no tag in this track do you want to continue?')
            warning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            sure = warning.exec_()
            if not sure == QMessageBox.Ok:
                return 'bad'

        track_name = clip.parent().name()
        if ' ' in track_name or '_' in track_name:
            self.error_window(4, track_name)
            return 'bad'

        filters_sh = [['code', 'is', clip.name()], ['project', 'is', self.sg_project]]
        SHT = self.sg.find_one('Shot', filters_sh, ['sg_head_in', 'sg_tail_out',
                                                    'sg_external_id', 'sg_offset_frame',
                                                    'sg_cut_in', 'sg_cut_out'])
        if not SHT:
            self.error_window(6, clip.name())
            return 'bad'

        sh_err = self.shot_fields_error(SHT)
        if not sh_err == 'no error':
            self.error_window(5, sh_err)
            return 'bad'

        return 'good'


