
import sgtk
import os
import sys
import threading

# by importing QT from sgtk rather than directly, we ensure that
# the code will be compatible with both PySide and PyQt.
from sgtk.platform.qt import QtCore, QtGui
from .ui.valid_ui import Valid_Ui_Form
from .ui.incoming_ui import Ui_Form
# standard toolkit logger
logger = sgtk.platform.get_logger(__name__)


def show_valid_dialog(app_instance):
    return AppValidDialog

def show_dialog(app_instance):
    return AppDialog

class AppValidDialog(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.ui = Valid_Ui_Form()
        self.ui.setupUi(self)
        self._app = sgtk.platform.current_bundle()
        #logger.info("Launching v0 UI...")


class AppDialog(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self._app = sgtk.platform.current_bundle()
        #logger.info("Launching v0 UI...")
