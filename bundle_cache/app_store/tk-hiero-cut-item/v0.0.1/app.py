# This example adds a right-click Menu to the Timeline View for getting the current Shot Selection.
# After running this action, 'hiero.selectedShots' will return the TrackItems selected in the timeline
from hiero.core import *
import hiero.ui

from hiero.ui import mainWindow # Need this to work around OS X 'Start Dictation' Bug
from PySide2 import QtGui
from PySide2 import QtCore
from PySide2 import QtUiTools
from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2 import QtWidgets
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import threading
import sgtk
import shutil
import tempfile
import subprocess
import os
import sys
from sgtk.platform import Application
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml

class Menu_ollin(Application,QAction):
    def init_app(self):
        QAction.__init__(self, "Get Selected Shots", None)
        self.triggered.connect(self.getShotSelection)
        hiero.core.events.registerInterest("kShowContextMenu/kTimeline", self.eventHandler)
        #self.engine = sgtk.platform.current_engine()
        self.sg = self.engine.shotgun
        self.sg_project = self.sg.find_one('Project',[['id','is',self.engine.context.project['id']]],['code'])
        self.sett = self.get_settings()

    def get_settings(self):
        tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
        self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                 "project_specs.yml")
        with open(p_specs_path, 'r') as stream:
            try:
                return yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def error_window(self, op, text_):
        if op == 1:
            error = 'Error:\nRevisa que la ruta no contenga espacios'
        if op == 2:
            error = 'Error:\nEstas tratando de ingestar un archivo .mov\n{0}'.format(text_)
        if op == 3:
            error = 'Error:\nNo existe la secuencia:{0}'.format(text_)
        if op == 4:
            error = 'Error:\nRevisa el nombre del track:{0}\nRecuerda:no debe tener espacios ni guiones'.format(text_)
        if op == 5:
            error = '{0}'.format(text_)
        if op == 6:
            error = 'No existe el shot: {0}'.format(text_)
        if op == 7:
            error = 'Verificar la duracion del corte en el shot: {0}'.format(text_)
        if op == 8:
            error = '{0}'.format(text_)
        warning = QMessageBox()
        warning.setText(error)
        warning.setStandardButtons(QMessageBox.Ok)
        sure = warning.exec_()
        return sure
    def create_folders(self, path):
        if not os.path.exists(path):
            try:
                chmod = '2775'
                ovfxPath=path.split("/")
                ovfxFolder=""
                for folder in ovfxPath:
                    ovfxFolder = ovfxFolder + folder + "/" 
                    if not os.path.exists(ovfxFolder):
                        os.system("sudo /usr/bin/mkdir %s"%(ovfxFolder))
                        os.system("sudo /usr/bin/chmod  %s %s "%(chmod,ovfxFolder))
                        os.system("sudo /usr/bin/chown %s %s"%('core', ovfxFolder))
                        if 'editorial' in ovfxFolder:
                            os.system("sudo /usr/bin/chgrp %s %s"%('editorial', ovfxFolder))
                        else:
                            os.system("sudo /usr/bin/chgrp %s %s"%('ovfx', ovfxFolder))
            except Exception as e:
              raise e
    def getShotSelection(self):
        """Get the shot selection and stuff it in: hiero.selectedShots"""
        warning = QMessageBox()
        warning.setText('Create CUT from selected clips?')
        warning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        sure = warning.exec_()
        if sure == QMessageBox.Ok:
            self.show_ui()
            self.config_path = self.engine.context.tank.pipeline_configuration.get_path()
            self.sg = self.engine.shotgun
            self.sg_project = self.engine.context.project
            self.sg_project = self.sg.find_one('Project',[['id','is',self.sg_project['id']]],['code'])
            os.system("USER=$(whoami)")
            self.user = os.environ["USER"]
            self.ref_template = self.engine.get_template_by_name("seq_editorial_ref_mov")
            selection = self._selection
            self.shot_dict = []
            if len(selection)==1:
                self.video_paths =[]
                self.total_duration = 0
                clip = selection[0]
                contin = self.validations(clip)
                if contin == 'bad':
                  return
                path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                seq = clip.parent().parent().name()
                self.set_fields_and_path(clip, seq, path_source)
            else:
                #------------------------------------------------------------------------------------------
                self.video_paths = []
                self.total_duration = 0
                for clip in selection:
                    path_source_ = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                    if path_source_ in self.video_paths:
                        pass
                    else:
                        self.video_paths.append(path_source_)
                if len(self.video_paths) > 1:
                    self.total_duration = clip.parent().parent().metadata()['foundry.timeline.duration']
            #-------------------------------------------------------------------------------------------
            for clip in selection:
                path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                seq = clip.parent().parent().name()
                contin = self.validations(clip)
                if contin == 'bad':
                    return
                self.set_fields_and_path(clip, seq, path_source)
                #prefixx = '/nfs/ovfxToolkit/TMP/'
            prefixx = self.sett['tmp_folder']
            (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "cut_dict_", suffix='.txt')
            file = open(dictPath, 'w')
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell = True)
            file.write(str(self.shot_dict))
            file.close()
            print dictPath
            tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
            config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
            p_specs_path = os.path.join(config_path, "hooks", "Hiero",
                                                     "ovfx_cut_item.py")
            scriptPath = p_specs_path
            self.send_job(scriptPath,'CUT_' + str(self.sg_project['code']),dictPath)
            warning = QMessageBox()
            warning.setText('CUT & CUT-ITEMS job now is  in Deadline!')
            warning.setStandardButtons(QMessageBox.Ok)
            warning.exec_()

    # generate paths and Cut
    def send_job(self, python_script, name, dict_):
        nuke_version = self.sett['main_nuke_path']
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
        bndl_path =  str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        nuke_cmd += ' "-t {0} {1} {2} {3}" '.format(python_script,dict_, self.config_path, bndl_path)
        nuke_cmd += ' -name "{0}" -pool editorial'.format(name)
        nuke_cmd += ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        #deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
        deadline = self.sett['deadline_command_path']
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd

    def set_fields_and_path(self, clip, seq, path_source):
        self.shot_d = {}
        seq = clip.parent().parent().name()
        fields = {'Shot': str(clip.name()),
            'Sequence': str(seq),
            'version': int(self.VERSION)}
        track = clip.parent()
        indice = 0
        for itm in track.items():
            indice += 1
            print itm.name()
            if itm == clip:
                break

        fields['width'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[0])
        fields['height'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[1])
        fields['elementName'] = track.name()
        fields['project'] = self.sg_project['code']
        print 'project:', self.sg_project['code']

        self.ref_path = self.ref_template.apply_fields(fields)

        self.shot_d['shot'] = str(clip.name())
        self.shot_d['seq'] = str(seq)
        self.shot_d['version'] = int(self.VERSION)
        self.shot_d['ref_path'] = str(self.ref_path)
        self.shot_d['project'] = self.sg_project
        self.shot_d['indice'] = indice
        self.shot_d['total_duration'] = self.total_duration
        self.shot_d['video_parts'] = self.video_paths

        if self.VERSION == '':
            self.VERSION = 1
        Cut_name = clip.parent().parent().name().upper()
        Vers = '{0:03d}'.format(int(self.VERSION))
        FR = framerate = float(clip.source().mediaSource().metadata()['media.input.frame_rate'])

        if int(self.VERSION) > 1:
            Cut_name = Cut_name + '_' + str(Vers)
        
        Cut = self.sg.find_one('Cut', [['code', 'is', Cut_name], ['project', 'is', self.sg_project]],['version'])
        self.shot_d['cut_name'] = str(Cut_name)
        out_ref = self.ref_path

        if not os.path.exists('{0}'.format(os.path.dirname(out_ref))):
            self.create_folders('{0}'.format(os.path.dirname(out_ref)))
        media = clip.source().mediaSource()
        self.shot_d['cut_first'] = media.startTime()
        self.shot_d['cut_duration'] = media.duration()
        self.shot_d['cut_last']= int(self.shot_d['cut_first']) + int(self.shot_d['cut_duration'])
        self.shot_d['cut_origin_path'] = path_source

        tup = hiero.core.Timecode.framesToHMSF(self.shot_d['cut_first'], framerate,True)
        timecode_in = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        tup = hiero.core.Timecode.framesToHMSF(self.shot_d['cut_last'], framerate,True)
        timecode_out = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        self.shot_d['cut_timecode_in'] = timecode_in
        self.shot_d['cut_timecode_out'] = timecode_out

        #origin_path = media.fileinfos()[0].filename().split(' ')[0]
        #self.shot_dict.append(self.shot_d)
        self.shot_d['out_ref'] = out_ref

        fr_range = str(clip.timelineIn()) + '-' + str(clip.timelineOut())
        first = int(clip.timelineIn())
        last = int(clip.timelineOut())
        tup = hiero.core.Timecode.framesToHMSF(last, framerate,True)
        timecode_out = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        tup = hiero.core.Timecode.framesToHMSF(first, framerate,True)
        timecode_in = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        self.shot_d['clip_duration'] = last - first + 1
        self.shot_d['clip_fr_range'] = fr_range
        self.shot_d['clip_first'] = first
        self.shot_d['clip_last'] = last
        self.shot_d['project_code'] = self.sg_project['code']
        self.shot_d['framerate'] = FR
        self.shot_d['artist'] = self.user
        self.shot_d['clip_timecode_in'] = timecode_in
        self.shot_d['clip_timecode_out'] = timecode_out
        self.shot_d['clip_name'] = str(clip.name())
        self.shot_dict.append(self.shot_d)
        print self.shot_dict

    def eventHandler(self, event):
        self._selection = None
        if hasattr(event.sender, 'getSelection') and event.sender.getSelection() is not None and len( event.sender.getSelection() ) != 0:
            selection = event.sender.getSelection() # Here, you could also use: hiero.ui.activeView().selection()

        self._selection = [shot for shot in selection if isinstance(shot,hiero.core.TrackItem)] # Filter out just TrackItems
        if len(selection)>0:
            title = "OLLIN Create Cut For Selected Shot"
            self.setText(title)
            menu = event.menu 
            for i in event.menu.actions():
                if str(i.text()) == "OLLIN Create Cut For Selected Shot":
                    menu.removeAction(i)
            menu.addAction(self)

    def cerrar(self):
        self.msgBox2.close()
        self.mb1.close()

    def valorde_version(self):
        version = int(self.v_line.text())
        self.msgBox2.close()
        self.VERSION = version

    def show_ui(self):
        self.msgBox2 = QMessageBox() 
        self.msgBox2.setText("SET CUT VERSION PLEASE:") 
        lay = self.msgBox2.layout()
        lay.itemAtPosition(lay.rowCount() - 1, 1).widget().hide()
        self.v_line = QLineEdit()
        butt = QPushButton('Ok')
        cancel_b = QPushButton('Cancel')
        butt.clicked.connect(self.valorde_version)
        cancel_b.clicked.connect(self.cerrar)
        lay.addWidget(self.v_line,lay.rowCount() - 1, 1, 1, lay.columnCount())
        lay.addWidget(butt,lay.rowCount(), 1, 1, lay.columnCount())
        lay.addWidget(cancel_b,lay.rowCount(), 0, 1, lay.columnCount())
        self.msgBox2.exec_()

    def validations(self, clip):
        path_source = clip.source().mediaSource().fileinfos()[0].filename()
        if ' ' in path_source:
            self.error_window(1, '')
            return 'bad'
      
        filters = [['code', 'is', clip.name()], ['project', 'is', self.sg_project]]
        shot = self.sg.find_one('Shot', filters,['sg_cut_duration'])
        if shot:
            first = int(clip.timelineIn())
            last = int(clip.timelineOut())
            duration = last - first + 1
            if shot['sg_cut_duration']:
                if not shot['sg_cut_duration'] == duration:
                    self.error_window(7, str('\nShot: {0}\nDuracion_ {1}'.format(clip.name(),duration)))
                    return 'bad'
            else:
                self.error_window(8, 'no existe el campo cut duration en el shot {0}'.format(clip.name()))
                return 'bad'
        seq1 = clip.parent().parent().name()
        filters = [['code', 'is', seq1], ['project', 'is', self.sg_project]]
        secuencia1 = self.sg.find_one('Sequence', filters)
        if not secuencia1:
            self.error_window(3, seq1)
            return 'bad'

        track_name = clip.parent().name()
        if ' ' in track_name:
            self.error_window(4, track_name)
            return 'bad'
        return 'good'

