# Copyright (c) 2016 ollin vfx
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import sgtk
import glob
import shutil

from tank import Hook
from tank import TankError


class PrimaryPublishClientHook(Hook):

    def execute(self, cadena, **kwargs):
        """
        Main hook entry point
        :param path:         the path where frames should be found.
        :param first_frame: Start frame for the output movie
        :param last_frame:  End frame for the output movie
        :param shot:                shotgun shot
        """
        print cadena
