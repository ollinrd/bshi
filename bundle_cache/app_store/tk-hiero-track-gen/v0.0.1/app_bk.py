from tank.platform import Application
import sgtk
import os
from hiero.ui import *
from hiero.core import *

class Menu_ollin(Application):
    def init_app(self):
        # print 'houdini nodes'
        self.engine.register_command(
            "Seq. Track Gen",
            self.create_seq_track,
            {}
        )
        self.seq_name = 'zc'

    def createTrackItem(self, track, trackItemName, sourceClip, lastTrackItem= None, trim_in =0, trim_out = 0):
      # create the track item
      trackItem = track.createTrackItem(trackItemName)
      
      # set it's source
      trackItem.setSource(sourceClip)
      
      # set it's timeline in and timeline out values, offseting by the track item before if need be
      if lastTrackItem:
        trackItem.trimIn(trim_in)
        trackItem.trimIn(trim_out)
        trackItem.setTimelineIn(lastTrackItem.timelineOut() + 1)
        trackItem.setTimelineOut(lastTrackItem.timelineOut() + sourceClip.duration()- trim_out)
        print trackItem.timelineIn(), trackItem.timelineOut()

      else:
        trackItem.trimIn(trim_in)
        trackItem.trimIn(trim_out)
        trackItem.setTimelineIn(0)
        trackItem.setTimelineOut(trackItem.sourceDuration() - trim_out)
        print trackItem.timelineIn(), trackItem.timelineOut()

      
      # add the item to the track

      track.addItem(trackItem)
      return trackItem


    def create_seq_track(self):
        engine = sgtk.platform.current_engine()
        project_id = 334
        project = engine.shotgun.find_one('Project', [['id', 'is', project_id]])
        # project = engine.context.project
        filters = [['code', 'is', self.seq_name],['project', 'is', project]]
        sg_sequence = engine.shotgun.find_one('Sequence', 
                                              filters,
                                              ['shots', 'code'])
        filters = [['entity', 'is', sg_sequence]]
        cuts = engine.shotgun.find('Cut', 
                                       filters,
                                       ['version.Version.sg_path_to_movie', 'code', 'cut_items'])
        cut = cuts[0]
        print cut['code']
        print  cut['version.Version.sg_path_to_movie']
        ref_movie_path = cut['version.Version.sg_path_to_movie']
        new_cutitems = []
        for cutit in cut['cut_items']:
            filters = [['id', 'is', cutit['id']]]
            cutit = engine.shotgun.find_one('CutItem',
                                            filters,
                                           ['code', 'shot', 'edit_in', 'edit_out', 'cut_order'])
            print cutit
            new_cutitems.append(cutit)
        new_cutitems = sorted(new_cutitems, key=lambda i: i['cut_order'])
        bin1, clipsBin, sequence = self.create_reference_track(ref_movie_path)
        # get shot versions
        shot_v = []
        for shot in sg_sequence['shots']:

            filters = [['id', 'is', shot['id']]] #'edapp'
            shot = engine.shotgun.find_one('Shot', filters, ['sg_head_in', 'sg_tail_out', 'code', 'sg_cut_in', 'sg_cut_out'])
            filters = [['sg_status_list', 'is', 'apr'], ['entity', 'is', shot]] #'edapp'
            version = engine.shotgun.find_one('Version', filters, ['sg_path_to_frames', 'sg_path_to_movie'])
            if version:
                shot_v.append({'shot': shot, 'version': version})
        shot_v = sorted(shot_v, key=lambda i: i['shot']['code'])
        self.create_apv_version_track(new_cutitems, bin1, clipsBin, shot_v, sequence)



    def create_apv_version_track(self, new_cutitems, bin1, clipsBin, shot_v, sequence):
        sources = []
        clips = []
        names = []
        trim_in = []
        trim_out = []
        for cutit in new_cutitems:
            print cutit['edit_in'], cutit['edit_out'], cutit['shot']
        for version in shot_v:
            print version['shot']['code']
            t_in = version['shot']['sg_cut_in'] - version['shot']['sg_head_in']
            t_out = version['shot']['sg_tail_out'] - version['shot']['sg_cut_out']
            trim_in.append(t_in)
            trim_out.append(t_out)
            names.append(version['shot']['code'])
            path = version['version']['sg_path_to_movie']
            source = MediaSource(path)
            sources.append(source)
            clip = Clip(source)
            clip.setName(version['shot']['code'])
            clips.append(clip)
            binitem = BinItem(clip)
            clipsBin.addItem(binitem)
            bin1.addItem(binitem)

        track = VideoTrack("apv_versions")

        current_item = self.createTrackItem(track, names[0], clips[0], trim_in = trim_in[0], trim_out =trim_out[0])
        for i in range(1, len(names)):
            current_item = self.createTrackItem(track, names[i], clips[i], lastTrackItem=current_item, trim_in = trim_in[i], trim_out = trim_out[i])
        sequence.addTrack(track)


    def create_reference_track(self, ref_movie_path):
        myProject = projects()[-1]
        bin1 = Bin("zr")
        clipsBin = myProject.clipsBin()
        clipsBin.addItem(bin1)
        source = MediaSource(ref_movie_path)
        clip = Clip(source)
        clip.setName(self.seq_name + '_ref')
        binitem = BinItem(clip)
        clipsBin.addItem(binitem)
        bin1.addItem(binitem)
        sequence = Sequence(self.seq_name)
        clipsBin.addItem(BinItem(sequence))
        track = VideoTrack(self.seq_name + "_Reference")
        self.createTrackItem(track, self.seq_name + '_ref', clip)
        sequence.addTrack(track)
        return bin1, clipsBin, sequence
        '''

        shot_v = []
        for shot in sg_sequence['shots']:

            filters = [['id', 'is', shot['id']]] #'edapp'
            shot = engine.shotgun.find_one('Shot', filters, ['sg_head_in', 'sg_tail_out', 'code'])

            filters = [['sg_status_list', 'is', 'apr'], ['entity', 'is', shot]] #'edapp'
            version = engine.shotgun.find_one('Version', filters, ['sg_path_to_frames', 'sg_path_to_movie'])
            if version:
                shot_v.append({'shot': shot, 'version': version})
        shot_v = sorted(shot_v, key=lambda i: i['shot']['code'])
        myProject = projects()[-1]
        bin1 = Bin("zr")
        clipsBin = myProject.clipsBin()
        clipsBin.addItem(bin1)
        sources = []
        clips = []
        names = []

        for version in shot_v:
            
            print version['shot']['code']
            names.append(version['shot']['code'])
            path = version['version']['sg_path_to_movie']
            source = MediaSource(path)
            sources.append(source)
            clip = Clip(source)
            clip.setName(version['shot']['code'])
            clips.append(clip)
            binitem = BinItem(clip)
            clipsBin.addItem(binitem)
            bin1.addItem(binitem)
        sequence = Sequence("zr")
        clipsBin.addItem(BinItem(sequence))

        track = VideoTrack("VideoTrack")

        current_item = self.createTrackItem(track, names[0], clips[0])
        for i in range(1, len(names)):
            current_item = self.createTrackItem(track, names[i], clips[i], lastTrackItem=current_item)
        sequence.addTrack(track)

        '''