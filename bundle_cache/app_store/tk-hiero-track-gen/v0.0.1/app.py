from tank.platform import Application
import sgtk
import os
from hiero.ui import *
from hiero.core import *
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtWidgets import *

class Menu_ollin(Application):
    def init_app(self):
        # print 'houdini nodes'
        self.engine.register_command(
            "Seq. Track Gen",
            self.create_seq_track,
            {}
        )
        self.sg_sequence = ''
    def createTrackItem(self, track, trackItemName, sourceClip, lastTrackItem= None, trim_in =0, trim_out = 0):
        # create the track item
        trackItem = track.createTrackItem(trackItemName)

        # set it's source
        trackItem.setSource(sourceClip)

        # set it's timeline in and timeline out values, offseting by the track item before if need be
        if lastTrackItem:
          trackItem.trimIn(trim_in)
          trackItem.trimOut(trim_out)
          trackItem.setTimelineIn(lastTrackItem.timelineOut() + 1)
          trackItem.setTimelineOut(lastTrackItem.timelineOut() + sourceClip.duration()- trim_out)
        else:
          trackItem.trimIn(trim_in)
          trackItem.trimOut(trim_out)
          trackItem.setTimelineIn(0)
          trackItem.setTimelineOut(trackItem.sourceDuration() - trim_out)

        # add the item to the track
        track.addItem(trackItem)
        return trackItem

    def createTrackItem_rev(self, track, trackItemName, sourceClip,  edt_in, edt_out, lastTrackItem= None, trim_in =0, trim_out = 0):
        # create the track item
        trackItem = track.createTrackItem(trackItemName)
        # set it's source
        trackItem.setSource(sourceClip)
        # set it's timeline in and timeline out values, offseting by the track item before if need be

        #trackItem.trimIn(trim_in)
        #trackItem.trimOut(trim_out)
        trackItem.setSourceIn(trim_in +1)
        trackItem.setSourceOut(sourceClip.sourceOut() - trim_out)
        trackItem.setTimelineIn(edt_in)
        trackItem.setTimelineOut(edt_out)
        # add the item to the track
        track.addItem(trackItem)
        return trackItem

    def show_ui(self):
        self.export_ui = self.mod_ps_publish.Ui_Form()
        self.Dialog = QtWidgets.QDialog()
        self.export_ui.setupUi(self.Dialog)
        self.export_ui.pushButton.clicked.connect(self.get_cuts)
        self.export_ui.buttonBox.accepted.connect(self.get_params)
        self.export_ui.buttonBox.rejected.connect(self.cerrar)
        self.Dialog.exec_()

    def get_cuts(self):
        self.export_ui.comboBox.clear()
        self.seq_name = str(self.export_ui.lineEdit.text())
        filters = [['code', 'is', self.seq_name], ['project', 'is', self.project]]
        sg_sequence = self.engined.shotgun.find_one('Sequence', 
                                              filters,
                                              ['shots', 'code'])
        if not sg_sequence:
            self.cerrar()
            warning = QMessageBox()
            warning.setText('Bad Sequence name !!!!!!!!')
            warning.setStandardButtons(QMessageBox.Ok)
            sure = warning.exec_()
            return
        filters = [['entity', 'is', sg_sequence], ['project', 'is', self.project]]
        cuts = self.engined.shotgun.find('Cut',
                                   filters,
                                   ['version.Version.sg_path_to_movie', 'code', 'cut_items', 'duration'])
        if 'list' in str(type(cuts)):
            for cut in cuts:
                self.export_ui.comboBox.addItem(cut['code'] + ' duration:' + str(cut['duration']))
        else:
            self.export_ui.comboBox.addItem(cut['code'] + ' duration:' + str(cut['duration']))
        self.sg_sequence = sg_sequence
        self.cuts = cuts

    def get_params(self):
        self.corte = str(self.export_ui.comboBox.currentText()).replace('duration:', '')
        self.Dialog.close()

    def cerrar(self):
        self.contin = False
        self.Dialog.close()

    def create_seq_track(self):
        self.execute_hook('hook_hiero_track_gen', cadena = 'hola')
        '''
        self.mod_ps_publish = self.import_module('tk_hiero_track')
        engine = sgtk.platform.current_engine()
        self.engined = engine
        project = engine.context.project
        self.project = project
        self.contin = True
        self.show_ui()
        if self.contin:
            if self.sg_sequence == '':
                    warning = QMessageBox()
                    warning.setText('Please select one Cut first !!!')
                    warning.setStandardButtons(QMessageBox.Ok)
                    sure = warning.exec_()
                    return
            sg_sequence = self.sg_sequence
            cuts = self.cuts
            for cut in cuts:
                if cut['code'] == self.corte.split(' ')[0] and str(cut['duration']) == self.corte.split(' ')[1]:
                    cut = cut

            ref_movie_path = cut['version.Version.sg_path_to_movie']
            new_cutitems = []
            for cutit in cut['cut_items']:
                filters = [['id', 'is', cutit['id']]]
                cutit = engine.shotgun.find_one('CutItem',
                                                filters,
                                                ['code', 'shot', 'edit_in', 'edit_out', 'cut_order'])

                new_cutitems.append(cutit)
            new_cutitems = sorted(new_cutitems, key=lambda i: i['cut_order'])
            bin1, clipsBin, sequence = self.create_reference_track(ref_movie_path)
            # get shot versions
            shot_v = []
            for shot in sg_sequence['shots']:

                filters = [['id', 'is', shot['id']]] #'edapp'
                shot = engine.shotgun.find_one('Shot', filters, ['sg_head_in', 'sg_tail_out', 'code', 'sg_cut_in', 'sg_cut_out'])
                filters = [['sg_status_list', 'is', 'edapp'], ['entity', 'is', shot]] #'edapp'
                version = engine.shotgun.find_one('Version', filters, ['sg_path_to_frames', 'sg_path_to_movie'])
                if version:
                    shot_v.append({'shot': shot, 'version': version})
            shot_v = sorted(shot_v, key=lambda i: i['shot']['code'])
            self.create_apv_version_track(new_cutitems, bin1, clipsBin, shot_v, sequence)
            warning = QMessageBox()
            warning.setText('Complete!!!!!!!!')
            warning.setStandardButtons(QMessageBox.Ok)
            sure = warning.exec_()
        '''



    def create_apv_version_track(self, new_cutitems, bin1, clipsBin, shot_v, sequence):
        sources = []
        clips = []
        names = []
        trim_in = []
        trim_out = []
        edt_start = []
        edt_out = []
        for version in shot_v:
            for cutit in new_cutitems:
                if cutit['shot']:
                    if cutit['shot']['id'] == version['shot']['id']:
                        edt_start.append(cutit['edit_in'])
                        edt_out.append(cutit['edit_out'])
            t_in = version['shot']['sg_cut_in'] - version['shot']['sg_head_in']
            t_out = version['shot']['sg_tail_out'] - version['shot']['sg_cut_out']
            trim_in.append(t_in)
            trim_out.append(t_out)
            names.append(version['shot']['code'])
            path = version['version']['sg_path_to_movie']
            source = MediaSource(path)
            sources.append(source)
            clip = Clip(source)
            clip.setName(version['shot']['code'])
            clips.append(clip)
            binitem = BinItem(clip)
            clipsBin.addItem(binitem)
            bin1.addItem(binitem)

        track = VideoTrack("apv_versions")

        current_item = self.createTrackItem_rev(track, names[0], clips[0], edt_start[0], edt_out[0], trim_in = trim_in[0], trim_out =trim_out[0])
        for i in range(1, len(names)):
            current_item = self.createTrackItem_rev(track, names[i], clips[i], edt_start[i], edt_out[i],
                                                lastTrackItem=current_item, trim_in = trim_in[i],
                                                trim_out = trim_out[i])
        sequence.addTrack(track)


    def create_reference_track(self, ref_movie_path):
        myProject = projects()[-1]
        bin1 = Bin(self.seq_name)
        clipsBin = myProject.clipsBin()
        clipsBin.addItem(bin1)
        source = MediaSource(ref_movie_path)
        clip = Clip(source)
        clip.setName(self.seq_name + '_ref')
        binitem = BinItem(clip)
        clipsBin.addItem(binitem)
        bin1.addItem(binitem)
        sequence = Sequence(self.seq_name)
        clipsBin.addItem(BinItem(sequence))
        track = VideoTrack(self.seq_name + "_Reference")
        self.createTrackItem(track, self.seq_name + '_ref', clip)
        sequence.addTrack(track)
        return bin1, clipsBin, sequence
