
from tank.platform import Application
import sgtk
import os
import pymel.core as pm
import maya.cmds as cmds



class Menu_rigging(Application):
    def init_app(self):

        if self.engine.context.step is not None:
            entityType = self.engine.context.entity['type']
            pipelineStep= self.engine.context.step['name']
        else:
            tipo= ''
            pipelineStep= ''

        #Pipeline step validation
        valid_steps=["rig", "fx"]
        if pipelineStep in valid_steps:

            # Get Modules
            ribbon_tool= self.import_module("ribbon_bridge")
            ribbon_ui= ribbon_tool.FormMain()

            locOnCVs_tool= self.import_module("locOnCVs_bridge")
            locOnCvs_ui= locOnCVs_tool.FormMain()



            # Set Menus
            MainMayaWindow = pm.language.melGlobals['gMainWindow']
            for i in sorted(pm.lsUI(menus=True)):
                if 'Rigging' in str(i):
                    cmds.deleteUI(str(i))

            customMenu = pm.menu('Rigging tools', parent=MainMayaWindow)
            #Tools

            pm.menuItem(label="Ribbon Tool",
                        command=ribbon_ui.initUI,
                        parent=customMenu)

            pm.menuItem(label="LocOnCVs Tool",
                        command=locOnCvs_ui.initUI,
                        parent=customMenu)

        else:
            for i in sorted(pm.lsUI(menus=True)):
                if 'Rigging' in str(i):
                    cmds.deleteUI(str(i))

            

"""
import sys
import sgtk
from sgtk import TankError
from sys import platform


class SimulationConstraint(Application):
    def init_app(self):
        valid_steps = ['fx', 'character fx']
        if self.engine.context.step['name'] in valid_steps:
            self.engine.register_command("Animation to Ziva bones",
                                         self.mainWindow)
            self.loaded = False

    def mainWindow(self):

        if not self.loaded:
            self.loaded = True
            self.eng_name = self.engine.name
            self.sg = self.engine.shotgun

            # Get functionaities
            self.tk_Simulation_ConstraintOps = self.import_module("tk_maya_anim_attach")
            #Get instance of operations class depending on current
            #engine

        self.bridge = self.tk_Simulation_ConstraintOps.FormMain(
            loc=self.disk_location, context=self.engine.context,
            sg=self.sg, engine=self.engine)

        #Reload context
        self.bridge.show()
"""