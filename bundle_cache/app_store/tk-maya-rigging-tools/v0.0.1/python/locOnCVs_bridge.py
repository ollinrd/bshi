from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import QtWidgets
import maya.cmds as cmds
import pymel.core as pm 
import maya.OpenMayaUI as OMUI
import sys
import os
import shiboken2
import sgtk
from  locOnCVs_core import locOnCrv

class FormMain():

    def __init__(self, parent=None):
        self.spanMode=1
        
        #Init Classes
        self.core_process= locOnCrv()


    def initUI(self, parent=None):
        #UI
        encoding = sys.getfilesystemencoding()
        self.loc= os.path.dirname(unicode(__file__, encoding))
        resources=self.loc.replace("python", "resources")
        self.uiFileName = resources + '/ollin-CVs-tool.ui'

        self._init_maya_ui(self.uiFileName, parent)
        self._config_UI_()
        self.show()


    def _init_maya_ui(self, uiFileName, parent=None):
        from pymel import versions
        if versions.current() < 201700:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        uifile = QtCore.QFile(self.uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)

        #Maya main window
        self.mayaWin = OMUI.MQtUtil.mainWindow() 
        self.mayaWin = shiboken2.wrapInstance(long(self.mayaWin), QtWidgets.QWidget)
        self.ui = self.loader.load(uifile, self.mayaWin)
        uifile.close()


    def _config_UI_(self):
        self.ui.spans_chkBox.stateChanged.connect(self.spansMode)

        self.ui.btn_create.clicked.connect(self.createLocs)


    def spansMode(self):
        self.spanMode= self.spanMode*-1
        if self.spanMode== -1:
            self.ui.span_box.setDisabled(False)
        else:
            self.ui.span_box.setDisabled(True)
            self.ui.span_box.setText("")



    def createLocs(self):
        name= self.ui.name_box.text()
        if self.ui.span_box.text() == "":
            number= ""

        else:
            number=int(self.ui.span_box.text())
        self.core_process.create(name,number)



    def close(self):
        #cerrar venatna
        self.ui.hide()

    def show(self):
        self.ui.show()


