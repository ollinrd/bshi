import pymel.core as pm
import maya.cmds as cmds
import sys

class ribbon():
    def __init__(self):
        pass

    def create(self, name, number, curveMode, coordinate ):
        #node Validation
        sel= pm.ls(selection=True)
        node_= sel[0]
        nodeType= pm.nodeType(node_.getShape())

        if curveMode == 1:
            if nodeType == "nurbsCurve":
                self.createSurface(name)
                self.createRibbon(name, number, coordinate)
            else:
                pm.confirmDialog(t="Ollin ribbon tool", m="please select a nurbsCurve to complete the operation")

        else:
            if nodeType == "nurbsSurface":
                self.createRibbon(name,number,coordinate)
            else:
                pm.confirmDialog(t="Ollin ribbon tool", m="please select a nurbsSurface to complete the operation")


    def createSurface(self, name):
        sel= pm.ls(selection=True)
        curve_= sel[0]
        for i in range(2):
            index= i+1
            crv=pm.duplicate(curve_,n="crv0{0}".format(index))
            crv_cvs= crv[0].getCVs(space="world")
            vertexNumber= len(crv_cvs)
            if index==1:
                val=1
                magnitud_ls=[]
            else:
                val=-1
                magnitud_ls=[]
            for v in range(vertexNumber):
                vertex=-1
                pm.select("crv0{0}.cv[{1}]".format(index,v),add=1)
                magnitud_ls.append(val)
            
            pm.moveVertexAlongDirection(n=magnitud_ls)
            pm.select(cl=1)
            
        surface=pm.loft("crv01", "crv02", sectionSpans=1, n= "{0}_surface".format(name))
        pm.delete(surface, ch=1)

        for i in range (2):
            index= i+1
            pm.delete("crv0{0}".format(index))

        pm.select(cl=1)
        pm.select("{0}_surface".format(name))
     


    def createRibbon(self, name, number, coordinate):
        sel=pm.ls(sl=True)[0].getShape()
        if number==1:
            Div = 0.5
        else:
            Div=1/float(number-1)
        print Div

        for y in range(number):
            #Crear grupos  y controles
            pm.select(cl=1)
            offset= pm.group(n= name+"_offset"+ "_0" + str(y))
            loc=pm.spaceLocator(n= name+ "_loc"+ "_0" + str(y))
            ctrl= pm.circle(d=1, s=4, n=name + "_ctrl"+ "_0" + str(y) , radius=0.2)[0]
            jnt=pm.joint(n=name + "_0" + str(y) + "_jnt", radius=0.2)
            pm.parent(loc, offset)
            pm.parent(ctrl, loc)
            pm.parent(jnt, ctrl)
            
            pos= pm.createNode('pointOnSurfaceInfo', n=(name + "_POS_" +  str(y)))
            fbf= pm.createNode('fourByFourMatrix', n=(name + "_FBF_" +  str(y)))
            dM= pm.createNode('decomposeMatrix', n=( name + "_DM_" + str(y)))

            sel.worldSpace.connect(pos.inputSurface)

            pos.turnOnPercentage.set(1)
            #Matrix
            #position
            pos.result.position.positionX.connect(fbf.in30)
            pos.result.position.positionY.connect(fbf.in31)
            pos.result.position.positionZ.connect(fbf.in32)

            #normal
            pos.result.normalizedNormal.normalizedNormalX.connect(fbf.in00)
            pos.result.normalizedNormal.normalizedNormalY.connect(fbf.in01)
            pos.result.normalizedNormal.normalizedNormalZ.connect(fbf.in02)

            #tangent U
            pos.result.normalizedTangentU.normalizedTangentUX.connect(fbf.in10)
            pos.result.normalizedTangentU.normalizedTangentUY.connect(fbf.in11)
            pos.result.normalizedTangentU.normalizedTangentUZ.connect(fbf.in12)

            #tangent V
            pos.result.normalizedTangentV.normalizedTangentVX.connect(fbf.in20)
            pos.result.normalizedTangentV.normalizedTangentVY.connect(fbf.in21)
            pos.result.normalizedTangentV.normalizedTangentVZ.connect(fbf.in22)

            if number==1:
                y = 1

            if coordinate ==1 :
                pos.parameterV.set(0.5)
                pos.parameterU.set(Div*y)

            if coordinate == 0:
                pos.parameterV.set(Div*y)
                pos.parameterU.set(0.5)

            fbf.output.connect(dM.inputMatrix)
            dM.outputTranslate.connect(offset.translate)
            dM.outputRotate.connect(offset.rotate)
