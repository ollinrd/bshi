import pymel.core as pm
import maya.cmds as cmds

class locOnCrv():

	def __init__(self):
		pass


	def create(self,name,number):
		if number:
			self.rebuildCurve(number)	
			self.CreateShit(name)
		else:
			self.CreateShit(name)

	def rebuildCurve(self, number):
		#Rebuild curve
		sel= pm.ls(selection=True)
		curve_= sel[0]
		degree= curve_.degree()
		pm.rebuildCurve(curve_,rebuildType=0,spans=number, degree= degree)
		pm.select(curve_)

	def CreateShit(self,name):
		sel= pm.ls(selection=True, fl=True)
		puntos = []
		numeroVertices= len(sel[0].getCVs(space='world'))
		CV=sel[0].getShape()
		jnts=[]
		position= sel[0].getCVs(space='world')
		setPOS=[]
		for x in range(numeroVertices):
		   setPOS.append(position[x])
		print position[0]
		print numeroVertices
		print setPOS

		Value= (1/float(numeroVertices))

		for y in range(numeroVertices):
			CV= sel[0].getShape()
			NPoC=pm.createNode('nearestPointOnCurve', n=( name + "NPoC" + str(y)))
			CV.worldSpace[0].connect(NPoC.inputCurve) #connect Attrs
			NPoC.inPosition.set(setPOS[y])
			Parameter=NPoC.parameter.get()
			print Parameter, setPOS[y]

			pm.select(cl=True)
			Joint= pm.joint( n=(name + "_0" + str(y) + "_Skin"), radius=0.2)
			Shape=pm.circle(d=1, s=4, n= (name + "_0" + str(y) + "_Anim"), radius=0.2)[0]
			pm.parent(Joint, Shape)
			grpScl=pm.group(Shape, n=(name + "_Scale_0" + str(y)))
			grp=pm.group(grpScl, n=(name + "_Offset_0" + str(y)))
			grp.translate.set(0,0,0)
			Joint.translate.set(0,0,0)
			pm.parent(grp, w = True)
			jnts.append(grp)

			poc= pm.createNode('pointOnCurveInfo', n=( name + "PoC" + str(y)))
			CV.worldSpace.connect(poc.inputCurve)
			print poc
			"""poc.parameter.set(Value*y)"""
			poc.parameter.set(Parameter)

			#Create Matrix node
			fbf =pm.createNode('fourByFourMatrix', n=(name + "_FBF_" +  str(y)))
			dM= pm.createNode('decomposeMatrix', n=(name + "-DM_" + str(y)))

			#position
			poc.result.position.positionX.connect(fbf.in30)
			poc.result.position.positionY.connect(fbf.in31)
			poc.result.position.positionZ.connect(fbf.in32)

			#normal
			poc.result.normalizedNormal.normalizedNormalX.connect(fbf.in00)
			poc.result.normalizedNormal.normalizedNormalY.connect(fbf.in01)
			poc.result.normalizedNormal.normalizedNormalZ.connect(fbf.in02)

			#tangent 
			poc.result.normalizedTangent.normalizedTangentX.connect(fbf.in10)
			poc.result.normalizedTangent.normalizedTangentY.connect(fbf.in11)
			poc.result.normalizedTangent.normalizedTangentZ.connect(fbf.in12)


			fbf.output.connect(dM.inputMatrix)
			dM.outputTranslate.connect(jnts[y].translate)
			#dM.outputRotate.connect(jnts[y].rotate)
			#poc.result.position.connect(jnts[y].translate)
			pm.delete(NPoC)


