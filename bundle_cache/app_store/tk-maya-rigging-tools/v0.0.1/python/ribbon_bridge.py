from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import QtWidgets
import maya.cmds as cmds
import pymel.core as pm 
import maya.OpenMayaUI as OMUI
import sys
import os
import shiboken2
import sgtk
from  ribbon_core import ribbon

class FormMain():

    def __init__(self, parent=None):
       
        self.engine = sgtk.platform.current_engine()
        self.crv=1
        self.u=1

        #Init Classes
        self.core_process = ribbon()

    def initUI(self,parent=None):
        #UI
        encoding = sys.getfilesystemencoding()
        self.loc= os.path.dirname(unicode(__file__, encoding))
        resources=self.loc.replace("python", "resources")
        self.uiFileName = resources + '/ollin-ribbon-tool.ui'
    
        self._init_maya_ui(self.uiFileName, parent)
        self._config_UI_()
        self.show()


    def _init_maya_ui(self, uiFileName, parent=None):
        from pymel import versions
        if versions.current() < 201700:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        uifile = QtCore.QFile(self.uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)

        #Maya main window
        self.mayaWin = OMUI.MQtUtil.mainWindow() 
        self.mayaWin = shiboken2.wrapInstance(long(self.mayaWin), QtWidgets.QWidget)
        self.ui = self.loader.load(uifile, self.mayaWin)
        uifile.close()


    def _config_UI_(self):
        #curve/surface
        self.ui.curve_rb.isChecked()
        self.ui.curve_rb.toggled.connect(self.crvMode) 
        self.ui.surface_rb.toggled.connect(self.surfaceMode)
        # U/V
        self.ui.createU_rb.isChecked()
        self.ui.createU_rb.toggled.connect(self.createU) 
        self.ui.createV_rb.toggled.connect(self.createV)

        #createRibbon
        self.ui.btn_create.clicked.connect(self.createRibbon)


    def crvMode(self):
        self.crv=1

    def surfaceMode(self):
        self.crv=0

    def createU(self):
        self.u=1

    def createV(self):
        self.u=0
       

    def createRibbon(self):
        name= self.ui.name_box.text()
        number=int(self.ui.number_box.text())
        self.core_process.create(name,number, self.crv, self.u)


    def close(self):
        #cerrar venatna
        self.ui.hide()

    def show(self):
        self.ui.show()

