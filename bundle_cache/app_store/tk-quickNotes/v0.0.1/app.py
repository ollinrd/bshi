import os
import sgtk
from tank.util import sgre as re

logger = sgtk.platform.get_logger(__name__)


class OVFXFileTransfer(sgtk.platform.Application):
    """
    This is the :class:`sgtk.platform.Application` subclass that defines the
    top-level publish2 interface.
    """

    def init_app(self):
        """
        Called as the application is being initialized
        """
        tk_quickNotes = self.import_module("tk_quickNotes")

        display_name = self.get_setting("display_name")
        # "Publish Render" ---> publish_render
        command_name = display_name.lower()
        # replace all non alphanumeric characters by '_'
        command_name = re.sub(r"[^0-9a-zA-Z]+", "_", command_name)

        # register command
        s_dialog = tk_quickNotes.ShowDialog()
        cb = lambda: s_dialog.show_dialog(self)
        menu_caption = "%s" % display_name
        menu_options = {
            "short_name": command_name,
            "description": "Quick Notes 2",
            "icon": os.path.join(self.disk_location, "emblem-mail.png")
        }

        self.engine.register_command(menu_caption, cb, menu_options)