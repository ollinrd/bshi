#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sgtk
import os
logger = sgtk.platform.get_logger(__name__)
from sgtk.platform.qt import QtCore, QtGui
import subprocess
import io
from .notes import AppDialog
class ShowDialog():
    def show_dialog(self, app):


        display_name = sgtk.platform.current_bundle().get_setting("display_name")

        # start ui
        self.ui_class = app.engine.show_dialog(display_name, app, AppDialog)
        self.sg = app.shotgun
        self.ui_class.SEND.clicked.connect(self.send)
        self.ui_class.DELE.clicked.connect(self.DELET)
        self.ui_class.update.clicked.connect(self.load_inbox)
        self.user_list = self.get_users()
        self.setTable()
        self.load_inbox()
        self.ui_class.searc.textChanged.connect(self.brws)

    def brws(self):
        to_f = str(self.ui_class.searc.text())
        for x in range(0, self.ui_class.myTable.rowCount()):
            if to_f.lower().replace(' ', '_') in str(self.ui_class.myTable.item(x, 1).text()).lower():
                self.ui_class.myTable.setRowHidden(x, False)
            else:
                self.ui_class.myTable.setRowHidden(x, True)

    def get_users(self):
        user_dicts = self.sg.find('HumanUser', 
                                  [['sg_status_list', 'is', 'act']],
                                  ['name', 'login', 'department'])
        names = []
        self.all_u = {}
        for user in user_dicts:
            if not ('Shotgun' in user['name'] or 'Template' in user['name']):
                try:
                    perm_dep = ['Compositing', 'IT', '3D', 'Supervisors', 'CG',
                                'Leads', 'Roto', 'Editorial', 'Coordination',
                                'Matchmove']
                    if user['department']['name'] in perm_dep:
                        names.append(user['name'].replace(' ', '_').replace('ñ','n'))
                        self.all_u[user['name'].replace(' ', '_').replace('ñ','n')] = user['login']
                except:
                    pass
        os.system("USER=$(whoami)")
        self.user = os.environ["USER"]
        names.sort()
        return names

    def setTable(self):
        self.ui_class.myTable = self.ui_class.tablePeople
        self.ui_class.myTable.header = ['Selected', 'Name']
        self.ui_class.myTable.size = [75, 375, 85, 600]
        self.ui_class.myTable.setColumnCount(len(self.ui_class.myTable.header))
        self.ui_class.myTable.setHorizontalHeaderLabels(self.ui_class.myTable.header)
        self.ui_class.myTable.setSelectionMode(QtGui.QTableView.ExtendedSelection)
        self.ui_class.myTable.setSelectionBehavior(QtGui.QTableView.SelectRows)
        self.ui_class.myTable.setSortingEnabled(1)
        self.ui_class.myTable.setAlternatingRowColors(True)
        indic = 0
        for nas in self.user_list:
            self.ui_class.myTable.insertRow(indic)
            self.ui_class.myTable.setCellWidget(indic, 0, QtGui.QCheckBox())
            self.ui_class.myTable.setItem(indic, 1, QtGui.QTableWidgetItem(str(nas)))
            indic += 1
        self.ui_class.myTable.resizeColumnsToContents()

    def send(self):
        os.system("USER=$(whoami)")
        user = os.environ["USER"]
        full_name = user + '_' + str(self.ui_class.subject.text()).replace(' ', '_')
        # self.showdialog('Please Verify the subject field !!')
        if full_name == '{0}_'.format(user):
            self.showdialog('Please verify the subject field !!')
            return
            '''
            if '' == '{0}'.format(self.message.toPlainText()):
                self.showdialog('Please verify the message field !!')
                return
            '''
        else:
            sel_users = []
            for x in range(0, self.ui_class.myTable.rowCount()):
                if self.ui_class.myTable.cellWidget(x, 0).isChecked():
                    nas = str(self.ui_class.myTable.item(x, 1).text())
                    if nas not in sel_users:
                        sel_users.append(nas)
            if len(sel_users) == 0:
                self.showdialog('Please select at least one user !!')
                return
            for nas in sel_users:
                to_folder = self.all_u[nas]
                script = self.ui_class.message.toPlainText()
                owner_folder = '/nfs/ovfxToolkit/TMP/notes_msn/{0}/'.format(to_folder)
                prefixx = '/nfs/ovfxToolkit/TMP/notes_msn'.format(to_folder)
                os.system('mkdir -p {0}'.format(prefixx))
                os.system('mkdir -p {0}'.format(owner_folder))
                subprocess.call('chmod 777 -R {0}'.format(prefixx), shell=True)
                subprocess.call('chmod 777 -R  {0}'.format(owner_folder), shell=True)
                dictPath = owner_folder + "{0}.txt".format(full_name)

                file = io.open(dictPath, 'w', encoding='utf8')
                file.write(u'{}'.format(script))
                file.close()
                try:
                    import requests
                    payload = {'from': user, 'to': to_folder, 'note': 'note'}
                    requests.get('http://172.16.30.28:8000/client/slackMessage',
                                 params=payload)
                except:
                    pass
            for x in range(0, self.ui_class.myTable.rowCount()):
                self.ui_class.myTable.cellWidget(x, 0).setChecked(False)
            self.showdialog('Note sent successfully !!')
            self.ui_class.message.setText('')
            self.ui_class.subject.setText('')
            self.ui_class.searc.setText('')

    def load_inbox(self):
        os.system("USER=$(whoami)")
        user = os.environ["USER"]
        self.ui_class.tableInbox.clear()
        self.ui_class.tableInbox.setRowCount(0)
        self.ui_class.tableInbox.setColumnCount(5)
        self.ui_class.tableInbox.setHorizontalHeaderLabels(['check', 'from',
                                                   'subject', '', ''])
        owner_folder = '/nfs/ovfxToolkit/TMP/notes_msn/{0}/'.format(user)
        try:
            arch = os.listdir(owner_folder)
        except:
            os.system('mkdir -p {0}'.format(owner_folder))
            os.system('chmod 777 {0}'.format(owner_folder))
            arch = os.listdir(owner_folder)
        arch_ls = ''
        indic = 0
        self.ui_class.tableInbox.hideColumn(4)
        for i in arch:
            arch_ls += ' ' + i

            self.ui_class.tableInbox.insertRow(indic)
            chb = QtGui.QCheckBox()
            self.ui_class.tableInbox.setCellWidget(indic, 0, chb)
            subj = i.replace(i.split('_')[0] + '_', '').replace('.txt', '')
            boton = QtGui.QPushButton('open')
            boton.clicked.connect(lambda: self.OPEN(boton))
            self.ui_class.tableInbox.setItem(indic, 1,
                                    QtGui.QTableWidgetItem(str(i.split('_')[0])))
            self.ui_class.tableInbox.setItem(indic, 2,
                                    QtGui.QTableWidgetItem(str(subj)))
            self.ui_class.tableInbox.setCellWidget(indic, 3,
                                          boton)
            self.ui_class.tableInbox.setItem(indic, 4,
                                    QtGui.QTableWidgetItem(str(owner_folder + i)))
            indic += 1
        self.ui_class.tableInbox.resizeColumnsToContents()

    def OPEN(self,pos):
        index = self.ui_class.tableInbox.indexAt(pos.pos())
        row = index.row()
        subject = str(self.ui_class.tableInbox.item(row, 2).text())
        note_path = str(self.ui_class.tableInbox.item(row, 4).text())
        # note_file = open(note_path, "r")
        note_file = io.open(note_path, 'r', encoding='utf8')
        note_content = note_file.read()
        note_file.close()
        message = QtGui.QTextEdit(note_content)
        layout = QtGui.QVBoxLayout()
        but = QtGui.QPushButton('CLOSE')
        but.clicked.connect(self.CLOSE)
        layout.addWidget(message)
        layout.addWidget(but)
        widget = QtGui.QWidget()
        widget.setLayout(layout)
        self.ui_class.tabWidget.addTab(widget, subject)

    def CLOSE(self):
        currentIndex = self.ui_class.tabWidget.currentIndex()
        self.ui_class.tabWidget.removeTab(currentIndex)

    def DELET(self):
        paths_to_del = []
        for x in range(0, self.ui_class.tableInbox.rowCount()):
            if self.ui_class.tableInbox.cellWidget(x, 0).isChecked():
                file_d = str(self.ui_class.tableInbox.item(x, 4).text())
                if not file_d == '' and not file_d == '/' and '.txt' in file_d:
                    paths_to_del.append(file_d)
        if '' not in paths_to_del:
            for i in paths_to_del:
                os.remove(i)
        self.showdialog('Messages deleted successfully !!')
        self.load_inbox()

    def showdialog(self, err):
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Information)
        msg.setText("{0}".format(err))
        msg.setWindowTitle("INFO")
        msg.setStandardButtons(QtGui.QMessageBox.Ok)
        msg.exec_()
