# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/ecampos/SGTKs/bshi/repo_bshi/hooks/quick_notes/views/notes.ui'
#
# Created: Thu Sep 17 13:41:58 2020
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

import sgtk
import sys
import os
from sgtk.platform.qt import QtCore, QtGui

logger = sgtk.platform.get_logger(__name__)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class AppDialog(QtGui.QTabWidget):
    def __init__(self, parent=None):
        super(AppDialog, self).__init__()

        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.tabWidget = QtGui.QTabWidget(self)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.tab)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_2 = QtGui.QLabel(self.tab)
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout_3.addWidget(self.label_2)
        self.line_4 = QtGui.QFrame(self.tab)
        self.line_4.setFrameShape(QtGui.QFrame.HLine)
        self.line_4.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_4.setObjectName(_fromUtf8("line_4"))
        self.verticalLayout_3.addWidget(self.line_4)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_5 = QtGui.QLabel(self.tab)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout.addWidget(self.label_5)
        self.subject = QtGui.QLineEdit(self.tab)
        self.subject.setObjectName(_fromUtf8("subject"))
        self.horizontalLayout.addWidget(self.subject)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.label = QtGui.QLabel(self.tab)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_2.addWidget(self.label)
        self.message = QtGui.QTextEdit(self.tab)
        self.message.setObjectName(_fromUtf8("message"))
        self.verticalLayout_2.addWidget(self.message)
        self.line_3 = QtGui.QFrame(self.tab)
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.verticalLayout_2.addWidget(self.line_3)
        self.label_3 = QtGui.QLabel(self.tab)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout_2.addWidget(self.label_3)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_7 = QtGui.QLabel(self.tab)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.horizontalLayout_4.addWidget(self.label_7)
        self.searc = QtGui.QLineEdit(self.tab)
        self.searc.setObjectName(_fromUtf8("searc"))
        self.horizontalLayout_4.addWidget(self.searc)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.tablePeople = QtGui.QTableWidget(self.tab)
        self.tablePeople.setObjectName(_fromUtf8("tablePeople"))
        self.tablePeople.setColumnCount(0)
        self.tablePeople.setRowCount(0)
        self.verticalLayout_2.addWidget(self.tablePeople)
        self.SEND = QtGui.QPushButton(self.tab)
        self.SEND.setObjectName(_fromUtf8("SEND"))
        self.verticalLayout_2.addWidget(self.SEND)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.verticalLayout_3.addLayout(self.verticalLayout_2)
        self.line = QtGui.QFrame(self.tab)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout_3.addWidget(self.line)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.tab_2)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.label_6 = QtGui.QLabel(self.tab_2)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.verticalLayout_4.addWidget(self.label_6)
        self.update = QtGui.QPushButton(self.tab_2)
        self.update.setObjectName(_fromUtf8("update"))
        self.verticalLayout_4.addWidget(self.update)
        self.tableInbox = QtGui.QTableWidget(self.tab_2)
        self.tableInbox.setObjectName(_fromUtf8("tableInbox"))
        self.tableInbox.setColumnCount(0)
        self.tableInbox.setRowCount(0)
        self.verticalLayout_4.addWidget(self.tableInbox)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.DELE = QtGui.QPushButton(self.tab_2)
        self.DELE.setObjectName(_fromUtf8("DELE"))
        self.horizontalLayout_2.addWidget(self.DELE)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.verticalLayout.addWidget(self.tabWidget)

        self.retranslateUi()
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        self.setWindowTitle(_translate("Form", "Form", None))
        self.label_2.setText(_translate("Form", "OLLIN QUICK NOTES", None))
        self.label_5.setText(_translate("Form", "Subject:", None))
        self.label.setText(_translate("Form", "Message:", None))
        self.label_3.setText(_translate("Form", "People:", None))
        self.label_7.setText(_translate("Form", "Search:", None))
        self.SEND.setText(_translate("Form", "SEND", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("Form", "MAIN", None))
        self.label_6.setText(_translate("Form", "Inbox:", None))
        self.update.setText(_translate("Form", "UPDATE INBOX", None))
        self.DELE.setText(_translate("Form", "Delete Selected", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("Form", "INBOX", None))

