# -*- coding: utf-8 -*-
# !/usr/bin/env python
# Copyright (c) 2019 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import sgtk
import copy
import os
import subprocess
import tempfile

class Actions(object):
    def __init__(self):
        self.__app = sgtk.platform.current_bundle()

        can_submit = self.__app.execute_hook_method(
            key="submitter_hook", method_name="can_submit", base_class=None,
        )

        if not can_submit:
            raise RuntimeError(
                "Unable to submit a version to Shotgun given the current configuration"
            )
        self.scr_media = """
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append('{0}/install/core/python')
import sgtk
print 'sgtk: ', sgtk
tk_nuke = sgtk.platform.current_engine()
if not tk_nuke:
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    cdm = sgtk.util.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.get_user()
    sgtk.set_authenticated_user(user)
    task_related = {1}
    tk = sgtk.sgtk_from_entity(task_related['type'], task_related['id'])
    ctx = tk.context_from_entity(task_related['type'], task_related['id'])
    sg = tk.shotgun
    tk_nuke = sgtk.platform.start_engine("tk-nuke", tk, ctx)
print 'tk_nuke:', tk_nuke
review_submission_app = tk_nuke.apps.get("tk-multi-reviewsubmission")
review_submission_app.execute_hook_method(key="render_media_hook",
                                          method_name="render",
                                          base_class=None,**{2})
            """
        self.scr_upload = """
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append('{0}/install/core/python')
import sgtk
print 'sgtk: ', sgtk
tk_nuke = sgtk.platform.current_engine()
if not tk_nuke:
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    cdm = sgtk.util.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.get_user()
    sgtk.set_authenticated_user(user)
    task_related = {1}
    tk = sgtk.sgtk_from_entity(task_related['type'], task_related['id'])
    ctx = tk.context_from_entity(task_related['type'], task_related['id'])
    sg = tk.shotgun
    tk_nuke = sgtk.platform.start_engine("tk-nuke", tk, ctx)
print 'tk_nuke:', tk_nuke
review_submission_app = tk_nuke.apps.get("tk-multi-reviewsubmission")
review_submission_app.execute_hook_method(key="submitter_hook",
                                          method_name="submit_version",
                                          base_class=None,
                                            **{2}
                                        )
            """
    def render_and_submit_version(
        self,
        template=None,
        fields=None,
        first_frame=None,
        last_frame=None,
        sg_publishes=None,
        sg_task=None,
        comment=None,
        thumbnail_path=None,
        progress_cb=None,
        color_space=None,
    ):
        print 'render and submmit'
        """
        Main application entry point to be called by other applications / hooks.

        :param template:        The template defining the path where frames should be found.
        :param fields:          Dictionary of fields to be used to fill out the template with.
        :param first_frame:     The first frame of the sequence of frames.
        :param last_frame:      The last frame of the sequence of frames.
        :param sg_publishes:    A list of shotgun published file objects to link the publish against.
        :param sg_task:         A Shotgun task object to link against. Can be None.
        :param comment:         A description to add to the Version in Shotgun.
        :param thumbnail_path:  The path to a thumbnail to use for the version when the movie isn't
                                being uploaded to Shotgun (this is set in the config)
        :param progress_cb:     A callback to report progress with.
        :param color_space:     The colorspace of the rendered frames

        :returns:               The Version Shotgun entity dictionary that was created.
        :rtype:                 dict
        """

        # Wrap the method so we don't have to worry about process_cb being None
        def dispatch_progress(*args):
            if progress_cb:
                progress_cb(*args)

        dispatch_progress(20, "Building the rendering options dictionary")
        job_id = None
        if not fields:
            fields = {}
        if 'job_id' in fields:
            job_id = fields['job_id']
        if not sg_publishes:
            sg_publishes = []

        dispatch_progress(10, "Preparing")

        if template:
            for key_name in [
                key.name
                for key in template.keys.values()
                if isinstance(key, sgtk.templatekey.SequenceKey)
            ]:
                fields[key_name] = "FORMAT: %d"

            # Get our input_path for frames to convert to movie
            input_path = template.apply_fields(fields)
        else:
            input_path = None

        # Make sure we don't overwrite the caller's fields
        fields = copy.copy(fields)

        width = self.__app.get_setting("movie_width")
        height = self.__app.get_setting("movie_height")
        fields["width"] = width
        fields["height"] = height
        ctx = self.__app.context
        first_frame = ctx.sg_head_in
        last_frame = ctx.sg_tail_out
        # Get an output path for the movie.
        output_path_template = self.__app.get_template("movie_path_template")

        if output_path_template:
            output_path = output_path_template.apply_fields(fields)
        else:
            output_path = None

        render_media_hook_args = {
            "input_path": input_path,
            "output_path": output_path,
            "width": width,
            "height": height,
            "first_frame": first_frame,
            "last_frame": last_frame,
            "version": fields.get("version", 0),
            "name": fields.get("name", "Unnamed"),
            "color_space": color_space,
            "description": comment,
            "task": sg_task
        }

        submit_hook_args = {
            "path_to_frames": input_path,
            "path_to_movie": output_path,
            "thumbnail_path": thumbnail_path,
            "sg_publishes": sg_publishes,
            "sg_task": sg_task,
            "description": comment,
            "first_frame": first_frame,
            "last_frame": last_frame,
        }

        dispatch_progress(20, "Executing the pre-rende hook")

        self.__app.execute_hook_method(
            key="render_media_hook",
            method_name="pre_render",
            base_class=None,
            **render_media_hook_args
        )
        dispatch_progress(30, "Executing the render hook")
        '''
        try:
            output_path = self.__app.execute_hook_method(
                key="render_media_hook",
                method_name="render",
                base_class=None,
                **render_media_hook_args
            )
            print output_path
        except Exception as e:
            print str(e)
        '''
        # finally:
        dispatch_progress(40, "Executing the post-render hook")
        engine = sgtk.platform.current_engine()
        if 'tk-nuke' in str(engine) and job_id:
            task = engine.context.to_dict()['task']
            python_path = engine.context.tank.pipeline_configuration.get_path()
            import nuke
            self.scr_media = self.scr_media.format(python_path, task, render_media_hook_args)
            scene_path = nuke.root().name().replace("/", os.path.sep)
            prefix = '/nfs/ovfxToolkit/TMP/'
            (fd, python_script_path) = tempfile.mkstemp(suffix='.py',
                                                        prefix=prefix)
            open_python_file = open(python_script_path, "w")
            open_python_file.write(self.scr_media)
            open_python_file.close()
            import sys
            if '/nfs/ovfxToolkit/Resources/site-packages' not in sys.path:
                sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
            import yaml
            proj = engine.context.to_dict()['project']
            tk = sgtk.sgtk_from_entity('Project', proj['id'])
            self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
            p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                          "project_specs.yml")
            with open(p_specs_path, 'r') as stream:
                try:
                    self.specs = yaml.load(stream)
                except yaml.YAMLError as exc:
                    print(exc)
            nuke_version = self.specs['main_nuke_path']
            nuke_cmd = " -SubmitCommandLineJob -executable"
            nuke_cmd += " {0} -arguments ".format(nuke_version)
            nuke_cmd += ' "-t ' + python_script_path + ' ' + scene_path + ' ' + python_path + ' "'
            nuke_cmd += ' -frames 1 -pool publish'
            nuke_cmd += ' -name "Publish media process" '
            b_name = os.path.basename(scene_path).split('.')[0]
            nuke_cmd += " -prop BatchName={0}".format(b_name)
            nuke_cmd += ' -prop JobDependencies=' + str(job_id)
            print nuke_cmd
            subprocess.call('chmod -R 777 ' + python_script_path, shell = True)
            deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
            print deadline + " " + nuke_cmd
            process = subprocess.Popen(deadline + " " + nuke_cmd,
                                       shell=True, stdout=subprocess.PIPE)
            out, err = process.communicate()
            if 'error' in str(err).lower() or 'error' in str(out).lower():
                process = subprocess.Popen(deadline + " " + nuke_cmd,
                                           shell=True, stdout=subprocess.PIPE)
                out, err = process.communicate()
            print out
            job_id = out.split("JobID=")[-1].split("The job")[0].strip()

            #UPLOAD JOB
            self.scr_upload = self.scr_upload.format(python_path, task, submit_hook_args)
            (fd, python_script_path) = tempfile.mkstemp(suffix='.py',
                                                        prefix=prefix)
            open_python_file = open(python_script_path, "w")
            open_python_file.write(self.scr_upload)
            open_python_file.close()
            nuke_cmd = " -SubmitCommandLineJob -executable"
            nuke_cmd += " /usr/local/Nuke11.3v4/Nuke11.3 -arguments "
            nuke_cmd += ' "-t ' + python_script_path + ' ' + scene_path + ' ' + python_path + ' "'
            nuke_cmd += ' -frames 1 -pool publish'
            nuke_cmd += ' -name "Upload process" '
            b_name = os.path.basename(scene_path).split('.')[0]
            nuke_cmd += " -prop BatchName={0}".format(b_name)
            nuke_cmd += ' -prop JobDependencies=' + str(job_id)
            print nuke_cmd
            subprocess.call('chmod -R 777 ' + python_script_path, shell = True)
            deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
            print deadline + " " + nuke_cmd
            process = subprocess.Popen(deadline + " " + nuke_cmd,
                                       shell=True, stdout=subprocess.PIPE)
            out, err = process.communicate()
            if 'error' in str(err).lower() or 'error' in str(out).lower():
                process = subprocess.Popen(deadline + " " + nuke_cmd,
                                           shell=True, stdout=subprocess.PIPE)
                out, err = process.communicate()
            print out
            job_id = out.split("JobID=")[-1].split("The job")[0].strip()
            version = None

        else:
            self.__app.execute_hook_method(
                key="render_media_hook",
                method_name="post_render",
                base_class=None,
                **render_media_hook_args
            )
            dispatch_progress(50, "Creating Shotgun Version and uploading movie")


            version = self.__app.execute_hook_method(
                key="submitter_hook",
                method_name="submit_version",
                base_class=None,
                **submit_hook_args
            )

        # Log metrics for this app's usage
        try:
            self.__app.log_metric("Render & Submit Version", log_version=True)
        except Exception:
            # ingore any errors. ex: metrics logging not supported
            pass

        return version






