import sgtk
import nuke
import os


class ScriptCleaner(sgtk.platform.Application):
    """
    App that creates a Nuke node that sends items to Shotgun for review.
    """

    def init_app(self):
        """
        Called as the application is being initialized
        """
        # assign this app to nuke handle so that the gizmo finds it
        #nuke.tk_nuke_quickreview = self

        # add to nuke node menu
        icon = os.path.join(self.disk_location, "resources", "cleaner_icon.png")

        #Validation to avoid load in nuke that runs without ui
        valid = True
        self.load = False
        for arg in nuke.rawArgs: 
            if arg == "-t":
                valid = False
        if valid:
            self.engine.register_command(
                "Script Cleaner",
                self.connectIntoCore,
                {"type": "script", "icon": icon}
            )

    def connectIntoCore(self):
        if not self.load:
            mod_scriptCleanrer_core = self.import_module('tk_nuke_scriptcleaner_core')
            mod_scriptCleanrer_bridge = self.import_module('tk_nuke_scriptcleaner_bridge')

            self.core = mod_scriptCleanrer_core.CoreFunc()
            self.bridge = mod_scriptCleanrer_bridge.BridgeFunc()

            self.bridge.setController(
                core = self.core,
                loc = self.disk_location)
            self.load = True

        self.core._saveInitialSelection()
        self.bridge.showUI()

