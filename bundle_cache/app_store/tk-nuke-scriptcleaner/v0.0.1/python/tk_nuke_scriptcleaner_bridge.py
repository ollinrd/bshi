import nuke
import sgtk
import os
from sgtk.platform.qt import QtCore, QtGui
import sys

class BridgeFunc():
    def __init__(self):
        """
        """
        self.allMarked = [False, False]
        self.posibleStatus = ["Clear", "Problems", "Warning"]

    def setController(self, core, loc, parent = None):
        self.core = core
        mainWinFile = os.path.join(
            loc, 'resources', 'mainUI.ui')

        nukeV_ = nuke.NUKE_VERSION_STRING
        nukeV = nukeV_.split("v")[0]
        if float(nukeV)<11:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        uifile = QtCore.QFile(str(mainWinFile))
        uifile.open(QtCore.QFile.ReadOnly)
        self.mainUI = self.loader.load(uifile, parent)
        uifile.close()

        self.connectButtons() 
        self.loadCheckboxes()

    def connectButtons(self):
        self.mainUI.buttonBox.rejected.connect(self._rejected)
        self.mainUI.btn_compSelect.clicked.connect(self.selComp)
        self.mainUI.btn_rotoSelect.clicked.connect(self.selRoto)
        self.mainUI.btn_check.clicked.connect(self._check)
        self.mainUI.btn_clear_comp.clicked.connect(self._clear_Comp)
        self.mainUI.btn_clear_roto.clicked.connect(self._clear_Roto)

    def selComp(self):
        # Select button comp using the same base
        # to update all marks
        self.updateMarks(tab = 0)

    def selRoto(self):
        # Select button roto using the same base
        # to update all marks
        self.updateMarks(tab = 1)

    def updateMarks(self, tab):
        currentBox = None
        if tab == 0:
            currentBox = self.checksComp
        else:
            currentBox = self.checksRoto

        for key, value in currentBox.iteritems():
            value[0].setChecked(not self.allMarked[tab])

        self.allMarked[tab] = not self.allMarked[tab]

        self.updateSelectButton()

    def updateSelectButton(self):
        if self.allMarked[0]:
            self.mainUI.btn_compSelect.setText("Clear select")
        else:
            self.mainUI.btn_compSelect.setText("Select all")

        if self.allMarked[1]:
            self.mainUI.btn_rotoSelect.setText("Clear select")
        else:
            self.mainUI.btn_rotoSelect.setText("Select all")

    def loadCheckboxes(self):
        self.checksComp = []
        self.checksRoto = []

        # Comp
        self.checksComp.append(('gizmos', [
                self.mainUI.chk_gizmos, self.mainUI.lbl_gizmos
                ]))
        self.checksComp.append(('nonConnected',[
                self.mainUI.chk_NonConnected, self.mainUI.lbl_nonConnected
                ]))
        self.checksComp.append(('plugins',[
                self.mainUI.chk_plugins, self.mainUI.lbl_plugins
                ]))
        self.checksComp.append(('writes',[
                self.mainUI.chk_multipleWrites, self.mainUI.lbl_multipleWrites
                ]))
        self.checksComp.append(('repeatedReads',[
                self.mainUI.chk_repeatedReads, self.mainUI.lbl_repeatedReads
                ]))
        self.checksComp.append(('unnecessary',[
                self.mainUI.chk_unnecesaryProcess, self.mainUI.lbl_unnecesarryProcess
                ]))
        self.checksComp = dict(self.checksComp)

        # Roto
        self.checksRoto.append(('gizmos', [
                self.mainUI.chk_Rgizmos, self.mainUI.lbl_gizmos_2
                ]))
        self.checksRoto.append(('plugins', [
                self.mainUI.chk_Rplugins, self.mainUI.lbl_plugins_2
                ]))
        self.checksRoto.append(('repeatedReads', [
                self.mainUI.chk_RrepeatedReads, self.mainUI.lbl_repeatedReads_2
                ]))

        self.checksRoto.append(('unnecessary', [
                self.mainUI.chk_RunnecesaryProcess, self.mainUI.lbl_unnecesarryProcess_2
                ]))
        self.checksRoto = dict(self.checksRoto)

    def loadFeedback(self):
        """
        """

    def showUI(self):
        text = 'Check'
        #for chk in self.checksComp:
        for key, value in self.checksComp.iteritems():
            value[1].setStyleSheet('color: rgb(100, 100, 100);')
            value[0].setText(text)

        #for chk in self.checksRoto:
        for key, value in self.checksRoto.iteritems():
            value[1].setStyleSheet('color: rgb(100, 100, 100);')
            value[0].setText(text)

        self.updateSelectButton()

        self.mainUI.show()   

    def _check(self):
        """
        """
        activeTab = self.mainUI.tabWidget.currentIndex()

        if activeTab == 0:
            self._checkComp(False)
        if activeTab == 1:
            self._checkRoto(False)

    def _checkComp(self, fix):
        step = 'Comp'
        globalFeed = ''
        # Area to only check all related with comp area
        if self.checksComp['gizmos'][0].isChecked():
            feed =self.core._process_Gizmos(fix = fix)
            self.setStatus(feed[1], self.checksComp['gizmos'], step)
            globalFeed = globalFeed + feed[0]

        if self.checksComp['nonConnected'][0].isChecked():
            feed =self.core._process_nonConnected(fix = fix)
            self.setStatus(feed[1], self.checksComp['nonConnected'], step)
            globalFeed = globalFeed + feed[0]

        if self.checksComp['plugins'][0].isChecked():
            feed =self.core._process_plugins(fix = fix)
            self.setStatus(feed[1], self.checksComp['plugins'], step)
            globalFeed = globalFeed + feed[0]

        if self.checksComp['writes'][0].isChecked():
            feed =self.core._process_writes(fix = fix)
            self.setStatus(feed[1], self.checksComp['writes'], step)
            globalFeed = globalFeed + feed[0]

        if self.checksComp['repeatedReads'][0].isChecked():
            feed =self.core._process_repeatedReads(fix = fix)
            self.setStatus(feed[1], self.checksComp['repeatedReads'], step)
            globalFeed = globalFeed + feed[0]

        if self.checksComp['unnecessary'][0].isChecked():
            feed =self.core._process_unnecessaryNodes(fix = fix)
            self.setStatus(feed[1], self.checksComp['unnecessary'], step)
            globalFeed = globalFeed + feed[0]

        self.mainUI.lbl_feedback.setText(globalFeed)

    def _checkRoto(self, fix):
        step = 'Roto'
        globalFeed = ''

        # Area to only check all related with roto area
        if self.checksRoto['gizmos'][0].isChecked():
            feed =self.core._process_Gizmos(fix = fix)
            self.setStatus(feed[1], self.checksRoto['gizmos'], step)
            globalFeed = globalFeed + feed[0]

        if self.checksRoto['plugins'][0].isChecked():
            feed =self.core._process_plugins(fix = fix)
            self.setStatus(feed[1], self.checksRoto['plugins'], step)
            globalFeed = globalFeed + feed[0]

        if self.checksRoto['repeatedReads'][0].isChecked():
            feed =self.core._process_repeatedReads(fix = fix)
            self.setStatus(feed[1], self.checksRoto['repeatedReads'], step)
            globalFeed = globalFeed + feed[0]
            
        if self.checksRoto['unnecessary'][0].isChecked():
            feed =self.core._process_unnecessaryNodes(fix = fix)
            self.setStatus(feed[1], self.checksRoto['unnecessary'], step)
            globalFeed = globalFeed + feed[0]

        lbl_feedback.setText(globalFeed)

    def _clear_Comp(self):
        """
        """
        #print "Bridge Method _clear_Comp"
        self._checkComp(True)

    def _clear_Roto(self):
        """
        """
        #print "Bridge Method _clear_Roto"
        self._checkComp(True)

    def setStatus(self, status, info, step):
        info[0].setText('')
        info[0].setText(status)
        color = 'color: rgb(20, 180, 20);'
        
        #Clear
        if status == self.posibleStatus[1]:
            color = 'color: rgb(180, 20, 20);'

        if status == self.posibleStatus[2]:
            color = 'color: rgb(244, 66, 36);'
        
        activeTab = self.mainUI.tabWidget.currentIndex()
        if activeTab == 0:
            self.mainUI.btn_clear_comp.setEnabled(True)
        else:
            self.mainUI.btn_clear_roto.setEnabled(True)

        info[1].setStyleSheet(color)

    def _rejected(self):
        """
        """
        print "_rejected"




