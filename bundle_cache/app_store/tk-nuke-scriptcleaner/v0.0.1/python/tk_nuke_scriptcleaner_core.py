import nuke
import uuid
import os

class CoreFunc():
    def __init__(self):
        """
        """
        self.posibleStatus = ["Clear", "Problems", "Warning"]
        self.rootTreeSearch = [] 

    def _saveInitialSelection(self):
        # Initial selection
        self.initialSelection = nuke.selectedNodes()

    def _getImportantNodes(self):
        """
        """
        readNs = []
        writeNs = []
        allWriteNs = []
        gizmoNs = []
        writeNsSG = []
        writeNsSGAll = []
        for n in nuke.allNodes():
            if n.Class() == 'Read':
                readNs.append(n)
            if n.Class() == 'Write':
                allWriteNs.append(n)
                if n['disable'].getValue() == 0.0:
                    writeNs.append(n)
            if n.Class() == 'WriteTank':
                writeNsSGAll.append(n)
                if n['disable'].getValue() == 0.0:
                    writeNsSG.append(n)
            
            #if (n.Class() + ".gizmo") in nuke.plugins(nuke.ALL | nuke.NODIR):
            #   if not n.Class() in ["Grain2","Tracker4"]:
            #       gizmoNs.append(n)
            allowed_gizmo_classes = ["Grain2", "Tracker4", "WriteTank", 'Viewer']
            for knob in n.knobs():
                if 'gizmo' in knob and n.Class() not in allowed_gizmo_classes:
                    gizmoNs.append(n)

        importantNs = {
            "Writes": writeNs,
            "AllWrites": allWriteNs,
            "Reads": readNs,
            "Gizmos":gizmoNs,
            "WriteTank":writeNsSG,
            "AllWriteTank": writeNsSGAll
        }

        return importantNs

    def _getWriteReadConnections(self, dictNodes):
        writes = dictNodes["Writes"]
        reads = dictNodes["Reads"]

        relations = []

        for w in writes:
            w_file = w.knob('file').getValue()
            #print w.name()
            for r in reads:
                if w_file == r.knob('file').getValue():
                    relations.append((r,w))

        #if len(relations) is not 0:
        #   print relations
        #else:
        #   print "No relations"

        return relations

    def _getDisconnectedNodes(self):
        
        evaluateNodes = []
        exps = []
        lNs = nuke.allNodes()
        index = 0
        for n in lNs:
            evaluateNodes.append(n)
            for k in n.knobs():
                if n.knob(k).hasExpression(index):
                    exps.append(n.knob(k).animation(index).expression() )

        # Get only nodes without child
        for n in lNs:
            for i in n.dependencies():
                if i in evaluateNodes:
                    evaluateNodes.remove(i)
            if n.dependencies() != []:
                if n in evaluateNodes:
                    evaluateNodes.remove(n)

        disN = []
        for n in evaluateNodes:
            # Inputs    
            if n.input(0) == None:
                typeN = n.Class()
                valid_types = [
                    'BackdropNode', 'StickyNote']
                if not typeN in valid_types:
                    vald = True
                    for ep in exps:
                        # Verify if the node is not asociated with any expresion
                        if str(ep).find(n.name()) is not -1:
                            vald = False
                    if vald:
                        disN.append(n)

        #if len(disN) > 0:
        #   print "No connected nodes"
        #   for al in disN:
        #       print "   ", al.name(), " ", al.Class()
        #else:
        #   print "No disconnected nodes"   

        return disN 
        
    def _searchRoot(self, n):
        """
        Recursive function to pop elements
        with parents
        """
        #toReturn = [n]
        self.rootTreeSearch.append(n)
        for newN in n.dependencies():
            if not newN in self.rootTreeSearch:
                self._searchRoot(n = newN)
        return self.rootTreeSearch

    def _getAllConnectedNodes(self, originalNodes):
        self.rootTreeSearch = []
        connected = self._searchRoot(originalNodes)
        valid_types = ['BackdropNode', 'StickyNote']
        for dN in nuke.allNodes():
            if dN.Class() == "Group" and dN.name().find("LUT") is not -1:
                connected.append(dN)
            else:
                if dN.Class() in valid_types:
                    connected.append(dN)
        return connected

    def deselect_all_nodes(self):
        """De-select all nodes"""

        for i in nuke.allNodes(recurseGroups=True):
            i.knob('selected').setValue(False)

    def convert_gizmo_to_group(self, gizmo):
        """Convert given gizmo (gizmo.fullName) to group"""
        
        #gizmo = nuke.toNode(gizmo_full_name)

        inputs = []
        for x in range(0, gizmo.maximumInputs()):
            if gizmo.input(x):
                inputs.append(gizmo.input(x))
            else:
                inputs.append(False)

        original_name = gizmo.knob('name').value()
        xpos = gizmo.xpos()
        ypos = gizmo.ypos()
        uid_name = uuid.uuid4()

        gizmo.knob('name').setValue('%s' % uid_name)
        self.deselect_all_nodes()
        gizmo.knob('selected').setValue(True)

        with gizmo:
            new_group = gizmo.makeGroup()

            self.deselect_all_nodes()
            nuke.delete(gizmo)

            newName = self._getValidName(name = original_name + "_grp")
            new_group.knob('name').setValue(newName)
            new_group['xpos'].setValue(xpos)
            new_group['ypos'].setValue(ypos)

            for x in range(0, new_group.maximumInputs()):
                new_group.setInput(x, None)
                if inputs[x]:
                    new_group.connectInput(x, inputs[x])

    def _getValidName(self, name, num = 1):
        valid = True
        for n in nuke.allNodes():
            if name + "_"+str(num) == n.name():
                valid = False

        if valid:
            return name+"_"+str(num)
        else:
            return self._getValidName(name = name, num = num+1)

    def getSubGizmos(self, subNs = None, baseNode = None):
        if subNs == None:
            subNs = []
        allowed_gizmo_classes = ["Grain2", "Tracker4", 'WriteTank', 'Viewer']

        if baseNode == None:
            for n in nuke.allNodes():
                if n.Class() == "Group":
                    for subN in n.nodes():
                        if subN.Class() == "Group":
                            subsubN = [subNs.append(subsubN) for subsubN in self.getSubGizmos(baseNode = subN)]
                        else:
                            for knob in subN.knobs():
                                if 'gizmo' in knob and subN.Class() not in allowed_gizmo_classes:
                                    subNs.append(subN)
        else:
            for subN in baseNode.nodes():
                if subN.Class() == "Group":
                    subsubN = [subNs.append(subsubN) for subsubN in self.getSubGizmos(baseNode = subN)]
                else:
                    for knob in subN.knobs():
                        if 'gizmo' in knob and subN.Class() not in allowed_gizmo_classes:
                            subNs.append(subN)

        return subNs

    def clearSubGizmos(self):
        subGizmos = self.getSubGizmos()

        if len(subGizmos) != 0:
            clearAll = False
            for gi in subGizmos:
                #print "Gizmo in subgroup converted: ", gi.name()
                self.convert_gizmo_to_group(gizmo = gi)
            self.clearSubGizmos()

    def _getPlugins(self):
        """
        """
        import os
        nukeDir = os.path.dirname(nuke.env['ExecutablePath'])
        pluginsRel = [[p, p.split('/')[-1]]for p in nuke.plugins() if p.find('LD_3DE_Classic_LD_Model') is not -1]

        scriptPlugins = []
        for n in nuke.allNodes():
            for pl in pluginsRel:
                if n.Class()+'.so' == pl[1]:
                    exist = False
                    for sP in scriptPlugins:
                        if sP[0] == pl[0]:
                            sP[1].append(n)
                            exist = True
                    if not exist:
                        scriptPlugins.append( (pl[0] , [n]))

        # Modify to get external folder plugins
        """
        for n in nuke.allNodes():
            #if (n.Class() + '.so')in nuke.plugins(nuke.ALL | nuke.NODIR):
            if (n.Class() + '.so')in nuke.plugins(nuke.NODIR):
                print n.Class()

        """

        return scriptPlugins

    def _process_Gizmos(self, fix):
        """
        """
        rGizmos = []
        initialSelection = nuke.selectedNodes()
        baseGizmos = self._getImportantNodes()['Gizmos']
        thisdir = "/nfs/ovfxToolkit/Dev/nukeGizmos"
        status = self.posibleStatus[0]

        feedback = ""

        # Organize gizmos into list of repeated nodes
        #for gn in nodes:
        for gn in baseGizmos:
            found = False
            for rG in rGizmos:
                if gn.Class() == rG[0]:
                    found = True
                    rG[1].append(gn.name())
            if not found:
                rGizmos.append((gn.Class(), [gn.name()])) 
        
        # Building feedback message
        if len(rGizmos)>0:
            feedback = "Gizmos info"
            status = self.posibleStatus[1]
        for rG in rGizmos:
            feedback = feedback + "\n\t" + rG[0]
            for rGsub in rG[1]:
                feedback = feedback +"\n\t\t"+rGsub

        if fix:
            for rG in baseGizmos:
                self.convert_gizmo_to_group(rG)

        # Sub gizmos
        if fix:
            self.clearSubGizmos()
            status = self.posibleStatus[0]
        else:
            subG = self.getSubGizmos()
            if len(subG)>0:
                status = self.posibleStatus[1]
                feedback = feedback + "\n\nSubGizmos"
                for sG in subG:
                    feedback = "{0}\n\t{1}".format(feedback, sG.name() )

        for n in initialSelection:
            n['selected'].setValue(True)

        return [feedback, status]

    def _process_nonConnected(self, fix):
        feedback = ''
        status = self.posibleStatus[0]

        valid_types = ['BackdropNode', 'StickyNote', 'Viewer']
        disNodes = [ dN for dN in self._getDisconnectedNodes() if not dN.Class() in valid_types ]
        if len(disNodes)>0:
            feedback = "\n\nDisconected nodes"
            for dN in disNodes:
                if not (dN.Class() == "Group" and "LUT" in dN.name()):
                    if fix:
                        feedback = feedback + "\tDeleted: "+dN.name()
                        nuke.delete(dN)
                    else:
                        feedback = feedback + "\t{0}".format(dN.name())
                        status = self.posibleStatus[1]

        return [feedback, status]


    def _process_plugins(self, fix):
        feedback = ''
        status = self.posibleStatus[0]

        # Plugins validation
        plugins_ext = self._getPlugins()
        if len(plugins_ext) != 0:
            status = self.posibleStatus[2]
            feedback = "Plugins founded"
        for px in plugins_ext:
            feedback = feedback + '\n\t' + px[0]
            for subPX in px[1]:
                feedback = feedback + '\n\t\t' +subPX.name()

        return [feedback, status]

    def _process_writes(self, fix):
        feedback = ''
        status = self.posibleStatus[0]

        importantNs = self._getImportantNodes()

        allWrites = []

        writeNodes = len(importantNs['Writes']) > 0

        #print importantNs['WriteTank']

        #If the script only has one SG write node
        if len(importantNs['WriteTank'])== 1:
            #if the script has also normal write nodes
            if writeNodes:
                feedback = "Write nodes in script"

                # for each write node, the script will be diable them
                for wn in importantNs['Writes']:
                    if fix:
                        feedback = feedback + "\n\tDisabled: " + wn.name()
                        wn['disable'].setValue(True)
                    else:
                        status = self.posibleStatus[1]
                        feedback = feedback + '\n\t' + wn.name()

        else:
            #If the script has more than one write node
            if len(importantNs['WriteTank']) >1:
                if len(self.initialSelection) !=1:
                        #"Mensaje de error, seleccionar solo un nodo"
                        msg = "\n\nTo complete Write nodes clener, select only one write node\n"
                        feedback = feedback + msg
                        status = self.posibleStatus[1]
                else:
                    #"Ya solo hay un nodo seleccionado"
                    n = self.initialSelection[0]
                    if n.Class() != 'WriteTank':
                        msg = "\n\nTo complete Write nodes clener, select only a valid SG write node\n"
                        feedback = feedback + msg
                        status = self.posibleStatus[1]
                    else:
                        feedback = 'SG Write nodes in scipt'
                        for nN in importantNs['WriteTank']:
                            if nN != n:
                                if fix:
                                    feedback = feedback + "\n\tDisabled:" + nN.name()
                                    nN['disable'].setValue(True)
                                else:
                                    feedback = feedback + "\n\t" + nN.name()
                                    status = self.posibleStatus[1]

                        if len(importantNs['Writes']) >0:
                            feedback = feedback + "\n\nWrite nodes in script"
                            for wn in importantNs['Writes']:
                                if fix:
                                    feedback = feedback + "\n\tDisabled: " + wn.name()
                                    wn['disable'].setValue(True)
                                else:
                                    status = self.posibleStatus[1]
                                    feedback = feedback + '\n\t' + wn.name()
            else:
                #If the script only has write nodes
                #"Si no hay write tanks"
                if len(importantNs['Writes']) >0:
                    lenSel = len(self.initialSelection)
                    if lenSel != 1:
                        msg = "\n\nTo complete Write nodes clener, select only one write node\n"
                        feedback = feedback + msg
                        status = self.posibleStatus[1]
                    else:
                        n = self.initialSelection[0]
                        if n.Class() != 'Write':
                            msg = "\n\nTo complete Write nodes clener, select a valid write node\n"
                            feedback = feedback + msg
                            status = self.posibleStatus[1]
                        else:
                            for wn in importantNs['Writes']:
                                if wn != n:
                                    if fix:
                                        feedback = feedback + "\n\tDisabled:",wn.name()
                                        nN['disable'].setValue(True)
                                    else:
                                        feedback = feedback + "\n\t",wn.name()
                                        status = self.posibleStatus[1]      

        return [feedback, status]

    def _process_unnecessaryNodes(self, fix):
        feedback = ''
        status = self.posibleStatus[0]
        #valid_types = ["Grain2", "Tracker4", 'Viewer']
        valid_types = ['Viewer']

        # If everything is connected into one write node... 
        valid = False
        NonDelete = None
        importantNs = self._getImportantNodes()

        if len(importantNs["Writes"]) == 1 and len(importantNs["WriteTank"]) == 0:
            NonDelete = self._getAllConnectedNodes(originalNodes = importantNs["Writes"][0])
            valid = True

        if len(importantNs["Writes"]) == 0 and len(importantNs["WriteTank"]) == 1:
            NonDelete = self._getAllConnectedNodes(originalNodes = importantNs["WriteTank"][0])
            valid = True

        if not valid:
            if len(self.initialSelection) == 1:
                #Validate write tank
                typeN = self.initialSelection[0].Class()
                if typeN == 'Write' or typeN == 'WriteTank':
                    NonDelete = self._getAllConnectedNodes(originalNodes = self.initialSelection[0])
                    valid = True
                else:
                    feedback = "Unnecessary nodes\n\tSelect one write node"

        if valid:
            toDel = [n for n in nuke.allNodes() if (not n.Class() in valid_types) and (not n in NonDelete)]
            if len(toDel) > 0:
                feedback = "Non necessary nodes"

                if not fix:
                    status = self.posibleStatus[1]

                for td in toDel:
                    if fix:
                        feedback = feedback + "\n\tDeleted: " + td.name()
                        nuke.delete(td)
                    else:
                        feedback = feedback + "\n\t" + td.name()        

                # Cleaning empty backdrops
                for td in self._emptyBackdrops():
                    if fix:
                        feedback = feedback + "\n\tDeleted: " + td.name()
                        nuke.delete(td)
                    else:
                        feedback = feedback + "\n\t" + td.name()        


        return [feedback, status]

    def _emptyBackdrops(self):
        """
        Clear all empty backdrops
        """
        emptyBackDrops = []
        for bd in nuke.allNodes('BackdropNode'):
            empty = True

            left = bd.xpos()
            top = bd.ypos()
            right = left + bd['bdwidth'].value()
            bottom = top + bd['bdheight'].value()
            r = (left, right, top, bottom)

            for n in nuke.allNodes():
                #for r in bdRanges:
                cond1 = n.xpos() > r[0]
                cond2 = n.xpos() + n.screenWidth() < r[1]
                cond3 = n.ypos() > r[2]
                cond4 = n.ypos() + n.screenHeight() < r[3]
                if cond1 and cond2 and cond3 and cond4:
                    empty = False

            if empty:
                emptyBackDrops.append(bd)

        return emptyBackDrops

    def _getDownstream(self, node):
        """
        Get all nodes that has as input the 
        'node' given as parameter.
        """
        downDepencies = []
        for n2 in [n for n in nuke.allNodes() if n is not node]:
            [downDepencies.append(n2) for dp in n2.dependencies() if dp == node]
        return downDepencies

    def equalRead(self, read1, read2):
        equal = True

        listknobs = [
            'first', 'last', 'origfirst', 'origlast', 'frame',
            'out_colorspace', 'auto_alpha', 'before', 'after', 'raw']

        for k in listknobs:
            if read1[k].value() != read2[k].value():
                equal = False
                break
            else:
                if k == 'raw' and not read1[k].value() :
                    newK = 'out_colorspace'
                    if read1[newK].value() != read2[newK].value():
                        equal = False
                        break
                if k == 'frame' and read1[k].value() != '':
                    newK = 'frame_mode'
                    if read1[newK].value() != read2[newK].value():
                        equal = False
                        break
        return equal

    def _process_repeatedReads(self, fix):
        feedback = ''
        status = self.posibleStatus[0]
        #  ["Clear", "Problems", "Warning"]
        readsRegister = []

        for r in nuke.allNodes('Read'):
            current = (r['file'].value(), [r])
            found = False
            for reg in readsRegister:
                if current[0] == reg[0]:
                    if len(reg[1]):
                        read = reg[1][0]
                        if self.equalRead(r, read):
                            reg[1].append(r)
                            found = True
            if not found:
                readsRegister.append(current)

        headerProblem = False
        for reg in readsRegister:
            if len(reg[1]) >1:
                topNode = None

                if fix:
                    if not headerProblem:
                        headerProblem = True
                        feedback = feedback + '\nRepeated reads deleted'
                        status = self.posibleStatus[0]
                else:
                    if not headerProblem:
                        headerProblem = True
                        feedback = feedback + '\nRepeated reads detected'
                        status = self.posibleStatus[1]

                for r in reg[1]:
                    if not topNode:
                        topNode = r
                    else:
                        k = 'ypos'
                        if r[k].value() < topNode[k].value():
                            topNode = r
                if topNode:
                    for r in reg[1]:
                        if fix and r!= topNode:
                            newDot = nuke.nodes.Dot()
                            label_text = 'dot_{0}'.format(r.name())
                            newDot['name'].setValue(label_text)
                            newDot['label'].setValue(label_text)
                            newDot.setInput(0, topNode)

                            for k in ['ypos', 'xpos']:
                                newDot[k].setValue(r[k].value())

                            dep = self._getDownstream(r)
                            for p in dep:
                                # Replace input connections 
                                # SG WRITE node, to new Read Node
                                for i in range (0, p.inputs()):
                                    if p.input(i) == r:
                                        p.setInput (i, newDot)

                            feedback = feedback+'\n\t'+r.name()
                            
                            nuke.delete(r)
                        if not fix:
                            feedback = feedback+'\n\t'+r.name()

                else:
                    print 'Check core _process_repeatedNodes'
        return [feedback, status]
        

def mainMethod():
    clearAll = True
    
    ClearOps = CoreFunc()
    importantNs = ClearOps._getImportantNodes()

    # Read of all original elements
    # Begin reads original ---------
    
    rArray = []
    for w in importantNs["Reads"]:
        fileExt = w.knob('file').getValue()
        if fileExt.find("/editorial/publish/elements/") is not -1:
            found = False
            for ra in rArray:
                if ra[0] == fileExt:
                    ra[1].append(w.name())
                    found = True
            if not found:
                rArray.append( (fileExt, [w.name()]) )
    if len(rArray) > 0:
        print "\n*********"
        print " Reads - publish elements"

    for ra in rArray:
        print "\n", ra[0]
        for nn in ra[1]:
            print "\t", nn

    # End reads original ---------

    # All Write Shotgun nodes
    #     Begin shotgun write -------
    if len(importantNs["WriteTank"]) >0:
        print "\n*********"
        print " Shotgun write nodes"
    for sw in importantNs["WriteTank"]:
        print "\t", sw.name()
    #     End shotgun write -------

    # All gizmos nodes 
    #     Begin gizmos ------------
    rGizmos = []
    baseGizmos = []
    for gn in importantNs["Gizmos"]:
        found = False
        baseGizmos.append(gn)
        for rG in rGizmos:
            if gn.Class() == rG[0]:
                found = True
                rG[1].append(gn.name())
        if not found:
            rGizmos.append( (   gn.Class(), [gn.name()]  ) ) 

    import os
    # Getting the current work directory (cwd)
    thisdir = "/nfs/ovfxToolkit/Dev/nukeGizmos"

    if len(rGizmos) > 0:
        print "\n*********"
        print " Gizmos nodes"
        clearAll = False
    for rG in rGizmos:
        print rG[0]
        # r=root, d=directories, f = files
        for r, d, f in os.walk(thisdir):
            for file in f:
                #if rG[0] in file and not "~" in file:
                if rG[0] in file:
                    print(os.path.join(r, file))
                    
        #for x in rG[1]:
        #   print "\t", x
    #     End gizmos --------------

    # Grouping gizmos --------------------------
    if len(baseGizmos) > 0:
        print "\n*********"
        print " Gizmos nodes creating groups"
        clearAll = False

    for rG in baseGizmos:
        print "\t"+rG.name()
        #bypassGroup = False
        #ClearOps.convert_gizmo_to_group(rG)
    # End grouping gizmos ---------------------

    
    # Disconnected Nodes -----------------------
    valid_types = ['BackdropNode', 'StickyNote', 'Viewer']
    disNodes = [ dN for dN in ClearOps._getDisconnectedNodes() if not dN.Class() in valid_types ]
    if len(disNodes)>0:
        print "\n*********"
        print "Disconected nodes"
        clearAll = False
        for dN in disNodes:
            if not (dN.Class() == "Group" and dN.name().find("LUT") is -1):
                print "\tDeleted: ",dN.name()
            #nuke.delete(dN)
    # Disconnected Nodes -----------------------

    #"  Gizmos in subgroups"
    ClearOps.clearSubGizmos()

    # Plugins validation
    plugins_ext = ClearOps._getPlugins()
    if len(plugins_ext) != 0:
        print "\n********"
        print "  Used plugins"
        clearAll = False
    for px in plugins_ext:
        print '\t', px[0]
        for subPX in px[1]:
            print '\t    ', subPX.name()

    # If everything is connected into one write node... 
    valid = False
    NonDelete = None
    if len(importantNs["Writes"]) == 1 and len(importantNs["WriteTank"]) == 0:
        NonDelete = ClearOps._getAllConnectedNodes(originalNodes = importantNs["Writes"][0])
        valid = True

    if len(importantNs["Writes"]) == 0 and len(importantNs["WriteTank"]) == 1:
        NonDelete = ClearOps._getAllConnectedNodes(originalNodes = importantNs["WriteTank"][0])
        valid = True

    if valid:
        toDel = [n for n in nuke.allNodes() if (not n.Class() in valid_types) and (not n in NonDelete)]
        if len(toDel) > 0:
            print "\n***** Unnecessary nodes ******"
            clearAll = False
            for td in toDel:
                #print "\tDeleted: ", td.name()
                print "\t", td.name()
                #nuke.delete(td)
            print "***** End to unnecessary ******"
    
    if clearAll:
        print "\nEverything clear"

#mainMethod()




