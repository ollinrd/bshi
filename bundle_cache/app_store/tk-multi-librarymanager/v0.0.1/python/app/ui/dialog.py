# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog.ui'
#
#      by: pyside-uic 0.2.13 running on PySide 1.1.0
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui
import os
import hou

class MyModel(QtCore.QAbstractTableModel):

    def __init__(self, data=[[]], header=[], image_location=None, parent=None):

        QtCore.QAbstractTableModel.__init__(self, parent)
        self.__data__ = data
        self.__header__ = header
        self.__parent__ = parent
        self._disk_location = image_location

    def rowCount(self, parent):

        return len(self.__data__)

    def columnCount(self, parent):

        return len(self.__data__[0]) - 2

    def data(self, index, role):

        # [name, version, description, status, type, source, autoload]

        if role == QtCore.Qt.DecorationRole:
            # Name column
            if index.column() == 0:
                icon_file = "{}/resources/none.png".format(self._disk_location)

                if self.__data__[index.row()][4].lower() == 'hda':
                    icon_file = "{}/resources/nodeType2b.png".format(self._disk_location)
                pixmap = QtGui.QPixmap()
                pixmap.load(icon_file)
                pixmap = pixmap.scaled(20, 20, aspectRatioMode=QtCore.Qt.KeepAspectRatio,
                                       transformMode=QtCore.Qt.SmoothTransformation)

                return pixmap

            # Source column
            if index.column() == 5:

                if self.__data__[index.row()][5].lower() == 'library':
                    icon_file = "{}/resources/l2.png".format(self._disk_location)
                else:
                    icon_file = "{}/resources/p2.png".format(self._disk_location)
                print icon_file
                pixmap = QtGui.QPixmap()
                pixmap.load(icon_file)
                pixmap = pixmap.scaled(20, 20, aspectRatioMode=QtCore.Qt.KeepAspectRatio,
                                       transformMode=QtCore.Qt.SmoothTransformation)

                return pixmap

        if role == QtCore.Qt.ToolTipRole:
            if index.column() == 2:
                np = ''
                count = 1
                for i in self.__data__[index.row()][2]:
                    np += i
                    count += 1
                    if count > 38 and i == ' ':
                        count = 1
                        np += '\n'

                # return self.__data__[index.row()][1]
                return np

        if role == QtCore.Qt.DisplayRole:
            # self.__data__[index.row()][index.column()]

            # Source column

            if index.column() == 5:
                if self.__data__[index.row()][5] == 'library':
                    return 'Library'
                else:
                    return self.__data__[index.row()][5]

            if index.column() == 6:
                if self.__data__[index.row()][index.column()] == 1:
                    return 'YES'
                else:
                    return 'NO'

            if index.column() == 1:
                return str(int(self.__data__[index.row()][1]))

            if index.column() == 6:

                return self.__data__[index.row()][6]

            return self.__data__[index.row()][index.column()]

    def headerData(self, col, orientation, role):

        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.__header__[col]

        return None

    def sort(self, col, order):
        """sort table by given column number col"""
        self.emit(QtCore.SIGNAL("layoutAboutToBeChanged()"))
        self.__data__ = sorted(self.__data__, key=operator.itemgetter(col))
        if order == QtCore.Qt.DescendingOrder:
            self.__data__.reverse()
        self.emit(QtCore.SIGNAL("layoutChanged()"))


class typeIconDelegate(QtGui.QItemDelegate):
    """
    A delegate that places a fully functioning QComboBox in every
    cell of the column to which it's applied
    """

    def __init__(self, parent):
        QtGui.QItemDelegate.__init__(self, parent)

    def paint(self, painter, option, index):

        self._disk_location = self.parent().engine.disk_location

        self.label = QtGui.QLabel(self.parent())
        self.label.setMaximumWidth(50)
        self.label.setMinimumWidth(50)
        self.label.setMaximumHeight(50)
        self.label.setMinimumHeight(50)

        self.label.setStyleSheet("QLabel {background-color: none;}")

        current_icon_name = self.parent().model().__data__[index.row()][index.column()]
        self.label.setText(current_icon_name)

        # sample_palette = QtGui.QPalette()
        # sample_palette.setColor(self.label.backgroundRole(), QtCore.Qt.red)

        # self.label.setAutoFillBackground(True)
        # self.label.setPalette(sample_palette)

        # self.label.setStyleSheet("QLabel { background-color: rgb(0, 255, 0); border: 2px solid blue}")
        pixmap = QtGui.QPixmap('{}/resources/nodeType2.png'.format(self._disk_location))
        self.label.setPixmap(pixmap)

        if not self.parent().indexWidget(index):
            self.parent().setIndexWidget(
                index,
                self.label
            )


class buttonDelegate(QtGui.QItemDelegate):
    """
    A delegate that places a fully functioning QComboBox in every
    cell of the column to which it's applied
    """

    def __init__(self, parent):

        QtGui.QItemDelegate.__init__(self, parent)

        self._parent = parent

    def paint(self, painter, option, index):

        self._disk_location = self._parent._disk_location

        rw = index.row()
        cl = index.column()
        is_default = self.parent().model.__data__[index.row()][6]

        if is_default:
            current_stus = 1
        else:
            current_stus = self.parent().model.__data__[index.row()][index.column()]

        if current_stus == 1:
            btn_style = "QPushButton {{background-image:url({}/resources/itemOn3.png) no-repeat; border: none; background-color: none;}}".format(self._disk_location)
            btn_text = 'loaded_{0}_{1}'.format(rw, cl)
        else:
            btn_style = "QPushButton {{background-image:url({}/resources/itemOff3.png) no-repeat; border: none; background-color: none;}}".format(self._disk_location)
            btn_text = 'unloaded_{0}_{1}'.format(rw, cl)

        self.push = QtGui.QPushButton(self.parent())
        self.push.setStyleSheet(btn_style)
        self.push.setObjectName(btn_text)
        self.push.setMinimumSize(54, 25)
        self.push.setMaximumSize(54, 25)

        if not is_default:
            self.connect(self.push, QtCore.SIGNAL("clicked()"), self.parent().currentIndexChanged)

        if not self.parent().indexWidget(index):
            self.parent().setIndexWidget(
                index,
                self.push
            )


class comboDelegate(QtGui.QItemDelegate):
    """
    A delegate that places a fully functioning QComboBox in every
    cell of the column to which it's applied
    """

    def __init__(self, parent):

        QtGui.QItemDelegate.__init__(self, parent)

    def paint(self, painter, option, index):

        rw = index.row()
        cl = index.column()
        pf_versions = self.parent().model.__data__[index.row()][6]

        v_list = []
        for pf in pf_versions:
            pass

        self._disk_location = self.parent().engine.disk_location
        """    
        if is_default:
            current_stus = 1
        else:
            current_stus = self.parent().model.__data__[index.row()][index.column()]
        """
        current_stus = self.parent().model.__data__[index.row()][index.column()]

        if current_stus == 1:
            btn_style = "QPushButton {{background-image:url({}/resources/itemOn3.png) no-repeat; border: none; background-color: none;}}".format(self._disk_location)
            btn_text = 'loaded_{0}_{1}'.format(rw, cl)
        else:
            btn_style = "QPushButton {{background-image:url({}/resources/itemOff3.png) no-repeat; border: none; background-color: none;}}".format(self._disk_location)
            btn_text = 'unloaded_{0}_{1}'.format(rw, cl)

        self.push = QtGui.QPushButton(self.parent())
        self.push.setStyleSheet(btn_style)
        self.push.setObjectName(btn_text)
        self.push.setMinimumSize(80, 20)
        self.push.setMaximumSize(80, 20)

        self.connect(self.push, QtCore.SIGNAL("clicked()"), self.parent().currentIndexChanged)

        if not self.parent().indexWidget(index):
            self.parent().setIndexWidget(
                index,
                self.push
            )


class TableView(QtGui.QTableView):
    """
    A simple table to demonstrate the QComboBox delegate.
    """
    loaded_increased = QtCore.Signal()
    loaded_decreased = QtCore.Signal()

    def __init__(self, *args, **kwargs):
        QtGui.QTableView.__init__(self, *args, **kwargs)
        self.model = None

        self.infoManager = None

        # Set the delegate buttons for column ( load/unload )
        self.setItemDelegateForColumn(3, buttonDelegate(self))
        # Set delegate comboBox for column (version)
        # self.setItemDelegateForColumn(6, comboDelegate(self))

        self.setAlternatingRowColors(True)
        self.setStyleSheet("alternate-background-color: White;background-color: #dbdbdb;");

        self._disk_location = None

    def load_houdini_hda(self, nodeName):
        print 'Loading HDA: {}...'.format(nodeName)

        # Copy to

    def unload_houdini_hda(self, nodeName):
        print 'Un-loading HDA: {}...'.format(nodeName)

    def enable_librearyItem(self, row):
        # Get the node definition
        item_name = self.model.__data__[row][0]
        print 'Enablig: ', item_name
        # get the source path for the item
        item_source = self.model.__data__[row][8]['path']['local_path_linux']

        if os.path.exists(item_source):
            # import to houdini
            hou.hda.installFile(item_source, oplibraries_file=None, change_oplibraries_file=True, force_use_assets=True)
            # update pf to entity itenlibrary list field
            print 'HDA enabled!!!'
        else:
            print 'HDA file not found on library for: {}, HDA not installed !!!'.format(item_name)

    def disable_librearyItem(self, row):
        item_name = self.model.__data__[row][0]
        print 'Disablig: ', item_name
        # Get the node definition
        item_source = self.model.__data__[row][8]['path']['local_path_linux']
        # Get the node entity path source
        # Delete similynk from source
        # remove pf from entity itenlibrary list field
        hou.hda.uninstallFile(item_source, oplibraries_file=None, change_oplibraries_file=True)
        print 'HDA disabled!!!'

    def register_in_entity(self, id, libraryItem, row):
        print 'register_in_entity', id, libraryItem
        entity = self.infoManager.engine.context.entity
        fields = ['sg_loaded_library_items', 'sg_default_library_items']
        filters = [['id', 'is', entity['id']]]
        context_ids = self.infoManager.engine.shotgun.find_one(entity['type'], filters, fields)

        id_string = '{}'.format(self.model.__data__[row][8]['id'])
        if libraryItem:
            id_string = 'L{}'.format(id)

        print 'id_string: ', id_string, context_ids['sg_loaded_library_items']


        if context_ids['sg_loaded_library_items']:
            if id_string not in context_ids['sg_loaded_library_items']:
                new_context_ids = context_ids['sg_loaded_library_items'] + ',{}'.format(id_string)
            else:
                new_context_ids = None
        else:
            new_context_ids = id_string

        print 'new_context_ids: ', new_context_ids

        if new_context_ids is not None:
            entity_update = self.infoManager.engine.shotgun.update(entity['type'], entity['id'],
                                                                       {'sg_loaded_library_items': new_context_ids})
            print entity_update
            print 'HDA registered to loaded field in entity!'

    def remove_from_entity(self, id, libraryItem, row):
        entity = self.infoManager.engine.context.entity
        fields = ['sg_loaded_library_items', 'sg_default_library_items']
        filters = [['id', 'is', entity['id']]]
        context_ids = self.infoManager.engine.shotgun.find_one(entity['type'], filters, fields)
        id_string = '{}'.format(self.model.__data__[row][8]['id'])
        if libraryItem:
            id_string = 'L{}'.format(id)

        if id_string in context_ids['sg_loaded_library_items']:
            new_context_ids = ''
            for id in context_ids['sg_loaded_library_items'].split(','):
                if id != id_string:
                    if new_context_ids:
                        new_context_ids += ',{}'.format(id)
                    else:
                        new_context_ids = '{}'.format(id)

            entity_update = self.infoManager.engine.shotgun.update(entity['type'], entity['id'],
                                                                   {'sg_loaded_library_items': new_context_ids})
            print 'HDA removed to loaded field in entity!'


            new_context_ids = context_ids['sg_loaded_library_items'] .replace(id_string, '')

    def currentIndexChanged(self):
        # button toggle
        obj_split = self.sender().objectName().split("_")

        status = obj_split[0].lower()

        autoload = self.model.__data__[int(obj_split[1])][6]

        if autoload:
            # Omit any change
            return

        print 'obj_split: ', obj_split
        print 'status: ', status

        if status == 'unloaded':
            btn_style = "QPushButton {{background-image:url({}/resources/itemOn3.png) no-repeat; border: none; background-color: none;}}".format(self._disk_location)
            self.sender().setStyleSheet(btn_style)
            new_name = 'loaded_{0}_{1}'.format(obj_split[1], obj_split[2])
            self.sender().setObjectName(new_name)

            # The element must be loaded to entity level folder
            self.enable_librearyItem(int(obj_split[1]))

            # The item must be registered in the shot field for loaded library items
            pf_id = self.model.__data__[int(obj_split[1])][8]['id']

            print 'nnnnnnnn: ', self.model.__data__[int(obj_split[1])][8]['path']['local_path_linux']

            is_lib = True if 'ovfxToolkit/Resources' in self.model.__data__[int(obj_split[1])][8]['path']['local_path_linux'] else False
            self.register_in_entity(pf_id, is_lib, int(obj_split[1]))

            self.loaded_increased.emit()
        else:
            btn_style = "QPushButton {{background-image:url({}/resources/itemOff3.png) no-repeat;border: none; background-color: none;}}".format(self._disk_location)
            self.sender().setStyleSheet(btn_style)
            new_name = 'unloaded_{0}_{1}'.format(obj_split[1], obj_split[2])
            self.sender().setObjectName(new_name)

            # The element must be loaded to entity folder
            self.disable_librearyItem(int(obj_split[1]))
            # The item must be un-registered in the entity field for loaded library items
            pf_id = self.model.__data__[int(obj_split[1])][8]['id']
            is_lib = True if 'ovfxToolkit/Resources' in self.model.__data__[int(obj_split[1])][8]['path']['local_path_linux'] else False
            self.remove_from_entity(pf_id, is_lib, int(obj_split[1]))

            self.loaded_decreased.emit()


class dataCollector():

    def __init__(self, *args, **kwargs):
        self.shotgun = None
        self.engine = None
        self.disk_location = None
        self.data = None
        self.sgtk = None

        self.create_sg_connection()
        self.sgDataItems = self.get_tool_items()
        self.reset_items_installed()
        self.lib_data, self.items, self.loaded, self.autoload = self.generate_table_data(self.sgDataItems)

        self.__app = None

    def reset_items_installed(self):
        for item_source in hou.hda.loadedFiles():
            if 'Resources/library' in item_source:
                hou.hda.uninstallFile(item_source, oplibraries_file=None, change_oplibraries_file=True)

    def create_sg_connection(self):
        try:
            # Tookit context shotgun instance
            import sgtk
            self.sgtk = sgtk

            self.__app = sgtk.platform.current_bundle()
            self.engine = sgtk.platform.current_engine()

            self.disk_location = self.__app.disk_location

            self.shotgun = self.engine.shotgun
        except:
            # Standalone shotgun instance
            SERVER_PATH = 'https://0vfx.shotgunstudio.com'
            SCRIPT_NAME = "Toolkit"
            SCRIPT_KEY = 'b20e30ae264400f844d4197b5805d1105ba6ddf06fd733b51a74b8fa290cbc73'
            import shotgun_api3
            sg = shotgun_api3.Shotgun(SERVER_PATH, SCRIPT_NAME, SCRIPT_KEY)
            self.shotgun = sg

    def show_sg(self):

        print 'sg: ', self.shotgun

    def get_items_DEPRECATED(self, project='LIBR'):

        project_items = []
        if self.shotgun:
            filters = [['project.Project.code', 'is', project]]
            fields = ['code', 'sg_category', 'description', 'sg_environment', 'project.Project.code', 'sg_status_list',
                      'sg_type', 'sg_published_files']
            project_items = self.shotgun.find('CustomNonProjectEntity09', filters, fields)

        return project_items

    def get_data_array_DEPRECATED(self):
        """
        This module get all the library elements available for the context.
        First gets the library items published in the Library project.
        Then gets the library items published for the current proyect.

        returns: a list of shotgun published files for every tool
        """
        items = []

        # Get the library items
        lib_items = self.get_items()
        if lib_items:
            items.extend(lib_items)

        # extend with project items
        if self.engine and self.engine.context.project:
            project = self.shotgun.find_one("Project", [['id', 'is', self.engine.context.project['id']]], ['code'])
            p_items = self.get_items(project=project['code'])

            if p_items:
                items.extend(p_items)

        return items

    def get_tool_items(self):
        """
        This method get the list of items available for librarys
        :return:
        """
        all_tool_items = []
        if self.shotgun:
            filters = []
            fields = ['code', 'sg_category', 'description', 'sg_environment', 'sg_status_list',
                      'sg_type', 'sg_published_files']

            all_tool_items = self.shotgun.find('CustomNonProjectEntity09', filters, fields)

        return all_tool_items

    def get_context_items(self):
        """
        This module gets the list of library items ids registered in the current context entity.
        Creates a dictionary holding the list of id for default items to be loaded and a list of ids
        items alredy selected for the context entity .

        :return:
        A dictionary containing a lost for default library items and a list for loaded library items.
        """
        entity = self.engine.context.entity

        fields = ['sg_loaded_library_items', 'sg_default_library_items']
        filters = [['id', 'is', entity['id']]]
        context_lib_ids = self.shotgun.find_one(entity['type'], filters, fields)

        # ID's for default items
        default_items = {'library': [], 'project': []}
        if context_lib_ids['sg_default_library_items']:
            default_items_ids = context_lib_ids['sg_default_library_items'].split(',')
            for id in default_items_ids:
                if 'L' in id:
                    default_items['library'].append(int(id.replace('L', '')))
                else:
                    default_items['project'].append(int(id))

        # ID's for context items
        context_items = {'library': [], 'project': []}
        if context_lib_ids['sg_loaded_library_items']:
            context_items_ids = context_lib_ids['sg_loaded_library_items'].split(',')
            print 'context_items_ids: ', context_items_ids
            for id in context_items_ids:
                if 'L' in id:
                    context_items['library'].append(int(id.replace('L', '')))
                else:
                    context_items['project'].append(int(id))

        return default_items, context_items

    def is_registered_item(self, pf, id_list):
        default_items, context_items = id_list
        print '\tverify: ', pf['project']['id'], self.engine.context.project['id']

        if pf['project']['id'] == self.engine.context.project['id']:
            # The publishedFile is regitered for the project
            source = 'project'
            print '\t', 11111111111111111111111
        elif pf['project']['id'] == 347:
            # The publishedFile is regitered in Library
            source = 'library'
            print '\t', 22222222222222222222222
        else:
            # The publishedFile belongs to other project and cant loaded
            source = None
            print '\t', 33333333333333333333333

        print '\tsource: ', source

        if pf['id'] in default_items[source]:
            # load item, item is default
            return True, True, source
        if pf['id'] in context_items[source]:
            # load item, item is not default
            return True, False, source

        # item not found, item not default, not in current project
        return False, False, source

    def generate_table_data(self, dataraw):
        """
            item build information:

            [  0,    1,        2,          3,      4,      5,        6   ]
            [name,   version, description, status, type,   source,   autoload]
            [string, int,     string,      bool,   string, string,   bool  ]

        """
        table_content = []

        starting_loaded_items = self.get_context_items()

        loaded_count = 0
        autoload_count = 0
        items_count = 0

        print 'dataraw: ', len(dataraw)
        for item in dataraw:
            print '\n', item['code']

            items_count += 1
            name = item['code']
            description = item['description']
            type = item['sg_type']
            #source = item['project.Project.code']

            # find all published files for every item library
            lib_entity = {'type': item['type'], 'id': item['id']}
            filters = [['entity', 'is', lib_entity]]
            fields = ['version_number', 'project', 'project.Project.code', 'path', 'name']
            pfs = self.shotgun.find("PublishedFile", filters, fields)

            print '\tpfs: ', len(pfs)
            version_list = []
            for pf in pfs:
                # Get the items registered for the context
                status, autoload, source = self.is_registered_item(pf, starting_loaded_items)
                print 'result: ', status, autoload, source

                if source:
                    # install library item
                    if status:
                        loaded_count += 1
                        item_source = pfs[0]['path']['local_path_linux']
                        hou.hda.installFile(item_source, oplibraries_file=None, change_oplibraries_file=True,
                                            force_use_assets=True)
                    if autoload:
                        autoload_count += 1

                    version = str(pf['version_number']).zfill(3)
                    """ REVISAR: como hacer un item para cada version (de published file) """
                    item_tool = [name, version, description, status, type, source, autoload,  item, pf]
                    table_content.append(item_tool)

        print 'table_content: ', len(table_content)

        return table_content, items_count, loaded_count, autoload_count


class Ui_Dialog(object):

    loaded_items = 0
    total_items = 0
    autoload_items = 0

    def increased(self):

        self.loaded_items += 1
        self.label3_1.setText(str(self.loaded_items))


    def decreased(self):

        self.loaded_items -= 1
        self.label3_1.setText(str(self.loaded_items))


    def setupUi(self, Dialog):
        header = ['Name', 'Version', 'Description', 'Status', 'Type', 'Source', 'Autoload']

        dataManagement = dataCollector()
        # Data
        tableData0 = dataManagement.lib_data
        self.total_items = dataManagement.items
        self.loaded_items = dataManagement.loaded
        self.autoload_items = dataManagement.autoload
        disk_location = dataManagement.disk_location

        # Model
        model = MyModel(tableData0, header, disk_location)

        window = Dialog
        window.setObjectName("Dialog")
        #window.setStyleSheet("QDialog { background-color: white;}")

        verticalLayout = QtGui.QVBoxLayout(window)
        # gridLayout = QtGui.QGridLayout(window)
        # gridLayout.setObjectName("gridLayout")
        window.show()


        table = TableView(window)
        table.loaded_increased.connect(self.increased)
        table.loaded_decreased.connect(self.decreased)
        table._disk_location = disk_location
        table.infoManager = dataManagement
        table.model = model
        # table.show()
        table.setStyleSheet("QTableView::item {padding: 10px; border: 0px solid black; color: #b0b0b0; }")

        font = QtGui.QFont()
        font.setFamily("Arial")
        # table.setFont(QtGui.QFont("Helvetica Neue", 10))
        table.setFont(font)

        # table.setSizeAdjustPolicy(QtGui.QAbstractScrollArea.AdjustToContents)
        table.setModel(model)
        # gridLayout.addWidget(table, 0, 0, 1, 1)
        verticalLayout.addWidget(table)

        horizontalLayout = QtGui.QHBoxLayout(window)

        label1 = QtGui.QLabel()
        label1.setText('Elementos en libreria: ')
        label1_1 = QtGui.QLabel()
        label1_1.setText(str(self.total_items))

        label2 = QtGui.QLabel()
        label2.setText('Elementos default: ')
        label2_1 = QtGui.QLabel()
        label2_1.setText(str(self.autoload_items))


        label3 = QtGui.QLabel()
        label3.setText('Elementos cargados: ')
        self.label3_1 = QtGui.QLabel()
        self.label3_1.setText(str(self.loaded_items))

        horizontalLayout.addWidget(label1)
        horizontalLayout.addWidget(label1_1)
        horizontalLayout.addStretch()
        horizontalLayout.addWidget(label2)
        horizontalLayout.addWidget(label2_1)
        horizontalLayout.addStretch()
        horizontalLayout.addWidget(label3)
        horizontalLayout.addWidget(self.label3_1)
        #
        verticalLayout.addLayout(horizontalLayout)



        #for r in range(model.rowCount(None)):
        #    table.setRowHeight(r, 50)
        # for c in range(model.columnCount()):
        #    table.

        table.setColumnWidth(0, 50)
        table.update()
        width = table.horizontalHeader().length()

        if table.verticalScrollBar().isVisible():
            width += table.verticalScrollBar().width()

        # table.setMaximumWidth(width)
        # table.setMinimumWidth(width)
        # window.setFixedWidth(width+50)

        # table.setColumnWidth(0, 20)

        header = table.horizontalHeader()
        header.setMinimumHeight(50)
        header.setMaximumHeight(50)
        header.setSectionResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtGui.QHeaderView.ResizeToContents)

        header.setSectionResizeMode(3, QtGui.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QtGui.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QtGui.QHeaderView.ResizeToContents)


        table.setShowGrid(False)
        #table.resizeColumnsToContents()
        # table.setSelectionMode(QtGui.QAbstractItemView.NoSelection)

        header.resizeSection(2, 220)
        header.resizeSection(6, 80)

        header = table.horizontalHeader()
        header.setStyleSheet(
            "QHeaderView::section {border: 0px; background-color: #36304a; color: white; padding-left: 14px; padding-top: 14px; padding-right: 14px;} QHeaderView {qproperty-defaultAlignment: AlignLeft;}")

        #header.setSectionResizeMode(2, QtGui.QHeaderView.ResizeToContents)
        #
        headerV = table.verticalHeader()
        headerV.hide()

        window.resize(736, 500)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "The Current Sgtk Environment", None, QtGui.QApplication.UnicodeUTF8))
        self.context.setText(QtGui.QApplication.translate("Dialog", "Your Current Context: ", None, QtGui.QApplication.UnicodeUTF8))

from . import resources_rc
