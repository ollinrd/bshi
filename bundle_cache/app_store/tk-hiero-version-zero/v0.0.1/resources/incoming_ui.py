# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'incoming.ui'
#
# Created: Tue Sep  4 13:49:01 2018
#      by: pyside-uic 0.2.13 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui
from PySide2 import QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(306, 252)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.spinBox_2 = QtWidgets.QSpinBox(Form)
        self.spinBox_2.setObjectName("spinBox_2")
        self.spinBox_2.setValue(1)
        self.horizontalLayout.addWidget(self.spinBox_2)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.checkBox = QtWidgets.QCheckBox(Form)
        self.checkBox.setObjectName("checkBox")
        self.verticalLayout.addWidget(self.checkBox)
        self.groupBox = QtWidgets.QGroupBox(Form)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.spinBox = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox.setObjectName("spinBox")
        self.spinBox.setValue(1)
        self.verticalLayout_2.addWidget(self.spinBox)
        self.verticalLayout.addWidget(self.groupBox)
        self.checkBox_2 = QtWidgets.QCheckBox(Form)
        self.checkBox_2.setObjectName("checkBox_2")
        self.verticalLayout.addWidget(self.checkBox_2)
        self.groupBox_2 = QtWidgets.QGroupBox(Form)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.spinBox_4 = QtWidgets.QSpinBox(self.groupBox_2)
        self.spinBox_4.setObjectName("spinBox_4")
        self.spinBox_4.setValue(1)
        self.verticalLayout_3.addWidget(self.spinBox_4)
        self.verticalLayout.addWidget(self.groupBox_2)
        self.buttonBox = QtWidgets.QDialogButtonBox(Form)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.groupBox.setEnabled(False)
        self.groupBox_2.setEnabled(False)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtWidgets.QApplication.translate("Form", "Ingest Options"))
        self.label.setText(QtWidgets.QApplication.translate("Form", "Plate Version:"))
        self.checkBox.setText(QtWidgets.QApplication.translate("Form", "V0 Internal"))
        self.groupBox.setTitle(QtWidgets.QApplication.translate("Form", "Internal Version Number"))
        self.checkBox_2.setText(QtWidgets.QApplication.translate("Form", "V0 Client"))
        self.groupBox_2.setTitle(QtWidgets.QApplication.translate("Form", "Client Version Number"))

