import sgtk
import sys
import os
from sgtk.platform.qt import QtCore, QtGui

logger = sgtk.platform.get_logger(__name__)

class AppDialog(QtGui.QTabWidget):
	def __init__(self, parent=None):
	 	super(AppDialog, self).__init__()
		self.opcionA = QtGui.QWidget()
		self.opcionB = QtGui.QWidget()
		self.opcionC = QtGui.QWidget()
		self.addTab(self.opcionA,"Send Files")
		self.addTab(self.opcionB,"Request Files")
		self.tabSend()
		self.tabRequest()

	def tabSend(self):
		layout = QtGui.QHBoxLayout()
		self.opcionA.setLayout(layout)

	def tabRequest(self):
		layout = QtGui.QHBoxLayout()
		self.opcionB.setLayout(layout)
