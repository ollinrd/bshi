import sgtk

logger = sgtk.platform.get_logger(__name__)

def show_dialog(app):
    """
    Show the main dialog ui

    :param app: The parent App
    """
    # defer imports so that the app works gracefully in batch modes
    from .ovfx_FileTransfer import AppDialog

    display_name = sgtk.platform.current_bundle().get_setting("display_name")

    # start ui
    app.engine.show_dialog(display_name, app, AppDialog)