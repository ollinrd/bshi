# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'progress.ui'
#
# Created: Mon Oct 12 17:53:44 2020
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

import sgtk
import sys
import os
from sgtk.platform.qt import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class AppDialog2(QtGui.QTabWidget):
    def __init__(self, parent=None):
        super(AppDialog2, self).__init__()
        self.verticalLayout66 = QtGui.QVBoxLayout(self)
        self.verticalLayout66.setObjectName(_fromUtf8("verticalLayout66"))
        self.horizontalLayout66 = QtGui.QHBoxLayout()
        self.horizontalLayout66.setObjectName(_fromUtf8("horizontalLayout66"))
        self.label66 = QtGui.QLabel(self)
        self.label66.setObjectName(_fromUtf8("label"))
        self.horizontalLayout66.addWidget(self.label66)
        self.progressBar = QtGui.QProgressBar(self)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.horizontalLayout66.addWidget(self.progressBar)
        self.verticalLayout66.addLayout(self.horizontalLayout66)
        self.label66_2 = QtGui.QLabel(self)
        self.label66_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout66.addWidget(self.label66_2)
        self.log_deb = QtGui.QLabel(self)
        self.log_deb.setText(_fromUtf8(""))
        self.log_deb.setObjectName(_fromUtf8("log_deb"))
        self.verticalLayout66.addWidget(self.log_deb)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout66.addItem(spacerItem)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        self.setWindowTitle(_translate("Progress", "Progress", None))
        self.label66.setText(_translate("Progress", "Progress", None))
        self.label66_2.setText(_translate("Progress", "LOG:", None))

