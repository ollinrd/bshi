# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'vendor_pub.ui'
#
# Created: Mon Oct 12 14:00:51 2020
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

import sgtk
import sys
import os
from sgtk.platform.qt import QtCore, QtGui

logger = sgtk.platform.get_logger(__name__)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class AppDialog(QtGui.QTabWidget):
    def __init__(self, parent=None):
        super(AppDialog, self).__init__()
        # Form.setObjectName(_fromUtf8("Form"))
        self.resize(1322, 704)
        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.xls_path = QtGui.QLineEdit(self)
        self.xls_path.setObjectName(_fromUtf8("xls_path"))
        self.horizontalLayout.addWidget(self.xls_path)
        self.xls_browse = QtGui.QPushButton(self)
        self.xls_browse.setObjectName(_fromUtf8("xls_browse"))
        self.horizontalLayout.addWidget(self.xls_browse)
        spacerItem = QtGui.QSpacerItem(80, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_2 = QtGui.QLabel(self)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_3.addWidget(self.label_2)
        self.inc_path = QtGui.QLineEdit(self)
        self.inc_path.setObjectName(_fromUtf8("inc_path"))
        self.horizontalLayout_3.addWidget(self.inc_path)
        self.dir_browse = QtGui.QPushButton(self)
        self.dir_browse.setObjectName(_fromUtf8("dir_browse"))
        self.horizontalLayout_3.addWidget(self.dir_browse)
        spacerItem1 = QtGui.QSpacerItem(80, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.s_all = QtGui.QPushButton(self)
        self.s_all.setObjectName(_fromUtf8("s_all"))
        self.horizontalLayout_2.addWidget(self.s_all)
        self.d_all = QtGui.QPushButton(self)
        self.d_all.setObjectName(_fromUtf8("d_all"))
        self.horizontalLayout_2.addWidget(self.d_all)
        spacerItem2 = QtGui.QSpacerItem(80, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.read_but = QtGui.QPushButton(self)
        self.read_but.setObjectName(_fromUtf8("read_but"))
        self.horizontalLayout_2.addWidget(self.read_but)
        self.clear_but = QtGui.QPushButton(self)
        self.clear_but.setObjectName(_fromUtf8("clear_but"))
        self.horizontalLayout_2.addWidget(self.clear_but)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.tableWidget = QtGui.QTableWidget(self)
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout.addWidget(self.tableWidget)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        spacerItem3 = QtGui.QSpacerItem(80, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem3)
        self.p_button = QtGui.QPushButton(self)
        self.p_button.setObjectName(_fromUtf8("p_button"))
        self.horizontalLayout_4.addWidget(self.p_button)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.verticalLayout66 = QtGui.QVBoxLayout(self)
        self.verticalLayout66.setObjectName(_fromUtf8("verticalLayout66"))
        self.horizontalLayout66 = QtGui.QHBoxLayout()
        self.horizontalLayout66.setObjectName(_fromUtf8("horizontalLayout66"))
        self.label66 = QtGui.QLabel(self)
        self.label66.setObjectName(_fromUtf8("label"))
        self.horizontalLayout66.addWidget(self.label66)
        self.progressBar = QtGui.QProgressBar(self)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.horizontalLayout66.addWidget(self.progressBar)
        self.verticalLayout66.addLayout(self.horizontalLayout66)
        self.label66_2 = QtGui.QLabel(self)
        self.label66_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout66.addWidget(self.label66_2)
        self.log_deb = QtGui.QLabel(self)
        self.log_deb.setText(_fromUtf8(""))
        self.log_deb.setObjectName(_fromUtf8("log_deb"))
        self.verticalLayout66.addWidget(self.log_deb)
        self.verticalLayout.addLayout(self.verticalLayout66)
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        self.setWindowTitle(_translate("Form", "Vendor Publisher", None))
        self.label.setText(_translate("Form", "XLS Path:", None))
        self.xls_browse.setText(_translate("Form", "Browse", None))
        self.label_2.setText(_translate("Form", "DIR Path:", None))
        self.dir_browse.setText(_translate("Form", "Browse", None))
        self.s_all.setText(_translate("Form", "Select all", None))
        self.d_all.setText(_translate("Form", "Deselect all", None))
        self.read_but.setText(_translate("Form", "Read File", None))
        self.clear_but.setText(_translate("Form", "Clear Table", None))
        self.p_button.setText(_translate("Form", "PUBLISH SELECTED ITEMS", None))
        self.label66.setText(_translate("Progress", "Progress", None))
        self.label66_2.setText(_translate("Progress", "LOG:", None))
