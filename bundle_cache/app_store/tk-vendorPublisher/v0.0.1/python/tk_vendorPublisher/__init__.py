#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sgtk
import os
import ctypes 
logger = sgtk.platform.get_logger(__name__)
from sgtk.platform.qt import QtCore, QtGui

#baseP, formP = uic.loadUiType('/nfs/ovfxToolkit/Resources/pythonTools/vendor_publisher/views/progress.ui')
import subprocess
import io
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import shotgun_api3
import xlrd
from xlrd.sheet import ctype_text
import threading
from .vendor_pub import AppDialog
from .progress import AppDialog2
# print help(QtCore)

'''
class Progress(baseP, formP):
    def __init__(self, parent=None): 
        formP.__init__(self, parent)
        self.setupUi(self)
        self._fromUtf8 = QtCore.QString.fromUtf8
        self.progressBar.setValue(0)
'''

class mythread(QtCore.QThread):
    def __init__(self, parent=None):
        print 'parent:' ,parent
        super(mythread, self).__init__(parent.ui_class)
        self.papa = parent
        self.progressWidget = AppDial(self)
        self.progressWidget.p_bar.progressBar.setValue(0)
        self.progressWidget.p_bar.show()

        print parent.mysignal
        #parent.mysignal.connect(self.update)
        #self.progressWidget.p_bar.connect(mysignal)#parent.ui_class, QtCore.SIGNAL('avance_principal'), self.update)

    def update(self, logg, avan):
        print 'entro thread'
        self.progressWidget.p_bar.log_deb.setText(str(logg))
        self.progressWidget.p_bar.progressBar.setValue(avan)
        if avan == 100:
            pass
            self.prgs_class.close()

    def get_id(self): 
        # returns id of the respective thread 
        if hasattr(self, '_thread_id'): 
            return self._thread_id 
        for id, thread in threading._active.items(): 
            if thread is self: 
                return id
   
    def raise_exception(self): 
        thread_id = self.get_id() 
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 
              ctypes.py_object(SystemExit)) 
        if res > 1: 
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0) 
            print('Exception raise failure')
class AppDial(QtGui.QMainWindow, AppDialog2):
    def __init__(self, parent= None):
        super(AppDial, self).__init__(parent.papa.ui_class)
        print 'parent2:' ,parent
        self.p_bar = AppDialog2()
        # self._fromUtf8 = QtCore.QString.fromUtf8


# main class -------------------------------------------------------------------
class ShowDialog():
    def show_dialog(self, app):
        print app.disk_location
        display_name = sgtk.platform.current_bundle().get_setting("display_name")
        # start ui
        self.mysignal = QtCore.Signal()
        self.ui_class = app.engine.show_dialog(display_name, app, AppDialog) 
        self.sg = app.shotgun
        self.app = app
        self.ui_class.read_but.clicked.connect(self.read_xls_file)
        self.ui_class.s_all.clicked.connect(self.select_all)
        self.ui_class.d_all.clicked.connect(self.deselect_all)
        self.ui_class.p_button.clicked.connect(self.publish_selected)
        self.ui_class.xls_browse.clicked.connect(self.browse_x)
        self.ui_class.dir_browse.clicked.connect(self.browse_d)
        self.ui_class.clear_but.clicked.connect(self.clean_table)
        fname = '/nfs/ollinvfx/Project/BSHI/editorial/incoming/20201009a_VRV/Submission Form_20201009a_VRV.xlsx'
        folder_ = '/nfs/ollinvfx/Project/BSHI/editorial/incoming/20201009a_VRV/'
        self.project = app.context.project
        self.project = self.sg.find_one('Project', [['id', 'is', self.project['id']]],
                                   ['code'])
        self.ui_class.xls_path.setText(fname)
        self.ui_class.inc_path.setText(folder_)
        self.errors = []

    def clean_table(self):
        self.ui_class.tableWidget.clear()
        self.ui_class.tableWidget.setRowCount(0)
        self.ui_class.tableWidget.setColumnCount(0)
        self.ui_class.progressBar.setValue(0)
        self.tex = ''

    def update_p(self, logg, porc):
        self.ui_class.progressBar.setValue(porc)
        self.tex = self.tex + '\n' + str(porc) + '% ' + str(logg)
        self.ui_class.log_deb.setText(self.tex)
        # self.mysignal.emit(logg, porc)#QtCore.SIGNAL('avance_principal'), logg, porc)

    def browse_x(self):
        xls_path = QtGui.QFileDialog.getOpenFileName(self.ui_class, "Select file",
                                                   "/nfs/ollinvfx/Project/{0}/editorial/".format(self.project['code']),
                                                   "Images (*.xls *.xlsx)")
        self.ui_class.xls_path.setText(str(xls_path[0]))

    def browse_d(self):
        dir_path = QtGui.QFileDialog.getExistingDirectory(self.ui_class, "Select Pack",
                                                        "/nfs/ollinvfx/Project/{0}/editorial/".format(self.project['code']),
                                                        QtGui.QFileDialog.ShowDirsOnly)
        self.ui_class.inc_path.setText(str(dir_path))


    def init_engine(self, sg, project, sgtk, ShotgunAuthenticator):
        cdm = sgtk.util.CoreDefaultsManager()
        authenticator = ShotgunAuthenticator(cdm)
        user = authenticator.create_script_user(api_script="vendor_incomig_tool",
                                                api_key="cop2oQorgych(owaphisojkbv")
        sgtk.set_authenticated_user(user)
        tk = sgtk.sgtk_from_entity("Project", project['id'])
        tk.synchronize_filesystem_structure()
        ctx = tk.context_from_entity("Project", project['id'])

        if sgtk.platform.current_engine():
            sgtk.platform.current_engine().destroy()
        engine = sgtk.platform.start_engine("tk-shell", tk, ctx)
        return engine, tk, ctx

    def publish_selected(self):
        self.ui_class.progressBar.setValue(0)
        self.tex = ''
        sys.path.append('/nfs/ovfxToolkit/Prod/Projects/{0}/install/core/python'.format(self.project['code']))
        import sgtk
        from tank_vendor.shotgun_authentication import ShotgunAuthenticator
        print 'start publisher'
        engine, tk, ctx = self.init_engine(self.sg, self.project, sgtk, ShotgunAuthenticator)
        for i in range(1, self.ui_class.tableWidget.rowCount()):
            error = False
            try:
                if self.ui_class.tableWidget.cellWidget(i, 0).isChecked():
                    tipo = ''
                    print str(self.ui_class.tableWidget.item(i, 14).text())
                    if '.nk' in str(self.ui_class.tableWidget.item(i, 14).text()):
                        tipo = 'nk'
                    if '.ma' in str(self.ui_class.tableWidget.item(i, 14).text()):
                        tipo = 'ma'
                    if '.mb' in str(self.ui_class.tableWidget.item(i, 14).text()):
                        tipo = 'mb'
                    if '.abc' in str(self.ui_class.tableWidget.item(i, 14).text()):
                        tipo = 'abc'
                    if '.3de' in str(self.ui_class.tableWidget.item(i, 14).text()):
                        tipo = '3de'
                    if '.obj' in str(self.ui_class.tableWidget.item(i, 14).text()):
                        tipo = 'obj'
                    if not tipo == "":
                        version = str(self.ui_class.tableWidget.item(i, 9).text())
                        version = int(version.split('_v0')[-1])
                        name = str(self.ui_class.tableWidget.item(i, 7).text()).split('_')[-2]
                        data = {'entity_type': str(self.ui_class.tableWidget.item(i, 13).text()),
                                'entity_id': str(self.ui_class.tableWidget.item(i, 12).text()),
                                'task_name': str(self.ui_class.tableWidget.item(i, 6).text()),
                                'file_path': str(self.ui_class.tableWidget.item(i, 14).text()),
                                'comments': str(self.ui_class.tableWidget.item(i, 11).text()),
                                'version': version,
                                'name': str(self.ui_class.tableWidget.item(i, 16).text()),
                                'tipo': tipo
                                }

                        self.update_p('Validating .{0} files'.format(tipo), 20)
                        engine.execute_hook_by_name('ovfx_vendor_publish', diction=data,
                                                    engine=engine, tk=tk, ctx=ctx)
            except Exception as e:
                print e
                self.update_p(sys.exc_info()[0], 20)
                error = True
                break
        if not error:
            self.update_p('Ready .nk files', 30)

        for i in range(1,self.ui_class.tableWidget.rowCount()):
            try:
                if self.ui_class.tableWidget.cellWidget(i, 0).isChecked():
                    if not str(self.ui_class.tableWidget.item(i, 14).text()).split('.')[-1] in ['nk', 'mov', 'abc', 'ma', '3de']:
                        version = str(self.ui_class.tableWidget.item(i, 9).text())
                        version = int(version.split('_v0')[-1])
                        name = str(self.ui_class.tableWidget.item(i, 7).text()).split('_')[-2]
                        data = {'entity_type': str(self.ui_class.tableWidget.item(i, 13).text()),
                                'entity_id': str(self.ui_class.tableWidget.item(i, 12).text()),
                                'task_name': str(self.ui_class.tableWidget.item(i, 6).text()),
                                'file_path': str(self.ui_class.tableWidget.item(i, 14).text()),
                                'comments': str(self.ui_class.tableWidget.item(i, 10).text()),
                                'version': version,
                                'name': name + str(self.ui_class.tableWidget.item(i, 16).text()),
                                'tipo': 'seq',
                                'vendor': str(self.ui_class.tableWidget.item(i, 16).text()),
                                'submiss': str(self.ui_class.tableWidget.item(i, 17).text()),
                                }
                        self.update_p('Validating Seq files', 60)
                        engine.execute_hook_by_name('ovfx_vendor_publish', diction=data,
                                                    engine=engine, tk=tk, ctx=ctx)
            except Exception as e:
                print e
                error = True
                self.update_p(sys.exc_info()[0], 60)
                break
        if not error:
            self.update_p('Ready Seq files', 100)


    def select_all(self):
        for i in range(1,self.ui_class.tableWidget.rowCount()):
            self.ui_class.tableWidget.cellWidget(i, 0).setChecked(True)

    def deselect_all(self):
        for i in range(1,self.ui_class.tableWidget.rowCount()):
            # if self.table_equiv.cellWidget(row, 10).isChecked():
            self.ui_class.tableWidget.cellWidget(i, 0).setChecked(False)

    def read_xls_file(self):

        self.ui_class.progressBar.setValue(0)
        self.tex = ''
        # self.thread = mythread(self)
        # self.thread.start()
        fname = self.ui_class.xls_path.text()
        xl_workbook = xlrd.open_workbook(str(fname))
        self.xl_sheet = xl_workbook.sheet_by_index(0)
        row = self.xl_sheet.row(0)  # 1st row
        # First validation col names and order
        col_list = ['submission',
                    'show',
                    'vendor',
                    'shot',
                    'submission notes',
                    'version',
                    'file name',
                    'format',
                    'frames',
                    'task',
                    'frame range',
                    ]
        for idx, cell_obj in enumerate(row):
            if not str(cell_obj.value).lower() == col_list[idx]:
                self.showdialog('las columnas no cumplen con el formato !')
            print('(%s) %s' % (idx, cell_obj.value))
        self.get_items()

    def get_items(self):
        self.items = []
        xl_sheet = self.xl_sheet
        num_cols = xl_sheet.ncols   # Number of columns
        for row_idx in range(0, xl_sheet.nrows):    # Iterate through rows
            if not row_idx == 0:
                info_dict ={
                            'subm_name': str(xl_sheet.cell(row_idx, 0).value),
                            'project_code': str(xl_sheet.cell(row_idx, 1).value),
                            'vendor': str(xl_sheet.cell(row_idx, 2).value),
                            'shot_code': str(xl_sheet.cell(row_idx, 3).value),
                            'subm_notes': str(xl_sheet.cell(row_idx, 4).value),
                            'version': str(xl_sheet.cell(row_idx, 5).value),
                            'filename': str(xl_sheet.cell(row_idx, 6).value),
                            'f_type': str(xl_sheet.cell(row_idx, 7).value),
                            'frange': str(xl_sheet.cell(row_idx, 8).value),
                            'task': str(xl_sheet.cell(row_idx, 9).value),
                            'first_frame': str(xl_sheet.cell(row_idx, 10).value).split('-')[0],
                            'last_frame': str(xl_sheet.cell(row_idx, 10).value).split('-')[1],
                }
                self.items.append(info_dict)
        self.show_data()

    def show_data(self):
        self.clean_table()
        items = self.items
        indice = 0
        self.ui_class.tableWidget.setColumnCount(18)
        self.main_folder = str(self.ui_class.inc_path.text())
        self.ui_class.tableWidget.insertRow(indice)
        self.ui_class.tableWidget.setItem(indice, 0, QtGui.QTableWidgetItem(str('CK')))
        self.ui_class.tableWidget.setItem(indice, 1, QtGui.QTableWidgetItem(str('ENTITY NAME')))
        self.ui_class.tableWidget.setItem(indice, 2, QtGui.QTableWidgetItem(str('FIRST FRAME')))
        self.ui_class.tableWidget.setItem(indice, 3, QtGui.QTableWidgetItem(str('SG FIRST FRAME')))
        self.ui_class.tableWidget.setItem(indice, 4, QtGui.QTableWidgetItem(str('LAST FRAME')))
        self.ui_class.tableWidget.setItem(indice, 5, QtGui.QTableWidgetItem(str('SG LAST FRAME')))
        self.ui_class.tableWidget.setItem(indice, 6, QtGui.QTableWidgetItem(str('TASK')))
        self.ui_class.tableWidget.setItem(indice, 7, QtGui.QTableWidgetItem(str('FILE NAME')))
        self.ui_class.tableWidget.setItem(indice, 8, QtGui.QTableWidgetItem(str('FILE TYPE')))
        self.ui_class.tableWidget.setItem(indice, 9, QtGui.QTableWidgetItem(str('VERSION')))
        self.ui_class.tableWidget.setItem(indice, 10, QtGui.QTableWidgetItem(str('NOTES')))
        self.ui_class.tableWidget.setItem(indice, 11, QtGui.QTableWidgetItem(str('STATUS')))
        self.ui_class.tableWidget.setItem(indice, 12, QtGui.QTableWidgetItem(str('ID')))
        self.ui_class.tableWidget.setItem(indice, 13, QtGui.QTableWidgetItem(str('E.TYPE')))
        self.ui_class.tableWidget.setItem(indice, 14, QtGui.QTableWidgetItem(str('PATH')))
        self.ui_class.tableWidget.setItem(indice, 15, QtGui.QTableWidgetItem(str('Errors')))
        self.ui_class.tableWidget.setItem(indice, 16, QtGui.QTableWidgetItem(str('Vendor')))
        self.ui_class.tableWidget.setItem(indice, 17, QtGui.QTableWidgetItem(str('Submission')))

        indice += 1
        for item in items:
            filters = [['code', 'is', item['shot_code']],
                       ['project', 'is', self.project]]
            entity = self.sg.find_one('Shot', filters, ['code', 'sg_head_in', 'sg_tail_out'])

            if not entity:
                entity = self.sg.find_one('Asset', filters, ['code'])
            if entity['type'] == 'Shot':
                self.ui_class.tableWidget.insertRow(indice)
                cb = QtGui.QCheckBox()
                self.ui_class.tableWidget.setCellWidget(indice, 0, cb)
                self.ui_class.tableWidget.setItem(indice, 1, QtGui.QTableWidgetItem(str(entity['code'])))
                self.ui_class.tableWidget.setItem(indice, 2, QtGui.QTableWidgetItem(str(item['first_frame'])))
                self.ui_class.tableWidget.setItem(indice, 3, QtGui.QTableWidgetItem(str(entity['sg_head_in'])))
                self.ui_class.tableWidget.setItem(indice, 4, QtGui.QTableWidgetItem(str(item['last_frame'])))
                self.ui_class.tableWidget.setItem(indice, 5, QtGui.QTableWidgetItem(str(entity['sg_tail_out'])))
                self.ui_class.tableWidget.setItem(indice, 6, QtGui.QTableWidgetItem(str(item['task'])))
                self.ui_class.tableWidget.setItem(indice, 7, QtGui.QTableWidgetItem(str(item['filename'])))
                self.ui_class.tableWidget.setItem(indice, 8, QtGui.QTableWidgetItem(str(item['f_type'])))
                self.ui_class.tableWidget.setItem(indice, 9, QtGui.QTableWidgetItem(str(item['version'])))
                self.ui_class.tableWidget.setItem(indice, 10, QtGui.QTableWidgetItem(str(item['subm_notes'])))
                self.ui_class.tableWidget.setItem(indice, 11, QtGui.QTableWidgetItem(str('')))
                self.ui_class.tableWidget.setItem(indice, 12, QtGui.QTableWidgetItem(str(entity['id'])))
                self.ui_class.tableWidget.setItem(indice, 13, QtGui.QTableWidgetItem(str(entity['type'])))
                self.ui_class.tableWidget.setItem(indice, 16, QtGui.QTableWidgetItem(str(item['vendor'])))
                self.ui_class.tableWidget.setItem(indice, 17, QtGui.QTableWidgetItem(str(item['subm_name'])))
                self.ui_class.tableWidget.setColumnHidden(14, True)
                self.ui_class.tableWidget.setColumnHidden(13, True)
                self.ui_class.tableWidget.setColumnHidden(12, True)
                self.ui_class.tableWidget.resizeColumnsToContents()
                # semaforo

                error = ""
                self.update_p('Validating: {0}'.format(item['filename']), ((indice*100)/10) -5 )
                if not int(entity['sg_head_in']) == int(item['first_frame']):

                    self.ui_class.tableWidget.item(indice, 11).setBackground(QtGui.QColor(255, 0, 0))
                    error += 'El primer cuadro no coincide con la informacion de SG\n'
                if not int(entity['sg_tail_out']) == int(item['last_frame']):
                    self.ui_class.tableWidget.item(indice, 11).setBackground(QtGui.QColor(255, 0, 0))
                    error += 'El ultimo cuadro no coincide con la informacion de SG\n'
                if '#' in item['filename']:
                    error += self.find_files(item, 1, indice, error)
                elif item['f_type'].lower() in ['exr', 'tiff', 'jpg', 'jpeg', 'dpx', 'png']:
                    error += self.find_files(item, 1, indice, error)
                elif item['f_type'].lower() in ['nk', 'mov', 'abc', 'ma', '3de']:
                    error += self.find_files(item, 2, indice, error)
                self.ui_class.tableWidget.setItem(indice, 15, QtGui.QTableWidgetItem(error))
                indice += 1
        self.update_p('Complete !!', 100)
        # self.thread.raise_exception()


    def find_files(self, item, opc, indice, error):
        if opc == 1:
            first_file = item['filename'].replace('####', str(item['first_frame']))
            last_file = item['filename'].replace('####', str(item['last_frame']))
            filenamefl = [first_file, last_file]
            flag = [0, 0]
            cont = 0
            x = 0
            for i in filenamefl:
                for root, dirs, files in os.walk(self.main_folder):
                    for file in files:
                        if file == i:
                            print 'valid', os.path.join(root, file)
                            find_ = root
                            flag[cont] = 1

                            cont += 1
                            break
                    if x == 1:
                        break
            if flag == [0, 0]:
                error += 'not found or incomplete sequence: ' + item['filename']
                self.ui_class.tableWidget.item(indice, 11).setBackground(QtGui.QColor(255, 0, 0))
                self.ui_class.tableWidget.setItem(indice, 14, QtGui.QTableWidgetItem(""))
            else:
                path_or = find_ + '/' + item['filename'].replace('####', '%04d')
                self.ui_class.tableWidget.setItem(indice, 14, QtGui.QTableWidgetItem(path_or))
        if opc == 2:
            flag = 0
            for i in str(item['filename']):
                for root, dirs, files in os.walk(self.main_folder):
                    for file in files:
                        if file == str(item['filename']):
                            print 'valid', os.path.join(root, file)
                            find_ = root
                            flag = 1
                            break
                if flag == 1:
                    break
            if flag == 0:
                error += 'not found:'+ item['filename']
                self.ui_class.tableWidget.item(indice, 11).setBackground(QtGui.QColor(255, 0, 0))
                self.ui_class.tableWidget.setItem(indice, 14, QtGui.QTableWidgetItem(""))
            else:
                path_or = find_ + '/' + item['filename']
                self.ui_class.tableWidget.setItem(indice, 14, QtGui.QTableWidgetItem(path_or))
        return error


    def showdialog(self, err):
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Information)
        msg.setText("{0}".format(err))
        msg.setWindowTitle("INFO")
        msg.setStandardButtons(QtGui.QMessageBox.Ok)
        msg.show()
