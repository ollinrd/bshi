# Copyright (c) 2017 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import pprint
import traceback
import subprocess
import time
import shutil
from sgtk.util.filesystem import ensure_folder_exists
import sgtk
from sgtk.util.filesystem import copy_file, ensure_folder_exists

HookBaseClass = sgtk.get_hook_baseclass()

import maya.cmds as cmds

class BasicABCFilePublishPlugin(HookBaseClass):
    """
    Plugin for creating generic publishes in Shotgun.

    This plugin is typically configured to act upon files that are dragged and
    dropped into the publisher UI. It can also be used as a base class for
    other file-based publish plugins as it contains standard operations for
    validating and registering publishes with Shotgun.

    Once attached to a publish item, the plugin will key off of properties that
    drive how the item is published.

    The ``path`` property, set on the item, is the only required property as it
    informs the plugin where the file to publish lives on disk.

    The following properties can be set on the item via the collector or by
    subclasses prior to calling methods on the base class::

        ``sequence_paths`` - If set in the item properties dictionary, implies
            the "path" property represents a sequence of files (typically using
            a frame identifier such as %04d). This property should be a list of
            files on disk matching the "path". If the ``work_template`` property
            is set, and corresponds to the listed frames, fields will be
            extracted and applied to the publish_template (if set) and copied to
            that publish location.

        ``work_template`` - If set in the item properties dictionary, this
            value is used to validate ``path`` and extract fields for further
            processing and contextual discovery. For example, if configured and
            a version key can be extracted, it will be used as the publish
            version to be registered in Shotgun.

    The following properties can also be set by a subclass of this plugin via
    :meth:`Item.properties` or :meth:`Item.local_properties`.

        publish_template - If set, used to determine where "path" should be
            copied prior to publishing. If not specified, "path" will be
            published in place.

        publish_type - If set, will be supplied to SG as the publish type when
            registering "path" as a new publish. If not set, will be determined
            via the plugin's "File Type" setting.

        publish_path - If set, will be supplied to SG as the publish path when
            registering the new publish. If not set, will be determined by the
            "published_file" property if available, falling back to publishing
            "path" in place.

        publish_name - If set, will be supplied to SG as the publish name when
            registering the new publish. If not available, will be determined
            by the "work_template" property if available, falling back to the
            ``path_info`` hook logic.

        publish_version - If set, will be supplied to SG as the publish version
            when registering the new publish. If not available, will be
            determined by the "work_template" property if available, falling
            back to the ``path_info`` hook logic.

        publish_dependencies - A list of files to include as dependencies when
            registering the publish. If the item's parent has been published,
            it's path will be appended to this list.

    NOTE: accessing these ``publish_*`` values on the item does not necessarily
    return the value used during publish execution. Use the corresponding
    ``get_publish_*`` methods which include fallback logic when no property is
    set. For example, if a ``work_template`` is used, the publish version and
    name might be extracted from the template fields in the fallback logic.

    This plugin will also set an ``sg_publish_data`` property on the item during
    the ``publish`` method which may be useful for child items.

        ``sg_publish_data`` - The dictionary of publish information returned
            from the tk-core register_publish method.

    NOTE: If you have multiple plugins acting on the same item, and you need to
    access or operate on the publish data, you can extract the
    ``sg_publish_data`` from the item after calling the base class ``publish``
    method in your plugin subclass.
    """

    ############################################################################
    # standard publish plugin properties


    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder        
        return os.path.join(
            self.disk_location,
            "icons",
            "ornatrix_icon.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish to Shotgun"


    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """
        Publishes the file to Shotgun. A <b>Publish</b> entry will be
        created in Shotgun which will include a reference to the file's current
        path on disk. Other users will be able to access the published file via
        the <b><a href='%s'>Loader</a></b> so long as they have access to
        the file's location on disk.

        <h3>File versioning</h3>
        The <code>version</code> field of the resulting <b>Publish</b> in
        Shotgun will also reflect the version number identified in the filename.
        The basic worklfow recognizes the following version formats by default:

        <ul>
        <li><code>filename.v###.ext</code></li>
        <li><code>filename_v###.ext</code></li>
        <li><code>filename-v###.ext</code></li>
        </ul>

        <br><br><i>NOTE: any amount of version number padding is supported.</i>

        <h3>Overwriting an existing publish</h3>
        A file can be published multiple times however only the most recent
        publish will be available to other users. Warnings will be provided
        during validation if there are previous publishes.
        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        return {
            "File Types": {
                "type": "list",
                "default": [
                    ["Alembic Geo", "abc"],
                    ["Alembic Cache", "abc"],
                    ["Ziva Alembic", "abc"],
                    ["Ornatrix ASS", "ass"],
                    ["3dsmax Scene", "max"],
                    ["NukeStudio Project", "hrox"],
                    ["Houdini Scene", "hip", "hipnc"],
                    ["Maya Scene", "ma", "mb"],
                    ["Motion Builder FBX", "fbx"],
                    ["Nuke Script", "nk"],
                    ["Photoshop Image", "psd", "psb"],
                    ["Rendered Image", "dpx", "exr"],
                    ["Texture", "tiff", "tx", "tga", "dds"],
                    ["Image", "jpeg", "jpg", "png"],
                    ["Movie", "mov", "mp4"],
                ],
                "description": (
                    "List of file types to include. Each entry in the list "
                    "is a list in which the first entry is the Shotgun "
                    "published file type and subsequent entries are file "
                    "extensions that should be associated."
                )
            },
        }

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["maya.session.characterFX"]

    ############################################################################
    # standard publish plugin methods

    def accept(self, settings, item):

        ornatrix_Nodes=cmds.ls(typ='HairShape')
        print ornatrix_Nodes
        result = [str(mset) for mset in cmds.listSets(allSets=True) if 'set_sgpublish' == str(mset).lower()]            
        if result:
            # return the accepted info
            if ornatrix_Nodes:
                print 'accepted'
                return {"accepted": True}
         

        
        return {"accepted": False}

    def validate(self, settings, item):
        publish_path =  item.properties.get("ornatrix_ass_path")
        publisher = self.parent
        engine = sgtk.platform.current_engine()
        #overwrite = settings.get("user_data")["overwrite"]
        overwrite = engine.overwrite_publish
        print 'File overwrite set: ', overwrite

        # ---- determine the information required to validate

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.
        publish_path = item.properties.work_path         
        publish_name = str(item.properties['publish_name'])

        current_version = item.properties["fields"]["version"]
        filters = [["published_file_type.PublishedFileType.code", "is", "Ornatrix Ass"], ['name', 'is', publish_name], ['version_number', 'is', current_version]]
        print 'filters: ', filters

        publishes = self.parent.engine.shotgun.find_one('PublishedFile', filters, [])

        publishes = self.parent.engine.shotgun.find_one('PublishedFile', 
            [["published_file_type.PublishedFileType.code", "is", "Ornatrix Ass"], ['name', 'is', publish_name], ['version_number', 'is', current_version]], 
            [])
        if publishes:
            self.logger.debug(
                "Conflicting publishes: %s" % (pprint.pformat(publishes),))

            publish_template = self.get_publish_template(settings, item)
            # -
            if not overwrite:
                # templates are in play and there is already a publish in SG
                # for this file path. We will raise here to prevent this from
                # happening.
                error_msg = (
                    "Can X not validate file path. There is already a publish in "
                    "Shotgun that matches this path. Please uncheck this "
                    "plugin or save the file to a different path."
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)

            else:                
                conflict_info = (
                    "If you continue, these conflicting publishes will no "
                    "longer be available to other users via the loader:<br>"
                    "<pre>%s</pre>" % (pprint.pformat(publishes),)
                )
                self.logger.warn(
                    "Found %s conflicting publishes in Shotgun" %
                        (len(publishes),),
                    extra={
                        "action_show_more_info": {
                            "label": "Show Conflicts",
                            "tooltip": "Show conflicting publishes in Shotgun",
                            "text": conflict_info
                        }
                    }
                )

        self.logger.info("A Publish will be created in Shotgun")     

        #if self.parent.context.entity['type'] == 'Asset':
        #    self.validate_internal_review(publish_path, current_version)           

        return True


    def validate_internal_review(self, path, version_number):
        #
        project = self.parent.context.project
        entity = self.parent.context.entity

        publish_name = os.path.splitext(os.path.basename(path))[0]

        # get the internal review version for current context
        version = self.parent.engine.shotgun.find_one('Version', 
            [['project', 'is', project], ['entity', 'is', entity], ['code', 'is', publish_name], ['sg_status_list', 'is', 'apr']], 
            [])
        
        if not version:
            import pymel
            mess = pymel.core.confirmDialog(t="Confirm",
                                            m= "There is  no  internal review version for this file \ndo you want to continue?",
                                            button = ["Yes", "No"], cancelButton = "No")
            if not mess == 'Yes':
                error_msg = "There is no internal review version aproved in shotgun for actual version number: {0}.\n".format(version_number)
                error_msg += "A internal review version must be aproved first in order to publish."
                raise Exception(error_msg)

        return False



    def get_nodes_to_export(self, cmds):       
        node_list = []
        result = [set for set in cmds.listSets(allSets=True) if 'set_sgpublish' == str(set).lower()]        
        if result:
            set_list = cmds.sets(result[0], q=True)
            cmds.select(set_list)
            node_list = [str(node) for node in cmds.ls(selection=True, l=True)]    

        return node_list



    def custom_sg(self):
        import sys
        sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
        from shotgun_api3 import Shotgun
        sys.path.append("/nfs/ovfxToolkit/Resources")
        from pythonTools.shotgunUtils.dbConnect import sgConnect

        sgConn = sgConnect()
        codeName = "editorial_status"
        SCRIPT_NAME = sgConn.API_NAMES.get(codeName)
        SCRIPT_KEY = sgConn.get_apikey_from_apiname(codeName)

        sg = Shotgun("https://0vfx.shotgunstudio.com", script_name=SCRIPT_NAME, api_key=SCRIPT_KEY)

        return sg
    
    def publish(self, settings, item):
        print 'alembic publish process started...'
        publish_path = item.properties['ornatrix_ass_path']
        publish_type = item.properties['ornatrix_publish_type']
        publish_name = os.path.basename(item.properties['ornatrix_ass_path']).split('.')[0]

        #Folder Exist
        self.parent.engine.ensure_folder_exists(os.path.dirname(publish_path))
        #Publish Operations
        import sgtk
        engine= sgtk.platform.current_engine()
        e_type= engine.context.entity["type"]

        if e_type.lower()== "shot":
            start_frame = int(cmds.getAttr('defaultRenderGlobals.startFrame'))
            end_frame = int(cmds.getAttr('defaultRenderGlobals.endFrame'))
        else:
            #asset jus one frame needed 
            start_frame = int(cmds.getAttr('defaultRenderGlobals.startFrame'))
            end_frame = int(cmds.getAttr('defaultRenderGlobals.startFrame'))
            
        copy_maya = self._CreateCopy()
        command= self.CreateKickass(publish_path,copy_maya, start_frame, end_frame)
        out = self._send_job(command)
        print 'OUT job: ', out
        job_id = out.split("JobID=")[-1].split("The job")[0].strip()

        # Clear tmp file created for deadline ass files generation.
        self._clear_maya_temp(job_id, copy_maya)



        publisher = self.parent
        publish_version = item.properties["fields"]['version']
        publish_path = item.properties['ornatrix_ass_path']
        print 'Get dependencies ...'
        publish_dependencies = self.get_publish_dependencies(settings, item)
        print 'Publish Dependencies : {0}'.format(publish_dependencies)
        publish_name = item.properties["publish_name"]
        #publish_name, extension = os.path.splitext(publish_name)
        
        # if the parent item has a publish path, include it in the list of
        # dependencies
        if "sg_publish_path" in item.parent.properties:
            publish_dependencies.append(item.parent.properties.sg_publish_path)

        # handle copying of work to publish if templates are in play
        #self._copy_work_to_publish(settings, item)

        # arguments for publish registration
        self.logger.info("Registering publish...")

        #pub_dependencies = self.validate_publish_dependencies(item.context.task['id'], publish_dependencies)
        pub_dependencies = publish_dependencies

        print 'Data name to publish: ', publish_name
        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": pub_dependencies
        }
        # log the publish data for debugging
        self.logger.debug(
            "Populated Publish data...",
            extra={
                "action_show_more_info": {
                    "label": "Publish Data",
                    "tooltip": "Show the complete Publish data dictionary",
                    "text": "<pre>%s</pre>" % (pprint.pformat(publish_data),)
                }
            }
        )

        # Search for the actual publish name in sg
        current_version = item.properties["fields"]["version"]
        print 'current_version: ', current_version
        actual_publish_name = item.properties["publish_name"]
        print 'actual_publish_name: ', actual_publish_name
        publish = self.parent.engine.shotgun.find_one('PublishedFile',
            [["published_file_type.PublishedFileType.code", "is", "Ornatrix Ass"], ['name', 'is', str(actual_publish_name)], ['version_number', 'is', current_version]],
            ["code", "path", "path_cache", "path_cache_storage", "name"])

        # create the publish and stash it in the item properties for other
        # plugins to use.
        print 'Publish found: ', publish

        if publish:
            sg = self.custom_sg()
            print 'SG: ', sg
            sg.delete('PublishedFile', publish['id'])
            print 'DELETED: ', publish['id']

        # Register the actual publish
        print 'publish data ABC: ', publish_data
        publish = sgtk.util.register_publish(**publish_data)
        print 'Published registered for Ornatix ASS Files: ', publish['id']


        """
        if not publish:
            # Register the actual publish
            publish = sgtk.util.register_publish(**publish_data)
            print 'Published: ', publish
            print 'Data: ', publish_data
        else:
            print 'Publish already registered, updating data...'

            #
            filters = [["code", "is", publish_type]]
            sg_published_file_type = self.parent.shotgun.find_one('PublishedFileType', filters=filters)
            print 'sg_published_file_type: ', publish_type, ' : ', sg_published_file_type

            thumbnail_path = item.get_thumbnail_as_path()
            print 'thumbnail_path: ', thumbnail_path

            #Update the published found with current data
            data = {
                "dependency_paths": pub_dependencies
            }

            if item.description:
                data["description"] = item.description

            print 'Updating published file...'
            publish_update = self.parent.shotgun.update(publish['type'], publish['id'], data)
            print 'Data result: ', publish_update
            print 'Data updated: ', data

            if thumbnail_path:            
                print 'Uploading new thumbnail...'
                self.parent.shotgun.upload_thumbnail("PublishedFile", publish["id"], thumbnail_path)
                print 'Publish updated!'
        """

        item.properties["sg_publish_data"] = publish
        # item.properties.sg_publish_data = sgtk.util.register_publish(**publish_data)

        self.logger.info("Publish registered!")


    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent

        # get the data for the publish that was just created in SG
        publish_data = item.properties.sg_publish_data

        # ensure conflicting publishes have their status cleared
        publisher.util.clear_status_for_conflicting_publishes(
            item.context, publish_data)

        self.logger.info(
            "Cleared the status of all previous, conflicting publishes")

        path = item.properties["ornatrix_ass_path"]
        self.logger.info(
            "Publish created for file: %s" % (path,),
            extra={
                "action_show_in_shotgun": {
                    "label": "Show Publish",
                    "tooltip": "Open the Publish in Shotgun.",
                    "entity": publish_data
                }
            }
        )


    def get_publish_template(self, settings, item):
        """
        Get a publish template for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A template representing the publish path of the item or
            None if no template could be identified.
        """

        return item.get_property("publish_template")



    def get_publish_dependencies(self, settings, item):
        """
        Get publish dependencies for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A list of file paths representing the dependencies to store in
            SG for this publish
        """

        #
        if self.parent.engine.context.entity["type"].lower()== "asset":
            ma_publish_template = self.parent.engine.get_template_by_name("maya_asset_publish")
            ma_publish_path = ma_publish_template.apply_fields(item.properties.fields)
            print 'ma_publish_path: ', ma_publish_path

        elif self.parent.engine.context.entity["type"].lower()== "shot":
            ma_publish_template = self.parent.engine.get_template_by_name("maya_shot_publish")
            ma_publish_path = ma_publish_template.apply_fields(item.properties.fields)
            print 'ma_publish_path: ', ma_publish_path

        # local properties first
        dependencies = [str(ma_publish_path)]

        return dependencies






############ Send Ornatrix Node to Deadline ##############

    def _getSelection(self):
        import maya.cmds as cmds
        import maya.mel as mel

        selection = []
        result = [set for set in cmds.listSets(allSets=True) if 'set_sgpublish' == str(set).lower()]        
        if result:
            set_list = cmds.sets(result[0], q=True)
            cmds.select(set_list)

        selection=cmds.ls(sl=1)

        print selection

        melCmd= 'select -r '

        for sel in selection:
            melCmd+=  sel + ' '
        melCmd+= ';'
            
        print 'Mel Selection command', melCmd

        return melCmd

 

    def CreateKickass(self,publish_path, maya_copy, start_frame, end_frame):
        import maya.cmds as cmds
        import maya.mel as mel
        import sgtk

        #Get Frames
        start_frame = start_frame
        end_frame = end_frame
        #by_frame = int(cmds.getAttr('defaultRenderGlobals.byFrame'))


        #Set the Ass storage folder
        ass_folder =  os.path.dirname(publish_path)
        

        print 'ASS FOLDER: ', ass_folder

        script_name = os.path.basename(publish_path).split('.')[0]

        #Ensure folders exist
        engine=sgtk.platform.current_engine()
        ensure_folder_exists(ass_folder)

        #Multiple Layers
        multipleLayer= False
        currentRL= 'masterLayer'

        #Current layer name selected
        sCurrentRL= cmds.editRenderLayerGlobals(q=1, currentRenderLayer=True)

        if len (cmds.ls(type= 'renderlayer')) >= 2:
            multipleLayer = True
            if sCurrentRL != 'defaultRenderLayer':
                currentRL = str(sCurrentRL.replace('rs_', ''))

        if multipleLayer:
            ass_folder = ass_folder + '/{0}'.format(currentRL)

        #Create ass path
        ass_file_path= publish_path
        deadline_dir= os.path.dirname(ass_file_path)
        deadline_name= os.path.basename(ass_file_path).replace('.%04d', '')
        ass_file_path= deadline_dir + '/' +deadline_name
        print ass_file_path


        pool= self._define_pool()

        e_type = 'shot'

        engine=sgtk.platform.current_engine()
        if engine.context.entity['type'] == 'Asset':
            e_type = 'asset'

        melSelection= self._getSelection() 

        ornatrixCmd= "OxSetIsRendering(true);"

        #deadline Command
        file_path= cmds.file(exn=1,q=1)
        f_dir = os.path.dirname(file_path)
        flags = ' "-proj {0} -batch -file {1}'.format(f_dir, maya_copy)
        flags += " -command  '{0} {1} arnoldExportAss".format(melSelection, ornatrixCmd) 
        flags += " -f <QUOTE>{0}<QUOTE>".format(ass_file_path)
        flags += " -s"
        flags += " -bb"
        flags += " -startFrame <STARTFRAME%4>"
        flags += " -endFrame <ENDFRAME%4>"
        flags += "'"
        flags += '"'

        maya_path = '/usr/autodesk/maya2018/bin/maya'
        if '2019' in cmds.about(version=True):
            maya_path = '/usr/autodesk/maya2019/bin/maya'

        chunkN = int((end_frame-start_frame)/10)
        if chunkN < 2:
            chunkN = 2
        if chunkN > 10:
            chunkN = 10

        batch_cmd = " -SubmitCommandLineJob "
        batch_cmd += " -executable {0} ".format(maya_path)
        batch_cmd += " -arguments {0}".format(flags)
        batch_cmd += ' -frames {0}-{1}'.format(start_frame, end_frame)
        batch_cmd += ' -pool {0} '.format("ass")
        batch_cmd += ' -chunksize {0}'.format(chunkN)
        batch_cmd += ' -name "{0}"'.format('{0} ASS render'.format(e_type))
        batch_cmd += ' -prop ConcurrentTasks=1'
        batch_cmd += ' -prop BatchName={0}'.format(script_name)
        batch_cmd += ' -prop OutputDirectory0={0}'.format(ass_folder)

        batch_cmd += self._construct_environment_varibles()

        print 'ASS batch_cmd: ', batch_cmd
        return batch_cmd

        #Mandar a Deadline
    def _define_pool(self):
        matchmove_steps = [4, 176]
        light_steps= [7, 185, 28, 39, 78]
        comp_steps= [8, 174]
        import sgtk
        engine=sgtk.platform.current_engine()

        if engine.context.step["id"] in matchmove_steps:
            return "mm"
        elif engine.context.step["id"] in light_steps:
            return "light"
        elif engine.context.step["id"] in comp_steps:
            return "comp"
        else:
            return "cg"

    def _construct_environment_varibles(self):
        import maya.cmds as cmds

        evariables = " "
        arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa3202/"

        alShaders = "/nfs/ovfxToolkitSG/Resources/plugins/autodesk/"
        alShaders += "alShaders-linux-1.0.0rc19-ai4.2.12.2"
        maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'

        if '2019' in cmds.about(version=True):
            arnoldPath = '/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa401'
            maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules2019'

        evariables += " -prop EnvironmentKeyValue0=ARNOLD_PLUGIN_PATH={0}".format(arnoldPath)
        evariables += " -prop EnvironmentKeyValue1=MTOA_TEMPLATES_PATH={0}/ae".format(alShaders)
        evariables += " -prop EnvironmentKeyValue2=MAYA_CUSTOM_TEMPLATE_PATH={0}/aexml".format(alShaders)
        evariables += " -prop EnvironmentKeyValue3=MAYA_MODULE_PATH={0}".format(maya_modules)
        evariables += " -prop EnvironmentKeyValue4=MAYA_DISABLE_CIP=1"

        return evariables


    def _send_job(self, command):
        deadline = '/opt/Thinkbox/Deadline10/bin/deadlinecommand'
        ass= command
        py_cmd = ass

        process = subprocess.Popen(deadline + " " + py_cmd, shell=True, stdout=subprocess.PIPE)

        out, err = process.communicate()
        print 'Out: ', out
        print 'Err: ', err

        return out
        

    def _CreateCopy(self):

        import maya.cmds as cmds
        import maya.mel as mel
        from pymel import versions
        import pymel.core as pm

        folder = self._get_maya_imageFolder()

        multipleLayers = multipleAOVs = createPrefix = False
        RenLayer = RenderPass = RenderPassF = RenLayerF = ""
        filePrefix = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')

        # Render layers
        multiLayer= False
        currentRL= 'masterLayer'

        #Current layer name selected
        sCurrentRL= cmds.editRenderLayerGlobals(q=1, currentRenderLayer=True)

        if len (cmds.ls(type= 'renderlayer')) >= 2:
            multiLayer = True
            if sCurrentRL != 'defaultRenderLayer':
                currentRL = str(sCurrentRL.replace('rs_', ''))


        multipleLayers= multiLayer
        layerName = currentRL

        print 'multipleLayers: ', multipleLayers

        # Select if multiple aovs for render
        multipleAOVs = self.render_multiaovs()
        print 'multipleAOVs: ', multipleAOVs

        if multipleLayers:
            RenLayer = "<RenderLayer>/"
            RenLayerF = "_<RenderLayer>"
            # Enable multiple render layers
            if mel.eval("optionVar -q renderViewRenderAllLayers;") == 0:
                mel.eval("switchRenderAllLayers;")
        else:
            RenLayer = "masterLayer/"

        if multipleAOVs:
            RenderPass = "_<RenderPass>"
            RenderPassF = "<RenderPass>/"
        else:
            RenderPass = "_beauty"
            RenLayerF = "_masterLayer"

        fileBase = folder.split('/')[0]
        print 'fileBase: ', fileBase

        f_name = str(cmds.file(q=True, sn=True))
        outputname = os.path.splitext(os.path.basename(f_name))[0]
        newPrefix = "{4}/{0}{1}{5}{3}{2}".format(RenLayer, RenderPassF,
                                                 RenderPass, RenLayerF,
                                                 fileBase, outputname)
        cmds.setAttr('defaultRenderGlobals.imageFilePrefix',
                     newPrefix, type="string")

        # set The global pading sequence
        cmds.setAttr("defaultRenderGlobals.extensionPadding", 4)
        # set tiled
        cmds.setAttr("defaultArnoldDriver.exrTiled", 0)
        #
        cmds.setAttr("defaultArnoldDriver.preserveLayerName", 0)
        #
        cmds.setAttr("defaultArnoldDriver.append", 0)
        #
        cmds.setAttr("defaultArnoldDriver.mergeAOVs", 0)

        try:
            cmds.file(save=True)
        except:
            print "Access denied, you can not save  your changes"

        from datetime import datetime

        now_str = datetime.now()
        now_str2 = str(now_str).split('.')[0].replace(' ', '_')
        f_parts = f_name.split(".")
        f_name_copy = f_parts[0] + '_' + now_str2 + "." + f_parts[-1]

        base_name = os.path.basename(f_name_copy)

        f_name_copy = f_name_copy.replace(base_name, "." + base_name) #Crea Archivo Oculto
        if os.access(f_name_copy, os.F_OK):
            os.remove(f_name_copy)

        shutil.copy(f_name, f_name_copy)
        print ">>>>>>>>>>>>>>>>", f_name_copy
        return f_name_copy

    def _get_maya_imageFolder(self):
        import maya.cmds as cmds
        import maya.mel as mel

        scene_info = cmds.file(query=True, sn=True)
        scene_info = scene_info.split("/")[-1]
        scene_info = scene_info.split(".")[0]
        folder = scene_info.split("_")
        if len(folder) > 3:
            folder = folder[-3] + "_" + folder[-2] + "_" + folder[-1] + "/"
        else:
            folder = ""

        return folder

    def _clear_maya_temp(self, job_id, maya_path):
        import maya.cmds as cmds
        file_path = cmds.file(query=True, sn=True)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])
        deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"

        python_file = maya_path + "_tmp.py"

        code = ["import os"]
        code.append("\nif os.access('{0}', os.F_OK):".format(maya_path))
        code.append("\n    os.remove('{0}')".format(maya_path))
        code.append("\nif os.access('{0}', os.F_OK):".format(python_file))
        code.append("\n    os.remove('{0}')".format(python_file))

        with open(python_file, 'w') as f:
            for r in code:
                f.write(str(r))

        clear_cmd = " -SubmitCommandLineJob -executable"
        clear_cmd += " python -arguments {0}".format(python_file)
        clear_cmd += ' -pool cg'
        clear_cmd += ' -name "Cleaning cache file"'
        clear_cmd += ' -prop BatchName={0}'.format(scene_name)
        clear_cmd += ' -prop ExtraInfo0={0}'.format(maya_path)
        clear_cmd += ' -prop JobDependencies={0}'.format(job_id)
        clear_cmd += ' -prop ConcurrentTasks=1'
        clear_cmd += " -prop JobDependencyPercentage=100"
        clear_cmd += " -prop OutputDirectory0=/home"

        #subprocess.Popen(deadline + " " + clear_cmd, shell=True)
        #publish_errors = []
        print "Sending clean cache"
        #out = self._send_job(clear_cmd, publish_errors)
        out = self._send_job(clear_cmd)
        print out

    def render_multiaovs(self):
        import mtoa.aovs as aovs
        import maya.mel as mel

        #mel.eval('unifiedRenderGlobalsWindow;')

        AOVsList = aovs.AOVInterface().getAOVNodes(names=True)
        if len(AOVsList) >= 1:
            return True

        #mds.deleteUI("unifiedRenderGlobalsWindow")

        return False


    ############################################################################
    # protected methods


