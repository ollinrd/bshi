# Copyright (c) 2017 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import sys
import os
import pprint
import traceback
import subprocess
import sgtk
from sgtk.util.filesystem import copy_file, ensure_folder_exists
HookBaseClass = sgtk.get_hook_baseclass()
import nuke


class BasicFilePublishPlugin(HookBaseClass):
    """
    Plugin for creating generic publishes in Shotgun.

    This plugin is typically configured to act upon files that are dragged and
    dropped into the publisher UI. It can also be used as a base class for
    other file-based publish plugins as it contains standard operations for
    validating and registering publishes with Shotgun.

    Once attached to a publish item, the plugin will key off of properties that
    drive how the item is published.

    The ``path`` property, set on the item, is the only required property as it
    informs the plugin where the file to publish lives on disk.

    The following properties can be set on the item via the collector or by
    subclasses prior to calling methods on the base class::

        ``sequence_paths`` - If set in the item properties dictionary, implies
            the "path" property represents a sequence of files (typically using
            a frame identifier such as %04d). This property should be a list of
            files on disk matching the "path". If the ``work_template`` property
            is set, and corresponds to the listed frames, fields will be
            extracted and applied to the publish_template (if set) and copied to
            that publish location.

        ``work_template`` - If set in the item properties dictionary, this
            value is used to validate ``path`` and extract fields for further
            processing and contextual discovery. For example, if configured and
            a version key can be extracted, it will be used as the publish
            version to be registered in Shotgun.

    The following properties can also be set by a subclass of this plugin via
    :meth:`Item.properties` or :meth:`Item.local_properties`.

        publish_template - If set, used to determine where "path" should be
            copied prior to publishing. If not specified, "path" will be
            published in place.

        publish_type - If set, will be supplied to SG as the publish type when
            registering "path" as a new publish. If not set, will be determined
            via the plugin's "File Type" setting.

        publish_path - If set, will be supplied to SG as the publish path when
            registering the new publish. If not set, will be determined by the
            "published_file" property if available, falling back to publishing
            "path" in place.

        publish_name - If set, will be supplied to SG as the publish name when
            registering the new publish. If not available, will be determined
            by the "work_template" property if available, falling back to the
            ``path_info`` hook logic.

        publish_version - If set, will be supplied to SG as the publish version
            when registering the new publish. If not available, will be
            determined by the "work_template" property if available, falling
            back to the ``path_info`` hook logic.

        publish_dependencies - A list of files to include as dependencies when
            registering the publish. If the item's parent has been published,
            it's path will be appended to this list.

    NOTE: accessing these ``publish_*`` values on the item does not necessarily
    return the value used during publish execution. Use the corresponding
    ``get_publish_*`` methods which include fallback logic when no property is
    set. For example, if a ``work_template`` is used, the publish version and
    name might be extracted from the template fields in the fallback logic.

    This plugin will also set an ``sg_publish_data`` property on the item during
    the ``publish`` method which may be useful for child items.

        ``sg_publish_data`` - The dictionary of publish information returned
            from the tk-core register_publish method.

    NOTE: If you have multiple plugins acting on the same item, and you need to
    access or operate on the publish data, you can extract the
    ``sg_publish_data`` from the item after calling the base class ``publish``
    method in your plugin subclass.
    """

    ############################################################################
    # standard publish plugin properties

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(
            self.disk_location,
            "icons",
            "aovs.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish to Shotgun"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """
        Publishes the file to Shotgun. A <b>Publish</b> entry will be
        created in Shotgun which will include a reference to the file's current
        path on disk. Other users will be able to access the published file via
        the <b><a href='%s'>Loader</a></b> so long as they have access to
        the file's location on disk.

        <h3>File versioning</h3>
        The <code>version</code> field of the resulting <b>Publish</b> in
        Shotgun will also reflect the version number identified in the filename.
        The basic worklfow recognizes the following version formats by default:

        <ul>
        <li><code>filename.v###.ext</code></li>
        <li><code>filename_v###.ext</code></li>
        <li><code>filename-v###.ext</code></li>
        </ul>

        <br><br><i>NOTE: any amount of version number padding is supported.</i>

        <h3>Overwriting an existing publish</h3>
        A file can be published multiple times however only the most recent
        publish will be available to other users. Warnings will be provided
        during validation if there are previous publishes.
        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        import nuke
        current_nuke_path = nuke.root().name()

        engine = sgtk.platform.current_engine()

        template = engine.get_template_by_name('nuke_shot_work')                
        fields = template.get_fields(current_nuke_path)                
        current_version = fields['version']

        # settings specific to this class
        nuke_publish_settings = {
            "Publish Template": {
                "type": "template",
                "default": None,
                "description": "Template path for published work files. Should"
                               "correspond to a template defined in "
                               "templates.yml.",
            },

            "File Types": {
                "type": "list",
                "default": [
                    ["Alembic Cache", "abc"],
                    ["3dsmax Scene", "max"],
                    ["NukeStudio Project", "hrox"],
                    ["Houdini Scene", "hip", "hipnc"],
                    ["Maya Scene", "ma", "mb"],
                    ["Motion Builder FBX", "fbx"],
                    ["Nuke Script", "nk"],
                    ["Photoshop Image", "psd", "psb"],
                    ["Rendered Image", "dpx", "exr"],
                    ["Texture", "tiff", "tx", "tga", "dds"],
                    ["Image", "jpeg", "jpg", "png"],
                    ["Movie", "mov", "mp4"],
                ],
                "description": (
                    "List of file types to include. Each entry in the list "
                    "is a list in which the first entry is the Shotgun "
                    "published file type and subsequent entries are file "
                    "extensions that should be associated."
                )
            },

            "Current Version": {
                "type": "string",
                "default": current_version,
                "description": (
                    "Current version required"
                )
            }            
        }


        return nuke_publish_settings

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["nuke.session.cgElements"]

    ############################################################################
    # standard publish plugin methods

    def accept(self, settings, item):
        import sgtk
        engine= sgtk.platform.current_engine()
        step=engine.context.step['name'].lower()
        valid_steps=['light']

        if step in valid_steps:
            print "valid"
            aovs= self.get_nodes_to_export()

            if aovs:
                print 'There are aovs in this script'
                print '----------Accepted----------'
                return {"accepted": True,
                        "checked": True
                        }

        return {"accepted": False}

    def validate(self,settings,item):
        publish_path = item.properties.get("")
        publisher=self.parent
        engine = sgtk.platform.current_engine()
        #overwrite = settings.get("user_data")["overwrite"]
        overwrite = engine.overwrite_publish
        print 'File overwrite set: ', overwrite

        # ---- determine the information required to validate

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.

        publish_path = item.properties.work_path         
        publish_name = str(item.properties['publish_name'])
        current_version = item.properties["fields"]["version"]

        aovs= self.get_nodes_to_export()
        publishFiles= []

        for p in aovs:
            aov_name= p["aov"]
            path= p["path"]
            publish_aov_name= publish_name +"_"+ aov_name

            AOV_filters = [["published_file_type.PublishedFileType.code", "is", "Maya Render Passes"], ['name', 'is', publish_aov_name], ['version_number', 'is', current_version]]
  
            publishes = self.parent.engine.shotgun.find_one('PublishedFile', AOV_filters, [])

            if publishes:
                publishFiles.append(publishes)

            


        if publishFiles:
            self.logger.debug(
                "Conflicting publishes: %s" % (pprint.pformat(publishes),))

            publish_template = self.get_publish_template(settings, item)
            # -
            if not overwrite:
                # templates are in play and there is already a publish in SG
                # for this file path. We will raise here to prevent this from
                # happening.
                error_msg = (
                    "Can X not validate file path. There is already a publish in "
                    "Shotgun that matches this path. Please uncheck this "
                    "plugin or save the file to a different path."
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)

            else:                
                conflict_info = (
                    "If you continue, these conflicting publishes will no "
                    "longer be available to other users via the loader:<br>"
                    "<pre>%s</pre>" % (pprint.pformat(publishFiles),)
                )
                self.logger.warn(
                    "Found %s conflicting publishes in Shotgun" %
                        (publishes),
                    extra={
                        "action_show_more_info": {
                            "label": "Show Conflicts",
                            "tooltip": "Show conflicting publishes in Shotgun",
                            "text": conflict_info
                        }
                    }
                )

        self.logger.info("A Publish will be created in Shotgun")     

        #if self.parent.context.entity['type'] == 'Asset':
        #    self.validate_internal_review(publish_path, current_version)           

        return True


    def get_nodes_to_export(self):
        import nuke
        import sgtk
        engine = sgtk.platform.current_engine()
        project_id= engine.context.project["id"]
        tk = sgtk.sgtk_from_entity("Project", project_id)
        mayaFiles=[]
        mayaPasses=[]

        allNodes= nuke.allNodes(recurseGroups=True)

        for node in allNodes:
            if node.Class()== "Read":
                nodeName=node['name'].value() 
                filePath=node['file'].value()

                template= tk.template_from_path(filePath)
                if template:
                    templateName= template.name

                    if templateName == "maya_shot_render_passes_exr":
                        fields = template.get_fields(filePath)
                        fields.update({"maya_extension":".ma"})

                        maya_work_tmpt= engine.get_template_by_name('maya_shot_work') 
                        mayaPath = maya_work_tmpt.apply_fields(fields)         
                        aovName= (filePath.split("/")[-2]).replace("_"," ")

                        data= {
                        "node": nodeName,
                        "aov" : aovName,
                        "path": filePath,
                        "upstream_file": mayaPath,
                              }
                        mayaFiles.append(mayaPath)
                        mayaPasses.append(data)
                else:
                    pass

        return mayaPasses



    def custom_sg(self):
        import sys
        sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
        from shotgun_api3 import Shotgun
        sys.path.append("/nfs/ovfxToolkit/Resources")
        from pythonTools.shotgunUtils.dbConnect import sgConnect

        sgConn = sgConnect()
        codeName = "editorial_status"
        SCRIPT_NAME = sgConn.API_NAMES.get(codeName)
        SCRIPT_KEY = sgConn.get_apikey_from_apiname(codeName)

        sg = Shotgun("https://0vfx.shotgunstudio.com", script_name=SCRIPT_NAME, api_key=SCRIPT_KEY)

        return sg

    def publish(self, settings, item):
        print 'AOVS publish process started...'
        import sgtk
        eng_getTemp= self.parent.engine.get_template_by_name
        engine = sgtk.platform.current_engine()
        e_type= engine.context.entity["type"]

        nuke_publish_path= item.properties["publish_path"]
        publish_path = item.properties['maya_passes_path']
        publish_type = item.properties['maya_passes_type']
        publish_version = item.properties["fields"]['version']
        work_path=item.properties["work_path"]
      

        #[Get AOVS and Upstream Files]
        aovs= self.get_nodes_to_export()
        
        self.aov_Files=[]

        for a in aovs:
            node_name= a["node"]
            aov_name= a["aov"]
            aov_seq_path= a["path"]
            maya_path=a["upstream_file"]
            maya_name=maya_path.split("_")[-2]

            original_name= item.properties["publish_name"].split("_")[-1]
            publish_name= item.properties["publish_name"].replace(original_name, maya_name)
            publish_name =publish_name+ "_" + aov_name


            #publish_version = item.properties["fields"]['version']
            #publish_name =item.properties["publish_name"] + "_" + aov_name
    
            #Create real path
            """
            work_template= eng_getTemp('maya_{0}_render_passes_exr'.format(e_type.lower()))
            aov_fields= work_template.get_fields(aov_seq_path)
            """
            


            work_template =eng_getTemp('maya_{0}_work'.format(e_type.lower()))
            aov_fields= work_template.get_fields(maya_path)
            aov_fields['render_pass']= aov_name.replace(" ", "_")
            aov_fields['render_layer']= 'masterLayer'

            #New publish path
            maya_passes_template= eng_getTemp('maya_publish_{0}_render_passes_exr'.format(e_type.lower()))
            publish_path = maya_passes_template.apply_fields(aov_fields)
            publish_version = aov_fields["version"]
           

            #Folder Exist
            self.parent.engine.ensure_folder_exists(os.path.dirname(publish_path))

            #Publish Operations
            publisher = self.parent


            #----------------------------------------------------------------------------------------------------
            print 'Get dependencies ...'
            publish_dependencies = self.get_publish_dependencies(settings, item, aov_name)
            print 'Publish Dependencies : {0}'.format(publish_dependencies)
            #publish_name = item.properties["publish_name"]
            #publish_name, extension = os.path.splitext(publish_name)
        
   
            # if the parent item has a publish path, include it in the list of
            # dependencies
            if "sg_publish_path" in item.parent.properties:
                if item.parent.properties.sg_publish_path not in publish_dependencies:
                    publish_dependencies.append(item.parent.properties.sg_publish_path)

            # handle copying of work to publish if templates are in play
            #self._copy_work_to_publish(settings, item)

            # arguments for publish registration
            self.logger.info("Registering publish...")

            #pub_dependencies = self.validate_publish_dependencies(item.context.task['id'], publish_dependencies)
            pub_dependencies = publish_dependencies

            #------------------------------------------------------------------------------------

            print 'Data name to publish: ', publish_name

            paths= {
                "node": node_name,
                "name": aov_name,
                "work_path": aov_seq_path,
                "publish_path": publish_path
            }

            self.aov_Files.append(paths)

            publish_data = {
                "tk": publisher.sgtk,
                "context": item.context,
                "comment": item.description,
                "path": publish_path,  
                "name": publish_name,
                "version_number": publish_version,
                "published_file_type": publish_type, 
                #pub_dependencies


            }
            # log the publish data for debugging
            self.logger.debug(
                "Populated Publish data...",
                extra={
                    "action_show_more_info": {
                        "label": "Publish Data",
                        "tooltip": "Show the complete Publish data dictionary",
                        "text": "<pre>%s</pre>" % (pprint.pformat(publish_data),)
                    }
                }
            )

            # Search for the actual publish name in sg
            current_version = item.properties["fields"]["version"]
            print 'current_version: ', current_version

            #actual_publish_name = item.properties["publish_name"]
            actual_publish_name = publish_name 
            print 'actual_publish_name: ', actual_publish_name


            publish = self.parent.engine.shotgun.find_one('PublishedFile',
                [["published_file_type.PublishedFileType.code", "is", "Maya Render Passes"], ['name', 'is', str(actual_publish_name)], ['version_number', 'is', current_version]],
                ["code", "path", "path_cache", "path_cache_storage", "name"])

            # create the publish and stash it in the item properties for other
            # plugins to use.
            print 'Publish found: ', publish

            sg = self.custom_sg()

            if publish:
                print 'SG: ', sg
                sg.delete('PublishedFile', publish['id'])
                print 'DELETED: ', publish['id']

            # Register the actual publish
            print 'publish data AOV {0}: '.format(aov_name), publish_data
            publish = sgtk.util.register_publish(**publish_data)
            print 'Published registered for Maya Render Pass: {0} '.format(aov_name), publish['id']

            #[Dependencies]
            data = {'upstream_published_files': pub_dependencies}
            sg.update('PublishedFile',  publish['id'], data)
            if item.get_thumbnail_as_path():
                print 'Uploading new thumbnail...'
                self.parent.shotgun.upload_thumbnail("PublishedFile", publish["id"], item.get_thumbnail_as_path())
                print 'Publish updated!'



        """
        if not publish:
            # Register the actual publish
            publish = sgtk.util.register_publish(**publish_data)
            print 'Published: ', publish
            print 'Data: ', publish_data
        else:
            print 'Publish already registered, updating data...'

            #
            filters = [["code", "is", publish_type]]
            sg_published_file_type = self.parent.shotgun.find_one('PublishedFileType', filters=filters)
            print 'sg_published_file_type: ', publish_type, ' : ', sg_published_file_type

            thumbnail_path = item.get_thumbnail_as_path()
            print 'thumbnail_path: ', thumbnail_path

            #Update the published found with current data
            data = {
                "dependency_paths": pub_dependencies
            }

            if item.description:
                data["description"] = item.description

            print 'Updating published file...'
            publish_update = self.parent.shotgun.update(publish['type'], publish['id'], data)
            print 'Data result: ', publish_update
            print 'Data updated: ', data

            if thumbnail_path:            
                print 'Uploading new thumbnail...'
                self.parent.shotgun.upload_thumbnail("PublishedFile", publish["id"], thumbnail_path)
                print 'Publish updated!'
        """

        aov_cmd=self.copy_aovs(self.aov_Files)
        self._send_job(aov_cmd)
        self.updateNodes(nuke_publish_path, self.aov_Files)
        self.returnNodes(self.aov_Files)


        item.properties["sg_publish_data"] = publish
        # item.properties.sg_publish_data = sgtk.util.register_publish(**publish_data)

        self.logger.info("Publish registered!")


    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent

        # get the data for the publish that was just created in SG
        publish_data = item.properties.sg_publish_data

        # ensure conflicting publishes have their status cleared
        #publisher.util.clear_status_for_conflicting_publishes(
            #item.context, publish_data)

        """
        self.logger.info(
            "Cleared the status of all previous, conflicting publishes")

        path = item.properties["maya_passes_path"]
        self.logger.info(
            "Publish created for file: %s" % (path,),
            extra={
                "action_show_in_shotgun": {
                    "label": "Show Publish",
                    "tooltip": "Open the Publish in Shotgun.",
                    "entity": publish_data
                }
            }
        )

        """
    def get_publish_template(self, settings, item):
        """
        Get a publish template for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A template representing the publish path of the item or
            None if no template could be identified.
        """

        return item.get_property("publish_template")



    def get_publish_dependencies(self, settings, item, aov_name):
        """
        Get publish dependencies for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A list of file paths representing the dependencies to store in
            SG for this publish

        """
        sg=self.custom_sg()
        engine= sgtk.platform.current_engine()
        user= engine.context.user
        project=engine.context.project
        entity=engine.context.entity
        entity_type=engine.context.entity["type"].lower()
        task= engine.context.task
     

        #Tomar el Valor de Maya
        aovs= self.get_nodes_to_export()

        for a in aovs:
            if a["aov"] == aov_name:
                maya_work_path= a["upstream_file"]

        #verify that the publish doesnt exist
        publish_name = os.path.basename(maya_work_path).split('.')[0][:-5]
        maya_work_tmpt=engine.get_template_by_name("maya_{0}_work".format(entity_type))
        maya_fields=maya_work_tmpt.get_fields(maya_work_path)
        current_version= maya_fields["version"]
            
        filters = [["published_file_type.PublishedFileType.code", "is", "Maya Scene"], ['name', 'is', publish_name], ['version_number', 'is', current_version]]
        publish_file = engine.shotgun.find_one('PublishedFile', filters, ["path"])
    

        if publish_file:
            publish_path= publish_file ["path"]["local_path_linux"]
            dependencies = [publish_file]

            print "#"*30,dependencies

            return dependencies

        else:

            #[Published File Data]
            root= "/nfs/ollinvfx/Project/"
            publish_template=engine.get_template_by_name("maya_{0}_publish".format(entity_type))
            publish_path=publish_template.apply_fields(maya_fields)  
            path_cache= publish_path.replace(root,"")
            code=os.path.basename(publish_path)
            publish_type_name= "Maya Scene"
            fields= ["published_file_type"]
            filters= [['published_file_type', 'name_contains', publish_type_name]]
            pf=sg.find_one('PublishedFile',filters, fields)
            publish_type= pf["published_file_type"]

            #Create Published FIle
            maya_data = {

                "code": code,
                "created_by" : user,
                "description": 'Maya light scene',
                "entity": entity,
                "published_file_type": publish_type,
                "name": publish_name,
                "path": {'content_type': None,
                'link_type': 'local',
                'local_path': publish_path,
                'local_path_linux': publish_path,},
                'path_cache' : path_cache,
                'project': project,
                'task': task,
                'image': item.get_thumbnail_as_path(), 
                #'upstream_published_files': [dependency],
                'version_number': current_version
            }
            print maya_data
            publish_file =sg.create("PublishedFile", maya_data)


            publish_path= publish_file ["path"]["local_path_linux"]
            dependencies = [publish_file]

            publish_folder = os.path.dirname(publish_path)
            ensure_folder_exists(publish_folder)
            copy_file(maya_work_path, publish_path)

            return dependencies


    def copy_aovs(self, aovs_paths):
        engine=self.parent.engine
        sg=self.custom_sg()
        user_id= engine.context.user["id"]
        usr=sg.find_one("HumanUser", [["id", "is", user_id]], ["login"])
        user=usr['login']
       
        aov_publishDir= os.path.dirname(os.path.dirname(os.path.dirname(aovs_paths[0]["publish_path"])))

        script_name= os.path.basename(nuke.root().name())
        script_name=script_name.split(".")[0]
        version= script_name.split("_")[-1]

       
        #[Python File]
        py_path= aov_publishDir + os.path.sep + "." + "publish_AOVS" + ".py"
        print "PYTHON_PATH:",py_path

        if not os.access(os.path.dirname(py_path),os.F_OK):
            os.makedirs(os.path.dirname(py_path), 0775)

        libs= """import os
import shutil\n
"""
        with open (py_path, 'w') as f:
                    f.write(libs) 

        for aov in aovs_paths:
            publishPath= aov["publish_path"]
            aov_publishDir= os.path.dirname(publishPath)
            py_cmd= """aov_work_path= '{0}'
aov_publish_path= '{1}'

aov_work_dir= os.path.dirname(aov_work_path)
aov_publish_dir= os.path.dirname(aov_publish_path)
aov_publish_name= os.path.basename(aov_publish_path)

if not os.access(aov_publish_dir,os.F_OK):
    os.makedirs(aov_publish_dir, 0775)

files=[f for f in os.listdir(aov_work_dir) if os.path.isfile(os.path.join(aov_work_dir, f))]
for f in files:
    name=f.split(".")[0]
    nf=f.replace(name, aov_publish_name.split(".")[0])
    publish_path= aov_publish_dir+ os.path.sep + nf
    work_path= aov_work_dir + os.path.sep + f  
    if not os.access(publish_path, os.F_OK):
        shutil.copy(work_path,publish_path)\n
            
""".format(aov["work_path"], aov["publish_path"], version)


            with open (py_path, 'a') as f:
                f.write(py_cmd) 


        with open (py_path, 'a') as f:
                f.write("os.remove('{0}')".format(py_path))


        #[Command]
        py_cmd = '-SubmitCommandLineJob'
        py_cmd += ' -executable /usr/bin/python2'
        py_cmd += ' -arguments ' + py_path
        py_cmd += ' -pool "publish"'
        py_cmd += ' -name "Publish AOVS EXR to shotgun"'
        py_cmd += ' -prop BatchName=' +str(script_name) 
        py_cmd += ' -prop ConcurrentTasks=1'
        py_cmd += ' -prop UserName="{0}"'.format(user)
        return py_cmd


    def updateNodes(self, nuke_publishPath, aov_Files):
        
        for aov in self.aov_Files:
            nodeName= aov["node"]
            aovName= aov["name"]
            publishPath= aov["publish_path"]

            currentNode= nuke.toNode(nodeName)
            currentNode['label'].setValue(aovName)
            currentNode['file'].setValue(publishPath)

        nuke.scriptSave(nuke_publishPath)

    def returnNodes(self, aovs_list):
        aov_ls = aovs_list
        for aov in aov_ls:
            nodeName= aov["node"]
            aovName= aov["name"]
            path= aov["work_path"]

            currentNode= nuke.toNode(nodeName)
            currentNode['label'].setValue(aovName)
            currentNode['file'].setValue(path)


        nuke.scriptSave()

        

    def _send_job(self, deadline_cmd):
        import subprocess
        try:
            deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
            process = subprocess.Popen(deadline + " " + deadline_cmd, 
                                       shell=True, stdout=subprocess.PIPE)

            out, err = process.communicate()

            print 'Out: ', out 
            print 'Err: ', err

            return out

        except Exception, e:
            publish_errors.append(str(e))









