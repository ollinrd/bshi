# Copyright (c) 2017 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import pprint
import traceback
import subprocess
import sgtk
from sgtk.util.filesystem import copy_file, ensure_folder_exists
import sys
HookBaseClass = sgtk.get_hook_baseclass()


class BasicFilePublishPlugin(HookBaseClass):
    """
    Plugin for creating generic publishes in Shotgun.

    This plugin is typically configured to act upon files that are dragged and
    dropped into the publisher UI. It can also be used as a base class for
    other file-based publish plugins as it contains standard operations for
    validating and registering publishes with Shotgun.

    Once attached to a publish item, the plugin will key off of properties that
    drive how the item is published.

    The ``path`` property, set on the item, is the only required property as it
    informs the plugin where the file to publish lives on disk.

    The following properties can be set on the item via the collector or by
    subclasses prior to calling methods on the base class::

        ``sequence_paths`` - If set in the item properties dictionary, implies
            the "path" property represents a sequence of files (typically using
            a frame identifier such as %04d). This property should be a list of
            files on disk matching the "path". If the ``work_template`` property
            is set, and corresponds to the listed frames, fields will be
            extracted and applied to the publish_template (if set) and copied to
            that publish location.

        ``work_template`` - If set in the item properties dictionary, this
            value is used to validate ``path`` and extract fields for further
            processing and contextual discovery. For example, if configured and
            a version key can be extracted, it will be used as the publish
            version to be registered in Shotgun.

    The following properties can also be set by a subclass of this plugin via
    :meth:`Item.properties` or :meth:`Item.local_properties`.

        publish_template - If set, used to determine where "path" should be
            copied prior to publishing. If not specified, "path" will be
            published in place.

        publish_type - If set, will be supplied to SG as the publish type when
            registering "path" as a new publish. If not set, will be determined
            via the plugin's "File Type" setting.

        publish_path - If set, will be supplied to SG as the publish path when
            registering the new publish. If not set, will be determined by the
            "published_file" property if available, falling back to publishing
            "path" in place.

        publish_name - If set, will be supplied to SG as the publish name when
            registering the new publish. If not available, will be determined
            by the "work_template" property if available, falling back to the
            ``path_info`` hook logic.

        publish_version - If set, will be supplied to SG as the publish version
            when registering the new publish. If not available, will be
            determined by the "work_template" property if available, falling
            back to the ``path_info`` hook logic.

        publish_dependencies - A list of files to include as dependencies when
            registering the publish. If the item's parent has been published,
            it's path will be appended to this list.

    NOTE: accessing these ``publish_*`` values on the item does not necessarily
    return the value used during publish execution. Use the corresponding
    ``get_publish_*`` methods which include fallback logic when no property is
    set. For example, if a ``work_template`` is used, the publish version and
    name might be extracted from the template fields in the fallback logic.

    This plugin will also set an ``sg_publish_data`` property on the item during
    the ``publish`` method which may be useful for child items.

        ``sg_publish_data`` - The dictionary of publish information returned
            from the tk-core register_publish method.

    NOTE: If you have multiple plugins acting on the same item, and you need to
    access or operate on the publish data, you can extract the
    ``sg_publish_data`` from the item after calling the base class ``publish``
    method in your plugin subclass.
    """

    ############################################################################
    # standard publish plugin properties

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(
            self.disk_location,
            "icons",
            "image_sequence.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish to Shotgun"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """
        Publishes the file to Shotgun. A <b>Publish</b> entry will be
        created in Shotgun which will include a reference to the file's current
        path on disk. Other users will be able to access the published file via
        the <b><a href='%s'>Loader</a></b> so long as they have access to
        the file's location on disk.

        <h3>File versioning</h3>
        The <code>version</code> field of the resulting <b>Publish</b> in
        Shotgun will also reflect the version number identified in the filename.
        The basic worklfow recognizes the following version formats by default:

        <ul>
        <li><code>filename.v###.ext</code></li>
        <li><code>filename_v###.ext</code></li>
        <li><code>filename-v###.ext</code></li>
        </ul>

        <br><br><i>NOTE: any amount of version number padding is supported.</i>

        <h3>Overwriting an existing publish</h3>
        A file can be published multiple times however only the most recent
        publish will be available to other users. Warnings will be provided
        during validation if there are previous publishes.
        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        return {
            "File Types": {
                "type": "list",
                "default": [
                    ["Alembic Cache", "abc"],
                    ["3dsmax Scene", "max"],
                    ["NukeStudio Project", "hrox"],
                    ["Houdini Scene", "hip", "hipnc"],
                    ["Maya Scene", "ma", "mb"],
                    ["Motion Builder FBX", "fbx"],
                    ["Nuke Script", "nk"],
                    ["Photoshop Image", "psd", "psb"],
                    ["Rendered Image", "dpx", "exr"],
                    ["Texture", "tiff", "tx", "tga", "dds"],
                    ["Image", "jpeg", "jpg", "png"],
                    ["Movie", "mov", "mp4"],
                ],
                "description": (
                    "List of file types to include. Each entry in the list "
                    "is a list in which the first entry is the Shotgun "
                    "published file type and subsequent entries are file "
                    "extensions that should be associated."
                )
            },
        }

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["file.*"]

    ############################################################################
    # standard publish plugin methods

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A publish task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled
        """
        path = item.properties.path

        # log the accepted file and display a button to reveal it in the fs
        self.logger.info(
            "File publisher plugin accepted: %s" % (path,),
            extra={
                "action_show_folder": {
                    "path": path
                }
            }
        )

        # return the accepted info
        return {"accepted": True}

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish.

        Returns a boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: True if item is valid, False otherwise.
        """


        publisher = self.parent
        path = item.properties.get("path")
        engine = sgtk.platform.current_engine()
        overwrite = engine.overwrite_publish

        # ---- determine the information required to validate

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.

        publish_path, fields = self.get_publish_path(settings, item)
        publish_name = self.get_publish_name(settings, item)

        # ---- check for conflicting publishes of this path with a status

        # Note the name, context, and path *must* match the values supplied to
        # register_publish in the publish phase in order for this to return an
        # accurate list of previous publishes of this file.
        publishes = publisher.util.get_conflicting_publishes(
            item.context,
            publish_path,
            publish_name,
            filters=["sg_status_list", "is_not", None]
        )

        if publishes:

            self.logger.debug(
                "Conflicting publishes: %s" % (pprint.pformat(publishes),))

            publish_template = self.get_publish_template(settings, item)

            #if "work_template" in item.properties or publish_template:
            if ("work_template" in item.properties or
                "publish_template" in item.properties) and not overwrite:
                # templates are in play and there is already a publish in SG
                # for this file path. We will raise here to prevent this from
                # happening.
                error_msg = (
                    "Can X not validate file path. There is already a publish in "
                    "Shotgun that matches this path. Please uncheck this "
                    "plugin or save the file to a different path."
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)

            else:
                conflict_info = (
                    "If you continue, these conflicting publishes will no "
                    "longer be available to other users via the loader:<br>"
                    "<pre>%s</pre>" % (pprint.pformat(publishes),)
                )
                self.logger.warn(
                    "Found %s conflicting publishes in Shotgun" %
                        (len(publishes),),
                    extra={
                        "action_show_more_info": {
                            "label": "Show Conflicts",
                            "tooltip": "Show conflicting publishes in Shotgun",
                            "text": conflict_info
                        }
                    }
                )

        self.logger.info("A Publish will be created in Shotgun and linked to:")
        self.logger.info("  %s" % (path,))

        return True

    def validate_publish_dependencies(self, entity_id, dependencies):

        #tk = sgtk.sgtk_from_entity('Task', 31876)
        #root_path = tk.pipeline_configuration.get_local_storage_roots()['primary'] + '/'

        validation = []
        for path in dependencies:
            print 'dep: ', path
            tk = sgtk.sgtk_from_path(path)
            root_path = tk.pipeline_configuration.get_local_storage_roots()['primary'] + '/'            
            pseudo_cache_path = path.replace(root_path, '')
            filters = [['path_cache', 'in', pseudo_cache_path]]
            result = self.parent.shotgun.find_one('PublishedFile', filters)
            if result:
                validation.append(result)

        return validation

    def extract_first_and_last_frame_number(self, dir_path):
        firstF = 1
        lastF = 1
        for root, dirs, files in os.walk(dir_path):
           nums = []
           for fi in files:
               tokens = fi.split('.')
               if len(tokens)>2:
                   num = tokens[-2]
                   if num.isdigit():
                       nums.append(num)
           if len(nums):
               firstF = min(nums)
               lastF = max(nums)  

        return firstF, lastF

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """
        publisher = self.parent
        import os
        import pprint
        import traceback
        import subprocess
        import sgtk
        # show custom range ui
        if item.context.entity['type'] == 'Shot':
            fields = ['sg_head_in', 'sg_tail_out']
            filters = [['id', 'is', item.context.entity['id']]]
            shot = self.parent.shotgun.find_one('Shot',filters,fields)
            first_frame = self.FIRST = shot['sg_head_in']
            last_frame = self.LAST = shot['sg_tail_out']
            try:
                import nuke
                p = nuke.Panel('Select frame range')
                p.addSingleLineInput('First:', '{0}'.format(first_frame))
                p.addSingleLineInput('Last:', '{0}'.format(last_frame))
                p.show()
                first_frame = self.FIRST = int(p.value('First:'))
                last_frame = self.LAST = int(p.value('Last:'))
            except:
                pass
        ctx = self.parent.context
        ctx.sg_head_in = first_frame
        ctx.sg_tail_out = last_frame


        import nuke
        import sys
        import os
        from stat import S_ISREG, ST_CTIME, ST_MODE
        from subprocess import Popen, PIPE
        import tempfile
        import subprocess
        import uuid
        # ---- determine the information required to publish

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.
        work_area_path = item.properties['sg_writenode']['cached_path'].value()
        publish_type = self.get_publish_type(settings, item)
        publish_name = self.get_publish_name(settings, item)        
        publish_version = self.get_publish_version(settings, item)
        publish_path, fields = self.get_publish_path(settings, item)
        publish_dependencies = self.get_publish_dependencies(settings, item)
        publish_name, extension = os.path.splitext(publish_name)        

        entity = self.parent.context.to_dict()['entity']
        entity = self.parent.shotgun.find_one('Shot', [['id', 'is', entity['id']]],
                                              ['code', 'sg_head_in', 'sg_tail_out',
                                               'sg_str_timecode', 'sg_output_alpha',
                                               'sg_main_plate_name', 'sg_sequence',
                                               'sg_resx','sg_resy'])

        # if the parent item has a publish path, include it in the list of
        # dependencies
        if "sg_publish_path" in item.parent.properties:
            publish_dependencies.append(item.parent.properties.sg_publish_path)

        # arguments for publish registration
        self.logger.info("Registering publish RENDER IMAGES...")
        print 'Registering publish RENDER IMAGES...'
        import nuke
        script_path = nuke.root().name().replace("/", os.path.sep)
        nk_publish = publisher.shotgun.find_one("PublishedFile",
                                                [["code", "is", os.path.basename(script_path)]],
                                                ["code", "path", "path_cache",
                                                 "path_cache_storage", "name"])
        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": publish_dependencies
        }
        # log the publish data for debugging
        self.logger.debug(
            "Populated Publish data...",
            extra={
                "action_show_more_info": {
                    "label": "Publish Data",
                    "tooltip": "Show the complete Publish data dictionary",
                    "text": "<pre>%s</pre>" % (pprint.pformat(publish_data),)
                }
            }
        )
        p_folder = os.path.dirname(publish_path)
        w_folder = os.path.dirname(work_area_path)
        self.parent.ensure_folder_exists(p_folder)
        py_path = self.parent.engine.context.tank.pipeline_configuration.get_path()
        tk = sgtk.sgtk_from_entity('Project', self.parent.context.to_dict()['project']['id'])
        self.conf_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        burnin_nk = '{0}/resources/ovfx_BSHI_Burnin.nk'.format(self.conf_path)
        r_path = work_area_path
        p_path = publish_path
        timeco = entity['sg_str_timecode']
        roto = False

        # main_plate, main_plate_last_frame = self.get_mainplate_reference(entity)
        main_plate = '/tmp/test'
        main_plate_last_frame = 1058
        if self.parent.context.to_dict()['task']:
            task = self.parent.shotgun.find_one('Task', 
                                                [['id', 'is', self.parent.context.to_dict()['task']['id']]],
                                                ['step.Step.short_name'])

            if 'rot' in task['step.Step.short_name'].lower():
                roto = True
            writeN = 'PublisherEXT'
            ALPHA = False
            if entity['sg_output_alpha']:
                ALPHA = True
                writeN = 'PublisherPNG'
            params = {'py_path': py_path,
                      'task': self.parent.context.to_dict()['task'], 
                      'w_folder': w_folder,
                      'p_folder': p_folder,
                      'publish_path': publish_path,
                      'publish_data': publish_data,
                      'nk_publish': nk_publish,
                      'nuke_node': burnin_nk,
                      'source_path': r_path,
                      'head_in': first_frame,
                      'tail_out': last_frame,
                      'publish_path': p_path,
                      'file_name': os.path.basename(script_path).split('.')[0],
                      'script_path': script_path,
                      'write_node': writeN,
                      'main_plate_path': main_plate,
                      'mp_last_f': main_plate_last_frame,
                      'slate_tc': timeco,
                      'rot': roto,
                      'alph': ALPHA
                      }
            publisher_script = '''
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import nuke
import sys
import os
from stat import S_ISREG, ST_CTIME, ST_MODE
from subprocess import Popen, PIPE
import tempfile
import subprocess
import uuid

sys.path.append('{py_path}/install/core/python')
import sgtk
tk_nuke = sgtk.platform.current_engine()
if not tk_nuke:
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    cdm = sgtk.util.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.get_user()
    sgtk.set_authenticated_user(user)
    task_related = {task}
    tk = sgtk.sgtk_from_entity(task_related['type'], task_related['id'])    
    ctx = tk.context_from_entity(task_related['type'], task_related['id'])    
    sg = tk.shotgun
    tk_nuke = sgtk.platform.start_engine("tk-nuke", tk, ctx)

nuke.root()['colorManagement'].setValue('OCIO')
nuke.root()['OCIO_config'].setValue('aces_1.0.3')
nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
nuke.root()['int16Lut'].setValue('ACES - ACEScc')
nuke.root()['logLut'].setValue('Input - ADX - ADX10')
nuke.root()['floatLut'].setValue('ACES - ACES2065-1')

nuke.nodePaste('{nuke_node}')
# set themainplate metadata source
metadata_node = nuke.toNode('PUBLISHER').node('internal').node('Meta')
metadata_node['file'].setValue(\"{main_plate_path}\")
metadata_node['first'].setValue({head_in})
metadata_node['last'].setValue({mp_last_f})
metadata_node['origfirst'].setValue({head_in})
metadata_node['origlast'].setValue({mp_last_f})
metadata_node['colorspace'].setValue('ACES - ACEScg')
if '{slate_tc}' == '00:00:00:00':
    metadata_node['disable'].setValue(True)
# metadata timeCode
timecode = '{slate_tc}'
tc = nuke.toNode('PUBLISHER').node("internal").node("metaTimeCode")
tc['startcode'].setValue(timecode)
tc['useFrame'].setValue(1)
if {head_in}:
    head_in = {head_in}
if {tail_out}:
    tail_out = {tail_out}
tc['frame'].setValue(head_in - 1)
sw_startFrame = head_in
sw_endFrame = {mp_last_f}
nuke.frame(sw_startFrame)
switch = nuke.toNode('PUBLISHER').node("internal").node("Switch1")
switch_which= switch['which']
switch_which.setAnimated()
switch_which.setValueAt( 0, sw_startFrame )
switch_which.setValueAt( 1, sw_endFrame +1 )
nuke.root()['first_frame'].setValue(head_in)
nuke.root()['last_frame'].setValue(tail_out)
publisher = nuke.toNode('PUBLISHER')
read = nuke.nodes.Read(name="read_source", file='{source_path}', first= head_in, last=tail_out)
#read['raw'].setValue(True)
read['colorspace'].setValue('ACES - ACEScg')
publisher.setInput(0, read)
write_n = publisher.node('internal').node('{write_node}')
write_n['file'].setValue('{publish_path}')
if {rot}:
    write_n['channels'].setValue('rgba')
if {alph}:
    write_n['channels'].setValue('rgb')
prefixx = '/nfs/ovfxToolkit/TMP/'
dictPath = prefixx + "Reformat_publish_{file_name}" + str(uuid.uuid4()) +'.nk'
nuke.nuke.scriptSaveAs(dictPath)
flags = '" -t -m 2 -V -x -X PUBLISHER.internal.{write_node}'
flags += ' <QUOTE>' + dictPath + '<QUOTE> <STARTFRAME%4>,<STARTFRAME%4>"'
nuke_cmd = "/opt/Thinkbox/Deadline10/bin/deadlinecommand -SubmitCommandLineJob -executable"
nuke_cmd += " /nfs/ovfxToolkit/Software/Nuke12.0v3/Nuke12.0 -arguments "
nuke_cmd += flags
nuke_cmd += ' -frames ' + str(head_in) + '-' + str(tail_out) + ' -pool publish'
nuke_cmd += ' -name "EXR publish render" -prop ConcurrentTasks=3'
nuke_cmd += " -prop BatchName={file_name}"
process = subprocess.Popen(nuke_cmd, 
                           shell=True, stdout=subprocess.PIPE)
out, err = process.communicate()
print out, err
job_id = out.split("JobID=")[-1].split("The job")[0].strip()
print 'J_ID: ', job_id
    '''.format(**params)
        if 'PreComp' in item.properties['sg_writenode']['tk_profile_list'].value():
            job_name = 'Publish Precomp {0}'.format(os.path.basename(publish_path).split('.')[0])
        else:
            job_name = 'Publish EXR submitter'
        prefixx = '/nfs/ovfxToolkit/TMP/'
        (file_, dictPath) = tempfile.mkstemp(prefix=prefixx + "Publish_exr_submitter_{0}".format(entity['code']),
                                             suffix='.py')
        submitter_py = open(dictPath, "w")
        submitter_py.write(publisher_script)
        submitter_py.close()
        if '/nfs/ovfxToolkit/Resources/site-packages' not in sys.path:
            sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
        import yaml
        proj = self.parent.context.to_dict()['project']
        tk = sgtk.sgtk_from_entity('Project', proj['id'])
        self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                      "project_specs.yml")
        with open(p_specs_path, 'r') as stream:
            try:
                self.specs = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        nuke_version = self.specs['main_nuke_path']
        cmd = nuke_version
        cmd += ' -t {0} {1} {2}'.format(dictPath, nuke.root().name(), py_path)
        print 'comando:', cmd
        # nuke_cmd = " -SubmitCommandLineJob -executable"
        # nuke_cmd += " {0} -arguments ".format(nuke_version)
        # nuke_cmd += ' "-t {0} {1} {2}" '.format(dictPath, nuke.root().name(), py_path)
        # nuke_cmd += ' -name "{0}" -pool comp'.format(job_name)
        # nuke_cmd += " -prop BatchName={0}".format(batchName)
        # subprocess.call('chmod -R 777 {0}'.format(dictPath), shell=True)
        # deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
        # process = subprocess.Popen(deadline + " " + nuke_cmd,
        # shell=True, stdout=subprocess.PIPE)
        job_id = None
        job_id = self.send_EXR_JOB(cmd)
        if job_id is None:
            job_id = self.send_EXR_JOB(cmd)
        print 'Deadline send!!! \n\n'
        actual_publish_name = os.path.basename(publish_path)
        publish = self.parent.shotgun.find_one("PublishedFile",
                                               [["code", "is", actual_publish_name]],
                                               ["code", "path", "path_cache",
                                                "path_cache_storage", "name"])

        # create the publish and stash it in the item properties for other
        # plugins to use.
        if not publish:
            # Register the actual publish
            publish = sgtk.util.register_publish(**publish_data)
            data = {'upstream_published_files': [nk_publish]}
            publish  = self.parent.shotgun.update(publish['type'], publish['id'], data)
            print 'NEW Publish:', publish
            print 'Publish already registered, updating data...'
        else:
            print 'Publish already registered, updating data...'
            import sys
            sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
            from shotgun_api3 import Shotgun
            SG_URL = 'https://0vfx.shotgunstudio.com'
            TANK_SCRIPT_NAME= "Toolkit"
            TANK_SCRIPT_KEY= "b20e30ae264400f844d4197b5805d1105ba6ddf06fd733b51a74b8fa290cbc73"
            shgn = Shotgun(SG_URL, 
                           TANK_SCRIPT_NAME, 
                           TANK_SCRIPT_KEY)
            shgn.delete("PublishedFile", publish["id"])

            publish = sgtk.util.register_publish(**publish_data)
            data = {'upstream_published_files': [nk_publish]}
            publish = self.parent.shotgun.update(publish['type'],
                                                 publish['id'], data)
        item.properties["sg_publish_data"] = publish
        item.properties['job_id'] = job_id

    def send_EXR_JOB(self, cmd):
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        out, err = process.communicate()
        print 'Error:', err
        job_id = None
        for line in out.splitlines():
            if 'J_ID' in line:
                job_id = line.split(' ')[-1]
                return job_id

    # item.properties.sg_publish_data = sgtk.util.register_publish(**publish_data)
    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent
        if 'PreComp' not in item.properties['sg_writenode']['tk_profile_list'].value():
            # get the data for the publish that was just created in SG
            publish_data = item.properties.sg_publish_data

            self.logger.info(
                "Cleared the status of all previous, conflicting publishes")

            path = item.properties.path
            self.logger.info(
                "Publish created for file: %s" % (path,),
                extra={
                    "action_show_in_shotgun": {
                        "label": "Show Publish",
                        "tooltip": "Open the Publish in Shotgun.",
                        "entity": publish_data
                    }
                }
            )
        print " PUBLISH READY"

    def get_publish_template(self, settings, item):
        """
        Get a publish template for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A template representing the publish path of the item or
            None if no template could be identified.
        """

        return item.get_property("publish_template")

    def get_publish_type(self, settings, item):
        """
        Get a publish type for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish type for

        :return: A publish type or None if one could not be found.
        """

        # publish type explicitly set or defined on the item        
        publish_type = item.get_property("publish_type")
        if publish_type:
            return publish_type

        # fall back to the path info hook logic
        publisher = self.parent
        path = item.properties.path

        if 'preComp' in path:
            return 'Precomp Render'

        # get the publish path components
        path_info = publisher.util.get_file_path_components(path)

        # determine the publish type
        extension = path_info["extension"]

        # ensure lowercase and no dot
        if extension:
            extension = extension.lstrip(".").lower()

            for type_def in settings["File Types"].value:

                publish_type = type_def[0]
                file_extensions = type_def[1:]

                if extension in file_extensions:
                    # found a matching type in settings. use it!
                    return publish_type

        # --- no pre-defined publish type found...

        if extension:
            # publish type is based on extension
            publish_type = "%s File" % extension.capitalize()
        else:
            # no extension, assume it is a folder
            publish_type = "Folder"

        return publish_type

    def get_publish_path(self, settings, item):
        """
        Get a publish path for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish path for

        :return: A string representing the output path to supply when
            registering a publish for the supplied item

        Extracts the publish path via the configured work and publish templates
        if possible.
        """

        # publish type explicitly set or defined on the item
        publish_path = item.get_property("publish_path")

        if publish_path:
            return publish_path

        # fall back to template/path logic
        path = item.properties['sg_writenode']['cached_path'].value()

        work_template = item.properties.get("work_template")        
        publish_template = self.get_publish_template(settings, item)
        work_fields = []
        publish_path = None
        import nuke
        engine = sgtk.platform.current_engine()
        current_nuke_path = nuke.root().name()
        template = engine.get_template_by_name('nuke_shot_work')                
        fields = template.get_fields(current_nuke_path)                
        # We need both work and publish template to be defined for template
        # support to be enabled.
        if work_template and publish_template:

            if work_template.validate(path):
                work_fields = work_template.get_fields(path)

            # If nodepath is a precomp, update fields with missing values
            if 'preComp' in path:

                ext = os.path.splitext(path)[1]
                work_fields['Extension'] = ext[1:]

                
                template = engine.get_template_by_name('nuke_shot_work')                
                fields = template.get_fields(current_nuke_path)                
                work_fields['version'] = fields['version']
                self.name_w = work_fields['name']

            missing_keys = publish_template.missing_keys(work_fields)           

            if missing_keys:
                self.logger.warning(
                    "Not enough keys to apply work fields (%s) to "
                    "publish template (%s)" % (work_fields, publish_template))
            else:
                publish_path = publish_template.apply_fields(work_fields)
                self.logger.debug(
                    "Used publish template to determine the publish path: %s" %
                    (publish_path,)
                )
        else:
            self.logger.debug("publish_template: %s" % publish_template)
            self.logger.debug("work_template: %s" % work_template)

        if not publish_path:
            publish_path = path
            self.logger.debug(
                "Could not validate a publish template. Publishing in place.")


        return publish_path , fields

    def get_mainplate_reference(self, entity):
        if entity['sg_main_plate_name']:
            elemName = str(entity['sg_main_plate_name'].split('element')[-1])[1:]
        else:
            elemName = 'mp01'
        seq = self.parent.shotgun.find_one('Sequence',[['id', 'is', entity['sg_sequence']['id']]], ['code'])
        # TODO: QUITAR EL 0 DEL NOMBRE DE LA SECUENCIA
        element_base_path = '/nfs/ollinvfx/Project/{2}/sequences/{0}/{1}/editorial/publish/elements/{2}_{1}_element_{3}/'.format(
            seq['code'], entity['code'], 'BSHI', elemName)

        print element_base_path

        # From base path, fetch versions and select last version in folder
        if os.path.exists(element_base_path):
            versions_ = os.listdir(element_base_path)
            versions_.sort()
            version_p = versions_[-1]

            exten = 'exr'
            scan_path = element_base_path + '{0}/{1}/{2}x{3}/{6}_{4}_element_{5}_{0}.%04d.{1}'.format(version_p, exten,
                                                                                                      entity['sg_resx'],
                                                                                                      entity['sg_resy'],
                                                                                                      entity['code'],
                                                                                                      elemName, 'BSHI')

            elements_template = self.parent.engine.get_template_by_name("elements_ext")
            fields = elements_template.get_fields(scan_path)

            files = self.parent.sgtk.paths_from_template(elements_template, fields, ['SEQ'])
            print 'files: ', len(files)
            files =  sorted(files)

            element_last_frame = elements_template.get_fields(files[-1])
            element_last_frame = element_last_frame['SEQ']
            print 'lastframe: ', element_last_frame


            # print os.path.dirname(scan_path)
            if os.path.exists(os.path.dirname(scan_path)):
                if len(os.listdir(os.path.dirname(scan_path))) < 1:
                    raise ValueError(
                        'Content is empty!!!, No files for exr path for the Main plate name (sg_main_plate_name) value for the shot: {0} in Shotgun.'.format(
                            entity['code']))
            else:
                raise ValueError(
                    'Path not found: {1}<BR> Folder path is missing for the Main plate name (sg_main_plate_name) value for the shot: {0} in Shotgun.'.format(
                        entity['code'], os.path.dirname(scan_path)))
        else:
            raise ValueError('No Main plate for current Shot.{0}'.format(element_base_path))

        return scan_path, element_last_frame
    def get_publish_version(self, settings, item):
        """
        Get the publish version for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish version for

        Extracts the publish version via the configured work template if
        possible. Will fall back to using the path info hook.
        """

        # fall back to the template/path_info logic
        publisher = self.parent
        path = item.properties.path

        # Return ouput precomp value from path 
        if 'preComp' in path:
            import nuke
            current_nuke_path = nuke.root().name()

            engine = sgtk.platform.current_engine()                
            template = engine.get_template_by_name('nuke_shot_work')                
            fields = template.get_fields(current_nuke_path)                
            version = fields['version']            
            return version


        # publish version explicitly set or defined on the item
        publish_version = item.get_property("publish_version")
        if publish_version:
            return publish_version

        work_template = item.properties.get("work_template")
        work_fields = None
        publish_version = None

        if work_template:
            if work_template.validate(path):
                self.logger.debug(
                    "Work file template configured and matches file.")
                work_fields = work_template.get_fields(path)

        if work_fields:
            # if version number is one of the fields, use it to populate the
            # publish information
            if "version" in work_fields:
                publish_version = work_fields.get("version")
                self.logger.debug(
                    "Retrieved version number via work file template.")

        else:
            self.logger.debug(
                "Using path info hook to determine publish version.")
            publish_version = publisher.util.get_version_number(path)
            if publish_version is None:
                publish_version = 1

        return publish_version

    def get_publish_name(self, settings, item):
        """
        Get the publish name for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish name for

        Uses the path info hook to retrieve the publish name.
        """

        # fall back to the path_info logic
        publisher = self.parent
        path = item.properties.path

        # Return ouput precomp value from path 
        if 'preComp' in path:
            work_template = item.properties.get("work_template")
            work_fields = work_template.get_fields(path)
            return work_fields.get('output')

        # publish name explicitly set or defined on the item
        publish_name = item.get_property("publish_name")
        
        if publish_name:
            return publish_name

        if "sequence_paths" in item.properties:
            # generate the name from one of the actual files in the sequence
            name_path = item.properties.sequence_paths[0]
            is_sequence = True
        else:
            name_path = path
            is_sequence = False

        return publisher.util.get_publish_name(
            name_path,
            sequence=is_sequence
        )

    def get_publish_dependencies(self, settings, item):
        """
        Get publish dependencies for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A list of file paths representing the dependencies to store in
            SG for this publish
        """

        # local properties first
        dependencies = item.local_properties.get("publish_dependencies")

        # have to check against `None` here since `[]` is valid and may have
        # been explicitly set on the item
        if dependencies is None:
            # get from the global item properties.
            dependencies = item.properties.get("publish_dependencies")

        if dependencies is None:
            # not set globally or locally on the item. make it []
            dependencies = []

        return dependencies

    def _get_next_version_info(self, path, item):
        """
        Return the next version of the supplied path.

        If templates are configured, use template logic. Otherwise, fall back to
        the zero configuration, path_info hook logic.

        :param str path: A path with a version number.
        :param item: The current item being published

        :return: A tuple of the form::

            # the first item is the supplied path with the version bumped by 1
            # the second item is the new version number
            (next_version_path, version)
        """

        if not path:
            self.logger.debug("Path is None. Can not determine version info.")
            return None, None

        publisher = self.parent

        # if the item has a known work file template, see if the path
        # matches. if not, warn the user and provide a way to save the file to
        # a different path
        work_template = item.properties.get("work_template")
        work_fields = None

        if work_template:
            if work_template.validate(path):
                work_fields = work_template.get_fields(path)

        # if we have template and fields, use them to determine the version info
        if work_fields and "version" in work_fields:

            # template matched. bump version number and re-apply to the template
            work_fields["version"] += 1
            next_version_path = work_template.apply_fields(work_fields)
            version = work_fields["version"]

        # fall back to the "zero config" logic
        else:
            next_version_path = publisher.util.get_next_version_path(path)
            cur_version = publisher.util.get_version_number(path)
            if cur_version is not None:
                version = cur_version + 1
            else:
                version = None

        return next_version_path, version

    def _save_to_next_version(self, path, item, save_callback):
        """
        Save the supplied path to the next version on disk.

        :param path: The current path with a version number
        :param item: The current item being published
        :param save_callback: A callback to use to save the file

        Relies on the _get_next_version_info() method to retrieve the next
        available version on disk. If a version can not be detected in the path,
        the method does nothing.

        If the next version path already exists, logs a warning and does
        nothing.

        This method is typically used by subclasses that bump the current
        working/session file after publishing.
        """

        (next_version_path, version) = self._get_next_version_info(path, item)

        if version is None:
            self.logger.debug(
                "No version number detected in the publish path. "
                "Skipping the bump file version step."
            )
            return None

        self.logger.info("Incrementing file version number...")

        # nothing to do if the next version path can't be determined or if it
        # already exists.
        if not next_version_path:
            self.logger.warning("Could not determine the next version path.")
            return None
        elif os.path.exists(next_version_path):
            self.logger.warning(
                "The next version of the path already exists",
                extra={
                    "action_show_folder": {
                        "path": next_version_path
                    }
                }
            )
            return None

        # save the file to the new path
        save_callback(next_version_path)
        self.logger.info("File saved as: %s" % (next_version_path,))

        return next_version_path
