# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import sgtk
import maya.cmds as cmds
import maya.mel as mel

HookBaseClass = sgtk.get_hook_baseclass()


class CameraPublishPlugin(HookBaseClass):
    """
    Plugin for publish cameras

    """

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "icon_256.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish The cameras"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """

        Test the new plug %s
        

        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        # inherit the settings from the base publish plugin
        base_settings = super(CameraPublishPlugin, self).settings or {}

        # settings specific to this class
        maya_publish_settings = {
            "Publish Template": {
                "type": "template",
                "default": None,
                "description": "Template path for published cameras files. Should"
                               "correspond to a template defined in "
                               "templates.yml.",
            }
        }

        # update the base settings
        base_settings.update(maya_publish_settings)

        return base_settings

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["maya.session.cameras"]

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A submit for review task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled. 
            This plugin makes use of the tk-multi-reviewsubmission app; if this
            app is not available then the item will not be accepted, this method
            will return a dictionary with the accepted field that has a value of
            False.

            Several properties on the item must also be present for it to be
            accepted. The properties are 'path', 'publish_name', 'color_space',
            'first_frame' and 'last_frame'
        """
        path = _session_path()  
        step=self.parent.context.step['name']      
        project =  self.parent.engine.shotgun.find_one('Project', [['id', 'is', self.parent.context.project['id']]], ['code'])
        entity_name = self.parent.context.entity["name"].replace('-', '_')
        cam_group = "|{0}_{1}_cam_grp".format(project["code"], entity_name)        
        cam_grp_exists = [node for node in cmds.ls(assemblies=True, long=True) if node == cam_group]


        

        print 'cam_group: ', cam_group
        print 'cam_grp_exists: ', cam_grp_exists


        if step == 'matchmove':

        
            if cam_grp_exists:            
                accepted = True
                publisher = self.parent
                template_name = settings["Publish Template"].value
                # ensure a work file template is available on the parent item
                work_template = item.parent.properties.get("work_template")
                if not work_template:
                    self.logger.debug(
                        "A work template is required for the session item in order to "
                        "publish session cameras. Not accepting session cam item."
                    )
                    accepted = False

                # ensure the publish template is defined and valid and that we also have
                publish_template = publisher.get_template_by_name(template_name)
                if not publish_template:
                    self.logger.debug(
                        "The valid publish template could not be determined for the "
                        "session camera item. Not accepting the item."
                    )
                    accepted = False            
                # we've validated the publish template. add it to the item properties
                # for use in subsequent methods
                item.properties["publish_template"] = publish_template

                # log the accepted file and display a button to reveal it in the fs
                self.logger.info(
                    "Camera  plugin accepted for the path: %s" % (path),
                        
                    )
                return {
                    "accepted": accepted,
                    "checked": True
                }

            else:
                self.logger.info(
                    "Camera  plugin not accepted for the path: %s" % (path),
                        
                    )

                return {
                    "accepted": False,
                    "visible": False,
                    "checked": True
                }
        elif step == 'layout':
            #Layout Accept

            CAMERAS_LAYOUT= LayoutCamera()

            if  CAMERAS_LAYOUT:
                accepted = True
                publisher = self.parent
                template_name = settings["Publish Template"].value
                # ensure a work file template is available on the parent item
                work_template = item.parent.properties.get("work_template")
                if not work_template:
                    self.logger.debug(
                        "A work template is required for the session item in order to "
                        "publish session cameras. Not accepting session cam item."
                    )
                    accepted = False

                # ensure the publish template is defined and valid and that we also have
                publish_template = publisher.get_template_by_name(template_name)
                if not publish_template:
                    self.logger.debug(
                        "The valid publish template could not be determined for the "
                        "session camera item. Not accepting the item."
                    )
                    accepted = False            
                # we've validated the publish template. add it to the item properties
                # for use in subsequent methods
                item.properties["publish_template"] = publish_template

                # log the accepted file and display a button to reveal it in the fs
                self.logger.info(
                    "Camera  plugin accepted for the path: %s" % (path),
                        
                    )
                return {
                    "accepted": accepted,
                    "checked": True
                }

            else:
                self.logger.info(
                    "Camera  plugin not accepted for the path: %s" % (path),
                        
                    )

                return {
                    "accepted": False,
                    "visible": False,
                    "checked": True
                }


    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish. Returns a
        boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        :returns: True if item is valid and not in proxy mode, False otherwise.
        """

        # the render task will always render full-res frames when publishing. If we're
        # in proxy mode in Nuke, that task will fail since there will be no full-res
        # frames rendered. The exceptions are if there is no proxy_render_template set
        # in the tk-nuke-writenode app, then the write node app falls back on the
        # full-res template. Or if they rendered in full res and then switched to
        # proxy mode later. In this case, this is likely user error, so we catch it.
        print 'MAYA camera validate'
        path = _session_path()

        # ---- ensure the session has been saved

        if not path:
            # the session still requires saving. provide a save button.
            # validation fails.
            error_msg = "The Maya session has not been saved."
            self.logger.error(
                error_msg,
                extra=_get_save_as_action()
            )
            raise Exception(error_msg)

        # get the normalized path
        path = sgtk.util.ShotgunPath.normalize(path)

        project =  self.parent.engine.shotgun.find_one('Project', [['id', 'is', self.parent.context.project['id']]], ['code'])
        step    =  self.parent.context.step['name']

        entity_name = self.parent.context.entity["name"].replace('-', '_')
        cam_group = "|{0}_{1}_cam_grp".format(project["code"], entity_name)        
        cam_grp_exists = [node for node in cmds.ls(assemblies=True, long=True) if node == cam_group]

        #Layout validation
        CAMERAS_LAYOUT = LayoutCamera()


        if step == 'matchmove':
            if not cam_grp_exists:
                error_msg = (
                    "Validation failed because there is a missing cameras in the scene "
                    "Please add a correct camera group and camera with name {}"
                    "Ask your lead for more information."
                )
                self.logger.error(error_msg.format(cam_group))
                raise Exception(error_msg.format(cam_group))  


            if not cmds.ls(assemblies=True, long=True):
                error_msg = (
                    "Validation failed because there is no cameras in the scene "
                    "to be exported. You can uncheck this plugin or create "
                    "cameras to export to avoid this error."
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)

        elif step == 'layout':
            if not CAMERAS_LAYOUT:
                error_msg = (
                    "Validation failed because there is a missing cameras in the scene "
                    "Please add a correct camera group and camera with name {}"
                    "Ask your lead for more information."
                )
                self.logger.error(error_msg.format(cam_group))
                raise Exception(error_msg.format(cam_group))  


            if not cmds.ls(assemblies=True, long=True):
                error_msg = (
                    "Validation failed because there is no cameras in the scene "
                    "to be exported. You can uncheck this plugin or create "
                    "cameras to export to avoid this error."
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)


        # get the configured work file template
        work_template = item.parent.properties.get("work_template")
        publish_template = item.properties.get("publish_template")

        # get the current scene path and extract fields from it using the work
        # template:
        work_fields = work_template.get_fields(path)

        # ensure the fields work for the publish template
        missing_keys = publish_template.missing_keys(work_fields)
        if missing_keys:
            error_msg = "Work file '%s' missing keys required for the " \
                        "publish template: %s" % (path, missing_keys)
            self.logger.error(error_msg)
            raise Exception(error_msg)

        # create the publish path by applying the fields. store it in the item's
        # properties. This is the path we'll create and then publish in the base
        # publish plugin. Also set the publish_path to be explicit.
        item.properties["path"] = publish_template.apply_fields(work_fields)
        item.properties["publish_path"] = item.properties["path"]

        # use the work file's version number when publishing
        if "version" in work_fields:
            item.properties["publish_version"] = work_fields["version"]

        for grp in cmds.ls(assemblies=True, long=True):
            if cmds.ls(grp, dag=True, type="camera"):
                if grp == cam_group:
                    #return super(CameraPublishPlugin, self).validate(settings, item)
                    return True
                elif 'layout' in grp:
                    return True



        
        self.logger.info("Could not find the camera {0}!".format(cam_group))
        raise Exception("Could not find the camera {0}!".format(cam_group))

        return #super(CameraPublishPlugin, self).validate(settings, item)

    def strip_version_from_name(self, path_string):

        name, ext = os.path.splitext(path_string)
        name = '_'.join(name.split('_')[:-1])

        return name


    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        # get the current scene path and extract fields from it
        # using the work template:
        scene_path = os.path.abspath(cmds.file(query=True, sn=True))
        work_template = item.parent.properties.get("work_template")
        fields = work_template.get_fields(scene_path)
        step    =  self.parent.context.step['name']
        
        publisher = self.parent

        # create the publish path by applying the fields
        # with the publish template:
        publish_template = item.properties.get("publish_template")
        publish_path = publish_template.apply_fields(fields)
        
        # ensure the publish folder exists:
        publish_folder = os.path.dirname(publish_path)
        self.parent.ensure_folder_exists(publish_folder)

        # determine the publish name:
        publish_code = os.path.basename(publish_path)
        publish_name = self.strip_version_from_name(publish_code)
        
        # Find additional info from the scene:
        #
        cmds.select(deselect=True, all=True)
        project =  self.parent.engine.shotgun.find_one('Project', [['id', 'is', self.parent.context.project['id']]], ['code'])
        entity_name = self.parent.context.entity["name"].replace('-', '_')

        #Matchmove
        if step == 'matchmove':
            cam_group = "|{0}_{1}_cam_grp".format(project["code"], entity_name)
            cmds.select(cam_group, hierarchy=True)

        #Layout
        if step == 'layout':
            LAYOUT_CAMERAS = LayoutCamera()
            cam_group = cmds.group(em=True, n='{0}_{1}_layout_cam_grp'.format(project["code"], entity_name))

            for c in LAYOUT_CAMERAS:
                cmds.parent(c, cam_group)
    
            cmds.select(cam_group, hierarchy=True)



        
        try:
            self.parent.log_debug("Export cameras")
            
            cmds.file(publish_path, type='mayaAscii', exportSelected=True, force=True)


        except Exception, e:
            raise Exception("Failed to export the Camera: %s" % e)

        print 'camera saved in: ', publish_path
        publish_version = fields['version']
        publish_type = 'Maya Camera'
        publish_dependencies = []

        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": publish_dependencies
        }        
           
        cam_publish = sgtk.util.register_publish(**publish_data)
        print 'Publish Camera .ma registred: ', cam_publish
        #super(CameraPublishPlugin, self).publish(settings, item)

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once all the publish
        tasks have completed.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """
        pass


def _session_path():
    """
    Return the path to the current session
    :return:
    """
    path = cmds.file(query=True, sn=True)

    if isinstance(path, unicode):
        path = path.encode("utf-8")

    return path


def _get_save_as_action():
    """
    Simple helper for returning a log action dict for saving the session
    """

    engine = sgtk.platform.current_engine()

    # default save callback
    callback = cmds.SaveScene

    # if workfiles2 is configured, use that for file save
    if "tk-multi-workfiles2" in engine.apps:
        app = engine.apps["tk-multi-workfiles2"]
        if hasattr(app, "show_file_save_dlg"):
            callback = app.show_file_save_dlg

    return {
        "action_button": {
            "label": "Save As...",
            "tooltip": "Save the current session",
            "callback": callback
        }
    }

def LayoutCamera():
    import maya.cmds as cmds
    import sgtk

    LayoutCams=[]
    engine= sgtk.platform.current_engine()
    Longproject = engine.context.project['name']
    projectSpace = Longproject.split(':')[0]
    project= projectSpace.strip()
    shot = engine.context.entity['name']

    DagObjects= cmds.ls(dag=1)

    for d in DagObjects:
        if d == '{0}_{1}_layout_cams'.format(project,shot):
             LayoutCams.append(d)

        



    return LayoutCams


