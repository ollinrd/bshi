# Copyright (c) 2017 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import pprint
import traceback
import maya.cmds as cmds
import pymel.core as pm

import sgtk
from sgtk.util.filesystem import copy_file, ensure_folder_exists

HookBaseClass = sgtk.get_hook_baseclass()


class BasicOBJFilePublishPlugin(HookBaseClass):
    """
    Plugin for creating generic publishes in Shotgun.

    This plugin is typically configured to act upon files that are dragged and
    dropped into the publisher UI. It can also be used as a base class for
    other file-based publish plugins as it contains standard operations for
    validating and registering publishes with Shotgun.

    Once attached to a publish item, the plugin will key off of properties that
    drive how the item is published.

    The ``path`` property, set on the item, is the only required property as it
    informs the plugin where the file to publish lives on disk.

    The following properties can be set on the item via the collector or by
    subclasses prior to calling methods on the base class::

        ``sequence_paths`` - If set in the item properties dictionary, implies
            the "path" property represents a sequence of files (typically using
            a frame identifier such as %04d). This property should be a list of
            files on disk matching the "path". If the ``work_template`` property
            is set, and corresponds to the listed frames, fields will be
            extracted and applied to the publish_template (if set) and copied to
            that publish location.

        ``work_template`` - If set in the item properties dictionary, this
            value is used to validate ``path`` and extract fields for further
            processing and contextual discovery. For example, if configured and
            a version key can be extracted, it will be used as the publish
            version to be registered in Shotgun.

    The following properties can also be set by a subclass of this plugin via
    :meth:`Item.properties` or :meth:`Item.local_properties`.

        publish_template - If set, used to determine where "path" should be
            copied prior to publishing. If not specified, "path" will be
            published in place.

        publish_type - If set, will be supplied to SG as the publish type when
            registering "path" as a new publish. If not set, will be determined
            via the plugin's "File Type" setting.

        publish_path - If set, will be supplied to SG as the publish path when
            registering the new publish. If not set, will be determined by the
            "published_file" property if available, falling back to publishing
            "path" in place.

        publish_name - If set, will be supplied to SG as the publish name when
            registering the new publish. If not available, will be determined
            by the "work_template" property if available, falling back to the
            ``path_info`` hook logic.

        publish_version - If set, will be supplied to SG as the publish version
            when registering the new publish. If not available, will be
            determined by the "work_template" property if available, falling
            back to the ``path_info`` hook logic.

        publish_dependencies - A list of files to include as dependencies when
            registering the publish. If the item's parent has been published,
            it's path will be appended to this list.

    NOTE: accessing these ``publish_*`` values on the item does not necessarily
    return the value used during publish execution. Use the corresponding
    ``get_publish_*`` methods which include fallback logic when no property is
    set. For example, if a ``work_template`` is used, the publish version and
    name might be extracted from the template fields in the fallback logic.

    This plugin will also set an ``sg_publish_data`` property on the item during
    the ``publish`` method which may be useful for child items.

        ``sg_publish_data`` - The dictionary of publish information returned
            from the tk-core register_publish method.

    NOTE: If you have multiple plugins acting on the same item, and you need to
    access or operate on the publish data, you can extract the
    ``sg_publish_data`` from the item after calling the base class ``publish``
    method in your plugin subclass.
    """

    ############################################################################
    # standard publish plugin properties

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(
            self.disk_location,
            "icons",
            "obj_cache.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish to Shotgun"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """
        Publishes the file to Shotgun. A <b>Publish</b> entry will be
        created in Shotgun which will include a reference to the file's current
        path on disk. Other users will be able to access the published file via
        the <b><a href='%s'>Loader</a></b> so long as they have access to
        the file's location on disk.

        <h3>File versioning</h3>
        The <code>version</code> field of the resulting <b>Publish</b> in
        Shotgun will also reflect the version number identified in the filename.
        The basic worklfow recognizes the following version formats by default:

        <ul>
        <li><code>filename.v###.ext</code></li>
        <li><code>filename_v###.ext</code></li>
        <li><code>filename-v###.ext</code></li>
        </ul>

        <br><br><i>NOTE: any amount of version number padding is supported.</i>

        <h3>Overwriting an existing publish</h3>
        A file can be published multiple times however only the most recent
        publish will be available to other users. Warnings will be provided
        during validation if there are previous publishes.
        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        return {
            "File Types": {
                "type": "list",
                "default": [
                    ["Alembic Cache", "abc"],
                    ["3dsmax Scene", "max"],
                    ["NukeStudio Project", "hrox"],
                    ["Houdini Scene", "hip", "hipnc"],
                    ["Maya Scene", "ma", "mb"],
                    ["Motion Builder FBX", "fbx"],
                    ["Nuke Script", "nk"],
                    ["Photoshop Image", "psd", "psb"],
                    ["Rendered Image", "dpx", "exr"],
                    ["Texture", "tiff", "tx", "tga", "dds"],
                    ["Image", "jpeg", "jpg", "png"],
                    ["Movie", "mov", "mp4"],
                ],
                "description": (
                    "List of file types to include. Each entry in the list "
                    "is a list in which the first entry is the Shotgun "
                    "published file type and subsequent entries are file "
                    "extensions that should be associated."
                )
            },
        }

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["maya.session.geometry"]

    ############################################################################
    # standard publish plugin methods
    def accept(self, settings, item):

        print('item obj acept: ', item)
        obj_posible_steps = ['rnd', 'model', 'character fx']
        if not item.context.step['name'] in obj_posible_steps:
            return {"accepted": False}

        #
        allSets = cmds.listSets(allSets=True)
        str_ = 'set_sgpublish'
        result = [str(mset) for mset in allSets if str_ == str(mset).lower()]
        if not result:
            context = self.parent.engine.context
            current_entity = context.entity
            asset_name = current_entity['name']
            for trans in pm.ls(type='transform'):
                l_name = trans.longName()
                if '|{0}|Geometry|Geo'.format(asset_name) == l_name:
                    result = True

        if result:
            # return the accepted info
            return {"accepted": True}

        return {"accepted": False}

    def validate(self, settings, item):
        publish_path = item.properties.get("obj_publish_path")
        publisher = self.parent
        engine = sgtk.platform.current_engine()
        #overwrite = settings.get("user_data")["overwrite"]
        overwrite = engine.overwrite_publish
        print('File overwrite set: ', overwrite)
        # ---- determine the information required to validate

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.
        publish_path = item.properties.work_path
        publish_name = item.properties['publish_name']

        current_version = item.properties["fields"]["version"]
        print('current_version: ', current_version)

        publishes = self.parent.engine.shotgun.find_one('PublishedFile', 
            [["published_file_type.PublishedFileType.code", "is", "Obj Cache"], ['name', 'is', publish_name], ['version_number', 'is', current_version]], 
            [])

        if publishes:

            self.logger.debug(
                "Conflicting publishes: %s" % (pprint.pformat(publishes),))

            publish_template = self.get_publish_template(settings, item)

            #if "work_template" in item.properties or publish_template:
            if not overwrite:
                # templates are in play and there is already a publish in SG
                # for this file path. We will raise here to prevent this from
                # happening.
                error_msg = (
                    "Can X not validate file path. There is already a publish in "
                    "Shotgun that matches this path. Please uncheck this "
                    "plugin or save the file to a different path."
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)

            else:
                conflict_info = (
                    "If you continue, these conflicting publishes will no "
                    "longer be available to other users via the loader:<br>"
                    "<pre>%s</pre>" % (pprint.pformat(publishes),)
                )
                self.logger.warn(
                    "Found %s conflicting publishes in Shotgun" %
                        (len(publishes),),
                    extra={
                        "action_show_more_info": {
                            "label": "Show Conflicts",
                            "tooltip": "Show conflicting publishes in Shotgun",
                            "text": conflict_info
                        }
                    }
                )
      
        #if self.parent.context.entity['type'] == 'Asset':
        #    self.validate_internal_review(publish_path, current_version)


        return True


    def validate_internal_review(self, path, version_number):
        #
        project = self.parent.context.project
        entity = self.parent.context.entity

        # Get the current output name        
        publish_name = os.path.splitext(os.path.basename(path))[0]

        # get the internal review version for current context
        version = self.parent.engine.shotgun.find_one('Version', 
            [['project', 'is', project], ['entity', 'is', entity], ['code', 'is', publish_name], ['sg_status_list', 'is', 'apr']], 
            [])
        
        if not version:            
            error_msg = "There is no internal review version aproved in shotgun for actual version number: {0}.\n".format(version_number)
            error_msg += "A internal review version must be aproved first in order to publish."
            raise Exception(error_msg)            

        return False



    def get_publish_template(self, settings, item):
        """
        Get a publish template for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A template representing the publish path of the item or
            None if no template could be identified.
        """

        return item.get_property("publish_template")


    def validate_publish_dependencies(self, entity_id, dependencies):

        #tk = sgtk.sgtk_from_entity('Task', 31876)
        #root_path = tk.pipeline_configuration.get_local_storage_roots()['primary'] + '/'

        validation = []
        for path in dependencies:
            print 'dep: ', path
            tk = sgtk.sgtk_from_path(path)
            root_path = tk.pipeline_configuration.get_local_storage_roots()['primary'] + '/'            
            pseudo_cache_path = path.replace(root_path, '')
            filters = [['path_cache', 'in', pseudo_cache_path]]
            result = self.parent.shotgun.find_one('PublishedFile', filters)
            if result:
                validation.append(result)

        return validation


    def select_nodes_to_export(self, cmds):       
        node_list = []
        result = [set for set in cmds.listSets(allSets=True) if 'set_sgpublish' == str(set).lower()]        
        if result:
            set_list = cmds.sets( result[0], q=True )
            cmds.select(set_list)
            node_list = [str(node) for node in cmds.ls(selection=True, l=True)]    

        return node_list


    def export_to_obj(self, obj_path):
        import maya.cmds as cmds

        nodes_to_export = self.select_nodes_to_export(cmds)

        start_frame = 1
        end_frame = 2
        list_of_nodes = ''
        for node in nodes_to_export:
            list_of_nodes += '-root {0} '.format(node)

        try:            
            cmds.file("{0}".format(obj_path), force=True, options="groups=1;ptgroups=1;materials=1;smoothing=1;normals=1", type="OBJexport", preserveReferences=True, exportSelected=True)
        except:
            raise Exception(
                "Failed execute OBJ export command"
            )    
        
        
        return True        

    def get_publish_dependencies(self, settings, item):
        """
        Get publish dependencies for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A list of file paths representing the dependencies to store in
            SG for this publish
        """

        #
        ma_publish_template = self.parent.engine.get_template_by_name("maya_asset_publish")
        ma_publish_path = ma_publish_template.apply_fields(item.properties.fields)

        print 'ma_publish_path: ', ma_publish_path

        # local properties first
        dependencies = [str(ma_publish_path)]

        return dependencies


    def publish(self, settings, item):
        publish_path = item.properties['obj_publish_path']
        self.parent.engine.ensure_folder_exists(os.path.dirname(publish_path))        
        self.export_to_obj(publish_path)

        publisher = self.parent
        publish_type = item.properties['obj_publish_type']        
        publish_name = item.properties["publish_name"]
        
        publish_version = item.properties["fields"]['version']
        publish_path = item.properties['obj_publish_path']
        publish_dependencies = self.get_publish_dependencies(settings, item)
        
        #publish_name, extension = os.path.splitext(publish_name)        
        publish_name = item.properties['publish_name']
        # if the parent item has a publish path, include it in the list of
        # dependencies
        if "sg_publish_path" in item.parent.properties:
            publish_dependencies.append(item.parent.properties.sg_publish_path)

        #pub_dependencies = self.validate_publish_dependencies(item.context.task['id'], publish_dependencies)
        pub_dependencies = publish_dependencies

        # arguments for publish registration
        self.logger.info("Registering publish...")               
        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": pub_dependencies
        }
        # log the publish data for debugging
        self.logger.debug(
            "Populated Publish data...",
            extra={
                "action_show_more_info": {
                    "label": "Publish Data",
                    "tooltip": "Show the complete Publish data dictionary",
                    "text": "<pre>%s</pre>" % (pprint.pformat(publish_data),)
                }
            }
        )

        # Search for the actual publish name in sg 
        current_version = item.properties["fields"]["version"]
        print 'current_version: ', current_version
        actual_publish_name = item.properties['publish_name']
        publish = self.parent.engine.shotgun.find_one('PublishedFile', 
            [["published_file_type.PublishedFileType.code", "is", "Obj Cache"], ['name', 'is', actual_publish_name], ['version_number', 'is', current_version]], 
            ["code", "path", "path_cache", "path_cache_storage", "name"])
        # create the publish and stash it in the item properties for other
        # plugins to use.        
        print 'publish: ', publish
        if not publish:
            # Register the actual publish
            print '-'*50
            print 'publish data OBJ: ', publish_data
            print '-'*50
            publish = sgtk.util.register_publish(**publish_data)       
            print 'Published registered for OBJ Files: ', publish

        """
        else:
            print 'Publish already registered, updating data...'
            #
            filters = [["code", "is", publish_type]]
            sg_published_file_type = self.parent.shotgun.find_one('PublishedFileType', filters=filters)
            print 'sg_published_file_type: ', publish_type, ' : ', sg_published_file_type

            thumbnail_path = item.get_thumbnail_as_path()
            print 'thumbnail_path: ', thumbnail_path            
 
            print 'pub_dependencies update?: ', pub_dependencies
            #Update the published found with current data
            data = {                
                'dependency_paths' : pub_dependencies
            }

            if item.description:
                data["description"] = item.description

            print 'Updating published file...'
            publish_update = self.parent.shotgun.update(publish['type'], publish['id'], data)
            print 'Data result: ', publish_update
            print 'Data updated: ', data

            if thumbnail_path:            
                print 'Uploading new thumbnail...'
                self.parent.shotgun.upload_thumbnail("PublishedFile", publish["id"], thumbnail_path)
                print 'Publish updated!'
        """

        item.properties["sg_publish_data"] = publish
        # item.properties.sg_publish_data = sgtk.util.register_publish(**publish_data)

        self.logger.info("Publish registered!")

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent

        # get the data for the publish that was just created in SG
        publish_data = item.properties.sg_publish_data

        # ensure conflicting publishes have their status cleared
        publisher.util.clear_status_for_conflicting_publishes(
            item.context, publish_data)

        self.logger.info(
            "Cleared the status of all previous, conflicting publishes")

        path = item.properties["obj_publish_path"]
        self.logger.info(
            "Publish created for file: %s" % (path,),
            extra={
                "action_show_in_shotgun": {
                    "label": "Show Publish",
                    "tooltip": "Open the Publish in Shotgun.",
                    "entity": publish_data
                }
            }
        )
