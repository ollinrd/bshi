# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import sgtk
import maya.cmds as cmds
import maya.mel as mel
import tempfile
import subprocess
from sgtk.util.filesystem import copy_file

HookBaseClass = sgtk.get_hook_baseclass()


class PlayblastPublishPlugin(HookBaseClass):
    """
    Plugin for publish geometry

    """

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "icon_256.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish maya playblast"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """

        Test the new plug %s
        

        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        # inherit the settings from the base publish plugin
        base_settings = super(PlayblastPublishPlugin, self).settings or {}

        # settings specific to this class
        maya_publish_settings = {
            "Publish Template": {
                "type": "template",
                "default": None,
                "description": "Template path for published playblast. Should"
                               "correspond to a template defined in "
                               "templates.yml.",
            }
        }

        # update the base settings
        base_settings.update(maya_publish_settings)

        return base_settings

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["maya.session.playblas"]

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A submit for review task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled. 
            This plugin makes use of the tk-multi-reviewsubmission app; if this
            app is not available then the item will not be accepted, this method
            will return a dictionary with the accepted field that has a value of
            False.

            Several properties on the item must also be present for it to be
            accepted. The properties are 'path', 'publish_name', 'color_space',
            'first_frame' and 'last_frame'
        """
        print '\n--ACEPT PLAYBLAST...'

        path = _session_path()        
        project =  self.parent.engine.shotgun.find_one('Project', [['id', 'is', self.parent.context.project['id']]], ['code'])
        cam_group = "|{0}_{1}_cam_grp".format(project["code"], self.parent.context.entity["name"])        
        cam_grp_exists = [node for node in cmds.ls(assemblies=True, long=True) if node == cam_group]
        
        print cam_grp_exists, 'yes'
        if cam_grp_exists:
            print 'Entro!'
            accepted = True
            publisher = self.parent
            print publisher
            template_name = settings["Publish Template"].value
            print 'template_name: ', template_name, item.parent.properties.get("work_template")
            # ensure a work file template is available on the parent item
            work_template = item.parent.properties.get("work_template")
            print 'work_template: ', work_template
            if not work_template:
                self.logger.debug(
                    "A work template is required for the session item in order to "
                    "publish session cameras. Not accepting session cam item."
                )
                accepted = False

            # ensure the publish template is defined and valid and that we also have
            publish_template = publisher.get_template_by_name(template_name)
            if not publish_template:
                self.logger.debug(
                    "The valid publish template could not be determined for the "
                    "session camera item. Not accepting the item."
                )
                accepted = False

            print 'publish_template: ', publish_template
            # we've validated the publish template. add it to the item properties
            # for use in subsequent methods
            item.properties["publish_template"] = publish_template

            # log the accepted file and display a button to reveal it in the fs
            self.logger.info(
                "Camera  plugin accepted for the path: %s" % (path),
                    
                )
            return {
                "accepted": accepted,
                "checked": True
            }
        else:
            self.logger.info(
                "Camera  plugin not accepted for the path: %s" % (path),
                    
                )

            return {
                "accepted": False,
                "visible": False,
                "checked": True
            }

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish. Returns a
        boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        :returns: True if item is valid and not in proxy mode, False otherwise.
        """

        # the render task will always render full-res frames when publishing. If we're
        # in proxy mode in Nuke, that task will fail since there will be no full-res
        # frames rendered. The exceptions are if there is no proxy_render_template set
        # in the tk-nuke-writenode app, then the write node app falls back on the
        # full-res template. Or if they rendered in full res and then switched to
        # proxy mode later. In this case, this is likely user error, so we catch it.

        print '\n--VALIDATE playblast...'

        path = _session_path()
        # ---- ensure the session has been saved
        if not path:
            # the session still requires saving. provide a save button.
            # validation fails.
            error_msg = "The Maya session has not been saved."
            self.logger.error(
                error_msg,
                extra=_get_save_as_action()
            )
            raise Exception(error_msg)            

        if not cmds.ls(assemblies=True, long=True):
            error_msg = (
                "Validation failed because there is no cameras in the scene "
                "to be exported. You can uncheck this plugin or create "
                "cameras to export to avoid this error."
            )
            self.logger.error(error_msg)
            raise Exception(error_msg)

        project =  self.parent.engine.shotgun.find_one('Project', [['id', 'is', self.parent.context.project['id']]], ['code'])
        cam_group = "|{0}_{1}_cam_grp".format(project["code"], self.parent.context.entity["name"])        
        cam_grp_exists = [node for node in cmds.ls(assemblies=True, long=True) if node == cam_group]
        
        if not cam_grp_exists:
            error_msg = (
                "Validation failed because there is a missing cameras in the scene "
                "Please add a correct camera group and camera with name {}"
                "Ask your lead for more information."
            )
            self.logger.error(error_msg.format(cam_group))
            raise Exception(error_msg.format(cam_group))            


        # get the normalized path
        path = sgtk.util.ShotgunPath.normalize(path)

        # get the configured work file template
        work_template = item.parent.properties.get("work_template")
        publish_template = item.properties.get("publish_template")

        # get the current scene path and extract fields from it using the work
        # template:
        work_fields = work_template.get_fields(path)

        # ensure the fields work for the publish template
        missing_keys = publish_template.missing_keys(work_fields)
        if missing_keys:
            error_msg = "Work file '%s' missing keys required for the " \
                        "publish template: %s" % (path, missing_keys)
            self.logger.error(error_msg)
            raise Exception(error_msg)

        # create the publish path by applying the fields. store it in the item's
        # properties. This is the path we'll create and then publish in the base
        # publish plugin. Also set the publish_path to be explicit.
        item.properties["path"] = publish_template.apply_fields(work_fields)
        item.properties["publish_playblast_path"] = item.properties["path"]

        # use the work file's version number when publishing
        if "version" in work_fields:
            item.properties["publish_version"] = work_fields["version"]

        return True

    def _construct_args(self, playblast_temp_path):

        # get render setting with & height
        width = cmds.getAttr('defaultResolution.width')
        height = cmds.getAttr('defaultResolution.height')

        # Set parameters        
        args = {
            'format': "image", 
            'filename': playblast_temp_path,
            'forceOverwrite': True,
            'sequenceTime': 0,
            'clearCache': 1,
            'viewer': 0,
            'showOrnaments': 0,
            'fp': 4,
            'percent': 60,
            'compression': 'jpg',        
            'widthHeight': [width, height],
            'quality': 80,
            'os':True
        }

        # TODO sacar la resolucion de las dimensiones de lo scans, (con mediainfo, ffprobe o PIL)       
        sound_list = cmds.ls(type='audio')
        if sound_list:
            try:
                mel.eval(('setSoundDisplay {0} 1;').format(sound_list[0]))
                args['sound'] = sound_list[0]
            except:
                pass

        return args

    def strip_version_from_name(self, path_string):

        name, ext = os.path.splitext(path_string)
        name = '_'.join(name.split('_')[:-1])

        return name

    def _set_maya_hardware_settings(self):

        mel.eval("setCurrentRenderer mayaHardware2")
        cmds.setAttr("hardwareRenderingGlobals.renderMode", 2)

        # cmds.setAttr("hardwareRenderingGlobals.motionBlurEnable", True)
        # cmds.setAttr("hardwareRenderingGlobals.motionBlurShutterOpenFraction", .5)
        # cmds.setAttr("hardwareRenderingGlobals.motionBlurSampleCount", 4)

        cmds.setAttr("hardwareRenderingGlobals.lineAAEnable", True)
        cmds.setAttr("hardwareRenderingGlobals.multiSampleEnable", True)
        cmds.setAttr("hardwareRenderingGlobals.multiSampleCount", 2)

    def _submit_version(self, path_to_frames, path_to_movie, sg_publishes,
                        sg_task, comment, first_frame, last_frame):
        """
        Create a version in Shotgun for this path and linked to this publish.
        """
        tk_maya = sgtk.platform.current_engine()

        # get current shotgun user
        current_user = sgtk.util.get_current_user(self.parent.sgtk)

        # create a name for the version based on the file name
        # grab the file name, strip off extension
        name = os.path.splitext(os.path.basename(path_to_movie))[0]
        name = '{}playblast_{}'.format(name[:-4], name[-4:])

        # do some replacements
        
        #cmd = "ffmpeg -i {0} -qscale:v 4 -start_number 1001 {1}".format(path_to_movie, path_to_frames)
        #print 'FFMPEG SUBMIT: ', cmd
        #tk_maya = sgtk.platform.current_engine()
        #tk_maya.ensure_folder_exists(os.path.dirname(path_to_frames))

        #subprocess.call(cmd, shell=True)
        # Create the version in Shotgun
        print 'ENTITY: ', self.parent.context.entity
        data = {
            "code": name,
            "sg_status_list": 'rev',
            "entity": self.parent.context.entity,
            "sg_task": self.parent.context.task,
            "sg_first_frame": first_frame,
            "sg_last_frame": last_frame,
            "frame_count": (last_frame-first_frame+1),
            "frame_range": "%s-%s" % (first_frame, last_frame),
            "sg_frames_have_slate": False,
            "published_files": sg_publishes,
            "created_by": current_user,
            "user": current_user,
            "description": comment,
            "sg_path_to_frames": path_to_frames + '.%04d.jpg',
            "sg_path_to_movie": path_to_movie,
            "sg_movie_has_slate": False,
            "project": self.parent.context.project
        }

        sg_version = self.parent.sgtk.shotgun.find_one('Version',
                                                       [['code', 'is',
                                                         name]])

        if not sg_version:
            sg_version = self.parent.sgtk.shotgun.create("Version", data)            
            print 'Version created: ', sg_version
            self.parent.log_debug("Created version in shotgun: %s" % str(data))

        return sg_version

    def _upload_version_movie(self, version_id, path_to_movie, thumbnail_path):
        
        upload_error = False

        try:
            self.parent.sgtk.shotgun.upload("Version", version_id, path_to_movie, "sg_uploaded_movie")            
        except Exception, e:
            self.parent.log_debug("Movie upload to Shotgun failed: %s" % e)
            upload_error = True

        if upload_error:
            if thumbnail_path and os.path.isfile(thumbnail_path):
                try:
                    self.parent.sgtk.shotgun.upload_thumbnail("Version", version_id, thumbnail_path)
                except Exception, e:
                    raise Exception("Thumbnail upload to Shotgun failed: %s" % e)

        return

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        print '\n--PULBISH playblast...'   

        tk_maya = sgtk.platform.current_engine()     

        # get the current scene path and extract fields from it
        # using the work template:
        scene_path = os.path.abspath(cmds.file(query=True, sn=True))

        work_template = item.parent.properties.get("work_template")

        fields = work_template.get_fields(scene_path)

        print fields, scene_path
                
        # create the publish path by applying the fields
        publish_path = item.properties["publish_playblast_path"]
        print 'publish_path: ', publish_path
        
        # ensure the publish folder exists:
        publish_folder = os.path.dirname(publish_path)
        self.parent.ensure_folder_exists(publish_folder)

        # determine the publish name:
        publish_code = os.path.basename(publish_path)
        publish_name = self.strip_version_from_name(publish_code)

        print 'publish_name: ', publish_name
        
        try:
            render_template = sgtk.platform.current_engine().get_template_by_name("nuke_shot_playblast_jpg")
            path_to_frames = render_template.apply_fields(fields)            
            tk_maya.ensure_folder_exists(os.path.dirname(path_to_frames))
            print 'path_to_frames: ', path_to_frames          
            args = self._construct_args(path_to_frames)
            '''
            # delete the temp mov file if it exists
            for root, dirs, files in os.walk(os.path.dirname(playblast_temp_path)):
                for name in files:
                    if name.startswith('maya_playblast') and name.endswith('mov'):
                        try:
                            os.remove(os.path.join(root, name))
                        except:
                            pass
            '''

            print 'Seting maya hardware_settings...'
            self._set_maya_hardware_settings()
            
            try:
                print 'Doing playblast with args: '
                print args
                
                cmds.playblast(**args)
            except Exception, e:
                raise Exception("Failed to create playblast: %s" % e)

            print 'Converting from JPG to MOV...'
            cmd = "ffmpeg -y -start_number {2} -i {1} {0}".format(publish_path, path_to_frames + '.%04d.jpg', cmds.getAttr('defaultRenderGlobals.startFrame'))
            #cmd = "ffmpeg -y -start_number {2} -i {0} -c:v -r 24 -pix_fmt yuv422p -y {1}".format(path_to_frames + '.%04d.jpg', publish_path, cmds.getAttr('defaultRenderGlobals.startFrame'))
            subprocess.call(cmd, shell=True)
            print 'VIDEOOOOO: ' + cmd
            
            # copy the temporal mov file to publish path
            #try:                
            #    copy_file(playblast_temp_path, publish_path)
            #except Exception, e:
            #    raise Exception("Failed to copy file from %s to %s - %s" % (playblast_temp_path, publish_path, e))

           
            # register file in shotgun
   
            publish_version = fields['version']
            publish_type = 'Maya Playblast'
            publish_dependencies = []
            publisher = self.parent
            thumbnail_path = item.get_thumbnail_as_path()

            print 'Register sg Mov publish...'
            # Register MOV playblast publish             
            publish_data = {
                "tk": publisher.sgtk,
                "context": item.context,
                "comment": item.description,
                "path": publish_path,
                "name": publish_name,
                "version_number": publish_version,
                "thumbnail_path": thumbnail_path,
                "published_file_type": publish_type,
                "dependency_paths": publish_dependencies
            }  
            playblast_publish = sgtk.util.register_publish(**publish_data)

            first_frame = int(cmds.playbackOptions(q=True, min=True))
            last_frame = int(cmds.playbackOptions(q=True, max=True))

            print 'submiting version.....'
            # create shotgun version
            sg_version = self._submit_version(path_to_frames,
                                              publish_path,
                                              [playblast_publish],
                                              self.parent.context.task,
                                              item.description,
                                              first_frame,
                                              last_frame)
            print 'uploading version....'                        
            # upload version movie
            self._upload_version_movie(sg_version['id'], publish_path, thumbnail_path)
            print '... Version uploaded'

        except Exception as e:
            import traceback
            print '\n--------------------------'
            print traceback.format_exc()
            raise Exception('Failed to create playblast version: %s' % e)



    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once all the publish
        tasks have completed.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """
        pass


def _session_path():
    """
    Return the path to the current session
    :return:
    """
    path = cmds.file(query=True, sn=True)

    if isinstance(path, unicode):
        path = path.encode("utf-8")

    return path


def _get_save_as_action():
    """
    Simple helper for returning a log action dict for saving the session
    """

    engine = sgtk.platform.current_engine()

    # default save callback
    callback = cmds.SaveScene

    # if workfiles2 is configured, use that for file save
    if "tk-multi-workfiles2" in engine.apps:
        app = engine.apps["tk-multi-workfiles2"]
        if hasattr(app, "show_file_save_dlg"):
            callback = app.show_file_save_dlg

    return {
        "action_button": {
            "label": "Save As...",
            "tooltip": "Save the current session",
            "callback": callback
        }
    }

