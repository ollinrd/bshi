# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.
 
import os
import subprocess
import sgtk
from sgtk.util.filesystem import ensure_folder_exists
import maya.cmds as cmds
import maya.mel as mel
import pymel.core as pm
import sys

HookBaseClass = sgtk.get_hook_baseclass()


class GeometryPublishAlembicPlugin(HookBaseClass):
    """
    Plugin for publish cameras

    """

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "icon_256.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish Alembic Geo"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """

        Test the new plug %s
        

        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        # inherit the settings from the base publish plugin
        base_settings = super(GeometryPublishAlembicPlugin, self).settings or {}

        # settings specific to this class
        maya_publish_settings = {
            "Publish Template": {
                "type": "template",
                "default": None,
                "description": "Template path for published cameras files. Should"
                               "correspond to a template defined in "
                               "templates.yml.",
            }
        }

        # update the base settings
        base_settings.update(maya_publish_settings)

        return base_settings

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["maya.session.geometry"]

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A submit for review task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled. 
            This plugin makes use of the tk-multi-reviewsubmission app; if this
            app is not available then the item will not be accepted, this method
            will return a dictionary with the accepted field that has a value of
            False.

            Several properties on the item must also be present for it to be
            accepted. The properties are 'path', 'publish_name', 'color_space',
            'first_frame' and 'last_frame'
        """
        print '--PUBLISH GEOMETRY alembic ACCEPT'
       
        path = _session_path()
        sg_set = [set for set in cmds.listSets(allSets=True) if len(cmds.listSets(allSets=True)) and set == "set_sgPublish"]
       
        print 'sg_set: ', sg_set        
        if sg_set:
            #Ziva validation
            zSolver_list= pm.ls(type="zSolverTransform")
            if zSolver_list:
                return {"accepted": False}    
            else: 
                accepted = True
                
            publisher = self.parent
            template_name = settings["Publish Template"].value            
            # ensure a work file template is available on the parent item
            work_template = item.parent.properties.get("work_template")

            print 'work_template: ', work_template
            if not work_template:
                self.logger.debug(
                    "A work template is required for the session item in order to "
                    "publish session cameras. Not accepting session cam item."
                )
                accepted = False

            print 'template_name: ', template_name
            # ensure the publish template is defined and valid and that we also have
            publish_template = publisher.get_template_by_name(template_name)
            if not publish_template:
                self.logger.debug(
                    "The valid publish template could not be determined for the "
                    "session camera item. Not accepting the item."
                )
                accepted = False

            print 'GEO publish_template: ', publish_template
            # we've validated the publish template. add it to the item properties
            # for use in subsequent methods
            item.properties["publish_template"] = publish_template

            # log the accepted file and display a button to reveal it in the fs
            self.logger.info(
                "Camera  plugin accepted for the path: %s" % (path),
                    
                )
            return {
                "accepted": accepted,
                "checked": True
            }
        else:
            self.logger.info(
                "Camera  plugin not accepted for the path: %s" % (path),
                    
                )

            return {
                "accepted": False,
                "visible": False,
                "checked": True
            }

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish. Returns a
        boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        :returns: True if item is valid and not in proxy mode, False otherwise.
        """

        # the render task will always render full-res frames when publishing. If we're
        # in proxy mode in Nuke, that task will fail since there will be no full-res
        # frames rendered. The exceptions are if there is no proxy_render_template set
        # in the tk-nuke-writenode app, then the write node app falls back on the
        # full-res template. Or if they rendered in full res and then switched to
        # proxy mode later. In this case, this is likely user error, so we catch it.

        print '\n--PUBLISH GEO ALEMBIC VALIDATE'
        print 'GEO alembic PUBLISH TEMPLATE', item.properties["publish_template"]
        # check that the AbcExport command is available!
        if not mel.eval("exists \"AbcExport\""):
            raise Exception("Could not find the AbcExport command needed to publish Alembic caches!")

        path = _session_path()

        # ---- ensure the session has been saved

        if not path:
            # the session still requires saving. provide a save button.
            # validation fails.
            error_msg = "The Maya session has not been saved."
            self.logger.error(
                error_msg,
                extra=_get_save_as_action()
            )
            raise Exception(error_msg)

        # get the normalized path
        path = sgtk.util.ShotgunPath.normalize(path)

        if not cmds.ls(assemblies=True, long=True):
            error_msg = (
                "Validation failed because there is no cameras in the scene "
                "to be exported. You can uncheck this plugin or create "
                "cameras to export to avoid this error."
            )
            self.logger.error(error_msg)
            raise Exception(error_msg)

        # get the configured work file template
        work_template = item.parent.properties.get("work_template")
        publish_template = item.properties["publish_template"]
        print 'x1 GEO alembic PUBLISH TEMPLATE', publish_template

        # get the current scene path and extract fields from it using the work
        # template:
        work_fields = work_template.get_fields(path)

        # ensure the fields work for the publish template
        missing_keys = publish_template.missing_keys(work_fields)
        if missing_keys:
            error_msg = "Work file '%s' missing keys required for the " \
                        "publish template: %s" % (path, missing_keys)
            self.logger.error(error_msg)
            raise Exception(error_msg)

        # create the publish path by applying the fields. store it in the item's
        # properties. This is the path we'll create and then publish in the base
        # publish plugin. Also set the publish_path to be explicit.
        item.properties["path"] = publish_template.apply_fields(work_fields)
        item.properties["publish_geo_abc_path"] = item.properties["path"]

        # use the work file's version number when publishing
        if "version" in work_fields:
            item.properties["publish_version"] = work_fields["version"]

        '''
        sg_set = [set for set in cmds.listSets(allSets=True) if len(cmds.listSets(allSets=True)) and set == "set_sgPublish"]
        if sg_set:
            return super(GeometryPublishAlembicPlugin, self).validate(settings, item)
        else:
            self.logger.info("Could not find the set: {0}!".format(sg_set))
            raise Exception("Could not find the set: {0}!".format(sg_set))

        return super(GeometryPublishAlembicPlugin, self).validate(settings, item)
        '''

        print 'item.properties["publish_geo_abc_path"]: ', item.properties["publish_geo_abc_path"]
        return True

    def strip_version_from_name(self, path_string):

        name, ext = os.path.splitext(path_string)
        name = '_'.join(name.split('_')[:-1])

        return name



    def get_sg_set(self, set_name="set_sgPublish"):
        sg_set = [set for set in cmds.listSets(allSets=True) if len(cmds.listSets(allSets=True)) and set == set_name]
        if sg_set:
            sg_set = sg_set[0]

        return sg_set




                                
    def _find_scene_animation_range(self):
        """
        Find the animation range from the current scene.
        """
        # look for any animation in the scene:
        animation_curves = cmds.ls(typ="animCurve")
        
        # if there aren't any animation curves then just return
        # a single frame:
        #if not animation_curves:
            #return (1, 1)
        
        # something in the scene is animated so return the
        # current timeline.  This could be extended if needed
        # to calculate the frame range of the animated curves.
        start = int(cmds.playbackOptions(q=True, min=True))
        end = int(cmds.playbackOptions(q=True, max=True))        
        
        return (start, end)

    def _search_key(self):
        all_n = cmds.ls()
        for i in all_n:           
           keyf = cmds.keyframe(i,  query=True, keyframeCount=True )           
           if keyf > 0:
               return True

        return False
        

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        # get the current scene path and extract fields from it
        # using the work template:
        print '\nPUBLISH ALEMBIC GEO'
        scene_path = os.path.abspath(cmds.file(query=True, sn=True))

        work_template = item.parent.properties.get("work_template")

        fields = work_template.get_fields(scene_path)

        print fields, scene_path
                
        # create the publish path by applying the fields
        # with the publish template:
        publish_path = item.properties.get("publish_geo_abc_path")
        print '1 publish_path: ', publish_path
        
        # ensure the publish folder exists:
        publish_folder = os.path.dirname(publish_path)
        self.parent.ensure_folder_exists(publish_folder)

        # determine the publish name:
        publish_code = os.path.basename(publish_path)
        publish_name = self.strip_version_from_name(publish_code)

        set_name = self.get_sg_set()
        print "#"*20, set_name

        #Preguntar si en el set hay mas de un namespace
        engine= sgtk.platform.current_engine()
        if cmds.objExists('set_sgCharacter'):
            nameSpaces=len(self.get_characters_nodes())
            if nameSpaces >= 2:
                abcCmds=self.abcCommands(item)
                for a in abcCmds:
                    self.sendJobs(a)



        if cmds.objExists('set_sgPublish'):
        ###----------------------------- Julio Cesar ---------------------------------------####
            ### Curves Visibility
            curves = cmds.ls(type="nurbsCurve")
            cVis = []
            for c in curves:
                transform = cmds.listRelatives(c, p=1, f=1)
                for t in transform:
                    n = "{0}.visibility".format(t)
                    vis = cmds.getAttr(n)

                    if vis == 0:
                        pass
                    else:
                        cVis.append(t)
                        #cmds.hide(t)
        else:
            cmds.confirmDialog( title= 'set_sgPublish is empty', message= 'Please create a Sg set')
            sys.stdout.write('Please create a Sg set')

        ###----------------------------------------------------------------###
        if set_name:
            alembic_args = []
            # Quite esta bandera porque estaba dando bronca para CFX -autoSubd 
            alembic_args_2 = ['-ro', '-uvWrite', '-writeColorSets',
                              '-writeFaceSets', '-worldSpace',
                              '-writeVisibility' , '-writeUVSets']

            print(">>>>>> Exportando alembic config maya_geo_nodes_alembic")
       
            step= engine.context.step["name"].lower()

            if self._search_key() or step == 'character fx'  or  step== 'animation':
                start_frame, end_frame = self._find_scene_animation_range()
                if start_frame and end_frame:
                    alembic_args.append('-fr %d %d' % (start_frame , end_frame))

                c_step = self.parent.engine.context.step['name'].lower()
                steps = 0

                if c_step == 'matchmove':
                    #Add Euler Filter
                    alembic_args_2.append('-ef')

                if c_step in ['animation', 'RnD', "character fx", 'layout']:
                    sg = self.parent.engine.shotgun
                    entity = self.parent.context.entity
                    field_name = 'sg_substeps_alembic'
                    fields_e = [field_name]
                    filter_e = [['id', 'is', entity['id']]]

                    entity_ = sg.find_one(entity['type'], filter_e, fields_e)
                    if entity_[field_name] is None:
                        steps = 0
                    elif entity_[field_name] in [0, 1]:
                        steps = 0
                    else:
                        steps = 1.0/float(entity_[field_name])

                if steps != 0:
                    alembic_args.append(' -step {0}'.format(steps))

                alembic_args = alembic_args + alembic_args_2

            publish_folder = os.path.dirname(publish_path)
            self.parent.ensure_folder_exists(publish_folder)

            #for set_group in cmds.listConnections('{0}.dagSetMembers'.format(set_name)):
            #    alembic_args.append(('-root  {0}').format(set_group))

            alembic_args.append("-dataFormat ogawa")
            node_list = []
            result = [set for set in cmds.listSets(allSets=True) if 'set_sgpublish' == str(set).lower()]
            if result:
                set_list = cmds.sets(result[0], q=True)
                cmds.select(set_list)
                ls_cur = cmds.ls(selection=True, l=True)
                node_list = [str(node) for node in ls_cur]
                for n_l in node_list:
                    alembic_args.append(('-root  {0}').format(n_l))
            alembic_args.append('-file %s' % publish_path.replace('\\', '/'))
        

            abc_export_cmd = 'AbcExport -j "%s"' % (' ').join(alembic_args)
            print('abc_export_cmd: ', abc_export_cmd)
            abc_python_cmd = ' '.join(alembic_args)

            try:
                self.parent.log_debug('Executing command: %s' % abc_export_cmd)
                #mel.eval(abc_export_cmd)
                cmds.AbcExport(j=abc_python_cmd)
            ###------------ Show curves again
                #for c in cVis:
                #    cmds.showHidden(c)
            ###---------------------------------------------------------------
            except Exception as e:
                raise Exception('Failed to export Alembic Cache: %s' % e)

        print 'Geo alembic file saved in: ', publish_path
        publish_version = fields['version']
        publish_type = 'Alembic Geo'
        publish_dependencies = []
        publisher = self.parent

        # register the publish:            
        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": publish_dependencies
        }   

        geo_publish = sgtk.util.register_publish(**publish_data)
        print 'Publish Maya Geo Alembic .abc registred: ', geo_publish   

        #super(GeometryPublishAlembicPlugin, self).publish(settings, item)

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once all the publish
        tasks have completed.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """
        pass


############ Animation Step: More than one Character ##############


    def get_characters_nodes(self):
        import pymel.core as pm

        node_list = []
        result = [set for set in cmds.listSets(allSets=True) if 'set_sgcharacter' == str(set).lower()]     

        if result:
            set_list = cmds.sets(result[0], q=True)
            cmds.select(set_list)
            node_list = [str(node) for node in pm.ls(selection=True)]  
           
            Ns_list= []
            
            for n in node_list:
                nameSpace= n.split(':')[0]
                Ns_list.append(nameSpace)

            #Borrar nameSpaces duplicados de la lista
            Ns_list = list(dict.fromkeys(Ns_list))
                
            NameSpaces= []
            sameSpace=[]
             
            for ns in Ns_list:
                for n in node_list:
                     n_ns = n.split(':')[0]
                     if ns == n_ns:
                         sameSpace.append(n)
                NameSpaces.append(sameSpace)
                sameSpace= []
            
            return NameSpaces      
    


    def abcCommands(self,item):
        engine=self.parent.engine
        project_code= engine.context.project["name"].split(":")[0].replace(" ", "")
        entity_type= engine.context.entity["type"].lower()

        #Get Scene path
        original_file_path = os.path.abspath(cmds.file(query=True, sn=True))
        file_path = os.path.abspath(cmds.file(query=True, sn=True))
        scene_folder = os.path.dirname(file_path)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])

        #Get Version
        filePath= os.path.basename(cmds.file(exn=1, q=1))
        version= (filePath.split('_')[-1]).split('.')[0]

        #Set the Abc storage folder
        scene_f_base= os.path.dirname( scene_folder)

        #abc_folder = os.path.join(scene_f_base, "abc_Files", version)


        original_file_path = os.path.abspath(cmds.file(query=True, sn=True))
        maya_work_tmpt=engine.get_template_by_name("maya_{0}_work".format(entity_type))
        print maya_work_tmpt

        maya_fields=maya_work_tmpt.get_fields(original_file_path)

        publish_template=engine.get_template_by_name("maya_{0}_alembic_geo".format(entity_type))
        abc_folder=os.path.dirname(publish_template.apply_fields(maya_fields))

        #Ensure folders exist
        
        ensure_folder_exists(abc_folder)

        script_name = os.path.splitext(os.path.basename(file_path))[0]
        Characters = self.get_characters_nodes()


        commands= []

        for nodes_to_export in Characters:
            #Create ass path

            ns= nodes_to_export[0].split(':')[0]
            characterName=""
            for word in ns.split("_"):
                if  word != project_code:
                    characterName+=word.capitalize()
                else:
                    characterName+=word

            version=scene_name.split("_")[-1]
            sn=[s for s in scene_name.split("_") if s != version ]
            sn='_'.join(sn)

            abc_path= os.path.join(abc_folder,sn + "{0}".format(characterName)+ "_geo" + "_{0}".format(version) + ".abc")
    
            start_frame = 1
            end_frame = 1
            list_of_nodes = ''

            for node in nodes_to_export:
                list_of_nodes += '-root {0} '.format(node)

            abc_steps_complete = ['animation', 'layout', 'character fx']
            c_step = self.parent.engine.context.step['name'].lower()
            if c_step in abc_steps_complete:
                start_frame = cmds.playbackOptions(query=True, min=True)
                end_frame = cmds.playbackOptions(query=True, max=True)

            abc_cmd = "-frameRange {0} {1}".format(start_frame, end_frame)
            if c_step in ['animation', 'layout', "character fx", "RnD"]:
                print "Heey"
                sg = self.parent.engine.shotgun
                entity = self.parent.context.entity
                steps = self.getAlembicSteps(sg, entity)
                if steps != 0:
                    abc_cmd += ' -step {0}'.format(steps)

            abc_cmd += " -ro -writeVisibility -uvWrite -worldSpace"
            abc_cmd += " -writeUVSets -writeColorSets"

            if c_step == 'matchmove':
                abc_cmd += " -ef"

            abc_cmd += " -dataFormat ogawa {0}".format(list_of_nodes)
            abc_cmd += "-file {0}".format(abc_path)

            CommandsDict= { 'namespace': ns, 'abcCommand' : abc_cmd}

            self._publish_Characters_abc(item,abc_path)

            commands.append(CommandsDict)



            
        print '###################################', commands


        #General Data
        engine = sgtk.platform.current_engine()
        e_type= engine.context.entity['type'].lower()

        #Batch Commands
        batch_cmds_ls= []

        for abc in commands:
            #deadline Command
            f_dir = os.path.dirname(file_path)
            flags = ' "-proj {0} -batch -file {1}'.format(f_dir, file_path)   
            flags += " -command  'AbcExport -j "
            flags += '<QUOTE> {0} <QUOTE>'.format(abc['abcCommand'])
            flags += ';'
            flags += "'"
            flags += '"'

            maya_path = '/usr/autodesk/maya2018/bin/maya'
            if '2019' in cmds.about(version=True):
                maya_path = '/usr/autodesk/maya2019/bin/maya'

            chunkN = int((end_frame-start_frame)/10)
            if chunkN < 2:
                chunkN = 2
            if chunkN > 10:
                chunkN = 10

            batch_cmd = " -SubmitCommandLineJob "
            batch_cmd += " -executable {0} ".format(maya_path)
            batch_cmd += " -arguments {0}".format(flags)
            batch_cmd += ' -pool cg '
            batch_cmd += ' -chunksize {0}'.format(chunkN)
            batch_cmd += ' -name "{0}"'.format('{0}, {1}, abc File'.format(e_type, abc['namespace']))
            batch_cmd += ' -prop ConcurrentTasks=1'
            batch_cmd += ' -prop BatchName={0}'.format(script_name)       
            batch_cmd += ' -prop OutputDirectory0={0}'.format(abc_folder) 
            batch_cmd += self._construct_environment_varibles()

            print 'abc batch_cmd: ', batch_cmd


            batch_cmds_ls.append(batch_cmd)
        
        return batch_cmds_ls


    def _publish_Characters_abc(self, item, abcPath):
        #[General Data]
        import maya.cmds as cmds
        engine=self.parent.engine
        sg = self.parent.engine.shotgun
        user=engine.context.user
        project=engine.context.project
        entity=engine.context.entity
        task=engine.context.task
        entity_type=engine.context.entity["type"].lower()

        root= "/nfs/ollinvfx/Project/"
        publish_path= abcPath
        path_cache= publish_path.replace(root,"")
        code=os.path.basename(publish_path)

        publish_name=code.split('.')[0]

        #Remover Version from name to avoid multiple versions in loader
        version= publish_name.split("_")[-1]
        publish_name=publish_name.replace("_"+version, "")

        publish_type_name= "Alembic Geo CFX"
        fields= ['code']
        filters= [['code', 'is', publish_type_name]]
        pfDict= sg.find_one('PublishedFileType',filters, fields)
        publish_type=pfDict

        #create publish dependencies
        mayaPath= cmds.file(q=1, sn=True)
        
        maya_work_tmpt=engine.get_template_by_name("maya_{0}_work".format(entity_type))
        maya_fields=maya_work_tmpt.get_fields(mayaPath)
 
        publish_template=engine.get_template_by_name("maya_{0}_publish".format(entity_type))
        mayaPath=publish_template.apply_fields(maya_fields)  

        versionNumber=maya_fields["version"]
        path_cache_dep=mayaPath.replace(root, "")



        dep_fields= ['name','id']
        dep_filters= [['path_cache', 'is', path_cache_dep]]

        dependency=sg.find_one('PublishedFile',dep_filters, dep_fields)

        
        #version
        abc_data = {

            "code": code,
            "created_by" : user,
            "description": "Single Character Alembic",
            "entity": entity,
            "published_file_type": publish_type,
            "name": publish_name,
            "path": {'content_type': None,
            'link_type': 'local',
            'local_path': publish_path,
            'local_path_linux': publish_path,},
            'path_cache': path_cache,
            'project': project,
            'task': task,
            'image': item.get_thumbnail_as_path(), 
            'upstream_published_files': [dependency],
            'version_number' : versionNumber
        }

        sg.create("PublishedFile", abc_data )
        print 'El archivo {0}, se ha publicado satisfactoriamente'.format(code)



    def _construct_environment_varibles(self):
        import maya.cmds as cmds

        evariables = " "
        arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa3202/"

        alShaders = "/ollin/ovfxToolkitSG/Resources/plugins/autodesk/"
        alShaders += "alShaders-linux-1.0.0rc19-ai4.2.12.2"
        maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'

        if '2019' in cmds.about(version=True):
            arnoldPath = '/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa401'
            maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules2019'

        evariables += " -prop EnvironmentKeyValue0=ARNOLD_PLUGIN_PATH={0}".format(arnoldPath)
        evariables += " -prop EnvironmentKeyValue1=MTOA_TEMPLATES_PATH={0}/ae".format(alShaders)
        evariables += " -prop EnvironmentKeyValue2=MAYA_CUSTOM_TEMPLATE_PATH={0}/aexml".format(alShaders)
        evariables += " -prop EnvironmentKeyValue3=MAYA_MODULE_PATH={0}".format(maya_modules)
        evariables += " -prop EnvironmentKeyValue4=MAYA_DISABLE_CIP=1"

        return evariables


    def sendJobs(self, batch_cmd):
        abc= batch_cmd
        deadline = '/opt/Thinkbox/Deadline10/bin/deadlinecommand'
     
        py_cmd = abc
        process = subprocess.Popen(deadline + " " + py_cmd, shell=True, stdout=subprocess.PIPE)

        out, err = process.communicate()
        print 'Out: ', out
        print 'Err: ', err

        return out


    def getAlembicSteps(self, sg, entity):
        field_name = 'sg_substeps_alembic'
        fields = [field_name]
        try:
            entity_ = sg.find_one(entity['type'], [['id', 'is', entity['id']]], fields)
            if entity_[field_name] is None:
                return 0
            if entity_[field_name] in [0, 1]:
                return 0
            return 1.0/float(entity_[field_name])
        except:
            return 0


    ############################################################################
    # protected methods
def _session_path():
    """
    Return the path to the current session
    :return:
    """
    path = cmds.file(query=True, sn=True)

    if isinstance(path, unicode):
        path = path.encode("utf-8")

    return path


def _get_save_as_action():
    """
    Simple helper for returning a log action dict for saving the session
    """

    engine = sgtk.platform.current_engine()

    # default save callback
    callback = cmds.SaveScene

    # if workfiles2 is configured, use that for file save
    if "tk-multi-workfiles2" in engine.apps:
        app = engine.apps["tk-multi-workfiles2"]
        if hasattr(app, "show_file_save_dlg"):
            callback = app.show_file_save_dlg

    return {
        "action_button": {
            "label": "Save As...",
            "tooltip": "Save the current session",
            "callback": callback
        }
    }

