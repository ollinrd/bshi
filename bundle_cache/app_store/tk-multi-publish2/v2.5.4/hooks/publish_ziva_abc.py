# Copyright (c) 2017 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import pprint
import traceback
import subprocess
from sgtk.util.filesystem import ensure_folder_exists
import sgtk
from sgtk.util.filesystem import copy_file, ensure_folder_exists

HookBaseClass = sgtk.get_hook_baseclass()
import pymel.core as pm
import maya.cmds as cmds

class BasicABCFilePublishPlugin(HookBaseClass):
    """
    Plugin for creating generic publishes in Shotgun.

    This plugin is typically configured to act upon files that are dragged and
    dropped into the publisher UI. It can also be used as a base class for
    other file-based publish plugins as it contains standard operations for
    validating and registering publishes with Shotgun.

    Once attached to a publish item, the plugin will key off of properties that
    drive how the item is published.

    The ``path`` property, set on the item, is the only required property as it
    informs the plugin where the file to publish lives on disk.

    The following properties can be set on the item via the collector or by
    subclasses prior to calling methods on the base class::

        ``sequence_paths`` - If set in the item properties dictionary, implies
            the "path" property represents a sequence of files (typically using
            a frame identifier such as %04d). This property should be a list of
            files on disk matching the "path". If the ``work_template`` property
            is set, and corresponds to the listed frames, fields will be
            extracted and applied to the publish_template (if set) and copied to
            that publish location.

        ``work_template`` - If set in the item properties dictionary, this
            value is used to validate ``path`` and extract fields for further
            processing and contextual discovery. For example, if configured and
            a version key can be extracted, it will be used as the publish
            version to be registered in Shotgun.

    The following properties can also be set by a subclass of this plugin via
    :meth:`Item.properties` or :meth:`Item.local_properties`.

        publish_template - If set, used to determine where "path" should be
            copied prior to publishing. If not specified, "path" will be
            published in place.

        publish_type - If set, will be supplied to SG as the publish type when
            registering "path" as a new publish. If not set, will be determined
            via the plugin's "File Type" setting.

        publish_path - If set, will be supplied to SG as the publish path when
            registering the new publish. If not set, will be determined by the
            "published_file" property if available, falling back to publishing
            "path" in place.

        publish_name - If set, will be supplied to SG as the publish name when
            registering the new publish. If not available, will be determined
            by the "work_template" property if available, falling back to the
            ``path_info`` hook logic.

        publish_version - If set, will be supplied to SG as the publish version
            when registering the new publish. If not available, will be
            determined by the "work_template" property if available, falling
            back to the ``path_info`` hook logic.

        publish_dependencies - A list of files to include as dependencies when
            registering the publish. If the item's parent has been published,
            it's path will be appended to this list.

    NOTE: accessing these ``publish_*`` values on the item does not necessarily
    return the value used during publish execution. Use the corresponding
    ``get_publish_*`` methods which include fallback logic when no property is
    set. For example, if a ``work_template`` is used, the publish version and
    name might be extracted from the template fields in the fallback logic.

    This plugin will also set an ``sg_publish_data`` property on the item during
    the ``publish`` method which may be useful for child items.

        ``sg_publish_data`` - The dictionary of publish information returned
            from the tk-core register_publish method.

    NOTE: If you have multiple plugins acting on the same item, and you need to
    access or operate on the publish data, you can extract the
    ``sg_publish_data`` from the item after calling the base class ``publish``
    method in your plugin subclass.
    """

    ############################################################################
    # standard publish plugin properties


    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder        
        return os.path.join(
            self.disk_location,
            "icons",
            "ziva_icon.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish to Shotgun"


    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """
        Publishes the file to Shotgun. A <b>Publish</b> entry will be
        created in Shotgun which will include a reference to the file's current
        path on disk. Other users will be able to access the published file via
        the <b><a href='%s'>Loader</a></b> so long as they have access to
        the file's location on disk.

        <h3>File versioning</h3>
        The <code>version</code> field of the resulting <b>Publish</b> in
        Shotgun will also reflect the version number identified in the filename.
        The basic worklfow recognizes the following version formats by default:

        <ul>
        <li><code>filename.v###.ext</code></li>
        <li><code>filename_v###.ext</code></li>
        <li><code>filename-v###.ext</code></li>
        </ul>

        <br><br><i>NOTE: any amount of version number padding is supported.</i>

        <h3>Overwriting an existing publish</h3>
        A file can be published multiple times however only the most recent
        publish will be available to other users. Warnings will be provided
        during validation if there are previous publishes.
        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        return {
            "File Types": {
                "type": "list",
                "default": [
                    ["Alembic Geo", "abc"],
                    ["Alembic Cache", "abc"],
                    ["Ziva Alembic", "abc"],
                    ["3dsmax Scene", "max"],
                    ["NukeStudio Project", "hrox"],
                    ["Houdini Scene", "hip", "hipnc"],
                    ["Maya Scene", "ma", "mb"],
                    ["Motion Builder FBX", "fbx"],
                    ["Nuke Script", "nk"],
                    ["Photoshop Image", "psd", "psb"],
                    ["Rendered Image", "dpx", "exr"],
                    ["Texture", "tiff", "tx", "tga", "dds"],
                    ["Image", "jpeg", "jpg", "png"],
                    ["Movie", "mov", "mp4"],
                ],
                "description": (
                    "List of file types to include. Each entry in the list "
                    "is a list in which the first entry is the Shotgun "
                    "published file type and subsequent entries are file "
                    "extensions that should be associated."
                )
            },
        }

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["maya.session.characterFX"]

    ############################################################################
    # standard publish plugin methods

    def accept(self, settings, item):


        result = [str(mset) for mset in cmds.listSets(allSets=True) if 'set_sgpublish' == str(mset).lower()]            
        if result:
            zSolver_list= pm.ls(type="zSolverTransform")
            if zSolver_list:
                # return the accepted info
                print "There is a zSolverTransform node type"
                return {"accepted": True}
        
        return {"accepted": False}

    def validate(self, settings, item):
        publish_path =  item.properties.get("ziva_abc_path")
        publisher = self.parent
        engine = sgtk.platform.current_engine()
        #overwrite = settings.get("user_data")["overwrite"]
        overwrite = engine.overwrite_publish
        print 'File overwrite set: ', overwrite

        # ---- determine the information required to validate

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.
        publish_path = item.properties.work_path         
        publish_name = str(item.properties['publish_name'])

        current_version = item.properties["fields"]["version"]
        filters = [["published_file_type.PublishedFileType.code", "is", "Ziva Alembic"], ['name', 'is', publish_name], ['version_number', 'is', current_version]]
        print 'filters: ', filters

        publishes = self.parent.engine.shotgun.find_one('PublishedFile', filters, [])

        publishes = self.parent.engine.shotgun.find_one('PublishedFile', 
            [["published_file_type.PublishedFileType.code", "is", "Alembic Geo"], ['name', 'is', publish_name], ['version_number', 'is', current_version]], 
            [])
        if publishes:
            self.logger.debug(
                "Conflicting publishes: %s" % (pprint.pformat(publishes),))

            publish_template = self.get_publish_template(settings, item)
            # -
            if not overwrite:
                # templates are in play and there is already a publish in SG
                # for this file path. We will raise here to prevent this from
                # happening.
                error_msg = (
                    "Can X not validate file path. There is already a publish in "
                    "Shotgun that matches this path. Please uncheck this "
                    "plugin or save the file to a different path."
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)

            else:                
                conflict_info = (
                    "If you continue, these conflicting publishes will no "
                    "longer be available to other users via the loader:<br>"
                    "<pre>%s</pre>" % (pprint.pformat(publishes),)
                )
                self.logger.warn(
                    "Found %s conflicting publishes in Shotgun" %
                        (len(publishes),),
                    extra={
                        "action_show_more_info": {
                            "label": "Show Conflicts",
                            "tooltip": "Show conflicting publishes in Shotgun",
                            "text": conflict_info
                        }
                    }
                )

        self.logger.info("A Publish will be created in Shotgun")     

        #if self.parent.context.entity['type'] == 'Asset':
        #    self.validate_internal_review(publish_path, current_version)           

        return True


    def validate_internal_review(self, path, version_number):
        #
        project = self.parent.context.project
        entity = self.parent.context.entity

        publish_name = os.path.splitext(os.path.basename(path))[0]

        # get the internal review version for current context
        version = self.parent.engine.shotgun.find_one('Version', 
            [['project', 'is', project], ['entity', 'is', entity], ['code', 'is', publish_name], ['sg_status_list', 'is', 'apr']], 
            [])
        
        if not version:
            import pymel
            mess = pymel.core.confirmDialog(t="Confirm",
                                            m= "There is  no  internal review version for this file \ndo you want to continue?",
                                            button = ["Yes", "No"], cancelButton = "No")
            if not mess == 'Yes':
                error_msg = "There is no internal review version aproved in shotgun for actual version number: {0}.\n".format(version_number)
                error_msg += "A internal review version must be aproved first in order to publish."
                raise Exception(error_msg)

        return False


    def validate_publish_dependencies(self, entity_id, dependencies):

        #tk = sgtk.sgtk_from_entity('Task', 31876)
        #root_path = tk.pipeline_configuration.get_local_storage_roots()['primary'] + '/'

        validation = []
        for path in dependencies:
            tk = sgtk.sgtk_from_path(path)
            root_path = tk.pipeline_configuration.get_local_storage_roots()['primary'] + '/'            
            pseudo_cache_path = path.replace(root_path, '')
            filters = [['path_cache', 'in', pseudo_cache_path]]
            result = self.parent.shotgun.find_one('PublishedFile', filters)
            print 'reult: ', result
            if result:
                validation.append(result)

        return validation

    def get_nodes_to_export(self, cmds):       
        node_list = []
        result = [set for set in cmds.listSets(allSets=True) if 'set_sgpublish' == str(set).lower()]        
        if result:
            set_list = cmds.sets(result[0], q=True)
            cmds.select(set_list)
            node_list = [str(node) for node in cmds.ls(selection=True, l=True)]    

        return node_list

    def export_to_alembic(self, abc_path):
        import maya.cmds as cmds

        nodes_to_export = self.get_nodes_to_export(cmds)

        start_frame = 1
        end_frame = 1
        list_of_nodes = ''
        for node in nodes_to_export:
            list_of_nodes += '-root {0} '.format(node)

        abc_steps_complete = ['character fx']
        c_step = self.parent.engine.context.step['name'].lower()
        if c_step in abc_steps_complete:
            start_frame = cmds.playbackOptions(query=True, min=True)
            end_frame = cmds.playbackOptions(query=True, max=True)

        abc_cmd = "-frameRange {0} {1}".format(start_frame, end_frame)
        if c_step in ['animation', 'layout', 'RnD', 'charcater fx']:
            sg = self.parent.engine.shotgun
            entity = self.parent.context.entity
            steps = self.getAlembicSteps(sg, entity)
            if steps != 0:
                abc_cmd += ' -step {0}'.format(steps)
        abc_cmd += " -ro -writeVisibility -uvWrite -worldSpace"
        abc_cmd += " -writeUVSets"

        if c_step == 'matchmove':
            abc_cmd += " -ef"

        abc_cmd += " -dataFormat ogawa {0}".format(list_of_nodes)
        abc_cmd += "-file {0}".format(abc_path)

       
       #try:
            #cmds.AbcExport(j=abc_cmd)
        #except Exception as e:
            #raise Exception("Failed execute Alembic command\n" + str(e))
        #return True
        
        return abc_cmd

    def getAlembicSteps(self, sg, entity):
        field_name = 'sg_substeps_alembic'
        fields = [field_name]
        try:
            entity_ = sg.find_one(entity['type'], [['id', 'is', entity['id']]], fields)
            if entity_[field_name] is None:
                return 0
            if entity_[field_name] in [0, 1]:
                return 0
            return 1.0/float(entity_[field_name])
        except:
            return 0


    def custom_sg(self):
        import sys
        sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
        from shotgun_api3 import Shotgun
        sys.path.append("/nfs/ovfxToolkit/Resources")
        from pythonTools.shotgunUtils.dbConnect import sgConnect

        sgConn = sgConnect()
        codeName = "editorial_status"
        SCRIPT_NAME = sgConn.API_NAMES.get(codeName)
        SCRIPT_KEY = sgConn.get_apikey_from_apiname(codeName)

        sg = Shotgun("https://0vfx.shotgunstudio.com", script_name=SCRIPT_NAME, api_key=SCRIPT_KEY)

        return sg
    
    def publish(self, settings, item):
        print 'alembic publish process started...'
        publish_path = item.properties['ziva_abc_path']
        publish_type = item.properties['ziva_publish_type']
        publish_name = os.path.basename(item.properties['ziva_abc_path']).split('.')[0]

        #Folder Exist
        self.parent.engine.ensure_folder_exists(os.path.dirname(publish_path))
        #Mandar a Alembic Granja del grabjerooo
        
        abc_command=self.export_to_alembic(publish_path)
        batch_cmd= self.ZivaToDeadline(abc_command, publish_path, publish_name)
        print batch_cmd
        job= self.sendJob(batch_cmd)

        #Preguntar si en el set hay mas de un namespace
        engine= sgtk.platform.current_engine()
        step= engine.context.step['name']
        

        publisher = self.parent
        publish_version = item.properties["fields"]['version']
        publish_path = item.properties['ziva_abc_path']
        print 'Get dependencies ...'
        publish_dependencies = self.get_publish_dependencies(settings, item)
        publish_name = item.properties["publish_name"]
        #publish_name, extension = os.path.splitext(publish_name)
        
        # if the parent item has a publish path, include it in the list of
        # dependencies
        if "sg_publish_path" in item.parent.properties:
            publish_dependencies.append(item.parent.properties.sg_publish_path)

        # handle copying of work to publish if templates are in play
        #self._copy_work_to_publish(settings, item)

        # arguments for publish registration
        self.logger.info("Registering publish...")

        #pub_dependencies = self.validate_publish_dependencies(item.context.task['id'], publish_dependencies)
        pub_dependencies = publish_dependencies

        print 'Data name to publish: ', publish_name
        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": pub_dependencies
        }

       
        # log the publish data for debugging
        self.logger.debug(
            "Populated Publish data...",
            extra={
                "action_show_more_info": {
                    "label": "Publish Data",
                    "tooltip": "Show the complete Publish data dictionary",
                    "text": "<pre>%s</pre>" % (pprint.pformat(publish_data),)
                }
            }
        )

        # Search for the actual publish name in sg
        current_version = item.properties["fields"]["version"]
        print 'current_version: ', current_version
        actual_publish_name = item.properties["publish_name"]
        print 'actual_publish_name: ', actual_publish_name
        publish = self.parent.engine.shotgun.find_one('PublishedFile',
            [["published_file_type.PublishedFileType.code", "is", "Ziva Alembic"], ['name', 'is', str(actual_publish_name)], ['version_number', 'is', current_version]],
            ["code", "path", "path_cache", "path_cache_storage", "name"])

        # create the publish and stash it in the item properties for other
        # plugins to use.
        print 'Publish found: ', publish

        if publish:
            sg = self.custom_sg()
            print 'SG: ', sg
            sg.delete('PublishedFile', publish['id'])
            print 'DELETED: ', publish['id']

        # Register the actual publish
        print 'publish data ABC: ', publish_data
        publish = sgtk.util.register_publish(**publish_data)
        print 'Published registered for Ziva ABC Files: ', publish['id']


        """
        if not publish:
            # Register the actual publish
            publish = sgtk.util.register_publish(**publish_data)
            print 'Published: ', publish
            print 'Data: ', publish_data
        else:
            print 'Publish already registered, updating data...'

            #
            filters = [["code", "is", publish_type]]
            sg_published_file_type = self.parent.shotgun.find_one('PublishedFileType', filters=filters)
            print 'sg_published_file_type: ', publish_type, ' : ', sg_published_file_type

            thumbnail_path = item.get_thumbnail_as_path()
            print 'thumbnail_path: ', thumbnail_path

            #Update the published found with current data
            data = {
                "dependency_paths": pub_dependencies
            }

            if item.description:
                data["description"] = item.description

            print 'Updating published file...'
            publish_update = self.parent.shotgun.update(publish['type'], publish['id'], data)
            print 'Data result: ', publish_update
            print 'Data updated: ', data

            if thumbnail_path:            
                print 'Uploading new thumbnail...'
                self.parent.shotgun.upload_thumbnail("PublishedFile", publish["id"], thumbnail_path)
                print 'Publish updated!'
        """

        item.properties["sg_publish_data"] = publish
        # item.properties.sg_publish_data = sgtk.util.register_publish(**publish_data)

        self.logger.info("Publish registered!")


    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent

        # get the data for the publish that was just created in SG
        publish_data = item.properties.sg_publish_data

        # ensure conflicting publishes have their status cleared
        publisher.util.clear_status_for_conflicting_publishes(
            item.context, publish_data)

        self.logger.info(
            "Cleared the status of all previous, conflicting publishes")

        path = item.properties["ziva_abc_path"]
        self.logger.info(
            "Publish created for file: %s" % (path,),
            extra={
                "action_show_in_shotgun": {
                    "label": "Show Publish",
                    "tooltip": "Open the Publish in Shotgun.",
                    "entity": publish_data
                }
            }
        )


    def get_publish_template(self, settings, item):
        """
        Get a publish template for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A template representing the publish path of the item or
            None if no template could be identified.
        """

        return item.get_property("publish_template")



    def get_publish_dependencies(self, settings, item):
        """
        Get publish dependencies for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A list of file paths representing the dependencies to store in
            SG for this publish
        """

        #
        if self.parent.engine.context.entity["type"].lower()== "asset":
            ma_publish_template = self.parent.engine.get_template_by_name("maya_asset_publish")
            ma_publish_path = ma_publish_template.apply_fields(item.properties.fields)
            print 'ma_publish_path: ', ma_publish_path

        elif self.parent.engine.context.entity["type"].lower()== "shot":
            ma_publish_template = self.parent.engine.get_template_by_name("maya_shot_publish")
            ma_publish_path = ma_publish_template.apply_fields(item.properties.fields)
            print 'ma_publish_path: ', ma_publish_path

        # local properties first
        dependencies = [str(ma_publish_path)]

        return dependencies


############ Send Ziva Node to Deadline ##############
 


    def ZivaToDeadline(self, abc_cmd, publish_path, publish_name): 
        #General Data
        z_abc_folder= os.path.dirname(publish_path)
        script_name= os.path.basename(publish_path).split('.')[0]

        engine = sgtk.platform.current_engine()
        e_type= engine.context.entity['type'].lower()

        #Batch Commands
        batch_cmds_ls= []


        #deadline Command
        file_path= cmds.file(exn=1,q=1)
        f_dir = os.path.dirname(file_path)
        flags = ' "-proj {0} -batch -file {1}'.format(f_dir, file_path)   
        flags += " -command  'AbcExport -j "
        flags += '<QUOTE> {0} <QUOTE>'.format(abc_cmd)
        flags += ';'
        flags += "'"
        flags += '"'

        maya_path = '/usr/autodesk/maya2018/bin/maya'
        if '2019' in cmds.about(version=True):
            maya_path = '/usr/autodesk/maya2019/bin/maya'

        start_frame=cmds.playbackOptions(query=True, min=True)

        end_frame=cmds.playbackOptions(query=True, max=True)

        chunkN = int((end_frame-start_frame)/10)
        if chunkN < 2:
            chunkN = 2
        if chunkN > 10:
            chunkN = 10

        batch_cmd = " -SubmitCommandLineJob"
        batch_cmd += " -executable {0}".format(maya_path)
        batch_cmd += " -arguments {0}".format(flags)
        batch_cmd += ' -pool cg'
        batch_cmd += ' -chunksize {0}'.format(chunkN)
        batch_cmd += ' -name "{0}"'.format('{0}, {1}, Ziva Abc'.format(e_type, publish_name))
        batch_cmd += ' -prop ConcurrentTasks=1'
        batch_cmd += ' -prop BatchName={0}'.format(script_name)       
        batch_cmd += ' -prop OutputDirectory0={0}'.format(z_abc_folder) 
        batch_cmd += self._construct_environment_varibles()

        print 'abc batch_cmd: ', batch_cmd
    
        return batch_cmd


    def _construct_environment_varibles(self):
        import maya.cmds as cmds

        evariables = " "
        arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa3202/"

        alShaders = "/ollin/ovfxToolkitSG/Resources/plugins/autodesk/"
        alShaders += "alShaders-linux-1.0.0rc19-ai4.2.12.2"
        maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'

        if '2019' in cmds.about(version=True):
            arnoldPath = '/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa401'
            maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules2019'

        evariables += " -prop EnvironmentKeyValue0=ARNOLD_PLUGIN_PATH={0}".format(arnoldPath)
        evariables += " -prop EnvironmentKeyValue1=MTOA_TEMPLATES_PATH={0}/ae".format(alShaders)
        evariables += " -prop EnvironmentKeyValue2=MAYA_CUSTOM_TEMPLATE_PATH={0}/aexml".format(alShaders)
        evariables += " -prop EnvironmentKeyValue3=MAYA_MODULE_PATH={0}".format(maya_modules)
        evariables += " -prop EnvironmentKeyValue4=MAYA_DISABLE_CIP=1"
        evariables += " -prop EnvironmentKeyValue5=zivadyn_LICENSE=5053@ovfxlicenses"

        return evariables


    def sendJob(self, batch_cmd):
        abc= batch_cmd
        deadline = '/opt/Thinkbox/Deadline10/bin/deadlinecommand'
     
        py_cmd = abc
        process = subprocess.Popen(deadline + " " + py_cmd, shell=True, stdout=subprocess.PIPE)

        out, err = process.communicate()
        print 'Out: ', out
        print 'Err: ', err

        return out

    ############################################################################
    # protected methods


