import os
import sys
#Shotgun
import sgtk
from sgtk.util.filesystem import copy_file, ensure_folder_exists

HookBaseClass = sgtk.get_hook_baseclass()

class BasicNotesPublishPlugin(HookBaseClass):
	"""
	Plugin for creating generic publishes in Shotgun.

	This plugin is typically configured to act upon files that are dragged and
	dropped into the publisher UI. It can also be used as a base class for
	other file-based publish plugins as it contains standard operations for
	validating and registering publishes with Shotgun.

	Once attached to a publish item, the plugin will key off of properties that
	drive how the item is published.

	The ``path`` property, set on the item, is the only required property as it
	informs the plugin where the file to publish lives on disk.

	The following properties can be set on the item via the collector or by
	subclasses prior to calling methods on the base class::

		``sequence_paths`` - If set in the item properties dictionary, implies
			the "path" property represents a sequence of files (typically using
			a frame identifier such as %04d). This property should be a list of
			files on disk matching the "path". If the ``work_template`` property
			is set, and corresponds to the listed frames, fields will be
			extracted and applied to the publish_template (if set) and copied to
			that publish location.

		``work_template`` - If set in the item properties dictionary, this
			value is used to validate ``path`` and extract fields for further
			processing and contextual discovery. For example, if configured and
			a version key can be extracted, it will be used as the publish
			version to be registered in Shotgun.

	The following properties can also be set by a subclass of this plugin via
	:meth:`Item.properties` or :meth:`Item.local_properties`.

		publish_template - If set, used to determine where "path" should be
			copied prior to publishing. If not specified, "path" will be
			published in place.

		publish_type - If set, will be supplied to SG as the publish type when
			registering "path" as a new publish. If not set, will be determined
			via the plugin's "File Type" setting.

		publish_path - If set, will be supplied to SG as the publish path when
			registering the new publish. If not set, will be determined by the
			"published_file" property if available, falling back to publishing
			"path" in place.

		publish_name - If set, will be supplied to SG as the publish name when
			registering the new publish. If not available, will be determined
			by the "work_template" property if available, falling back to the
			``path_info`` hook logic.

		publish_version - If set, will be supplied to SG as the publish version
			when registering the new publish. If not available, will be
			determined by the "work_template" property if available, falling
			back to the ``path_info`` hook logic.

		publish_dependencies - A list of files to include as dependencies when
			registering the publish. If the item's parent has been published,
			it's path will be appended to this list.

	NOTE: accessing these ``publish_*`` values on the item does not necessarily
	return the value used during publish execution. Use the corresponding
	``get_publish_*`` methods which include fallback logic when no property is
	set. For example, if a ``work_template`` is used, the publish version and
	name might be extracted from the template fields in the fallback logic.

	This plugin will also set an ``sg_publish_data`` property on the item during
	the ``publish`` method which may be useful for child items.

		``sg_publish_data`` - The dictionary of publish information returned
			from the tk-core register_publish method.

	NOTE: If you have multiple plugins acting on the same item, and you need to
	access or operate on the publish data, you can extract the
	``sg_publish_data`` from the item after calling the base class ``publish``
	method in your plugin subclass.
	"""

	############################################################################
	# standard publish plugin properties
	
 	@property
   	def icon(self):
		"""
		Path to an png icon on disk
		"""
		# look for icon one level up from this hook's folder in "icons" folder        
		return os.path.join(
			self.disk_location,
			"icons",
			"notes.png"
		)
	
	@property
	def name(self):
		"""
		One line display name describing the plugin
		"""
		return "Create a reviewing note"

	@property
	def description(self):
		"""
		Verbose, multi-line description of what the plugin does. This can
		contain simple html for formatting.
		"""

		loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

		return """
		Publishes the file to Shotgun. A <b>Publish</b> entry will be
		created in Shotgun which will include a reference to the file's current
		path on disk. Other users will be able to access the published file via
		the <b><a href='%s'>Loader</a></b> so long as they have access to
		the file's location on disk.

		<h3>File versioning</h3>
		The <code>version</code> field of the resulting <b>Publish</b> in
		Shotgun will also reflect the version number identified in the filename.
		The basic worklfow recognizes the following version formats by default:

		<ul>
		<li><code>filename.v###.ext</code></li>
		<li><code>filename_v###.ext</code></li>
		<li><code>filename-v###.ext</code></li>
		</ul>

		<br><br><i>NOTE: any amount of version number padding is supported.</i>

		<h3>Overwriting an existing publish</h3>
		A file can be published multiple times however only the most recent
		publish will be available to other users. Warnings will be provided
		during validation if there are previous publishes.
		""" % (loader_url,)

	@property
	def settings(self):
		"""
		Dictionary defining the settings that this plugin expects to recieve
		through the settings parameter in the accept, validate, publish and
		finalize methods.

		A dictionary on the following form::

			{
				"Settings Name": {
					"type": "settings_type",
					"default": "default_value",
					"description": "One line description of the setting"
			}

		The type string should be one of the data types that toolkit accepts
		as part of its environment configuration.
		"""
		return {
			"File Types": {
				"type": "list",
				"default": [
					["Alembic Cache", "abc"],
					["3dsmax Scene", "max"],
					["NukeStudio Project", "hrox"],
					["Houdini Scene", "hip", "hipnc"],
					["Maya Scene", "ma", "mb"],
					["Motion Builder FBX", "fbx"],
					["Nuke Script", "nk"],
					["Photoshop Image", "psd", "psb"],
					["Rendered Image", "dpx", "exr"],
					["Texture", "tiff", "tx", "tga", "dds"],
					["Image", "jpeg", "jpg", "png"],
					["Movie", "mov", "mp4"],
				],
				"description": (
					"List of file types to include. Each entry in the list "
					"is a list in which the first entry is the Shotgun "
					"published file type and subsequent entries are file "
					"extensions that should be associated."
				)
			},
		}

	@property
	def item_filters(self):
		"""
		List of item types that this plugin is interested in.

		Only items matching entries in this list will be presented to the
		accept() method. Strings can contain glob patters such as *,
		for example
		["maya.*", "file.maya"]
		"""
		return ["maya.session.notification"]

	def accept(self, settings, item):
		posible_steps= ['character fx','rig','rnd']
		engine=self.parent.engine
		#return {"accepted": True}
		print 'Aceptadoooo'
		if engine.context.step['name'].lower() in posible_steps:
			# return the accepted info
			return {
            "accepted": True,
            "checked": True
        }
		#else:
			#return {"accepted" : False}

	def validate(self, settings, item):
		publisher= self.parent

		return True
		# Que este publicado
		# Si el item de publicado esta en check



	def publish(self, settings, item):
		sg = self.parent.engine.shotgun
		self.createNote()
		#self.updateTaskStatus()


	def finalize(self, settings, item):
		"""
		Execute the finalization pass. This pass executes once
		all the publish tasks have completed, and can for example
		be used to version up files.

		:param settings: Dictionary of Settings. The keys are strings, matching
			the keys returned in the settings property. The values are
			`Setting` instances.
		:param item: Item to process
		"""

		publisher = self.parent

		# get the data for the publish that was just created in SG
		#publish_data = item.properties.sg_publish_data

		# ensure conflicting publishes have their status cleared
		#publisher.util.clear_status_for_conflicting_publishes(
			#item.context, publish_data)

		self.logger.info(
			"Cleared the status of all previous, conflicting publishes")



	def createNote(self):
		sg = self.parent.engine.shotgun
		engine=self.parent.engine
		entity=engine.context.entity
		publisher = self.parent
		step= engine.context.step['name']
		entity=engine.context.entity
		task= engine.context.task
		task_name=engine.context.task['name']


		#Data
		project= engine.context.project['name']
		projectid= engine.context.project['id']
		pubFile= self.GetPublishFile()


		note_data= {	
			'project' :  {'type': 'Project', 'id': projectid},
			'content' : 'La tarea de  {0} esta terminada y lista para su revision'.format(task_name),
			'note_links': [entity,task,pubFile],
			'tasks' : [task],
			'created_by' : sgtk.platform.current_engine().context.user,
			'addressings_to': self.GetAddressee (),
			'subject': 'La tarea de  {0} esta lista para su revision'.format(step)
			}
		sg.create('Note', note_data)
		

	def GetAddressee(self):
	 
		sg = self.parent.engine.shotgun
		projectid= self.parent.context.project['id']

		engine=self.parent.engine

		if engine.context.step["name"].lower() == "character fx":
			field_name= 'sg_grooming_notification'
		elif engine.context.step["name"].lower() == 'rig':
			field_name= 'sg_rig_notification'

		# Get CG Leads
		filtro=[["id", "is", projectid]]
		addfields01=sg.find_one('Project', filtro, [field_name])
		CGLeads=addfields01[field_name]
		# Get vfxCoord
		addfields02= sg.find('Group', [['code', 'is', 'vfxCoord']],['code', 'users', 'addressing_to', 'sg_ticket_type', 'sg_priority']) 
		vfxCoord= addfields02[0]['users'] 
	   
		finalUsers = CGLeads + vfxCoord
	   
		print finalUsers
		return finalUsers

	def GetPublishFile(self):
		import sys
		import maya.cmds as cmds

		sg= self.parent.engine.shotgun
		entity= self.parent.engine.context.entity
		task= self.parent.engine.context.task
		code=os.path.basename(cmds.file(exn=1, q=1))
		path= sg.find_one('PublishedFile',[["entity", "is", entity ],["task", "is", task ], ['code', 'contains', code ]],['path'])

		if path:
			publishFile= path['path']
			return publishFile
		else:
			error_msg =("Can't create the review note, because tha file has not been published yet.")
			self.logger.error(error_msg)
			raise Exception(error_msg)
			return None 


	def updateTaskStatus(self):
		import sys
		import sgtk
		sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
		from shotgun_api3 import Shotgun
		SG_URL = 'https://0vfx.shotgunstudio.com'
		TANK_SCRIPT_NAME = "Toolkit"
		TANK_SCRIPT_KEY = "b20e30ae264400f844d4197b5805d1105ba6ddf06fd733b51a74b8fa290cbc73"
		sg = Shotgun(SG_URL,
		             TANK_SCRIPT_NAME,
		             TANK_SCRIPT_KEY)

		engine= sgtk.platform.current_engine()
		task= engine.context.task
		taskid=task["id"]
		step=engine.context.step["name"]
		
		#Task
		if step.lower()== "character fx":
			status= 'cmpt'
		else:
			status= 'rev'

		data={'sg_status_list':'{0}'.format(status)}

		sg.update("Task", taskid, data)

					

				












