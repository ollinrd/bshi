# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import sgtk
import maya.cmds as cmds
import maya.mel as mel

HookBaseClass = sgtk.get_hook_baseclass()


class GeometryPublishPlugin(HookBaseClass):
    """
    Plugin for publish geometry

    """

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(
            self.disk_location,
            os.pardir,
            "icons",
            "icon_256.png"
        )

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Publish maya geometry"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://support.shotgunsoftware.com/hc/en-us/articles/219033078"

        return """

        Test the new plug %s
        

        """ % (loader_url,)

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        # inherit the settings from the base publish plugin
        base_settings = super(GeometryPublishPlugin, self).settings or {}

        # settings specific to this class
        maya_publish_settings = {
            "Publish Template": {
                "type": "template",
                "default": None,
                "description": "Template path for published Geometry files. Should"
                               "correspond to a template defined in "
                               "templates.yml.",
            }
        }

        # update the base settings
        base_settings.update(maya_publish_settings)

        return base_settings

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["maya.session.geometry"]

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A submit for review task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled. 
            This plugin makes use of the tk-multi-reviewsubmission app; if this
            app is not available then the item will not be accepted, this method
            will return a dictionary with the accepted field that has a value of
            False.

            Several properties on the item must also be present for it to be
            accepted. The properties are 'path', 'publish_name', 'color_space',
            'first_frame' and 'last_frame'
        """
        print 'MAYA GEO ACEPT...'
        path = _session_path()
        sg_set = [set for set in cmds.listSets(allSets=True) if len(cmds.listSets(allSets=True)) and set == "set_sgPublish"]

        posible_steps = ['fx', 'look dev', 'rnd', 'model']

        if self.parent.engine.context.step['name'].lower() in posible_steps:

            print 'sg_set: ', sg_set
            if sg_set:
                print 'SG sets exists...'
                accepted = True
                publisher = self.parent
                template_name = settings["Publish Template"].value
                print 'template_name: ', template_name, item.parent.properties.get("work_template")
                # ensure a work file template is available on the parent item
                work_template = item.parent.properties.get("work_template")
                print 'work_template: ', work_template
                if not work_template:
                    self.logger.debug(
                        "A work template is required for the session item in order to "
                        "publish session cameras. Not accepting session cam item."
                    )
                    accepted = False

                # ensure the publish template is defined and valid and that we also have
                publish_template = publisher.get_template_by_name(template_name)
                if not publish_template:
                    self.logger.debug(
                        "The valid publish template could not be determined for the "
                        "session camera item. Not accepting the item."
                    )
                    accepted = False

                print publish_template
                # we've validated the publish template. add it to the item properties
                # for use in subsequent methods
                item.properties["publish_geo_template"] = publish_template

                # log the accepted file and display a button to reveal it in the fs
                self.logger.info(
                    "Camera  plugin accepted for the path: %s" % (path),

                    )
                return {
                    "accepted": accepted,
                    "checked": True
                }
            else:
                self.logger.info(
                    "Camera  plugin not accepted for the path: %s" % (path),

                    )
                print 'maya geo not accepted'
                return {
                    "accepted": False,
                    "visible": False,
                    "checked": True
                }
        else:
            self.logger.info(
                "Camera  plugin not accepted for the path: %s" % (path),

            )
            print 'maya geo not accepted'
            return {
                "accepted": False,
                "visible": False,
                "checked": True
            }

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish. Returns a
        boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        :returns: True if item is valid and not in proxy mode, False otherwise.
        """

        # the render task will always render full-res frames when publishing. If we're
        # in proxy mode in Nuke, that task will fail since there will be no full-res
        # frames rendered. The exceptions are if there is no proxy_render_template set
        # in the tk-nuke-writenode app, then the write node app falls back on the
        # full-res template. Or if they rendered in full res and then switched to
        # proxy mode later. In this case, this is likely user error, so we catch it.

        print '\n--GEO VALIDATE'
        path = _session_path()

        # ---- ensure the session has been saved

        if not path:
            # the session still requires saving. provide a save button.
            # validation fails.
            error_msg = "The Maya session has not been saved."
            self.logger.error(
                error_msg,
                extra=_get_save_as_action()
            )
            raise Exception(error_msg)

        # get the normalized path
        path = sgtk.util.ShotgunPath.normalize(path)

        if not cmds.ls(assemblies=True, long=True):
            error_msg = (
                "Validation failed because there is no cameras in the scene "
                "to be exported. You can uncheck this plugin or create "
                "cameras to export to avoid this error."
            )
            self.logger.error(error_msg)
            raise Exception(error_msg)

        sg_set = [set for set in cmds.listSets(allSets=True) if len(cmds.listSets(allSets=True)) and set == "set_sgPublish"]

        if not sg_set:
            error_msg = (
                "Validation failed because there is no SG set in the scene "
                "Please create a SG set from |PROXY node."
                "Contact lead for further knowledgment."
            )
            self.logger.error(error_msg)
            raise Exception(error_msg)

        # get the configured work file template
        work_template = item.parent.properties.get("work_template")
        publish_template = item.properties.get("publish_geo_template")

        # get the current scene path and extract fields from it using the work
        # template:
        work_fields = work_template.get_fields(path)

        # ensure the fields work for the publish template
        missing_keys = publish_template.missing_keys(work_fields)
        if missing_keys:
            error_msg = "Work file '%s' missing keys required for the " \
                        "publish template: %s" % (path, missing_keys)
            self.logger.error(error_msg)
            raise Exception(error_msg)

        # create the publish path by applying the fields. store it in the item's
        # properties. This is the path we'll create and then publish in the base
        # publish plugin. Also set the publish_path to be explicit.
        item.properties["path"] = publish_template.apply_fields(work_fields)
        item.properties["publish_geo_path"] = item.properties["path"]

        # use the work file's version number when publishing
        if "version" in work_fields:
            item.properties["publish_version"] = work_fields["version"]

        print 'item.properties["publish_geo_path"]: ', item.properties["publish_geo_path"]

        return True#super(GeometryPublishPlugin, self).validate(settings, item)

    def strip_version_from_name(self, path_string):

        name, ext = os.path.splitext(path_string)
        name = '_'.join(name.split('_')[:-1])

        return name

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        print '\nPUBLISH MAYA GEO '
        # get the current scene path and extract fields from it
        # using the work template:
        scene_path = os.path.abspath(cmds.file(query=True, sn=True))

        work_template = item.parent.properties.get("work_template")

        fields = work_template.get_fields(scene_path)

        print fields, scene_path
                
        # create the publish path by applying the fields
        # with the publish template:        
        publish_path = item.properties.get("publish_geo_path")        
        
        # ensure the publish folder exists:
        publish_folder = os.path.dirname(publish_path)
        self.parent.ensure_folder_exists(publish_folder)

        # determine the publish name:
        publish_code = os.path.basename(publish_path)
        publish_name = self.strip_version_from_name(publish_code)
        
        cmds.select(deselect=True, all=True)

        sg_set = [set for set in cmds.listSets(allSets=True) if len(cmds.listSets(allSets=True)) and set == "set_sgPublish"]
        if sg_set:
            sg_set = sg_set[0]

        parent_dict = {}        
        set_groups = cmds.listConnections('{0}.dagSetMembers'.format(sg_set))
        for set_group in set_groups:
            cmds.select(set_group, hierarchy=True)
            parent_name = cmds.listRelatives(set_group, parent=True)            
            if parent_name:
                parent_dict[set_group] = parent_name
                cmds.parent(set_group, world=True, absolute=True)

        fields['geo_name'] = fields['name']
        
        publish_folder = os.path.dirname(publish_path)
        self.parent.ensure_folder_exists(publish_folder)        

        cmds.file(publish_path, exportSelected=True, type="mayaAscii", 
                  f = True, pr =True, options= "v=0;")
      
        for key, value in parent_dict.iteritems():
            cmds.parent(key, value, absolute=True )

        print 'Geo file saved in: ', publish_path

        publish_version = fields['version']
        publish_type = 'Maya Geo'
        publish_dependencies = []
        publisher = self.parent
        

        # register the publish:            
        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": publish_dependencies
        }   

        geo_publish = sgtk.util.register_publish(**publish_data)
        print 'Publish Maya Geo .ma registred: ', geo_publish            


           

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once all the publish
        tasks have completed.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """
        pass


def _session_path():
    """
    Return the path to the current session
    :return:
    """
    path = cmds.file(query=True, sn=True)

    if isinstance(path, unicode):
        path = path.encode("utf-8")

    return path


def _get_save_as_action():
    """
    Simple helper for returning a log action dict for saving the session
    """

    engine = sgtk.platform.current_engine()

    # default save callback
    callback = cmds.SaveScene

    # if workfiles2 is configured, use that for file save
    if "tk-multi-workfiles2" in engine.apps:
        app = engine.apps["tk-multi-workfiles2"]
        if hasattr(app, "show_file_save_dlg"):
            callback = app.show_file_save_dlg

    return {
        "action_button": {
            "label": "Save As...",
            "tooltip": "Save the current session",
            "callback": callback
        }
    }

