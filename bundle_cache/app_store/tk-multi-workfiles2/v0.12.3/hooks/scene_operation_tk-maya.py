# Copyright (c) 2015 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.
import os
import sgtk
import sys
from sgtk import TankError
from sgtk.platform.qt import QtGui
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
sys.path.append('/usr/lib/python2.7/site-packages')
import yaml

import maya.cmds as cmds
import pymel.core as pm
import pymel.util
import maya.mel as mel

import sgtk
from sgtk.platform.qt import QtGui

HookClass = sgtk.get_hook_baseclass()


class SceneOperation(HookClass):
    """
    Hook called to perform an operation with the
    current scene
    """

    def execute(
        self,
        operation,
        file_path,
        context,
        parent_action,
        file_version,
        read_only,
        **kwargs
    ):
        """
        Main hook entry point

        :param operation:       String
                                Scene operation to perform

        :param file_path:       String
                                File path to use if the operation
                                requires it (e.g. open)

        :param context:         Context
                                The context the file operation is being
                                performed in.

        :param parent_action:   This is the action that this scene operation is
                                being executed for.  This can be one of:
                                - open_file
                                - new_file
                                - save_file_as
                                - version_up

        :param file_version:    The version/revision of the file to be opened.  If this is 'None'
                                then the latest version should be opened.

        :param read_only:       Specifies if the file should be opened read-only or not

        :returns:               Depends on operation:
                                'current_path' - Return the current scene
                                                 file path as a String
                                'reset'        - True if scene was reset to an empty
                                                 state, otherwise False
                                all others     - None
        """

        self.debug = True

        #Get Info from Shotgun
        entity_sg = None
        fields = []
        e_type = context.entity['type']
        e_id = context.entity['id']
        if e_type == 'Asset':
            fields = []
        elif e_type == 'Shot':
            fields = ['sg_tail_out', 'sg_head_in', 'sg_resy', 'sg_resx']

        print('Entity type:', e_type)
        print('Entity id:', e_id)
        print('fields:', fields)

        sg = self.parent.shotgun
        entity_sg = sg.find_one(e_type, [['id', 'is', e_id]], fields)


        if self.debug:
            print(30*'-')
            print('ovfx_scene_operation_tk-maya R&D')
            print('  Operation: ', operation)
            print(30*'-')


        if operation == "current_path":
            # return the current scene path
            return cmds.file(query=True, sceneName=True)


        elif operation in ["prepare_new", 'new_file']:
            if self.debug:
                print('Creating new file')
                print ("Test _")
            # Basic variables needed
            current_entity = context.entity
            self.statusTaskValidation(context)
            self.config_colorspace(context)
            if current_entity['type'] == 'Asset':
                self._new_asset_scene(context)
            self.config_plugins()
            self.config_renderSettings(context, entity_sg)

            #--------[Load Framework]----------
            fw2= self.load_framework("oln-framework-mayaspecs")

            step= context.step["name"]
            if step in ['animation', 'layout']:
                #Load ATools
                fw2.mayaAnimTools.loadATools()



        elif operation == "open":
            # do new scene as Maya doesn't like opening
            # the scene it currently has open!
            cmds.file(new=True, force=True)
            cmds.file(file_path, open=True, force=True)

            # Check if status on task is not in progress
            self.statusTaskValidation(context)
            self.config_colorspace(context)
            self.config_plugins()

            #--------[Load Framework]----------
            fw2= self.load_framework("oln-framework-mayaspecs")

            step= context.step["name"]
            if step in ['animation', 'layout']:
                #Load ATools
                fw2.mayaAnimTools.loadATools()

            self.config_renderSettings(context, entity_sg)

        elif operation == "save":
            # save the current scene:
            cmds.file(save=True)
        elif operation == "save_as":
            # first rename the scene as file_path:
            cmds.file(rename=file_path)

            # Maya can choose the wrong file type so
            # we should set it here explicitely based
            # on the extension
            maya_file_type = None
            if file_path.lower().endswith(".ma"):
                maya_file_type = "mayaAscii"
            elif file_path.lower().endswith(".mb"):
                maya_file_type = "mayaBinary"


            #[Update File and Artist Name and Date]
            mask_ls=cmds.ls(typ="zshotmask")

            if mask_ls:
                mask=cmds.ls(typ="zshotmask")[0]
                from datetime import date
                fp=cmds.file(query=True, sn=True)
                fileName= os.path.basename(fp).replace(".ma", "")

                user=context.user["name"]
                
                dateV= date.today()
                finalDate=str(dateV).replace("-", ' / ')

                cmds.setAttr("{0}.topLeftText".format(mask), fileName,type="string")
                cmds.setAttr("{0}.bottomLeftText".format(mask), user,type="string")
                cmds.setAttr("{0}.topRightText".format(mask), finalDate,type="string")
                

            # save the scene:
            if maya_file_type:
                cmds.file(save=True, force=True, type=maya_file_type)
            else:
                cmds.file(save=True, force=True)



        elif operation == "reset":
            """
            Reset the scene to an empty state
            """
            while cmds.file(query=True, modified=True):
                # changes have been made to the scene
                res = QtGui.QMessageBox.question(
                    None,
                    "Save your scene?",
                    "Your scene has unsaved changes. Save before proceeding?",
                    QtGui.QMessageBox.Yes
                    | QtGui.QMessageBox.No
                    | QtGui.QMessageBox.Cancel,
                )

                if res == QtGui.QMessageBox.Cancel:
                    return False
                elif res == QtGui.QMessageBox.No:
                    break
                else:
                    scene_name = cmds.file(query=True, sn=True)
                    if not scene_name:
                        cmds.SaveSceneAs()
                    else:
                        cmds.file(save=True)

            # do new file:
            cmds.file(newFile=True, force=True)
            return True


    def config_plugins(self):
        plugin_list = ['AbcExport', 'AbcImport']
        for p_name in plugin_list:
            if not cmds.pluginInfo(p_name, q=True, loaded=True):
                cmds.loadPlugin(p_name)

    def config_renderSettings(self, context, sg_entity):
        # Variables and plugins validation
        print 'Config render settings from App'
      
        arnold_loaded = False
      
        if  cmds.pluginInfo('mtoa', q=True, loaded=True) == False:
            cmds.loadPlugin('mtoa')
        arnold_loaded = True

        print arnold_loaded
  

        ##########################################################
        ####################      Common      ####################
        ##########################################################
        #### [Fie output] ####
        # Clear name prefix
        at_ = 'defaultRenderGlobals.imageFilePrefix'
        cmds.setAttr(at_, '', type='string')

        if arnold_loaded:
            #Change Render Using : Arnold
            cmds.setAttr("defaultRenderGlobals.currentRenderer", "arnold", type="string")
            import maya.mel as mel

            mel.eval('unifiedRenderGlobalsWindow;')
         

            # Current renderer
            at_00 = "defaultRenderGlobals.currentRenderer"
            cmds.setAttr(at_00, l=False)

            at_01 = "defaultRenderGlobals.currentRenderer"
            cmds.evalDeferred('cmds.setAttr("{0}", "arnold", type="string")'.format(at_01))

            # Image format
            at_02 = "defaultArnoldDriver.ai_translator"
            cmds.evalDeferred('cmds.setAttr("{0}", "exr", type="string")'.format(at_02))


            # Compression zips
            at_03 = "defaultArnoldDriver.exrCompression"
            cmds.evalDeferred('cmds.setAttr("{0}", 2)'.format(at_03))

            # Half presicion
            at_04 = "defaultArnoldDriver.halfPrecision"
            cmds.evalDeferred('cmds.setAttr("{0}", 1)'.format(at_04))

            # Autocrop
            val_ = str(context.entity["type"] == "Shot")
            at_05 = "defaultArnoldDriver.autocrop"
            cmds.evalDeferred('cmds.setAttr("{0}", {1})'.format(at_05, val_))

        #### [Metadata] ####
        # Frame/animation
        cmds.setAttr('defaultRenderGlobals.outFormatControl', 0)
        cmds.setAttr('defaultRenderGlobals.animation', 1)
        cmds.setAttr('defaultRenderGlobals.putFrameBeforeExt', 1)

        # Frame padding
        cmds.setAttr('defaultRenderGlobals.extensionPadding', 4)

        if context.step['name'].lower() == "matchmove":
            return

        #### [Frame Range] #####
        start_f = 1001
        end_f = 1096
        if context.entity['type'] == 'Shot':
            start_f_tmp = sg_entity['sg_head_in']
            if start_f_tmp is not None:
                start_f = start_f_tmp
            end_f_tmp = sg_entity['sg_tail_out']
            if end_f_tmp is not None:
                end_f = end_f_tmp

        # Start frame
        #cmds.setAttr('defaultRenderGlobals.startFrame', start_f)

        # End frame
        #cmds.setAttr('defaultRenderGlobals.endFrame', end_f)

        #### [Image Size] ####
        width_ = 1920
        height_ = 1080
        if context.entity['type'] == 'Shot':
            # Width
            width_tmp = sg_entity['sg_resx']
            if width_tmp is not None:
                width_ = width_tmp
            # Height
            height_tmp = sg_entity['sg_resy']
            if height_tmp is not None:
                height_ = height_tmp

        #cmds.setAttr('defaultResolution.width', width_)
        #cmds.setAttr('defaultResolution.height', height_)

        #### [Textures] ####
        if arnold_loaded:
            # Auto conver Textures to TX
            at_06= "defaultArnoldRenderOptions.autotx"
            cmds.evalDeferred('cmds.setAttr("{0}", 0)'.format(at_06))

            # Use existing TX Textures
            at_07 = "defaultArnoldRenderOptions.use_existing_tiled_textures"
            cmds.evalDeferred('cmds.setAttr("{0}", 1)'.format(at_07))

        cmds.evalDeferred('cmds.deleteUI("unifiedRenderGlobalsWindow")', lp= True)

    def config_colorspace(self, context):
        # Validating project color space config
        if self.debug:
            print('config_colorspace method')

        # Basic variables needed
        current_entity = context.entity
        sg = self.parent.shotgun

        colorspace_str = 'sRGB'

        # env variables
        context_project = context.project

        # Get sg project info
        sg_project = sg.find_one("Project",
                                 [["id", "is", context_project["id"]]],
                                 ["code", 'sg_color_space'])

        # Get sg info from curren entity
        entity_fields = ['sg_color_space']
        entity_filter = [["project", "is", sg_project],
                         ['code', 'is', current_entity['name']]]
        sg_entity = sg.find_one(current_entity['type'],
                                entity_filter,
                                entity_fields)

        # Getting color space from shotgun! ----------------
        invdalid_cs = [None, '', 'Default']

        if sg_entity['sg_color_space'] in invdalid_cs:
            methods_ = {
                'Asset': self.config_colorspace_asset,
                'Shot': self.config_colorspace_shot,
                'Sequence': self.config_colorspace_sequence}
            c_type = current_entity['type']
            if c_type in methods_.keys():
                colorspace_str = methods_[c_type](sg_project, sg, context)
            else:
                if self.debug:
                    feed = 'invalid config color space'
                    feed += ' for {0}'.format(c_type)
                    print(feed)
        else:
            if self.debug:
                print('\tColorspace from project')

        if colorspace_str in invdalid_cs:
            colorspace_str = 'sRGB'

        # Setting color space config! -------------------------
        if 'aces' in colorspace_str:
            # Enable color managment
            cmds.colorManagementPrefs(cmEnabled=True, e=True)

            # Advance configuration from ocio files!!
            ocio_path = '/nfs/ovfxToolkit/Resources/share/'
            ocio_path += 'OpenColorIO-Configs-master/'
            ocio_path += 'aces_1.0.3/config.ocio'

            if os.path.isfile(ocio_path):
                # Set maya configs

                # Set ocio config path
                cmds.colorManagementPrefs(e=True, configFilePath=ocio_path)

                # Enable read config from ocio file
                cmds.colorManagementPrefs(e=True, cfe=True)

                # setting render color space in ACESsg
                cmds.colorManagementPrefs(rsn='ACES - ACEScg', e=True)

                # Set viewer config
                cmds.colorManagementPrefs(vtn='sRGB (ACES)', e=True)

            else:
                # Enable read config from ocio file
                cmds.colorManagementPrefs(e=True, cfe=False)

                # setting render color space in ACESsg
                cmds.colorManagementPrefs(renderingSpaceName='ACEScg', e=True)

                # Config viewer color space
                cmds.colorManagementPrefs(vtn='ACES RRT v1.0', e=True)
                
                str_feed = 'RnD Message!\n\tNo Ocio config found! missing file'
                str_feed += ':\n\t{0}'.format(ocio_path)
                cmds.error(str_feed)

    def config_colorspace_asset(self, sg_project, sg, context):
        # Config color space if the current environment is asset
        if self.debug:
            print('config_colorspace_asset method')
            print('\tColorspace from project')
        return sg_project['sg_color_space']

    def config_colorspace_sequence(self, sg_project, sg, context):
        # Config color space if the current environment is asset
        if self.debug:
            print('config_colorspace_sequence method')
            print('\tColorspace from project')
        return sg_project['sg_color_space']

    def config_colorspace_shot(self, sg_project, sg, context):
        # Config color space if the current environment is shot
        if self.debug:
            print('config_colorspace_shot method')

        # Getting env info
        entity_ = context.entity
        entity_n = entity_['name']

        shot_fields = ['sg_color_space', 'sg_sequence', 'code']

        c_entity = sg.find_one("Shot",
                               [["project", "is", sg_project],
                                ['code', 'is', entity_n]],
                               shot_fields)

        # Getting sg sequence
        seq_id = c_entity['sg_sequence']['id']
        seq_filters = [['project', 'is', sg_project],
                       ['id', 'is', seq_id]]
        seq_fields = ['code', 'sg_color_space']
        sg_seq = sg.find_one('Sequence', seq_filters, seq_fields)

        colorspace = sg_seq['sg_color_space']
        if colorspace in [None, '', 'Default']:
            colorspace = sg_project['sg_color_space']
            if self.debug:
                print('\tColorspace from project')
        else:
            if self.debug:
                print('\tColorspace from sequence')
        return colorspace

    # TODO
    # metodo para validar el status de la task que se abrio
    def statusTaskValidation(self, context):
        if self.debug:
            print('Status task validation')

        sg = self.parent.shotgun

        # Getting task status from Shotgun db
        task = sg.find_one('Task',
                           [['project', 'is', context.project],
                            ['id', 'is', context.task['id']]],
                           ['sg_status_list'])

        # Possible status to verify if the artist
        # could change to in progress
        modif_status = ['wtg', 'hld']

        # If the SG query return nothing
        if not task:
            if self.debug:
                print('Task not found! check with pipeline team')
            return

        status_code = task['sg_status_list']

        if status_code in modif_status:
            if self.debug:
                print('Asking to change status')

            c_status = sg.find_one('Status',
                                   [['code', 'is', status_code]],
                                   ['name', 'code'])
            status_name = c_status['name']
            ttl = 'Status change'
            msgg = ' Attention !!!!!\n'
            msgg += 'This task status is\n'
            msgg += '>>>>  {0}  <<<<'.format(status_name)
            msgg += '\nDo yo want to change to "In progress"?'
            btns = ['Yes', 'No']
            opt = cmds.confirmDialog(title=ttl, message=msgg, button=btns,
                                     defaultButton='No', dismissString='No')

            if opt == 'Yes':
                # Getting "In progress" SG Status
                data = {'sg_status_list': 'ip'}
                sg.update('Task', context.task['id'], data)

    def _new_asset_scene(self, context):
        current_step = context.step['name'].lower()
        asset_name = context.entity['name']

        # Modelling!
        if current_step in ['rnd', 'model', 'look dev']:
            master_ = cmds.createNode('transform', n=asset_name)
            geo_ = cmds.createNode('transform', n='Geometry', p=master_)
            cmds.createNode('transform', n='Geo', p=geo_)


    def _call_SGCameraBurnins(self, sg_step):
        # SG Camera Burnins at startup
        OllinTools = sgtk.platform.current_engine().apps.get("tk-maya-ollin-tools", False)
        mod_cam = OllinTools.import_module("cam_operations")
        ollin_cam_ops = mod_cam.cam_operations(sg_step)
        cb = lambda : ollin_cam_ops.createUI("cam_operations")
        cb()
