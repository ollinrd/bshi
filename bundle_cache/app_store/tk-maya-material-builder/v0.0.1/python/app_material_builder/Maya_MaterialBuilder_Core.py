import maya.cmds as cmds
import pymel.core as pm
import os
#import sgtk
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')\


class Material_Ctrl():

    def __init__(self):
        """
        """

        self.extension = '.jpg'

        ColorCorrect = 'aiColorCorrect'
        Bump2d = 'bump2d'
        Range = 'aiRange'
        Clamp = 'aiClamp'
        displacement_ = 'displacement_Shader'

        self.shader_relations = {
            'baseColor':    {'tokens': ['COL', 'Albedo'],
                             'nodes': [ColorCorrect],
                             'input': 'outColor'},

            'normalCamera': {'tokens': ['BMP', 'NormalBump', 'Normal_LOD0'],
                             'nodes': [Bump2d],
                             'input': 'outNormal'},

            'opacity':      {'tokens': ['OPC', 'Transparency'],
                             'nodes': [Range, Clamp],
                             'input': 'outColor'},

            'specular':     {'tokens': ['Specular'],
                             'nodes': [Range, Clamp],
                             'input': 'outColor.outColorR'},

            'specularIOR':  {'tokens': ['SPC_IOR', 'Glossiness', 'Gloss'],
                             'nodes': [Range, Clamp],
                             'input': 'outColor.outColorR'},

            'specularRoughness':
                            {'tokens': ['RGH', 'Roughness'],
                             'nodes': [Range, Clamp],
                             'input': 'outColor.outColorR'},

            'subsurface':   {'tokens': ['SSS_WGHT'],
                             'nodes': [ColorCorrect, Range],
                             'input': 'outColor.outColorR'},

            'subsurfaceColor':
                            {'tokens': ['SSS_COL'],
                             'nodes': [ColorCorrect],
                             'input': 'outColor'},

            'subsurfaceRadius':
                            {'tokens': ['SSS_RDS'],
                             'nodes': [ColorCorrect, Range],
                             'input': 'outColor'},

            'transmission': {'tokens': ['TRNS'],
                             'nodes': [Range],
                             'input': 'outColor.outColorR'}
        }

        self.sg_relations = {
            'displacementShader': {
                'tokens': ['DSP', 'Height', 'Displacement'],
                'nodes': [displacement_],
                'input': 'displacement'}
        }
        self.methods = {
            ColorCorrect: self._create_aiColorCorrect,
            Bump2d: self._create_bump2d,
            Range: self._create_aiRange,
            Clamp: self._create_aiClamp,
            displacement_: self._create_shader_displacement
        }


    def _override_extension(self, ext):
        self.extension = ext

    def _get_validFiles(self, path):
        all_textures = []

        # r=root, d=directories, f = files
        for r, d, f in os.walk(path):
            for file in f:
                if self.extension in file:
                    all_textures.append(os.path.join(r, file))

        return all_textures

    def _createShader(self, tex_path):
        """
        """

        shader_rel = self.shader_relations
        sg_rel = self.sg_relations

        # Creating shader
        aiStandard = pm.shadingNode('aiStandardSurface', asShader=True)

        # Creating shading group
        aiStandard_sg = pm.sets(renderable=True, noSurfaceShader=True,
                                empty=True, name=aiStandard.name() + '_SG')

        pm.connectAttr(aiStandard.outColor, aiStandard_sg.surfaceShader, f=1)

        all_textures = self._get_validFiles(tex_path)

        for t_path in all_textures:
            # Creating file nodes with the texture path already loaded
            file_n = self._create_fileNodes(t_path)

            self._update_fileName(file_n)

            # Getting the relation to shader and shading group
            nodes_to_shader = self._get_dependencies(t_path, shader_rel)
            nodes_to_sg = self._get_dependencies(t_path, sg_rel)

            # Making nodes connections to shader ---------
            self._make_connections(file_n, nodes_to_shader, aiStandard,
                                   shader_rel)

            # Making nodes connections to SG -----------
            self._make_connections(file_n, nodes_to_sg, aiStandard_sg,
                                   sg_rel)

    def _get_dependencies(self, path, relation):
        """
        Processing AOV file type.

        Getting the necessary methods to
        process the current path depenting
        of the main config type of file.
        """
        # Getting inside self.shader_relations
        for rel in relation.keys():
            # Current is one of the dictionaries
            # Eg. Transmision: {token, nodes}
            current = relation[rel]
            # Verify if one of the tokens in the current dictionary
            # Then if the path has one of then, then return the list
            # of nodes that should be created.
            for token in current['tokens']:
                if token in path:
                    # Returning th list of nodes to create
                    return (rel, current['nodes'])

        return (None, [])

    def _create_fileNodes(self, path):
        """
        All connection between place2dTexture and
        file node
        """
        place2dTex = pm.shadingNode('place2dTexture', asUtility=True)
        fileN = pm.shadingNode('file', asTexture=True)
        sam_connections = [
            'coverage', 'translateFrame', 'rotateFrame',
            'mirrorU', 'mirrorV', 'stagger', 'wrapU',
            'wrapV', 'repeatUV', 'offset', 'rotateUV', 'noiseUV',
            'vertexUvOne', 'vertexUvTwo', 'vertexUvThree', 'vertexCameraOne']
        dif_connections = [
            ['outUV', 'uv'], ['outUvFilterSize', 'uvFilterSize']]

        for con in sam_connections:
            arg1 = '{0}.{1}'.format(place2dTex.name(), con)
            arg2 = '{0}.{1}'.format(fileN.name(), con)
            pm.connectAttr(arg1, arg2, f=1)
        for con in dif_connections:
            arg1 = '{0}.{1}'.format(place2dTex.name(), con[0])
            arg2 = '{0}.{1}'.format(fileN.name(), con[1])
            pm.connectAttr(arg1, arg2)

        pm.setAttr(fileN.fileTextureName, path)

        return fileN

    def _update_fileName(self, fileN):
        file_path = pm.getAttr(fileN.fileTextureName)
        file_path = os.path.basename(file_path)
        ext = '.{0}'.format(file_path.split('.')[-1])
        new_name = file_path.replace(ext, '_file')
        fileN.rename(new_name)

    def _create_aiRange(self, prev):
        """
        Creating aiRange node
            connecting from prev node outColor to new node input
        """
        # Creating new node
        aiRange = pm.shadingNode('aiRange', asUtility=True)

        #Connecting
        pm.connectAttr(prev.outColor, aiRange.input, f=1)

        #Renaming
        self._rename_node(prev, aiRange)
        return aiRange

    def _create_aiClamp(self, prev):
        """
        Creating aiClamp node
            connecting from prev node outColor to new node input
        """
        # Creating new node
        aiClamp = pm.shadingNode('aiClamp', asUtility=True)

        #Connecting
        pm.connectAttr(prev.outColor, aiClamp.input, f=1)

        #Renaming
        self._rename_node(prev, aiClamp)

        return aiClamp

    def _create_aiColorCorrect(self, prev):
        """
        Creating aiColorCorrect node
            connecting from prev node outColor to new node input
        """
        # Creating new node
        aiColorCorrect = pm.shadingNode('aiColorCorrect', asUtility=True)

        #Connecting
        pm.connectAttr(prev.outColor, aiColorCorrect.input, f=1)

        #Renaming
        self._rename_node(prev, aiColorCorrect)
        return aiColorCorrect

    def _create_bump2d(self, prev):
        """
        Creating bump2d node
            connecting from prev node outAlpha to new node bumpbalue
        """
        # Creating new node
        bump2d = pm.shadingNode('bump2d', asUtility=True)

        #Connecting
        pm.connectAttr(prev.outAlpha, bump2d.bumpValue, f=1)

        # Renaming
        self._rename_node(prev, bump2d)
        return bump2d

    def _create_shader_displacement(self, prev):
        """
        Creating displacement shader node
            connecting from prev node outAlpha to new node bumpbalue
        """

        # Creating new node
        cur_node = pm.shadingNode('displacementShader', asShader=True)

        #Connecting to prev node
        pm.connectAttr(prev.outAlpha, cur_node.displacement, f=1)

        return cur_node

    def _rename_node(self, prev, current):
        #Getting the corect name from pewviuos node and
        # Reasigned to the current node with the actual type
        # node at the end
        new_name = prev.name()
        rep = new_name.split('_')[-1]
        rep = '_{0}'.format(rep)
        type_str = '_{0}'.format(current.type())
        new_name = new_name.replace(rep, type_str)
        current.rename(new_name)

    def _make_connections(self, firstNode, nodes, goal, relations):
        # starting the chain with the first node
        #    file node usually
        prevNode = firstNode

        for node in nodes[1]:
            prevNode = self.methods[node](prevNode)
        # prevNode is the last node of the chain
        if nodes[0]:
            #Connecting
            # attribute of the goal to connect
            attr_key = nodes[0]
            node_attr = goal.name()
            node_attr += '.{0}'.format(attr_key)

            # Algo the attr_key is the relations dictionaries key, to get info
            # like input, nodes and tokens
            orig_attr = prevNode.name()
            orig_attr += '.{0}'.format(relations[attr_key]['input'])

            pm.connectAttr(orig_attr, node_attr, f=1)
    