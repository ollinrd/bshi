#!/usr/bin/env pythonS
# -⁻- coding: UTF-8 -*-
from sgtk.platform.qt import QtCore
import os
import maya.cmds as cmds


class FormMain():
    def __init__(self, parent=None, loc=None):
        # self.btn_accept.clicked.connect(self.general_settings)
        uiFileName = loc + "/resources/Maya_MaterialBuilder_mainWindow.ui"

        from pymel import versions
        if versions.current() < 201700:
            from PySide import QtUiTools
            import PySide
            self.PS = PySide.QtGui
            self.loader = QtUiTools.QUiLoader()
            self.QFileDialog = PySide.QtGui.QFileDialog
        else:
            from PySide2 import QtUiTools
            import PySide2
            self.PS = PySide2.QtWidgets
            self.loader = QtUiTools.QUiLoader()
            self.QFileDialog = PySide2.QtWidgets.QFileDialog

        uifile = QtCore.QFile(uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)
        self.ui = self.loader.load(uifile, parent)
        uifile.close()

        self.connect_buttons()

        # -------------------------------------------------
        # self.ui.frameGeometry().setHeight(180)
        """
        self.ui.btn_scan.clicked.connect(self._scan)
        self.ui.btn_create.clicked.connect(self._create)
        self.ui.btn_browser.clicked.connect(self._browser)

        self.frame_data = self.ui.frame_data
        self.cams_chkboxes = []
        """

    def connect_buttons(self):
        """
        Connect all button with their respective methods.
        """
        self.ui.btn_browser.clicked.connect(self._btn_browser)
        self.ui.btn_scan.clicked.connect(self._btn_scan)
        self.ui.btn_process.clicked.connect(self._btn_process)

    def _btn_browser(self):
        print('browser!!')
        self.ui.hide()
        fil = "Textures files (*.jpg *.exr *.tiff)"
        fileName = self.QFileDialog.getOpenFileName(filter=fil)
        fileF = fileName[0].split(os.sep)[-1]
        if '.' in fileF:
            ext = fileF.split('.')[-1]
            self.Core._override_extension(ext)
        path = fileName[0].replace(os.sep+fileF, '')
        if fileName:
            self.ui.lnEdit_path.setText(path)
        self.ui.show()

    def _btn_scan(self):
        print('scan!!')
        path = self.ui.lnEdit_path.text()
        if os.path.exists(path) and os.sep in path:
            """
            """
            text_ = 'Valid textures found:\n\n'
            text_ += '\n'.join(self.Core._get_validFiles(path))
            self.ui.lbl_feedback.setText(text_)
        else:
            self._invalid_path_behavior

    def _btn_process(self):
        print('process!!')
        path = self.ui.lnEdit_path.text()

        if os.path.exists(path) and os.sep in path:
            self.Core._createShader(path)
            self.ui.hide()
            ttl = 'Process'
            msgg = 'Material created!'
            btns = ['Ok']
            cmds.confirmDialog(title=ttl, message=msgg, button=btns,
                               defaultButton='Ok')

        else:
            self._invalid_path_behavior()

    def _invalid_path_behavior(self):
        ttl = 'Process'
        msgg = 'Invalid Path\nDo you want to try again?'
        btns = ['Yes', 'No']
        op = cmds.confirmDialog(title=ttl, message=msgg, button=btns,
                                defaultButton='Yes', dismissString='No')
        if op == 'Yes':
            self.ui.show()
        else:
            self.ui.hide()

    def set_Operator(self, CoreFunc):
        """
        """
        self.Core = CoreFunc

    def show(self):
        #self.ui.setWindowFlags(
        #    self.ui.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        self.ui.show()
        
