from sgtk.platform import Application


class MaterialBuilder(Application):
    def init_app(self):
        self.engine.register_command("Material builder", self.mainWindow)
        self.loaded = False

    def mainWindow(self):
        print('Material builder app')
        if not self.loaded:
            self.loaded = True
            self.mods_matBuilder = self.import_module("app_material_builder")
            self.mainForm = self.mods_matBuilder.FormMain(loc=self.disk_location)
            self.edlCtrl = self.mods_matBuilder.Material_Ctrl()

            # Set the communication between UI and core functions
            self.mainForm.set_Operator(CoreFunc=self.edlCtrl)
            self.mainForm.show()
        else:
            #self.mainForm.Clean()
            self.mainForm.show()
