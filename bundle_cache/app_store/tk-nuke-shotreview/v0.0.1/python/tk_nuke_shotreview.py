import nuke
# Plana de import nuke
import tank
import os

class ShotReview():
    def __init__(self):
        """
        """
        self.status_lib = []
        self.AllShotVersions = None

    def loadScene(self, lbl_feedback, mainWindow, cam, sg, engine):        
        #********************#
        self.feedback = ""
        self.msgBox = ""

        projID = engine.context.project.get('id')
        shotFilters = [
                        [ 'project', 'is', {'type': 'Project', 'id': projID}],
                        {
                            'filter_operator': 'all',
                            'filters': [['code', 'is', self.shot]]
                        }
                    ]

        fields = ['id', 'sg_head_in', 'sg_tail_out']
        shot_info = sg.find_one('Shot', shotFilters, fields)
        shot_id = shot_info.get('id')

        # Changing first and last frame from project config in Nuke
        nuke.root().knob('first_frame').setValue(shot_info.get('sg_head_in'))
        nuke.root().knob('last_frame').setValue(shot_info.get('sg_tail_out'))

        sg_filter = [
            ['project', 'is', {'type': 'Project', 'id': projID}],
            {
                'filter_operator': 'all',
                'filters': [
                    ['entity', 'is', {'type': 'Shot', 'id': shot_id}],
                    ['code', 'contains', self.shot]

                ]
            }
        ]
        fields = ['sg_status_list', 'code', 'sg_path_to_frames', 'created_at', 'sg_task.Task.step', 'sg_version_type']
        self.AllShotVersions = sg.find("Version", sg_filter, fields)


        # Cargar publish y obtener info
        publishInfo = self.load_publish()

        self.resolution = None
        readPublicado = None

        if publishInfo is not None:
            readPublicado = publishInfo[0] 

        # Load Main Plate
        self.load_MainPlate()

        # Shot sequence
        self.load_reference()
        
        # Load LUT
        if readPublicado is not None:
            self.load_LUT(readNode=readPublicado, projCode=self.projID, sg = sg, engine= engine)
        else:
            self.feedback += "\nThe lut does not load, because mainPlate does not exists."
            self.msgBox += "\nLut does not load"

        if nuke.activeViewer() is not None:
            activeViewer = nuke.activeViewer().node()
            activeViewer.knob('full_frame_processing').setValue("on")

        # Load Version 0 # Yabin Request
        #self.load_version0(shot_id)
        
        if self.feedback is not "":
            print self.feedback

        #mainWindow.hide()

        if self.msgBox is not "":
            lbl_feedback.setText("Warning! check script editor!")
            #nuke.message(self.msgBox)
        else:
            lbl_feedback.setText("Success!")
        return self.msgBox

    def load_publish(self):
        """
        """
        possibles_v = [v for v in self.AllShotVersions if not '_edt_' in v.get('code') and self.step == v.get("sg_task.Task.step")]
        lastPublish = self.getLastVersion(possibles_v)

        if not lastPublish:
            self.feedback += "\nNo publish version yet"
            self.msgBox += "No publish version yet"
            return None

        else:
            publishFullPath = lastPublish.get('sg_path_to_frames')

            fileName = publishFullPath.split('/')[-1]
            publishPath = publishFullPath.replace(fileName, '')

            if os.path.exists(publishPath):
                shotFrames = os.listdir( publishPath )
                frameNumber = []
                if len(shotFrames) != 0:
                    for i in shotFrames:
                        temp = i.split(".")
                        if len(temp) == 3:
                            temp = temp[1]
                            if temp.isdigit():
                                frameNumber.append(temp)
                    firstFrame = min(frameNumber)
                    lastFrame = max(frameNumber)
                    fileName = publishFullPath
                    lver = fileName.split('_')[-2]
                    lver = lver + '_' + fileName.split('_')[-1].split('.')[0]
                    labelPublicado = "PUBLISH_" + lver
                    self.readPublicado = nuke.nodes.Read(
                        file=fileName, first=firstFrame, 
                        last=lastFrame, label=labelPublicado)

                    return [self.readPublicado]
            else:
                self.feedback += "\nMissing publish:\n\t{0}".format(publishPath)
                self.msgBox += "Missing publish"
                return None

    def load_MainPlate(self):
        possibles_v = [v for v in self.AllShotVersions if (
            ('_edt_' in v.get('code')) and ('_mp' in v.get('code')) and (not '_ref_'  in v.get('code') and (not 'Quick Daily' in v['sg_version_type'])) 
            )]

        last_mp = self.getLastVersion(possibles_v)

        if not last_mp:
            self.feedback += "\nNo main plate publish.\n\tCheck with Editorial"
            self.msgBox += "No main plate publish"
        else:
            mpFullPath = last_mp.get('sg_path_to_frames')

            mpPathFile = mpFullPath.split('/')[-1]
            mpPath = mpFullPath.replace(mpPathFile, '')

            if os.path.exists(mpPath):
                mpFrames = os.listdir(mpPath)
                mpframeNumber = []
                for i in mpFrames:
                    temp = i.split(".")
                    if len(temp) == 3:
                        temp = temp[1]
                        #print temp
                        if temp.isdigit():
                            mpframeNumber.append(temp)

                mpfirstFrame = min(mpframeNumber)
                mplastFrame = max(mpframeNumber)

                #Crear read node de mainPlate
                mpFilename = mpFullPath
                mpVersion = mpFilename.split('_')[-2]
                mpVersion = mpVersion + '_' + mpFilename.split('_')[-1].split('.')[0]

                labelMP = "MP_"+mpVersion
                self.readMainPlate = nuke.nodes.Read(file=mpFilename, first=mpfirstFrame, last=mplastFrame, label=labelMP)

                if self.projID == "FTHM":
                    self.load_Reformat(n = self.readMainPlate)
                else:
                    self.feedback += "\nReformat non specified"
                    self.msgBox += "\nReformat not specified"
                
            else:
                self.feedback += "\nMissing main plate:\n\t{0}".format(mpPath)
                self.msgBox += "Missing main plate"
        
    def getContext(self, project_id = None):
        self.projID = project_id
        
    def setShotNameUser(self, name):
        self.shot = name
        myShotNumber = name.split("_")
        self.myShotSequence = myShotNumber[0]
        self.myShotNumber = myShotNumber[1]

    def setStep(self,step):
        self.step =step

    def getLastVersion(self, versions):
        last = None
        if len(versions) >0:
            last = versions[0]

        for v in versions:
            date_long = str(v.get('created_at'))
            date_short = date_long.split(' ')[0]
            date_elements = date_short.split('-')
            year = int(date_elements[0])
            month = int(date_elements[1])
            day = int(date_elements[2])

            hour_long = date_long.split(' ')[1]
            hour_elements = hour_long.split('-')[0].split(':')
            hour = int(hour_elements[0])
            minutes = int(hour_elements[1])
            seconds = int(hour_elements[2])

            l_date_long = str(last.get('created_at'))
            l_date_short = l_date_long.split(' ')[0]
            l_date_elements = l_date_short.split('-')
            l_year = int(l_date_elements[0])
            l_month = int(l_date_elements[1])
            l_day = int(l_date_elements[2])

            l_hour_long = l_date_long.split(' ')[1]
            l_hour_elements = l_hour_long.split('-')[0].split(':')
            l_hour = int(l_hour_elements[0])
            l_minutes = int(l_hour_elements[1])
            l_seconds = int(l_hour_elements[2])

            # Start comparisons
            if last != v:
                if date_short != l_date_short:
                    if year > l_year:
                        last = v
                    elif year == l_year and month > l_month:
                        last = v
                    elif year == l_year and month == l_month and day > l_day:
                        last = v
                else:
                    if hour > l_hour:
                        last = v
                    elif hour == l_hour and minutes > l_minutes:
                        last = v
                    elif hour == l_hour and minutes == l_minutes and seconds > l_seconds:
                        last = v

        return last

    def load_reference(self):
        #Cargar ultima referencia
        
        possibles_v = [v for v in self.AllShotVersions if (
            ('_edt_' in v.get('code')) and ('_ref_' in v.get('code')))]        

        last_ref = self.getLastVersion(possibles_v)

        if not last_ref:
            self.feedback += "\nNo reference published.\n\tCheck with Editorial"
            self.msgBox += "\nNo reference published"
        else:
            refFullPath = last_ref.get('sg_path_to_frames')
            
            refFile = refFullPath.split('/')[-1]
            refPath = refFullPath.replace(refFile, '')        
            if os.path.exists(refPath):
                try:
                    refFrames = os.listdir(refPath)
                    refframeNumber = []
                    for i in refFrames:
                        temp = i.split(".")
                        if len(temp) == 3:
                            temp = temp[1]
                            #print temp
                            if temp.isdigit():
                                refframeNumber.append(temp)
                    #print frameNumber
                    reffirstFrame = min(refframeNumber)
                    reflastFrame = max(refframeNumber)

                    refVersion = refFile.split('_')[-2]
                    refVersion += '_' + refFile.split('_')[-1].split('.')[0]
                    labelRef = "REF_" + refVersion
                    refFilename = refFullPath

                    readReference = nuke.nodes.Read(file=refFilename, first=reffirstFrame, last=reflastFrame, label=labelRef)
                except:
                    self.feedback += "\nProblem with the reference:\n{0}".format(refPath)
                    self.msgBox += "\n Reference problem"
            else:
                self.feedback += "\nReference does not exists: \n\t{0}".format(refPath)
                self.msgBox += "\nReference does not exists"

    def load_LUT(self, readNode, projCode, sg, engine):

        print "New LUT METHOD"        
        #config_path = parent.context.tank.pipeline_configuration.get_path()
        projectName = engine.context.project.get('name').split(":")[0]
        config_path = engine.context.tank.pipeline_configuration.get_path()
        lut = os.path.join(config_path, "config", "resources", "LUT_GROUP.nk")

        lut_node = nuke.toNode("LUT_{0}".format(engine.context.project.get('name')))

        if not lut_node:
            lut_node = nuke.nodePaste(lut)

        if lut_node:
            cdl_template = engine.get_template_by_name("cdl_shot")
            project = sg.find_one('Project', [['code', 'is', projCode]])
            
            shot_sg = sg.find_one('Shot', [['project', 'is', project],['code', 'is', self.shot]], ['code', 'sg_lut','sg_sequence.Sequence.sg_lut'])
            SHOT = shot_sg['code']
            SEQ = shot_sg['code'].split('_')[0]

            no_lut = False
            if shot_sg["sg_lut"]:
                time= 'noche'
                if 'nl' in self.SHOT['sg_lut']:
                    no_lut = True

            elif shot_sg["sg_sequence.Sequence.sg_lut"]:
                time= 'noche'
                if 'nl' in self.SHOT['sg_lut']:
                    no_lut = True
            else:
                time= 'dia'

            fields = {
                      'project': projCode,
                      'Shot': SHOT,
                      'Sequence': SEQ,
                      'tiempo': time,
                     }
            lut_path = cdl_template.apply_fields(fields)


            if os.path.exists(lut_path):     
                ocioNode = lut_node.node('OCIOFileTransform4')   
                ocioNode.showControlPanel()
                ocioNode["file"].setValue(lut_path)
                ocioNode["reload"].execute()
                ocioNode.hideControlPanel()
                ocioNode['disable'].setValue(no_lut)   

                #lut_node.node('Vectorfield1')["vfield_file"].setValue(lut_path)    
                #lut_node.setInput (0,readNode)  

                #Connect Lut with Main Plate and Ref node
                allNodes = nuke.allNodes(recurseGroups= False)
                number=1
                num_list= []
                for n in allNodes:
                    if n.Class() == 'Group':
                        name=n.knob('name').getValue()
                        if "{0}_".format(projectName) in name:
                            number=name.split("_")[-1]
                            num_list.append(int(number))
                if num_list:
                    number= max(num_list, key=int)
                    number+= 1

                import copy
                lut_copy=copy.copy(lut_node)
                lut_copy=nuke.toNode( "Group1")
                lut_copy.knob('name').setValue('LUT_{0}_{1}'.format(projectName, number))

                if self.readPublicado:
                    lut_node.setInput(0,self.readPublicado)

                if self.readMainPlate:
                    #lut_copy.setInput(0,self.readReference)
                    lut_copy.setInput(0, self.readMainPlate)         

            else:
                nuke.delete(lut_node)
                self.feedback += "\n Lut does not exists "
                self.msgBox += "\n Lut does not exists "

    def load_version0(self, shot_id):
        """
        Load read from SG published version
        """
        from os import listdir
        from os.path import isfile, join

        engine = tank.platform.current_engine()
        sg = engine.shotgun
        projID = engine.context.project.get('id')
        
        

        sg_filter =  [
            ['project', 'is',  {'type': 'Project', 'id': projID} ],
            {
                'filter_operator': 'all',
                'filters' : [
                    ['entity', 'is', {'type': 'Shot','id':shot_id}],
                    ['name', 'contains', 'version0'],
                    # Filter to return only publish
                    #['published_file_type', 'is', {'type':'PublishedFileType', 'id':48} ]
                ]
            }
        ]

        fields_required = ['name', 'published_file_type', 'path_cache']
        ver_ss = sg.find('PublishedFile', sg_filter, fields_required)

        ver_jpg = None
        ver_mov = None

        for ver in ver_ss:
            tmpV = self.AllShotVersions[0].get('sg_path_to_frames')

            osPath  = tmpV.split('Project')[0] + 'Project/'    

            publishType = ver.get('published_file_type').get('id')
            folder_path = ver.get('path_cache')
            fullPath = osPath + folder_path

            fileName = folder_path.split('/')[-1]
            folder_path = folder_path.replace(fileName, '')
            path = osPath + folder_path


            onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
            data = dict([ ('files', onlyfiles), ('fullpath', fullPath)])
            if publishType == 48:
                # JPG
                ver_jpg = data
            else:
                # MOV
                ver_mov = data

        
        readN = None
        validJPG = True
        # Processing JPG Version
        if ver_jpg:
            #len(ver_jpg['files'])
            framesNum = []
            for i in ver_jpg['files']:
                temp = i.split(".")
                if len(temp) == 3:
                    temp = temp[1]
                    if temp.isdigit():
                        framesNum.append(temp)

            if len(framesNum):
                ff = min(framesNum)
                lf = max(framesNum)
                readN = nuke.nodes.Read(file=ver_jpg['fullpath'], first = ff, last = lf, label = 'Version 0')
            else:
                validJPG = False

        if ver_mov and not validJPG:
                # len(ver_mov['files'])
                # Processing MOV files
                fm = nuke.root().firstFrame()
                lm = nuke.root().lastFrame()
                dif = lm - fm -1
                readN = nuke.nodes.Read(file=ver_mov['fullpath'], frame_mode = 1, frame = fm, last = dif, label = 'Version 0')
                readN.knob("reload").execute()

        if readN == None:
            self.feedback += "\nNo version0 found"
            self.msgBox += "\nNo version0 found"

    def load_Reformat(self, n):
        #nodes = nuke.nodePaste(RefPath)
        node = nuke.nodes.Reformat(
            format = "2156 1806 0 0 2156 1806 2 exr",
            resize = "height",
            black_outside = True,
            name = "Review_Reformat"
            )
        node.setInput (0,n)

    def get_status_personal(self, userID):
        """
        This method return an array with status
        Status = [short Name, long name]

        Only copies to the person that launch the engine
        """
        engine = tank.platform.current_engine()
        sg = engine.shotgun

        projID = engine.context.project.get('id')

        sg_filter =  [
            ['project', 'is', {'type': 'Project', 'id': projID}],
            {'filter_operator': 'any',
                'filters':
                [
                    ['sg_task.Task.addressings_cc.HumanUser.id', 'is', userID],
                    ['sg_task.Task.addressings_cc.Group.id', 'is', 122] #CompLeads
                ]
            }
        ]
        sSt = "sg_status_list"
        ver_ss = sg.find("Version", sg_filter, [sSt])
        lVs = []
        info_ver = []
        for x in ver_ss:
            if not x.get(sSt) in lVs:
                code_ = x.get(sSt)
                lVs.append(code_)
                name = self.getStatusName(code = code_)
                info_ver.append((name, code_))
        return info_ver

    def get_status_general(self):
        """
        This method return an array with status
        Status = [short Name, long name]

        Only copies to the person that launch the engine
        """
        engine = tank.platform.current_engine()
        sg = engine.shotgun

        projID = engine.context.project.get('id')

        sg_filter =  [
            ['project', 'is', {'type': 'Project', 'id': projID}]
        ]
        sSt = "sg_status_list"
        ver_ss = sg.find("Version", sg_filter, [sSt])
        lVs = []
        info_ver = []
        for x in ver_ss:
            if not x.get(sSt) in lVs:
                code_ = x.get(sSt)
                lVs.append(code_)
                name = self.getStatusName(code = code_)
                info_ver.append((name, code_))
        return info_ver

    def getStatusName(self, code):
        engine = tank.platform.current_engine()
        sg = engine.shotgun

        name = ""
        if self.status_lib is not []:
            for el in self.status_lib:
                if el[0] == code:
                    name = el[1]
                    break
            else:
                name = sg.find_one("Status", [['code', 'is', code] ] ,["name"])
                name = name.get("name")
                self.status_lib.append((code, name))    

        else:
            name = sg.find_one("Status", [['code', 'is', code] ] ,["name"])
            name = name.get("name")
            self.status_lib.append((code, name))
        return name

    def getSteps(self):
        import sgtk
        engine = sgtk.platform.current_engine()
        sg = engine.shotgun
        step_ls= []
        step_names= []

        projID = engine.context.project.get('id')
        sg_filter =  [
            ['project', 'is', {'type': 'Project', 'id': projID}]]
        Fields= "sg_task.Task.step"
        ver_ss = sg.find("Version", sg_filter, [Fields])


        for e in ver_ss:
            step=e["sg_task.Task.step"]
            if step:
                step_name=step["name"]
                if step_name not in step_names:
                    step_names.append(step_name)
                    step_ls.append(step)
        step_dict= { 
                    "names": step_names,
                    "steps": step_ls
        }

        return step_dict
            
    def getMainPlateExtension(self, folder):
        from os import listdir
        from os.path import isfile, join
        
        folders = [f for f in listdir(folder) if not isfile(join(folder, f))]
        NonExtensions = ["JPG", "HALF", "FULL"]
        valdExt = []
        for f in folders:
            valid = True
            for ex in NonExtensions:
                if f.find(ex) is not -1:
                    valid = False
            if valid:
                valdExt.append(f)

        return valdExt

    def getResolution(self, folder):
        """
        """
        from os import listdir
        from os.path import isfile, join
        
        folders = [f for f in listdir(folder) if not isfile(join(folder, f))]
        NonExtensions = []
        valdExt = []
        for f in folders:
            valid = True
            for ex in NonExtensions:
                if f.find(ex) is not -1:
                    valid = False
            if valid:
                valdExt.append(f)

        return valdExt[0]





