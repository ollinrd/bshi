#!/usr/bin/env pythonS
# -⁻- coding: UTF-8 -*-
import codecs
import sys
from sys import platform
import os
import unicodedata
from sgtk.platform.qt import QtCore, QtGui
import tank
import sgtk 
import nuke

# import the shotgun_fields module from the framework
shotgun_fields = sgtk.platform.import_framework(
    "tk-framework-qtwidgets", "shotgun_fields")

# import the shotgun_globals module from shotgunutils framework
shotgun_globals = sgtk.platform.import_framework(
    "tk-framework-shotgunutils", "shotgun_globals")

# import the shotgun_model module from shotgunutils framework
shotgun_model = sgtk.platform.import_framework(
    "tk-framework-shotgunutils", "shotgun_model")

# import the views module from qtwidgets framework
views = sgtk.platform.import_framework(
    "tk-framework-qtwidgets", "views")

class FormMain():
    def __init__(self, loc, parent = None):
        
        uiFileName = os.path.join(loc, "resources", "empty_widget.ui")
        uiDetailsFile = os.path.join(loc, "resources", "details_window.ui")
        uiFilters = os.path.join(loc, "resources", "filters_widget.ui")

        cV= nuke.NUKE_VERSION_STRING
        nukeV = cV.split("v")[0]
        if float(nukeV)<11:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        # Main window
        uifile = QtCore.QFile(uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)
        self.ui = self.loader.load(uifile, parent)
        uifile.close()

        # Details window
        detailsFile = QtCore.QFile(uiDetailsFile)
        detailsFile.open(QtCore.QFile.ReadOnly)
        self.uiDetails = self.loader.load(detailsFile, parent)
        detailsFile.close()

        # Filters window
        uiFilterW = QtCore.QFile(uiFilters)
        uiFilterW.open(QtCore.QFile.ReadOnly)
        self.ui_filters = self.loader.load(uiFilterW, parent)
        uiFilterW.close()

        # Filter buttons
        self.ui_filters.btnBox_01.accepted.connect(self._filter_OK_Validation)
        self.ui_filters.btnBox_01.rejected.connect(self._rejectFilters)

        self.uiDetails.btnBox.accepted.connect(self.CloseDetails)

        self.ui.btn_accept.clicked.connect(self.Load_nodes)
        self.ui.btn_filters.clicked.connect(self._loadingCheckBoxes_savingDefaults)
        self.ui.btn_refresh.clicked.connect(self._refresh)
        #[Filters]
        self.filter_user_list = []
        self.filter_general_list = []
        self.filter_steps_list =[]


        # 
        self.ModelInfo = None

    def CloseDetails(self):
        self.ui.show()

    def set_Operator(self, CoreFunc):
    	"""
    	Getting external object
    	Allow connect core operations to UI
    	"""
    	self.ShotReview = CoreFunc

    def _defaultDetailsWindow(self, status):
        tmp_dtl = self.uiDetails
        status_= [
            (tmp_dtl.lbl_mainPlate, tmp_dtl.lbl_b_mainPlate), 
            (tmp_dtl.lbl_publish, tmp_dtl.lbl_b_publish),
            (tmp_dtl.lbl_reference, tmp_dtl.lbl_b_reference), 
            (tmp_dtl.lbl_lut, tmp_dtl.lbl_b_lut),
            (tmp_dtl.lbl_version0, tmp_dtl.lbl_b_version0),
            (tmp_dtl.lbl_reformat, tmp_dtl.lbl_b_reformat)
            ]
        
        for sts in status_:
            sts[0].setText(status)
            sts[1].setStyleSheet('color: rgb(100, 100, 100);')

    def Load_nodes(self):
        name = ""
        name = str(self.ui.lbl_feedback.text())

        self.ShotReview.getContext(project_id = self.projCode)
        #self.ShotReview.getContext(project_id = "FTHM")

        cond1 = name.find("<Shot>") is -1
        cond2 = name.find("Warning!") is -1
        cond3 = name.find("Success!") is -1
        if  cond1 and cond2  and cond3:
            #self.ui.lbl_feedback.setText("Warning! check script editor!")
            self.ShotReview.setShotNameUser(name)
            self.ShotReview.setStep(self.clickedStep)
            cam = self.ui_filters.chkBox_cams.isChecked()
            engine = tank.platform.current_engine()
            sg = engine.shotgun
            msgBox = self.ShotReview.loadScene(lbl_feedback = self.ui.lbl_feedback, mainWindow = self.ui, cam = cam, sg = sg, engine=engine)

            if msgBox is not '':
                self.hide()
                #print nuke.message(msgBox)

                self._defaultDetailsWindow(status = 'Pending')

                # Evaluate first element in imessage string to evaluate if the script needs something
                # to compelte a task, according to that, the second element is set as status in details 
                # window, the thirth and fourth elements are references to detail ui elements to modify
                tmpD = self.uiDetails
                evals = [
                    ["Missing publish",'Missing', tmpD.lbl_b_publish , self.uiDetails.lbl_publish],
                    ["No publish", 'No published', tmpD.lbl_b_publish , self.uiDetails.lbl_publish],

                    ["Missing main plate",'Missing', self.uiDetails.lbl_b_mainPlate, self.uiDetails.lbl_mainPlate],
                    ["No main plate",'No mainplate', self.uiDetails.lbl_b_mainPlate, self.uiDetails.lbl_mainPlate],

                    ["Reference problem",'Problem', self.uiDetails.lbl_b_reference , self.uiDetails.lbl_reference],
                    ["Reference does not exists",'Missing', self.uiDetails.lbl_b_reference , self.uiDetails.lbl_reference],
                    ["No reference",'No reference', self.uiDetails.lbl_b_reference , self.uiDetails.lbl_reference],

                    ["Lut does not exists", 'Undefined', self.uiDetails.lbl_b_lut, self.uiDetails.lbl_lut],
                    ["Lut does not load", 'Unused', self.uiDetails.lbl_b_lut, self.uiDetails.lbl_lut],

                    ["Reformat not loaded", 'Problem', self.uiDetails.lbl_b_reformat, self.uiDetails.lbl_reformat],
                    ["Reformat not specified", 'Undefined', self.uiDetails.lbl_b_reformat, self.uiDetails.lbl_reformat],

                    ["No version0 found", 'Missing', self.uiDetails.lbl_b_version0, self.uiDetails.lbl_version0]
                ]

                for rel in evals:
                    val1 = '42, 255, 0' in rel[2].styleSheet()
                    val2 = '100, 100, 100' in rel[2].styleSheet()

                    if val1 or val2:
                        if rel[0] in msgBox:
                            grays = ['Undefined', 'Unused', 'No']
                            mainPlate_Color = 'color: rgb(255, 42, 0);'
                            mainPlate_status = rel[1]
                            for g in grays:
                                if g in rel[1]:
                                    mainPlate_Color = 'color: rgb(90, 90, 90);'
                        else:
                            mainPlate_status = "Loaded"
                            mainPlate_Color = 'color: rgb(42, 255, 0);'

                        rel[2].setStyleSheet(mainPlate_Color)
                        rel[3].setText(mainPlate_status)

                self.uiDetails.setWindowFlags(
                self.uiDetails.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)

                # disable main windows stays in front of all
                self.ui.setWindowFlags(
                self.ui.windowFlags() & ~QtCore.Qt.WindowStaysOnTopHint)                

                self.uiDetails.show()
            #self.ui.lbl_feedback.setText("Success!")
        else:
            if not cond3:
                self.ui.lbl_feedback.setText("Warning! Shot already loaded")
            else:
                if not cond1:
                    self.ui.lbl_feedback.setText("Warning! No valid shot specified")
                else:
                    self.ui.lbl_feedback.setText("Warning! Check script editor or select a new version")

    def load_data(self):
        # ----------------------
        engine = tank.platform.current_engine()
        sg = engine.shotgun

        projName = engine.context.project.get('name')
        projID = engine.context.project.get('id')
        self.userID = engine.context.user.get('id')

        if engine.context.user.get('name') == 'Fidel Moreno':
            self.userID = 100
            # For test
        self.userName = engine.context.user.get('name')

        # Fathom id for test
        self.projID = projID
        self.projCode = sg.find_one("Project", [["id", "is", projID]], ["code"]).get("code")
        
        filters = [
            ['project', 'is', {'type': 'Project', 'id': projID}],
            ]
        fields = ["code", "sg_status_list"]
        allShots = sg.find("Shot", filters, fields)

        self.lista_shots = []
        for s in allShots:
            self.lista_shots.append(s.get("code"))

        completer = QtGui.QCompleter()
        self.ui.QLine_Shot.setCompleter(completer)

        model = QtGui.QStringListModel()
        completer.setModel(model)

        model.setStringList(self.lista_shots)

        self.populadeUI()

        self._signals()

    # Display user window
    def show(self):

        for x in range(0,100):
            try:
                self._auto_delegate_table.setRowHeight(x,70)
            except:
                raise

        # Adjusting the columns size
        self._auto_delegate_table.setColumnWidth(1,260)
        self._auto_delegate_table.setColumnWidth(2,120)
        self._auto_delegate_table.setColumnWidth(3,75)

        self.ui.setWindowFlags(
        self.ui.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        self.ui.show()

    def hide(self):
        self.ui.hide()

    def populadeUI(self):
        """
        This widget populate the ui from shots that
        are only available to review.
        """

        self.QtCoreObj = QtCore.QObject()
        self._fields_manager = shotgun_fields.ShotgunFieldManager(self.QtCoreObj)
        self._fields_manager.initialized.connect(self._populate_ui)
        self._fields_manager.initialize()

    def _populate_ui(self):
        """
        The fields manager has been initialized. Now we can requests some
        widgets to use in the UI.
        :return:
        """
        entity_type = "Version"

        # get a list of fields for the entity type
        #fields = shotgun_globals.get_entity_fields(entity_type)
        fields = [ "code", "image", "frame_range", "sg_status_list","entity"]

        # make sure the fields list only includes editable fields
        fields = [f for f in fields
                  if shotgun_globals.field_is_editable(entity_type, f)]

        filters = [
            ['project', 'is', {'type': 'Project', 'id': self.projID}],
            ['entity', 'type_is', "Shot"],
            {
                'filter_operator': 'any',
                'filters':[
                    ['sg_status_list', 'is', "ldapv"],
                    ['sg_status_list', 'is', "apr"],
                    # Peticion de Yabin, que se agregara un filtro mas
                    ['sg_status_list', 'is', "fedapp"]
                ]
            }
            ]


        fields = self._fields_manager.supported_fields(entity_type, fields)
        columns = fields

        text_aux = "\t* If the version does not appear, modify the status of version "
        text_aux = text_aux + "or write the shot name. *"
        auto_delegate_lbl = QtGui.QLabel(text_aux)

        self.table = QtGui.QTableView()
        # create the table view
        self._auto_delegate_table = views.ShotgunTableView(self._fields_manager, parent=self.table)
        self._auto_delegate_table.horizontalHeader().setStretchLastSection(True)

        # setup the model
        self._sg_model = shotgun_model.SimpleShotgunModel(self.ui)

        # Load filters from Json file
        filters = self._LoadConfigFile(UI = True)

        self.ModelInfo = [
            ("entity_type", entity_type),
            ("fields", fields ),
            ("columns", columns),
            ("filters", filters)
            ]
        self._sg_model.load_data(entity_type, fields=fields, columns=columns,
                                 editable_columns=None, filters = filters)
        
        # Aqui sucede la magia
        self._auto_delegate_table.setModel(self._sg_model)

        for x in range(0,1000):
            try:
                self._auto_delegate_table.setRowHeight(x,70)
            except:
                raise

        # the sg model's first column always includes the entity code and
        # thumbnail. hide that column
        self._auto_delegate_table.hideColumn(0)

        layout = self.ui.gridLayout_2
        layout.addWidget(auto_delegate_lbl)
        layout.addWidget(self.ui.frm_inputText)
        layout.addWidget(self._auto_delegate_table)
        layout.addWidget(self.ui.lbl_feedback)
        layout.addWidget(self.ui.frm_buttons)
        self.ui.setLayout(layout)

        self.ui.chk_personal.setText("CC to " + self.userName)

    def _filter_OK_Validation(self):

        # current_chkboxes VS self.original_user_chkboxes
        # If something change, then update the main window
        if self.ui.chk_personal.isChecked():
            # Verify if the filters window has a change
            # if it has, i will update the main windows
            # according the filters
            current_user_chkboxes = []
            for i in reversed(range(self.ui_filters.verticalLayout_2.count())): 
                chkBox = self.ui_filters.verticalLayout_2.itemAt(i).widget()
                current_user_chkboxes.append([chkBox.text(), chkBox.isChecked()])

            if current_user_chkboxes != self.original_user_chkboxes:
                self._updateMainWindow()
        else:
            # Global settings
            # Verify if the filters window has a change
            # if it has, i will update the main windows
            # according the filters
            current_general_chkboxes = []
            for i in reversed(range(self.ui_filters.verticalLayout_4.count())): 
                chkBox = self.ui_filters.verticalLayout_4.itemAt(i).widget()
                current_general_chkboxes.append([chkBox.text(), chkBox.isChecked()])

            if current_general_chkboxes != self.original_general_chkboxes:
                self._updateMainWindow()

        current_step_chkboxes= []
        for i in reversed(range(self.ui_filters.verticalLayout_8.count())):
            chkBox = self.ui_filters.verticalLayout_8.itemAt(i).widget()
            current_step_chkboxes.append([chkBox.text(), chkBox.isChecked()])

        if current_step_chkboxes != self.original_steps_chkboxes:
            self._updateMainWindow()


        # Enable main window stays in top
        self.ui.setWindowFlags(
            self.ui.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        
    def _updateMainWindow(self):
        # Update the main window info
        # According with filters and option of 
        # CC to current user

        #Save config file
        self._SaveConfigFile()

        entity_type = "Version"
        fields = [ "code", "image", "frame_range", "sg_status_list","entity"]

        status = ["ldapv", "apr", "fedapp"]
        per_filter = ['entity', 'type_is', "Shot"]

        # Verifing checkBoxes
        # Personal settings
        if self.ui.chk_personal.isChecked():
            status = []
            for i in reversed(range(self.ui_filters.verticalLayout_2.count())): 
                chkBox = self.ui_filters.verticalLayout_2.itemAt(i).widget()
                if chkBox.isChecked():
                    for fil in self.filter_user_list:
                        if fil[0] == chkBox.text():
                            status.append(fil[1])


            if status == []:
                status = ["ldapv", "apr", "fedapp"]


            # Filtro personal
            per_filter = {   'filter_operator': 'any',
                'filters':
                [
                    ['sg_task.Task.addressings_cc.HumanUser.id', 'is', self.userID],
                    ['sg_task.Task.addressings_cc.Group.id', 'is', 122] #CompLeads
                ]
            }
        else:
            # General settings
            status = []
            for i in reversed(range(self.ui_filters.verticalLayout_4.count())): 
                chkBox = self.ui_filters.verticalLayout_4.itemAt(i).widget()
                if chkBox.isChecked():
                    for fil in self.filter_general_list:
                        print fil, " --> ", chkBox.text()
                        if fil[0] == chkBox.text():
                            status.append(fil[1])

            if self.ui.chk_lead.isChecked():
                #Validar filtro por lead
                leadGroup= self.leadGroups()
                if leadGroup:
                    leadFilter=['user', 'in', leadGroup]
                    per_filter= leadFilter

            if status == []:
                status = ["ldapv", "apr", "fedapp"]

        #Steps
        steps= []
        all_steps= self.ShotReview.getSteps()["steps"]
        steps_names=[]
        for i in reversed(range(self.ui_filters.verticalLayout_8.count())):
            chkBox= self.ui_filters.verticalLayout_8.itemAt(i).widget()
            if chkBox.isChecked():
                for fil in self.filter_steps_list:
                    if fil == chkBox.text():
                        steps_names.append(fil)

        for s in all_steps:
            for sn in steps_names:
                if sn == s["name"]:
                    steps.append(s)


        # Status filters
        fil_status = [['sg_status_list', 'is', x] for x in status]

        if steps:
            fil_steps = [['sg_task.Task.step', 'is', x] for x in steps]
        else:
            fil_steps = []

        # Todos los filtros
        filters = [
            ['project', 'is', {'type': 'Project', 'id': self.projID}],
            per_filter,
            {
                'filter_operator': 'any',
                'filters':fil_status
            },
            {
                'filter_operator': 'any',
                'filters':fil_steps
            }
        ]

        fields = [ "code", "image", "frame_range", "sg_status_list","entity"]

        # make sure the fields list only includes editable fields
        fields = [f for f in fields
                  if shotgun_globals.field_is_editable(entity_type, f)]

        fields = self._fields_manager.supported_fields(entity_type, fields)
        columns = fields
        self.ModelInfo = [
            ("entity_type", entity_type),
            ("fields", fields ),
            ("columns", columns),
            ("filters", filters)
            ]
        self._sg_model.load_data(entity_type, fields=fields, columns=columns,
                                 editable_columns=None, filters = filters)


        self._auto_delegate_table = views.ShotgunTableView(self._fields_manager, parent=self.table)
        self._auto_delegate_table.horizontalHeader().setStretchLastSection(True)

        self._auto_delegate_table.setModel(self._sg_model)

        for x in range(0,1000):
            try:
                self._auto_delegate_table.setRowHeight(x,70)
            except:
                raise

    def _loadingCheckBoxes_savingDefaults(self):
        # Personal filters
        
        self.original_user_chkboxes = []
        if self.filter_user_list == []:
            lStatus = self.ShotReview.get_status_personal(userID = self.userID)
            if lStatus != self.filter_user_list:
                self.filter_user_list = lStatus
                for i in reversed(range(self.ui_filters.verticalLayout_2.count())): 
                    widgetToRemove = self.ui_filters.verticalLayout_2.itemAt(i).widget()
                    self.ui_filters.verticalLayout_2.removeWidget(widgetToRemove)
                    widgetToRemove.setParent( None )

                for lS in lStatus:
                    tmp_chkBox = QtGui.QCheckBox()
                    tmp_chkBox.setText(lS[0])
                    if lS[1] in ["ldapv", "apr", "fedapp"]:
                        tmp_chkBox.setChecked(True)
                    self.ui_filters.verticalLayout_2.addWidget(tmp_chkBox)

        # General filters
        self.original_general_chkboxes = []
        if self.filter_general_list == []:
            self.l_general_status = self.ShotReview.get_status_general()

            if self.l_general_status != self.filter_general_list:
                self.filter_general_list = self.l_general_status
                for i in reversed(range(self.ui_filters.verticalLayout_4.count())): 
                    widgetToRemove = self.ui_filters.verticalLayout_4.itemAt(i).widget()
                    self.ui_filters.verticalLayout_4.removeWidget(widgetToRemove)
                    widgetToRemove.setParent( None )

                for lS in self.l_general_status:
                    tmp_chkBox = QtGui.QCheckBox()
                    tmp_chkBox.setText(lS[0])
                    if lS[1] in ["ldapv", "apr", "fedapp"]:
                        tmp_chkBox.setChecked(True)
                    self.ui_filters.verticalLayout_4.addWidget(tmp_chkBox)
       
        #Steps filters

        self.original_steps_chkboxes = []
        if self.filter_steps_list == []:
            self.l_steps = self.ShotReview.getSteps()["names"]


            if self.l_steps != self.filter_steps_list:
                self.filter_steps_list = self.l_steps
                for i in reversed(range(self.ui_filters.verticalLayout_8.count())):
                    widgetToRemove= self.ui_filters.verticalLayout_8.itemAt(i).widget()
                    self.ui_filters.verticalLayout_8.removeWidget(widgetToRemove)
                    widgetToRemove.setParent(None)

                for ls in self.l_steps:
                    tmp_chkBox = QtGui.QCheckBox()
                    tmp_chkBox.setText(ls)
                    if ls in ["compositing", "paint"]:
                        tmp_chkBox.setChecked(True)
                    self.ui_filters.verticalLayout_8.addWidget(tmp_chkBox)


        # Update check-boxes if config file is available
        self._LoadConfigFile()

        # Saving original selection personal
        for i in reversed(range(self.ui_filters.verticalLayout_2.count())): 
            chkBox = self.ui_filters.verticalLayout_2.itemAt(i).widget()
            self.original_user_chkboxes.append([chkBox.text(), chkBox.isChecked()])
            
        # Saving original selection group
        for i in reversed(range(self.ui_filters.verticalLayout_4.count())): 
            chkBox = self.ui_filters.verticalLayout_4.itemAt(i).widget()
            self.original_general_chkboxes.append([chkBox.text(), chkBox.isChecked()])


        #Saving steps selection group
        for i in reversed(range(self.ui_filters.verticalLayout_8.count())):
            chkBox= self.ui_filters.verticalLayout_8.itemAt(i).widget()
            self.original_steps_chkboxes.append([chkBox.text(),chkBox.isChecked()])    
            
        self.ui_filters.show()

    def _rejectFilters(self):

        # Set original personal actives 
        lBoxes = []
        for i in reversed(range(self.ui_filters.verticalLayout_2.count())): 
            chbox = self.ui_filters.verticalLayout_2.itemAt(i).widget()
            lBoxes.append( (chbox.text(), chbox) )
            
        dictBoxes = dict(lBoxes)

        # Set original personal status
        for oAct in self.original_user_chkboxes:
            box = dictBoxes[oAct[0]]
            box.setChecked(oAct[1])

        # Set original general actives 
        l_general_boxes = []
        for i in reversed(range(self.ui_filters.verticalLayout_4.count())): 
            chbox = self.ui_filters.verticalLayout_4.itemAt(i).widget()
            l_general_boxes.append( (chbox.text(), chbox) )
            
        dict_general_boxes = dict(l_general_boxes)
        
        for oAct in self.original_general_chkboxes:
            box = dict_general_boxes[oAct[0]]
            box.setChecked(oAct[1])

        # Set original steps actives
        l_step_boxes= []

        for i in reversed(range(self.ui_filters.verticalLayout_8.count())): 
            chbox = self.ui_filters.verticalLayout_8.itemAt(i).widget()
            l_step_boxes.append( (chbox.text(), chbox) )
            
        dict_step_boxes = dict(l_step_boxes)

        for oAct in self.original_steps_chkboxes:
            box = dict_step_boxes[oAct[0]]
            box.setChecked(oAct[1])




        # Enable main window stays in top
        self.ui.setWindowFlags(
            self.ui.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)

    def _signals(self):
        """
        Connect al user signals
        """
        self._auto_delegate_table.clicked.connect(self._setFromSelection)
        self._auto_delegate_table.clicked.connect(self._getStepFromSelection)
        self.ui.chk_personal.clicked.connect(self._updateMainWindow)
        self.ui.chk_lead.clicked.connect(self._updateMainWindow)

    def _setFromSelection(self, item):
        cellContent = item.sibling(item.row(),5).data()
        self.ui.lbl_feedback.setText(cellContent)


    def _getStepFromSelection(self, item):
        import sgtk
        engine = sgtk.platform.current_engine()
        sg = engine.shotgun
        versionName= item.sibling(item.row(),1).data()
        filters=[["code", "is", versionName]]
        field=["sg_task.Task.step"]
        versionInfo = sg.find("Version",filters, field)
        step= versionInfo[0]["sg_task.Task.step"]
        self.clickedStep= step
        

    def _SaveConfigFile(self):
        #
        # Saving config into json file
        #
        import json
        statusDict = dict(self.l_general_status)


        # Creating data structure to save in json
        filterPersonal = self.ui.chk_personal.isChecked()
        filterLead= self.ui.chk_lead.isChecked()
        loadCam = self.ui_filters.chkBox_cams.isChecked()

        current_user_chkboxes = []
        for i in reversed(range(self.ui_filters.verticalLayout_2.count())): 
            chkBox = self.ui_filters.verticalLayout_2.itemAt(i).widget()
            if chkBox.isChecked():
                current_user_chkboxes.append(chkBox.text())

        current_general_chkboxes = []
        for i in reversed(range(self.ui_filters.verticalLayout_4.count())): 
            chkBox = self.ui_filters.verticalLayout_4.itemAt(i).widget()
            if chkBox.isChecked():
                current_general_chkboxes.append(chkBox.text())


        current_step_chkboxes = []
        for i in reversed(range(self.ui_filters.verticalLayout_8.count())):
            chkBox = self.ui_filters.verticalLayout_8.itemAt(i).widget()
            if chkBox.isChecked():
                current_step_chkboxes.append(chkBox.text())

        # Compacting all data
        data = {
            "is_personal": filterPersonal,
            "is_lead": filterLead,
            "load_cam": loadCam,
            "personal_f": current_user_chkboxes,
            "general_f": current_general_chkboxes,
            "steps_f":    current_step_chkboxes,
            "status_dict": statusDict
        }

        # Saving data
        raw_config = self._getConfigFilePath()

        with open(raw_config["file"], "w") as write_file:
            json.dump(data, write_file)

    def _LoadConfigFile(self, UI = None):
        #
        # Loading old config from json file
        #
        import json
        #Steps 
        steps= []
        steps_names = []

        raw_config = self._getConfigFilePath()

        status = ['ldapv', 'apr', 'fedpp']
        per_filter = ['entity', 'type_is', "Shot"]

        if not raw_config["is_new"]:
            data = None
            with open(raw_config["file"], "r") as read_file:
                data = json.load(read_file)

            # Creating data structure to save in json
            try:
                self.ui.chk_personal.setChecked(data["is_personal"])
                self.ui.chk_lead.setChecked(data["is_lead"])
                self.ui_filters.chkBox_cams.setChecked(data["load_cam"])
            except:
                pass

            status_dict = data["status_dict"]
            if UI is not None:

             
                for i in reversed(range(self.ui_filters.verticalLayout_8.count())):
                    chkBox=self.ui_filters.verticalLayout_8.itemAt(i).widget()
                    if chkBox.isChecked():
                        for fil in self.filter_steps_list:
                            if fil == chkBox.text():
                                steps_names.append(fil)

                stepDicts=self.ShotReview.getSteps()["steps"]
             
                for s in stepDicts:
                    for sn in steps_names:
                        if sn == s["name"]:
                            steps.append(s)


                if data["is_personal"]:
                    status = [status_dict[f] for f in data["personal_f"] ]
                    per_filter = {   'filter_operator': 'any',
                        'filters': [
                            ['sg_task.Task.addressings_cc.HumanUser.id', 'is', self.userID],
                            ['sg_task.Task.addressings_cc.Group.id', 'is', 122] #CompLeads
                        ]
                    }
                else:
                    # General settings
                    status = [status_dict[f] for f in data["general_f"] ]

            else:
                config_user_chkboxes = data["personal_f"]
                for i in reversed(range(self.ui_filters.verticalLayout_2.count())): 
                    chkBox = self.ui_filters.verticalLayout_2.itemAt(i).widget()
                    chkBox.setChecked(chkBox.text() in config_user_chkboxes )

                config_general_chkboxes = data["general_f"]
                for i in reversed(range(self.ui_filters.verticalLayout_4.count())): 
                    chkBox = self.ui_filters.verticalLayout_4.itemAt(i).widget()
                    chkBox.setChecked(chkBox.text() in config_general_chkboxes )

                try:
                    config_step_chkboxes = data["steps_f"] 
                    for i in reversed(range(self.ui_filters.verticalLayout_8.count())):
                        chkBox= self.ui_filters.verticalLayout_8.itemAt(i).widget()
                        chkBox.setChecked(chkBox.text() in config_step_chkboxes)
                except:
                    pass

                return None

        if status == []:
            status = ['ldapv', 'apr', 'fedpp']
        
        # Status filters
        fil_status = [['sg_status_list', 'is', x] for x in status]
        if steps:
            fil_steps  = [['sg_task.Task.step', 'is', x ] for x in steps]
        else:
            fil_steps= []

        # Todos los filtros
        filters = [
            ['project', 'is', {'type': 'Project', 'id': self.projID}],
            per_filter,
            {
                'filter_operator': 'any',
                'filters':fil_status
            },
            {
                'filter_operator': 'any',
                'filters':fil_steps
            }
        ]

        return filters

    def _getConfigFilePath(self):
        #
        # Get json config file or create path
        # to generate a new config
        from os.path import expanduser
        import os

        newFile = False
        home = expanduser("~")
        finalPath = "{0}/.config/OllinVFX_ShotReview".format(home)
        fileName = "ShotReview_config.json"

        if not os.access(finalPath, os.F_OK):
            os.makedirs(finalPath, 0777)
            newFile = True

        FilePath = "{0}/{1}".format(finalPath, fileName)

        if not os.access(FilePath, os.F_OK):
            newFile = True

        data = {
            "file": FilePath,
            "is_new": newFile
        }

        return data

    def _refresh(self):
        """
        """
        print "Refresh"
        ModelDict = dict(self.ModelInfo)
        entity_type = ModelDict["entity_type"]
        fields = ModelDict["fields"]
        columns = ModelDict["columns"]
        filters = ModelDict["filters"]

        self._sg_model.load_data(
            entity_type, fields=fields, columns=columns,
            editable_columns=None, filters = filters)


        self._auto_delegate_table = views.ShotgunTableView(self._fields_manager, parent=self.table)
        self._auto_delegate_table.horizontalHeader().setStretchLastSection(True)

        self._auto_delegate_table.setModel(self._sg_model)

        for x in range(0,1000):
            try:
                self._auto_delegate_table.setRowHeight(x,70)
            except:
                pass

    def leadGroups(self):
        import sgtk
        engine= sgtk.platform.current_engine()
        sg=engine.shotgun
        userId=engine.context.user['id']
        if  userId == 100 or userId == 587 :
            #Isaac Camacho
            grp_id= 232
            filters= [['id','is', grp_id]]
            group=sg.find_one('Group', filters, ['code', 'users'])
            g_users=group["users"]
            return g_users

        elif userId== 219:
            #Eli Reyes
            grp_id=231
            filters= [['id','is', grp_id]]
            group=sg.find_one('Group', filters, ['code', 'users'])
            g_users=group["users"]
            return g_users

        elif userId== 189:
            #Ernesto Cespedes
            grp_id=229
            filters= [['id','is', grp_id]]
            group=sg.find_one('Group', filters, ['code', 'users'])
            g_users=group["users"]
            return g_users

        elif userId== 102:
            #Erick Oviedo
            grp_id=230
            filters= [['id','is', grp_id]]
            group=sg.find_one('Group', filters, ['code', 'users'])
            g_users=group["users"]
            return g_users
        else:
            return None








