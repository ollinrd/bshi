import sgtk
import os
import tank
import socket
# Plana de import nuke
import nuke
import sys
from tank.platform import Application


class Shot_review(Application):
    def init_app(self):
        # get modules
        
        valid = True
        for arg in nuke.rawArgs: 
            if arg == "-t":
                valid = False
                break

        if valid:
            self.data_loaded = False
            # Force that the app loads only if the menu button is clicked
            icon = os.path.join(self.disk_location, "resources", "ShotReview_icon.png")

            self.engine.register_command("Shot review", self.mainWindow, {"type": "script", "icon": icon})

    def mainWindow(self):
        # Set the communication between UI and core functions
        if not self.data_loaded:
            mod_shotReview = self.import_module("tk_nuke_shotreview")
            mod_shotReviewBridge = self.import_module("tk_nuke_shotreview_Bridge")

            # Core script functions
            self.shot_review_functions = mod_shotReview.ShotReview()

            # Bridge an UI functions
            self.shot_review_bridge = mod_shotReviewBridge.FormMain(loc = self.disk_location)
            
            # Connecting bridge with core
            self.shot_review_bridge.set_Operator(
            	CoreFunc = self.shot_review_functions)

            # Loading SG data to UI
            self.shot_review_bridge.load_data()

            self.data_loaded = True

        self.shot_review_bridge.show()