import sgtk
import os
import tank
import socket
# Plana de import nuke
import nuke
import sys
from tank.platform import Application
import nuke
import os
import sys
import shutil
import re
import threading
import time
import datetime

class Scene_collect(Application):
    def init_app(self):
        # get modules
        
        valid = True
        for arg in nuke.rawArgs: 
            if arg == "-t":
                valid = False
                break

        if valid:
            self.data_loaded = False
            # Force that the app loads only if the menu button is clicked
            icon = os.path.join(self.disk_location, "resources", "Collect_icon.png")

            self.engine.register_command("Scene Collect", self.collectFiles_01, {"type": "script", "icon": icon})




    # Child Functions
    def collectPanel(self):
        f_fecha = str(datetime.datetime.now()).split(' ')[0].replace('-', '')
        scriptName = os.path.basename(nuke.Root().name())
        op = "/nfs/ollinvfx/Project/BSHI/editorial/publish/outgoing/Collects/{0}/{1}".format(f_fecha,scriptName.replace('.nk',''))
        colPanel = nuke.Panel("Collect Files 1.2")
        colPanel.addFilenameSearch("Output Path:", op)

        colPanel.addButton("Cancel")
        colPanel.addButton("OK")

        retVar = colPanel.show()
        pathVar = colPanel.value("Output Path:")

        return (retVar, pathVar)

    # Check files
    def checkForKnob(self, node, checkKnob ):
        try:
            node[checkKnob]
        except:
            return False
        else:
            return True

    # Parent Function
    def collectFiles_01(self):
        panelResult = self.collectPanel()

        #copy script to target directory
        script2Copy = nuke.root()['name'].value()
        scriptName = os.path.basename(nuke.Root().name())
        fileNames = []
        paddings = ['%01d', '%02d', '%03d', '%04d', '%05d', '%06d', '%07d', '%08d', '%d', '%1d']
        videoExtension = ['mov', 'avi', 'mpeg', 'mpg', 'mp4', 'R3D']
        cancelCollect = 0

        # hit OK
        if panelResult[0] == 1 and panelResult[1] != '':

            targetPath = panelResult[1]

            # Check to make sure a file path is not passed through
            if os.path.isfile(targetPath):
                targetPath = os.path.dirname(targetPath)

            # Make sure target path ends with a slash (for consistency)
            if not targetPath.endswith('/'):
                targetPath += '/'

            # Check if local directory already exists. Ask to create it if it doesn't
            if not os.path.exists(targetPath):
                if nuke.ask("Directory does not exist. Create now?"):
                    try:
                        os.makedirs(targetPath)
                    except:
                        raise Exception, "Something's not working!"
                        return False
                else:
                    nuke.message("Cannot proceed without valid target directory.")
                    return False
           
            # Get script name
            scriptName = os.path.basename(nuke.Root().name())
        
            footagePath = targetPath + 'footage/'
            if (os.path.exists(footagePath)):
                pass
            else:
                os.mkdir(footagePath)

            task = nuke.ProgressTask("Collect Files 1.2")
            count = 0

            allNds = nuke.allNodes(recurseGroups = True)
            cnt = 0
            for fileNode in allNds:
                try:
                    if fileNode.Class() == 'WriteTank':
                        nuke.delete(fileNode)
                except:
                    pass
                try:
                    if fileNode.Class() == 'Viewer':
                        nuke.delete(fileNode)
                except:
                    pass
                cnt += 1

            allNds = nuke.allNodes(recurseGroups = True)




            for fileNode in allNds:

                if task.isCancelled():
                    cancelCollect = 1
                    break
                count += 1
                task.setMessage("Collecting:  " + str(fileNode['name'].value()) )
                task.setProgress(count*100/len(allNds))


                # LUT caso
                if fileNode.Class() == 'Group' and 'LUT' in fileNode.name():
                    with fileNode:
                        for n in nuke.allNodes():
                            for k in n.knobs():
                                if 'file' in k and 'File' in n[k].Class():
                                    if n[k].value() != '':
                                        nodeName = n.fullName()
                                        fileNodePathNew = footagePath + nodeName
                                        fileNodePath = n[k].value()
                                        newFilenamePath = fileNodePathNew + '/' + fileNodePath.split('/')[-1] 

                                        if (os.path.exists(fileNodePath)):
                                                #shutil.copy2(fileNodePath, newFilenamePath)
                                                if not (os.path.exists(fileNodePathNew)):
                                                    os.mkdir(fileNodePathNew)
                                                fileNodePath = fileNodePath.replace(' ', '\\ ')
                                                newFilenamePath = newFilenamePath.replace(' ', '\\ ')
                                                os.system("ln -s {0} {1}".format(fileNodePath, newFilenamePath))
                                        else:
                                            print fileNodePath, "Lut does not exist"
                

                if self.checkForKnob(fileNode, 'file'):
                    if not self.checkForKnob(fileNode, 'Render'):
                        fileNodePath = fileNode['file'].value()
                        if (fileNodePath == ''):
                            continue
                        else:
                            readName = fileNode['name'].value() + '/'
                            readFilename = fileNodePath.split('/')[-1]

                            seq = False
                            for p in paddings:
                                if p in readFilename:
                                    seq = True
                            if 'editorial' in fileNode['file'].value() and 'mp0' in fileNode['file'].value():
                                for i in fileNode['file'].value().split('_'):
                                    if 'mp0' in i:
                                        fileNode['name'].setValue(i)
                                        fileNode['disable'].setValue(True)

                            elif self.checkForKnob(fileNode, 'first') or seq:
                                if (fileNodePath.lower().endswith(tuple(videoExtension))):
                                    newFilenamePath = footagePath + readName + readFilename 
                                    if (os.path.exists(newFilenamePath)):
                                        #print (newFilenamePath + '     DUPLICATED')
                                        'DUPLICATED'
                                    else:
                                        if (os.path.exists(fileNodePath)):
                                            #shutil.copy2(fileNodePath, newFilenamePath)
                                            if not (os.path.exists(footagePath + readName)):
                                                os.mkdir(footagePath + readName)
                                            fileNodePath = fileNodePath.replace(' ', '\\ ')
                                            newFilenamePath = newFilenamePath.replace(' ', '\\ ')
                                            os.system("ln -s {0} {1}".format(fileNodePath, newFilenamePath))
                                            #print (newFilenamePath + '     COPIED')                                       
                                        else:
                                            print (fileNodePath + '     MISSING VIDEO')        

                                else:
                                    # frame range
                                    if not 'first' in fileNode.knobs():
                                        print 'SPECIAL CASE'
                                        from os import listdir
                                        from os.path import isfile, join
                                        mypath = fileNodePath.replace(readFilename, '')
                                        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
                                        
                                        for f in onlyfiles:
                                            #readFilename = f
                                            newFilenamePath = footagePath + readName + f

                                            if (os.path.exists(newFilenamePath)):
                                                #print (newFilenamePath + '     DUPLICATED')
                                                'DUPLICATED'
                                            else:
                                                if not (os.path.exists(footagePath + readName)):
                                                    os.mkdir(footagePath + readName)

                                                #shutil.copy2(fileNodePath, newFilenamePath)
                                                #p = subprocess.Popen( os.system("ln -s {0} {1}".format(fileNodePath, newFilenamePath)) )
                                                if f.split('.')[0] == readFilename.split('.')[0]:
                                                    if not (os.path.exists(newFilenamePath)):
                                                        origin_path = mypath +'/' +f
                                                        origin_path = origin_path.replace(' ', '\\ ')
                                                        newFilenamePath = newFilenamePath.replace(' ', '\\ ')
                                                        os.system("ln -s {0} {1}".format(origin_path, newFilenamePath))
                                        
                                    else:
                                        frameFirst = fileNode['first'].value()
                                        frameLast = fileNode['last'].value()
                                        framesDur = frameLast - frameFirst
                                        
                                        if (frameFirst == frameLast):
                                            newFilenamePath = footagePath + readName + readFilename

                                            for pad in paddings:
                                                if pad in fileNodePath:
                                                    fileNodePath = fileNodePath.replace(pad, '%04d' %frameFirst)
                                                if pad in newFilenamePath:
                                                    newFilenamePath = newFilenamePath.replace(pad, '%04d' %frameFirst)

                                            if (os.path.exists(newFilenamePath)):
                                                #print (newFilenamePath + '     DUPLICATED')
                                                'DUPLICATED'
                                            else:
                                                if (os.path.exists(fileNodePath)):
                                                    if not (os.path.exists(footagePath + readName)):
                                                        os.mkdir(footagePath + readName)

                                                    #shutil.copy2(fileNodePath, newFilenamePath)

                                                    #p = subprocess.Popen( os.system("ln -s {0} {1}".format(fileNodePath, newFilenamePath)) )
                                                    fileNodePath = fileNodePath.replace(' ', '\\ ')
                                                    newFilenamePath = newFilenamePath.replace(' ', '\\ ')
                                                    os.system("ln -s {0} {1}".format(fileNodePath, newFilenamePath))
                                                    #print (newFilenamePath + '     COPIED')                             
                                                else:
                                                    print fileNodePath, '\tMISSING FILE'
                                                    

                                        else:
                                            
                                            newFilenamePath = footagePath + readName
                                            if (os.path.exists(newFilenamePath)):
                                                #print (newFilenamePath + '     DUPLICATED')
                                                'DUPLICATED'
                                            else:
                                                os.mkdir(newFilenamePath)
            
                                            # rename sequence
                                            for frame in range(framesDur + 1):
                                                for pad in paddings:
                    
                                                    # Copy sequence file
                                                    if (re.search(pad, fileNodePath.split("/")[-1])):
                                                        originalSeq = fileNodePath.replace(pad, str(pad % frameFirst))
                                                        frameSeq = fileNodePath.split("/")[-1].replace(pad, str(pad % frameFirst))
                                                        fileNames.append (frameSeq)
                                                        newSeq = newFilenamePath + frameSeq
                                                        frameFirst += 1
                                                        #task.setMessage("Collecting file:   " + frameSeq)
                
                                                        originalSeq_modif = originalSeq.replace(' ', '\\ ')
                                                        newSeq = newSeq.replace(' ', '\\ ')
                                                        if (os.path.exists(newSeq)):
                                                            #print (newSeq + '     DUPLICATED')
                                                            'DUPLICATED'
                                                        else:
                                                            if (os.path.exists(originalSeq)):
                                                                #shutil.copy(originalSeq, newSeq)
                                                                os.system("ln -s {0} {1}".format(originalSeq_modif, newSeq))
                                                                #print (newSeq + '     COPIED')                      
                                                            else:
                                                                print (originalSeq + '     MISSING SEQUENCE')
                                            #print ('\n')
                                    
                            # Copy single file
                            else:
                                newFilenamePath = footagePath + readName + '/' + readFilename
                                if (os.path.exists(newFilenamePath)):
                                    #print (newFilenamePath + '     DUPLICATED')
                                    'DUPLICATED'
                                else:
                                    if (os.path.exists(fileNodePath)):
                                        newFilenamePath = footagePath + readName
                                        if (os.path.exists(newFilenamePath)):
                                            #print (newFilenamePath + '     DUPLICATED')
                                            'DUPLICATED'
                                        else:
                                            os.mkdir(newFilenamePath)
                                        #shutil.copy2(fileNodePath, newFilenamePath)
                                        fileNodePath = fileNodePath.replace(' ', '\\ ')
                                        newFilenamePath = newFilenamePath.replace(' ', '\\ ')
                                        os.system("ln -s {0} {1}".format(fileNodePath, newFilenamePath))
                                        #print (newFilenamePath + '     COPIED')
                                    else:
                                        print (fileNodePath + '     MISSING' + fileNode.name())                                    
                else:
                    pass
            

            
            if (cancelCollect == 0):
                # Save script to archive path
                newScriptPath = targetPath + scriptName
                nuke.scriptSaveAs(newScriptPath)
                print 'Saving Script'
            
                with nuke.root():
                    ''
        
                #link files to new path
                for fileNode in nuke.allNodes():
                    # LUT caso
                    if fileNode.Class() == 'Group' and 'LUT' in fileNode.name():
                        for n in fileNode.nodes():
                            for k in n.knobs():
                                if 'file' in k and 'File' in n[k].Class():
                                    if n[k].value() != '':
                                        readFilename = n[k].value().split('/')[-1]
                                        nodeName = n.fullName() + '/'
                                        reloadPath = '[python {nuke.script_directory()}]/footage/' + nodeName +readFilename
                                        n[k].setValue(reloadPath)



                    if self.checkForKnob(fileNode, 'file'):
                        if not self.checkForKnob(fileNode, 'Render'):
                            fileNodePath = fileNode['file'].value()
                            if (fileNodePath == ''):
                                pass
                            else:
                                nodeName = fileNode['name'].value()+'/'
                                fileNodePath = fileNode['file'].value()
                                readFilename = fileNodePath.split("/")[-1]

                                if 'ReadGeo' in fileNode.Class():
                                    activeItems = fileNode['scene_view'].getSelectedItems()
                                    reloadPath = '[python {nuke.script_directory()}]/footage/' + nodeName +readFilename
                                    fileNode.readKnobs('file {0}'.format(reloadPath))
                                    sceneView = fileNode['scene_view']
                                    sceneView.setSelectedItems(activeItems)
                                else:
                                    reloadPath = '[python {nuke.script_directory()}]/footage/' + nodeName +readFilename
                                    fileNode['file'].setValue(reloadPath)
                                    print 'Updating: ', fileNode.name(), reloadPath
                        else:
                            pass
                    else:
                        pass
        
                nuke.scriptSave()
                del task        
                print ('COLLECT DONE!!')
                nuke.message('COLLECT DONE!!')
                os.system('chmod 777 -R {0}'.format(targetPath))
            else:
                del task
                print ('COLLECT CANCELLED')
                nuke.message('COLLECT CANCELLED')

        # If they just hit OK on the default ellipsis...
        elif panelResult[0] == 1 and panelResult[1] == '':
            nuke.message("Select a path")
            return False

        # hit CANCEL
        else:
            print ('COLLECT CANCELLED')
