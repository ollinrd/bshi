import maya.cmds as cmds
import codecs
import sys
import os
from  anim_attach_Core import muscle_ops
from  anim_attach_Core import fat_ops
from  anim_attach_Core import skin_ops
#from sgtk.platform.qt import QtCore, QtGui
from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import QtWidgets


class FormMain():

    def __init__(self, loc, context, sg, engine, parent=None):
        self.loc=loc
        self.uiFileName = loc + '/resources/AnimAttach_v002.ui'
        print(self.uiFileName)


        self.context = context
        self.engine = engine
        self.startFrame = 0
        self.staticFrame = 0
        self.placeKey = 0
        self.placeBody = 0
        self.fv = 0
        #Skin
        self.blendshapeMode=1

        #Init Classes
        self.core_process = muscle_ops()
        self.core_fat_process= fat_ops()
        self.core_skin= skin_ops()
        self.Skin_weights_path = ''
        

        self.test = 0
        self.relationship = {}
        self.fat_relationship= {}
        self._init_maya_ui(self.uiFileName, parent)
        self._config_UI_()
        self.show()

    def _init_maya_ui(self, uiFileName, parent=None):
        from pymel import versions
        if versions.current() < 201700:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        uifile = QtCore.QFile(self.uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)

        self.ui = self.loader.load(uifile, parent)
        uifile.close()


    def _config_UI_(self):
        #Name Space List
        cmds.namespace(setNamespace=':')
        nameSpaces = cmds.namespaceInfo(listOnlyNamespaces=True, recurse=True)

        for n in nameSpaces:
            if n == 'UI' or n == 'shared':
                nameSpaces.remove(n)

        ### [Muscles UI]###------------------------------------------------------------------------------------------------------------- 
        #Fill ComboBox
        self.ui.driverBox_01.addItems(nameSpaces)
        self.ui.drivenBox_02.addItems(nameSpaces)

        #Connect RelationshipButton
        self.ui.Relationship_Btn_01.clicked.connect(self._get_relationship)

        #connect Test button
        #self.ui.Test_Btn_01.clicked.connect(self._Test)
        self.ui.Test_Btn_01.clicked.connect(self._Test)

        #Connect Break Connections
        #self.ui.break_Btn_01.clicked.connect(self.breakCon)
        self.ui.break_Btn_01.clicked.connect(self.breakConSel)

        #Connect Accept
        self.ui.Accept_Btn_01.clicked.connect(self.close)

        ###[Fat UI]###--------------------------------------------------------------------------------------------------------------------
        self.ui.fat_drBox_driver.addItems(nameSpaces)
        self.ui.fat_drBox_driven.addItems(nameSpaces)

        #Connect 
        self.ui.fat_btn_connect.clicked.connect(self.connectFat_)

        #Break connections
        self.ui.fat_btn_break.clicked.connect(self.break_fatConnections)



        ###[Skin UI]###---------------------------------------------------------------------------------------------------------------------

        #Switch modes
        self.ui.skin_rb_blend.isChecked()
        self.ui.skin_rb_blend.toggled.connect(self.blendMode) 

        self.ui.skin_rb_ziva.toggled.connect(self.attachmentsMode)


        #Loading nodes
        self.ui.skin_btn_anim.clicked.connect(self.loadAnimNode)
        self.ui.skin_btn_fat.clicked.connect(self.loadFatNode)

        #blendshape mode

        self.ui.skin_btn_map.clicked.connect(self.fileBrowser)
        self.ui.Skin_LnEdit_map.setText("/nfs/ollinvfx/Project/PIGS/library/Character FX/Ziva/Skin_weights/peso_fat_correctivo.tif")


        #Connect
        self.ui.skin_btn_connect.clicked.connect(self.connectSkin_)

        #Break connection
        self.ui.skin_btn_break.clicked.connect(self.break_SkinConnections_)


    def close(self):
        #cerrar venatna
        self.ui.hide()

    def show(self):
        self.ui.show()


    ####----------[Muscle Operations]----------####

    def _get_relationship(self):
        #Get Combobox selected Text
        driverNs = str(self.ui.driverBox_01.currentText())
        drivenNs = str(self.ui.drivenBox_02.currentText())
        self.relationship = self.core_process.CreateRelationship(driverNs,
                                                                 drivenNs)

    def _get_keyframeSpacing(self):
        SF = self.ui.StartFrame_In_01.text()
        StaticGeo = self.ui.staticGeo_In_02.text()
        PlaceGeo = self.ui.PlaceGeo_In_03.text()
        placeBody = self.ui.lnE_placebody.text()
        return [SF, StaticGeo, PlaceGeo, placeBody]

    def _Test(self):
        RelList = self.relationship
        if RelList == {}:
            cmds.confirmDialog(m='Please create a Driver-Driven relationship',
                               b='ok')
            sys.stdout.write('No relationship exists in the scene ')

        else:
            keyFrames = self._get_keyframeSpacing()
            if self.test == 1:
                # disconnect
                drivenNs = self.ui.drivenBox_02.currentText()
                print(self.fv)
                self.core_process.breakConnections(drivenNs, self.fv)
                # Connect
                self.core_process.createConnections(
                    RelList, int(keyFrames[0]), int(keyFrames[1]),
                    int(keyFrames[2]), int(keyFrames[3]))
                self.test = 1

            else:
                self.core_process.createConnections(
                    RelList, int(keyFrames[0]), int(keyFrames[1]),
                    int(keyFrames[2]), int(keyFrames[3]))
                self.test = 1

            self.startFrame = self.ui.StartFrame_In_01.text()
            self.staticFrame = self.ui.staticGeo_In_02.text()
            self.placeKey = self.ui.PlaceGeo_In_03.text()
            self.placeBody = self.ui.lnE_placebody.text()
            self.fv = int(self.startFrame)-int(self.staticFrame)-int(self.placeKey)
            staticframeF = int(self.startFrame) - int(self.staticFrame)
            mucslesframeF = staticframeF - int(self.placeKey)
            bodyframeF = mucslesframeF - int(self.placeBody)

            self.ui.lbl_anim_frame.setText(self.startFrame)
            self.ui.lbl_muscles_frame.setText(str(staticframeF))
            self.ui.lbl_bones_frame.setText(str(mucslesframeF))
            self.ui.lbl_body_frame.setText(str(bodyframeF))

    def breakCon(self):
        #RelList = self.relationship
        #if RelList == {}:
            #cmds.confirmDialog(m='Please create a Driver-Driven relationship', b= 'ok')
            #sys.stdout.write('No relationship exists in the scene ')
        #else:
        keyFrames = self._get_keyframeSpacing()
        fv = int(keyFrames[0])-int(keyFrames[1])-int(keyFrames[2])
        print fv
        drivenNs = self.ui.drivenBox_02.currentText()
        if drivenNs:
            self.core_process.breakConnections(drivenNs, fv)
            self.test = 0
        else:
            cmds.confirmDialog(m='Please Add a drive  namespace', b='ok')
            sys.stdout.write('Please Add a drive  namespace ')

    def breakConSel(self):
        print "Breaking"
        self.core_process.breakConnectionSel()

    ####----------[Fat Operations]----------####

    def connectFat_(self):

        driverNs = str(self.ui.fat_drBox_driver.currentText())
        drivenNs = str(self.ui.fat_drBox_driven.currentText())
        self.core_fat_process.set_namespaces(driverNs, drivenNs)
        fat_relationship=self.core_fat_process.get_geo_relations()
        self.core_fat_process.connect_geometries(fat_relationship)

    def break_fatConnections(self):
        driverNs = str(self.ui.fat_drBox_driver.currentText())
        drivenNs = str(self.ui.fat_drBox_driven.currentText())
        self.core_fat_process.set_namespaces(driverNs, drivenNs)
        fat_relationship=self.core_fat_process.get_geo_relations()
        self.core_fat_process.break_connections(fat_relationship)



    ####----------[Skin Operations]----------####

    def blendMode(self):
        self.ui.skin_frame_cMap.setEnabled(True)
        self.blendshapeMode=1

    def attachmentsMode(self):
        self.ui.skin_frame_cMap.setEnabled(False)
        self.blendshapeMode=0


    def loadAnimNode(self):
        import pymel.core as pm
        selectionList=pm.ls(sl=1)

        if len(selectionList)== 1:
            animNode=selectionList[0]
            self.ui.Skin_LnEdit_anim.setText("{0}".format(animNode))

        elif len(selectionList) < 1:
            pm.confirmDialog(t= "Anim Attach", m = "Selecciona un objeto")

        else:
            pm.confirmDialog(t= "Anim Attach", m= "Solo Selecciona un objeto") 


    def loadFatNode(self):
        import pymel.core as pm
        selectionList=pm.ls(sl=1)

        if len(selectionList)== 1:
            fatNode=selectionList[0]
            self.ui.Skin_LnEdit_fat.setText("{0}".format(fatNode))

        elif len(selectionList) < 1:
            pm.confirmDialog(t= "Anim Attach", m = "Selecciona un objeto")

        else:
            pm.confirmDialog(t= "Anim Attach", m= "Solo Selecciona un objeto") 

    def connectSkin_(self):
        import pymel.core as pm
        animNode= self.ui.Skin_LnEdit_anim.text()
        fatNode= self.ui.Skin_LnEdit_fat.text()
        mapPath= self.ui.Skin_LnEdit_map.text()
        if self.blendshapeMode== 1:
            if os.path.exists(mapPath):
                self.core_skin.blendshape_connection(fatNode, animNode, mapPath)
            else:
                pm.confirmDialog(title="anim attach", m= "The weight map does not exist")
        elif self.blendshapeMode== 0:
            self.core_skin.zivaAttachment_connection(fatNode, animNode)

    def break_SkinConnections_(self):
        animNode= self.ui.Skin_LnEdit_anim.text()
        fatNode= self.ui.Skin_LnEdit_fat.text()
        if self.blendshapeMode== 1:
            self.core_skin.breakConn(animNode, fatNode, self.blendshapeMode)
           
        elif self.blendshapeMode== 0:
             self.core_skin.breakConn(animNode, fatNode, self.blendshapeMode)

            
    #---[Skin Wrights File Browser]---#
    def  fileBrowser(self):
        self._init_fileBrowser()

    def _init_fileBrowser(self):
        uifileBrowser= self.loc +'/resources/FileBrowser.ui'
        self._init_fileBrowser_ui(uifileBrowser, None)
        self.uiFileBrowser.treeView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.uiFileBrowser.treeView.customContextMenuRequested.connect(self.fileBrowser_context_menu)
        self.fileBrowser_populate()
        self.uiFileBrowser.show()

    def _init_fileBrowser_ui(self, uiFileName, parent=None):
        from pymel import versions
        if versions.current() < 201700:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        uifile = QtCore.QFile(uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)

        self.uiFileBrowser = self.loader.load(uifile, parent)
        uifile.close()

    def fileBrowser_populate(self):
        path= "/nfs/ollinvfx/Project/PIGS/library/Character FX/Ziva/Skin_weights/"
        self.model= QtWidgets.QFileSystemModel()
        self.model.setRootPath((QtCore.QDir.rootPath()))
        self.uiFileBrowser.treeView.setModel(self.model)
        self.uiFileBrowser.treeView.setRootIndex(self.model.index(path))
        self.uiFileBrowser.treeView.setSortingEnabled(True)

    def fileBrowser_context_menu(self):
        menu=QtWidgets.QMenu()
        select= menu.addAction("Select")
        select.triggered.connect(self.fileBrowser_select_file)
        cursor= QtGui.QCursor()
        menu.exec_(cursor.pos())


    def fileBrowser_select_file(self):
        index= self.uiFileBrowser.treeView.currentIndex()
        file_path= self.model.filePath(index)
        self.ui.Skin_LnEdit_map.setText(file_path)
        self.uiFileBrowser.hide()
        print file_path














