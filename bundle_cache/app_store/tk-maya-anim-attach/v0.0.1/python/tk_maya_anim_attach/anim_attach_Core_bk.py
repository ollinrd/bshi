import maya.cmds as cmds
import pymel.core as pm
import sys

class BonesSimulation():

    def __init__(self):
        pass


    #def UI_OPERATIONS(self):
        
        #Call interface

        #import Bones_simulation_bridge as bsb 
        #uiOps=bsb.formMain()
        #uiOps._init_maya_ui()
        #self.uiOps.show()


    def CreateRelationship(self,Geo_NS, Bones_NS):
        geometryNs = Geo_NS
        BonesNs = Bones_NS

        # Get Geometry
        TransformGeo = []
        cmds.select(geometryNs + ':*')
        G_nodes = cmds.ls(sl=1)
        cmds.select(cl=1)
        G_MeshList = []
        for n in G_nodes:
            if cmds.nodeType(n) == 'mesh':
                G_MeshList.append(n)

        for m in G_MeshList:
            Geometry = cmds.listRelatives(m, p=1)
            TransformGeo.append(Geometry[0])

        # Get Bones
        TransformBones = []
        cmds.select(BonesNs + ':*')
        B_nodes = cmds.ls(sl=1)
        cmds.select(cl=1)
        B_MeshList = []

        for n in B_nodes:
            if cmds.nodeType(n) == 'mesh':
                B_MeshList.append(n)

        for m in B_MeshList:
            Bone = cmds.listRelatives(m, p=1)
            TransformBones.append(Bone[0])

        # Create RelationShips
        GeoRelationship = []

        for g in TransformGeo:
            geometry = g.split(':')[-1]
            for b in TransformBones:
                bones = b.split(':')[-1]
                if geometry == bones:
                    BonesDict = {
                        "name": geometry,
                        "geometryName": g,
                        "boneName": b,
                    }
                    GeoRelationship.append(BonesDict)
                    break

        return GeoRelationship


    def createConnections(self, Rel_List, START_ANIM, STATIC_FRAMES, PLACE_BONES):
        StartAnim = START_ANIM
        stableFrames = STATIC_FRAMES
        placeBones = PLACE_BONES

        GeoRelationship= Rel_List

        # Group Driven Bones
        for n in GeoRelationship:
            parentGrp = cmds.listRelatives(n['boneName'], p=1)[0]
            bridge = cmds.group(n['boneName'], n="{0}_grp".format(n['name']))
            cmds.parent(n['boneName'], parentGrp)
            c = cmds.parentConstraint(n['boneName'], bridge)
            cmds.delete(c)
            cmds.makeIdentity(bridge, a=1, t=1, r=1, s=1)
            cmds.parent(n['boneName'], bridge)
            n['group'] = bridge

        # TimeOp
        sv = StartAnim - stableFrames
        PV = sv - placeBones
        cmds.refresh(su=True)

        # Constraint
        cmds.currentTime(StartAnim)
        for n in GeoRelationship:
            Bonesconst = cmds.parentConstraint(n['geometryName'], n['boneName'], mo=0, w=1)
            cmds.delete(Bonesconst)
            axes = ['x', 'y', 'z']
            Trl = []
            rot = []
            for axes in axes:
                rv = cmds.getAttr('{0}.r{1}'.format(n['boneName'], axes))
                rot.append(rv)
                n['rotate'] = rot

                tv = cmds.getAttr('{0}.t{1}'.format(n['boneName'], axes))
                Trl.append(tv)
                n['translate'] = Trl

            cmds.setKeyframe(n['boneName'])
            const = cmds.parentConstraint(n['geometryName'], n['group'], mo=1, w=1)[0]
            n['const'] = const
            attr = cmds.parentConstraint(const, q=1, wal=1)[0]
            n['attr'] = attr
            cmds.setKeyframe(n['const'], at=n['attr'], v=1)

        # Colocar Posicion Inicial
        cmds.currentTime(PV)

        for n in GeoRelationship:
            attrs = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz']
            for a in attrs:
                cmds.setAttr('{0}.{1}'.format(n['boneName'], a), 0)

            cmds.setKeyframe(n['boneName'])
            cmds.setKeyframe(n['const'], at=n['attr'], v=0)

            # Colocar posicion estable
        cmds.currentTime(sv)

        for n in GeoRelationship:
            cmds.setAttr('{0}.tx'.format(n['boneName']), n['translate'][0])
            cmds.setAttr('{0}.ty'.format(n['boneName']), n['translate'][1])
            cmds.setAttr('{0}.tz'.format(n['boneName']), n['translate'][2])

            cmds.setAttr('{0}.rx'.format(n['boneName']), n['rotate'][0])
            cmds.setAttr('{0}.ry'.format(n['boneName']), n['rotate'][1])
            cmds.setAttr('{0}.rz'.format(n['boneName']), n['rotate'][2])

            cmds.setKeyframe(n['boneName'])
            cmds.setKeyframe(n['const'], at=n['attr'], v=0)

        cmds.refresh(su=False)
        sys.stdout.write('the connections has been successfully created!')


    def breakConnections(self, Rel_List, prekey):
        cmds.refresh(su=True)
        cmds.currentTime(prekey)
        GeoRelationship= Rel_List

        for n in GeoRelationship:
        
            # break connections 
            cmds.cutKey(n['boneName']) 
        
      
            cmds.setAttr('{0}.tx'.format(n['boneName']),0)
            cmds.setAttr('{0}.ty'.format(n['boneName']),0)
            cmds.setAttr('{0}.tz'.format(n['boneName']),0)
    
            cmds.setAttr('{0}.rx'.format(n['boneName']),0)
            cmds.setAttr('{0}.ry'.format(n['boneName']),0)
            cmds.setAttr('{0}.rz'.format(n['boneName']),0)
        
            # Delete Constraint
            constraint=n['const']
            if constraint:
                cmds.delete(constraint)
        
            # Delete Grps
            supLevel=cmds.listRelatives(n['group'],p=1)
            cmds.parent(n['boneName'], supLevel, r=1)
            bridge=n['group']
            cmds.delete(bridge)
        
        cmds.refresh(su=False)









