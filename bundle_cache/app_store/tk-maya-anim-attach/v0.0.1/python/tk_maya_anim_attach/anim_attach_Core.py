import maya.cmds as cmds
import pymel.core as pm
import sys


class muscle_ops():

    def __init__(self):
        pass

    def CreateRelationship(self, Geo_NS, Bones_NS):
        geometryNs = Geo_NS
        BonesNs = Bones_NS

        # Get Geometry
        TransformGeo = []
        cmds.select(geometryNs + ':*')
        G_nodes = cmds.ls(sl=1, l=1)
        cmds.select(cl=1)
        G_MeshList = []
        for n in G_nodes:
            if cmds.nodeType(n) == 'mesh':
                G_MeshList.append(n)

        for m in G_MeshList:
            Geometry = cmds.listRelatives(m, p=1)
            TransformGeo.append(Geometry[0])

        # Get Bones
        TransformBones = []
        cmds.select(BonesNs + ':*')
        B_nodes = cmds.ls(sl=1, l=1)
        cmds.select(cl=1)
        B_MeshList = []

        for n in B_nodes:
            if cmds.nodeType(n) == 'mesh':
                B_MeshList.append(n)

        for m in B_MeshList:
            Bone = cmds.listRelatives(m, p=1)
            TransformBones.append(Bone[0])

        # Create RelationShips
        GeoRelationship = []

        for g in TransformGeo:
            geometry = g.split(':')[-1]
            for b in TransformBones:
                bones = b.split(':')[-1]
                if geometry == bones:
                    BonesDict = {
                        "name": geometry,
                        "geometryName": g,
                        "boneName": b,
                    }
                    GeoRelationship.append(BonesDict)
                    break

        return GeoRelationship

    def createConnections(self, Rel_List, START_ANIM, STATIC_FRAMES, PLACE_BONES, PLACE_BODY):
        StartAnim = START_ANIM
        stableFrames = STATIC_FRAMES
        placeBones = PLACE_BONES
        GeoRelationship = Rel_List

        # TimeOp
        sv = StartAnim - stableFrames
        PV = sv - placeBones

        cmds.refresh(su=True)

        first_bone = GeoRelationship[0]['boneName']
        grp_master = cmds.listRelatives(first_bone, p=1)[0]
        grp_master = cmds.listRelatives(grp_master, p=1)[0]

        # Place body to  previs anim position
        firstF = PV-PLACE_BODY
        cmds.currentTime(firstF)
        cmds.setKeyframe(grp_master)

        anim_t_curve = GeoRelationship[0]['geometryName']
        #anim_t_curve = anim_t_curve.split(':')[0]+':global_C0_ctl'
        anim_t_curve = anim_t_curve.split(':')[0]+':world_ctl'

        print "anim curve!", anim_t_curve

        # Colocar Posicion Inicial
        cmds.currentTime(PV)

        anim_t = cmds.getAttr(anim_t_curve+'.translate')[0]
        anim_r = cmds.getAttr(anim_t_curve+'.rotate')[0]

        cmds.playbackOptions(minTime=firstF, animationStartTime=firstF)

        print ">>>>>", anim_t
        print ">>>>>", anim_r
        cmds.setAttr(grp_master + '.translateX', anim_t[0])
        cmds.setAttr(grp_master + '.translateY', anim_t[1])
        cmds.setAttr(grp_master + '.translateZ', anim_t[2])
        cmds.setAttr(grp_master + '.rotateX', anim_r[0])
        cmds.setAttr(grp_master + '.rotateY', anim_r[1])
        cmds.setAttr(grp_master + '.rotateZ', anim_r[2])
        cmds.setKeyframe(grp_master)

        for n in GeoRelationship:
            cmds.setKeyframe(n['boneName'])
            const = cmds.parentConstraint(n['geometryName'], n['boneName'],
                                          w=1.0)
            n.update({'constraint': const})

        cmds.currentTime(sv)
        for n in GeoRelationship:
            cmds.setKeyframe(n['boneName'])
            cmds.delete(n['constraint'])

        # Group Driven Bones
        for n in GeoRelationship:
            parentGrp = cmds.listRelatives(n['boneName'], p=1, f=1)[0]
            #bridge = cmds.group(n['boneName'], n="{0}_grp".format(n['name']))
            bridge = cmds.group(n['boneName'], n="{0}_grp".format(n['boneName']))
            cmds.parent(n['boneName'], parentGrp)
            cmds.parent(n['boneName'], bridge)
            c = cmds.parentConstraint(n['geometryName'], bridge, mo=1)
            atts = cmds.listAttr(c, k=1)
            att_t = ''
            for a in atts:
                if n['boneName'].split(':')[-1] in a:
                    att_t = a
            cmds.setKeyframe(c, at=att_t, v=0.0, t=sv-1)
            cmds.setKeyframe(c, at=att_t, v=1.0, t=sv)

        cmds.refresh(su=False)
        sys.stdout.write('the connections has been successfully created!')

    def breakConnections(self, Bones_NS, prekey):
        cmds.refresh(su=True)
        cmds.currentTime(prekey)

        nameSpace = Bones_NS
        tfm = pm.ls(typ='transform')
        rootFile = []

        # Get Root Group
        for n in tfm:
            nln = n.longName()
            pipN = nln.count('|')
            if pipN == 1 and nameSpace in n.name():
                rootFile.append(n)
        element = rootFile[0]
        self.clearElement(element)
        cmds.refresh(su=False)

    def breakConnectionSel(self):
        firstF = cmds.playbackOptions(q=True, animationStartTime=True)
        cmds.currentTime(firstF)

        for s in pm.ls(sl=1):
            self.clearElement(s)

    def clearConstraint(self, element):
        """
        Cleaning constraints
        """
        try:
            pm.delete(element)
        except:
            print "problem deleting: ", element

    def clearGeometry(self, element):
        """
        Clear keyframes
        """
        nodes = pm.keyframe(element, q=True, name=1)
        for node in nodes:
            pm.delete(node)

    def moveUpNodes(self, element, nodes):
        father = pm.listRelatives(element, p=1)
        if len(father):
            father = father[0]
            for n in nodes:
                pm.parent(n, father)
            pm.delete(element)

    def clearElement(self, element):
        """"
        Master operation
        """
        e_type = element.type()
        if e_type == "parentConstraint":
            self.clearConstraint(element)

        if e_type == "transform":
            self.clearGeometry(element)
            rel = element.listRelatives()
            if len(rel) == 2:
                valid = False
                for r in rel:
                    if r.type() == "parentConstraint":
                        valid = True
                if valid:
                    self.moveUpNodes(element, rel)
            for r in rel:
                try:
                    self.clearElement(r)
                except:
                    print "problem ", r
                


class fat_ops():
    def __init__(self):
        
        self.driver_ns = ''
        self.driven_ns = ''

    def set_namespaces(self, driver_ns, driven_ns):
        # set working namespaces
        self.driver_ns = driver_ns
        self.driven_ns = driven_ns

    def get_geo_relations(self):
        # ['type': "Bone/muscle", 'driver': 'geo', 'driven': 'geo']
        geos_driver = []
        geos_driven = []

        master_relations = []

        tmp_ls = pm.ls(type='mesh')
        for tmp_n in tmp_ls:
            if self.driver_ns in tmp_n.longName():
                geos_driver.append(tmp_n)
            if self.driven_ns in tmp_n.longName():
                geos_driven.append(tmp_n)

        for driver in geos_driver:
            name_ = driver.name().split(self.driver_ns+':')[-1]
            for tmp_driven in geos_driven:
                driven_full = tmp_driven.longName()
                driver_full = driver.longName()
                driver_fake = driver_full.replace(self.driver_ns, self.driven_ns)

                #if self.driven_ns + ':' + name_ == tmp_driven.name():
                if driver_fake in driven_full:
                    if 'skel' in tmp_driven.name():
                        reg = {'type': 'bone',
                               'driver': driver,
                               'driven': tmp_driven}
                        master_relations.append(reg)
                    if 'muscles' in tmp_driven.name():
                        reg = {'type': 'muscle',
                               'driver': driver,
                               'driven': tmp_driven}
                        master_relations.append(reg)

        return master_relations

    def connect_geometries(self, geo_registers):
        for reg in geo_registers:
            driver_p = pm.listRelatives(reg['driver'], p=1)[0]
            driven_p = pm.listRelatives(reg['driven'], p=1)[0]

            if reg['type'] == 'bone':
                has_conn = False
                #if len(pm.listConnections(driven_p))>4:
                if "parentConstraint" in pm.listConnections(driven_p):
                    has_conn = True
                if not has_conn:
                    pm.parentConstraint(driver_p, driven_p, mo=1)
            if reg['type'] == 'muscle':
                blend = pm.blendShape(driver_p, driven_p)
                pm.setAttr(str(blend[0]) + '.weight[0]', 1.0)


        pm.confirmDialog(m="Fat Connection complete")
        print('Connection complete')


    def break_connections(self, geo_registers):
        import pymel.core as pm
        startFrame= str(pm.playbackOptions(q=1,min=1))
        pm.currentTime(startFrame)
       

        for reg in geo_registers:
            driver_node= pm.listRelatives(reg['driver'], p=1)[0]
            driven_node = pm.listRelatives(reg['driven'], p=1)[0]
            const_list=[]
            if reg['type'] == 'bone':
                listConn= pm.listConnections(driven_node)
                for con in listConn:
                    if "parentConstraint" in str(con) and con not in const_list:
                        const_list.append(con)

                for con in const_list:        
                    pm.delete(con)

            if reg['type'] == 'muscle':
                blendshapes=pm.ls(*pm.listHistory(driven_node) or [], type= 'blendShape')
                for bs in blendshapes:
                    pm.delete(bs)

        pm.confirmDialog(m="Break connections complete")
        sys.stdout.write("Break connections")
        


class skin_ops():
    def __init_(self):
        import pymel.core as pm
        import maya.mel as mel
        blendshape_mode= 1
        zivaAttachments_mode = 0

    def blendshape_connection(self,fat_node, anim_node, wm_path):
        import maya.mel as mel
        import pymel.core as pm
        anim_node= anim_node
        fat_node=fat_node
        path= wm_path
        blendShape_node=pm.blendShape(fat_node,anim_node)
        pm.blendShape(anim_node, edit = True, w = [0, 1.0] )

        #Connect map
        pm.select(cl=1)
        pm.select(anim_node)
        mel.eval('ArtPaintBlendShapeWeightsTool;')
        mel.eval('artAttrInitPaintableAttr;')
        mel.eval('artBlendShapeTargetIndex;')
        mel.eval('artAttrPaintOperation artAttrCtx Replace;') 
        ctx= pm.currentCtx() 
        pm.artAttrCtx( ctx, e = True, ifl = path, ifm = "luminance" )
        pm.confirmDialog(title= "Anim Attach ", m= "Connections complete")


    def zivaAttachment_connection(self, fat_node,anim_node):
        import maya.mel as mel
        import pymel.core as pm
        original_node= anim_node
        fat=fat_node
        anim_base= original_node + "_animBase"
        startFrame= str(pm.playbackOptions(q=1,min=1))
        pm.currentTime(startFrame)

        pm.rename(original_node, anim_base)

        #Duplicate without connections;
        anim_cage= pm.duplicate(anim_base, name= original_node)

        #convert zbone and ztissue
        pm.select(original_node)
        mel.eval("ziva -tissue")

        pm.select(anim_base)
        mel.eval("ziva -bone")

        pm.select(fat)
        mel.eval("ziva -bone")


        #Create attachments
        mel.eval("ziva -attachment {0} {1}".format(original_node, anim_base))
        mel.eval("ziva -goalingAttachment {0} {1}".format(original_node, fat))

        pm.confirmDialog(title= "Anim Attach ", m= "Connections complete")


    def breakConn(self, animNode, fatNode,mode):
        import pymel.core as pm
        blendShapeMode = mode
        targetMesh= animNode

        if blendShapeMode == 1:
            startFrame= str(pm.playbackOptions(q=1,min=1))
            pm.currentTime(startFrame)
            blendshapes = pm.ls(*pm.listHistory(targetMesh) or [], type= 'blendShape')
            for b in blendshapes:
                pm.delete(b)

        else:
            #blendShapeMode = 0
            zNodes= ["zGoalAttachment","zAttachment","zMaterial", "zTet", "zTissue", "zBone", "zGeo","zEmbedder","zSolver"]
            nodePrefix="_animBase"
            startFrame= str(pm.playbackOptions(q=1,min=1))
            pm.currentTime(startFrame)

            originalNode= animNode
            fatNode= fatNode
            animatedNode= originalNode + nodePrefix
            deleteNodes= []

            #Get Ziva Nodes
            originalNode_ls=pm.listHistory(originalNode)
            for zn in zNodes:
                for n in originalNode_ls:
                    if str(zn) in str(n):
                        deleteNodes.append(n)
            #Delete Ziva Nodes              
            for node in deleteNodes:
                pm.delete(node)
                       
            pm.delete(originalNode)
            pm.rename(animatedNode, originalNode)









        