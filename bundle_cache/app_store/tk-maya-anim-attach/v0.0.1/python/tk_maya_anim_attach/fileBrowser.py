import sys
import os
import pymel.core as pm
from PySide2 import QtWidgets

from sgtk.platform.qt import QtCore, QtGui

class fileBrowser():

	def __init__(self, loc, parent=None):
		
		self.uiFileBrowser= loc +'/resources/FileBrowser.ui'
		self.path = ´´
		self._init_maya_ui(self.uiFileBrowser, parent)
		self.ui.treeView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.ui.treeView.customContextMenuRequested.connect(self.context_menu)
		self.populate()
		self.show()

	def _init_maya_ui(self, uiFileName, parent=None):
		from pymel import versions
		if versions.current() < 201700:
		    from PySide import QtUiTools
		    self.loader = QtUiTools.QUiLoader()
		else:
		    from PySide2 import QtUiTools
		    self.loader = QtUiTools.QUiLoader()

		uifile = QtCore.QFile(self.uiFileBrowser)
		uifile.open(QtCore.QFile.ReadOnly)

		self.ui = self.loader.load(uifile, parent)
		uifile.close()


	def populate(self):
		path= "/nfs/ollinvfx/Project/PIGS/library/Character FX/Ziva/Skin_weights/"
		self.model= QtWidgets.QFileSystemModel()
		self.model.setRootPath((QtCore.QDir.rootPath()))
		self.ui.treeView.setModel(self.model)
		self.ui.treeView.setRootIndex(self.model.index(path))
		self.ui.treeView.setSortingEnabled(True)

	def context_menu(self):
		menu=QtWidgets.QMenu()
		select= menu.addAction("Select")
		select.triggered.connect(self.select_file)
		cursor= QtGui.QCursor()
		menu.exec_(cursor.pos())


	def select_file(self):
		index= self.ui.treeView.currentIndex()
		file_path= self.model.filePath(index)
		self.path= file_path
		self.ui.hide()
		print file_path



	def show(self):	
		self.ui.show()
