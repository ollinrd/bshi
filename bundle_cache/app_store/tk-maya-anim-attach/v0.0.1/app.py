import sys
import sgtk
from sgtk import TankError
from sgtk.platform import Application
from sys import platform


class SimulationConstraint(Application):
    def init_app(self):
        valid_steps = ['fx', 'character fx']
        if self.engine.context.step['name'] in valid_steps:
            self.engine.register_command("Animation to Ziva bones",
                                         self.mainWindow)
            self.loaded = False

    def mainWindow(self):

        if not self.loaded:
            self.loaded = True
            self.eng_name = self.engine.name
            self.sg = self.engine.shotgun

            # Get functionaities
            self.tk_Simulation_ConstraintOps = self.import_module("tk_maya_anim_attach")
            #Get instance of operations class depending on current
            #engine

        self.bridge = self.tk_Simulation_ConstraintOps.FormMain(
            loc=self.disk_location, context=self.engine.context,
            sg=self.sg, engine=self.engine)

        #Reload context
        self.bridge.show()
