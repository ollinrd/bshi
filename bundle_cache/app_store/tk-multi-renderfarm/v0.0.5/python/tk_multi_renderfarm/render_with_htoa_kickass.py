import glob
import os
import subprocess

ass_files_list = glob.glob("/nfs/benito1/Project/JUM2/sequences/mr1/mr1_380/work/acastaneda/fx/houdini/ass/JUM2_mr1_380_FXFF_ASS_v002/*.ass")

ass_folder_path =  "/home/cgarcia/Desktop/"
alfred_script = os.path.join(ass_folder_path, "mr1_380.alf")
job_file_open = file(alfred_script, 'w')
job_file_open.write("##AlfredToDo 3.0\n\n")
job_file_open.write("Job -title {%s} -subtasks {\n" % "mr1_380")
kick_bin_path = "/nfs/benito1/Project/common/software/solidangle"
kick_bin_path += "/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557"
kick_bin_path += "/htoa-1.14.4_r987f7af_houdini-16.0.557/scripts/bin/kick"

plugins_path = "/nfs/benito1/Project/common/software/solidangle"
plugins_path += "/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557"
plugins_path += "/htoa-1.14.4_r987f7af_houdini-16.0.557/arnold/plugins"
pre_cmd = "export solidangle_LICENSE=5053@172.16.29.159; "

for ass_file in ass_files_list:
    task = "\tTask {%s} -cmds {\n\
    \t\tRemoteCmd {%s %s  -l %s -v 4 -dw -i %s } -service {Houdini|arnold} \n\
    \t}\
    \n" % (os.path.basename(ass_file), pre_cmd, kick_bin_path,
           plugins_path, ass_file)
    
    job_file_open.write(task)

job_file_open.write("}")
job_file_open.close()


renderman = "/opt/pixar/RenderManStudio-2.0.2-maya2009/bin/alfred"
if not os.access(renderman, os.F_OK):
    rederman = '/opt/pixar/RenderMan_Studio-1.0.1-Maya2008/bin/alfred'

print(renderman)
print (alfred_script)
#subprocess.Popen((renderman, alfred_script))

