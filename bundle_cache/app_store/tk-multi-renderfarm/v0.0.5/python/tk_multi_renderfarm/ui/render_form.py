# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resources/render_form.ui'
#
# Created: Tue May  9 20:13:41 2017
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui

class Ui_render_form(object):
    def setupUi(self, render_form):
        render_form.setObjectName("render_form")
        render_form.resize(348, 249)
        self.verticalLayout = QtGui.QVBoxLayout(render_form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pages = QtGui.QStackedWidget(render_form)
        self.pages.setObjectName("pages")
        self.nuke_render = NukeRenderForm()
        self.nuke_render.setObjectName("nuke_render")
        self.pages.addWidget(self.nuke_render)
        self.maya_render = MayaRenderForm()
        self.maya_render.setObjectName("maya_render")
        self.pages.addWidget(self.maya_render)
        self.houdini_render = HoudiniRenderForm()
        self.houdini_render.setObjectName("houdini_render")
        self.pages.addWidget(self.houdini_render)
        self.render_result = RenderResultForm()
        self.render_result.setObjectName("render_result")
        self.pages.addWidget(self.render_result)
        self.verticalLayout.addWidget(self.pages)

        self.retranslateUi(render_form)
        self.pages.setCurrentIndex(2)
        QtCore.QMetaObject.connectSlotsByName(render_form)

    def retranslateUi(self, render_form):
        render_form.setWindowTitle(QtGui.QApplication.translate("render_form", "Form", None, QtGui.QApplication.UnicodeUTF8))

from ..render_result_form import RenderResultForm
from ..houdini_render_form import HoudiniRenderForm
from ..maya_render_form import MayaRenderForm
from ..nuke_render_form import NukeRenderForm
