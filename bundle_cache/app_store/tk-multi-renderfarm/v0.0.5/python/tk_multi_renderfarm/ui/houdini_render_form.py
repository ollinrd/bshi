# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'houdini_render_form.ui'
#
# Created: Thu Jun  1 16:12:44 2017
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui


class Ui_HoudiniRenderForm(object):
    def setupUi(self, HoudiniRenderForm):
        HoudiniRenderForm.setObjectName("HoudiniRenderForm")
        HoudiniRenderForm.resize(347, 170)
        self.verticalLayout = QtGui.QVBoxLayout(HoudiniRenderForm)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.start_label = QtGui.QLabel(HoudiniRenderForm)
        self.start_label.setObjectName("start_label")
        self.horizontalLayout.addWidget(self.start_label)
        self.start_line = QtGui.QLineEdit(HoudiniRenderForm)
        self.start_line.setMaximumSize(QtCore.QSize(75, 16777215))
        self.start_line.setObjectName("start_line")
        self.horizontalLayout.addWidget(self.start_line)
        self.end_label = QtGui.QLabel(HoudiniRenderForm)
        self.end_label.setObjectName("end_label")
        self.horizontalLayout.addWidget(self.end_label)
        self.end_line = QtGui.QLineEdit(HoudiniRenderForm)
        self.end_line.setMaximumSize(QtCore.QSize(75, 16777215))
        self.end_line.setObjectName("end_line")
        self.horizontalLayout.addWidget(self.end_line)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem2)
        self.by_label = QtGui.QLabel(HoudiniRenderForm)
        self.by_label.setObjectName("by_label")
        self.horizontalLayout_7.addWidget(self.by_label)
        self.by_line = QtGui.QLineEdit(HoudiniRenderForm)
        self.by_line.setMaximumSize(QtCore.QSize(75, 16777215))
        self.by_line.setObjectName("by_line")
        self.horizontalLayout_7.addWidget(self.by_line)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem4)
        self.by_label_2 = QtGui.QLabel(HoudiniRenderForm)
        self.by_label_2.setObjectName("by_label_2")
        self.horizontalLayout_8.addWidget(self.by_label_2)
        self.suffix = QtGui.QLineEdit(HoudiniRenderForm)
        self.suffix.setMaximumSize(QtCore.QSize(75, 16777215))
        self.suffix.setObjectName("suffix")
        self.horizontalLayout_8.addWidget(self.suffix)
        spacerItem5 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem5)
        self.verticalLayout.addLayout(self.horizontalLayout_8)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem6 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem6)
        self.submit_btn = QtGui.QPushButton(HoudiniRenderForm)
        self.submit_btn.setObjectName("submit_btn")
        self.horizontalLayout_4.addWidget(self.submit_btn)
        self.cancel_btn = QtGui.QPushButton(HoudiniRenderForm)
        self.cancel_btn.setObjectName("cancel_btn")
        self.horizontalLayout_4.addWidget(self.cancel_btn)
        spacerItem7 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem7)
        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.retranslateUi(HoudiniRenderForm)
        QtCore.QMetaObject.connectSlotsByName(HoudiniRenderForm)

    def retranslateUi(self, HoudiniRenderForm):
        HoudiniRenderForm.setWindowTitle(QtGui.QApplication.translate("HoudiniRenderForm", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.start_label.setText(QtGui.QApplication.translate("HoudiniRenderForm", "Start", None, QtGui.QApplication.UnicodeUTF8))
        self.end_label.setText(QtGui.QApplication.translate("HoudiniRenderForm", "End", None, QtGui.QApplication.UnicodeUTF8))
        self.by_label.setText(QtGui.QApplication.translate("HoudiniRenderForm", "Step", None, QtGui.QApplication.UnicodeUTF8))
        self.by_label_2.setText(QtGui.QApplication.translate("HoudiniRenderForm", "Suffix:", None, QtGui.QApplication.UnicodeUTF8))
        self.submit_btn.setText(QtGui.QApplication.translate("HoudiniRenderForm", "Submit", None, QtGui.QApplication.UnicodeUTF8))
        self.cancel_btn.setText(QtGui.QApplication.translate("HoudiniRenderForm", "Cancel", None, QtGui.QApplication.UnicodeUTF8))

