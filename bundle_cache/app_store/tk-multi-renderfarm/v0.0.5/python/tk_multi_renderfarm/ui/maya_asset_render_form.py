# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'maya_render_form.ui'
#
# Created: Mon Oct  3 11:49:41 2016
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui


class Ui_MayaRenderForm(object):
    def setupUi(self, MayaRenderForm):
        MayaRenderForm.setObjectName("MayaRenderForm")
        MayaRenderForm.resize(337, 260)
        self.verticalLayout = QtGui.QVBoxLayout(MayaRenderForm)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.start_label = QtGui.QLabel(MayaRenderForm)
        self.start_label.setObjectName("start_label")
        self.horizontalLayout.addWidget(self.start_label)
        self.start_line = QtGui.QLineEdit(MayaRenderForm)
        self.start_line.setMaximumSize(QtCore.QSize(75, 16777215))
        self.start_line.setObjectName("start_line")
        self.horizontalLayout.addWidget(self.start_line)
        self.end_label = QtGui.QLabel(MayaRenderForm)
        self.end_label.setObjectName("end_label")
        self.horizontalLayout.addWidget(self.end_label)
        self.end_line = QtGui.QLineEdit(MayaRenderForm)
        self.end_line.setMaximumSize(QtCore.QSize(75, 16777215))
        self.end_line.setObjectName("end_line")

        self.horizontalLayout.addWidget(self.end_line)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")

        self.by_label = QtGui.QLabel(MayaRenderForm)
        self.by_label.setObjectName("by_label")
        self.horizontalLayout_2.addWidget(self.by_label)
        self.by_line = QtGui.QLineEdit(MayaRenderForm)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.by_line.sizePolicy().hasHeightForWidth())
        self.by_line.setSizePolicy(sizePolicy)
        self.by_line.setMaximumSize(QtCore.QSize(50, 16777215))
        self.by_line.setObjectName("by_line")
        self.horizontalLayout_2.addWidget(self.by_line)
        self.render_type_label = QtGui.QLabel(MayaRenderForm)
        self.render_type_label.setObjectName("render_type_label")
        self.horizontalLayout_2.addWidget(self.render_type_label)
        self.render_type_cmb = QtGui.QComboBox(MayaRenderForm)
        self.render_type_cmb.setObjectName("render_type_cmb")
        self.render_type_cmb.addItem("")
        self.render_type_cmb.addItem("")
        self.render_type_cmb.addItem("")
        self.horizontalLayout_2.addWidget(self.render_type_cmb)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.path_label = QtGui.QLabel(MayaRenderForm)
        self.path_label.setObjectName("path_label")
        self.horizontalLayout_3.addWidget(self.path_label)
        self.path_line = QtGui.QLineEdit(MayaRenderForm)
        self.path_line.setObjectName("path_line")
        self.horizontalLayout_3.addWidget(self.path_line)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        #Asset Notes to SG
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.description_label = QtGui.QLabel(MayaRenderForm)
        self.description_label.setObjectName("description_label")
        self.horizontalLayout_4.addWidget(self.description_label)
        self.description_line = QtGui.QLineEdit(MayaRenderForm)
        self.description_line.setObjectName("description_line")
        self.description_line.setMinimumHeight(34)
        self.horizontalLayout_4.addWidget(self.description_line)
        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem2)
        self.submit_btn = QtGui.QPushButton(MayaRenderForm)
        self.submit_btn.setObjectName("submit_btn")
        self.horizontalLayout_5.addWidget(self.submit_btn)
        self.cancel_btn = QtGui.QPushButton(MayaRenderForm)
        self.cancel_btn.setObjectName("cancel_btn")
        self.horizontalLayout_5.addWidget(self.cancel_btn)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem4)

        self.retranslateUi(MayaRenderForm)
        QtCore.QMetaObject.connectSlotsByName(MayaRenderForm)

    def retranslateUi(self, MayaRenderForm):
        MayaRenderForm.setWindowTitle(QtGui.QApplication.translate("MayaRenderForm", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.start_label.setText(QtGui.QApplication.translate("MayaRenderForm", "Start", None, QtGui.QApplication.UnicodeUTF8))
        self.end_label.setText(QtGui.QApplication.translate("MayaRenderForm", "End", None, QtGui.QApplication.UnicodeUTF8))
        self.by_label.setText(QtGui.QApplication.translate("MayaRenderForm", "By", None, QtGui.QApplication.UnicodeUTF8))
        self.render_type_cmb.setItemText(0, QtGui.QApplication.translate("MayaRenderForm", "farm ass + farm kick", None, QtGui.QApplication.UnicodeUTF8))
        self.render_type_cmb.setItemText(1, QtGui.QApplication.translate("MayaRenderForm", "local ass + farm kick", None, QtGui.QApplication.UnicodeUTF8))
        self.render_type_cmb.setItemText(2, QtGui.QApplication.translate("MayaRenderForm","Redshift", None, QtGui.QApplication.UnicodeUTF8))
        self.path_label.setText(QtGui.QApplication.translate("MayaRenderForm", "Path", None, QtGui.QApplication.UnicodeUTF8))
        self.description_label.setText(QtGui.QApplication.translate("MayaRenderForm", "Description", None, QtGui.QApplication.UnicodeUTF8))
        self.submit_btn.setText(QtGui.QApplication.translate("MayaRenderForm", "Submit", None, QtGui.QApplication.UnicodeUTF8))
        self.cancel_btn.setText(QtGui.QApplication.translate("MayaRenderForm", "Cancel", None, QtGui.QApplication.UnicodeUTF8))

