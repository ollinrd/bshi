# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import tank
from tank.platform.qt import QtCore, QtGui

class MayaRenderForm(QtGui.QWidget):
    """
    Implementation of the main publish UI
    """
    # signals
    submit = QtCore.Signal()
    cancel = QtCore.Signal()
    
    def __init__(self, parent=None):
        """
        Construction
        
        :param parent:    The parent QWidget for this form
        """
        QtGui.QWidget.__init__(self, parent)
    
        self._reporter = None
        self.__title = "Doing Something"
        self.__current_stage = 1
        
        # set up the UI
        import sgtk
        engine=sgtk.platform.current_engine()
        entity=engine.context.entity["type"].lower()

        if entity== "shot":
             from .ui.maya_render_form import Ui_MayaRenderForm
        else:
            from .ui.maya_asset_render_form import Ui_MayaRenderForm


        self._ui = Ui_MayaRenderForm()
        self._ui.setupUi(self)

        self._ui.submit_btn.clicked.connect(self._on_submit)
        self._ui.cancel_btn.clicked.connect(self._on_cancel)

    def fill_UI(self):
        import maya.cmds as cmds
        self.start_frame = str(int(cmds.getAttr('defaultRenderGlobals.startFrame')))
        self.end_frame = str(int(cmds.getAttr('defaultRenderGlobals.endFrame')))
        self.by_frame = str(int(cmds.getAttr('defaultRenderGlobals.byFrame')))

        file_path = os.path.abspath(cmds.file(query=True, sn=True))
        scene_folder = os.path.dirname(file_path)
        self.path = os.path.join(scene_folder, "ASS_Files")
        self.path = self.path.replace("/maya/scenes", "/maya")

    # @property
    def __get_start_frame(self):
        return self._safe_to_string(self._ui.start_line.text()).strip()

    # @start_frame.setter
    def __set_start_frame(self, value):
        self._ui.start_line.setText(value)
    start_frame = property(__get_start_frame, __set_start_frame)

    # @property
    def __get_end_frame(self):
        return self._safe_to_string(self._ui.end_line.text()).strip()

    # @end_frame.setter
    def __set_end_frame(self, value):
        self._ui.end_line.setText(value)
    end_frame = property(__get_end_frame, __set_end_frame)

    # @property
    def __get_by_line(self):
        return self._safe_to_string(self._ui.by_line.text()).strip()

    # @by_frame.setter
    def __set_by_line(self, value):
        self._ui.by_line.setText(value)
    by_frame = property(__get_by_line, __set_by_line)

    # @property
    def __get_render_type_cmb(self):
        return self._ui.render_type_cmb.currentText()

    # @render_type_cmb.setter
    def __set_render_type_cmb(self, value):
        index = self._ui.render_type_cmb.findText(value)
        self._ui.render_type_cmb.setCurrentIndex(index)
    render_type = property(__get_render_type_cmb, __set_render_type_cmb)

    # @property
    def __get_path(self):
        return self._safe_to_string(self._ui.path_line.text()).strip()
    
    # @path.setter
    def __set_path(self, value):
        self._ui.path_line.setText(value)
    path = property(__get_path, __set_path)

    # @property
    def __get_description(self):
        return self._safe_to_string(self._ui.description_line.text()).strip()
    description= property(__get_description)

    def _on_submit(self):
        self.submit.emit()
        
    def _on_cancel(self):
        self.cancel.emit()

    def _safe_to_string(self, value):
        """
        safely convert the value to a string - handles
        QtCore.QString if usign PyQt
        """
        #
        if isinstance(value, basestring):
            # it's a string anyway so just return
            return value
        
        if hasattr(QtCore, "QString"):
            # running PyQt!
            if isinstance(value, QtCore.QString):
                # QtCore.QString inherits from str but supports 
                # unicode, go figure!  Lets play safe and return
                # a utf-8 string
                return str(value.toUtf8())
        
        # For everything else, just return as string
        return str(value)
