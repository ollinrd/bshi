# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.


from tank.platform.qt import QtCore, QtGui


class HoudiniRenderForm(QtGui.QWidget):
    """
    Implementation of the main publish UI
    """

    # signals
    submit = QtCore.Signal()
    cancel = QtCore.Signal()
    
    def __init__(self, parent=None):
        """
        Construction
        """
        QtGui.QWidget.__init__(self, parent)
        
        self.expand_single_items = False
        self.allow_no_task = False
        
        self._group_widget_info = {}
        self._tasks = []
    
        # set up the UI
        from .ui.houdini_render_form import Ui_HoudiniRenderForm
        self._ui = Ui_HoudiniRenderForm()
        self._ui.setupUi(self)

        self._ui.submit_btn.clicked.connect(self._on_submit)
        self._ui.cancel_btn.clicked.connect(self._on_cancel)

    def fill_UI(self):
        import hou
        start_frame, end_frame = hou.playbar.playbackRange()

        self.start_frame = str(start_frame)
        self.end_frame = str(end_frame)
        self.by_frame = "1"

    # @property
    def __get_start_frame(self):
        return self._safe_to_string(self._ui.start_line.text()).strip()

    # @start_frame.setter
    def __set_start_frame(self, value):
        self._ui.start_line.setText(value)
    start_frame = property(__get_start_frame, __set_start_frame)

    # @property
    def __get_end_frame(self):
        return self._safe_to_string(self._ui.end_line.text()).strip()

    # @end_frame.setter
    def __set_end_frame(self, value):
        self._ui.end_line.setText(value)
    end_frame = property(__get_end_frame, __set_end_frame)

    # @property
    def __get_by_line(self):
        return self._safe_to_string(self._ui.by_line.text()).strip()

    # @by_frame.setter
    def __set_by_line(self, value):
        self._ui.by_line.setText(value)
    by_frame = property(__get_by_line, __set_by_line)

    # @property
    def __get_suffix(self):
        return self._safe_to_string(self._ui.suffix.text()).strip()

    # @suffix.setter
    def __set_suffix(self, value):
        self._ui.suffix.setText(value)
    suffix = property(__get_suffix, __set_suffix)

    # @property
    def __get_frames_per_node(self):
        return self._safe_to_string(self._ui.fpn_line.text()).strip()

    # @frames_per_node.setter
    def __set_frames_per_node(self, value):
        self._ui.fpn_line.setText(value)

    def _on_submit(self):
        self.submit.emit()

    def _on_cancel(self):
        self.cancel.emit()

    def _safe_to_string(self, value):
        """
        safely convert the value to a string - handles
        QtCore.QString if usign PyQt
        """
        #
        if isinstance(value, basestring):
            # it's a string anyway so just return
            return value
        
        if hasattr(QtCore, "QString"):
            # running PyQt!
            if isinstance(value, QtCore.QString):
                # QtCore.QString inherits from str but supports 
                # unicode, go figure!  Lets play safe and return
                # a utf-8 string
                return str(value.toUtf8())
        
        # For everything else, just return as string
        return str(value)