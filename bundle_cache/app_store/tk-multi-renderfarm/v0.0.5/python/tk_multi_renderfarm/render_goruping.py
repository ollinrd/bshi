# Copyright (c) 2016 Ollin VFX.
import os
import math
import glob
import time
import shutil
import threading
import subprocess
import psutil

from localizer import relocalize_nodes
from localizer import unlocalize_nodes
from tank import TankError
from tank.platform.qt import QtGui
from tempfile import mkstemp

nuke_version = ""


class RenderHandler(object):
    """
    Main publish handler
    """
        
    def __init__(self, app):
        self._app = app
        
    def show_render_dlg(self):
        """
        Displays the publish dialog
        """
        
        try:
            # create new multi-publish dialog instance
            from .render_form import RenderForm
            display_name = self._app.get_setting("display_name")
            form = self._app.engine.show_dialog(display_name, self._app,
                                                RenderForm, self._app, self)

            form.nuke_submit.connect(lambda f=form: self._on_nuke_submit(f))
            form.maya_submit.connect(lambda f=form: self._on_maya_submit(f))
            form.houdini_submit.connect(lambda f=form: self._on_houdini_submit(f))
            self.__write_node_app = self._app.engine.apps.get("tk-nuke-writenode")

        except TankError, e:
            QtGui.QMessageBox.information(None, "Unable To Render!", "%s\n" % e)

        except Exception, e:
            self._app.log_exception("Unable to Render")

    def show_deadline(self):
        renderman_path = "/opt/Thinkbox/Deadline10/bin/deadlinemonitor"
        for pid in psutil.pids():
            p = psutil.Process(pid)
            if "deadlinemonitor" in p.name():
                break
        else:
            subprocess.Popen((renderman_path), shell=True)

    def _on_nuke_submit(self, render_form):
        import nuke
        nuke.root()['proxy'].setValue(False)
        global nuke_version 

        # Proyect must use this Nuke version to work and render
        nuke_version = "Nuke11.3v4/Nuke11.3"

        script = nuke.root().name()
        script_name = os.path.basename(script)
        script_copy = os.path.join(os.path.dirname(script),
                                   ".{0}".format(script_name))

        if os.access(script_copy, os.F_OK):
            os.remove(script_copy)

        unlocalize_nodes(nuke.allNodes())
        nuke.scriptSave()

        shutil.copy(script, script_copy)

        relocalize_nodes(nuke.allNodes())
        nuke.scriptSave()
        publish_errors = []

        try:
            selected = nuke.selectedNode()
            render_path = self.__write_node_app.get_node_render_path(selected)
            self._app.ensure_folder_exists(os.path.dirname(render_path))
        except(ValueError):
            publish_errors.append("Error: Please select a write node")
            render_form.show_result(not publish_errors, publish_errors)
            return

        nuke_cmd = self._construct_nuke_cmd(selected, render_form)

        self._send_job(nuke_cmd, publish_errors)
        self.show_deadline()

        render_form.show_result(not publish_errors, publish_errors)

    def _send_job(self, deadline_cmd, publish_errors):
        try:
            deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"

            self._app.log_debug(deadline)
            self._app.log_debug(deadline_cmd)
            process = subprocess.Popen(deadline + " " + deadline_cmd, 
                             shell=True, stdout=subprocess.PIPE)

            out, err = process.communicate()

            return out

        except Exception, e:
            publish_errors.append(str(e))

    def _set_batch(self, start, end, frames_per_node):
        batch = (float(end) - (float(start)-1.0)) / float(frames_per_node)
        return int(math.ceil(batch))

    def _construct_nuke_cmd(self, selected, render_form):
        #cache_size = "768m"
        import nuke 

        start = int(render_form.nuke_start_frame)
        end = int(render_form.nuke_end_frame)
        threads = render_form.nuke_threads
        force_license = render_form.nuke_force_license

        if force_license:
            execcommand = "-xi"
        else:
            execcommand = "-x"

        if threads != "maximo":
            add_threads = "-m {0}".format(threads)
        else:
            add_threads = ""

        if selected:
            if selected.Class() in ["WriteTank"]:#, "Write"]:
                # flags = " -t {0} -V {1} -X {2} -c {3}"(add_threads, execcommand, selected.name(), cache_size)
                print type(selected)
                flags = '" -t {0} -V {1} -X {2}'.format(add_threads,
                                                        execcommand,
                                                        selected.name())
            else:
                # flags = " -t {0} -V {1} -c {2}".format(add_threads. execcommand, cache_size)
                flags = '" -t {0} -V {1}'.format(add_threads, execcommand)
        else:
            flags = '" -t {0} -V {1}'.format(add_threads, execcommand)

        script_path = nuke.root().name().replace("/", os.path.sep)
        flags += ' <QUOTE>{0}<QUOTE> <STARTFRAME%4>,<STARTFRAME%4>"'.format(script_path)
        pool = self._define_pool()

        script_name = os.path.basename(script_path)
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " /usr/local/{0} -arguments ".format(nuke_version)
        nuke_cmd += flags
        nuke_cmd += ' -frames {0}-{1} -pool {2}'.format(start, end, pool)
        nuke_cmd += ' -name "{0}" -prop ConcurrentTasks=3'.format(script_name)

        return nuke_cmd

    def _define_pool(self):

        matchmove_steps = [4, 176]
        light_steps = [7, 185, 28, 39, 78]
        comp_steps = [8, 174]

        if self._app.context.step["id"] in matchmove_steps:
            return "mm"
        elif self._app.context.step["id"] in light_steps:
            return "light"
        elif self._app.context.step["id"] in comp_steps:
            return "comp"
        else:
            return "cg"

    def _construct_send_arnold_cmd(self, render_form, file_path, job_id):
        import maya.cmds as cmds

        start_frame = int(render_form.maya_start_frame)
        end_frame = int(render_form.maya_end_frame)
        
        scene_folder = os.path.dirname(file_path)
        ass_folder_path = os.path.join(scene_folder, "ASS_Files")
        # scene_name = os.path.basename(os.path.splitext(file_path)[0])
        scene_name = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')

        if scene_name:
            ass_file_path = os.path.join(ass_folder_path,
                                         scene_name + ".<STARTFRAME%4>.ass")
        else:
            scene_name = os.path.basename(os.path.splitext(file_path)[0])
            ass_file_path = os.path.join(ass_folder_path,
                                         scene_name + ".<STARTFRAME%4>.ass")

        # build the alfred job
        kick_bin_path = "/nfs/benito1/Project/common/software/solidangle"
        kick_bin_path += "/mtoa/{0}_new/bin/kick".format(cmds.about(version=True))

        shaders_path = "/nfs/benito1/Project/common/software/solidangle"
        shaders_path += "/mtoa/{0}_new/shaders".format(cmds.about(version=True))

        flags = ' " -i  <QUOTE>{0}<QUOTE> -dw -nstdin -v 4 -l {1}"'.format(ass_file_path,
                                                                           shaders_path)

        script_name = os.path.splitext(os.path.basename(file_path))[0]
        pool = self._define_pool()

        maya_cmd = " -SubmitCommandLineJob -executable"
       
        maya_cmd += " " + kick_bin_path + " -arguments "
        maya_cmd += flags
        maya_cmd += ' -frames {0}-{1} -pool {2}'.format(start_frame, end_frame, pool)
        maya_cmd += ' -prop BatchName={0} -name "{0}_exr" '.format( script_name)
        maya_cmd += ' -prop JobDependencies={0} -prop ConcurrentTasks=2'.format(job_id)

        deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"

        print deadline + " " + maya_cmd
        subprocess.Popen(deadline + " " + maya_cmd, shell=True)

    def _on_maya_submit(self, render_form):
        import maya.cmds as cmds
        # pull rest of info from UI

        renderMethod = render_form.maya_render_method

        if renderMethod == "kick+ass":
            self._app.log_debug("Rendering with kickass")
            batch_cmd = self._render_with_mtoa_kickass(render_form)
            publish_errors = []

            self._create_aovs_folders()

            out = self._send_job(batch_cmd, publish_errors)
            job_id = out.split("JobID=")[-1].split("The job")[0].strip()

            self.show_deadline()

            render_form.show_result(not publish_errors, publish_errors)
            file_path = os.path.abspath(cmds.file(query=True, sn=True))
            self._construct_send_arnold_cmd(render_form, file_path, job_id)

        else:
            if renderMethod == "mayabatch":
                print "Rendering with mayabatch"

    def _get_temp_file(self, sufix=''):
        """
        Ensure unique temporary file name.
        """
        fd, tmp_file = mkstemp(sufix)
        os.close(fd)
        os.remove(tmp_file)
        return tmp_file

    def _create_aovs_folders(self):
        import mtoa.aovs as aovs
        import maya.cmds as cmds

        # Create AOV
        # aovs.AOVInterface().addAOV('cputime', aovType='float')

        # List all AOVs with their names
        raw_aovs = aovs.AOVInterface().getAOVNodes(names=True)
        if raw_aovs:
            aov_list = zip(*raw_aovs)[0]
            file_path = os.path.abspath(cmds.file(query=True, sn=True))
            scene_folder = os.path.dirname(file_path)
            allCameras = cmds.listRelatives(cmds.ls(type="camera"), p=True)

            for aov in aov_list:
                if len(allCameras)>2:
                    for camera in allCameras:
                        camera_path = os.path.join(scene_folder, "images", camera)
                        
                        if cmds.getAttr("{0}.renderable".format(camera)):
                            
                            if not os.access(camera_path, os.F_OK):
                                os.makedirs(camera_path, 0777)
                            aov_path = os.path.join(camera_path, aov)

                            if not os.access(aov_path, os.F_OK):
                                os.makedirs(aov_path, 0777)

                        subprocess.call('chmod -R 777 {0}'.format(camera_path), shell = True)
                else:
                    aov_path_nocam = os.path.join(scene_folder, "images", aov)
                    if not os.access(aov_path_nocam, os.F_OK):
                            os.makedirs(aov_path_nocam, 0777)

                    subprocess.call('chmod -R 777 {0}'.format(aov_path_nocam), shell = True)

    def _render_with_mtoa_kickass(self, render_form):
        import maya.cmds as cmds

        start_frame = int(render_form.maya_start_frame)
        end_frame = int(render_form.maya_end_frame)

        file_path = os.path.abspath(cmds.file(query=True, sn=True))
        scene_folder = os.path.dirname(file_path)
        # scene_name = os.path.basename(os.path.splitext(file_path)[0])
        scene_name = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')
        ass_folder_path = os.path.join(scene_folder, "ASS_Files")
        images_folder =  os.path.join(scene_folder, "images")
        script_name = os.path.splitext(os.path.basename(file_path))[0]

        if not os.access(ass_folder_path, os.F_OK):
            os.mkdir(ass_folder_path, 0775)
        
        subprocess.call('chmod -R 777 {0}'.format(ass_folder_path), shell = True)
        subprocess.call('chmod -R 777 {0}'.format(images_folder), shell = True)

        if scene_name:
            ass_file_path = os.path.join(ass_folder_path,
                                         scene_name + ".<STARTFRAME%4>.ass")
        else:
            scene_name = os.path.basename(os.path.splitext(file_path)[0])
            ass_file_path = os.path.join(ass_folder_path,
                                         scene_name + ".<STARTFRAME%4>.ass")

        pool = self._define_pool()

        flags = ' "-proj {0} -batch -file {1}'.format(os.path.dirname(file_path), file_path)
        flags += " -command 'currentTime <STARTFRAME%4>;"
        flags += " arnoldExportAss -f <QUOTE>{0}<QUOTE>'".format(ass_file_path)
        flags += '"'

        batch_cmd = " -SubmitCommandLineJob -executable"
        batch_cmd += " /usr/autodesk/maya2016/bin/maya -arguments "
        batch_cmd += flags
        batch_cmd += ' -frames {0}-{1} -pool {2} -prop ConcurrentTasks=2'.format(start_frame, 
                                                                     end_frame, pool)
        batch_cmd += ' -prop BatchName={0} -name "{0}"_ass '.format(script_name)

        return batch_cmd

    def _on_houdini_submit(self, render_form):
        
        self._app.log_debug("Rendering with houdini ass")
        publish_errors = []
        alfred_script = self._render_with_htoa_kickass(render_form, publish_errors)
        self._send_job(alfred_script, publish_errors)
        render_form.show_result(not publish_errors, publish_errors)

    def _render_with_htoa_kickass(self, render_form, publish_errors):
        import hou

        start_frame = render_form.houdini_start_frame
        end_frame = render_form.houdini_end_frame
        
        by_frame = render_form.houdini_by_frame
        suffix = str(render_form.houdini_suffix)

        if not suffix:
            publish_errors.append("Please Write a suffix for the ass files")
            return 

        file_path = os.path.abspath(hou.hipFile.name())
        scene_folder = os.path.dirname(file_path)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])
        ass_folder_path = os.path.join(scene_folder, "ASS_Files")

        if not os.access(ass_folder_path, os.F_OK):
            self._app.ensure_folder_exists(ass_folder_path)
        
        ass_file_path = os.path.join(ass_folder_path,
                                     scene_name + "_{0}.$F4.ass".format(suffix))
        
        decoration = "********"

        arnold_nodes = hou.selectedNodes()
        for arnold_node in arnold_nodes:
            
            if arnold_node.type().name() == "arnold":
                arnold_node.parm("ar_ass_export_enable").set(1)
                arnold_node.parm("ar_ass_file").set(ass_file_path)

                arnold_node.parm("trange").set(1)
                arnold_node.parm("f1").set(start_frame)
                arnold_node.parm("f2").set(end_frame)
                arnold_node.parm("f3").set(by_frame)

                arnold_node.parm("execute").pressButton()
                break
            else:
                hou.ui.readMultiInput("Error!!", [])
                publish_errors.append("Please select an arnold node from the tree view")
                return 

        app_version = hou.applicationVersionString()

        if app_version == '14.0.291':
            htoa_version = "/htoa/htoa-1.4.0_r1291_houdini-14.0.291"
        else:
            htoa_version = "/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557"

        self._app.log_debug("\n\n"+decoration+" EXPORTNG ASS FILES "+decoration+"\n")

        alfred_script = os.path.join(ass_folder_path, scene_name + ".alf")
        self._app.log_debug("\n\n"+decoration+" CREATING ALF JOB "+decoration+"\n")
        job_file_open = file(alfred_script, 'w')
        job_file_open.write("##AlfredToDo 3.0\n\n")
        job_file_open.write("Job -title {%s\n} -subtasks {\n" % scene_name )
        kick_bin_path = "/nfs/benito1/Project/common/software/solidangle"
        kick_bin_path += "{0}/scripts/bin/kick".format(htoa_version)

        plugins_path = "/nfs/benito1/Project/common/software/solidangle"
        plugins_path += "{0}/arnold/plugins".format(htoa_version)
        pre_cmd = "export solidangle_LICENSE=5053@172.16.29.159:27000@172.16.29.189; "

        ass_files_list = glob.glob("{0}/{1}_{2}*.ass".format(ass_folder_path, scene_name,suffix))
        override = " -set options.shader_searchpath /nfs/benito1/Project/common/software/solidangle/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557/arnold/plugins -set options.procedural_searchpath /nfs/benito1/Project/common/software/solidangle/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557/arnold/procedurals"

        for ass_file in ass_files_list:
            task = "\tTask {%s\n} -cmds {\n\
            \t\tRemoteCmd {%s\n %s\n  -l %s\n -v 4 %s\n -dw -nstdin -i %s\n } -service {Houdini|arnold} \n\
            \t}\
            \n" % (os.path.basename(ass_file), pre_cmd, kick_bin_path,
                   plugins_path, override, ass_file)
            
            job_file_open.write(task)

        job_file_open.write("}")
        job_file_open.close()

        self._app.log_debug("\n\n"+decoration+" LAUNCHING ALF JOB "+decoration+"\n")

        return alfred_script
