# Copyright (c) 2016 Ollin VFX.


def unlocalize_nodes(nodes=[]):
    for n in nodes:
        if n.Class() == "Group":
            unlocalize_nodes(n.nodes())
        else:
            if n.Class() == "Read":
                if "local" in n.knobs():
                    if "local_path" in n.knobs():
                        if "prev_local_state" not in n.knobs():
                            prev_local_state = nuke.String_Knob("prev_local_state")
                            n.addKnob(prev_local_state)
                        n.knob("prev_local_state").setValue(str(n.knob("local").value()))
                        if n.knob("local").value():
                            if n.knob("local_path").value().strip() != "":
                                current_path = n.knob("file").value()
                                remote_path = current_path.replace(n.knob("local_path").value(), "")
                                n.knob("file").setValue(remote_path)
                                n.knob("label").setValue("")
                                #Keep the local path for future localization needs
                                #n.knob("local_path").setValue("")
                            n.knob("local").setValue(False)


#This function will be called usually after unlocalizing, for instance
#when doing farm rendering
def relocalize_nodes(nodes=[]):
    for n in nodes:
        if n.Class() == "Group":
            relocalize_nodes(n.nodes())
        else:
            if n.Class() == "Read":
                if "local" in n.knobs():
                    if "local_path" in n.knobs():
                        #Get the node previous state
                        #If the node was localized before this, relocalize it
                        if "prev_local_state" not in n.knobs():
                            prev_local_state = nuke.String_Knob("prev_local_state")
                            n.addKnob(prev_local_state)
                            n.knob("prev_local_state").setValue(str(n.knob("local").value()))
                        if n.knob("prev_local_state").value() == str(True):
                            if not n.knob("local").value():
                                if n.knob("local_path").value().strip() != "":
                                    local_path = n.knob("local_path").value()
                                    current_path = n.knob("file").value()
                                    n.knob("file").setValue(local_path+current_path)
                                    n.knob("label").setValue("localized to "+local_path)
                                n.knob("local").setValue(True)
