# Copyright (c) 2016 Ollin VFX.
import os
import math
import glob
import time
import shutil
import threading
import subprocess
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages/')
import psutil

from localizer import relocalize_nodes
from localizer import unlocalize_nodes
from tank import TankError
from tank.platform.qt import QtGui
from tempfile import mkstemp

nuke_version = ""


class RenderHandler(object):
    """
    Main publish handler
    """
        
    def __init__(self, app):
        self._app = app
        
    def show_render_dlg(self):
        """
        Displays the publish dialog
        """
        
        try:
            # create new multi-publish dialog instance
            from .render_form import RenderForm
            display_name = self._app.get_setting("display_name")
            form = self._app.engine.show_dialog(display_name, self._app,
                                                RenderForm, self._app, self)

            form.nuke_submit.connect(lambda f=form: self._on_nuke_submit(f))
            form.maya_submit.connect(lambda f=form: self._on_maya_submit(f))
            form.houdini_submit.connect(lambda f=form: self._on_houdini_submit(f))
            self.__write_node_app = self._app.engine.apps.get("tk-nuke-writenode")

        except TankError, e:
            QtGui.QMessageBox.information(None, "Unable To Render!", "%s\n" % e)

        except Exception, e:
            self._app.log_exception("Unable to Render")

    def show_deadline(self):
        renderman_path = "/opt/Thinkbox/Deadline10/bin/deadlinemonitor"
        for pid in psutil.pids():
            p = psutil.Process(pid)
            if "deadlinemonitor" in p.name():
                break
        else:
            subprocess.Popen((renderman_path), shell=True)

    def _on_nuke_submit(self, render_form):
        import nuke
        nuke.root()['proxy'].setValue(False)
        global nuke_version
        from datetime import datetime

        # Proyect must use this Nuke version to work and render
        import sys
        import sgtk
        if '/nfs/ovfxToolkit/Resources/site-packages' not in sys.path:
            sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
        import yaml
        proj = self._app.context.project
        tk = sgtk.sgtk_from_entity('Project', proj['id'])
        self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                      "project_specs.yml")
        with open(p_specs_path, 'r') as stream:
            try:
                self.specs = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        nuke_version = self.specs['main_nuke_path']
        script = nuke.root().name()
        now_str = datetime.now()
        now_str2 = str(now_str).split('.')[0].replace(' ', '_')
        script_name = os.path.basename(script) + '_' + now_str2

        script_copy = os.path.join(os.path.dirname(script), ".{0}".format(script_name))

        if os.access(script_copy, os.F_OK):
            os.remove(script_copy)

        with nuke.root():
            print ' '
            # In case another script miss graph level

        selected = nuke.selectedNode()
        selected_name = selected.name()
        selected_profile = selected.knob('profile_name').getValue()
        render_path = self.__write_node_app.get_node_render_path(selected)

        unlocalize_nodes(nuke.allNodes())
        nuke.scriptSave()

        shutil.copy(script, script_copy)

        relocalize_nodes(nuke.allNodes())
        nuke.scriptSave()
        publish_errors = []

        try:
            selected = nuke.selectedNode()
            profile = int(selected["tk_profile_list"].getValue())
            output_name = ''
            if profile in [6, 7]:
                output_name = selected["tank_channel"].getValue()
            if self._app.context.entity['type'] == 'Asset':
                publish_name = 'EXR Render in Workarea'
            else:
                if 'profile_name' in selected.knobs():
                    publish_name = selected['profile_name'].value()

            render_path = selected.knob('cached_path').getValue()
            self._app.ensure_folder_exists(os.path.dirname(render_path))
        except(ValueError):
            publish_errors.append("Error: Please select a write node")
            render_form.show_result(not publish_errors, publish_errors)
            return

        nuke_cmd = self._construct_nuke_cmd(selected, publish_name,
                                            render_form, script_copy)

        self._send_job(nuke_cmd, publish_errors)
        self.show_deadline()

        render_form.show_result(not publish_errors, publish_errors)

    def _send_job(self, deadline_cmd, publish_errors):
        try:
            deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"

            #self._app.log_debug(deadline)
            #self._app.log_debug(deadline_cmd)
            process = subprocess.Popen(deadline + " " + deadline_cmd, 
                                       shell=True, stdout=subprocess.PIPE)

            out, err = process.communicate()
            print 'Out: ', out
            print 'Err: ', err

            return out

        except Exception, e:
            publish_errors.append(str(e))

    def _set_batch(self, start, end, frames_per_node):
        batch = (float(end) - (float(start)-1.0)) / float(frames_per_node)
        return int(math.ceil(batch))

    def _construct_nuke_cmd(self, selected, deadline_name, render_form, script_copy):
        #cache_size = "768m"
        import nuke 

        start = int(render_form.nuke_start_frame)
        end = int(render_form.nuke_end_frame)
        threads = render_form.nuke_threads
        force_license = render_form.nuke_force_license
        frames_per_node = int(render_form.nuke_frames_per_node)
        bndl = self._app.engine.context.tank.pipeline_configuration.get_path()
        if frames_per_node < 1:
            frames_per_node = 1

        if force_license:
            execcommand = "-xi"
        else:
            execcommand = "-x"

        if threads != "maximo":
            add_threads = "-m {0}".format(threads)
        else:
            add_threads = "".format(bndl)

        if selected:
            if selected.Class() in ["WriteTank"]:#, "Write"]:
                # flags = " -t {0} -V {1} -X {2} -c {3}"(add_threads, execcommand, selected.name(), cache_size)
                print type(selected)
                flags = '" -t {0} -V {1} -X {2}'.format(add_threads,
                                                        execcommand,
                                                        selected.name())
            else:
                # flags = " -t {0} -V {1} -c {2}".format(add_threads. execcommand, cache_size)
                flags = '" -t {0} -V {1}'.format(add_threads, execcommand)
        else:
            flags = '" -t {0} -V {1}'.format(add_threads, execcommand)

        script_path = nuke.root().name().replace("/", os.path.sep)
        flags += ' <QUOTE>{0}<QUOTE> {1} <STARTFRAME%4>,<ENDFRAME%4>" '.format(script_copy, bndl)
        pool = self._define_pool()
        scene_folder = os.path.dirname(os.path.dirname(script_path))
        script_name = os.path.basename(script_path)

        batch_name = os.path.splitext(script_name)[0]

        special_nodes = self._nuke_getSpecialNodes()
        pool_name = "comp"
        if len(special_nodes[0]) > 0:
            pool_name = "opticalflares"

        task_num = 3
        if len(special_nodes[1]) > 0:
            task_num = 2

        if frames_per_node > 1:
            task_num = 1

        destiny_path = os.path.dirname(selected.knob('cached_path').getValue())

        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        nuke_cmd += flags
        nuke_cmd += ' -frames {0}-{1} -pool {2}'.format(start, end, pool)
        nuke_cmd += ' -chunksize {0}'.format(frames_per_node)
        nuke_cmd += ' -name "{0}" -prop ConcurrentTasks={1}'.format(deadline_name, task_num)
        nuke_cmd += " -prop OutputDirectory0={0}".format(destiny_path)
        nuke_cmd += " -prop BatchName={0}".format(batch_name)
        proj_info = self._app.context.project
        task_info = self._app.context.task

        # To send render video review
        if self._app.context.entity['type'] == 'Asset':
            # Get necessary info
            exr_tmplt = self._app.engine.get_template_by_name('nuke_asset_render_exr')
            fields = exr_tmplt.get_fields(destiny_path)


            seq_video_tmplt = self._app.engine.get_template_by_name('nuke_asset_render_movie')
            seq_video_path = seq_video_tmplt.apply_fields(fields)

            # Nuke script full path
            nuke_cmd += ' -prop ExtraInfo0={0}'.format(script_path)

            # EXR sequence, results of EXR render
            nuke_cmd += ' -prop ExtraInfo1={0}'.format(destiny_path)

            # Video path to be created
            nuke_cmd += ' -prop ExtraInfo2={0}'.format(seq_video_path)

            # Proj ID
            nuke_cmd += ' -prop ExtraInfo3={0}'.format(proj_info['id'])

            # Proj Code
            nuke_cmd += ' -prop ExtraInfo4={0}'.format(fields['project_code'])

            # Task ID
            nuke_cmd += ' -prop ExtraInfo5={0}'.format(task_info['id'])
        else:
            nuke_cmd += ' -prop ExtraInfo0={0}'.format(batch_name)
            nuke_cmd += ' -prop ExtraInfo1={0}'.format(destiny_path)
            #nuke_cmd += ' -prop ExtraInfo2={0}'.format(extra_info['name'])
        nuke_cmd += ' -priority 50'

        return nuke_cmd

    def _nuke_getSpecialNodes(self, group=None):
        """
        Get optical flares and Deep Reads
        """
        import nuke
        allOptical = allDeep = allGroups = []

        if not group:
            allOptical =  nuke.allNodes("OpticalFlares")
            allDeep = nuke.allNodes("DeepRead")
            allGroups = nuke.allNodes("Group")
            
        else:
            with group:
                allOptical =  nuke.allNodes("OpticalFlares")
                allDeep = nuke.allNodes("DeepRead")
                allGroups = nuke.allNodes("Group")

        for grp in allGroups:
            ls_nodes = self._nuke_getSpecialNodes(grp)
            allOptical = allOptical + ls_nodes[0]
            allDeep = allDeep + ls_nodes[1]

        return [allOptical, allDeep]

    def _define_pool(self):

        matchmove_steps = [4, 176, 228]
        light_steps = [7, 185, 28, 39, 78]
        comp_steps = [8, 174]

        matchmove_steps = [228]
        light_steps = [235, 240]
        comp_steps = [227, 238, 230, 231]
        #comp (comp, cleanup, paint, roto)

        if self._app.context.step["id"] in matchmove_steps:
            return "matchmove"
        #elif self._app.context.step["id"] in light_steps:
            #return "light"
        elif self._app.context.step["id"] in comp_steps:
            return "comp"
        else:
            return "cg"

    def _clear_maya_temp(self, job_id, maya_path):
        import maya.cmds as cmds
        file_path = cmds.file(query=True, sn=True)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])
        deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"

        python_file = maya_path + "_tmp.py"

        code = ["import os"]
        code.append("\nif os.access('{0}', os.F_OK):".format(maya_path))
        code.append("\n    os.remove('{0}')".format(maya_path))
        code.append("\nif os.access('{0}', os.F_OK):".format(python_file))
        code.append("\n    os.remove('{0}')".format(python_file))

        with open(python_file, 'w') as f:
            for r in code:
                f.write(str(r))

        clear_cmd = " -SubmitCommandLineJob -executable"
        clear_cmd += " python -arguments {0}".format(python_file)
        clear_cmd += ' -pool cg'
        clear_cmd += ' -name "Cleaning cache file"'
        clear_cmd += ' -prop BatchName={0}'.format(scene_name)
        clear_cmd += ' -prop ExtraInfo0={0}'.format(maya_path)
        clear_cmd += ' -prop JobDependencies={0}'.format(job_id)
        clear_cmd += ' -prop ConcurrentTasks=1'
        clear_cmd += " -prop JobDependencyPercentage=100"
        clear_cmd += " -prop OutputDirectory0=/home"

        #subprocess.Popen(deadline + " " + clear_cmd, shell=True)
        publish_errors = []
        print "Sending clean cache"
        out = self._send_job(clear_cmd, publish_errors)
        print out

    def _construct_send_arnold_cmd(self, render_form, file_path, description, job_id=0):
        # Create ASS files
        import maya.cmds as cmds
        import mtoa.aovs as aovs

        start_frame = int(render_form.maya_start_frame)
        end_frame = int(render_form.maya_end_frame)

        scene_folder = os.path.dirname(file_path)

        # Changed, load from UI
        ass_folder_path = render_form.maya_path

        if render_form.maya_path == "":
            scene_f_dir = os.path.dirname(scene_folder)
            ass_folder_path = os.path.join(scene_f_dir, "ASS_files")

        scene_name = os.path.basename(os.path.splitext(file_path)[0])
        file_prefix = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')

        multiLayer, layerName = self.render_multilayer()

        #if multiLayer:
            #ass_folder_path = ass_folder_path + '/{0}'.format(layerName)

        print 'LayerName: ', layerName
        print 'ass_folder_path: ', ass_folder_path


        ass_file_path = os.path.join(ass_folder_path, scene_name + ".<STARTFRAME%4>.ass")

        folder = self._get_maya_imageFolder()

        outDir = scene_folder.replace("maya/scenes", "maya/scenes/data")
        outDir += "/"+folder

        kick_bin_path = "/nfs/ovfxToolkit/Resources/plugins/autodesk"
        shaders_path = "/nfs/ovfxToolkit/Resources/plugins/autodesk"
        

        if cmds.about(version=True) == "2019":
            kick_bin_path += "/mtoa401/bin/kick"
            shaders_path += "/mtoa401/shaders"
        else:
            kick_bin_path += "/mtoa3202/bin/kick"
            shaders_path += "/mtoa3202/shaders"

        flags = ' " -i  <QUOTE>{0}<QUOTE>'.format(ass_file_path)
        flags += ' -dw -nstdin -v 4 -l {0}"'.format(shaders_path)

        scene_name = os.path.splitext(os.path.basename(file_path))[0]

        if self._app.engine.context.entity['type'] == 'Asset':
            e_type = 'asset'
        else:
            e_type = 'shot'

        get_templ = self._app.engine.get_template_by_name
        work_tmplt = get_templ("maya_{0}_work".format(e_type))
        fields = work_tmplt.get_fields(file_path)
        fields['render_layer'] = layerName
        fields['render_pass'] = 'beauty'
        print fields

        # Pases
        seq_output_tmplt = get_templ('maya_{0}_render_passes_exr'.format(e_type))

        #no pases
        AOVsList = aovs.AOVInterface().getAOVNodes(names=True)
        if len(AOVsList) is 0:
            seq_output_tmplt = get_templ('maya_{0}_render_exr'.format(e_type))
        seq_output_path = seq_output_tmplt.apply_fields(fields)

        print 'seq_output_path: ', seq_output_path

        seq_video_tmplt = get_templ('maya_{0}_exr_passes_video'.format(e_type))
        seq_video_path = seq_video_tmplt.apply_fields(fields)
        print 'seq_video_path: ', seq_video_path

        file_prefix = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')
        if '<RenderPass>' in file_prefix:
            str_ = '/masterLayer/'
            #tmp_1 = seq_output_path.split(str_)[0]
            #tmp_2 = file_prefix.split(str_)[1]
            #seq_output_path = tmp_1 + str_ + tmp_2
            seq_output_path = seq_output_path.replace('<RenderPass>', 'beauty')

            
        elif e_type == "asset" and "<RenderPass>" not in file_prefix:
            afterVersion= file_prefix.split("_v0")[-1]
            removing= afterVersion.split("_")[0]+ "_"
            afterVersion= afterVersion.replace(removing, "")
            seq_output_path = seq_output_path.replace('beauty', afterVersion)
            

    

        find_one = self._app.engine.shotgun.find_one
        project = find_one("Project", [['id', 'is', self._app.engine.context.project['id']]], ['code'])

        pool = self._define_pool()
    

        maya_cmd = " -SubmitCommandLineJob -executable"

        maya_cmd += " " + kick_bin_path + " -arguments "
        maya_cmd += flags
        maya_cmd += ' -frames {0}-{1} -pool {2}'.format(start_frame, end_frame, pool)
        maya_cmd += ' -name "{0} " '.format('{0} EXR render'.format(e_type))
        maya_cmd += ' -prop BatchName={0}'.format(scene_name)
        maya_cmd += ' -prop ExtraInfo0={0}'.format(file_path)
        maya_cmd += ' -prop ExtraInfo1={0}'.format(seq_output_path)
        maya_cmd += ' -prop ExtraInfo2={0}'.format(seq_video_path)
        maya_cmd += ' -prop ExtraInfo3={0}'.format(self._app.engine.context.project['id'])
        maya_cmd += ' -prop ExtraInfo4={0}'.format(project['code'])
        try:
            maya_cmd += ' -prop ExtraInfo5={0}'.format(self._app.engine.context.task['id'])
        except:
            #fields
            import sgtk
            id_task = sgtk.platform.current_engine().context.task['id']
            maya_cmd += ' -prop ExtraInfo5={0}'.format(id_task)

        if description != "":
            maya_cmd += ' -prop ExtraInfo6="{0}"'.format(description)


        print '-----------------'
        print maya_cmd
        print '-----------------'

        if job_id:
            maya_cmd += ' -prop JobDependencies={0} -prop ConcurrentTasks=1'.format(job_id)
        maya_cmd += " -prop JobDependencyPercentage=90 "
        # Adjusto to get one folder level up
        maya_cmd += " -prop OutputDirectory0={0}".format(outDir)
        maya_cmd += self._construct_environment_varibles()

        print 'EXR maya_cmd: ', maya_cmd
        
        deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"

        print deadline + " " + maya_cmd +' JOB'
        process=subprocess.Popen(deadline + " " + maya_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #out,err= process.communicate()
        
        print "........................"
        print "[Render EXR Job Info]"
        #print"Out:", out
        #print "Err:", err
        print "........................"



    def _construct_send_redshift_cmd(self, render_form, file_path):
        import maya.cmds as cmds
        mayaVersion = cmds.about(version=True)
        sceneFile = os.path.basename(file_path)
        sceneFolder = sceneFile.split('.')[0]
        sceneVersion = sceneFolder.split('_')[-1]
        outputPath = os.path.dirname(file_path) + '/render/' + sceneFolder.replace('_' + sceneVersion,'') + '/' + sceneVersion
        start_frame = int(render_form.maya_start_frame)
        end_frame = int(render_form.maya_end_frame)
        
        self._app.ensure_folder_exists(outputPath)

        infoJob = 'Name={0}\n' \
            'Frames={2}-{3}\n' \
            'Pool=redshift\n' \
            'Plugin=MayaBatch\n' \
            'OutputDirectory0={1}\n' \
            'EnvironmentKeyValue0=MAYA_MODULE_PATH=/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'.format(
                sceneFile.split('.')[0], outputPath, start_frame, end_frame)
        infoTxt = 'SceneFile={0}\n' \
            'Version={1}\n' \
            'Build=None\n' \
            'ProjectPath={3}\n' \
            'StrictErrorChecking=True\n' \
            'UseLegacyRenderLayers=0\n' \
            'LocalRendering=False\n' \
            'MaxProcessors=0\n' \
            'FrameNumberOffset=0\n' \
            'StrictErrorChecking=True\n' \
            'OutputFilePath={2}\n' \
            'Renderer=Redshift\n' \
            'GPUsPerTask=0\n' \
            'UseOnlyCommandLineOptions=0\n' \
            'IgnoreError211=FalseS\n'.format(file_path, mayaVersion, outputPath, os.path.dirname(file_path).replace('/scenes',''))

        maya_deadline_job_file = r'/tmp/maya_deadline_job.job'    
        with open(maya_deadline_job_file, 'w') as job_file:
            job_file.write(infoJob)

        maya_deadline_info_file = r'/tmp/maya_deadline_info.job'       
        with open(maya_deadline_info_file, 'w') as job_file:
            job_file.write(infoTxt)
        
        deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
        subprocess.Popen(deadline + " " + maya_deadline_job_file + " " + maya_deadline_info_file, shell=True)

    def _on_maya_submit(self, render_form):
        import maya.cmds as cmds
        # pull rest of info from UI

        renderMethod = render_form.maya_render_method

        #Description for SG review
        if self._app.engine.context.entity['type'].lower()=="asset":
            description= render_form.maya_description
        else: 
            description =""

        publish_errors = []

        cam_val= self.cam_validation()
        if cam_val == False:
            cmds.warning("Invalid Renderable Camera number")
            return False

        if renderMethod == "farm ass + farm kick":
            self._app.log_debug("Rendering with kickass")

            copy_maya = self._validation_maya()
            batch_cmd = self._render_with_mtoa_kickass(render_form, copy_maya)
            self._create_aovs_folders()
            out = self._send_job(batch_cmd, publish_errors)
            print 'OUT job: ', out
            job_id = out.split("JobID=")[-1].split("The job")[0].strip()
            file_path = os.path.abspath(cmds.file(query=True, sn=True))
            self._construct_send_arnold_cmd(render_form, file_path, description, job_id)

            # Clear tmp file created for deadline ass files generation.
            self._clear_maya_temp(job_id, copy_maya)
   
        elif renderMethod == "local ass + farm kick":
            copy_maya = self._validation_maya(local=True)
            self._render_with_mtoa_kickass(render_form, copy_maya, True)
            self._create_aovs_folders()
            file_path = os.path.abspath(cmds.file(query=True, sn=True))
            self._construct_send_arnold_cmd(render_form, file_path, description)
        elif renderMethod == "Redshift":
            file_path = os.path.abspath(cmds.file(query=True, sn=True))
            self._construct_send_redshift_cmd(render_form, file_path)

        self.show_deadline()

        render_form.show_result(not publish_errors, publish_errors)

    def _get_temp_file(self, sufix=''):
        """
        Ensure unique temporary file name.
        """
        fd, tmp_file = mkstemp(sufix)
        os.close(fd)
        os.remove(tmp_file)
        return tmp_file

    def _create_aovs_folders(self):
        import mtoa.aovs as aovs
        import maya.cmds as cmds
        import pymel.core as pm

        # Create AOV
        # aovs.AOVInterface().addAOV('cputime', aovType='float')

        # List all AOVs with their names
        raw_aovs = aovs.AOVInterface().getAOVNodes(names=True)

        # Open data folder permission
        file_path = os.path.abspath(cmds.file(query=True, sn=True))
        scene_folder = os.path.dirname(file_path)
        image_folder = scene_folder + "/data"

        try:
            subprocess.call('sudo chmod 777 {0}'.format(image_folder), shell = True)
        except:
            print "Check folder permission: {0}".format(image_folder)

        image_folder += "/" + self._get_maya_imageFolder()

        print image_folder, "<-------------- Image folder"

        rLayer = False
        aovsV = False

        #[Layers]
        renderLayers = cmds.ls(type="renderLayer")
        customLayers= []
        for l in renderLayers:
            if "defaultRenderLayer" not in l:
                customLayers.append(l)
        if len(renderLayers) > 1:
            rLayer = True

        #[AOVs]
        AOVsList = aovs.AOVInterface().getAOVNodes(names=True)
        if len(AOVsList) is not 0:
            aovsV = True

        #[Cameras]
        allCameras = cmds.listRelatives(cmds.ls(type="camera"), p=True)
        renderCams = []
        for c in allCameras:
            if cmds.getAttr("{0}.renderable".format(c)):
                renderCams.append(c)

        createdFolders = []
        """
        for cam in renderCams:
            if rLayer and aovsV:
                for rL in renderLayers:
                    for RPass in AOVsList:
                        RPass = str(RPass[0])
                        fPath = image_folder + "{0}_{1}/{2}".format(cam,rL, RPass)
                        if not os.access(fPath, os.F_OK):
                            os.makedirs(fPath, 0777)
                        subprocess.call('chmod 777 {0}'.format(fPath), shell=True)
            else:
                if rLayer:
                    for rL in renderLayers:
                        fPath = image_folder + "{0}_{1}".format(cam,rL)
                        if not os.access(fPath, os.F_OK):
                            os.makedirs(fPath, 0777)
                        subprocess.call('chmod 777 {0}'.format(fPath), shell=True)

                elif aovsV:
                    for RPass in AOVsList:
                        RPass = str(RPass[0])
                        fPath = image_folder + "{0}/{1}".format(cam, RPass)
                        if not os.access(fPath, os.F_OK):
                            os.makedirs(fPath, 0777)
                        subprocess.call('chmod 777 {0}'.format(fPath), shell=True)
                else:
                    fPath = image_folder + cam
                    if not os.access(fPath, os.F_OK):
                        os.makedirs(fPath, 0777)
                    subprocess.call('chmod 777 {0}'.format(fPath), shell=True)

        """

    def cam_validation(self):
        import maya.cmds as cmds
        cams= cmds.ls(typ="camera")
        cams_renderable=[]

        for c in cams:
            if cmds.getAttr(c + ".renderable"):
                cams_renderable.append(c)

        if len(cams_renderable) > 1:
            cmds.confirmDialog(t="Cam Validation",m="Tienes mas de una camara rendereable!, manten una sola camara en Render Settings", ma= "center")
            return False
        else:
            print "Your current cam is {0}".format(cams_renderable[0])
            return True
                

    def _render_with_mtoa_kickass(self, render_form, maya_copy, local=False):
        import maya.cmds as cmds
        import maya.mel as mel

        start_frame = int(render_form.maya_start_frame)
        end_frame = int(render_form.maya_end_frame)
        by_frame = int(render_form.maya_by_frame)

        original_file_path = os.path.abspath(cmds.file(query=True, sn=True))
        file_path = os.path.abspath(cmds.file(query=True, sn=True))
        scene_folder = os.path.dirname(file_path)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])

        # Cuation, almost always the artist leave empty this config
        file_prefix = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')
        print 'file_prefix: ', file_prefix

        # -------------------------------
        # Changes
        # -------------------------------
        # Use of the QLine ui to set the ASS storage folder
        ass_folder_path = render_form.maya_path
        if render_form.maya_path == "":
            scne_f_base = os.path.dirname(scene_folder)
            ass_folder_path = os.path.join(scne_f_base, "ASS_files")

        print 'ASS FOLDER: ', ass_folder_path

        #image_folder =  os.path.join(scene_folder, "images")

        # Forcing to the up folder
        image_folder = scene_folder.replace("/maya/scenes", "/maya/scenes/data")

        file_prefix_subFolder = ""
        if file_prefix is not None:
            file_prefix_subFolder = file_prefix.split("/")[0]

        image_folder += "/" + file_prefix_subFolder

        script_name = os.path.splitext(os.path.basename(file_path))[0]

        if not os.access(ass_folder_path, os.F_OK):
            #os.mkdir(ass_folder_path, 0775)
            self._app.ensure_folder_exists(ass_folder_path)

        subprocess.call('chmod 777 {0}'.format(ass_folder_path), shell=True)
        subprocess.call('chmod 777 {0}'.format(image_folder), shell=True)

        if not scene_name:
            file_split = os.path.splitext(file_path)[0]
            scene_name = os.path.basename(file_split)

        multiLayer, layerName = self.render_multilayer()

        #if multiLayer:
        #    ass_folder_path = ass_folder_path + '/{0}'.format(layerName)

        print 'LayerName: ', layerName

        print 'ass_folder_path: ', ass_folder_path
        print scene_name


        #ornatrix Validation
        ornatrix_Nodes=cmds.ls(typ='HairShape')
        if ornatrix_Nodes:
            ornatrixCmd= "OxSetIsRendering(true);"
        else:
            ornatrixCmd= ""

        print "Ornatrix Comand:", ornatrixCmd


        if local:
            ass_file_path = os.path.join(ass_folder_path,
                                         scene_name + ".%04i.ass")
            current_time = "currentTime %i;"
            export_cmd_template = 'arnoldExportAss -f "%s";'
            for frame in range(start_frame, end_frame + 1, by_frame):
                self._app.log_debug("Exporting frame %i" % frame)
                filename = ass_file_path % frame
                self._app.log_debug(filename)
                mel.eval(current_time % (frame))
                mel.eval(ornatrixCmd)
                mel.eval(export_cmd_template % filename)
        else:
            ass_file_path = os.path.join(ass_folder_path,
                                         scene_name + ".ass")
            pool = self._define_pool()

            e_type = 'shot'
            if self._app.engine.context.entity['type'] == 'Asset':
                e_type = 'asset'

            get_tmpl = self._app.engine.get_template_by_name
            work_tmplt = get_tmpl("maya_{0}_work".format(e_type))
            fields = work_tmplt.get_fields(file_path)

            fields['render_layer'] = layerName
            fields['render_pass'] = 'beauty'

            if not self.render_multiaovs():
                tmp_name = 'maya_{0}_render_exr'.format(e_type)
                seq_output_tmplt = get_tmpl(tmp_name)
            else:
                tmp_name = 'maya_{0}_render_passes_exr'.format(e_type)
                seq_output_tmplt = get_tmpl(tmp_name)

            seq_output_path = seq_output_tmplt.apply_fields(fields)
            print 'Template: ', seq_output_tmplt
            print 'seq_output_path: ', seq_output_path
            #print 'seq_output_path: ', seq_output_path['boom']
            #
            seq_video_tmplt = get_tmpl('maya_{0}_exr_video'.format(e_type))
            seq_video_path = seq_video_tmplt.apply_fields(fields)
            print 'seq_video_path: ', seq_video_path

            print 1, maya_copy
            print 2, seq_output_path
            print 3, seq_video_path

            f_dir = os.path.dirname(file_path)
            flags = ' "-proj {0} -batch -file {1}'.format(f_dir, maya_copy)
            flags += " -command '{0} arnoldExportAss".format(ornatrixCmd)
            flags += " -f <QUOTE>{0}<QUOTE>".format(ass_file_path)
            flags += " -startFrame <STARTFRAME%4>"
            flags += " -endFrame <ENDFRAME%4>"
            flags += "'"
            flags += '"'

            maya_path = '/usr/autodesk/maya2018/bin/maya'
            if '2019' in cmds.about(version=True):
                maya_path = '/usr/autodesk/maya2019/bin/maya'

            chunkN = int((end_frame-start_frame)/10)
            if chunkN < 2:
                chunkN = 2
            if chunkN > 10:
                chunkN = 10

            batch_cmd = " -SubmitCommandLineJob "
            batch_cmd += " -executable {0} ".format(maya_path)
            batch_cmd += " -arguments {0}".format(flags)
            batch_cmd += ' -frames {0}-{1}'.format(start_frame, end_frame)
            batch_cmd += ' -pool ass '
            batch_cmd += ' -chunksize {0}'.format(chunkN)
            batch_cmd += ' -name "{0}"'.format('{0} ASS render'.format(e_type))
            batch_cmd += ' -prop ConcurrentTasks=1'
            batch_cmd += ' -prop BatchName={0}'.format(script_name)
            batch_cmd += ' -prop ExtraInfo0={0}'.format(file_path)
            batch_cmd += ' -prop ExtraInfo1={0}'.format(seq_output_path)
            batch_cmd += ' -prop ExtraInfo2={0}'.format(seq_video_path)
            batch_cmd += ' -prop OutputDirectory0={0}'.format(ass_folder_path)

            batch_cmd += self._construct_environment_varibles()

            print 'ASS batch_cmd: ', batch_cmd
            return batch_cmd

    def _construct_environment_varibles(self):
        import maya.cmds as cmds

        evariables = " "
        arnoldPath = "/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa3202/"

        alShaders = "/ollin/ovfxToolkitSG/Resources/plugins/autodesk/"
        alShaders += "alShaders-linux-1.0.0rc19-ai4.2.12.2"
        maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'

        if '2019' in cmds.about(version=True):
            arnoldPath = '/nfs/ovfxToolkit/Resources/plugins/autodesk/mtoa401'
            maya_modules = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules2019'

        evariables += " -prop EnvironmentKeyValue0=ARNOLD_PLUGIN_PATH={0}".format(arnoldPath)
        evariables += " -prop EnvironmentKeyValue1=MTOA_TEMPLATES_PATH={0}/ae".format(alShaders)
        evariables += " -prop EnvironmentKeyValue2=MAYA_CUSTOM_TEMPLATE_PATH={0}/aexml".format(alShaders)
        evariables += " -prop EnvironmentKeyValue3=MAYA_MODULE_PATH={0}".format(maya_modules)
        evariables += " -prop EnvironmentKeyValue4=MAYA_DISABLE_CIP=1"

        return evariables

    def _on_houdini_submit(self, render_form):
        self._app.log_debug("Rendering with houdini ass")
        publish_errors = []
        alfred_script = self._render_with_htoa_kickass(render_form, publish_errors)
        self._send_job(alfred_script, publish_errors)
        render_form.show_result(not publish_errors, publish_errors)

    def _render_with_htoa_kickass(self, render_form, publish_errors):
        import hou

        start_frame = render_form.houdini_start_frame
        end_frame = render_form.houdini_end_frame
        
        by_frame = render_form.houdini_by_frame
        suffix = str(render_form.houdini_suffix)

        if not suffix:
            publish_errors.append("Please Write a suffix for the ass files")
            return 

        file_path = os.path.abspath(hou.hipFile.name())
        scene_folder = os.path.dirname(file_path)
        scene_name = os.path.basename(os.path.splitext(file_path)[0])
        ass_folder_path = os.path.join(os.path.dirname(scene_folder), "ASS_files")

        if not os.access(ass_folder_path, os.F_OK):
            self._app.ensure_folder_exists(ass_folder_path)
        
        ass_file_path = os.path.join(ass_folder_path,
                                     scene_name + "_{0}.$F4.ass".format(suffix))
        
        decoration = "********"

        arnold_nodes = hou.selectedNodes()
        for arnold_node in arnold_nodes:
            
            if arnold_node.type().name() == "arnold":
                arnold_node.parm("ar_ass_export_enable").set(1)
                arnold_node.parm("ar_ass_file").set(ass_file_path)

                arnold_node.parm("trange").set(1)
                arnold_node.parm("f1").set(start_frame)
                arnold_node.parm("f2").set(end_frame)
                arnold_node.parm("f3").set(by_frame)

                arnold_node.parm("execute").pressButton()
                break
            else:
                hou.ui.readMultiInput("Error!!", [])
                publish_errors.append("Please select an arnold node from the tree view")
                return 

        app_version = hou.applicationVersionString()

        if app_version == '14.0.291':
            htoa_version = "/htoa/htoa-1.4.0_r1291_houdini-14.0.291"
        else:
            htoa_version = "/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557"

        self._app.log_debug("\n\n"+decoration+" EXPORTNG ASS FILES "+decoration+"\n")

        alfred_script = os.path.join(ass_folder_path, scene_name + ".alf")
        self._app.log_debug("\n\n"+decoration+" CREATING ALF JOB "+decoration+"\n")
        job_file_open = file(alfred_script, 'w')
        job_file_open.write("##AlfredToDo 3.0\n\n")
        job_file_open.write("Job -title {%s\n} -subtasks {\n" % scene_name )
        kick_bin_path = "/nfs/benito1/Project/common/software/solidangle"
        kick_bin_path += "{0}/scripts/bin/kick".format(htoa_version)

        plugins_path = "/nfs/benito1/Project/common/software/solidangle"
        plugins_path += "{0}/arnold/plugins".format(htoa_version)
        pre_cmd = "export solidangle_LICENSE=5053@172.16.29.159:27000@172.16.29.189; "

        ass_files_list = glob.glob("{0}/{1}_{2}*.ass".format(ass_folder_path, scene_name,suffix))
        override = " -set options.shader_searchpath /nfs/benito1/Project/common/software/solidangle/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557/arnold/plugins -set options.procedural_searchpath /nfs/benito1/Project/common/software/solidangle/htoa/htoa-1.14.4_r987f7af_houdini-16.0.557/arnold/procedurals"

        for ass_file in ass_files_list:
            task = "\tTask {%s\n} -cmds {\n\
            \t\tRemoteCmd {%s\n %s\n  -l %s\n -v 4 %s\n -dw -nstdin -i %s\n } -service {Houdini|arnold} \n\
            \t}\
            \n" % (os.path.basename(ass_file), pre_cmd, kick_bin_path,
                   plugins_path, override, ass_file)
            
            job_file_open.write(task)

        job_file_open.write("}")
        job_file_open.close()

        str_ = "\n\n"+decoration+" LAUNCHING ALF JOB "+decoration+"\n"

        self._app.log_debug(str_)

        return alfred_script

    def _get_maya_imageFolder(self):
        import maya.cmds as cmds
        import maya.mel as mel

        scene_info = cmds.file(query=True, sn=True)
        scene_info = scene_info.split("/")[-1]
        scene_info = scene_info.split(".")[0]
        folder = scene_info.split("_")
        if len(folder) > 3:
            folder = folder[-3] + "_" + folder[-2] + "_" + folder[-1] + "/"
        else:
            folder = ""

        return folder

    def current_renderLayer(self, cmds):
        print sCurrentRL

    def render_multiaovs(self):
        import mtoa.aovs as aovs
        AOVsList = aovs.AOVInterface().getAOVNodes(names=True)
        if len(AOVsList) >= 1:
            return True

        return False

    def render_multilayer(self):
        import maya.cmds as cmds

        multiLayer = False
        currentRL = 'masterLayer'

        # Current layer name selected
        sCurrentRL = cmds.editRenderLayerGlobals(query=True,
                                                 currentRenderLayer=True)

        customLayers=[]
        layers_ls= cmds.ls(typ= "renderLayer")
        for l in layers_ls:
             if "defaultRenderLayer" not in l:
                 customLayers.append(l)

        #
        if customLayers:
            multiLayer = True
            if sCurrentRL != 'defaultRenderLayer':
                currentRL = str(sCurrentRL.replace('rs_', ''))

        return multiLayer, currentRL

    def _validation_maya(self, local=None):
        """
        Verify scene options
            Valid file prefix
            Frame extension name
        """
        import maya.cmds as cmds
        import maya.mel as mel
        from pymel import versions
        import pymel.core as pm

        if local is not None:
            # "Change images path"
            cmds.workspace(fr=("images", "scenes/data"))

        folder = self._get_maya_imageFolder()

        multipleLayers = multipleAOVs = createPrefix = False
        RenLayer = RenderPass = RenderPassF = RenLayerF = ""
        filePrefix = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')

        # Render layers
        multipleLayers, layerName = self.render_multilayer()
        print 'multipleLayers: ', multipleLayers

        # Select if multiple aovs for render
        multipleAOVs = self.render_multiaovs()
        print 'multipleAOVs: ', multipleAOVs

        if multipleLayers:
            RenLayer = "<RenderLayer>/"
            RenLayerF = "_<RenderLayer>"
            # Enable multiple render layers
            if mel.eval("optionVar -q renderViewRenderAllLayers;") == 0:
                mel.eval("switchRenderAllLayers;")
        else:
            RenLayer = "masterLayer/"

        if multipleAOVs:
            RenderPass = "_<RenderPass>"
            RenderPassF = "<RenderPass>/"
        else:
            RenderPass = "_beauty"
            RenLayerF = "_masterLayer"

        fileBase = folder.split('/')[0]
        print 'fileBase: ', fileBase


        f_name = str(cmds.file(q=True, sn=True))
        outputname = os.path.splitext(os.path.basename(f_name))[0]
        newPrefix = "{4}/{0}{1}{5}{3}{2}".format(RenLayer, RenderPassF,
                                                 RenderPass, RenLayerF,
                                                 fileBase, outputname)
        
        cmds.setAttr('defaultRenderGlobals.imageFilePrefix',
                     newPrefix, type="string")

        print 25*'='
        print newPrefix
        print 25*'='
        print 1, RenLayer
        print 2, RenderPassF
        print 3, RenderPass
        print 4, RenLayerF
        print 5, fileBase
        print 6, outputname

        mel.eval("setMayaSoftwareFrameExt(3,0)")

        """--------------------------------------------------
            SET ARNOLD RENDER SETTINGS FORMAT TO EXR 16BIT
           --------------------------------------------------
        """

        # set The global pading sequence
        cmds.setAttr("defaultRenderGlobals.extensionPadding", 4)
        # set tiled
        cmds.setAttr("defaultArnoldDriver.exrTiled", 0)
        #
        cmds.setAttr("defaultArnoldDriver.preserveLayerName", 0)
        #
        cmds.setAttr("defaultArnoldDriver.append", 0)
        #
        cmds.setAttr("defaultArnoldDriver.mergeAOVs", 0)

        try:
            cmds.file(save=True)
        except:
            print "Access denied, you can not save  your changes"

        if local is None:
            from datetime import datetime

            now_str = datetime.now()
            now_str2 = str(now_str).split('.')[0].replace(' ', '_')
            f_parts = f_name.split(".")
            f_name_copy = f_parts[0] + '_' + now_str2 + "." + f_parts[-1]

            base_name = os.path.basename(f_name_copy)

            f_name_copy = f_name_copy.replace(base_name, "." + base_name)

            if os.access(f_name_copy, os.F_OK):
                os.remove(f_name_copy)

            shutil.copy(f_name, f_name_copy)
            print ">>>>>>>>>>>>>>>>", f_name_copy
            return f_name_copy
        return ""
####