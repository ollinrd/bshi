# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import shutil

from tank.platform.qt import QtCore, QtGui


class NukeRenderForm(QtGui.QWidget):
    """
    Implementation of the main publish UI
    """

    # signals
    submit = QtCore.Signal()
    cancel = QtCore.Signal()
    
    def __init__(self, parent=None):
        """
        Construction
        """
        QtGui.QWidget.__init__(self, parent)
        
        self.expand_single_items = False
        self.allow_no_task = False
        
        self._group_widget_info = {}
        self._tasks = []
    
        # set up the UI
        from .ui.nuke_render_form import Ui_NukeRenderForm
        self._ui = Ui_NukeRenderForm()
        self._ui.setupUi(self)

        self._ui.submit_btn.clicked.connect(self._on_submit)
        self._ui.cancel_btn.clicked.connect(self._on_cancel)

    def fill_UI(self):
        import nuke
        first_f = int(nuke.Root()['first_frame'].value())
        last_f = int(nuke.Root()['last_frame'].value())
        self.start_frame = str(first_f)
        self.end_frame = str(last_f)
        fpn = (last_f - first_f)/16
        if fpn < 2:
            fpn = 2
        self.frames_per_node = "{0}".format(fpn)
        self.threads = "1"

    # @property
    def __get_start_frame(self):
        return self._safe_to_string(self._ui.start_line.text()).strip()

    # @start_frame.setter
    def __set_start_frame(self, value):
        self._ui.start_line.setText(value)
    start_frame = property(__get_start_frame, __set_start_frame)

    # @property
    def __get_end_frame(self):
        return self._safe_to_string(self._ui.end_line.text()).strip()

    # @end_frame.setter
    def __set_end_frame(self, value):
        self._ui.end_line.setText(value)
    end_frame = property(__get_end_frame, __set_end_frame)

    # @property
    def __get_frames_per_node(self):
        return self._safe_to_string(self._ui.fpn_line.text()).strip()

    # @frames_per_node.setter
    def __set_frames_per_node(self, value):
        self._ui.fpn_line.setText(value)
    frames_per_node = property(__get_frames_per_node, __set_frames_per_node)

    # @property
    def __get_threads_combo(self):
        return self._ui.threads_combo.currentText()

    # @threads.setter
    def __set_threads_combo(self, value):
        index = self._ui.threads_combo.findText(value)
        self._ui.threads_combo.setCurrentIndex(index)
    threads = property(__get_threads_combo, __set_threads_combo)

    # @property
    def __get_delay(self):
        return self._safe_to_string(self._ui.delay_line.text()).strip()

    # @delay.setter
    def __set_delay(self, value):
        self._ui.delay_line.setText(value)
    delay = property(__get_delay, __set_delay)

    def is_checked_force_license(self):
        return self._ui.force_check.isChecked()
    
    def is_checked_notify_status(self):
        return self._ui.notify_check.isChecked()

    def _on_submit(self):
        self.submit.emit()

    def _on_cancel(self):
        self.cancel.emit()

    def _safe_to_string(self, value):
        """
        safely convert the value to a string - handles
        QtCore.QString if usign PyQt
        """
        #
        if isinstance(value, basestring):
            # it's a string anyway so just return
            return value
        
        if hasattr(QtCore, "QString"):
            # running PyQt!
            if isinstance(value, QtCore.QString):
                # QtCore.QString inherits from str but supports 
                # unicode, go figure!  Lets play safe and return
                # a utf-8 string
                return str(value.toUtf8())
        
        # For everything else, just return as string
        return str(value)