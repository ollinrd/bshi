from sgtk.platform.qt import QtCore, QtGui
from .ui.render_form import Ui_render_form


class RenderForm(QtGui.QWidget):
    """
    Main application dialog window
    """
    nuke_submit = QtCore.Signal()
    maya_submit = QtCore.Signal()
    houdini_submit = QtCore.Signal()
    cancel = QtCore.Signal()

    def __init__(self, app, handler, parent = None):
        """
        Constructor
        """
        QtGui.QWidget.__init__(self)
        self._app = app
        self._ui = Ui_render_form()
        self._ui.setupUi(self)
        self._ui.nuke_render.submit.connect(self._on_nuke_render)
        self._ui.nuke_render.cancel.connect(self._on_close)
        self._ui.maya_render.submit.connect(self._on_maya_render)
        self._ui.maya_render.cancel.connect(self._on_close)
        self._ui.houdini_render.submit.connect(self._on_houdini_render)
        self._ui.houdini_render.cancel.connect(self._on_close)
        self._ui.render_result.close.connect(self._on_close)

        self.show_engine_render()

    def show_engine_render(self):
        if self._app.engine.name == 'tk-nuke':
            self._ui.pages.setCurrentWidget(self._ui.nuke_render)
            self._ui.nuke_render.fill_UI()
        elif self._app.engine.name == 'tk-maya':
            self._ui.pages.setCurrentWidget(self._ui.maya_render)
            self._ui.maya_render.fill_UI()
        else:
            self._ui.pages.setCurrentWidget(self._ui.houdini_render)
            self._ui.houdini_render.fill_UI()

    def show_result(self, success, errors):
        """
        Show the result of the publish in the UI
        """
        # show page:
        self._ui.pages.setCurrentWidget(self._ui.render_result)
        self._ui.render_result.status = success
        self._ui.render_result.errors = errors

    @property
    def nuke_start_frame(self):
        return self._ui.nuke_render.start_frame

    @property
    def nuke_end_frame(self):
        return self._ui.nuke_render.end_frame

    @property
    def nuke_frames_per_node(self):
        return self._ui.nuke_render.frames_per_node

    @property
    def nuke_threads(self):
        return self._ui.nuke_render.threads

    @property
    def nuke_delay(self):
        return self._ui.nuke_render.delay

    @property
    def nuke_force_license(self):
        return self._ui.nuke_render.is_checked_force_license()

    @property
    def nuke_notify(self):
        return self._ui.nuke_render.is_checked_notify_status()

    @property
    def maya_start_frame(self):
        return self._ui.maya_render.start_frame

    @property
    def maya_end_frame(self):
        return self._ui.maya_render.end_frame

    @property
    def maya_by_frame(self):
        return self._ui.maya_render.by_frame

    @property
    def maya_render_method(self):
        return self._ui.maya_render.render_type

    @property
    def maya_path(self):
        return self._ui.maya_render.path

    @property
    def maya_description(self):
        return self._ui.maya_render.description

    @property
    def houdini_start_frame(self):
        return self._ui.houdini_render.start_frame

    @property
    def houdini_end_frame(self):
        return self._ui.houdini_render.end_frame

    @property
    def houdini_by_frame(self):
        return self._ui.houdini_render.by_frame
#----------------------------------------------------------
    @property
    def houdini_suffix(self):
        return self._ui.houdini_render.suffix

    def _on_close(self):
        self.close()

    def _on_nuke_render(self):
        self.nuke_submit.emit()

    def _on_maya_render(self):
        self.maya_submit.emit()

    def _on_houdini_render(self):
        self.houdini_submit.emit()
