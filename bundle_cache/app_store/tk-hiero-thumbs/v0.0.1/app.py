# This example adds a right-click Menu to the Timeline View for getting the current Shot Selection.
# After running this action, 'hiero.selectedShots' will return the TrackItems selected in the timeline
from hiero.core import *
import hiero.ui

from hiero.ui import mainWindow # Need this to work around OS X 'Start Dictation' Bug
from PySide2 import QtGui
from PySide2 import QtCore
from PySide2 import QtUiTools
from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2 import QtWidgets
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import threading
import sgtk
import shutil
import tempfile
import subprocess
import os
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml
from sgtk.platform import Application

class Menu_ollin(Application,QAction):
    def init_app(self):
        self.UI_ = self.import_module("tk_hiero_version_zero")
        #self.engine = sgtk.platform.current_engine()
        QAction.__init__(self, "Get Selected Shots", None)
        self.triggered.connect(self.getShotSelection)
        hiero.core.events.registerInterest("kShowContextMenu/kTimeline",
                                           self.eventHandler)

    def get_settings(self):
        tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
        self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                      "project_specs.yml")
        with open(p_specs_path, 'r') as stream:
            try:
                return yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def getShotSelection(self):
        """Get the shot selection and stuff it in: hiero.selectedShots"""
        warning = QMessageBox()
        warning.setText('Create THUMBNAILS of selected clips?')
        warning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        sure = warning.exec_()

        if sure == QMessageBox.Ok:
            
            self.config_path = self.engine.context.tank.pipeline_configuration.get_path()
            self.sg = self.engine.shotgun
            self.sg_project = self.engine.context.project
            self.sg_project = self.sg.find_one('Project',[['id','is',self.sg_project['id']]],['code'])
            self.settings_ = self.get_settings()
            os.system("USER=$(whoami)")
            self.user = os.environ["USER"]
            self.thumb_template = self.engine.get_template_by_name("editorial_thumbnail")

            selection = self._selection
            self.shot_dict = []
            if len(selection)==1:
                clip = selection[0]
                path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                seq = clip.name().split('_')[0]
                self.set_fields_and_path(clip, seq, path_source)
            else:
                for clip in selection:
                    path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                    seq = clip.name().split('_')[0]
                    self.set_fields_and_path(clip, seq, path_source)

            prefixx = "/nfs/ovfxToolkit/TMP/"
            (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "thumbnail_dict_", suffix='.txt')
            file = open(dictPath, 'w')
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell = True)
            file.write(str(self.shot_dict))
            file.close()
            tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
            config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
            scriptPath = os.path.join(config_path, "hooks", "Hiero",
                                                   "ovfx_thumbnail_template.py")
            self.send_job(scriptPath, 'thumbnails_' + str(self.sg_project['code']) + str(clip.name()),dictPath)

    def send_job(self, python_script, name, dict_):
        nuke_version = self.settings_["main_nuke_path"]
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        bndl_path = self.engine.context.tank.pipeline_configuration.get_path()
        nuke_cmd += ' "-t {0} {1} {2}" '.format(python_script, dict_, bndl_path)
        nuke_cmd += ' -name "{0}" -pool editorial'.format(name)
        nuke_cmd += ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        deadline = self.settings_["deadline_command_path"]
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd
        warning = QMessageBox()
        warning.setText('THUMBNAILS job now is  in Deadline!')
        warning.setStandardButtons(QMessageBox.Ok)
        warning.exec_()

    def set_fields_and_path(self, clip, seq, path_source):
        self.shot_d = {}
        seq = clip.parent().parent().name()
        fields = {'Shot': str(clip.name()),
                  'Sequence': str(seq)}
        track = clip.parent()
        indice = 0
        for itm in track.items():
            indice += 1
            print itm.name()
            if itm == clip:
                break
        fields['project'] = self.sg_project['code']
        self.thumb_path = self.thumb_template.apply_fields(fields)
        self.shot_d['clip_thumb_path'] = self.thumb_path
        print 'THUMB PATH: ', self.thumb_path
        self.shot_d['shot'] = str(clip.name())
        self.shot_d['seq'] = str(seq)
        self.shot_d['project'] = self.sg_project
        self.shot_d['indice'] = indice
        FR = framerate = float(clip.source().mediaSource().metadata()['media.input.frame_rate'])
        self.shot_d['cut_origin_path'] = path_source
        fr_range = str(clip.timelineIn()) + '-' + str(clip.timelineOut())
        first = int(clip.timelineIn())
        last = int(clip.timelineOut())
        tup = hiero.core.Timecode.framesToHMSF(last, framerate,True)
        timecode_out = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        tup = hiero.core.Timecode.framesToHMSF(first, framerate,True)
        timecode_in = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        self.shot_d['clip_duration'] = last - first + 1
        self.shot_d['clip_fr_range'] = fr_range
        self.shot_d['clip_first'] = first
        self.shot_d['clip_last'] = last
        self.shot_d['project_code'] = self.sg_project['code']
        self.shot_d['framerate'] = FR
        self.shot_d['artist'] = self.user
        self.shot_d['clip_timecode_in'] = timecode_in
        self.shot_d['clip_timecode_out'] = timecode_out
        self.shot_d['clip_name'] = str(clip.name())
        self.shot_dict.append(self.shot_d)

    def eventHandler(self, event):
        self._selection = None
        if hasattr(event.sender, 'getSelection') and event.sender.getSelection() is not None and len( event.sender.getSelection() ) != 0:
            selection = event.sender.getSelection() # Here, you could also use: hiero.ui.activeView().selection()
            self._selection = [shot for shot in selection if isinstance(shot,hiero.core.TrackItem)] # Filter out just TrackItems
            if len(selection) > 0:
                title = "OLLIN Create Thumbnal For Selected Shot"
                self.setText(title)
                menu = event.menu 
                for i in event.menu.actions():
                    if str(i.text()) == "OLLIN Create Thumbnal For Selected Shot":
                        menu.removeAction(i)
                menu.addAction(self)
