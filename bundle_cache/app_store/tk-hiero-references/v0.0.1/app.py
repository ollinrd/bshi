# This example adds a right-click Menu to the Timeline View for getting the current Shot Selection.
# After running this action, 'hiero.selectedShots' will return the TrackItems selected in the timeline
from hiero.core import *
import hiero.ui

from hiero.ui import mainWindow # Need this to work around OS X 'Start Dictation' Bug
from PySide2 import QtGui
from PySide2 import QtCore
from PySide2 import QtUiTools
from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2 import QtWidgets
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import threading
import sgtk
import shutil
import tempfile
import subprocess
import os
import sys
from sgtk.platform import Application
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml

class Menu_ollin(Application,QAction):
    def init_app(self):
        QAction.__init__(self, "Get Selected Shots", None)
        self.triggered.connect(self.getShotSelection)
        hiero.core.events.registerInterest("kShowContextMenu/kTimeline", self.eventHandler)
        self.sg = self.engine.shotgun
        self.sg_project = self.sg.find_one('Project',[['id','is',self.engine.context.project['id']]],['code'])
        # projecct setting specs
        self.sett = self.get_settings()

    def get_settings(self):
        tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
        self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        p_specs_path = os.path.join(self.config_path, "resources", "project",
                                    "project_specs.yml")
        with open(p_specs_path, 'r') as stream:
            try:
                return yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def getShotSelection(self):
        """Get the shot selection and stuff it in: hiero.selectedShots"""
        warning = QMessageBox()
        warning.setText('Create REFERENCES of selected clips?')
        warning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        sure = warning.exec_()
        if sure == QMessageBox.Ok:
            self.show_ui()
            self.config_path = self.engine.context.tank.pipeline_configuration.get_path()
            self.sg = self.engine.shotgun
            self.sg_project = self.engine.context.project
            self.sg_project = self.sg.find_one('Project',[['id','is',self.sg_project['id']]],['code'])
            os.system("USER=$(whoami)")
            self.user = os.environ["USER"]
            mov_template_name = self.sett['editorial_mov_reference_template']
            jpg_template_name = self.sett['editorial_jpg_reference_template']
            self.mov_template = self.engine.get_template_by_name(mov_template_name)
            self.jpg_template = self.engine.get_template_by_name(jpg_template_name)

            selection = self._selection
            self.shot_dict = []
            if len(selection) == 1:
                clip = selection[0]
                path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                seq = clip.name().split('_')[0]
                self.set_fields_and_path(clip, seq, path_source)
            else:
                for clip in selection:
                    path_source = clip.source().mediaSource().fileinfos()[0].filename().split(' ')[0]
                    seq = clip.name().split('_')[0]
                    self.set_fields_and_path(clip, seq, path_source)
            warning = QMessageBox()
            warning.setText('REFERENCES job now is  in Deadline!')
            warning.setStandardButtons(QMessageBox.Ok)
            warning.exec_()

    # generate paths and Cut
    def send_job(self, python_script, name, dict_):
        nuke_version = self.sett["main_nuke_path"]
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " {0} -arguments ".format(nuke_version)
        nuke_cmd += ' "-t {0} {1} {2}" '.format(python_script,dict_, self.config_path)
        nuke_cmd += ' -name "{0}" -pool editorial'.format(name)
        nuke_cmd += ' -prop BatchName={0}'.format(name)
        subprocess.call('chmod -R 777 {0}'.format(python_script), shell = True) 
        print 'chmod -R 777 {0}'.format(python_script)
        deadline = self.sett["deadline_command_path"]
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd

    def set_fields_and_path(self, clip, seq, path_source):
        self.shot_d = {}
        seq = clip.parent().parent().name()
        fields = {'Shot': str(clip.name()),
                  'Sequence': str(seq),
                  'version': int(self.VERSION)}
        track = clip.parent()
        indice = 0
        for itm in track.items():
            indice += 1
            print itm.name()
            if itm == clip:
                break

        fields['width'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[0])
        fields['height'] = int(clip.source().mediaSource().metadata()['foundry.source.resolution'].split('x')[1])
        fields['elementName'] = track.name()
        fields['project'] = self.sg_project['code']

        self.path_dest_mov = self.mov_template.apply_fields(fields)
        self.path_dest_jpg = self.jpg_template.apply_fields(fields)

        self.shot_d['clip_mov'] = self.path_dest_mov
        self.shot_d['clip_jpg'] = self.path_dest_jpg

        self.shot_d['shot'] = str(clip.name())
        self.shot_d['seq'] = str(seq)
        self.shot_d['version'] = int(self.VERSION)
        self.shot_d['project'] = self.sg_project
        self.shot_d['indice'] = indice
        self.shot_d['t_name'] = track.name()

        if self.VERSION == '':
            self.VERSION = 1
        Vers = '{0:03d}'.format(int(self.VERSION))
        self.shot_d['v_str'] = Vers
        FR = framerate = float(clip.source().mediaSource().metadata()['media.input.frame_rate'])

        #out_ref = self.ref_path

        media = clip.source().mediaSource()
        self.shot_d['cut_origin_path'] = path_source
        self.shot_d['version'] = int(self.VERSION)
        fr_range = str(clip.timelineIn()) + '-' + str(clip.timelineOut())
        first = int(clip.timelineIn())
        last = int(clip.timelineOut())
        tup = hiero.core.Timecode.framesToHMSF(last, framerate,True)
        timecode_out = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        tup = hiero.core.Timecode.framesToHMSF(first, framerate,True)
        timecode_in = '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(tup[0], tup[1], tup[2], tup[3])
        self.shot_d['clip_duration'] = last - first + 1
        self.shot_d['clip_fr_range'] = fr_range
        self.shot_d['clip_first'] = first
        self.shot_d['clip_last'] = last
        self.shot_d['project_code'] = self.sg_project['code']
        self.shot_d['framerate'] = FR
        self.shot_d['artist'] = self.user
        self.shot_d['clip_timecode_in'] = timecode_in
        self.shot_d['clip_timecode_out'] = timecode_out
        self.shot_d['clip_name'] = str(clip.name())
        self.shot_d['has_cut'] = self.has_cut.isChecked()

        prefixx = self.sett["tmp_folder"]
        (file, dictPath) = tempfile.mkstemp(prefix=prefixx + "references_dict_", suffix='.txt')
        file = open(dictPath, 'w')
        subprocess.call('chmod -R 777 {0}'.format(dictPath), shell = True)
        file.write(str(self.shot_d))
        file.close()
        tk = sgtk.sgtk_from_entity('Project', self.sg_project['id'])
        config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        scriptPath = os.path.join(config_path, "hooks", "Hiero",
                                  "ovfx_references_template.py")
        self.send_job(scriptPath, 'references_' + str(self.sg_project['code']) + str(clip.name()),dictPath)

    def eventHandler(self, event):
        self._selection = None
        if hasattr(event.sender, 'getSelection') and event.sender.getSelection() is not None and len( event.sender.getSelection() ) != 0:
            selection = event.sender.getSelection()
            self._selection = [shot for shot in selection if isinstance(shot,hiero.core.TrackItem)] # Filter out just TrackItems
            if len(selection) > 0:
                title = "OLLIN Create Reference For Selected Shot"
                self.setText(title)
                menu = event.menu
                for i in event.menu.actions():
                    if str(i.text()) == "OLLIN Create Reference For Selected Shot":
                        menu.removeAction(i)
                menu.addAction(self)

    def pa(self):
        pass
      
    def cerrar(self):
        self.msgBox2.close()
        self.mb1.close()

    def valorde_version(self):
        version = int(self.v_line.text())
        self.msgBox2.close()
        self.VERSION = version

    def show_ui(self):
        self.msgBox2 = QMessageBox() 
        self.msgBox2.setText("SET REFERENCE VERSION PLEASE:") 
        lay = self.msgBox2.layout()
        lay.itemAtPosition(lay.rowCount() - 1, 1).widget().hide()
        self.v_line = QLineEdit()
        self.has_cut = QCheckBox('CUT')
        self.has_cut.setChecked(True)
        butt = QPushButton('Ok')
        cancel_b = QPushButton('Cancel')
        butt.clicked.connect(self.valorde_version)
        cancel_b.clicked.connect(self.cerrar)
        lay.addWidget(self.v_line,lay.rowCount() - 1, 1, 1, lay.columnCount())
        lay.addWidget(butt,lay.rowCount(), 1, 1, lay.columnCount())
        lay.addWidget(cancel_b,lay.rowCount(), 0, 1, lay.columnCount())
        lay.addWidget(self.has_cut,lay.rowCount(), 0, 1, lay.columnCount())
        self.msgBox2.exec_()
