from tank.platform import Application
import sgtk
import os
import hou


class Menu_ollin(Application):

    def init_app(self):
        # print 'houdini nodes'
        self.engine.register_command(
            "Alembic publish node",
            self.create_alembicpublish_node,
            {}
        )

        self.engine.register_command(
            "Geo publish node",
            self.create_geo_node,
            {}
        )

        self.engine.register_command(
            "VDB publish node",
            self.create_vdb_node,
            {}
        )

        self.engine.register_command(
            "MANTRA publish node",
            self.create_mantra_node,
            {}
        )

    def validate_node_selection(self):

        node = None
        type = None

        if len(hou.selectedNodes()) == 0 or len(hou.selectedNodes()) > 1:
            node = None
            #raise ValueError("You must select only one camera node")
            hou.ui.displayMessage("You must select only one single node")
        else:
            node = hou.selectedNodes()[0]
            if node.type().name() == 'cam':
                type = 'cam'
            else:
                type = 'geo'

            # Validate node node to avoid underscores
            if '_' in node.name():
                hou.ui.displayMessage('Error: Remove underscores "_" from selected Node!!!')
                node = None
                type = None

        return node, type

    #def create_cam_node(self):
    def create_alembicpublish_node(self):
        ROP = None
        try:
            pub_node, type = self.validate_node_selection()

            if pub_node:
                sceneRoot = hou.node('/obj/')

                # Ask for the nodename input
                #answ = hou.ui.readInput('Name:', buttons=(['OK']), default_choice=0)

                # Get context information
                e_type = self.engine.context.entity['type'].lower()

                w_t = 'houdini_{0}_work'.format(e_type)
                work_template = self.engine.get_template_by_name(w_t)

                if type == 'cam':
                    c_t = 'houdini_{0}_camera_publish'.format(e_type)
                else:
                    c_t = 'houdini_{}_alembic_geo_publish'.format(e_type)

                alembic_template = self.engine.get_template_by_name(c_t)
                print 'cam_template: ', alembic_template
                fields = work_template.get_fields(str(hou.hipFile.path()))
                fields['node'] = pub_node.name()
                context = self.engine.context
                config_path = context.tank.pipeline_configuration.get_path()
                publish_hook = config_path + '/config/hooks/ovfx_houdini_publish.py'
                alembic_path = alembic_template.apply_fields(fields)


                ROP = sceneRoot.createNode('ropnet')
                node_name = '{}_publish'.format(pub_node.name())
                ROP.setName(node_name)
                parm_group = ROP.parmTemplateGroup()
                parm_folder = hou.FolderParmTemplate("folder", "Extras")

                # Custom button to export the current camera to the publish area
                #script_s = 'hou.pwd().node("{}_alembic")'.format(cam.name())
                #script_s += '.parm("execute").pressButton()'

                publish_node_btn = hou.ButtonParmTemplate(
                    "publish_sg", "PUBLISH TO SG",
                    #script_callback='{0}'.format(script_s),
                    script_callback='execfile("{0}")'.format(publish_hook),
                    script_callback_language=hou.scriptLanguage.Python)
                parm_folder.addParmTemplate(publish_node_btn)

                parm_output = hou.StringParmTemplate(
                    "out_path",
                    "Output path",
                    1,
                    default_value=([alembic_path]),
                    naming_scheme=hou.parmNamingScheme.Base1,
                    string_type=hou.stringParmType.FileReference)
                parm_folder.addParmTemplate(parm_output)

                parm_fs = hou.FloatParmTemplate(
                    "frame_range",
                    "Start/Enc/Inc",
                    3,
                    default_expression=('$FSTART','$FEND','1'),
                    default_expression_language=(
                        hou.scriptLanguage.Hscript,
                        hou.scriptLanguage.Hscript,
                        hou.scriptLanguage.Hscript),
                    naming_scheme=hou.parmNamingScheme.Base1
                )
                parm_folder.addParmTemplate(parm_fs)

                parm_group.append(parm_folder)
                ROP.setParmTemplateGroup(parm_group)


                ABC = ROP.createNode('alembic')
                ABC.setName('{}_alembic'.format(pub_node.name()))
                # Set parameter expresion values
                ABC.parm('trange').set(1)
                ABC.parm('f1').setExpression('ch("../frame_range1")')
                ABC.parm('f2').setExpression('ch("../frame_range2")')
                ABC.parm('f3').setExpression('ch("../frame_range3")')
                ABC.parm('filename').setExpression('chs("../out_path")')

                # Lock parameter edting
                ABC.parm('trange').lock(True)
                ABC.parm('f1').lock(True)
                ABC.parm('f2').lock(True)
                ABC.parm('f3').lock(True)
                ABC.parm('filename').lock(True)
                #
                ABC.parm('objects').set(pub_node.name())
                #ABC.parm('execute').pressButton()

                message = 'Alembic publish node created: {}.'.format('{}_alembic'.format(pub_node.name()))
                hou.ui.displayMessage(message)
        except:
            if ROP:
                ROP.destroy()
            import traceback
            print traceback.format_exc()
            raise ValueError(str(traceback.format_exc()))

    def create_geo_node(self):

        pub_node, type = self.validate_node_selection()
        #
        if pub_node:
            # Getting templates
            e_type = self.engine.context.entity['type'].lower()
            work_s = 'houdini_{0}_work'.format(e_type)
            geo_w = 'houdini_{0}_geo'.format(e_type)
            geo_s = 'houdini_{0}_geo_publish'.format(e_type)

            # Tamplates!
            work_template = self.engine.get_template_by_name(work_s)
            geo_template_work = self.engine.get_template_by_name(geo_w)
            geo_template_pub = self.engine.get_template_by_name(geo_s)

            # print hou.hipFile.path()

            fields = work_template.get_fields(str(hou.hipFile.path()))
            #answ = hou.ui.readInput('Name:', buttons=(['OK']), default_choice=0)
            #nodeName = answ[1]

            config_path = self.engine.context.tank.pipeline_configuration
            config_path = config_path.get_path()

            dl_render_s = '/config/hooks/ovfx_houdini_dl_render.py'
            hou_publish = '/config/hooks/ovfx_houdini_publish.py'

            render_hook = config_path + dl_render_s
            publish_hook = config_path + hou_publish

            fields['node'] = pub_node.name()
            geo_path = geo_template_work.apply_fields(fields)
            self.engine.ensure_folder_exists(os.path.dirname(geo_path))

            editors = []
            for pane in hou.ui.paneTabs():
                if isinstance(pane, hou.NetworkEditor) and pane.isCurrentTab():
                    editors.append(pane)
            editors[-1].pwd().path()

            locacion = hou.node(editors[-1].pwd().path())
            geo = locacion.createNode('rop_geometry')
            geo.setName('{}_publish'.format(pub_node.name()))
            geo.setInput(0, pub_node)
            parm_group = geo.parmTemplateGroup()
            parm_folder = hou.FolderParmTemplate("folder", "Extras")

            script_l = hou.scriptLanguage.Python

            path_geo = geo_path.split('/houdini/geo')[0]
            path_geo += '/houdini/geo'
            loc = 'import os\nif not os.path.exists("{0}"):\n'.format(path_geo)
            loc += '    os.mkdir("{0}")\n'.format(path_geo)
            loc += '    os.chmod("{0}", 0777)\n'.format(path_geo)
            localC = loc + 'hou.pwd().parm("execute").pressButton()'

            geo_cmds = {'localCash': localC,
                        'cDeadline': '{0}execfile("{1}")'.format(loc, render_hook),
                        'publish': 'execfile("{0}")'.format(publish_hook)}

            btn_1 = hou.ButtonParmTemplate("RENDER", "LOCAL CASH",
                                           script_callback=geo_cmds['localCash'],
                                           script_callback_language=script_l)

            btn_2 = hou.ButtonParmTemplate("R_in_dl", "CASH IN DEADLINE",
                                           script_callback=geo_cmds['cDeadline'],
                                           script_callback_language=script_l)
            btn_3 = hou.ButtonParmTemplate("publish_sg", "PUBLISH TO SG",
                                           script_callback=geo_cmds['publish'],
                                           script_callback_language=script_l)

            parm_folder.addParmTemplate(btn_1)
            parm_folder.addParmTemplate(btn_2)
            parm_folder.addParmTemplate(btn_3)
            parm_group.append(parm_folder)
            geo.setParmTemplateGroup(parm_group)
            geo.setParms({'trange': 1})
            geo.setParms({'sopoutput': geo_path})

            message = 'GEO publish node created: {}.'.format('{}_publish'.format(pub_node.name()))
            hou.ui.displayMessage(message)

    def create_vdb_node(self):
        e_type = self.engine.context.entity['type'].lower()

        # Getting templates
        work_s = 'houdini_{0}_work'.format(e_type)
        vdb_w = 'houdini_{0}_vdb'.format(e_type)
        vdb_p = 'houdini_{0}_vdb_publish'.format(e_type)
        work_template = self.engine.get_template_by_name(work_s)
        vdb_template_work = self.engine.get_template_by_name(vdb_w)
        vdb_template_pub = self.engine.get_template_by_name(vdb_p)

        # print str(hou.hipFile.path())
        fields = work_template.get_fields(str(hou.hipFile.path()))
        answ = hou.ui.readInput('Name:', buttons=(['OK']), default_choice=0)
        nodeName = answ[1] + '_publish'
        config_path = self.engine.context.tank.pipeline_configuration
        config_path = config_path.get_path()

        dl_render_s = '/config/hooks/ovfx_houdini_dl_render.py'
        hou_publish = '/config/hooks/ovfx_houdini_publish.py'
        render_hook = config_path + dl_render_s
        publish_hook = config_path + hou_publish

        fields['node'] = answ[1]
        # print fields
        vdb_path = vdb_template_work.apply_fields(fields)
        self.engine.ensure_folder_exists(os.path.dirname(vdb_path))

        hNet = hou.NetworkEditor
        hPan = hou.ui.paneTabs()
        editors = [p for p in hPan if isinstance(p, hNet) and p.isCurrentTab()]

        editors[-1].pwd().path()
        locacion = hou.node(editors[-1].pwd().path())
        nodito = locacion.createNode('subnet')
        nodito.setName(nodeName)
        parm_group = nodito.parmTemplateGroup()
        parm_folder = hou.FolderParmTemplate("folder", "Extras")
        str_param = hou.StringParmTemplate(
            "out_path", "Output path", 1, default_value=([vdb_path]),
            naming_scheme=hou.parmNamingScheme.Base1,
            string_type=hou.stringParmType.FileReference)
        parm_folder.addParmTemplate(str_param)

        parm_fs = hou.FloatParmTemplate(
            "frame_range",
            "Start/Enc/Inc",
            3,
            default_expression=('$FSTART', '$FEND', '1'),
            default_expression_language=(
                hou.scriptLanguage.Hscript,
                hou.scriptLanguage.Hscript,
                hou.scriptLanguage.Hscript),
            naming_scheme=hou.parmNamingScheme.Base1
        )
        parm_folder.addParmTemplate(parm_fs)

        #parm_folder.addParmTemplate(hou.IntParmTemplate("F_R",
        #              "Frame Range", 2,default_value=([1,2])))
        print '****: ', nodeName
        s_tmp = 'hou.pwd().node("{0}").parm("execute").pressButton()'.format(nodeName)

        vdb_cmds = {'localCash': s_tmp,
                    'cDeadline': 'execfile("{0}")'.format(render_hook),
                    'publish': 'execfile("{0}")'.format(publish_hook)}

        script_l = hou.scriptLanguage.Python


        btn_1 = hou.ButtonParmTemplate("RENDER", "LOCAL CASH",
                                       script_callback=vdb_cmds['localCash'],
                                       script_callback_language=script_l)

        btn_2 = hou.ButtonParmTemplate("R_in_dl", "CASH IN DEADLINE",
                                       script_callback=vdb_cmds['cDeadline'],
                                       script_callback_language=script_l)


        btn_3 = hou.ButtonParmTemplate("publish_sg", "PUBLISH TO SG",
                                       script_callback=vdb_cmds['publish'],
                                       script_callback_language=script_l)
        parm_folder.addParmTemplate(btn_1)
        parm_folder.addParmTemplate(btn_2)
        parm_folder.addParmTemplate(btn_3)

        parm_group.append(parm_folder)
        nodito.setParmTemplateGroup(parm_group)
        cvdb = nodito.createNode('convertvdb')
        cvdb.setParms({'conversion': 1})
        cvdb.setInput(0, nodito.indirectInputs()[0])
        vdb_vm = nodito.createNode('vdbvectormerge')
        vdb_vm.setInput(0, cvdb)
        vdb_vm.setPosition([vdb_vm.position()[0], cvdb.position()[1] - 1])
        rop = nodito.createNode('rop_geometry')
        rop.setPosition([rop.position()[0], vdb_vm.position()[1] - 1])
        rop.setInput(0, vdb_vm)
        rop.setParms({'trange': 1})
        rop.parm('f1').setExpression('ch("../frame_range1")')
        rop.parm('f2').setExpression('ch("../frame_range2")')
        rop.parm('f3').setExpression('ch("../frame_range3")')
        rop.parm('sopoutput').setExpression('chs("../out_path")')
        rop.setName(nodeName)

        message = 'VDB publish node created: {}.'.format(nodeName)
        hou.ui.displayMessage(message)

    def create_mantra_node(self):
        e_type = self.engine.context.entity['type'].lower()

        # Getting templates
        work_s = 'houdini_{0}_work'.format(e_type)
        mantra_s = 'houdini_{0}_render'.format(e_type)
        work_template = self.engine.get_template_by_name(work_s)
        mantra_template = self.engine.get_template_by_name(mantra_s)

        # print str(hou.hipFile.path())
        fields = work_template.get_fields(str(hou.hipFile.path()))
        #print fields
        config_path = self.engine.context.tank.pipeline_configuration
        config_path = config_path.get_path()

        dl_render_s = '/config/hooks/ovfx_houdini_dl_render.py'
        hou_publish = '/config/hooks/ovfx_houdini_publish.py'
        render_hook = config_path + dl_render_s
        publish_hook = config_path + hou_publish
        # get height an width
        cam_node = None
        if len(hou.selectedNodes()) == 1:
            cam_node = hou.selectedNodes()[0]
            if 'Object cam' in str(cam_node.type()):
                fields['width'] = cam_node.parm('resx').eval()
                fields['height'] = cam_node.parm('resy').eval()
            else:
                hou.ui.displayMessage("ERROR:Please select a cam Node !!!!")
                return
        else:
            hou.ui.displayMessage("ERROR: Please select just one Node !!!!")
            return

        fields['node'] = cam_node.name()
        print fields

        mantra_path = mantra_template.apply_fields(fields)
        locacion = hou.node('/out/')

        node_name = "{}_mantra_ovfx".format(cam_node.name())

        mantra_node = locacion.createNode('ifd')
        mantra_node.parm("vm_picture").set(mantra_path)
        mantra_node.parm("camera").set(hou.selectedNodes()[0].path())
        mantra_node.setName(node_name)

        parm_group = mantra_node.parmTemplateGroup()
        parm_folder = hou.FolderParmTemplate("folder", "Ollin extras")
        str_param = hou.StringParmTemplate(
            "out_path", "Output path", 1, default_value=([mantra_path]),
            naming_scheme=hou.parmNamingScheme.Base1,
            string_type=hou.stringParmType.FileReference)
        parm_folder.addParmTemplate(str_param)
        parm_fs = hou.FloatParmTemplate(
            "frame_range",
            "Start/Enc/Inc",
            3,
            default_expression=('$FSTART', '$FEND', '1'),
            default_expression_language=(
                hou.scriptLanguage.Hscript,
                hou.scriptLanguage.Hscript,
                hou.scriptLanguage.Hscript),
            naming_scheme=hou.parmNamingScheme.Base1
        )
        parm_folder.addParmTemplate(parm_fs)
        mantra_cmds = {'cDeadline': 'execfile("{0}")'.format(render_hook),
                       'publish': 'execfile("{0}")'.format(publish_hook)}
        script_l = hou.scriptLanguage.Python
        btn_2 = hou.ButtonParmTemplate("R_in_dl", "CASH IN DEADLINE",
                                       script_callback=mantra_cmds['cDeadline'],
                                       script_callback_language=script_l)
        btn_3 = hou.ButtonParmTemplate("publish_sg", "PUBLISH TO SG",
                                       script_callback=mantra_cmds['publish'],
                                       script_callback_language=script_l)
        h_w = hou.IntParmTemplate(
            "Height_and_width",
            "Height/Width",
            2,
            default_expression=(str(fields['height']), str(fields['width'])),
            default_expression_language=(
                hou.scriptLanguage.Hscript,
                hou.scriptLanguage.Hscript),
            naming_scheme=hou.parmNamingScheme.Base1
        )
        parm_folder.addParmTemplate(h_w)
        parm_folder.addParmTemplate(btn_2)
        parm_folder.addParmTemplate(btn_3)
        parm_group.append(parm_folder)
        mantra_node.setParmTemplateGroup(parm_group)
        hou.ui.displayMessage("New mantra node /out/{0} created!!".format(mantra_node.name()))
