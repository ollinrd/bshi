# Copyright (c) 2017 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import sgtk
import tempfile
import datetime
from time import gmtime, strftime
from sgtk.platform.qt import QtCore, QtGui
import subprocess

from .ui.dialog import Ui_Dialog

logger = sgtk.platform.get_logger(__name__)

overlay = sgtk.platform.import_framework("tk-framework-qtwidgets", "overlay_widget")
sg_data = sgtk.platform.import_framework("tk-framework-shotgunutils", "shotgun_data")
task_manager = sgtk.platform.import_framework("tk-framework-shotgunutils", "task_manager")


class Dialog(QtGui.QWidget):
    """
    Main dialog window for the App
    """

    (DATA_ENTRY_UI, UPLOAD_COMPLETE_UI) = range(2)

    def __init__(self,nuke_review_node,  parent=None):
        """
        :param nuke_review_node: Selected nuke gizmo to render.
        :param parent: The parent QWidget for this control
        """
        QtGui.QWidget.__init__(self, parent)

        self._bundle = sgtk.platform.current_bundle()
        self.nuke_review_node = nuke_review_node
        self._context = self._bundle.context
        self._title = self._generate_title()

        self._task_manager = task_manager.BackgroundTaskManager(
            parent=self,
            start_processing=True,
            max_threads=2
        )
        # set up data retriever
        self.__sg_data = sg_data.ShotgunDataRetriever(
            self,
            bg_task_manager=self._task_manager
        )
        self.__sg_data.work_completed.connect(self.__on_worker_signal)
        self.__sg_data.work_failure.connect(self.__on_worker_failure)
        self.__sg_data.start()

        # set up the UI
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.context_widget.set_up(self._task_manager)
        self.ui.context_widget.set_context(self._context)
        self.ui.context_widget.context_label.hide()
        self.ui.context_widget.restrict_entity_types_by_link("Version", "entity")

        self.ui.context_widget.context_changed.connect(self._on_context_change)

        self._overlay = overlay.ShotgunOverlayWidget(self)
        self.ui.submit.clicked.connect(self._submit)
        self.ui.cancel.clicked.connect(self.close)

        # set up basic UI
        self.ui.version_name.setText(self._title)
        self.ui.start_frame.setText(str(self._get_first_frame()))
        self.ui.end_frame.setText(str(self._get_last_frame()))

        self._setup_playlist_dropdown()


    def _setup_playlist_dropdown(self):
        """
        Sets up the playlist dropdown widget
        """
        self.ui.playlists.setToolTip(
            "<p>Shows the 10 most recently updated playlists for "
            "the project that have a viewing date "
            "set to the future.</p>"
        )

        self.ui.playlists.addItem("Add to playlist", 0)

        from tank_vendor.shotgun_api3.lib.sgtimezone import LocalTimezone
        datetime_now = datetime.datetime.now(LocalTimezone())

        playlists = self._bundle.shotgun.find(
            "Playlist",
            [
                ["project", "is", self._bundle.context.project],
                {
                    "filter_operator": "any",
                    "filters": [
                        ["sg_date_and_time", "greater_than", datetime_now],
                        ["sg_date_and_time", "is", None]
                    ]
                }
            ],
            ["code", "id", "sg_date_and_time"],
            order=[{"field_name": "updated_at", "direction": "desc"}],
            limit=10,
        )

        for playlist in playlists:

            if playlist.get("sg_date_and_time"):
                # 'Add to playlist dailies (Today 12:00)'
                caption = "%s (%s)" % (
                    playlist["code"],
                    self._format_timestamp(playlist["sg_date_and_time"])
                )
            else:
                caption = playlist["code"]

            self.ui.playlists.addItem(caption, playlist["id"])

    def _format_timestamp(self, datetime_obj):
        """
        Formats the given datetime object in a short human readable form.

        :param datetime_obj: Datetime obj to format
        :returns: date str
        """
        from tank_vendor.shotgun_api3.lib.sgtimezone import LocalTimezone
        datetime_now = datetime.datetime.now(LocalTimezone())

        datetime_tomorrow = datetime_now + datetime.timedelta(hours=24)

        if datetime_obj.date() == datetime_now.date():
            # today - display timestamp - Today 01:37AM
            return datetime_obj.strftime("Today %I:%M%p")

        elif datetime_obj.date() == datetime_tomorrow.date():
            # tomorrow - display timestamp - Tomorrow 01:37AM
            return datetime_obj.strftime("Tomorrow %I:%M%p")

        else:
            # 24 June 01:37AM
            return datetime_obj.strftime("%d %b %I:%M%p")

    def closeEvent(self, event):
        """
        Executed when the dialog is closed.
        """
        try:
            self.ui.context_widget.save_recent_contexts()
            self.__sg_data.stop()
            self._task_manager.shut_down()
        except Exception:
            logger.exception("Error running Loader App closeEvent()")

        # okay to close dialog
        event.accept()

    def _get_first_frame(self):
        """
        Returns the first frame for this session
        """
        import nuke
        return int(nuke.root()["first_frame"].value())

    def _get_last_frame(self):
        """
        Returns the last frame for this session
        """
        import nuke
        return int(nuke.root()["last_frame"].value())

    def _generate_title(self):
        """
        Create a title for the version
        """
        return self._bundle.execute_hook_method(
            "settings_hook",
            "get_title",
            context=self._context,
            base_class=self._bundle.base_hooks.ReviewSettings
        )

    def _setup_formatting(self, sg_version_name):
        """
        Sets up slates and burnins

        :param str sg_version_name: Name of the version.
        """
        # set the fonts for all text fields
        font = os.path.join(self._bundle.disk_location, "resources", "OpenSans-Regular.ttf")
        font = font.replace(os.sep, "/")
        self._group_node.node("top_left_text")["font"].setValue(font)
        self._group_node.node("top_right_text")["font"].setValue(font)
        self._group_node.node("bottom_left_text")["font"].setValue(font)
        self._group_node.node("framecounter")["font"].setValue(font)
        self._group_node.node("slate_info")["font"].setValue(font)

        # get burnins and slate info from hook
        fields_dict = self._bundle.execute_hook_method(
            "settings_hook",
            "get_burnins_and_slate",
            sg_version_name=sg_version_name,
            context=self._context,
            base_class=self._bundle.base_hooks.ReviewSettings
        )

        # set up burnins
        self._group_node.node("top_left_text")["message"].setValue(fields_dict["top_left"])
        self._group_node.node("top_right_text")["message"].setValue(fields_dict["top_right"])
        self._group_node.node("bottom_left_text")["message"].setValue(fields_dict["bottom_left"])
        # note: bottom right is used as a frame counter.

        # set up slate
        self._group_node.node("slate_info")["message"].setValue("\n".join(fields_dict["slate"]))

    @sgtk.LogManager.log_timing

    def _render(self, mov_path, start_frame, end_frame, comments, version_name):
        """
        Renders write node

        :param mov_path: temporary path where quicktime should be written
        :param int start_frame: First frame to render
        :param int end_frame: Last frame to render
        """
        import nuke
        sg = self._bundle.shotgun
        task = sg.find_one("Task",
                           [["id", "is", self._context.task["id"]]],
                           ["task_assignees", "step"])
        user = task["task_assignees"][0]
        artist = user['name']
        prefixx = os.path.basename(mov_path).split('.')[0]
        (file, mov_path) = tempfile.mkstemp(prefix='/tmp/' + prefixx , suffix='.mov')
        if self._context.entity['type'] == 'Shot':
            fields = ['code', 'sg_head_in','sg_tail_out', 'sg_lens_focal_length',
                      'sg_external_id', 'sg_in_timecode', 'sg_camera',
                      'sg_offset_frame', 'sg_resx', 'sg_resy', 'sg_without_lut',
                      'sg_str_timecode', 'sg_lut', 'sg_sequence']
        else:
            fields = ['code']
        entity = sg.find_one(self._context.entity['type'],
                             [['id', 'is', self._context.entity['id']]],
                             fields)
        mov_path = mov_path.replace(os.sep, "/")
        date_aux = strftime("%Y-%m-%d", gmtime()).split("-")
        date = date_aux[0] + " / " + date_aux[1] + " / " + date_aux[2]
        date_aux = strftime("%Y-%m-%d", gmtime()).split("-")
        date = date_aux[0] + " / " + date_aux[1] + " / " + date_aux[2] + ' - '
        date_aux2 = str(datetime.datetime.now().time()).split('.')[0].split(':')
        date += date_aux2[0] + ':' + date_aux2[1]
        nk_name = os.path.basename(nuke.root()['name'].value()).replace('.nk', '_quick_review.nk')
        total_f = int(end_frame) - int(start_frame) + 1
        burn = self.nuke_review_node
        if entity['id'] in [12986]:
            burn.node('internal').node("OCIOColorSpace2")['disable'].setValue(False)
            burn.node('internal').node('write_avid1')['raw'].setValue(True)
        burn.node('internal').node("Switch")
        burn.node('internal').node("Switch")["which"].setValueAt(0, start_frame - 1)
        burn.node('internal').node("Switch")["which"].setValueAt(1, start_frame)
        burn.node('internal').node('label_comments').knob('message').setValue(comments)
        burn.node('internal').node('label_date_time').knob('message').setValue(date)
        burn.node('internal').node('label_script_name').knob('message').setValue('Script: {0}'.format(nk_name.split('.')[0]))
        burn.node('internal').node('label_sequence').knob('message').setValue('')
        burn.node('internal').node('label_internal').knob('message').setValue(entity['code'])
        burn.node('internal').node('label_artist').knob('message').setValue('Artist: {0}'.format(artist))
        burn.node('internal').node('label_frames').knob('message').setValue(str(total_f))
        burn.node('internal').node('label_date_times1').knob('message').setValue('{0}'.format(date))
        burn.node('internal').node('label_version1').knob('message').setValue(nk_name.split('.')[0])
        burn.node('internal').node('label_artistname').knob('message').setValue(artist)
        burn.node('internal').node('label_project').knob('message').setValue('BSHI')
        timeco = '00:00:00:00'
        set_time = burn.node("internal").node("AddTimeCode2")
        set_time['startcode'].setValue(timeco)
        set_time['frame'].setValue(start_frame - 1)

        burn.node('internal').node('write_avid1').knob('file').setValue(mov_path)
        tempNK = os.path.dirname(nuke.root()['name'].value()) + '/.QR_' + os.path.basename(nuke.root()['name'].value())
        import sys
        reload(sys)
        sys.setdefaultencoding("utf-8")
        data_sc = {'py_path': self._bundle.engine.context.tank.pipeline_configuration.get_path(),
                   'task': task,
                   'tempNK': tempNK,
                   'burn': burn.name(),
                   'st_fr': start_frame,
                   'end_fr': end_frame,
                   'user': user,
                   'entity': entity,
                   'project': self._context.project,
                   'description': comments.encode("utf8"),
                   'version_name': version_name,
                   'mov_path': mov_path,
                   'h_res': entity['sg_resx'],
                   'v_res': entity['sg_resy'],
                   }
        py_scr = """#!/usr/bin/env python
# -*- coding: utf-8 -*-
import nuke
import sys
import os
sys.path.append('{py_path}/install/core/python')
import sgtk
tk_nuke = sgtk.platform.current_engine()
if not tk_nuke:
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    cdm = sgtk.util.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.get_user()
    sgtk.set_authenticated_user(user)
    task_related = {task}
    tk = sgtk.sgtk_from_entity(task_related['type'], task_related['id'])    
    ctx = tk.context_from_entity(task_related['type'], task_related['id'])    
    sg = tk.shotgun
    tk_nuke = sgtk.platform.start_engine("tk-nuke", tk, ctx)
nuke.root()['colorManagement'].setValue('OCIO')
nuke.root()['OCIO_config'].setValue('aces_1.0.3')
nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
nuke.root()['int16Lut'].setValue('ACES - ACEScc')
nuke.root()['logLut'].setValue('Input - ADX - ADX10')
nuke.root()['floatLut'].setValue('ACES - ACES2065-1')
nuke.nodePaste('{tempNK}')
nuke.nodePaste('{tempNK}')
currentFormat = '{h_res} {v_res} 1 BSHI'
currentFormat = nuke.addFormat(currentFormat)
nuke.root()['format'].setValue(currentFormat)
w_node = nuke.toNode('{burn}').node('internal').node('write_avid1')
nuke.executeMultiple([w_node], ([{st_fr} - 1, {end_fr}, 1],),[nuke.views()[0]])
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
data = {{
        "code": '{version_name}',
        "description": '''{description}'''.encode("utf8"),
        "project": {project},
        "entity": {entity},
        "sg_task": {task},
        "created_by": {user},
        "user": {user},
        "sg_first_frame": {st_fr},
        "sg_last_frame": {end_fr},
        "frame_count": {end_fr} - {st_fr} + 1,
        "frame_range": "%d-%d" % ({st_fr} - 1, {end_fr}),
        "sg_movie_has_slate": True,
        'sg_version_type': 'Quick Daily',
        'sg_status_list': 'na'
    }}
version = tk_nuke.shotgun.create('Version', data)
tk_nuke.shotgun.upload(
                "Version",
                version["id"],
                '{mov_path}',
                "sg_uploaded_movie"
            )
if '/nfs/ollinvfx/Project/BSHI/' in '{tempNK}':
    os.system('rm {tempNK}')
""".format(**data_sc)
        prefixx = '/nfs/ovfxToolkit/TMP/'
        (file, tempPath) = tempfile.mkstemp(prefix=prefixx + nk_name.replace('.nk', ''), suffix='.py')
        submitter_py = open(tempPath, "w")
        submitter_py.write(py_scr)
        submitter_py.close()
        nuke.scriptSave()
        os.system('rsync -avh {0} {1}'.format(nuke.root()['name'].value(), tempNK))
        print tempPath
        bndl = self._bundle.engine.context.tank.pipeline_configuration.get_path()
        flags = '" -t {0} {1} {2}"'.format(tempPath, tempNK, bndl)

        # flags += ' <QUOTE>' + tempPath + '<QUOTE> {0} <STARTFRAME%4>,<STARTFRAME%4>"'.format(bndl, nuke.root()['name'].value())
        nuke_cmd = "/opt/Thinkbox/Deadline10/bin/deadlinecommand -SubmitCommandLineJob -executable"
        nuke_cmd += " /nfs/ovfxToolkit/Software/Nuke12.0v3/Nuke12.0 -arguments "
        nuke_cmd += flags
        nuke_cmd += ' -pool publish'
        nuke_cmd += ' -name "Quick review render"'
        nuke_cmd += " -prop BatchName={0}".format(nk_name)
        print nuke_cmd
        process = subprocess.Popen(nuke_cmd, 
                                   shell=True, stdout=subprocess.PIPE)
        out, err = process.communicate()
        print out, err


    def _navigate_panel_and_close(self, panel_app, version_id):
        """
        Navigates to the given version in the given panel app
        and then closes this window.

        :param panel_app: Panel app instance to navigate.
        :prarm int version_id: Version id to navigate to
        """
        self.close()
        panel_app.navigate("Version", version_id, panel_app.PANEL)

    def _navigate_sg_and_close(self, version_id):
        """
        Navigates to the given version in shotgun and closes
        the window.

        :prarm int version_id: Version id to navigate to
        """
        self.close()
        # open sg media center playback overlay page
        url = "%s/page/media_center?type=Version&id=%d" % (
            self._bundle.sgtk.shotgun.base_url,
            version_id
        )
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def _on_context_change(self, context):
        """
        Called when user selects a new context

        :param context: Context which was selected
        """
        logger.debug("Setting version context to %s" % context)
        self._context = context
        self._title = self._generate_title()
        self.ui.version_name.setText(self._title)

    def _submit(self):
        """
        Submits the render for review.
        """
        try:
            self._overlay.start_spin()
            self._run_submission()
        except Exception, e:
            logger.exception("An exception was raised.")
            self._overlay.show_error_message("An error was reported: %s" % e)

    def _upload_to_shotgun(self, shotgun, data):
        """
        Upload quicktime to Shotgun.

        :param shotgun: Shotgun API instance
        :param: parameter dictionary
        """
        logger.debug("Uploading movie to Shotgun...")
        try:
            shotgun.upload(
                "Version",
                data["version_id"],
                data["file_name"],
                "sg_uploaded_movie"
            )
            logger.debug("...Upload complete!")
        finally:
            sgtk.util.filesystem.safe_delete_file(data["file_name"])

    def _run_submission(self):
        """
        Carry out the render and upload.
        """
        # get inputs - these come back as unicode so make sure convert to utf-8
        version_name = self.ui.version_name.text()
        if isinstance(version_name, unicode):
            version_name = version_name.encode("utf-8")
        version_basename = version_name.split(',')[0].replace(' ', '_')
        date_aux2 = version_name.split(',')[1].split(' ')[1].split('-')
        timestamp = date_aux2[0][:-2] + date_aux2[1] + date_aux2[2]
        time_ = version_name.split(' ')[-1].split(':')
        version_name = version_basename + '_' + timestamp + time_[0] + time_[1]
        print 'version_name: ', version_name
        description = self.ui.description.toPlainText()
        if isinstance(description, unicode):
            description = description.encode("utf-8")

        # generate temp file for mov sequence
        mov_path = os.path.join(tempfile.gettempdir(), "quickreview.mov")
        mov_path = sgtk.util.filesystem.get_unused_path(mov_path)

        # get frame ranges from ui
        try:
            start_frame = int(self.ui.start_frame.text())
            end_frame = int(self.ui.end_frame.text())
        except Exception, e:
            raise ValueError("Could not determine frame range values from UI.")
        import nuke
        # and render!
        self._render(mov_path, start_frame, end_frame, description, version_name)
        nuke.message('Quick review now is in deadline !')
        self.close()

    def __on_worker_failure(self, uid, msg):
        """
        Asynchronous callback - the worker thread errored.
        """
        self._overlay.show_error_message("An error was reported: %s" % msg)
        self.ui.submit.hide()
        self.ui.cancel.setText("Close")

    def __on_worker_signal(self, uid, request_type, data):
        """
        Signaled whenever the worker completes something.
        """
        # call post hook - note that we don't do this in
        # the thread because calls to the nuke API sometimes
        # crash when executed from a thread, so for maximum
        # safety and stability, call post hook from main thread.


        # hide spinner
        self._overlay.hide()

        # show success screen
        self.ui.stack_widget.setCurrentIndex(self.UPLOAD_COMPLETE_UI)
        self.ui.jump_to_panel.hide()
        # show 'jump to panel' button if we have panel loaded
        found_panel = False
        nuke.message('Quick review now is in deadline !')
        self.close()
