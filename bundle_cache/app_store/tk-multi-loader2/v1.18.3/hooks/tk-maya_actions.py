# Copyright (c) 2015 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

"""
Hook that loads defines all the available actions, broken down by publish type. 
"""

import glob
import os
import re
import pymel.core as pm
import maya.cmds as cmds
import maya.mel as mel
import sgtk
import json
import sys

HookBaseClass = sgtk.get_hook_baseclass()


class MayaActions(HookBaseClass):
    
    ##############################################################################################################
    # public interface - to be overridden by deriving classes 
    
    def generate_actions(self, sg_publish_data, actions, ui_area):
        """
        Returns a list of action instances for a particular publish.
        This method is called each time a user clicks a publish somewhere in the UI.
        The data returned from this hook will be used to populate the actions menu for a publish.
    
        The mapping between Publish types and actions are kept in a different place
        (in the configuration) so at the point when this hook is called, the loader app
        has already established *which* actions are appropriate for this object.
        
        The hook should return at least one action for each item passed in via the 
        actions parameter.
        
        This method needs to return detailed data for those actions, in the form of a list
        of dictionaries, each with name, params, caption and description keys.
        
        Because you are operating on a particular publish, you may tailor the output 
        (caption, tooltip etc) to contain custom information suitable for this publish.
        
        The ui_area parameter is a string and indicates where the publish is to be shown. 
        - If it will be shown in the main browsing area, "main" is passed. 
        - If it will be shown in the details area, "details" is passed.
        - If it will be shown in the history area, "history" is passed. 
        
        Please note that it is perfectly possible to create more than one action "instance" for 
        an action! You can for example do scene introspection - if the action passed in 
        is "character_attachment" you may for example scan the scene, figure out all the nodes
        where this object can be attached and return a list of action instances:
        "attach to left hand", "attach to right hand" etc. In this case, when more than 
        one object is returned for an action, use the params key to pass additional 
        data into the run_action hook.
        
        :param sg_publish_data: Shotgun data dictionary with all the standard publish fields.
        :param actions: List of action strings which have been defined in the app configuration.
        :param ui_area: String denoting the UI Area (see above).
        :returns List of dictionaries, each with keys name, params, caption and description
        """
        app = self.parent
        app.log_debug("Generate actions called for UI element %s. "
                      "Actions: %s. Publish Data: %s" % (ui_area, actions, sg_publish_data))
        
        action_instances = []

        configs_dict = {
            'reference': {
                'name': 'reference', 'params': None,
                'caption': 'Create Reference',
                'description': 'This will add the item to the scene as a standard reference.'
                },
            'import': {
                'name': 'import', 'params': None,
                'caption': 'Import into Scene',
                'description': 'This will import the item into the current scene.'
                },
            'texture_node': {
                'name': 'texture_node', 'params': None,
                'caption': 'Create Texture Node',
                'description': 'Creates a file texture node for the selected item..'
                },
            'udim_texture_node': {
                'name': 'udim_texture_node', 'params': None,
                'caption': 'Create Texture Node',
                'description': 'Creates a file texture node for the selected item..'
                },
            'image_plane': {
                'name': 'image_plane', 'params': None,
                'caption': 'Create Image Plane',
                'description': 'Creates an image plane for the selected item..'
                },
            'import_and_connect': {
                'name': 'import_and_connect', 'params': None,
                'caption': 'Import and connect',
                'description': 'Import materials and connect them.'
                },
            'reference_and_connect': {
                'name': 'reference_and_connect', 'params': None,
                'caption': 'Reference and connect',
                'description': 'Reference materials and connect them.'
                },
            'connect_mat': {
                'name': 'connect_mat', 'params': None,
                'caption': 'Connect materials',
                'description': 'Connect materials to geometry.'
                },
            'import_vdb': {
                'name': 'import_vdb', 'params': None,
                'caption': 'Import VDB Sequence',
                'description': 'Imports a VDB volume into scene..'
                },
            'import_ass_seq':{
                'name': 'import_ass_seq', 'params': None,
                'caption': 'Import ass Sequence',
                'description': 'Imports a Ass sequence into scene..'
                }
        }

        for key in configs_dict.keys():
            if key in actions:
                # Special cases
                if key == 'udim_texture_node':
                    if self._get_maya_version() >= 2015:
                        action_instances.append(configs_dict[key])
                else:
                    action_instances.append(configs_dict[key])

        return action_instances

    def execute_multiple_actions(self, actions):
        """
        Executes the specified action on a list of items.

        The default implementation dispatches each item from ``actions`` to
        the ``execute_action`` method.

        The ``actions`` is a list of dictionaries holding all the actions to execute.
        Each entry will have the following values:

            name: Name of the action to execute
            sg_publish_data: Publish information coming from Shotgun
            params: Parameters passed down from the generate_actions hook.

        .. note::
            This is the default entry point for the hook. It reuses the ``execute_action``
            method for backward compatibility with hooks written for the previous
            version of the loader.

        .. note::
            The hook will stop applying the actions on the selection if an error
            is raised midway through.

        :param list actions: Action dictionaries.
        """
        for single_action in actions:
            name = single_action["name"]
            sg_publish_data = single_action["sg_publish_data"]
            params = single_action["params"]
            self.execute_action(name, params, sg_publish_data)

    def execute_action(self, name, params, sg_publish_data):
        """
        Execute a given action. The data sent to this be method will
        represent one of the actions enumerated by the generate_actions method.

        :param name: Action name string representing one of the items returned by generate_actions.
        :param params: Params data, as specified by generate_actions.
        :param sg_publish_data: Shotgun data dictionary with all the standard publish fields.
        :returns: No return value expected.
        """
        app = self.parent
        app.log_debug("Execute action called for action %s. "
                      "Parameters: %s. Publish Data: %s" % (name, params, sg_publish_data))

        # resolve path
        # toolkit uses utf-8 encoded strings internally and Maya API expects unicode
        # so convert the path to ensure filenames containing complex characters are supported
        path = self.get_publish_path(sg_publish_data).decode("utf-8")

        key_n_methods = {
            'reference': self._create_reference,
            'import': self._do_import,
            'texture_node': self._create_texture_node,
            'udim_texture_node': self._create_udim_texture_node,
            'image_plane': self._create_image_plane,
            'connect_mat': self._connect_materials,
            'import_and_connect': self._import_n_connect,
            'reference_and_connect': self._reference_n_connect,
            'import_vdb': self._do_import_vdb,  
            'import_ass_seq': self.import_ass_seq    
            }

        if name in key_n_methods:
            key_n_methods[name](path, sg_publish_data)
        else:
            app.log_debug('Invalid action called: {0}'.format(name))

    ##############################################################################################################
    # helper methods which can be subclassed in custom hooks to fine tune the behaviour of things

    def _create_reference(self, path, sg_publish_data):
        """
        Create a reference with the same settings Maya would use
        if you used the create settings dialog.
        
        :param path: Path to file.
        :param sg_publish_data: Shotgun data dictionary with all the standard publish fields.
        """
        if not os.path.exists(path):
            raise Exception("File not found on disk - '%s'" % path)
        
        # make a name space out of entity name + publish name
        # e.g. bunny_upperbody
        #namespace = "%s %s" % (sg_publish_data.get("entity").get("name"),
        #                       sg_publish_data.get("name"))
        namespace = sg_publish_data.get("name")
        namespace = namespace.replace(" ", "_")
        pm.system.createReference(path,
                                  loadReferenceDepth="all",
                                  mergeNamespacesOnClash=False,
                                  namespace=namespace)

    def _do_import(self, path, sg_publish_data):
        """
        Create a reference with the same settings Maya would use
        if you used the create settings dialog.
        
        :param path: Path to file.
        :param sg_publish_data: Shotgun data dictionary with all the standard publish fields.
        """
        if not os.path.exists(path):
            raise Exception("File not found on disk - '%s'" % path)

        # make a name space out of entity name + publish name
        # e.g. bunny_upperbody
        namespace = sg_publish_data.get("name")
        namespace = namespace.replace(" ", "_")

        # perform a more or less standard maya import, putting all nodes brought in into a specific namespace
        if sg_publish_data['published_file_type']['name'] == 'Geometry Ass':
            cmds.file(path, i=True, type="ASS", namespace=namespace)
        else:
            print 'importing name space: ', namespace
            cmds.file(path, i=True, renameAll=True, namespace=namespace, loadReferenceDepth="all", preserveReferences=True)

    def _do_import_vdb(self, path, sg_publish_data):
        """
        Create a reference with the same settings Maya would use
        if you used the create settings dialog.

        :param path: Path to file.
        :param sg_publish_data: Shotgun data dictionary with all the standard publish fields.
        """

        dir_path = os.path.dirname(path)
        if os.path.exists(dir_path):
            if not len(os.listdir((dir_path))):
                raise Exception("File not found on disk - '%s'" % path)

        # Get required template for vdb sequence
        if self.parent.engine.context.entity['type'] == 'Shot':
            vdb_template = self.parent.engine.get_template_by_name("houdini_shot_vdb_publish")
        else:
            vdb_template = self.parent.engine.get_template_by_name("houdini_asset_vdb_publish")

        fields = vdb_template.get_fields(path)
        sequece_paths = ['{0}/{1}'.format(dir_path, f) for f in os.listdir((dir_path))]

        # Get the a full frame filename from path
        volume_name = str(fields['node'])

        transform = cmds.createNode('transform', n='{}Transform'.format(volume_name))
        # Create the volume
        volume_node = cmds.createNode('aiVolume', n='{}_vdb'.format(volume_name), p=transform)
        attr = '{0}.filename'.format(volume_node)
        # import the volume file VDB
        cmds.setAttr(attr, sequece_paths[0], type="string")
        attr = '{0}.useFrameExtension'.format(volume_node)
        cmds.setAttr(attr, 1)

    def _create_texture_node(self, path, sg_publish_data):
        """
        Create a file texture node for a texture

        :param path:             Path to file.
        :param sg_publish_data:  Shotgun data dictionary with all the standard publish fields.
        :returns:                The newly created file node
        """
        file_node = cmds.shadingNode('file', asTexture=True)
        cmds.setAttr("%s.fileTextureName" % file_node, path, type="string")
        return file_node

    def _create_udim_texture_node(self, path, sg_publish_data):
        """
        Create a file texture node for a UDIM (Mari) texture
        
        :param path:             Path to file.
        :param sg_publish_data:  Shotgun data dictionary with all the standard publish fields.
        :returns:                The newly created file node
        """
        # create the normal file node:
        file_node = self._create_texture_node(path, sg_publish_data)
        if file_node:
            # path is a UDIM sequence so set the uv tiling mode to 3 ('UDIM (Mari)')
            cmds.setAttr("%s.uvTilingMode" % file_node, 3)
            # and generate a preview:
            mel.eval("generateUvTilePreview %s" % file_node)
        return file_node



    def import_ass_seq(self, path, sg_publish_data):
        from mtoa.core import createStandIn

        path=path
        dir= os.path.dirname(path)
        fileName= os.path.basename(path).split('.')[0]
        files= os.listdir(dir)

        #Filtrar que el firstAss contenga el nombre del archivo

        firstAss= files[0]

        for f in files:
            if fileName in f:
                firstAss=f
                break

        newPath=os.path.join(dir, firstAss)


        #namespace_= os.path.basename(path).split('_')[6] 
        namespace_= 'assFile_seq'
        namespaceList=cmds.namespaceInfo(ls=1)
        nsGroomingList= []

        #Delete empty namespaces

        for ns in namespaceList:
            if namespace_ in ns:
                nsGroomingList.append(ns)

        for nsg in nsGroomingList:
            lod=cmds.namespaceInfo(nsg, lod =1 )
            if not lod:
                cmds.namespace( rm= nsg)
                nsGroomingList.remove(nsg)
                       
        # Importar Archivo
        node= createStandIn()
        cmds.setAttr('{}.dso'.format(node), newPath, type="string")
        cmds.setAttr('{0}.useFrameExtension'.format(node), 1)
        #Validate namespace existance
        if not namespace_ in cmds.namespaceInfo(ls=1):
            cmds.namespace( add='{0}'.format(namespace_) )
    
        cmds.rename('{0}'.format(node), '{0}:{1}'.format(namespace_, node))
    


    

        

    def _create_image_plane(self, path, sg_publish_data):
        """
        Create a file texture node for a UDIM (Mari) texture

        :param path: Path to file.
        :param sg_publish_data: Shotgun data dictionary with all the standard
            publish fields.
        :returns: The newly created file node
        """

        app = self.parent
        has_frame_spec = False

        # replace any %0#d format string with a glob character. then just find
        # an existing frame to use. example %04d => *
        frame_pattern = re.compile("(%0\\dd)")
        frame_match = re.search(frame_pattern, path)
        if frame_match:
            has_frame_spec = True
            frame_spec = frame_match.group(1)
            glob_path = path.replace(frame_spec, "*")
            frame_files = glob.glob(glob_path)
            if frame_files:
                path = frame_files[0]
            else:
                app.logger.error(
                    "Could not find file on disk for published file path %s" %
                    (path,)
                )
                return

        print sg_publish_data
        # Only for JPG lut sequence
        # Compared by id in case of the name changes
        #      Only for publish file type: JPG lut sequenc
        if sg_publish_data['published_file_type']['id'] == 183:
            ########
            ## Julio Cesar
            ########
            shot_name = sg_publish_data['entity']['name']
            self._loadImagePlane_withLut( path, shot_name)
        else:
            # Metodo original!!!!
            # create an image plane for the supplied path, visible in all views
            (img_plane, img_plane_shape) = cmds.imagePlane(
                fileName=path,
                showInAllViews=True
            )
            app.logger.debug(
                "Created image plane %s with path %s" %
                (img_plane, path)
            )

            if has_frame_spec:
                # setting the frame extension flag will create an expression to use
                # the current frame.
                cmds.setAttr("%s.useFrameExtension" % (img_plane_shape,), 1)

    def _loadImagePlane_withLut(self, path, name):
        # Julio Cesar
        # ********
        # TO-DO
        # ********
        # Debe recibir info del nombre del shot nadamas y el path que se va a cargar
        # Se debe hacer un barrido de todas las camaras de la escena y las que 
        # ya tengan un image plane, deben aparecer listadas en el warning
        # las que no (que no sean top, left, right button ni persp)
        # y tengan en el nombre el nombre del shot, entonces ahi se debe aplicar.

        camlist= cmds.ls(type='camera')

        for camShape in camlist:
            # validar si existe el Image Plane en la camara seleccionada
            #camShape = cmds.listRelatives(cam, type='camera', fullPath=True)[0]
            camP= cmds.listRelatives(camShape, p=1)
            cam=''.join(camP)
            ip = cmds.listConnections(camShape, type="imagePlane") or []
            if ip:
                planeName = ip[0].split('>')[1]
                cmds.confirmDialog(m="La camara seleccionada ya tiene a %s Insertado" % planeName, b='Entiendo')
                return
            else:
                plane = cmds.imagePlane(c=cam, n=name+"_cam_imageplane", fn=str(path))
                cmds.setAttr(plane[0]+'.useFrameExtension', 1)
            #Mensaje de buen funcionamiento
            sys.stdout.write('La Aplicacion funciona satisfactoriamente')

    def _import_n_connect(self, path, sg_publish_data):
        """
        Using normal import and the connect_materials methods
        """
        self._do_import(path, sg_publish_data)

        new_path = path.replace('.ma', '.json')

        # new path for relations file!
        self._connect_materials(new_path, sg_publish_data)

        #app.logger.debug(
        #    "Imported materials %s using relations file %s" %
        #    (path, new_path)
        #)
        ttl = 'Import and connect'
        msgg = 'Import and connect\nComplete'
        btns = ['Ok']
        cmds.confirmDialog(title=ttl, message=msgg, button=btns,
                           defaultButton='Ok', dismissString='No')

    def _reference_n_connect(self, path, sg_publish_data):
        self._create_reference(path, sg_publish_data)

        new_path = path.replace('.ma', '.json')
        # new path for relations file!
        self._connect_materials(new_path, sg_publish_data)

        ttl = 'Ref and connect'
        msgg = 'Reference and connect\nComplete'
        btns = ['Ok']
        cmds.confirmDialog(title=ttl, message=msgg, button=btns,
                           defaultButton='Ok', dismissString='No')

    def _connect_materials(self, path, sg_publish_data):
        if not os.path.exists(path):
            raise Exception("File not found on disk - '%s'" % path)

        print("... Starting: Connecting material process...")
        self._feedback_str = ""

        cmds.refresh(suspend=True)

        relations = json.loads(open(path).read())

        #for connection in relations:
        for key in relations.keys():
            connection_info = relations[key]
            #-------------Filter the line
            #divisor = '-::RnD::-'
            #shaderDivisor = '-::Shaders::-'
            #-------------if the line of the connections has the format
            if not 'members' in connection_info:
                self.__connect_geo(key, connection_info)
            else:
                self.__connect_faces(key, connection_info)

        ############# Connections ##### End
        print(self._feedback_str)
        cmds.refresh(suspend=False)
        cmds.refresh()

    def __connect_geo(self, key, connection_info):
        shape_name_1 = key.split(':')[-1].split('|')[-1]
        shape_name_2 = shape_name_1 + 'Deformed'
        shape_name_3 = '_' + shape_name_1

        matched_shapes = pm.ls(shape_name_1, type='shape', r=1)
        matched_shapes += pm.ls(shape_name_2, type='shape', r=1)
        matched_shapes += pm.ls(shape_name_3, type='shape', r=1)

        if 'aiSubdiv' in connection_info.keys():
            for m_shape in matched_shapes:
                aiAtts = connection_info['aiSubdiv']
                for att in aiAtts.keys():
                    m_shape.setAttr(att, aiAtts[att])
        if 'Shader' not in connection_info.keys():
            return

        shader_original = connection_info['Shader']
        shader_name = pm.ls(shader_original, type='shadingEngine', r=1)
        if len(shader_name) == 0:
            str_ = 'Missing shader: {0}'.format(shader_original)
            self._feedback_str += str_
            return

        shader_name = shader_name[0].name()
        for m_shape in matched_shapes:
            shape_name = m_shape.name()
            cmds.sets(shape_name, forceElement=shader_name, e=1)

    def deleteLast(self, cString):
        """
        Auxiliar method
        """
        lastC = cString[len(cString)-1]
        if lastC == " " or lastC == "]":
            cString[len(cString)-1] = ''
            return self.deleteLast(cString=cString)
        else:
            return cString

    def __connect_faces(self, key, connection_info):
        """
        """
        meshElements2 = connection_info['members'].replace("\n", "")

        tmpStr = list(meshElements2)
        tmpStr[0] = ''
        tmpStr = self.deleteLast(cString=tmpStr)
        tmpStr[len(meshElements2)-1] = ''
        meshElements2 = ''.join(tmpStr)

        facesProcess = False

        meshElements2 = meshElements2.split(", ")
        for meshElements in meshElements2:
            #print("\t" + meshElements)
            if '.f[' in meshElements:
                sepElements = meshElements.split("'")
                for spE in sepElements:
                    if ".f[" in spE:
                        self.__connect_faces_process(key, spE)
                        facesProcess = True
            elif 'Mesh' in meshElements:
                mesh_ = meshElements.split("'")[1]
                self.__connect_mesh_process(key, mesh_, connection_info)
            else:
                print("Undefined element", meshElements)

    def __connect_mesh_process(self, key, mesh_, connection_info):
        """
        print("Mesh process")
        print("   Key: ", key)
        print("   Mesh: ", mesh_)
        print("   Connection: ", connection_info)
        """

        shape_name_1 = mesh_.split(':')[-1].split('|')[-1]
        shape_name_2 = shape_name_1 + 'Deformed'
        shape_name_3 = '_' + shape_name_1

        matched_shapes = pm.ls(shape_name_1, type='shape', r=1)
        matched_shapes += pm.ls(shape_name_2, type='shape', r=1)
        matched_shapes += pm.ls(shape_name_3, type='shape', r=1)

        if 'aiSubdiv' in connection_info.keys():
            for m_shape in matched_shapes:
                aiAtts = connection_info['aiSubdiv']
                for att in aiAtts.keys():
                    m_shape.setAttr(att, aiAtts[att])

        if 'Material' in connection_info.keys():
            # Connect with material

            # shader_original = connection_info['Shader']
            mat_original = connection_info['Material']
            mat_ = pm.ls("*:" + mat_original)
            if len(mat_) == 0:
                return
            mat_ = mat_[-1]

            shader_name = pm.listConnections(mat_, t='shadingEngine')
            if len(shader_name) == 0:
                str_ = 'Missing shader: {0}'.format(shader_original)
                self._feedback_str += str_
                return

            shader_name = shader_name[0].name()
            for m_shape in matched_shapes:
                shape_name = m_shape.name()
                cmds.sets(shape_name, forceElement=shader_name, e=1)

    def __connect_faces_process(self,  key, spE):

        shape_name = spE.split('.')[0]
        shape_name2 = shape_name + 'Deformed'
        shape_name3 = '_' + shape_name
        matched_shapes = pm.ls(shape_name, type='shape', r=1)
        matched_shapes += pm.ls(shape_name2, type='shape', r=1)
        matched_shapes += pm.ls(shape_name3, type='shape', r=1)

        shaderName = pm.ls(key, type='shadingEngine', r=1)[0].name()
        divElem = spE.split("[")
        tmp_faces = divElem[1].split(']')[0]
        faces_list = tmp_faces.split(',')
        for face_range in faces_list:
            facesIDs = face_range.split(':')
            if len(facesIDs) == 3:
                for x in range(int(facesIDs[0]), int(facesIDs[1])+1,
                               int(facesIDs[2])):
                    for m_shape in matched_shapes:
                        target = '{0}.f[{1}]'.format(m_shape, x)
                        cmds.sets(target, forceElement=shaderName, e=1)
            else:
                for m_shape in matched_shapes:
                    target = '{0}.f[{1}]'.format(m_shape, face_range)
                    cmds.sets(target, forceElement=shaderName, e=1)

        if len(matched_shapes) == 0:
            self._feedback_str += 'missing shape {0}'.format(shape_name)

    def _get_maya_version(self):
        """
        Determine and return the Maya version as an integer

        :returns:    The Maya major version
        """
        if not hasattr(self, "_maya_major_version"):
            self._maya_major_version = 0
            # get the maya version string:
            maya_ver = cmds.about(version=True)
            # handle a couple of different formats: 'Maya XXXX' & 'XXXX':
            if maya_ver.startswith("Maya "):
                maya_ver = maya_ver[5:]
            # strip of any extra stuff including decimals:
            major_version_number_str = maya_ver.split(" ")[0].split(".")[0]
            if major_version_number_str and major_version_number_str.isdigit():
                self._maya_major_version = int(major_version_number_str)
        return self._maya_major_version
