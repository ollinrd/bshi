import nuke
import uuid
import os
import sgtk

class CoreFunc():
    def __init__(self):
       tk_nuke = sgtk.platform.current_engine()
       project = tk_nuke.shotgun.find_one("Project",
                                              [["id", "is",
                                                tk_nuke.context.project["id"]]],
                                               ["code", 'sg_color_space'])
       colorspace = project['sg_color_space']
       if 'aces' in colorspace:
           print 'USING ACES !!!'
           nuke.root()['colorManagement'].setValue('OCIO')
           nuke.root()['OCIO_config'].setValue('aces_1.0.3')
           nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
           nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
           nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
           nuke.root()['int16Lut'].setValue('ACES - ACEScc')
           nuke.root()['logLut'].setValue('Input - ADX - ADX10')
           nuke.root()['floatLut'].setValue('ACES - ACES2065-1')
           nuke.message('Aces configuration')