import sgtk
import nuke
import os


class ScriptCleaner(sgtk.platform.Application):
    """
    App that creates a Nuke node that sends items to Shotgun for review.
    """

    def init_app(self):
        """
        Called as the application is being initialized
        """
        # assign this app to nuke handle so that the gizmo finds it
        #nuke.tk_nuke_quickreview = self

        # add to nuke node menu
        icon = os.path.join(self.disk_location, "resources", "cleaner_icon.png")

        #Validation to avoid load in nuke that runs without ui
        valid = True
        self.load = False
        for arg in nuke.rawArgs: 
            if arg == "-t":
                valid = False
        if valid:
            self.engine.register_command(
                "Project Init",
                self.connectIntoCore,
                {"type": "script", "icon": icon}
            )

    def connectIntoCore(self):
        if not self.load:
            mod_scriptCleanrer_core = self.import_module('tk_nuke_projectinit_core')
            mod_scriptCleanrer_core.CoreFunc()
            nuke.root()['colorManagement'].setValue('OCIO')
            nuke.root()['OCIO_config'].setValue('aces_1.0.3')
            nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
            nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
            nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
            nuke.root()['int16Lut'].setValue('ACES - ACEScc')
            nuke.root()['logLut'].setValue('Input - ADX - ADX10')
            nuke.root()['floatLut'].setValue('ACES - ACES2065-1')
            nuke.addOnCreate(self._set_reader_ACES, nodeClass="Read")
            for nod in nuke.allNodes('Viewer'):
                nod['viewerProcess'].setValue('Rec.709 (ACES)')
            self.load = True

    def _set_reader_ACES(self):
        read = nuke.thisNode()
        read['raw'].setValue(False)
        read['colorspace'].setValue('ACES - ACEScg')

