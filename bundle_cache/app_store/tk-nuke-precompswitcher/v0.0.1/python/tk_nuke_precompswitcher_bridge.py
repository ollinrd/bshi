import nuke
import sgtk
import os
from sgtk.platform.qt import QtCore, QtGui
import sys

class BridgeFunc():
    def __init__(self, loc, parent=None):
        """
        """
        uiFileName = os.path.join(loc, "resources", "main_empty.ui")

        cV = nuke.NUKE_VERSION_STRING
        nukeV = cV.split("v")[0]
        if float(nukeV) < 11:
            from PySide import QtUiTools
            self.loader = QtUiTools.QUiLoader()
        else:
            from PySide2 import QtUiTools
            self.loader = QtUiTools.QUiLoader()

        # Main window
        uifile = QtCore.QFile(uiFileName)
        uifile.open(QtCore.QFile.ReadOnly)
        self.ui = self.loader.load(uifile, parent)
        uifile.close()

    def setController(self, core):
        self.core = core
        self.ui.btnsKit_OkCancel.accepted.connect(self._processPrecompsSelected)

    def _processPrecompsSelected(self):
        lay = self.ui.verticalLayout.children()[0]
        toProcessNodes = []
        for l in lay.children():
            # for el in range(lay.count()):
            chkBox = l.itemAt(0).widget()
            t = l.itemAt(1).widget().text()
            if chkBox.isChecked():
                n = nuke.toNode(t)
                toProcessNodes.append(n)

        for n in toProcessNodes:
            # Go inside groups
            fullN = n.fullName()
            if '.' in fullN:
                shortN = n.name()
                contextN = fullN.replace('.{0}'.format(shortN), '')
                cN = nuke.toNode(contextN)
                with cN:
                    val = self.core.process_node(n)
            else:
                val = self.core.process_node(n)

    def getValidSubNodes(self, node):
        valid_nodes = []
        with node:
            for n in nuke.allNodes():
                if self.core.validPrecomp(n):
                    valid_nodes.append(n)
                elif n.Class() == 'Group':
                    valid_nodes += self.getValidSubNodes(n)
        return valid_nodes

    def _processNodes(self):
        valid_nodes = []
        with nuke.root():
            for sgN in nuke.allNodes():
                if self.core.validPrecomp(sgN):
                    valid_nodes.append(sgN)
                elif sgN.Class() == 'Group':
                    valid_nodes += self.getValidSubNodes(sgN)

        lay01 = QtGui.QVBoxLayout()
        all_regs = self.core.sg_find_all_precomps()

        for vn in valid_nodes:
            # Custom vertical layout for node
            new_lay = QtGui.QHBoxLayout()

            text = ''
            wished_type = 128
            precomp_name = ''
            # 128 for Image sequences
            # 129 for Script file
            if vn.Class() == 'Group':
                text = 'Contenedor'
                precomp_name = vn['sg_precomp_info'].value()
            if vn.Class() == 'WriteTank':
                text = 'Rendereable'
                precomp_name = vn['tank_channel'].value()
            if vn.Class() == 'Read':
                text = 'Secuencia'
                wished_type = 129
                precomp_name = vn['file'].value()

            # Creating checkbox
            chkBox = QtGui.QCheckBox(text)
            chkBox.setStyleSheet('color: rgb(150, 40, 40);')

            for reg in all_regs:
                pType = reg.get('published_file_type')
                if pType.get('id') == wished_type:
                    if '.' in precomp_name:
                        # group or sg write
                        if '/' in precomp_name:
                            precomp_name = precomp_name.split('/')[-1]
                    precomp_name = precomp_name.split('.')[0].split('_')[-1]
                    if reg.get('name') == precomp_name:
                        chkBox.setStyleSheet('color: rgb(40, 150, 40);')
            # Text for node name
            label = QtGui.QLabel()
            label.setText(vn.fullName())

            # Adding elements tinto new layout
            new_lay.addWidget(chkBox)
            new_lay.addWidget(label)

            # Adding layout into master layout for all nodes
            lay01.addLayout(new_lay)

        p = self.ui.verticalLayout.parent()
        for c in p.children():
            if 'CheckBox' in str(c):
                c.setParent(None)
        self.ui.verticalLayout.addLayout(lay01)

    def showUI(self):
        print "Showing UI"
        self._processNodes()
        self.ui.setWindowFlags(
            self.ui.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        self.ui.show()
