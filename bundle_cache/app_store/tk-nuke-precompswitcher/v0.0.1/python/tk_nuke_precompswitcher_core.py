import nuke
import os
import sgtk
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml


class CoreFunc():
    def __init__(self):
        """
        """
        self.engine = sgtk.platform.current_engine()
        self.sg = self.engine.shotgun
        self.checkShotInfo()
        config_path = self.engine.context.tank.pipeline_configuration.get_path()
        settings_path = config_path + "/config/resources/project/project_specs.yml"
        stream = open(settings_path, "r")
        self.settings  = dict(yaml.load(stream))

    def checkShotInfo(self):
        """
        """
        
        shotId = 1000
        # info = self.sg.find('Shot', 'is', {'id':shotId})

    def _searchRoot(self, node):
        """ 
        Recursive function to pop elements
        with parents
        """
        toReturn = [node]
        if len(node.dependencies()) is not 0:
            for newN in node.dependencies():
                [toReturn.append(x) for x in self._searchRoot(node = newN)]        
        return toReturn

    def _deleteTree(self, node):
        """
        """
        toDel = self._searchRoot(node)
        for d in toDel:
            nuke.delete(d)

    def _getSameReads(self, path):
        """
        """
        return [r for r in nuke.allNodes('Read') if r['file'].value() == path]

    def _getDownstream(self, node):
        """
        Get all nodes that has as input the 
        'node' given as parameter.
        """
        downDepencies = []
        for n2 in [n for n in nuke.allNodes() if n is not node]:
            [downDepencies.append(n2) for dp in n2.dependencies() if dp == node]
        return downDepencies

    def export_script(self, node, path):
        # Clear selection
        for n in nuke.allNodes(recurseGroups=True):
            n.knob('selected').setValue(False)

        #Selection with all necessary nodes
        for el in self._searchRoot(node = node):
            el.knob('selected').setValue(True)

        #Exporting selection
        nuke.nodeCopy(path)

        for n in nuke.allNodes(recurseGroups=True):
            n.knob('selected').setValue(False)

    def import_script(self, path, nodeSG):
        grp_name = self.getValidName('Grp_Precomp')

        grp = nuke.nodes.Group(name = grp_name)
        grp.begin()
        nodes = nuke.nodePaste(path)
        sgNode = [n for n in nuke.allNodes() if n.Class()=='WriteTank'][0]
        textInfo = sgNode['cached_path'].value().split('/')[-1]
        
        output_group=nuke.nodes.Output()
        output_group.setInput (0, sgNode)
        # nuke.delete(sgNode)
        for n in nuke.allNodes():
            n.knob('selected').setValue(False)
        grp.end()
        
        xpos = nodeSG.knob('xpos').value()
        ypos = nodeSG.knob('ypos').value()

        grp['xpos'].setValue(xpos)
        grp['ypos'].setValue(ypos)

        text_knob = nuke.Text_Knob('sg_precomp_info','Precomp file',textInfo)
        grp.addKnob(text_knob)
       
        dep = self._getDownstream(nodeSG)
        for p in dep:
            # Replace input connections 
            # SG WRITE node, to new Read Node
            for i in range (0, p.inputs()):
                if p.input(i) == nodeSG:
                    p.setInput (i, grp)

        grp.knob('selected').setValue(True)

    def sg_find_precompRender(self, file_name, node):
        sg_valid = False
        # files_path = node['cached_path'].value()
        # Precomp render
        publishType = 128 

        projID = self.engine.context.project.get('id')

        # x = self.engine.get_template_by_name('nuke_shot_precomp_pub_exr')
        # data = self.engine.get_template_by_name('nuke_shot_render_precomp_exr')

        # fields = data.get_fields(node['cached_path'].value())
        
        # fields['version']=1
        # path = x.apply_fields(fields)

        lastPart = ''
        knob = 'tank_channel'
        if knob in node.knobs():
            lastPart = node[knob].value()
        else:
            # Get from read node
            lastPart = file_name.split('_')[-1].split('.')[0]
        
        sg_filter =  [
            ['project', 'is', {'type': 'Project', 'id': projID}],
            {
                "filter_operator": "all",
                "filters": [
                    ['code', 'contains', lastPart], 
                    ['published_file_type', 'is', {'type': 'PublishedFileType', 'id': publishType}]
                ]
            }
        ]
        sSt = ["path_cache", "code", "name"]
        published_files = self.sg.find("PublishedFile", sg_filter, sSt)

        return published_files

    def sg_find_precompScript(self):

        sg_valid = False
        # Precomp Script
        publishType = 129 

        projID = self.engine.context.project.get('id')

        sg_filter =  [
            ['project', 'is', {'type': 'Project', 'id': projID}],
            {
                "filter_operator": "all",
                "filters": [
                    # ['sg_sg_original_w_source', 'is', files_path],
                    # ['name', 'is', name],
                    ['published_file_type', 'is', {'type': 'PublishedFileType', 'id': publishType}]
                ]
            }
        ]
        sSt = ["path_cache", "code", "name"]
        published_files = self.sg.find("PublishedFile", sg_filter, sSt)
        return published_files

    def sg_find_all_precomps(self):
        sg_valid = False
        # Precomp Script
        publishType = [128, 129]

        projID = self.engine.context.project.get('id')

        sg_filter = [
            ['project', 'is', {'type': 'Project', 'id': projID}],
            {
                "filter_operator": "any",
                "filters": [
                    ['published_file_type', 'is', {'type': 'PublishedFileType', 'id': publishType[0]}],
                    ['published_file_type', 'is', {'type': 'PublishedFileType', 'id': publishType[1]}]
                ]
            }
        ]
        sSt = ["path_cache", "code", "name", "published_file_type"]
        published_files = self.sg.find("PublishedFile", sg_filter, sSt)
        return published_files


    def process_n_writeTank(self, node, grp = None):
        """
        Evaluate SG precomp Write 
        """
        from os import listdir
        from os.path import isfile, join

        sg_valid = False

        if not 'PreComp' in node['profile_name'].value():
            return False

        file_name = node['cached_path'].value().split('/')[-1]

        published_files = self.sg_find_precompRender(file_name, node)

        if len(published_files) == 1:
            # Read node creator
            f_path = published_files[0]['path_cache']

            # Validacion ruta del sistema operativo
            f_path = self.settings['project_server_path'] + f_path 

            # 
            # Validacion deadline Render!!!
            # 
            # 
            # Fin validacion deadline render!!!
            # 

            rN = None
            # Validation if read node exists with the same attributes
            mRs = self._getSameReads(f_path)
            # 
            if not mRs:
                folder_f_path = f_path.split('/')[-1]
                folder_f_path = f_path.split(folder_f_path)[0]
                valid = False

                onlyfiles = [f for f in listdir(folder_f_path) if isfile(join(folder_f_path, f))]
                nums = []
                for fi in onlyfiles:
                    tokens = fi.split('.')
                    if len(tokens)>2:
                        num = tokens[-2]
                        if num.isdigit():
                            nums.append(num)
                if len(nums):
                    first_f=min(nums)
                    last_f=max(nums)
                    rN = nuke.nodes.Read(file=f_path, first=first_f, last=last_f)
                else:
                    rN = nuke.nodes.Read(file=f_path, first=nuke.root()['first_frame'].value(),
                                         last=nuke.root()['last_frame'].value())
            else:
                mainRead = mRs[0]
                for mr in mRs:
                    if mr['ypos'].value() < mainRead['ypos'].value():
                        mainRead = mr

                rN = nuke.nodes.Dot()
                rN.setInput(0, mainRead)
                rN['label'].setValue('Precomp\n'+f_path.split('/')[-1])
                rN['note_font_size'].setValue(22)
                rN['hide_input'].setValue(True)

            dep = self._getDownstream(node)
            for p in dep:
                # Replace input connections 
                # SG WRITE node, to new Read Node
                for i in range (0, p.inputs()):
                    if p.input(i) == node:
                        p.setInput (i, rN)

            xpos = node.knob('xpos').value()
            ypos = node.knob('ypos').value()

            rN['xpos'].setValue(xpos)
            rN['ypos'].setValue(ypos)

            sg_valid = True
            self._deleteTree(node = node)

            rN.knob('selected').setValue(True)
            if grp:
                return rN

        else:
            if len(published_files) >1:
                nuke.message("More than one publish files match with\nthe original write node")
            else:
                nuke.message('No publish precomp found')
        return sg_valid

    def process_n_groupPrecomp(self, node):
        """
        """
        import copy
        if 'sg_precomp_info' in node.knobs():
            print 'Converting precomp group to read node'
            valid = False
            readPath = ''
            newN = None
            with node:
                listNodes = node.nodes()
                for n in listNodes:
                    if n.Class() == 'Output':
                        connections = n.dependencies()
                        if connections:
                            valid = True
                            sg_precomp = connections[0]
                            if sg_precomp.Class() == 'Read':
                                newN = self.process_n_readPrecomp(sg_precomp)

                            else:
                                newN = self.process_n_writeTank(sg_precomp, grp = True)
                            
                        else:
                            valid = False

            oldNodes = nuke.allNodes()
            copy.copy(newN)

            for n in nuke.allNodes():
                if not n in oldNodes:
                    newN = n
                    break

            downNode = self._getDownstream(node)

            for p in downNode:
                # Replace input connections 
                # SG WRITE node, to new Read Node
                for i in range (0, p.inputs()):
                    if p.input(i) == node:
                        p.setInput (i, newN)

            xpos = node.knob('xpos').value()
            ypos = node.knob('ypos').value()

            newN['xpos'].setValue(xpos)
            newN['ypos'].setValue(ypos)

            nuke.delete(node)

        else:
            print '\n\n\n\nNot valid Group'

    def process_n_readPrecomp(self, node):
        #cdl_template = self.engine.get_template_by_name("nuke_shot_precomp_pub")
        path = node['file'].value() 

        if path.find('preComp') is not -1:
            file_name = path.split('/')[-1]

            precompFounded = self.sg_find_precompRender(file_name, node)
            if len(precompFounded):
                reg_publishRender = self.sg_find_precompRender(file_name, node)[0]

                name = reg_publishRender['name']
                reg_publishScript = self.sg_find_precompScript()

                if len(reg_publishScript):
                    # Validacion ruta del sistema operativo
                    validReg = None
                    for reg in reg_publishScript:
                        if name == reg.get('name'):
                            validReg = reg
                    if validReg != None:
                        f_path = self.settings['project_server_path'] + validReg['path_cache']

                        #print reg_publishScript[0]
                        self.import_script( f_path , node)

                        nuke.delete(node)
                        return True
                    else:
                        nuke.message('No precomp "{0}" detected'.format(name))
                        return False
                else:
                    nuke.message('No precomp script publish detected')
                    return False
            else:
                nuke.message('No precomp script publish detected', file_name)
                return False
        else:
            return False

    def process_node(self, node):
        """
        Evaluate Node posibilities
            WriteTank node
            Group of precomp Nodes
            Read Precomp nodes
        """

        if node.Class() == 'WriteTank':
            print "WriteTank"
            return self.process_n_writeTank(node = node) 

        # Evaluate group of precomps
        if node.Class() == 'Group':
            print 'Group'            
            return self.process_n_groupPrecomp(node = node)

        if node.Class() == 'Read':
            print 'Read'
            return self.process_n_readPrecomp (node = node)

    def validPrecomp(self, node):
        valid = False
        cs = node.Class()
        if cs == 'Read':
            valid = 'PreComp' in node['file'].value()

        if cs == 'WriteTank':
            valid = 'PreComp' in node['profile_name'].value()

        if cs == 'Group':
            valid = 'sg_precomp_info' in node.knobs()

        return valid

    def process_selection(self):
        from sgtk.platform.qt import QtCore, QtGui

        validTypes = ['WriteTank','Group','Read']
        sel = nuke.selectedNodes()
        newSel = []

        launchUI = True
        for n in sel:
            if self.validPrecomp(n):
                newSel.append(n)
                launchUI = False

        for n in newSel:
            val = self.process_node(node=n)
            
        return launchUI

    def getValidName(self, name):
        if nuke.toNode(name) is not None:
            if len(name.split('_')) >2:

                num = name.split('_')[-1]
                originName = name.replace('_'+num, '')
                num = int(num)
                newName = '{0}_{1}'.format(originName, num+1)
                return self.getValidName(newName)
            else:
                return self.getValidName(name + '_1')
        else:
            return name




