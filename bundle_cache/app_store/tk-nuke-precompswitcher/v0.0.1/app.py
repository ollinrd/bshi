import sgtk
import nuke
import os


class PrecompSwitcher(sgtk.platform.Application):
    """
    App that creates a Nuke node that sends items to Shotgun for review.
    """

    def init_app(self):
        """
        Called as the application is being initialized
        """
        # assign this app to nuke handle so that the gizmo finds it
        #nuke.tk_nuke_quickreview = self

        # add to nuke node menu
        icon = os.path.join(self.disk_location, "resources", "switch_icon.png")

        #Validation to avoid load in nuke that runs without ui
        valid = True
        self.load = False
        for arg in nuke.rawArgs: 
            if arg == "-t":
                valid = False
        if valid:
            self.engine.register_command(
                "Precomp Switcher",
                self.connectIntoCoreSwitch,
                {"type": "script", "icon": icon}
            )

    def connectIntoCoreSwitch(self):
        if not self.load:
            m_core = self.import_module('tk_nuke_precompswitcher_core')
            m_bridge = self.import_module('tk_nuke_precompswitcher_bridge')

            self.core = m_core.CoreFunc()
            self.bridge = m_bridge.BridgeFunc(self.disk_location)

            self.bridge.setController(
                core = self.core)
            self.load = True

        if self.core.process_selection():
            self.bridge.showUI()




