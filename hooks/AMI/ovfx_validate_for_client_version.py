
from tank import Hook
import subprocess
import tempfile
import os
import sys
import subprocess
import datetime
import yaml
import tempfile

class ValidateVersionHook(Hook):
    def execute(self, version, engine, sg, **kwargs):
            errors = []
            if version['entity']['type'] == 'Shot':
                shot = sg.find_one('Shot', [['id', 'is', version['entity']['id']]],
                                           ["sg_head_in", "sg_tail_out", "sg_offset_frame",
                                            "sg_main_plate_name", "sg_camera", "code",
                                            "sg_resx", "sg_resy", "project.Project.code",
                                            'sg_external_id', 'sg_sequence.Sequence.code',
                                            'sg_external_in_frame_name', 'project'])
                validate_result, v_errors = self.validate_sgfields_for_shot(shot)
                validate_result, v_errors = self.validate_sgfields_for_version(version, v_errors)
                if v_errors:
                    return False, v_errors
                specs = self.get_project_specs(engine,shot['project'])
                overwrite = self.validate_version_number(version, shot, engine, specs)
                print 'OVERWrite: ', overwrite
                return overwrite, v_errors

    def validate_sgfields_for_shot(self, shot):

        errors = []
        if not shot['sg_head_in']:
            errors.append('missing Shot Head-in ')
        if not shot['sg_tail_out']:
            errors.append('missing Shot Tail-out')
        if not shot['sg_offset_frame']:
            errors.append('Offset frame')
        validate = True
        if errors:
            validate = False
        return validate, errors

    def validate_sgfields_for_version(self, version, errors):
        if not version['sg_submission_notes']:
            errors.append('missing Version submission_notes ')
        if not version['sg_client_version_number']:
            errors.append('missing Version client_version_number ')
        ''' this is  for claw
        if version['project']['id'] == 341:
          ci_in = str(version['sg_context_cut_item_in'])
          ci_out = str(version['sg_context_cut_item_out'])
          if ci_in == 'None' or ci_out == 'None':
              errors.append('missing Version  sg_context_cut_item_out or sg_context_cut_item_in')
        '''
        validate = True
        if errors:
            validate = False
        return validate, errors

    def get_project_specs(self, engine, sg_project):
        config_path = '/nfs/ovfxToolkit/Prod/Projects/BSHI/'
        with open(config_path + "/resources/project/project_specs.yml", 'r') as stream:
            try:
                specs = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        print 'SPECS:', specs
        return specs


    def validate_version_number(self, version, shot, engine, specs):
        '''
        Validate True if version number dont exist in outgoing folder, return False.
        '''
        overwrite = {}
        image_template_name = 'nuke_shot_delivery_{0}'.format(specs['main_format'])
        # validate IMAGES
        image_template = engine.get_template_by_name(image_template_name)
        if version['sg_client_task_name']:
            step_name = version['sg_client_task_name']
        else:
            step_name = 'comp'

        fields = {'Shot': shot['code'],
                  'external_id': shot['sg_external_id'],
                  'Step': step_name,
                  'Step_name': step_name,
                  'version': version['sg_client_version_number'],
                  'version_dlv': version['sg_client_version_number'],
                  'project_code': shot['project.Project.code'],
                  'Sequence': shot['sg_sequence.Sequence.code'],
                  'external_base_name': shot['sg_external_in_frame_name']
                  }
        image_path = image_template.apply_fields(fields)
        if os.path.exists(os.path.dirname(image_path)):
            overwrite['images'] = True
            print fields['Step_name']
            print image_path
        elif os.path.exists(os.path.dirname(image_path.replace(fields['Step_name'],
                                                               fields['Step_name'].lower() ))):
            overwrite['images'] = True
            image_path = image_path.replace(fields['Step_name'], fields['Step_name'].lower())
            print image_path
        elif os.path.exists(os.path.dirname(image_path.replace(fields['Step_name'],
                                                               fields['Step_name'].upper() ))):
            overwrite['images'] = True
            print fields['Step_name']
            image_path = image_path.replace(fields['Step_name'], fields['Step_name'].upper())
            print image_path
        else:
            overwrite['images'] = False
            print fields['Step_name']
            print image_path

        fields = image_template.get_fields(image_path)
        for i in specs['client_template_mov']:
            mov_template = engine.get_template_by_name(i)
            mov_path = mov_template.apply_fields(fields)
            if os.path.exists(os.path.dirname(mov_path)):
                overwrite['mov'] = True
                print mov_path
                return overwrite
            elif os.path.exists(os.path.dirname(mov_path.replace(fields['Step'],
                                                                   fields['Step'].upper() ))):
                overwrite['mov'] = True
                image_path = mov_path.replace(fields['Step'], fields['Step'].upper())
                print mov_path
                return overwrite
            elif os.path.exists(os.path.dirname(mov_path.replace(fields['Step'],
                                                                   fields['Step'].lower() ))):
                overwrite['mov'] = True
                image_path = mov_path.replace(fields['Step_name'], fields['Step'].lower())
                print mov_path
                return overwrite
            else:
                overwrite['mov'] = False
                print mov_path
                return overwrite
