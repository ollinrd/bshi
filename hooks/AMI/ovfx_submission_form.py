import os
import sys
sys.path.append("/nfs/ovfxToolkit/Resources")
sys.path.append("/nfs/ovfxToolkit/Resources/site-packages")
from datetime import datetime, timedelta
import xlwt
# from sgtk import Hook
# import sgtk
import subprocess
import xlsxwriter
import tempfile
import ast

from pythonTools.shotgunUtils.dbConnect import sgConnect

reload(sys)
sys.setdefaultencoding('utf-8')
dict_txt = sys.argv[1]
base_path = sys.argv[2]
file = open(dict_txt, 'r')
items = file.read()
file.close()
main_dict = ast.literal_eval(items)
user = main_dict['user']
playlist_id = main_dict['plids']

def create_folders(path):
    if not os.path.exists(path):
        try:
            chmod = '2775'
            ovfxPath=path.split("/")
            ovfxFolder=""
            for folder in ovfxPath:
                ovfxFolder = ovfxFolder + folder + "/" 
                if not os.path.exists(ovfxFolder):
                    os.system("sudo /usr/bin/mkdir %s"%(ovfxFolder))
                    os.system("sudo /usr/bin/chmod  %s %s "%(chmod,ovfxFolder))
                    os.system("sudo /usr/bin/chown %s %s"%('core', ovfxFolder))
                    if 'editorial' in ovfxFolder:
                        os.system("sudo /usr/bin/chgrp %s %s"%('editorial', ovfxFolder))
                    else:
                        os.system("sudo /usr/bin/chgrp %s %s"%('ovfx', ovfxFolder))
        except Exception as e:
          raise e
def connShotgun(api_namekey):
    sgConn = sgConnect()
    conn = sgConn.connect(api_namekey)

    return conn

try:
    sg = connShotgun("submission_form")

    def _get_full_version(version,sg):
        return sg.find_one("Version",
                       [["id", "is", version["id"]]],
                       ["sg_submission_notes", "client_code", "code", 'sg_status_list',
                        "sg_first_frame", "sg_last_frame", "sg_external_id", 'frame_count', 'frame_range',
                        "sg_task", "entity", "sg_path_to_frames", 'sg_path_to_movie', 'sg_submitted_for',
                        "sg_client_version_number", "sg_version_type", 'sg_version_file_type', 'sg_submission_name',
                        "created_by", "project", "code", 'sg_vendor'])


    for pl_id in playlist_id:
        playlist = sg.find_one("Playlist",
                               [["id", "is", int(pl_id)]],
                               ["versions", "code", "project.Project.code"])

        if 'Client' not in playlist['code']:
            print '<br>La playlist {0} no es una playlist de cliente !!!'.format(playlist['code'])
        else:
            playlist_values = playlist["code"].split("_")
            if len(playlist["code"].split("_")) > 1:
                if type(playlist_values[-1]) == int and len(playlist_values[-2]) == 8:
                    raise Exception("El nombre de la playlist {0} no tiene el formato correcto".format(playlist["code"]))
            else:
                raise Exception("El nombre de la playlist {0} no tiene el formato correcto".format(playlist["code"]))
            versions = [_get_full_version(v, sg) for v in playlist["versions"]]

            project = sg.find_one("Project", [["code", "is", "OSOY"]], ["code"])

            #-----------------------------------------------
            fecha_0 = playlist['code'].split('_')[-1]
            letra_1 = ''
            if len(fecha_0) < 8:
                fecha_0 = playlist['code'].split('_')[-2]
                letra_1 = '_' + playlist['code'].split('_')[-1]

            elif len(fecha_0) >8:
                letra_1 = '_' + fecha_0[8:]
                fecha_0 = fecha_0[:8]

            #print 'DAT:   ', dat
            today = datetime.strptime(fecha_0, "%Y%m%d")

            fecha_0 = playlist['code'].split('_')[-1]
            letra_1 = ''
            if len(fecha_0) < 8:
                fecha_0 = playlist['code'].split('_')[-2]
                letra_1 = '_' + playlist['code'].split('_')[-1]

            elif len(fecha_0) >8:
                letra_1 = '_' + fecha_0[8:]
                fecha_0 = fecha_0[:8]
            #print '----> ', playlist['code'].split('_')
            submission_name = '{0}_ollinvfx_submission_sheet_{1}_{2}_{3}{4}'.format(
                                playlist['project.Project.code'].lower(), fecha_0[2:4], fecha_0[4:6], fecha_0[6:],letra_1)
            # submission_name = versions[0]['sg_submission_name']  
                
            an = fecha_0[2:4]
            mes = fecha_0[4:6]
            dia = fecha_0[6:]

            fecha_0 = "_".join([an,mes,dia])  
            xls_submission_name = '{0}_ollinvfx_submission_form_{1}{2}'.format(playlist['project.Project.code'].lower(), fecha_0, letra_1)
            #-----------------------------------------------

            if playlist['project.Project.code'] in ['OSOY', 'ZENT']:
                submission_name = '{0}_ollinvfx_submission_form_{1}_{2}_{3}_{4}'.format(
                    playlist['project.Project.code'][1:].lower(),
                    playlist['code'].split('_')[-1][2:4],
                    playlist['code'].split('_')[-1][4:6],
                    playlist['code'].split('_')[-1][6:8],
                    playlist['code'].split('_')[-1][8])    


            xls_path = base_path + '/{0}.xlsx'.format(xls_submission_name)
            if not os.path.exists(os.path.dirname(xls_path)):
                create_folders(os.path.dirname(xls_path))
            # xls_path = '/home/ecampos/Desktop/{0}.xls'.format(versions[0]['sg_submission_name'])

            workbook = xlsxwriter.Workbook(xls_path)
            worksheet = workbook.add_worksheet()
            worksheet.set_column("A:A", 15)
            worksheet.set_column("B:B", 24)
            worksheet.set_column("C:C", 18)
            worksheet.set_column("D:D", 35)
            worksheet.set_column("E:E", 18)
            worksheet.set_column("F:F", 48)
            worksheet.set_column("G:G", 48)

            font = xlwt.Font() # Create the Font
            font.height = 320
            worksheet.set_row(0, 20)
            worksheet.set_default_row(hide_unused_rows=True)
            worksheet.set_column('H:XFD', None, None, {'hidden': True})

            eformat = workbook.add_format({'bold': True, 'font_size': 12, 
                                          'align': 'center', 'valign': 'center'})

            row = 0
            col = 0

            eformat = workbook.add_format({'bold': True, 'font_color': "white", 'font_name': 'Arial',
                                          'bg_color': 'black', 'align': 'center'})



            row = 0
            col = 0

            eformat2 = workbook.add_format({'bold': False, 'font_color': "black",'font_size': 10,'font_name': 'Arial',
                                           'align': 'left'})
            eformat3 = workbook.add_format({'bold': False, 'font_color': "black", 'font_size': 10, 'font_name': 'Arial',
                                           'align': 'center'})
            expenses = ["Vendor", "Submission Date", "{0} name".format(versions[0]['entity']['type']), "Version", "File Type", "Submission Notes", 'Client Notes']

            for i in expenses:
                worksheet.write(row, col, i, eformat)
                col += 1
            col = 0
            row += 1
            date = datetime.now().date()
            d = str(date).split('-')
            date = '{0}/{1}/{2}'.format(d[1], d[2], d[0])
            error_fields = True

            for  version in versions:
                ver = sg.find_one('Version', [['id', 'is', version['id']]], ["sg_submission_notes", "client_code", "code",
                        "sg_first_frame", "sg_last_frame", "sg_external_id", 'frame_count', 'frame_range',
                        "sg_task", "entity", "sg_path_to_frames", 'sg_path_to_movie', 'sg_submitted_for',
                        "sg_client_version_number", "sg_version_type", 'sg_version_file_type', 'sg_submission_name',
                        "created_by", "project", "code", 'sg_vendor', 'sg_status_list'])
                if not version.get('sg_vendor'):
                    error_fields = False
                    print '<br>La version {0} no  contiene el campo sg_vendor'.format(version['code'])
                    break
                # if not version.get('sg_submission_name'):
                    # print '<br>La version {0} no  contiene el campo sg_submission_name'.format(version['code'])
                    # error_fields = False
                    # break
                if not version.get('entity'):
                    print '<br>La version {0} no  contiene el campo entity'.format(version['code'])
                    error_fields = False
                    break
                if not version.get('sg_version_file_type'):
                    print '<br>La version {0} no  contiene el campo sg_version_file_type'.format(version['code'])
                    error_fields = False
                    break
                if not version.get('frame_count'):
                    print '<br>La version {0} no  contiene el campo frame_count'.format(version['code'])
                    error_fields = False
                    break
                if project['code'] not in ['OSOY', 'ZENT'] and not version.get('sg_submitted_for'):
                    print '<br>La version {0} no  contiene el campo sg_submitted_for'.format(version['code'])
                    error_fields = False
                    break
                if not version.get('sg_submission_notes'):
                    print '<br>La version {0} no  contiene el campo sg_submission_notes'.format(version['code'])
                    error_fields = False
                    break

            if error_fields == False:
                workbook.close()
                print "<br>FALTAN CAMPOS DENTRO DE LA PLAYLIST FAVOR DE VERIFICARLOS !!!"
                sys.exit()

            for ver in versions:
                if ver['sg_status_list'] in ['edapp', 'dlvr', 'fp4k']:
                    if ver['entity']['type'] == 'Shot':
                        entity = sg.find_one('Shot', [['id', 'is', ver['entity']['id']]], ['sg_external_id'])
                        ent_name = entity['sg_external_id']
                    elif ver['entity']['type'] == 'Sequence':
                        entity = sg.find_one('Asset', [['id', 'is', ver['entity']['id']]], ['code'])
                        ent_name = entity['code']
                    if version['entity']['type'] == 'Sequence':
                        entity = sg.find_one('Sequence', [['id', 'is', ver['entity']['id']]], ['code'])
                        ent_name = entity['code']

                    if ver['sg_version_file_type'] == 'MOV':
                        path = os.path.basename(ver['sg_path_to_movie'])
                    else:
                        first = int(str(ver['frame_range']).split('-')[0])
                        last = int(str(ver['frame_range']).split('-')[1])
                        framerange = '{0}-{1}'.format(first - 1, last)
                        frms = '[{0}]'.format(framerange)
                        path = os.path.basename(ver['sg_path_to_frames']).replace('%04d', frms)

                    #if playlist['project.Project.code'] in ['OSOY', 'ZENT']:
                    ver['sg_vendor'] = 'OLN'

                    #expe = [date, ver['sg_vendor'], submission_name, sh['sg_external_id'], path, ver['sg_version_file_type'], ver['frame_count'], ver['sg_submission_notes']]
                    vendor = ver['sg_vendor']
                    date = date
                    shotcode = ent_name
                    vers = ver['code']
                    description = ver['sg_version_file_type']
                    submissionnotes = ver['sg_submission_notes']

                    expe = [vendor, date, shotcode, vers, description, submissionnotes, '']
                    for ex in expe:
                        worksheet.write(row, col, ex, eformat3)
                        col += 1
                    col = 0
                    row += 1

            #contents, review_count, delivery_count = add_version_info(versions, worksheet, row, sg, workbook)
            #self._register_delivery(user, playlist, contents, review_count,
                                    #delivery_count)

            workbook.close()
            os.system('chmod 777 {0}'.format(xls_path))
            copp = '/nfs/ollinvfx/Project/OSOY/editorial/publish/outgoing/SUBMISSION_FORM/{0}'.format(os.path.basename(xls_path))
            os.system('cp {0} {1}'.format(xls_path, copp))
            os.system('chmod 777 {0}'.format(copp))
            submission_form = sg.upload('Playlist', pl_id, xls_path, field_name= 'sg_submission_form')
            print '<br>SE HA CREADO EL SUBMISSION FORM EN LA RUTA: {0}'.format(xls_path)
            ingest_path = '/mnt/storage/outgoing/PIGS/20{0}_{1}/Submission_Form/'.format(fecha_0.replace('_', ''),letra_1.replace('_', ''))
            CLIENT_COMMAND = 'ssh ingest mkdir -p "{0}"'.format(ingest_path)
            CLIENT_COMMAND_2 = 'ssh ingest chmod -R 775 "{0}"'.format(os.path.dirname(os.path.dirname(ingest_path)))
            CLIENT_COMMAND_3 = 'ssh ingest chgrp -R editorial "{0}"'.format(os.path.dirname(os.path.dirname(ingest_path)))
            subprocess.Popen(CLIENT_COMMAND, shell=True, stdout=subprocess.PIPE)
            subprocess.Popen(CLIENT_COMMAND_2, shell=True, stdout=subprocess.PIPE)
            subprocess.Popen(CLIENT_COMMAND_3, shell=True, stdout=subprocess.PIPE)
            os.system('scp ' + xls_path + ' ecampos@ingest:' + ingest_path + '/{0}'.format(playlist['code']+ '.xlsx'))
except Exception as e:
    import traceback
    print '<br>', traceback.format_exc()
    print '<br>', '-'*42
    print e

