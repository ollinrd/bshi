
from tank import Hook
import subprocess
import tempfile
import os
import sys
import subprocess
import datetime
import yaml
import tempfile
import requests

class OverwriteVersionHook(Hook):
    def execute(self, version, engine, sg, p_type, uid, **kwargs):
            errors = []
            if version['entity']['type'] == 'Shot':
                self.p_type = p_type
                self.uid = uid
                shot = sg.find_one('Shot', [['id', 'is', version['entity']['id']]],
                                           ["sg_head_in", "sg_tail_out", "sg_offset_frame",
                                            "sg_main_plate_name", "sg_camera", "code",
                                            "sg_resx", "sg_resy", "project.Project.code",
                                            'sg_external_id', 'sg_sequence.Sequence.code',
                                            'sg_external_in_frame_name'])
                validate_result, v_errors = self.validate_sgfields_for_shot(shot)
                validate_result, v_errors = self.validate_sgfields_for_version(version, v_errors)
                if v_errors:
                    return False, v_errors
                config_path = engine.context.tank.pipeline_configuration.get_path()
                specs = self.get_project_specs(engine)
                deleted = self.validate_version_number(version, shot, engine, specs)
                print 'deleted: ', deleted
                return str(deleted)

    def validate_sgfields_for_shot(self, shot):

        errors = []
        if not shot['sg_head_in']:
            errors.append('missing Shot Head-in ')
        if not shot['sg_tail_out']:
            errors.append('missing Shot Tail-out')
        if not shot['sg_offset_frame']:
            errors.append('Offset frame')
        validate = True
        if errors:
            validate = False
        return validate, errors

    def validate_sgfields_for_version(self, version, errors):
        if not version['sg_submission_notes']:
            errors.append('missing Version submission_notes ')
        if not version['sg_client_version_number']:
            errors.append('missing Version client_version_number ')
        ''' this is  for claw
        if version['project']['id'] == 341:
          ci_in = str(version['sg_context_cut_item_in'])
          ci_out = str(version['sg_context_cut_item_out'])
          if ci_in == 'None' or ci_out == 'None':
              errors.append('missing Version  sg_context_cut_item_out or sg_context_cut_item_in')
        '''
        validate = True
        if errors:
            validate = False
        return validate, errors

    def get_project_specs(self, engine):
        config_path = '/nfs/ovfxToolkit/Prod/Projects/BSHI/'
        with open(config_path + "/resources/project/project_specs.yml", 'r') as stream:
            try:
                specs = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        print 'SPECS:', specs
        return specs


    def validate_version_number(self, version, shot, engine, specs):
        '''
        Validate True if version number dont exist in outgoing folder, return False.
        '''
        overwrite = {}
        image_template_name = 'nuke_shot_delivery_exr'
        # validate IMAGES
        image_template = engine.get_template_by_name(image_template_name)
        if version['sg_client_task_name']:
            step_name = version['sg_client_task_name']
        else:
            step_name = 'comp'

        fields = {'Shot': shot['code'],
                  'external_id': shot['sg_external_id'],
                  'Step': step_name,
                  'Step_name': step_name,
                  'version': version['sg_client_version_number'],
                  'version_dlv': version['sg_client_version_number'],
                  'project_code': shot['project.Project.code'],
                  'Sequence': shot['sg_sequence.Sequence.code'],
                  'external_base_name': shot['sg_external_in_frame_name']
                  }
        if self.p_type == 'primary':
            image_path = image_template.apply_fields(fields)
            print 'image_path = ', image_path
            if image_path == '' or image_path == '/' or not image_path:
                return

            elif os.path.exists(os.path.dirname(image_path)):
                pub_path = os.path.dirname(image_path)
                os.system('rm -rf {0}'.format(os.path.dirname(pub_path)))
                print pub_path
            elif os.path.exists(os.path.dirname(image_path.replace(fields['Step'],
                                                                   fields['Step'].lower() ))):
                pub_path = image_path.replace(fields['Step'],fields['Step'].lower())
                os.system('rm -rf {0}'.format(os.path.dirname(pub_path)))
                print pub_path
            elif os.path.exists(os.path.dirname(image_path.replace(fields['Step'],
                                                                   fields['Step'].upper() ))):
                pub_path = image_path.replace(fields['Step'],fields['Step'].upper())
                os.system('rm -rf {0}'.format(os.path.dirname(pub_path)))
                print pub_path
            else:
                print 'cant overwrite ' + pub_path
                return
            if pub_path:
                filters = [['sg_path_to_movie', 'is', pub_path]]
                try:
                    version_td = self.sg.find_one('Version', filters, ['code'])
                    self.sg.delete('Version', version_td['id'])
                except:
                    pass
                r = requests.get('http://172.16.28.73:7000/client/delivery/?next={0}_{1}'.format(self.uid, version['id']))
                return pub_path


        elif self.p_type == 'secondary':
            for i in specs['client_template_mov']:
                mov_template = engine.get_template_by_name(i)
                mov_path = mov_template.apply_fields(fields)
                if mov_path:
                    if mov_path:
                        if os.path.exists(os.path.dirname(mov_path)) and not mov_path == '/':
                            os.system('rm -rf {0}'.format(os.path.dirname(mov_path)))
                            pub_path = mov_path
                        elif os.path.exists(os.path.dirname(mov_path.replace(fields['Step_name'],
                                                            fields['Step_name'].upper() ))) and not mov_path == '/':
                            os.system('rm -rf {0}'.format(os.path.dirname(mov_path.replace(fields['Step_name'],
                                                          fields['Step_name'].upper()))))
                            pub_path = mov_path.replace(fields['Step_name'],fields['Step_name'].upper())
                        else:
                            pub_path = False
                            print 'cant overwrite ' + mov_path
                    if pub_path:
                        filters = [['sg_path_to_movie', 'is', pub_path]]
                        try:
                            version_td = self.sg.find_one('Version', filters, ['code'])
                            self.sg.delete('Version', version_td['id'])
                        except:
                            pass
                        r = requests.get('http://172.16.28.73:7000/client/review/?next={0}_{1}'.format(self.uid, version['id']))
                        return pub_path
