
from tank import Hook
import subprocess
import tempfile
import os
import sys
import subprocess
import datetime
import yaml
import tempfile
import datetime
import yaml

class PackagePlaylistHook(Hook):
    def execute(self, sg, project, playlist, engine, u_id, settings, pl_ids, **kwargs):
        result = 'Package completed'
        config_path = engine.context.tank.pipeline_configuration.get_path()
        pl_code = playlist['code'].split('_')[-1]
        self.temp_folder = getattr(settings, "TEMPORAL_FILES_FOLDER")
        self.u_id = u_id
        fields = {'date_folder': pl_code}
        print project
        specs = self.get_project_specs(project)
        main_template, main_path, client_main_path = self.build_required_templates(project, fields,
                                                                                   engine, config_path,
                                                                                   specs, playlist)
        proceso = ''
        for version in playlist['versions']:
            fecha_0 = playlist['code'].split('_')[-1]
            letra_1 = ''
            if len(fecha_0) < 8:
                fecha_0 = playlist['code'].split('_')[-2]
                letra_1 = '_' + playlist['code'].split('_')[-1]

            elif len(fecha_0) > 8:
                letra_1 = '_' + fecha_0[8:]
                fecha_0 = fecha_0[:8]
            version = sg.find_one('Version', [['id', 'is', version['id']]],
                                  ['sg_path_to_movie', 'sg_version_type', 
                                   'sg_path_to_frames', 'sg_status_list', 
                                   'entity', 'code', 'entity.Shot.sg_external_id',
                                   'sg_client_version_number', 'sg_task',
                                   'sg_client_task_name', 'sg_version_file_type'])
            if version['sg_status_list'] == 'edapp' or 'v000' in version['code']:
                sub_name = '{0}_ollinvfx_submission_sheet_{1}_{2}_{3}{4}'.format(
                    playlist['project.Project.code'].lower(), fecha_0[2:4], fecha_0[4:6], fecha_0[6:],letra_1)  
                udapte_data = {'sg_date_submitted': datetime.datetime.now().date(), 'sg_submission_name': sub_name}
                sg.update('Version', version['id'], udapte_data)
                fecha_0 = playlist['code'].split('_')[-1]
                letra_1 = ''
                if len(fecha_0) < 8:
                    fecha_0 = playlist['code'].split('_')[-2]
                    letra_1 = playlist['code'].split('_')[-1]
                # START REVIEWS
                if version['sg_version_type'] == 'Client':
                    path_to_movie = version['sg_path_to_movie']
                    new_path_to_movie_name = os.path.basename(version['sg_path_to_movie'])
                    previous_folder = version['sg_path_to_movie'].split('.')[-1]
                    videos_folder = main_path + '/' + previous_folder + '/'
                    videos_folder = videos_folder.replace('_/', '/')
                    engine.ensure_folder_exists(videos_folder)
                    os.system('cp -av ' + path_to_movie + ' ' + videos_folder + new_path_to_movie_name)
                    # moving para ingesta 
                    pl_code = playlist['code'].split('_')[-1]
                    if version['sg_client_version_number'] is None:
                        version['sg_client_version_number'] = 1
                    asset_code = ''
                    if version['entity']['type'] == 'Shot':
                        SHOT = sg.find_one('Shot', [['id', 'is', version['entity']['id']],
                                           ['project', 'is', project]], ['sg_output_alpha', 'code', 'sg_resx', 'sg_resy',
                                                                         'sg_sequence.Sequence.episode.Episode.code',
                                                                         'sg_external_id'])
                        videos_to_send_path = client_main_path + version['entity.Shot.sg_external_id'] + specs['pack_padding'] + '/'
                    keyMov = 'editorial_outgoing_MOV'
                    if keyMov in specs:
                        for mov_type in specs[keyMov]:
                            if version['entity']['type'] == 'Shot':
                                if mov_type[0].lower() in path_to_movie.lower():
                                    videos_to_send_path = specs['editorial_outgoing_client_folder']
                                    videos_to_send_path += mov_type[1].format(fecha_0, letra_1, playlist['project.Project.code'],
                                                                              version['entity.Shot.sg_external_id'],
                                                                              specs['pack_padding'].format(version['sg_client_version_number']),
                                                                              SHOT['sg_external_id'],
                                                                              'BSHI_{0}_{1}_v{2}'.format(SHOT['sg_external_id'],
                                                                                                         version['sg_client_task_name'],
                                                                                                         version['sg_client_version_number']))
                    videos_to_send_path = videos_to_send_path.replace('_/', '/')
                    CLIENT_COMMAND = 'ssh ingest mkdir -p "{0}"'.format(videos_to_send_path)
                    CLIENT_COMMAND_2 = 'ssh ingest chmod -R 775 "{0}"'.format(os.path.dirname(os.path.dirname(videos_to_send_path)))
                    CLIENT_COMMAND_3 = 'ssh ingest chgrp -R editorial "{0}"'.format(os.path.dirname(os.path.dirname(videos_to_send_path)))
                    subprocess.Popen(CLIENT_COMMAND, shell=True, stdout=subprocess.PIPE)
                    subprocess.Popen(CLIENT_COMMAND_2, shell=True, stdout=subprocess.PIPE)
                    subprocess.Popen(CLIENT_COMMAND_3, shell=True, stdout=subprocess.PIPE)

                    os.system('scp ' + path_to_movie + ' ecampos@ingest:' + videos_to_send_path + new_path_to_movie_name)
                    args = path_to_movie + ' ecampos@ingest:' + videos_to_send_path + '/'

                    if version['entity']['type'] == 'Shot':
                        name = fields['date_folder'] + '_' + version['entity.Shot.sg_external_id']
                    self.send_copy_job(specs, args, name, playlist, sg, specs['editorial_outgoing_client_folder'])

                    # Update version status
                    print 'Updating packaged version status: '
                    if not'v000' in version['code']:
                        print sg.update("Version", version['id'], {'sg_status_list': 'dlvr'})
                    print 'Version status update Done!'
                    CLIENT_COMMAND_perm = 'ssh ingest chmod -R 775 "{0}"'.format(client_main_path)
                    subprocess.Popen(CLIENT_COMMAND_perm, shell=True, stdout=subprocess.PIPE)

                elif version['sg_version_type'] == 'Client Delivery':
                    # START Delivery
                    print version['code']
                    print version['sg_version_type']
                    path_to_frames = os.path.dirname(version['sg_path_to_frames'])
                    if version['entity']['type'] == 'Shot':
                        SHOT = sg.find_one('Shot',
                                           [['id', 'is', version['entity']['id']],
                                            ['project', 'is', project]],
                                           ['code', 'sg_resx', 'sg_resy',
                                            'sg_sequence.Sequence.episode.Episode.code'])
                        pl_code = playlist['code'].split('_')[-1]
                        if not version['sg_client_version_number']:
                            version['sg_client_version_number'] = 1
                        previous_folder = os.path.basename(os.path.dirname(version['sg_path_to_frames']))
                        # Copy Frames
                        # Set step Name
                        if 'v000' not in version['code']:
                            if version['sg_client_task_name']:
                                step_name = version['sg_client_task_name']
                            else:
                                step_name = 'comp'
                        else:
                            step_name = 'comp'
                        new_client_path = client_main_path + version['entity.Shot.sg_external_id'] + specs['pack_padding'].format(version['sg_client_version_number']) + '/'
                        if not version['sg_version_file_type'] == 'PNG':
                            if 'editorial_outgoing_EXR' in specs:
                                new_client_path = specs['editorial_outgoing_client_folder'] + specs['editorial_outgoing_EXR'].format(fecha_0,letra_1,
                                                                                                                                     version['entity.Shot.sg_external_id'],
                                                                                                                                     version['sg_client_version_number'],step_name,
                                                                                                                                     playlist['project.Project.code'],
                                                                                                                                     specs['pack_padding'].format(version['sg_client_version_number']),
                                                                                                                                     step_name.upper(), SHOT['sg_resx'],
                                                                                                                                     SHOT['sg_resy'],SHOT['sg_sequence.Sequence.episode.Episode.code'])
                        else:
                            if 'editorial_outgoing_PNG' in specs:
                                new_client_path = specs['editorial_outgoing_client_folder'] + specs['editorial_outgoing_PNG'].format(fecha_0,letra_1,
                                                                                                                                     version['entity.Shot.sg_external_id'],
                                                                                                                                     version['sg_client_version_number'],step_name,
                                                                                                                                     playlist['project.Project.code'],
                                                                                                                                     specs['pack_padding'].format(version['sg_client_version_number']),
                                                                                                                                     step_name.upper(), SHOT['sg_resx'],
                                                                                                                                     SHOT['sg_resy'],SHOT['sg_sequence.Sequence.episode.Episode.code'])
                        if 'alpha' in path_to_frames:
                            new_client_path = specs['editorial_outgoing_client_folder'] + specs['editorial_outgoing_EXR'].format(fecha_0,letra_1,
                                                                                                                                 version['entity.Shot.sg_external_id'] + '_alpha',
                                                                                                                                 version['sg_client_version_number'])
                    # MOVIG FRAMES TO ingest
                    new_client_path = new_client_path.replace('_/', '/')
                    frames_to_send_path = new_client_path
                    CLIENT_COMMAND = 'ssh ingest mkdir -p "{0}"'.format(frames_to_send_path)
                    CLIENT_COMMAND_2 = 'ssh ingest chmod -R 775 "{0}"'.format(client_main_path)
                    CLIENT_COMMAND_3 = 'ssh ingest chgrp -R editorial "{0}"'.format(client_main_path)
                    process = subprocess.Popen(CLIENT_COMMAND, shell=True, stdout=subprocess.PIPE)
                    out, err = process.communicate()
                    print 'comando folders', CLIENT_COMMAND
                    print 'CREACION DE FOLDERS: ', out, err
                    subprocess.Popen(CLIENT_COMMAND_2, shell=True, stdout=subprocess.PIPE)
                    subprocess.Popen(CLIENT_COMMAND_3, shell=True, stdout=subprocess.PIPE)
                    args = path_to_frames + '/* ecampos@ingest:' + frames_to_send_path + '/'
                    if version['entity']['type'] == 'Shot':
                        name = fields['date_folder'] + '_' + version['entity.Shot.sg_external_id']
                    print 'Intento de enviar a granja'
                    self.send_copy_job(specs, args, name, playlist, sg ,specs['editorial_outgoing_client_folder'])
                    print 'DELIV'
                    # Update version status to Delivered!!!
                    print 'Updating packaged version status: '
                    if not'v000' in version['code']:
                        print sg.update("Version", version['id'], {'sg_status_list': 'fp4k'})
                    print 'Version status update Done!'                    
        #CLIENT_COMMAND_perm = 'ssh ingest chmod -R 775 "{0}"'.format(specs['editorial_outgoing_client_folder'])
        #subprocess.Popen(CLIENT_COMMAND_perm, shell=True, stdout=subprocess.PIPE)
        # submission form
        hookPath = self.conf_path + '/hooks/AMI/ovfx_submission_form.py'
        prefixx = self.temp_folder
        (filev, dictPath) = tempfile.mkstemp(prefix=prefixx + "submissionF_dict_", suffix='.txt')
        filev = open(dictPath, 'w')
        userid = int(u_id)
        os.system('chmod -R 777 {0}'.format(dictPath))
        user = sg.find_one("HumanUser", [["id", "is", userid]], ["login"])
        main_dict = {'user': user, 'plids': pl_ids}
        filev.write(str(main_dict))
        filev.close()
        cmd = 'python {0} {1} {2}'.format(hookPath, dictPath, main_path)
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        out, err = process.communicate()
        result += '<br>{0}'.format(out)
        if err is not None:
            result += '<br>Error: {0}'.format(err)
        proceso += str(out)
        return result

    def build_required_templates(self, pjct, flds, eng, config_path, specs, playlist):
        _main_template = eng.get_template_by_name("editorial_outgoing_folder")
        _main_path = _main_template.apply_fields(flds)
        base_package = os.path.basename(_main_path)
        pack_basename = specs['pack_name']
        project_code_start_at = specs['pack_naming_project_code_start_at']
        new_pack_base = pack_basename.format(pjct['code'][project_code_start_at:].lower(),
                                             base_package[:-1], base_package[-1])
        pack_basename = pack_basename.format(pjct['code'][project_code_start_at:].lower(),
                                             base_package[:-1], base_package[-1]) + '/'
        client_main_path = specs['editorial_outgoing_client_folder'] + pack_basename
        _main_path = _main_template.apply_fields({'date_folder': new_pack_base})
        return _main_template, _main_path, client_main_path

    def get_project_specs(self, project):
        self.conf_path = '/nfs/ovfxToolkit/Prod/Projects/{0}/'.format(project['code'])
        with open(self.conf_path + "/resources/project/project_specs.yml", 'r') as stream:
            try:
                specs = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        return specs

    def get_date(self, date='today'):
        format_date = None
        if date == 'today':
            format_date = "{0}".format(datetime.date.today().strftime('%Y%m%d'))
        return format_date


    def send_copy_job(self, specs, args, name, playlist, sg, main_path):

        prefixx = self.temp_folder
        userid = self.u_id
        user = sg.find_one("HumanUser", [["id", "is", userid]], ["login"])
        VALUES = {'playlists': playlist, 'user': user}
        (filepl, dictPathpl) = tempfile.mkstemp(prefix=prefixx + "outgoingPL_dict_", suffix='.txt')
        filepl = open(dictPathpl, 'w')
        filepl.write(str(VALUES))
        filepl.close()

        prefixx = self.temp_folder
        (filev, dictPath) = tempfile.mkstemp(prefix=prefixx + "outgoingF_dict_", suffix='.py')
        filev = open(dictPath, 'w')
        command = 'rsync -avhL {0}'.format(args)
        CLIENT_COMMAND_perm = 'ssh ingest chmod -R 777 "{0}"'.format(main_path)
        scrpt = '''
import sys
import os
import subprocess
process = subprocess.Popen('{0}', shell=True, stdout=subprocess.PIPE)
out, err = process.communicate()
print out, err
process2 = subprocess.Popen('{2}', shell=True, stdout=subprocess.PIPE)
out, err = process2.communicate()
if err == None:
    print 'PROCESS OUT: ', out
    print 'PROCESS ERRROR: ', err
    import sys
    sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
    import shotgun_api3
    import sys
    import ast
    import os
    file = open('{1}', 'r')
    print file 
    DICT = file.read()
    file.close()
    VALUES = ast.literal_eval(DICT)
    Playlist = VALUES['playlists']
    user = VALUES['user']
    SERVER_PATH = 'https://0vfx.shotgunstudio.com'
    SCRIPT_NAME = "Pack_tool"
    SCRIPT_KEY = 'gvuybjqkcrhgjseenVa&pe1nv'
    sg = shotgun_api3.Shotgun(SERVER_PATH, SCRIPT_NAME, SCRIPT_KEY)
    PL = sg.find_one('Playlist', [['id', 'is', Playlist['id']]], ['versions', 'code', 'project'])
    good_status_list = ['edapp', 'dlvr', 'fp4k']
    app_versions = []
    data = {{
            'title': 'Pack_' + str(PL['code']),
            'sg_playlist': PL,
            'project': PL['project'],
            'sg_status_list': 'ip',
            'created_by': user,
            'updated_by': user
    }}
    deliv = sg.find_one('Delivery', [['title', 'is', 'Pack_' + str(PL['code'])]])
    if deliv:
        sg.delete('Delivery',deliv['id'])
        deliv = sg.create('Delivery', data)
    else:
        deliv = sg.create('Delivery', data)
    for version in PL['versions']:
        vr = sg.find_one('Version', [['id', 'is', version['id']]],
                                    ['sg_status_list', 'code'])
        print vr['sg_status_list']
        if vr['sg_status_list'] in good_status_list or "v000" in vr["code"]:
            sg.update('Version', vr['id'], {{'sg_delivery_link': [deliv]}})
else:
    raise ValueError('No se puede empaquetar: ' + err)
        '''.format(command, dictPathpl, CLIENT_COMMAND_perm)
        filev.write(scrpt)
        filev.close()
        os.system('chmod -R 777 {0}'.format(dictPath))
        cmd = " -SubmitCommandLineJob -executable"
        cmd += " python -arguments "
        cmd += '{0}'.format(dictPath)
        cmd += ' -name "{0}" -pool ami'.format(name)
        cmd += ' -prop BatchName={0}'.format(playlist['code'])
        deadline = specs['deadline_command_path']
        process = subprocess.Popen(deadline + " " + cmd,
                                   shell=True, stdout=subprocess.PIPE)
        print deadline + " " + cmd
        out, err = process.communicate()
        print 'PROCESS: ', out, err, process
        print out
        return str(out)



def send_rename_and_copy_job(self, specs, path_to_frames, name, playlist, sg, main_path, np_name, dest_folder):

    prefixx = self.temp_folder
    userid = self.u_id
    user = sg.find_one("HumanUser", [["id", "is", userid]], ["login"])
    VALUES = {'playlists': playlist, 'user': user}
    (filepl, dictPathpl) = tempfile.mkstemp(prefix=prefixx + "outgoingPL_dict_", suffix='.txt')
    filepl = open(dictPathpl, 'w')
    filepl.write(str(VALUES))
    filepl.close()

    prefixx = self.temp_folder
    (filev, dictPath) = tempfile.mkstemp(prefix=prefixx + "outgoingF_dict_", suffix='.py')
    filev = open(dictPath, 'w')
    scrpt = '''
import sys
import os
import subprocess
import os
import zipfile
import subprocess
_src = "{0}"
orig= _src
command = 'rsync -avh ' + orig + ' ecampos@ingest:{2}/'
process2 = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
out, err = process2.communicate()
    '''.format(os.path.dirname(path_to_frames), np_name.split('.')[0], dest_folder)
    filev.write(scrpt)
    filev.close()
    os.system('chmod -R 777 {0}'.format(dictPath))
    cmd = " -SubmitCommandLineJob -executable"
    cmd += " python -arguments "
    cmd += '{0}'.format(dictPath)
    cmd += ' -name "{0}" -pool ami'.format(name)
    cmd += ' -prop BatchName={0}'.format(playlist['code'])
    deadline = specs['deadline_command_path']
    process = subprocess.Popen(deadline + " " + cmd, 
                               shell=True, stdout=subprocess.PIPE)
    print deadline + " " + cmd
    out, err = process.communicate()
    print 'PROCESS: ', out, err, process
    print out
    return str(out)
