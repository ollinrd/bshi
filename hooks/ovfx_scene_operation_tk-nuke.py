# Copyright (c) 2015 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import nuke
import sgtk
from sgtk import TankError
from sgtk.platform.qt import QtGui

HookClass = sgtk.get_hook_baseclass()
import nukescripts
class tpanel( nukescripts.PythonPanel):
    def __init__(self):
        nukescripts.PythonPanel.__init__(self, 'Crear Template')
        self.dropdown = nuke.Enumeration_Knob('Layer1', 'Template:', ['none', 'keyTemplate', 'projectionTemplate', 'simpleExtensionTemplate', 'cleaunup3DStab'])
        self.description = nuke.Multiline_Eval_String_Knob('Description', 'Description')
        self.description.setValue('no usar ningun template')
        self.update = nuke.PyScript_Knob('update', 'Update')
        self.addKnob( self.dropdown)
        self.addKnob( self.description)

    def knobChanged(self, knob):

        #check which knob was changed
        if knob == self.dropdown:
            if self.dropdown.value() == 'none':
                self.description.setValue('no usar ningun template')
            if self.dropdown.value() == 'keyTemplate':
                self.description.setValue('Base general para greenScreen/blueScreen')
            if self.dropdown.value() == 'projectionTemplate':
                self.description.setValue('Projeccion sencilla, ya sea con matte painting\no plato freezeado, o ambos,')
            if self.dropdown.value() == 'simpleExtensionTemplate':
                self.description.setValue('Extensiones sencillas, sin paralaje\no sin necesidad de camara 3d')
            if self.dropdown.value() == 'cleaunup3DStab':
                self.description.setValue('Cleanups con punto de intenres con \ncamara u objetos con mucho movimiento,\nrequiere camara 3D y objetos')



class SceneOperation(HookClass):
    """
    Hook called to perform an operation with the
    current scene
    """

    def execute(
        self,
        operation,
        file_path,
        context,
        parent_action,
        file_version,
        read_only,
        **kwargs
    ):
        print 'ovfx scene operation hook:', file_path
        self.set_basic_elements()
        engine = self.parent.engine

        if hasattr(engine, "hiero_enabled") and (
            engine.hiero_enabled or engine.studio_enabled
        ):
            return self._scene_operation_hiero_nukestudio(
                operation,
                file_path,
                context,
                parent_action,
                file_version,
                read_only,
                **kwargs
            )
        self.USER = context.user['name']
        self.current_context = context
        # If we didn't hit the Hiero or Nuke Studio case above, we can
        # continue with the typical Nuke scene operation logic.
        print '\tOperation:' + str(operation)
        if file_path:
            file_path = file_path.replace("/", os.path.sep)

        if operation == "current_path":
            # return the current script path
            return nuke.root().name().replace("/", os.path.sep)
        elif operation == "prepare_new":
            self.prepare_new_operations()
        elif operation == "open":
            # open the specified script and set scene
            self.open_operations(file_path)
            self.ProjectTools()
        elif operation == "save":
            # save the current script:
            nuke.scriptSave()
            self.ProjectTools()
        elif operation == "save_as":
            self.save_operations(file_path)
            self.ProjectTools()

        elif operation == "reset":
            """
            Reset the scene to an empty state
            """
            return self.reset_operations()
            self.ProjectTools()


    def set_basic_elements(self):
        self.shot_fields = ['code', 'sg_head_in', 'sg_tail_out',
                            'sg_resx', 'sg_resy', 'sg_format_custom_list',
                            'sg_format_custom', 'sg_sequence', 'sg_camera',
                            'sg_working_duration', 'sg_resize', 'sg_respeed',
                            'sg_director_notes', 'sg_editorial_notes',
                            'sg_project_lut_type', 'sg_pixel_aspect',
                            'sg_frame_rate', 'sg_sequence.Sequence.code']

        self.seq_fields = ['code', 'sg_format_custom', 'sg_format_custom_list']

        self.project_fields = ['code', 'sg_format_custom', 'sg_centric_config_path']

    def prepare_new_operations(self):
        context = self.current_context
        sg = self.parent.shotgun
        self.validate_NukeCachePath()
        self.statusTaskValidation()
        if context.entity['type'] == 'Shot':
            self.set_basic_elements()
            filters = [['id', 'is', context.project['id']],
                       ['name', 'is', context.project['name']]]
            self.current_project = sg.find_one('Project', filters,
                                               self.project_fields)
            filters = [['id', 'is', context.entity['id']]]
            self.current_shot = sg.find_one('Shot', filters,
                                            self.shot_fields)
            filters = [['id', 'is', self.current_shot['sg_sequence']['id']]]
            self.current_seq = sg.find_one('Sequence', filters,
                                           self.seq_fields)
            shot = self.current_shot
            if shot['sg_tail_out'] and shot['sg_head_in']:
                nuke.root()['last_frame'].setValue(shot['sg_tail_out'])
                nuke.root()['first_frame'].setValue(shot['sg_head_in'])
            self.get_format()
            self.ask_for_comp_template()
        self._set_color_config()  
        nuke.root()['lock_range'].setValue(True)

    def ask_for_comp_template(self):
        x = tpanel()
        x.showModalDialog()
        tk = self.parent.sgtk
        config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        compTemplatePath = config_path + '/resources/Comp_templates/'
        if x.dropdown.value() == 'keyTemplate':
            nuke.nodePaste(compTemplatePath + 'keyTemplate.nk')
        if x.dropdown.value() == 'projectionTemplate':
            nuke.nodePaste(compTemplatePath + 'projectionTemplate.nk')
        if x.dropdown.value() == 'simpleExtensionTemplate':
            nuke.nodePaste(compTemplatePath + 'simpleExtensionTemplate.nk')
        if x == 'cleaunup3DStab':
            nuke.nodePaste(compTemplatePath + 'cleanup3DStabTemplate.nk')


        print 'equis', x.dropdown.value()


    def _restart_engine(self, ctx):
        """
        Set context to the new context.  This will
        clear the current scene and restart the
        current engine with the specified context
        """
        
        # restart engine:        
        try:
            current_engine_name = self.parent.engine.name
            
            # stop current engine:            
            if sgtk.platform.current_engine(): 
                sgtk.platform.current_engine().destroy()
                
            # start engine with new context:
            sgtk.platform.start_engine(current_engine_name, ctx.tank, ctx)
        except Exception, e:
            raise TankError("Failed to change work area and start a new engine - %s" % e)




    def open_operations(self, file_path):
        nuke.scriptOpen(file_path)
        context = self.current_context
        sg = self.parent.shotgun
        self.validate_NukeCachePath()
        self.statusTaskValidation()
        if context.entity['type'] == 'Shot':
            self.set_basic_elements()
            filters = [['id', 'is', context.project['id']],
                       ['name', 'is', context.project['name']]]
            self.current_project = sg.find_one('Project', filters,
                                               self.project_fields)
            filters = [['id', 'is', context.entity['id']]]
            self.current_shot = sg.find_one('Shot', filters,
                                            self.shot_fields)
            filters = [['id', 'is', self.current_shot['sg_sequence']['id']]]
            self.current_seq = sg.find_one('Sequence', filters,
                                           self.seq_fields)
            shot = self.current_shot
            if shot['sg_tail_out'] and shot['sg_head_in']:
                nuke.root()['last_frame'].setValue(shot['sg_tail_out'])
                nuke.root()['first_frame'].setValue(shot['sg_head_in'])
            self.get_format()
            self.update_backdrop()
        self._set_color_config()

        # reset any write node render paths:
        if self._reset_write_node_render_paths():
            # something changed so make sure to save the script again:
            nuke.scriptSave()
        nuke.root()['lock_range'].setValue(True)

    def save_operations(self, file_path):
        context = self.current_context
        sg = self.parent.shotgun
        if context.entity['type'] == 'Shot':
            self.set_basic_elements()
            filters = [['id', 'is', context.project['id']],
                       ['name', 'is', context.project['name']]]
            self.current_project = sg.find_one('Project', filters,
                                               self.project_fields)
            filters = [['id', 'is', context.entity['id']]]
            self.current_shot = sg.find_one('Shot', filters,
                                            self.shot_fields)
            filters = [['id', 'is', self.current_shot['sg_sequence']['id']]]
            self.current_seq = sg.find_one('Sequence', filters,
                                           self.seq_fields)
            shot = self.current_shot
            if shot['sg_tail_out'] and shot['sg_head_in']:
                nuke.root()['last_frame'].setValue(shot['sg_tail_out'])
                nuke.root()['first_frame'].setValue(shot['sg_head_in'])
            # self.get_format()
        old_path = nuke.root()["name"].value()
        self._set_color_config()
        try:
            # rename script:
            nuke.root()["name"].setValue(file_path)
            # reset all write nodes:
            self._reset_write_node_render_paths()
            # save script:
            nuke.scriptSaveAs(file_path, -1)
        except Exception as e:
            # something went wrong so reset to old path:
            nuke.root()["name"].setValue(old_path)
            raise TankError("Failed to save scene %s", e)
        nuke.root()['lock_range'].setValue(True)

    def reset_operations(self):
        while nuke.root().modified():
            # changes have been made to the scene
            res = QtGui.QMessageBox.question(
                None,
                "Save your script?",
                "Your script has unsaved changes. Save before proceeding?",
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No | QtGui.QMessageBox.Cancel)
            if res == QtGui.QMessageBox.Cancel:
                return False
            elif res == QtGui.QMessageBox.No:
                break
            else:
                nuke.scriptSave()
        # now clear the script:
        nuke.scriptClear()
        return True

    def get_format(self):
        h_res = False
        v_res = False
        SHOT = self.current_shot
        # Info taken from shotgun shot info
        if not SHOT['sg_resx']:
            nuke.message('ERROR:\nHorizontal resolution not set in Shotgun.\nPlease notify EDITORIAL department to fix this problem.')
        else:
            h_res = SHOT['sg_resx']
        if not SHOT['sg_resy']:
            nuke.message('ERROR:\nVertical resolution not set in Shotgun.\nPlease notify EDITORIAL department to fix this problem.')
        else:
            v_res = SHOT['sg_resy']
        if (not h_res or not v_res):
            nuke.message('ERROR:\nResolution not found for shot.\nnotify EDITORIAL department')
        else:
            projName = self.current_project['code']

            currentFormat = ''
            if SHOT['sg_pixel_aspect'] == 2:
                base_name = '{0}_anam_SG'.format(projName)
            else:
                base_name = '{0}_SG'.format(projName)
            createFormat = True
            sameFormats = []
            repeatedFormats = []
            for f in nuke.formats():
                try:
                    if f.width() == h_res and f.height() == v_res and f.pixelAspect() == float(SHOT['sg_pixel_aspect']):
                        createFormat = False
                        if f.name():
                            if base_name in f.name():
                                sameFormats.append(f)
                            else:
                                repeatedFormats.append(f)
                        else:
                            repeatedFormats.append(f)
                except:
                    pass
            if createFormat:
                newName = self.get_unique_name(base_name)
                currentFormat = '{0} {1} {2}'.format(h_res, v_res, base_name)
                if SHOT['sg_pixel_aspect']:
                    currentFormat = '{0} {1} {3} {2}'.format(h_res, v_res, base_name, SHOT['sg_pixel_aspect'])

                currentFormat = nuke.addFormat(currentFormat)
            else:
                if len(sameFormats):
                    minFormatName = sameFormats[0]
                    for f in sameFormats:
                        if len(f.name()) < len(minFormatName.name()):
                            minFormatName = f
                    currentFormat = minFormatName
                else:
                    newName = self.get_unique_name(base_name)
                    currentFormat = repeatedFormats[0]
                    currentFormat.setName(newName)
            count = 0
            for f in nuke.formats():
                if f.name() != currentFormat.name():
                    if f.width() == h_res and f.height() == v_res:
                        count += 1
                        f.setName('undefined_{0}'.format(count))
            if nuke.root()['format'].value() != currentFormat.name():
                nuke.root()['format'].setValue(currentFormat.name())
                nuke.knobDefault("Root.format", currentFormat.name())
                nuke.knobDefault("Read.proxy_format", currentFormat.name())
            for n in nuke.allNodes():
                knobs = n.knobs()
                if 'format' in knobs:
                    nFormat = n['format'].value()
                    try:
                        if nFormat.width() == h_res and nFormat.height() == v_res:
                            n['format'].setValue(currentFormat)
                            pxy = 'proxy_format'
                            if pxy in knobs:
                                n[pxy].setValue(currentFormat)
                    except:
                        pass

    def get_unique_name(self, name):
        validName = True
        for f in nuke.formats():
            if name == f.name():
                validName = False
                new_name = name + "_01"
                if name[-3] == '_' and name.split('_')[-1].isdigit():
                    tmp = name.split('_')[-1]
                    convert = int(tmp) + 1
                    oldS = '_'+tmp
                    newS = '_'+'%02d' % convert
                    new_name = name.replace(name.replace(oldS, newS))
                return self.get_unique_name(new_name)
        if validName:
            return name

    def validate_NukeCachePath(self):
        nuke_tmp = os.getenv('NUKE_TEMP_DIR')
        userId = os.getuid()
        correct_nukeTemp = "/ollin/tmp/" + "nuke-u" + str(userId)
        if nuke_tmp != correct_nukeTemp:
            os.environ['NUKE_TEMP_DIR'] = correct_nukeTemp

    def statusTaskValidation(self):
        context = self.current_context
        sg = self.parent.shotgun
        # Getting task status from Shotgun db
        task = sg.find_one('Task',
                           [['project', 'is', context.project],
                            ['id', 'is', context.task['id']]],
                           ['sg_status_list'])
        # Possible status to verify if the artist
        # could change to in progress
        modif_status = ['wtg', 'hld']
        status_code = task['sg_status_list']
        if status_code in modif_status:
            c_status = sg.find_one('Status',
                                   [['code', 'is', status_code]],
                                   ['name', 'code'])
            status_name = c_status['name']
            msgg = ' Attention !!!!!\n'
            msgg += 'This task status is\n'
            msgg += '>>>>  {0}  <<<<'.format(status_name)
            msgg += '\nDo yo want to change to "In progress"?'
            opt = nuke.ask(msgg)
            if opt:
                # Getting "In progress" SG Status
                data = {'sg_status_list': 'ip'}
                sg.update('Task', context.task['id'], data)

    def ProjectTools(self):
        menu = nuke.menu('Nuke').addMenu('Project Tools')
        nuke.paste_roto_template = self.paste_roto_template
        menu.addCommand("Roto Template", "nuke.paste_roto_template()", "")

    def paste_roto_template(self):
        
        config_p = self.current_project['sg_centric_config_path'].split('bundle_cache')[0]
        nuke.nodePaste(config_p + '/resources/roto_template.nk')

    def update_backdrop(self):
        sg = self.parent.shotgun
        shot = self.current_shot
        # Get the elements associated to the shot
        elements = sg.find('Element', [['shots', 'is', shot]], ['code'])
        e_names = [i['code'] + '\n' for i in elements]
        e_names = ''.join(e_names)
        # Get the asset linked to the shot
        assets = sg.find('Asset', [['shots', 'is', shot]], ['code'])
        a_names = [i['code'] + '\n' for i in assets]
        a_names = ''.join(a_names)
        # Populate shot info into backdrop text
        b_txt = 'EDITORIAL INFO ---------------------------------------\n\n'
        b_txt += 'HEAD IN: {0}\n'.format(shot['sg_head_in'])
        b_txt += 'TAIL OUT: {0}\n'.format(shot['sg_tail_out'])
        b_txt += 'WORKING DURATION: {0}\n'.format(shot['sg_working_duration'])
        b_txt += 'RESPEED: {0}\n'.format(shot['sg_respeed'])
        b_txt += 'RESIZE: {0}\n\n'.format(shot['sg_resize'])
        b_txt += 'NOTES -----------------------------------------\n\n'
        b_txt += 'CLIENT REQUEST:\n{0}\n\n'.format(shot['sg_director_notes'])
        b_txt += 'EDITORIAL:\n{0}\n\n'.format(shot['sg_editorial_notes'])
        b_txt += 'LINKS -----------------------------------------\n\n'
        b_txt += 'ELEMENTS:\n{0}\n'.format(e_names)
        b_txt += 'ASSETS:{0}'.format(a_names)
        backdrop_text = b_txt
        nuke.zoom(0)
        # Validate if the backdrop exists
        bd_name = 'BD_' + shot['code']
        bd = nuke.toNode(bd_name)
        if bd is None:
            bd = nuke.nodes.BackdropNode()
        # Update values
        xC = bd.xpos() + 200
        yC = bd.ypos() + 200
        bd['bdwidth'].setValue(400)
        bd['bdheight'].setValue(500)
        bd['name'].setValue(bd_name)
        bd['label'].setValue(backdrop_text)
        bd['note_font_size'].setValue(14)
        # Zoom to BackDrop Node
        nuke.zoom(1, [xC, yC])

    def _set_color_config(self):
        self._set_color_nuke()
        nuke.removeOnCreate(self._set_read, nodeClass="Read")
        nuke.addOnCreate(self._set_read, nodeClass="Read")
        nuke.removeOnCreate(self._setup_lut, nodeClass="Viewer")
        nuke.addOnCreate(self._setup_lut, nodeClass="Viewer")

    def _set_color_nuke(self):
        nuke.root()['colorManagement'].setValue('OCIO')
        nuke.root()['OCIO_config'].setValue('aces_1.0.3')
        nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
        nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
        nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
        nuke.root()['int16Lut'].setValue('ACES - ACEScc')
        nuke.root()['logLut'].setValue('Input - ADX - ADX10')
        nuke.root()['floatLut'].setValue('ACES - ACES2065-1')

    def _set_read(self):
        read = nuke.thisNode()
        read['colorspace'].setValue('ACES - ACEScg')
        print 'reaad file', read['file'].value()
        if '/nfs/ollinvfx/Project/BSHI/' not in read['file'].value():
            if '/nfs/library/' not in read['file'].value():
                nuke.message('Este read no corresponde a nunguna ruta valida de poryecto\nPorfavor leventa un ticket para copiarlo en una ruta correcta')
        if '/elements/' in read['file'].value() and 'mp0' in read['file'].value():
            read['name'].setValue('_ELEMENT_' + os.path.basename(read['file'].value()).split('.')[0])

    def _setup_lut(self, startup=None):
        viewer = nuke.thisNode()
        if viewer:
            if 'Rec.709 (ACES)' in viewer['viewerProcess'].values():
                viewer['viewerProcess'].setValue('Rec.709 (ACES)')
                print 'End of assigned viewer process Rec.709 (ACES)'
        return
        config_path = self.parent.context.tank.pipeline_configuration.get_path()
        lutPath = '/config/resources/LUT_GROUP.nk'
        lut = config_path + lutPath
        lutName = 'LUT_BSHI'
        lut_node = nuke.toNode(lutName)
        if lut_node:
            nuke.delete(lut_node)
            lut_node = None
        if not lut_node and startup is None:
            print lut
            if os.path.exists(lut):
                lut_node = nuke.nodePaste(lut)
                lut_node.knob('selected').setValue(False)
            else:
                nuke.message('Warning, project lut does not exists! {0}'.format(lut))
        if lut_node:
            downDepencies = []
            for n2 in [n for n in nuke.allNodes() if n is not lut_node]:
                [downDepencies.append(n2) for dp in n2.dependencies() if dp == lut_node]
            for p in downDepencies:
                # Replace input connections
                for i in range (0, p.inputs()):
                    if p.input(i) == lut_node:
                        p.setInput(i, lut_node.input(0))
            lut_node.setInput(0, None)
        viewer = nuke.thisNode()
        if viewer:
            if 'None' in viewer['viewerProcess'].values():
                print 'assigned viewer process None'
                viewer['viewerProcess'].setValue('None')
                print 'End of assigned viewer process None'
            else:
                for val in viewer['viewerProcess'].values():
                    if 'Raw' in val:
                        viewer['viewerProcess'].setValue(val)
            viewer["input_process"].setValue(True)
            viewer.setInput(0, None)

    def _get_current_hiero_project(self):
        """
        Returns the current project based on where in the UI the user clicked
        """
        import hiero
        # get the menu selection from hiero engine
        selection = self.parent.engine.get_menu_selection()

        if len(selection) != 1:
            raise TankError("Please select a single Project!")
        if not isinstance(selection[0], hiero.core.Bin):
            raise TankError("Please select a Hiero Project!")
        project = selection[0].project()
        if project is None:
            # apparently bins can be without projects (child bins I think)
            raise TankError("Please select a Hiero Project!")
        return project

    def _reset_write_node_render_paths(self):
        """
        Use the tk-nuke-writenode app interface to find and reset
        the render path of any Shotgun Write nodes in the current script
        """
        write_node_app = self.parent.engine.apps.get("tk-nuke-writenode")
        if not write_node_app:
            return False
        # only need to forceably reset the write node render paths if the app version
        # is less than or equal to v0.1.11
        from distutils.version import LooseVersion
        if write_node_app.version == "Undefined" or LooseVersion(
            write_node_app.version
        ) > LooseVersion("v0.1.11"):
            return False
        write_nodes = write_node_app.get_write_nodes()
        for write_node in write_nodes:
            write_node_app.reset_node_render_path(write_node)
        return len(write_nodes) > 0

    def _scene_operation_hiero_nukestudio(
        self,
        operation,
        file_path,
        context,
        parent_action,
        file_version,
        read_only,
        **kwargs
    ):
        import hiero
        if operation == "current_path":
            # return the current script path
            project = self._get_current_hiero_project()
            curr_path = project.path().replace("/", os.path.sep)
            return curr_path
        elif operation == "open":
            hiero.core.events.sendEvent("kBeforeProjectLoad", None)
            # open the specified script
            hiero.core.openProject(file_path.replace(os.path.sep, "/"))
        elif operation == "save":
            # save the current script:
            project = self._get_current_hiero_project()
            project.save()
        elif operation == "save_as":
            project = self._get_current_hiero_project()
            project.saveAs(file_path.replace(os.path.sep, "/"))
            # ensure the save menus are displayed correctly
            _update_save_menu_items(project)
        elif operation == "reset":
            # do nothing and indicate scene was reset to empty
            return True
        elif operation == "prepare_new":
            # add a new project to hiero
            hiero.core.newProject()


def _update_save_menu_items(project):
    import hiero
    project_path = project.path()
    # get the basename of the path without the extension
    file_base = os.path.splitext(os.path.basename(project_path))[0]
    save_action = hiero.ui.findMenuAction("foundry.project.save")
    save_action.setText("Save Project (%s)" % (file_base,))
    save_as_action = hiero.ui.findMenuAction("foundry.project.saveas")
    save_as_action.setText("Save Project As (%s)..." % (file_base,))
