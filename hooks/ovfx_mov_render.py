import os
import sgtk
import nuke
import sys
HookClass = sgtk.get_hook_baseclass()

class MatteColor(HookClass):
    def execute(self, config_path, sg, entity, reads_info,
                mov_path, still, type_, **kwargs):
        nuke.root()['colorManagement'].setValue('OCIO')
        nuke.root()['OCIO_config'].setValue('aces_1.0.3')
        nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
        nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
        nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
        nuke.root()['int16Lut'].setValue('ACES - ACEScc')
        nuke.root()['logLut'].setValue('Input - ADX - ADX10')
        nuke.root()['floatLut'].setValue('ACES - ACES2065-1')
        indic = 0
        sg_project = sg.find_one(entity['type'], [['id', 'is', entity['id']]], ['project'])['project']
        tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
        config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        lut_node = nuke.nodes.OCIOColorSpace(in_colorspace='ACES - ACEScg',
                                             out_colorspace='Output - Rec.709')
        lut_node['disable'].setValue(True)
        if entity['id'] in [12986] and entity['type'] == 'Shot':
            lut_node['disable'].setValue(False)

        appen = nuke.createNode('AppendClip')
        for read_info in reads_info:
            indic = self.init_read_node(read_info['path'], read_info['sf'],
                                        read_info['lf'], appen, indic, entity)
        if not still:
            mov_node = self.set_write_node(mov_path, config_path)
            lut_node.setInput(0, appen)
            mov_node.setInput(0, lut_node)
            if type_ == 'matte':
                final_frame = indic + 1
            nuke.execute(mov_node, 1, final_frame+7, 1,
                         [nuke.views()[0]])
            nuke.execute(mov_node, 1, final_frame, 1,
                         [nuke.views()[0]])
        else:
            mov_node = self.set_write_node_still(mov_path, config_path)
            lut_node.setInput(0, appen)
            mov_node.setInput(0, lut_node)
            if type_ == 'matte':
                final_frame = indic + 1
            nuke.execute(mov_node, 1, 1, 1,
                         [nuke.views()[0]])
            nuke.execute(mov_node, 1, 1, 1,
                         [nuke.views()[0]])

    def set_write_node_still(self, mov_path, config_path):
        writeNodePath = '{0}/resources/st_al_publish/still_write.nk'.format(config_path)
        mov_node = nuke.nodePaste(writeNodePath)
        mov_node['file'].setValue(mov_path.replace('.mov', '.jpg'))
        mov_node['raw'].setValue(True)
        return mov_node

    def set_write_node(self, mov_path, config_path, ENTITY):
        writeNodePath = '{0}/resources/st_al_publish/movie_write.nk'.format(config_path)
        mov_node = nuke.nodePaste(writeNodePath)
        mov_node['file'].setValue(mov_path)
        if ENTITY['id'] in [12986] and ENTITY['type'] == 'Shot':
            mov_node['raw'].setValue(True)
        return mov_node

    def init_read_node(self, read_path, sf, lf, appen, indic, ENTITY):
        read = nuke.nodes.Read(file=read_path, first=sf, last=lf)
        read['colorspace'].setValue('ACES - ACEScg')
        if ENTITY['id'] in [12986] and ENTITY['type'] == 'Shot':
            read['raw'] = True
        appen.setInput(indic, read)
        indic += 1
        return indic
