import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml

def get_project_specs(config_path):
    with open(config_path + "/config/resources/project/project_specs.yml", 'r') as stream:
        try:
            specs = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return specs

def get_client_template(engine, entity, config_path):
    specs = get_project_specs(config_path)
    exr_template_name = specs['image_client_template_exr']
    internal_template = engine.get_template_by_name(exr_template_name)
    return internal_template