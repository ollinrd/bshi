from tank import Hook
import subprocess
import tempfile
import os
import sys
import subprocess
import datetime
import yaml
import tempfile
import datetime
import yaml
import zipfile
import sgtk

class PackagePlaylistHook(Hook):
    def execute(self, diction, engine, tk, ctx, **kwargs):
        if diction['entity_type'] == 'Shot':
            fields = ['sg_head_in', 'sg_tail_out', 'code', 'sg_camera',
                      'sg_main_plate_name', 'sg_resx', 'sg_resy', 'sg_str_timecode',
                      'sg_sequence', 'project', 'sg_pvz', 'sg_sequence.Sequence.code']
            self.sg = engine.shotgun
            self.tk = tk
            self.ctx = ctx
            self.engine = engine
            self.diction = diction
            self.entity = self.sg.find_one('Shot',
                                           [['id', 'is', int(diction['entity_id'])]],
                                           fields)
            if diction['tipo'] == 'seq':
                self.send_publish_seq()
            if diction['tipo'] in ['nk', 'ma', 'abc', 'mb', '3de']:
                tipo = diction['tipo']
                self.send_publish_scene(tipo)


    def send_publish_scene(self, tipo):
            project = self.sg.find_one('Project', [['id', 'is', self.entity['project']['id']]], ['code'])
            task = self.sg.find_one('Task', [['content', 'is', self.diction['task_name']],
                                             ['entity', 'is', self.entity]], ['content','step.Step.short_name'])
            fields ={'Step': task['step.Step.short_name'],
                     'task_name': task['content'],
                     'project_code': project['code'],
                     'Shot': self.entity['code'],
                     'name': self.diction['name'],
                     'version': self.diction['version'],
                     'width': self.entity['sg_resx'],
                     'height': self.entity['sg_resy'],
                     'Sequence': self.entity['sg_sequence.Sequence.code']
                     }

            if tipo == 'nk':
                w_template = self.engine.get_template_by_name('nuke_shot_publish')
                file_type = "Nuke Script"

            if tipo in ['ma', 'mb']:
                if '_cam_' in self.diction['file_path']:
                    w_template = self.engine.get_template_by_name('maya_shot_camera')
                    fields['maya_extension'] = tipo
                    file_type = "Maya Camera"
                if '_geo_' in self.diction['file_path']:
                    w_template = self.engine.get_template_by_name('maya_shot_geo')
                    fields['maya_extension'] = tipo
                    file_type = "Maya Geo"
                else:
                    w_template = self.engine.get_template_by_name('maya_shot_publish')
                    fields['maya_extension'] = tipo
                    file_type = "Maya Scene"

            if tipo == 'abc':
                if '_cam_' in self.diction['file_path']:
                    w_template = self.engine.get_template_by_name('maya_shot_alembic_cam')
                    file_type = "Alembic Camera"
                if '_geo_' in self.diction['file_path']:
                    w_template = self.engine.get_template_by_name('maya_shot_alembic_geo')
                    file_type = "Alembic Geo"
                else:
                    w_template = self.engine.get_template_by_name('shot_alembic_cache')
                    file_type = "Alembic Cache"

            if tipo == 'obj':
                    w_template = self.engine.get_template_by_name('shot_obj_cache')
                    file_type = "Obj File"

            if tipo == '3de':
                    w_template = self.engine.get_template_by_name('shot_3de')
                    fields['maya_extension'] = tipo
                    file_type = "3de File"

            script_path = w_template.apply_fields(fields)
            self.engine.ensure_folder_exists(os.path.dirname(script_path))
            comment = self.diction['comments']
            filt = [['project', 'is', project],
                    ['code', 'is', os.path.basename(script_path)]]
            p_file = self.sg.find_one('PublishedFile', filt)
            if p_file:
                import sys
                sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
                from shotgun_api3 import Shotgun
                SG_URL = 'https://0vfx.shotgunstudio.com'
                TANK_SCRIPT_NAME= "Toolkit"
                TANK_SCRIPT_KEY= "b20e30ae264400f844d4197b5805d1105ba6ddf06fd733b51a74b8fa290cbc73"
                shgn = Shotgun(SG_URL, 
                               TANK_SCRIPT_NAME, 
                               TANK_SCRIPT_KEY)
                shgn.delete("PublishedFile", p_file["id"])

            self.engine.sgtk.synchronize_filesystem_structure()
            self.engine.sgtk.create_filesystem_structure('Task', task['id'],
                                                         engine=None)
            copy_cmd = 'rsync -avh {0} {1}'.format(self.diction['file_path'],
                                                   script_path)
            process = subprocess.Popen(copy_cmd,
                                       shell=True, stdout=subprocess.PIPE)
            out, err = process.communicate()
            file_to_publish = script_path
            self.tk = sgtk.sgtk_from_entity(task['type'], task['id'])
            self.ctx = self.tk.context_from_entity(task['type'], task['id'])
            p_file = sgtk.util.register_publish(self.tk,
                                                self.ctx,
                                                file_to_publish,
                                                os.path.basename(script_path).split('.')[0], 
                                                published_file_type=file_type,
                                                version_number=self.diction['version'],
                                                task=task,
                                                description=comment)
            print p_file




    def send_publish_seq(self):
            prefixx = '/nfs/ovfxToolkit/TMP/'
            (file_, dictPath) = tempfile.mkstemp(prefix=prefixx + "publish_job_submitter_{0}".format(self.entity['code']),
                                                 suffix='.py')
            project = self.sg.find_one('Project', [['id', 'is', self.entity['project']['id']]], ['code'])
            self.burnin = '/nfs/ovfxToolkit/Prod/Projects/{0}/resources/ovfx_BSHI_Burnin.nk'.format(project['code'])
            task = self.sg.find_one('Task', [['content', 'is', self.diction['task_name']],
                                             ['entity', 'is', self.entity]], ['content','step.Step.short_name'])
            p_template = self.engine.get_template_by_name('nuke_shot_render_pub_exr')
            ps_template = self.engine.get_template_by_name('nuke_shot_publish')
            w_template = self.engine.get_template_by_name('nuke_shot_work')
            mov_template = self.engine.get_template_by_name('nuke_shot_render_movie')
            
            fields ={'Step': task['step.Step.short_name'],
                     'task_name': task['content'],
                     'project_code': project['code'],
                     'Shot': self.entity['code'],
                     'name': self.diction['name'],
                     'version': self.diction['version'],
                     'width': self.entity['sg_resx'],
                     'height': self.entity['sg_resy'],
                     'Sequence': self.entity['sg_sequence.Sequence.code']
                     }
            print fields
            
            p_path = p_template.apply_fields(fields)
            fields2 = fields
            pfields = fields
            pfields['name'] = self.diction['vendor']
            ps_path = ps_template.apply_fields(pfields)
            script_path = w_template.apply_fields(pfields)
            mov_path = mov_template.apply_fields(pfields)

            self.engine.ensure_folder_exists(os.path.dirname(p_path))
            self.engine.ensure_folder_exists(os.path.dirname(script_path))
            first_frame = self.entity['sg_head_in']
            last_frame = self.entity['sg_tail_out']
            thumbpath = thumbnail_path = self.diction['file_path'].replace('%04d',str(first_frame))

            comment = self.diction['comments']
            p_file = self.sg.find_one('PublishedFile', [['project', 'is', project], ['code', 'is', os.path.basename(ps_path)]])
            self.engine.sgtk.synchronize_filesystem_structure()
            self.engine.sgtk.create_filesystem_structure('Task', task['id'], engine=None)
            ps_path = ps_path
            if not os.access(ps_path, os.F_OK):
                os.system('touch {0}'.format(ps_path))
            file_to_publish = ps_path
            self.tk = sgtk.sgtk_from_entity(task['type'], task['id'])    
            self.ctx = self.tk.context_from_entity(task['type'], task['id'])
            if not p_file:
                p_file = sgtk.util.register_publish(
                  self.tk,
                  self.ctx,
                  file_to_publish, 
                  os.path.basename(ps_path).split('.')[0], 
                  published_file_type="Nuke Script",
                  version_number = self.diction['version'],
                  task= task,
                  description = comment)

            sg_publish_data = p_file

            main_plate, main_plate_last_frame = self.get_mainplate_reference(self.entity)
            ALPHA = False
            ocio_path_ = ''
            WNODE = 'PublisherEXT'
            current_user = self.sg.find_one('CustomNonProjectEntity12', [['sg_vendor_code', 'is', self.diction['vendor']]],
                                            ['sg_vendor_assigned_artist'])['sg_vendor_assigned_artist']
            fields2['name'] = self.diction['name']
            script_path = w_template.apply_fields(fields2)
            print 'aaaaaaaaaaaaaaaaaa', script_path, pfields
            colorspace = 'ACES - ACEScg'
            user_data = {"is_wip": False}
            review_args = [p_path, pfields,
                           script_path, current_user,
                           first_frame,
                           last_frame,
                           [sg_publish_data], task, comment,
                           thumbnail_path, user_data, colorspace]
            roto = False
            novid = False
            if 'rot' in task['step.Step.short_name'].lower():
                roto = True
            if 'udplate' in fields['name'].lower() or 'cones' in fields['name'].lower():
                novid = True
            data = {
                    'burnin': self.burnin,
                    'first': first_frame,
                    'last': last_frame,
                    'exr_path': p_path,
                    'name': os.path.basename(script_path).split('.')[0],
                    'sgtk_path': self.engine.context.tank.pipeline_configuration.get_path(),# '/nfs/ovfxToolkit/Prod/Projects/{0}'.format(project['code']),
                    'task': task,
                    'review_args': review_args,
                    'orig_path': self.diction['file_path'],
                    'main_plate': main_plate,
                    'main_plate_last_frame': main_plate_last_frame,
                    'WNODE': WNODE,
                    'timecode': self.entity['sg_str_timecode'],
                    'script_path': script_path,
                    'comment': self.diction['comments'],
                    'publish_version': self.diction['version'],
                    'roto': roto,
                    'novid': novid,

            }
            reformat_script = """
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import nuke
import sys
import os
from stat import S_ISREG, ST_CTIME, ST_MODE
from subprocess import Popen, PIPE
import tempfile
import subprocess
import uuid

nuke.nodePaste('{burnin}')
nuke.root()['colorManagement'].setValue('OCIO')
nuke.root()['OCIO_config'].setValue('aces_1.0.3')
nuke.root()['workingSpaceLUT'].setValue('Input - RED - Linear - REDWideGamutRGB')
nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
nuke.root()['int8Lut'].setValue('Output - Rec.709 (D60 sim.)')
nuke.root()['int16Lut'].setValue('Input - RED - REDLog3G10 - REDWideGamutRGB')
nuke.root()['logLut'].setValue('Input - RED - REDLog3G10 - REDWideGamutRGB')
nuke.root()['floatLut'].setValue('Input - RED - Linear - REDWideGamutRGB')


sw_startFrame = {first}
sw_endFrame = {last}

nuke.frame(sw_startFrame)

nuke.root()['first_frame'].setValue({first})
nuke.root()['last_frame'].setValue({last})
publisher = nuke.toNode('PUBLISHER')


metadata_node = nuke.toNode('PUBLISHER').node('internal').node('Meta')
metadata_node['file'].setValue(\"{main_plate}\")
metadata_node['first'].setValue({first})
metadata_node['last'].setValue({main_plate_last_frame})
metadata_node['origfirst'].setValue({first})
metadata_node['origlast'].setValue({main_plate_last_frame})

# metadata timeCode
timecode = '{timecode}'
print 'Time code: ', timecode
tc = nuke.toNode('PUBLISHER').node("internal").node("metaTimeCode")
tc['startcode'].setValue(timecode)
tc['useFrame'].setValue(1)
tc['frame'].setValue({first} - 1)

sw_startFrame = {first}
sw_endFrame = {main_plate_last_frame}

nuke.frame(sw_startFrame)
switch = nuke.toNode('PUBLISHER').node("internal").node("Switch1")
switch_which= switch['which']
switch_which.setAnimated()
switch_which.setValueAt( 0, sw_startFrame )
switch_which.setValueAt( 1, sw_endFrame +1 )


read = nuke.nodes.Read(name="read_source", file='{orig_path}', first={first}, last={last})

publisher.setInput(0, read)

read['colorspace'].setValue('ACES - ACEScg')
write_n = publisher.node('internal').node('{WNODE}')

write_n['file'].setValue('{exr_path}')
if {roto}:
    write_n['channels'].setValue('rgba')
prefixx = '/nfs/ovfxToolkit/TMP/'
dictPath = prefixx + "Reformat_publish_{name}" + str(uuid.uuid4()) +'.nk'
nuke.nuke.scriptSaveAs(dictPath)
    """.format(**data)
            print 'dict_path: ', dictPath
            submitter_py = open(dictPath, "w")
            submitter_py.write(reformat_script)
            submitter_py.close()

            # batchName = os.path.basename(script_path).split('.')[0]

            # current_version = nuke.NUKE_VERSION_STRING
            flags = '" -t -m 2 -V -x -X PUBLISHER.internal.PublisherEXT'
            flags += ' <QUOTE>' + dictPath + '<QUOTE> <STARTFRAME%4>,<STARTFRAME%4>"'
            nuke_cmd = "/opt/Thinkbox/Deadline10/bin/deadlinecommand -SubmitCommandLineJob -executable"
            nuke_cmd += " /usr/local/Nuke11.3v4/Nuke11.3 -arguments "
            nuke_cmd += flags
            nuke_cmd += ' -frames {0}-{1} -pool publish'.format(first_frame, last_frame)
            nuke_cmd += ' -name "render publish" -prop ConcurrentTasks=1'
            nuke_cmd += " -prop BatchName=" + data['name']

            process = subprocess.Popen(nuke_cmd, 
                                       shell=True, stdout=subprocess.PIPE)

            out, err = process.communicate()

            job_id = out.split("JobID=")[-1].split("The job")[0].strip()
            # Proyect must use this Nuke version to work and render
            '''
            nuke_version = "Nuke11.3v4/Nuke11.3"

            nuke_cmd = " -SubmitCommandLineJob -executable"
            nuke_cmd += " /usr/local/{0} -arguments ".format(nuke_version)
            nuke_cmd += ' "-t {0}" '.format(dictPath)
            nuke_cmd += ' -name "{0}" -pool publish'.format('Jobs submitter')
            nuke_cmd += " -prop BatchName={0}".format(batchName)
            subprocess.call('chmod -R 777 {0}'.format(dictPath), shell=True)
            deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
            process = subprocess.Popen(deadline + " " + nuke_cmd, shell=True,
                                       stdout=subprocess.PIPE)
            print deadline + " " + nuke_cmd
            print 'Deadline send!!! \n\n'
            out, err = process.communicate()
            print 'Error:', err
            job_id = None
            for line in out.splitlines():
                if 'J_ID' in line:
                    job_id = line.split(' ')[-1]
                    return job_id
            '''

            self.scr_media = """
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append('{0}/install/core/python')
import sgtk
print 'sgtk: ', sgtk
tk_nuke = sgtk.platform.current_engine()
if not tk_nuke:
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    cdm = sgtk.util.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.get_user()
    sgtk.set_authenticated_user(user)
    task_related = {1}
    tk = sgtk.sgtk_from_entity(task_related['type'], task_related['id'])
    ctx = tk.context_from_entity(task_related['type'], task_related['id'])
    sg = tk.shotgun
    tk_nuke = sgtk.platform.start_engine("tk-nuke", tk, ctx)
print 'tk_nuke:', tk_nuke
review_submission_app = tk_nuke.apps.get("tk-multi-reviewsubmission")
review_submission_app.execute_hook_method(key="render_media_hook",
                                          method_name="render",
                                          base_class=None,**{2})
            """
            self.scr_upload = """
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append('{0}/install/core/python')
import sgtk
print 'sgtk: ', sgtk
tk_nuke = sgtk.platform.current_engine()
if not tk_nuke:
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    cdm = sgtk.util.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.get_user()
    sgtk.set_authenticated_user(user)
    task_related = {1}
    tk = sgtk.sgtk_from_entity(task_related['type'], task_related['id'])
    ctx = tk.context_from_entity(task_related['type'], task_related['id'])
    sg = tk.shotgun
    tk_nuke = sgtk.platform.start_engine("tk-nuke", tk, ctx)
print 'tk_nuke:', tk_nuke
review_submission_app = tk_nuke.apps.get("tk-multi-reviewsubmission")
review_submission_app.execute_hook_method(key="submitter_hook",
                                          method_name="submit_version",
                                          base_class=None,
                                            **{2}
                                        )
            """
            if novid == False:
                render_media_hook_args = {
                    "input_path": p_path,
                    "output_path": mov_path,
                    "width": 1920,
                    "height": 1080,
                    "first_frame": first_frame,
                    "last_frame": last_frame,
                    "version": pfields.get("version", 0),
                    "name": pfields.get("name", "Unnamed"),
                    "color_space": colorspace,
                    "description": self.diction['comments'],
                    "task": task
                }

                submit_hook_args = {
                    "path_to_frames": p_path,
                    "path_to_movie": mov_path,
                    "thumbnail_path": thumbnail_path,
                    "sg_publishes": [sg_publish_data],
                    "sg_task": task,
                    "description": self.diction['comments'],
                    "first_frame": first_frame,
                    "last_frame": last_frame,
                }
                task = task
                python_path = self.engine.context.tank.pipeline_configuration.get_path()
                # import nuke
                self.scr_media = self.scr_media.format(python_path, task, render_media_hook_args)
                scene_path = script_path
                prefix = '/nfs/ovfxToolkit/TMP/'
                (fd, python_script_path) = tempfile.mkstemp(suffix='.py',
                                                            prefix=prefix)
                open_python_file = open(python_script_path, "w")
                open_python_file.write(self.scr_media)
                open_python_file.close()
                import sys
                if '/nfs/ovfxToolkit/Resources/site-packages' not in sys.path:
                    sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
                import yaml
                proj = self.engine.context.to_dict()['project']
                tk = sgtk.sgtk_from_entity('Project', proj['id'])
                self.config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
                p_specs_path = os.path.join(self.config_path, "resources", "project",
                                                              "project_specs.yml")
                with open(p_specs_path, 'r') as stream:
                    try:
                        self.specs = yaml.load(stream)
                    except yaml.YAMLError as exc:
                        print(exc)
                nuke_version = self.specs['main_nuke_path']
                nuke_cmd = " -SubmitCommandLineJob -executable"
                nuke_cmd += " {0} -arguments ".format(nuke_version)
                nuke_cmd += ' "-t ' + python_script_path + ' ' + scene_path + ' ' + python_path + ' "'
                nuke_cmd += ' -frames 1 -pool publish'
                nuke_cmd += ' -name "Publish media process" '
                b_name = os.path.basename(scene_path).split('.')[0]
                nuke_cmd += " -prop BatchName={0}".format(b_name)
                nuke_cmd += ' -prop JobDependencies=' + str(job_id)
                print nuke_cmd
                subprocess.call('chmod -R 777 ' + python_script_path, shell = True)
                deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
                print deadline + " " + nuke_cmd
                process = subprocess.Popen(deadline + " " + nuke_cmd,
                                           shell=True, stdout=subprocess.PIPE)
                out, err = process.communicate()
                if 'error' in str(err).lower() or 'error' in str(out).lower():
                    process = subprocess.Popen(deadline + " " + nuke_cmd,
                                               shell=True, stdout=subprocess.PIPE)
                    out, err = process.communicate()
                print out
                job_id = out.split("JobID=")[-1].split("The job")[0].strip()

                #UPLOAD JOB
                self.scr_upload = self.scr_upload.format(python_path, task, submit_hook_args)
                (fd, python_script_path) = tempfile.mkstemp(suffix='.py',
                                                            prefix=prefix)
                open_python_file = open(python_script_path, "w")
                open_python_file.write(self.scr_upload)
                open_python_file.close()
                nuke_cmd = " -SubmitCommandLineJob -executable"
                nuke_cmd += " /usr/local/Nuke11.3v4/Nuke11.3 -arguments "
                nuke_cmd += ' "-t ' + python_script_path + ' ' + scene_path + ' ' + python_path + ' "'
                nuke_cmd += ' -frames 1 -pool publish'
                nuke_cmd += ' -name "Upload process" '
                b_name = os.path.basename(scene_path).split('.')[0]
                nuke_cmd += " -prop BatchName={0}".format(b_name)
                nuke_cmd += ' -prop JobDependencies=' + str(job_id)
                print nuke_cmd
                subprocess.call('chmod -R 777 ' + python_script_path, shell = True)
                deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
                print deadline + " " + nuke_cmd
                process = subprocess.Popen(deadline + " " + nuke_cmd,
                                           shell=True, stdout=subprocess.PIPE)
                out, err = process.communicate()
                if 'error' in str(err).lower() or 'error' in str(out).lower():
                    process = subprocess.Popen(deadline + " " + nuke_cmd,
                                               shell=True, stdout=subprocess.PIPE)
                    out, err = process.communicate()
                print out
                job_id = out.split("JobID=")[-1].split("The job")[0].strip()
                version = None

    def get_mainplate_reference(self, entity):
        if entity['sg_main_plate_name']:
            elemName = str(entity['sg_main_plate_name'].split('element')[-1])[1:]
        else:
            elemName = 'mp01'
        seq = self.engine.shotgun.find_one('Sequence',[['id', 'is', entity['sg_sequence']['id']]], ['code'])
        # TODO: QUITAR EL 0 DEL NOMBRE DE LA SECUENCIA
        element_base_path = '/nfs/ollinvfx/Project/{2}/sequences/{0}/{1}/editorial/publish/elements/{2}_{1}_element_{3}/'.format(
            seq['code'], entity['code'], 'BSHI', elemName)
        # From base path, fetch versions and select last version in folder
        if os.path.exists(element_base_path):
            versions_ = os.listdir(element_base_path)
            versions_.sort()
            version_p = versions_[-1]
            if entity['sg_pvz']:
                exten = 'png'
            else:
                exten = 'exr'
            scan_path = element_base_path + '{0}/{1}/{2}x{3}/{6}_{4}_element_{5}_{0}.%04d.{1}'.format(version_p, exten,
                                                                                                      entity['sg_resx'],
                                                                                                      entity['sg_resy'],
                                                                                                      entity['code'],
                                                                                                      elemName, 'BSHI')
            elements_template = self.engine.get_template_by_name("elements_ext")
            print scan_path
            fields = elements_template.get_fields(scan_path)
            print fields
            files = self.engine.sgtk.paths_from_template(elements_template, fields, ['SEQ'])
            print 'files: ', len(files)
            files = sorted(files)
            element_last_frame = elements_template.get_fields(files[-1])
            element_last_frame = element_last_frame['SEQ']
            print 'lastframe: ', element_last_frame
            if os.path.exists(os.path.dirname(scan_path)):
                if len(os.listdir(os.path.dirname(scan_path))) < 1:
                    raise ValueError(
                        'Content is empty!!!, No files for exr path for the Main plate name (sg_main_plate_name) value for the shot: {0} in Shotgun.'.format(
                            entity['code']))
            else:
                raise ValueError(
                    'Path not found: {1}<BR> Folder path is missing for the Main plate name (sg_main_plate_name) value for the shot: {0} in Shotgun.'.format(
                        entity['code'], os.path.dirname(scan_path)))
        else:
            raise ValueError('No Main plate for current Shot.{0}'.format(element_base_path))

        return scan_path, element_last_frame