# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

"""
Before App Launch Hook

This hook is executed prior to application launch and is useful if you need
to set environment variables or run scripts as part of the app initialization.
"""

import os
import tank
import pickle
import getpass
import sys
import socket

class BeforeAppLaunch(tank.Hook):
    """
    Hook to set up the system prior to app launch.
    """
    
    def execute(self, app_path, app_args, version, engine_name, **kwargs):
        """
        The execute functon of the hook will be called prior to starting the required application        
        
        :param app_path: (str) The path of the application executable
        :param app_args: (str) Any arguments the application may require
        :param version: (str) version of the application being run if set in the
            "versions" settings of the Launcher instance, otherwise None
        :param engine_name (str) The name of the engine associated with the
            software about to be launched.

        """

        # accessing the current context (current shot, etc)
        # can be done via the parent object
        #

        multi_launchapp = self.parent
        
        # > current_entity = multi_launchapp.context.entity
        
        # you can set environment variables like this:
        # os.environ["MY_SETTING"] = "foo bar"

        import stat
        import sgtk

        ovfxTmp = "/ollin/tmp"
        print engine_name
        if engine_name == "tk-katana":
            ovfxArnoldKatana = "/nfs/ovfxToolkit/Resources/plugins/katana/ktoa"
            tank.util.append_path_to_env_var("solidangle_LICENSE", "")
            tank.util.append_path_to_env_var("DEFAULT_RENDERER","arnold")
            tank.util.append_path_to_env_var("KTOA_ROOT", ovfxArnoldKatana)
            tank.util.append_path_to_env_var("PATH", ovfxArnoldKatana + "/bin")
            tank.util.append_path_to_env_var("KATANA_RESOURCES", ovfxArnoldKatana)
            license = "5053@172.16.29.159:27000@172.16.29.189"
            os.environ["solidangle_LICENSE"] = license


        if engine_name == "tk-nuke":

            # Append path of Custom Gizmos to NUKE_PATH Environment Variable
            ovfx_gizmos_path = "/nfs/ovfxToolkit/Dev/nukeGizmos"
            engine = sgtk.platform.current_engine()
            config_path = engine.context.tank.pipeline_configuration.get_path()

            tank.util.append_path_to_env_var("NUKE_PATH", ovfx_gizmos_path)
            tank.util.append_path_to_env_var("NUKE_PATH", config_path + "/config/resources")
            gizmo_path = '/nfs/ovfxToolkit/Resources/ollinNukeTools'
            #UDG_GIZMOS = '/nfs/ollinvfx/Project/UDG/editorial/publish/reference/Gizmos/UDG_GIZMOS'
            tank.util.append_path_to_env_var("NUKE_PATH", gizmo_path)
            #tank.util.append_path_to_env_var("NUKE_PATH", UDG_GIZMOS)

            # StickIt node dependencies
            stickIt2_paths = ['/nfs/ovfxToolkit/Dev/nukeGizmos/StickIt2.0/hagbarth',
                              '/nfs/ovfxToolkit/Dev/nukeGizmos/StickIt2.0/hagbarth/icons',
                              '/nfs/ovfxToolkit/Dev/nukeGizmos/StickIt2.0/hagbarth/tools',
                              '/nfs/ovfxToolkit/Dev/nukeGizmos/StickIt2.0/hagbarth/grapichs',
                              '/nfs/ovfxToolkit/Dev/nukeGizmos/StickIt2.0/hagbarth/python']

            for st_path in stickIt2_paths:
                tank.util.append_path_to_env_var("NUKE_PATH", st_path)

            if 'aligato' in socket.gethostname():
                tank.util.append_path_to_env_var("NUKE_PATH", "/nfs/ovfxToolkit/Dev/nukePlugins/renderFarm")
                #tank.util.append_path_to_env_var("OPTICAL_FLARES_LICENSE_PATH", "/ollin/opticalFlares")
            else:
                tank.util.append_path_to_env_var("NUKE_PATH", "/nfs/ovfxToolkit/Dev/nukePlugins/gui")

            # Check if /ollin/tmp folder exists
            if os.path.isdir(ovfxTmp):

                #Check if /ollin/tmp folder has 01777 permissions,
                #  ie. 777 + sticky bit
                if oct(stat.S_IMODE(os.lstat(ovfxTmp).st_mode)) == "01777":

                    # Get the current user id, build a custom folder
                    # name with it and set the Nuke
                    # Temp Environment Variable
                    userId = os.getuid()
                    nukeTemp = "/ollin/tmp/" + "nuke-u" + str(userId)
                    os.environ['NUKE_TEMP_DIR'] = nukeTemp
            #os.environ["foundry_LICENSE"] = '4101@172.16.28.122'
            os.environ["foundry_HEARTBEAT_DISABLE"] = "true"
            os.environ["QT_COMPRESS_TABLET_EVENTS"] = "1"
            os.environ["OPTICAL_FLARES_PATH"] = "/ollin/opticalFlares"

        elif engine_name == "tk-hiero":
            '''
            ovfx_gizmos_path = '/nfs/ovfxToolkit/Resources/hieroTools'
            tank.util.append_path_to_env_var("HIERO_PLUGIN_PATH",
                                             ovfx_gizmos_path)
            print(ovfx_gizmos_path)

            # Check if /ollin/tmp folder exists
            if os.path.isdir(ovfxTmp):
                if oct(stat.S_IMODE(os.lstat(ovfxTmp).st_mode)) == "01777":
                    userId = os.getuid()
                    nukeTemp = "/ollin/tmp/" + "nuke-u" + str(userId)
                    os.environ['NUKE_TEMP_DIR'] = nukeTemp
            '''
            pass

        elif engine_name == "tk-maya":
            mod_path = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules'
            if '2019' in version:
                mod_path = '/nfs/ovfxToolkit/Resources/plugins/autodesk/modules2019'
                #mod_path = '/nfs/library/tmp/JulioCesar/modules_2019'
               


            os.environ['MAYA_MODULE_PATH'] = mod_path

            #ziva_lic = '/nfs/ovfxToolkit/Resources/plugins/autodesk'
            #ziva_lic += '/ZivaVFX-Maya-1_619/'
            #os.environ["zivadyn_LICENSE"] = ziva_lic

            os.environ["zivadyn_LICENSE"] = '5053@ovfxlicenses'

            # Append ziva tools library
            path_z = '/nfs/ovfxToolkit/Resources/plugins/autodesk/'
            path_z = 'ziva-vfx-utils/zBuilder'
            sys.path.append(path_z)

            # START Erasing .lock files -------------------------------
            home_path = os.getenv('HOME')
            hidden_path = '/.config/Autodesk/'
            mypath = home_path + hidden_path
            from os import listdir
            from os.path import isfile, join

            if os.path.exists(mypath):
                onlyfiles = [join(mypath, f) for f in listdir(mypath) if isfile(join(mypath, f))]
                for f_ in onlyfiles:
                    if '.lock' in f_:
                        os.remove(f_)
            # END Erasing .lock files -------------------------------
        elif engine_name == "tk-houdini":
            #os.environ['HOUDINI_PATH'] = "$HOUDINI_PATH;/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"
            if 'HOUDINI_PATH' in os.environ.keys():
                os.environ['HOUDINI_PATH'] = os.environ['HOUDINI_PATH'] + ";/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"
            else:
                os.environ['HOUDINI_PATH'] = "/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"

            if 'HOUDINI_MENU_PATH' in os.environ.keys():
            #os.environ['HOUDINI_MENU_PATH'] = "$HOUDINI_MENU_PATH;/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"
                os.environ['HOUDINI_MENU_PATH'] = os.environ['HOUDINI_MENU_PATH'] + ";/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"
            else:
                os.environ['HOUDINI_MENU_PATH'] = "/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"

            #HOUDINI_PATH = "$HOUDINI_PATH;/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"
            #HOUDINI_MENU_PATH = "$HOUDINI_MENU_PATH;/nfs/ovfxToolkit/Software/Thinkbox/Deadline10/submitters/HoudiniSubmitter;&"


    def _get_license_server(self):
        current_user = getpass.getuser()
        lic_serv = '/nfs/ovfxToolkit/Resources/custom/license_servers.pkl'
        pkl_file = open(lic_serv, 'rb')
        license_dict = pickle.load(pkl_file)
        pkl_file.close()

        for license_key, users_value in license_dict.iteritems():
            for user in users_value:
                if current_user == user:
                    return license_key
        return ""
