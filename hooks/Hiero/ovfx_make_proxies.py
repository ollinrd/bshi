import nuke
import sys
import os
import time
import unicodedata
from stat import S_ISREG, ST_CTIME, ST_MODE
from datetime import datetime
from time import gmtime, strftime
import datetime as dt_
import ast
import threading
from subprocess import Popen, PIPE
import tempfile
import nuke
import shutil
import uuid
import subprocess


shot_dict_txt = nuke.rawArgs[3]
config_path = nuke.rawArgs[4]
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
sys.path.append('{0}/install/core/python'.format(config_path))
import yaml
import sgtk
file = open(shot_dict_txt,'r')
SHOTLIST = file.read()
file.close()
shot = ast.literal_eval(SHOTLIST)
pro_code = shot['project']['code']
shot_name = shot['clip_name']
seq_name = shot['seq']
scan_path = shot['clip_plate_path']
print scan_path


work_path = shot['clip_plate_path'].split(seq_name)[0]
formato = shot['clip_plate_path'].split('.')[-1]
sg_project = shot['project']
tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
bndl_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
burnin_path = '{0}/resources/Hiero/proxies.nk'.format(bndl_path)




def get_settings(config_path):
  
  settings_path = config_path + "/resources/project/project_specs.yml"    
  stream = open(settings_path, "r")
  settings = dict(yaml.load(stream))

  return settings

def set_lut_node(lut_node, SHOT):
    no_lut = False
    if SHOT['sg_lut']:
        cdl_path = '/nfs/ollinvfx/Project/PIGS/editorial/publish/LUT/PIGS_noche.cube'
        if 'nl' in SHOT['sg_lut']:
            no_lut = True
    elif SHOT['sg_sequence.Sequence.sg_lut']:
        if 'nl' in SHOT['sg_sequence.Sequence.sg_lut']:
            no_lut = True
        cdl_path = '/nfs/ollinvfx/Project/PIGS/editorial/publish/LUT/PIGS_noche.cube'
    else:
        cdl_path = '/nfs/ollinvfx/Project/PIGS/editorial/publish/LUT/PIGS_dia.cube'
    print 'cdl_path', cdl_path
    ocioNode = lut_node
    ocioNode.showControlPanel()
    ocioNode["file"].setValue(cdl_path)
    ocioNode["reload"].execute()
    ocioNode.hideControlPanel()
    ocioNode['disable'].setValue(no_lut)

def connect_to_shotgun(api_namekey, dbConnect):
  
  sg_connection = dbConnect.sgConnect()
  sg = sg_connection.connect(api_namekey)
  print 'sg: ', sg, sg_connection

  return sg , sg_connection

settings = get_settings(bndl_path)
api_namekey = settings.get("ingesta_editorial_apikeyname")
sys.path.append(settings.get("python_utils_path"))
from pythonTools.shotgunUtils import dbConnect
sg, sg_connect = connect_to_shotgun(api_namekey, dbConnect)
from tank_vendor.shotgun_authentication import ShotgunAuthenticator
sg_auth = sg_connect.authenticateuser_from_apiname(api_namekey, sgtk, ShotgunAuthenticator)


tk = sgtk.sgtk_from_path(work_path)
tk.synchronize_filesystem_structure()
ctx = ctx = tk.context_from_entity("Shot", shot['id'])
engine = sgtk.platform.start_engine("tk-nuke", tk, ctx)
print 'ENGINE = ', engine
sg = engine.shotgun
usuario = sg.find_one('HumanUser', [['login', 'is', shot['artist']]],['name'])
artist = "{0}. {1}".format(usuario["name"].split(" ")[0][0], usuario["name"].split(" ")[-1])
proxies_template = engine.get_template_by_name('elements_proxy_full')
half_proxies_template = engine.get_template_by_name('elements_proxy_half')
lut_proxies_template = engine.get_template_by_name('elements_proxy_lut')

SHOT = sg.find_one('Shot', [['code', 'is', shot_name], ['project', 'is', shot['project']]], ['sg_head_in','sg_tail_out', 'sg_lens_focal_length',
                                                                                            'sg_external_id', 'sg_in_timecode', 'sg_camera',
                                                                                            'sg_offset_frame', 'sg_resx', 'sg_resy', 'sg_lut','sg_sequence.Sequence.sg_lut'])
if SHOT:
    fields = shot['fields']
    proxies_path = proxies_template.apply_fields(fields)

    first_frame = SHOT['sg_head_in']
    last_frame = SHOT['sg_tail_out']
    base_ = os.listdir(os.path.dirname(scan_path))
    base_.sort()
    first_frame = int(base_[0].split('.')[-2])
    last_frame = int(base_[-1].split('.')[-2])

    total_f = int(last_frame) - int(first_frame) + 1
    proxies_out_name = os.path.basename(proxies_path).split('.')[0] + '_jpg'
    half_proxies_out_name = proxies_out_name + '_half'
    lut_proxies_out_name = proxies_out_name + '_lut'
    extension = scan_path.split('.')[-1]
    half_exr_proxies_out_name = proxies_out_name + '_{0}half'.format(extension.upper())
    engine.ensure_folder_exists(os.path.dirname(proxies_path))
    h_res = False
    v_res = False
    if 'sg_resx' not in SHOT:
        nuke.message('ERROR:\nHorizontal resolution not set in Shotgun.\nPlease notify EDITORIAL department to fix this problem.')
    else:
        h_res = SHOT['sg_resx']
    if 'sg_resy' not in SHOT:
        nuke.message('ERROR:\nVertical resolution not set in Shotgun.\nPlease notify EDITORIAL department to fix this problem.')
    else:
        v_res = SHOT['sg_resy']

    if (not h_res or not v_res):
        nuke.message('ERROR:\nResolution not found for shot.\nnotify EDITORIAL department')
    else:
        nuke.addFormat('{0} {1} 1 OSOY_UHD'.format(h_res, v_res))                
        nuke.root()['format'].setValue('OSOY_UHD')
        nuke.knobDefault("Root.format", 'OSOY_UHD')
        nuke.knobDefault("Read.proxy_format", "OSOY_UHD")

    read = nuke.nodes.Read(name="read_source", file=scan_path, first=first_frame, last=last_frame )

    nuke.nodePaste(burnin_path)
    full_node = nuke.toNode('full_res')
    lin_jpg_node = nuke.toNode('half_res_lin')
    half_node = nuke.toNode('Reformat1')
    ocio_node = nuke.toNode('OCIOColorSpace1')
    exr_half_node_reformat = nuke.toNode('reformat_proxie')
    exr_half_node = nuke.toNode('PublisherEXT')
    exr_lin_node = nuke.toNode('PublisherEXT_lin')
    crop_node = nuke.toNode('Crop7')
    crop_node['disable'].setValue(True)
    crop_node.setInput(0, read)
    full_node.setInput(0, read)
    half_node.setInput(0, read)
    ocio_node.setInput(0, read)

    exr_half_node_reformat.setInput(0, read)
    nuke.root()['last_frame'].setValue(last_frame)
    nuke.root()['first_frame'].setValue(first_frame)
    lin_exr_proxies_path = proxies_path.replace('JPG_FULL', '{0}_LIN'.format(extension.upper())).replace('.jpg','.' + extension)
    full_node['file'].setValue(proxies_path)
    exr_lin_node['file'].setValue(lin_exr_proxies_path)

    fields['width'] = int(int(fields['width']) / 2)
    fields['height'] = int(int(fields['height']) / 2)
    lut_proxies_path = lut_proxies_template.apply_fields(fields)
    half_proxies_path = half_proxies_template.apply_fields(fields)
    half_exr_proxies_path = half_proxies_path.replace('JPG_HALF', '{0}_HALF'.format(extension.upper())).replace('.jpg','.' + extension)
    lin_jpg_proxies_path = half_proxies_path.replace('JPG_HALF', 'JPG_LIN')
    lin_jpg_node['file'].setValue(lin_jpg_proxies_path)

    # engine.ensure_folder_exists(os.path.dirname(lut_proxies_path))
    engine.ensure_folder_exists(os.path.dirname(half_proxies_path))
    #engine.ensure_folder_exists(os.path.dirname(half_exr_proxies_path))
   # engine.ensure_folder_exists(os.path.dirname(lin_exr_proxies_path))
    engine.ensure_folder_exists(os.path.dirname(lin_jpg_proxies_path))

    nuke.toNode('half_res')['file'].setValue(half_proxies_path)
    # nuke.toNode('half_res_lut')['file'].setValue(lut_proxies_path)
    # exr_half_node['file'].setValue(half_exr_proxies_path)

    nuke.executeMultiple([nuke.toNode('full_res')], ([first_frame, last_frame, 1],),[nuke.views()[0]])
    nuke.executeMultiple([nuke.toNode('half_res')], ([first_frame, last_frame, 1],),[nuke.views()[0]])
    # nuke.executeMultiple([exr_lin_node], ([first_frame, last_frame, 1],),[nuke.views()[0]])
    # nuke.executeMultiple([exr_half_node], ([first_frame, last_frame, 1],),[nuke.views()[0]])
    # nuke.executeMultiple([lin_jpg_node], ([first_frame, last_frame, 1],),[nuke.views()[0]])
    lut_node = nuke.toNode('LUT_PIGS').node('OCIOFileTransform4')
    set_lut_node(lut_node, SHOT)

    #nuke.executeMultiple([nuke.toNode('half_res_lut')], ([first_frame, first_frame + 7, 1],),[nuke.views()[0]])
    # nuke.executeMultiple([nuke.toNode('half_res_lut')], ([first_frame, last_frame, 1],),[nuke.views()[0]])

    prefixx = '/nfs/ovfxToolkit/TMP/'
    dictPath = prefixx + "Render_proxies_{0}_".format(shot_name) + str(uuid.uuid4()) +'.nk'
    nuke.nuke.scriptSaveAs(dictPath)
    print dictPath
    compare_path = os.path.basename(scan_path)
    p_file_scan = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['code', 'is', compare_path]])
    data = {'upstream_published_files': [p_file_scan]}
    print p_file_scan
    '''
    file_to_publish = lin_jpg_proxies_path

    name = lut_proxies_out_name
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['name', 'is', os.path.basename(lut_proxies_out_name)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="JPG lut sequence",
      thumbnail_path = lut_proxies_out_name.replace('%04d', str(first_frame)),
      version_number = 1,
      description = 'scans in jpg')
    publish_update = sg.update('PublishedFile', p_file['id'], data)

    file_to_publish = lin_exr_proxies_path
    name = lut_proxies_out_name
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['name', 'is', os.path.basename(lut_proxies_out_name)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="JPG lut sequence",
      thumbnail_path = lut_proxies_out_name.replace('%04d', str(first_frame)),
      version_number = 1,
      description = 'scans in jpg')
    publish_update = sg.update('PublishedFile', p_file['id'], data)

    file_to_publish = lut_proxies_path
    name = lut_proxies_out_name
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['name', 'is', os.path.basename(lut_proxies_out_name)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="JPG lut sequence",
      thumbnail_path = lut_proxies_out_name.replace('%04d', str(first_frame)),
      version_number = 1,
      description = 'scans in jpg')
    publish_update = sg.update('PublishedFile', p_file['id'], data)
    '''
    file_to_publish = proxies_path
    name = proxies_out_name
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['name', 'is', os.path.basename(proxies_out_name)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="Image",
      thumbnail_path = proxies_path.replace('%04d', str(first_frame)),
      version_number = 1,
      description = 'scans in jpg')
    publish_update = sg.update('PublishedFile', p_file['id'], data)

    file_to_publish = half_proxies_path
    name = half_proxies_out_name
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['name', 'is', os.path.basename(half_proxies_out_name)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="Image",
      thumbnail_path = half_proxies_path.replace('%04d', str(first_frame)),
      version_number = 1,
      description = 'scans in jpg format half res')
    publish_update = sg.update('PublishedFile', p_file['id'], data)
    '''
    file_to_publish = half_exr_proxies_path
    name = half_exr_proxies_out_name.replace('_jpg_', '_')
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['name', 'is', os.path.basename(half_exr_proxies_out_name)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="Proxy",
      thumbnail_path = half_exr_proxies_out_name.replace('%04d', str(first_frame)),
      version_number = 1,
      description = 'scans in {0} format half res'.format(extension))
    publish_update = sg.update('PublishedFile', p_file['id'], data)
    '''