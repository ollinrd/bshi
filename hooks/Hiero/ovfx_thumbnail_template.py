import sys
import os
import tempfile
import ast
import nuke
import shutil

shot_dict_txt = nuke.rawArgs[3]
config_path = nuke.rawArgs[4]

# Import shotgun api module
sys.path.append('{0}/install/core/python'.format(config_path))
print config_path
sys.path.append("/nfs/ovfxToolkit/Resources/site-packages")

import sgtk
import shotgun_api3
import yaml

file = open(shot_dict_txt,'r')
SHOTLIST = file.read()
file.close()
SHOTS = ast.literal_eval(SHOTLIST)

def get_settings(sg_project):
    tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
    config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
    p_specs_path = os.path.join(config_path, "resources", "project",
                                                  "project_specs.yml")
    with open(p_specs_path, 'r') as stream:
        try:
            return yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

def create_folders(path):
    if not os.path.exists(path):
        try:
            chmod = '2775'
            ovfxPath=path.split("/")
            ovfxFolder=""
            for folder in ovfxPath:
                ovfxFolder = ovfxFolder + folder + "/" 
                if not os.path.exists(ovfxFolder):
                    os.system("sudo /usr/bin/mkdir %s"%(ovfxFolder))
                    os.system("sudo /usr/bin/chmod  %s %s "%(chmod,ovfxFolder))
                    os.system("sudo /usr/bin/chown %s %s"%('core', ovfxFolder))
                    if 'editorial' in ovfxFolder:
                        os.system("sudo /usr/bin/chgrp %s %s"%('editorial', ovfxFolder))
                    else:
                        os.system("sudo /usr/bin/chgrp %s %s"%('ovfx', ovfxFolder))
        except Exception as e:
          raise e

def connect_to_shotgun(proj_settings):
  sys.path.append(proj_settings.get("python_utils_path"))
  from pythonTools.shotgunUtils import dbConnect

  api_namekey = proj_settings.get("ingesta_editorial_apikeyname")
  sg_connect = dbConnect.sgConnect()
  sg = sg_connect.connect(api_namekey)
  print 'sg: ', sg

  return sg

sg_project = SHOTS[0]['project']
settings = get_settings(sg_project)
sg = connect_to_shotgun(settings)


for shot in SHOTS:
  sg_project = shot['project']
  SHOT = sg.find_one('Shot', [['project', 'is', sg_project], ['code', 'is', shot['clip_name']]],['sg_cut_in', 'sg_cut_out'])
  if not SHOT == None:
    if not os.path.exists('{0}'.format(os.path.dirname(shot['clip_thumb_path']))):
        create_folders('{0}'.format(os.path.dirname(shot['clip_thumb_path'])))
    #os.system('chmod 777 {0}'.format(os.path.dirname(shot['clip_thumb_path'])))
    sg_project = shot['project']
    artist = sg.find_one('HumanUser', [['login', 'is', shot['artist']]])
    first_frame = shot['clip_first']
    tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
    config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
    nuke_script = '{0}/resources/Hiero/hiero_export.nk'.format(config_path)
    nuke.nodePaste(r'{0}'.format(nuke_script))
    first = shot['clip_first']
    last = shot['clip_last']
    mid_frame = int(int(last - first) / 2)
    mid_frame = int(first + mid_frame)
    write_images =nuke.toNode('write_images')
    write_images['file'].setValue(shot['clip_thumb_path'])
    path = shot['cut_origin_path']
    read_th = nuke.nodes.Read(name="THUMB", file=path, first=mid_frame, last=mid_frame )
    read_th['raw'].setValue(True)
    write_images.setInput(0, read_th)
    nuke.executeMultiple([write_images], ([mid_frame , mid_frame , 1],),
                                       [nuke.views()[0]])
