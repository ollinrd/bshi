import nuke
import sys
import os
import time
import unicodedata
from stat import S_ISREG, ST_CTIME, ST_MODE
from datetime import datetime
from time import gmtime, strftime
import datetime as dt_
import ast
import threading
from subprocess import Popen, PIPE
import tempfile
import nuke
import shutil
import uuid
import subprocess


shot_dict_txt = nuke.rawArgs[3]
config_path = nuke.rawArgs[4]
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
sys.path.append('{0}/install/core/python'.format(config_path))
import timecode
import sgtk
import yaml


def get_settings(conf_path):
    #config_path = engine.context.tank.pipeline_configuration.get_path()
    p_specs_path = os.path.join(conf_path, "resources", "project",
                                             "project_specs.yml")
    stream = open(p_specs_path, "r")
    settings = dict(yaml.load(stream))
    return settings

def connect_to_shotgun(api_namekey, dbConnect):
  
    sg_connection = dbConnect.sgConnect()
    sg = sg_connection.connect(api_namekey)
    print 'sg: ', sg, sg_connection

    return sg, sg_connection


def start_engine(shot, work_path):
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    sg_project = shot['project']
    tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
    config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
    settings = get_settings(config_path)
    api_namekey = settings.get("ingesta_editorial_apikeyname")   
    sys.path.append(settings.get("python_utils_path"))
    from pythonTools.shotgunUtils import dbConnect
    sg, sg_connect = connect_to_shotgun(api_namekey, dbConnect)
    sg_auth = sg_connect.authenticateuser_from_apiname(api_namekey, sgtk, ShotgunAuthenticator)

    tk = sgtk.sgtk_from_path(work_path)
    tk.synchronize_filesystem_structure()
    ctx = ctx = tk.context_from_entity("Shot", shot['id'])
    engine = sgtk.platform.start_engine("tk-nuke", tk, ctx)
    return engine, tk, ctx

def get_sg_templates(engine):
    config_path = engine.context.tank.pipeline_configuration.get_path()
    with open(config_path + "/config/resources/project/project_specs.yml", 'r') as stream:
        try:
            specs = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    print 'SPECS:', specs
    return specs

def _start_pix_encode(pix_jpg_path, pix_mov_path, timecode, first_frame):
    #pix_path
    # pix_jpg_path, pix_mov_path
    try:

        cmd_list = ["ffmpeg -start_number", str(first_frame-1), 
            " -f image2 -r 23.976 -i", pix_jpg_path,
            "-c:v libx264 -profile high -r 23.976 -pix_fmt yuv420p -b:v 3200000 -timecode",
            timecode, "-s 1920x1080 -movflags faststart  -y", pix_mov_path] 
        print " ".join(cmd_list)
        proc = Popen(" ".join(cmd_list), stdin=PIPE, stderr=PIPE,
                     stdout=PIPE,  shell=True)
        print proc.communicate()

    except Exception, e:
        self.parent.log_error("Failed to encode: %s" % e)

def _frames_to_timecode(total_frames, frame_rate, drop):
    if drop and frame_rate not in [24.00, 59.94]:
        print NotImplementedError("Time code calculation logic only supports drop frame "
                                  "calculations for 29.97 and 59.94 fps.")
    fps_int = int(round(frame_rate))
    if drop:
        FRAMES_IN_ONE_MINUTE = 1800 - 2
        FRAMES_IN_TEN_MINUTES = (FRAMES_IN_ONE_MINUTE * 10) - 2
        ten_minute_chunks = total_frames / FRAMES_IN_TEN_MINUTES
        one_minute_chunks = total_frames % FRAMES_IN_TEN_MINUTES
        ten_minute_part = 18 * ten_minute_chunks
        one_minute_part = 2 * ((one_minute_chunks - 2) / FRAMES_IN_ONE_MINUTE)
        if one_minute_part < 0:
            one_minute_part = 0
        total_frames += ten_minute_part + one_minute_part
        if fps_int == 60:
            total_frames = total_frames * 2
        smpte_token = ":"
    else: 
        smpte_token = ":"
    hours = int(total_frames / (3600 * fps_int))
    minutes = int(total_frames / (60 * fps_int) % 60)
    seconds = int(total_frames / fps_int % 60)
    frames = int(total_frames % fps_int)
    print "%02d:%02d:%02d%s%02d" % (hours, minutes, seconds, smpte_token, frames)
    return "%02d:%02d:%02d%s%02d" % (hours, minutes, seconds, smpte_token, frames)

# read dictionary from hiero
file = open(shot_dict_txt, 'r')
SHOTLIST = file.read()
file.close()
shot = ast.literal_eval(SHOTLIST)
seq_name = shot['seq']
work_path = shot['clip_plate_path'].split('/' + seq_name + '/')[0]
engine, tk, ctx = start_engine(shot, work_path)
sg_project = shot['project']
tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
conf_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
specs = get_settings(conf_path)
pro_code = shot['project']['code']
shot_name = shot['clip_name']
scan_path = shot['clip_plate_path']
first_frame = shot['first']
last_frame = shot['last']

formato = specs['main_format']
burnin_path = '{0}/resources/ovfx_BSHI_Burnin.nk'.format(conf_path)
print 'BURNING: ', burnin_path
print 'SCAN PATH: ', scan_path
print 'MAIN FORMAT: ', formato
print 'ENGINE = ', engine
gizmo_path = specs['version_zero_nuke_gizmo_path']
sgtk.util.append_path_to_env_var("NUKE_PATH", gizmo_path)
sg = engine.shotgun
usuario = sg.find_one('HumanUser', [['login', 'is', shot['artist']]],['name'])
artist = usuario['name']
plate_template = engine.get_template_by_name(specs['scan_template'])
cdl_template = engine.get_template_by_name("cdl_shot")
internal_jpgs_template = engine.get_template_by_name("nuke_shot_render_jpg_out_v0")
internal_rev_template = engine.get_template_by_name("nuke_shot_render_movie_v0")
internal_pub_template = engine.get_template_by_name("nuke_shot_render_pub")
SHOT = sg.find_one('Shot', [['code', 'is', shot_name],['project', 'is', shot['project']]], ['code', 'sg_head_in','sg_tail_out', 'sg_lens_focal_length',
                                                                                            'sg_external_id', 'sg_in_timecode', 'sg_camera',
                                                                                            'sg_offset_frame', 'sg_resx', 'sg_resy', 'sg_without_lut',
                                                                                            'sg_str_timecode', 'sg_lut', 'sg_sequence'])

nuke.root()['colorManagement'].setValue('OCIO')
nuke.root()['OCIO_config'].setValue('aces_1.0.3')
nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
nuke.root()['int16Lut'].setValue('ACES - ACEScc')
nuke.root()['logLut'].setValue('Input - ADX - ADX10')
nuke.root()['floatLut'].setValue('ACES - ACES2065-1')

if SHOT:
    SEQ = sg.find_one('Sequence', [['id', 'is', SHOT['sg_sequence']['id']]], ['sg_lut'])
    fields = shot['fields']
    fields['project'] = pro_code
    fields['project_code'] = pro_code
    fields['task_name'] = 'edt'
    fields['Step'] = 'editorial'
    fields['Step_name'] = 'comp'
    fields['name'] = 'master'
    fields['external_id'] = SHOT['sg_external_id']
    fields['Extension'] = specs['main_format']
    fields['Extension_cdl'] = specs['color_format']
    VERSION = shot['version']
    str_v = '{0:03d}'.format(int(VERSION))
    plate_path = scan_path
    no_lut = False
    if SHOT['sg_lut']:
        fields['tiempo'] = 'noche'
        if 'nl' in SHOT['sg_lut']:
            no_lut = True
    elif SEQ['sg_lut']:
        fields['tiempo'] = 'noche'
        if 'nl' in SEQ['sg_lut']:
            no_lut = True
    else:
        fields['tiempo'] = 'dia'
    cdl_path = cdl_template.apply_fields(fields)
    internal_jpgs_path = internal_jpgs_template.apply_fields(fields)
    internal_rev_path = internal_rev_template.apply_fields(fields)
    cdl_path = cdl_path.replace('master', fields['elementName'])
    internal_jpgs_path = internal_jpgs_path.replace('master', fields['elementName'])
    internal_rev_path = internal_rev_path.replace('master', fields['elementName'])
    internal_pub_path = internal_pub_template.apply_fields(fields)
    if not SHOT['sg_without_lut'] is True:
        if os.access(cdl_path, os.F_OK):
            pass
        elif os.access(cdl_path.replace('.ccc', '.cc'), os.F_OK):
            cdl_path = cdl_path.replace('.ccc', '.cc')
        elif os.access(cdl_path.replace('.ccc', '.cube'), os.F_OK):
            cdl_path = cdl_path.replace('.ccc', '.cube')
    lut = os.path.basename(cdl_path)

    date_aux = strftime("%Y-%m-%d", gmtime()).split("-")
    date = date_aux[0] + " / " + date_aux[1] + " / " + date_aux[2]

    images = os.listdir(os.path.dirname(plate_path))
    images.sort()
    total_f = int(last_frame) - int(first_frame) + 1
    out_name = os.path.basename(internal_rev_path).split('.')[0]
    engine.ensure_folder_exists(os.path.dirname(internal_jpgs_path))
    engine.ensure_folder_exists(os.path.dirname(internal_rev_path))
    engine.ensure_folder_exists(os.path.dirname(internal_pub_path))

    h_res = False
    v_res = False

    if 'sg_resx' not in SHOT:               
        nuke.message('ERROR:\nHorizontal resolution not set in Shotgun.\nPlease notify EDITORIAL department to fix this problem.')
    else:
        h_res = SHOT['sg_resx']
    if 'sg_resy' not in SHOT:               
        nuke.message('ERROR:\nVertical resolution not set in Shotgun.\nPlease notify EDITORIAL department to fix this problem.')
    else:
        v_res = SHOT['sg_resy']

    if (not h_res or not v_res):
        nuke.message('ERROR:\nResolution not found for shot.\nnotify EDITORIAL department')
    else:
        nuke.addFormat('{0} {1} 1 OSOY_UHD'.format(h_res, v_res))
        nuke.root()['format'].setValue('OSOY_UHD')
        nuke.knobDefault("Root.format", 'OSOY_UHD')
        nuke.knobDefault("Read.proxy_format", "OSOY_UHD")

    read = nuke.nodes.Read(name="read_source", file=scan_path, first=first_frame, last=last_frame )

    read.knob('colorspace').setValue('ACES - ACEScg')
    burn = nuke.nodePaste(burnin_path)
    print 'BURNIN INFO: ', burn, burnin_path
    burn.setInput(0, read)
    nuke.root()['last_frame'].setValue(last_frame)
    nuke.root()['first_frame'].setValue(first_frame)
    if SHOT['id'] in [12986]:
        read['raw'].setValue(True)
        burn.node('internal').node("OCIOColorSpace2")['disable'].setValue(False)
        burn.node('internal').node('write_avid1')['raw'].setValue(True)
    burn.node('internal').node("Switch")
    burn.node('internal').node("Switch")["which"].setValueAt(0, first_frame - 1)
    burn.node('internal').node("Switch")["which"].setValueAt(1, first_frame)
    burn.node('internal').node('label_comments').knob('message').setValue('{0}'.format('Version 0'))
    burn.node('internal').node('label_date_time').knob('message').setValue('{0}'.format(date))
    burn.node('internal').node('label_script_name').knob('message').setValue('Script: {0}'.format(out_name + '.nk'))
    burn.node('internal').node('label_sequence').knob('message').setValue('Sequence: {0}'.format(seq_name))
    burn.node('internal').node('label_internal').knob('message').setValue('{0}'.format(shot_name))
    burn.node('internal').node('label_artist').knob('message').setValue('Artist: {0}'.format(artist))
    burn.node('internal').node('label_frames').knob('message').setValue('{0}'.format(total_f))
    burn.node('internal').node('label_date_times1').knob('message').setValue('{0}'.format(date))
    burn.node('internal').node('label_version1').knob('message').setValue('{0}'.format(out_name))
    burn.node('internal').node('label_artistname').knob('message').setValue('{0}'.format(artist))
    burn.node('internal').node('label_project').knob('message').setValue('BSHI')
    timeco = SHOT['sg_str_timecode']
    print timeco
    set_time = burn.node("internal").node("AddTimeCode2")

    timeco = SHOT['sg_str_timecode']
    set_time['startcode'].setValue(timeco)
    set_time['frame'].setValue(first_frame - 1)

    burn.node('internal').node('write_avid1').knob('file').setValue(internal_rev_path)

    prefixx = specs['tmp_folder']
    dictPath = prefixx + "Reformat_publish_v000_{0}_".format(shot_name) + str(uuid.uuid4()) +'.nk'
    nuke.nuke.scriptSaveAs(dictPath)
    print dictPath

    nuke.executeMultiple([burn.node('internal').node('write_avid1')], ([first_frame - 1, first_frame + 7, 1],),[nuke.views()[0]])
    nuke.executeMultiple([burn.node('internal').node('write_avid1')], ([first_frame - 1, last_frame, 1],),[nuke.views()[0]])

    # _start_pix_encode(internal_jpgs_path, internal_rev_path, timeco, first_frame)

    rate = shot['framerate']
    rango = '{0}-{1}'.format(first_frame, last_frame)
    data = {'project': shot['project'],
            'code': out_name,
            'sg_version_type': 'Version 0',
            'sg_status_list': 'na',
            'entity': SHOT,
            'sg_path_to_movie': internal_rev_path,
            'sg_path_to_frames': scan_path,
            'user': usuario,
            'created_by': usuario,
            'sg_first_frame': first_frame,
            'sg_last_frame': last_frame,
            'frame_count': total_f,
            'sg_uploaded_movie_frame_rate': float(rate),
            'frame_range': rango,
            'sg_movie_has_slate': True,
            }
    filters = [['code', 'is', out_name], ['project', 'is', shot['project']]]
    version_sg = engine.shotgun.find_one('Version', filters)
    if not version_sg:
        version_sg = engine.shotgun.create('Version', data)
    else:
        engine.shotgun.delete('Version', version_sg['id'])
        version_sg = engine.shotgun.create('Version', data)
    cut_items = engine.shotgun.find('CutItem', [['shot', 'is', SHOT]], ['version'])
    for cut_it in cut_items:
        data_cut = {'version': version_sg}
        engine.shotgun.update('CutItem', cut_it['id'], data_cut)
    try:
        result = engine.shotgun.upload('Version', version_sg['id'], internal_rev_path, 'sg_uploaded_movie')
    except:
        print 'NO SE PUDO SUBIR EL VIDEO'
    engine.shotgun.update('Shot', SHOT['id'], {'sg_versions_zero_published_': True})

    thumbpath = internal_jpgs_path.replace('%04d', str(first_frame))

    file_to_publish = internal_rev_path
    name = os.path.basename(file_to_publish)
    name = name.replace('_' + name.split('_')[-1], '')
    compare_path = os.path.basename(scan_path)
    p_file_scan = sg.find_one('PublishedFile', [['project', 'is', shot['project']],
                                               ['code', 'is', compare_path]])
    data = {'upstream_published_files': [p_file_scan]}
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['code', 'is', os.path.basename(file_to_publish)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="Movie",
      thumbnail_path = thumbpath,
      version_number = shot['version'],
      description = 'Version 0',
      version_entity = version_sg)
    publish_update = sg.update('PublishedFile', p_file['id'], data)

    file_to_publish = internal_jpgs_path
    name = os.path.basename(file_to_publish)
    name = name.replace('_' + name.split('_')[-1], '')
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']],['code', 'is', os.path.basename(file_to_publish)]])
    if not p_file == None:
      sg.delete("PublishedFile", p_file['id'])
    p_file = sgtk.util.register_publish(
      tk, 
      ctx, 
      file_to_publish, 
      name, 
      published_file_type="Image",
      thumbnail_path = thumbpath,
      version_number = shot['version'],
      description = 'Version 0',
      version_entity = version_sg)
    publish_update = sg.update('PublishedFile', p_file['id'], data)

    nuke.delete(burn)
    nuke.delete(read)




















