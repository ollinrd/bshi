# Copyright (c) 2016 ollin vfx
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.
from tank.platform import Application
import sgtk
import os
from hiero.ui import *
from hiero.core import *
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtWidgets import *
from tank import Hook


class HieroTrackGenHook(Hook):

    def execute(self, cadena, **kwargs):
        """
        Main hook entry point
        :param path:         the path where frames should be found.
        :param first_frame: Start frame for the output movie
        :param last_frame:  End frame for the output movie
        :param shot:                shotgun shot
        """
        self.create_seq_track()

    def create_track(self, hiero_sequence, track_name):
        track = VideoTrack(track_name)
        hiero_sequence.addTrack(track)
        return track

    def create_clip_for_track(self, path, name, bin1):

        source = MediaSource(path)
        clip = Clip(source)
        clip.setName(name)
        ref_binitem = BinItem(clip)
        bin1.addItem(ref_binitem)
        return clip


    def createTrackItem(self, track, trackItemName, sourceClip, lastTrackItem, trim_in, trim_out):
        # create the track item
        trackItem = track.createTrackItem(trackItemName)
        # set it's source
        trackItem.setSource(sourceClip)

        # set it's timeline in and timeline out values, offseting by the track item before if need be
        if lastTrackItem:
            trackItem.trimIn(trim_in)
            trackItem.trimOut(trim_out)
            trackItem.setTimelineIn(lastTrackItem.timelineOut() + 1)
            trackItem.setTimelineOut(lastTrackItem.timelineOut() + sourceClip.duration()- trim_out)
        else:
            trackItem.trimIn(trim_in)
            trackItem.trimOut(trim_out)
            trackItem.setTimelineIn(0)
            trackItem.setTimelineOut(trackItem.sourceDuration() - trim_out)

        # add the item to the track
        track.addItem(trackItem)
        return trackItem

    def createTrackItem_rev(self, track, trackItemName, sourceClip,  edt_in, edt_out, lastTrackItem, trim_in, trim_out):
        # create the track item
        trackItem = track.createTrackItem(trackItemName)
        # set it's source
        trackItem.setSource(sourceClip)
        # set it's timeline in and timeline out values, offseting by the track item before if need be
        trackItem.setSourceIn(trim_in +1)
        trackItem.setSourceOut(sourceClip.sourceOut() - trim_out)
        trackItem.setTimelineIn(edt_in)
        trackItem.setTimelineOut(edt_out)
        # add the item to the track
        track.addItem(trackItem)
        return trackItem


    def create_all_tracks(self, sequence, cut_name):
        engine = sgtk.platform.current_engine()
        project = engine.context.project
        seq_name = sequence['code']
        cut = cut_name
        filters = [['code', 'is', seq_name], ['project', 'is', project]]
        sg_sequence = engine.shotgun.find_one('Sequence', 
                                              filters,
                                              ['shots', 'code'])
        filters = [['code', 'is', cut], ['project', 'is', project]]
        hiero_sequence = Sequence(seq_name)
        myProject = projects()[-1]
        bin1 = Bin(seq_name)
        clipsBin = myProject.clipsBin()
        clipsBin.addItem(BinItem(hiero_sequence))
        # create sequence in hiero
        clipsBin.addItem(bin1)
        # get cut
        cut = engine.shotgun.find_one('Cut',
                                      filters,
                                      ['version.Version.sg_path_to_movie',
                                       'code', 'cut_items', 'duration'])
        ref_path = cut['version.Version.sg_path_to_movie']
        # create ref item
        ref_clip = self.create_clip_for_track(ref_path, seq_name + '_reference', bin1)
        # create ref track
        reference_track = self.create_track(hiero_sequence, 'Reference')
        # create ref track item
        ref_trackitem = self.createTrackItem(reference_track, seq_name + '_reference', ref_clip, None, 0, 0)
        # get cut items
        shots = []
        new_cutitems = []
        for cut_item in cut['cut_items']:
            filters = [['id', 'is', cut_item['id']]]
            cut_item = engine.shotgun.find_one('CutItem',
                                               filters,
                                               ['code', 'shot', 'edit_in',
                                                'edit_out', 'cut_order'])
            # get shots 
            if not cut_item['shot'] in shots:
                shots.append(cut_item['shot'])
            new_cutitems.append(cut_item)
        new_cutitems = sorted(new_cutitems, key=lambda i: i['cut_order'])


        # get versions
        versions_zero = []
        task_names = []
        versions_w_task = {}

        if not self.client:
            for shot in shots:
                filters =[['entity', 'is', shot]]
                versions = engine.shotgun.find('Version', filters, ['sg_path_to_frames',
                                                                    'sg_path_to_movie',
                                                                    'updated_at', 'code',
                                                                    'sg_task.Task.step',
                                                                    'sg_task', 'entity',
                                                                    'sg_task.Task.step.Step.code',
                                                                    'sg_version_type'])
                if versions:
                    for version in versions:
                        if 'version0_edt_mp01' in version['code'] and 'client' not in version['code'].lower():
                            versions_zero.append(version)
                        if version['sg_task']:
                            if version['sg_task.Task.step']:
                                if version['sg_task.Task.step.Step.code'] and version['entity']:
                                    if version['sg_task.Task.step.Step.code'] not in task_names:
                                        task_names.append(version['sg_task.Task.step.Step.code'])
                                    llave = version['sg_task.Task.step.Step.code'] + str(version['entity']['id'])
                                    if llave not in versions_w_task:
                                        versions_w_task[llave] = []
                                    if version['sg_version_type'] not in ['Client Delivery', 'Client']:
                                        versions_w_task[llave].append(version)
            v0_shot = {}
            recent_v0 ={}
            for cutit in new_cutitems:
                if cutit['shot']:

                    for version_zero in versions_zero:
                        if version_zero['entity']['id'] ==cutit['shot']['id']:
                            if not str(cutit['shot']['id']) in v0_shot:
                                v0_shot[str(cutit['shot']['id'])] =[]
                            v0_shot[str(cutit['shot']['id'])].append(version_zero)

            for i in v0_shot:
                last_date = v0_shot[i][0]['updated_at']
                for vz in v0_shot[i]:
                    if last_date <= vz['updated_at']:
                        recent_v0[i] = vz
            # create v0 track
            track_v0 = self.create_track(hiero_sequence, 'Version 0')
            lastTrackItem= None
            for rec_v0 in recent_v0:
                sources = []
                clips = []
                names = []
                trim_in = []
                trim_out = []
                edt_start = []
                edt_out = []
                filters = [['id', 'is', recent_v0[rec_v0]['entity']['id']]]
                shot = engine.shotgun.find_one('Shot', filters, ['sg_head_in', 'sg_tail_out',
                                                                 'code', 'sg_cut_in', 'sg_cut_out'])
                for cutit in new_cutitems:
                    if cutit['shot']:
                        if cutit['shot']['id'] == recent_v0[rec_v0]['entity']['id']:
                            edt_in = cutit['edit_in']
                            edt_out = cutit['edit_out']
                if shot:
                    t_in = shot['sg_cut_in'] - shot['sg_head_in']
                    t_out = shot['sg_tail_out'] - shot['sg_cut_out']
                    trim_in = t_in
                    trim_out = t_out
                    name = shot['code']
                    path = recent_v0[rec_v0]['sg_path_to_movie']
                    clip = self.create_clip_for_track(path, name, bin1)
                    lastTrackItem = self.createTrackItem_rev(track_v0, name, clip,
                                                        edt_in, edt_out, lastTrackItem,
                                                        trim_in, trim_out)
            recent_tasks = {}
            task_tracks = {}
            for i in versions_w_task:
                last_date = versions_w_task[i][0]['updated_at']
                for version_w_task in versions_w_task[i]:
                    if last_date <= version_w_task['updated_at']:
                        recent_tasks[i] = version_w_task
            task_tracks = {}
            for i in task_names:
                task_tracks['{0}'.format(i)] = self.create_track(hiero_sequence, i)
            for recent_task in recent_tasks:
                
                sources = []
                clips = []
                names = []
                trim_in = []
                trim_out = []
                edt_start = []
                edt_out = []
                filters = [['id', 'is', recent_tasks[recent_task]['entity']['id']]]
                shot = engine.shotgun.find_one('Shot', filters, ['sg_head_in', 'sg_tail_out',
                                                                 'code', 'sg_cut_in', 'sg_cut_out'])
                for cutit in new_cutitems:
                    if cutit['shot']:
                        if cutit['shot']['id'] == recent_tasks[recent_task]['entity']['id']:
                            edt_in = cutit['edit_in']
                            edt_out = cutit['edit_out']
                if shot:
                    t_in = shot['sg_cut_in'] - shot['sg_head_in']
                    t_out = shot['sg_tail_out'] - shot['sg_cut_out']
                    trim_in = t_in
                    trim_out = t_out
                    name = shot['code']
                    path = recent_tasks[recent_task]['sg_path_to_movie']
                    clip = self.create_clip_for_track(path, name, bin1)
                    print name, edt_in, edt_out 
                    lastTrackItem = self.createTrackItem_rev(task_tracks['{0}'.format(recent_tasks[recent_task]['sg_task.Task.step.Step.code'])], name, clip,
                                                        edt_in, edt_out, lastTrackItem,
                                                        trim_in, trim_out)
        else:
            for shot in shots:
                if shot:
                    filters =[['entity', 'is', shot],['project', 'is', project]]
                    versions = engine.shotgun.find('Version', filters, ['sg_path_to_frames',
                                                                        'sg_path_to_movie',
                                                                        'updated_at', 'code',
                                                                        'sg_task.Task.step',
                                                                        'sg_task', 'entity',
                                                                        'sg_task.Task.step.Step.code',
                                                                        'sg_version_type',
                                                                        'sg_status_list'])
                    '''
                    if versions:
                        for version in versions:
                            if version['sg_task']:
                                if version['sg_task.Task.step']:
                                    if version['sg_task.Task.step.Step.code'] and version['entity']:
                                        if version['sg_task.Task.step.Step.code'] not in task_names:
                                            task_names.append(version['sg_task.Task.step.Step.code'])
                                        llave = version['sg_task.Task.step.Step.code'] + str(version['entity']['id'])
                                        if version['sg_version_type'] in ['Client']:
                                            if version['sg_status_list'] in ['edapp']:
                                                if llave not in versions_w_task:
                                                    versions_w_task[llave] = []
                                                    versions_w_task[llave].append(version)
                                            elif version['sg_status_list'] in ['dlvr']:
                                                if llave not in versions_w_task:
                                                    versions_w_task[llave] = []
                                                    versions_w_task[llave].append(version)
                    '''
                    cli_template = engine.get_template_by_name('nuke_shot_render_2048')
                    if versions:
                        for version in versions:
                            if version['sg_version_type'] in ['Client'] and 'V000' not in version['code']:
                                print version['code']
                                if version['sg_path_to_movie']:
                                    fields = cli_template.get_fields(version['sg_path_to_movie'])
                                    if fields['Step_name'] and version['entity']:
                                        if fields['Step_name'] not in task_names:
                                            task_names.append(fields['Step_name'])
                                        llave = fields['Step_name'] + str(version['entity']['id'])
                                        if version['sg_status_list'] in ['edapp']:
                                            if llave not in versions_w_task:
                                                versions_w_task[llave] = []
                                                versions_w_task[llave].append(version)
                                        elif version['sg_status_list'] in ['dlvr']:
                                            if llave not in versions_w_task:
                                                versions_w_task[llave] = []
                                                versions_w_task[llave].append(version)

    


            recent_tasks = {}
            task_tracks = {}
            lastTrackItem = None
            for i in versions_w_task:
                print versions_w_task[i]
                last_date = versions_w_task[i][0]['updated_at']
                for version_w_task in versions_w_task[i]:
                    if last_date <= version_w_task['updated_at']:
                        recent_tasks[i] = version_w_task
            task_tracks = {}
            apr_track = self.create_track(hiero_sequence, 'Apr_versions')
            for recent_task in recent_tasks:
                
                sources = []
                clips = []
                names = []
                trim_in = []
                trim_out = []
                edt_start = []
                edt_out = []
                filters = [['id', 'is', recent_tasks[recent_task]['entity']['id']]]
                shot = engine.shotgun.find_one('Shot', filters, ['sg_head_in', 'sg_tail_out',
                                                                 'code', 'sg_cut_in', 'sg_cut_out'])
                for cutit in new_cutitems:
                    if cutit['shot']:
                        if cutit['shot']['id'] == recent_tasks[recent_task]['entity']['id']:
                            edt_in = cutit['edit_in']
                            edt_out = cutit['edit_out']
                if shot:
                    t_in = shot['sg_cut_in'] - shot['sg_head_in']
                    t_out = shot['sg_tail_out'] - shot['sg_cut_out']
                    trim_in = t_in
                    trim_out = t_out
                    name = shot['code']
                    path = recent_tasks[recent_task]['sg_path_to_movie']
                    clip = self.create_clip_for_track(path, name, bin1)
                    print name, edt_in, edt_out 
                    lastTrackItem = self.createTrackItem_rev(apr_track, name, clip,
                                                        edt_in, edt_out, lastTrackItem,
                                                        trim_in, trim_out)



    def create_seq_track(self):
        self.mod_ps_publish = self.parent.import_module('tk_hiero_track')
        engine = sgtk.platform.current_engine()
        self.engined = engine
        project = engine.context.project
        self.project = project
        self.contin = True
        self.show_ui()
        if self.contin:
            if self.sg_sequence == '':
                    warning = QMessageBox()
                    warning.setText('Please select one Cut first !!!')
                    warning.setStandardButtons(QMessageBox.Ok)
                    sure = warning.exec_()
                    return
            sg_sequence = self.sg_sequence
            cuts = self.cuts
            
            for cut in cuts:
                if cut['code'] == self.corte.split(' ')[0] and str(cut['duration']) == self.corte.split(' ')[1]:
                    cut = cut
            self.create_all_tracks(sg_sequence, cut['code'])
            warning = QMessageBox()
            warning.setText('Complete!!!!!!!!')
            warning.setStandardButtons(QMessageBox.Ok)
            sure = warning.exec_()


    def show_ui(self):
        self.export_ui = self.mod_ps_publish.Ui_Form()
        self.Dialog = QtWidgets.QDialog()
        self.export_ui.setupUi(self.Dialog)
        self.export_ui.pushButton.clicked.connect(self.get_cuts)
        self.export_ui.buttonBox.accepted.connect(self.get_params)
        self.export_ui.buttonBox.rejected.connect(self.cerrar)
        self.Dialog.exec_()

    def get_cuts(self):
        self.export_ui.comboBox.clear()
        self.seq_name = str(self.export_ui.lineEdit.text())
        filters = [['code', 'is', self.seq_name], ['project', 'is', self.project]]
        sg_sequence = self.engined.shotgun.find_one('Sequence', 
                                              filters,
                                              ['shots', 'code'])
        if not sg_sequence:
            self.cerrar()
            warning = QMessageBox()
            warning.setText('Bad Sequence name !!!!!!!!')
            warning.setStandardButtons(QMessageBox.Ok)
            sure = warning.exec_()
            return
        filters = [['entity', 'is', sg_sequence], ['project', 'is', self.project]]
        cuts = self.engined.shotgun.find('Cut',
                                   filters,
                                   ['version.Version.sg_path_to_movie', 'code', 'cut_items', 'duration'])
        if 'list' in str(type(cuts)):
            for cut in cuts:
                self.export_ui.comboBox.addItem(cut['code'] + ' duration:' + str(cut['duration']))
        else:
            self.export_ui.comboBox.addItem(cut['code'] + ' duration:' + str(cut['duration']))
        self.sg_sequence = sg_sequence
        self.cuts = cuts

    def get_params(self):
        self.corte = str(self.export_ui.comboBox.currentText()).replace('duration:', '')
        self.client = self.export_ui.client_rev.isChecked()
        self.Dialog.close()

    def cerrar(self):
        self.contin = False
        self.Dialog.close()

