import nuke
import sys
import os
import time
import unicodedata
from stat import S_ISREG, ST_CTIME, ST_MODE
from datetime import datetime
from time import gmtime, strftime
import datetime as dt_
import ast
import threading
from subprocess import Popen, PIPE
import tempfile
import nuke
import shutil
import uuid
import subprocess
import datetime
shot_dict_txt = nuke.rawArgs[3]
config_path = nuke.rawArgs[4]
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
sys.path.append('{0}/install/core/python'.format(config_path))

import sgtk
import yaml


def get_settings(conf_path):
    #config_path = engine.context.tank.pipeline_configuration.get_path()
    p_specs_path = os.path.join(conf_path, "resources", "project",
                                             "project_specs.yml")
    stream = open(p_specs_path, "r")
    settings = dict(yaml.load(stream))
    return settings


def connect_to_shotgun(api_namekey, dbConnect):
  
    sg_connection = dbConnect.sgConnect()
    sg = sg_connection.connect(api_namekey)
    print 'sg: ', sg, sg_connection

    return sg, sg_connection


def start_engine(shot, work_path):
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
    sg_project = shot['project']
    tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
    conf_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
    settings = get_settings(conf_path)
    project_frame_rate = settings['frame_rate']
    api_namekey = settings.get("ingesta_editorial_apikeyname")   
    sys.path.append(settings.get("python_utils_path"))
    from pythonTools.shotgunUtils import dbConnect
    sg, sg_connect = connect_to_shotgun(api_namekey, dbConnect)
    sg_auth = sg_connect.authenticateuser_from_apiname(api_namekey, sgtk, ShotgunAuthenticator)

    tk = sgtk.sgtk_from_path(work_path)
    tk.synchronize_filesystem_structure()
    ctx = ctx = tk.context_from_entity("Shot", shot['id'])
    engine = sgtk.platform.start_engine("tk-nuke", tk, ctx)
    return engine, tk, ctx, project_frame_rate

def get_sg_templates(engine):
    config_path = engine.context.tank.pipeline_configuration.get_path()
    with open(config_path + "/config/resources/project/project_specs.yml", 'r') as stream:
        try:
            specs = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    print 'SPECS:', specs
    return specs

def _fill_slate_labels(burn, shot, name_file, external_name_file, user):
    artist_name = user
    artist_label = burn.node("client").node("label_artist3")
    shot_label = burn.node("client").node("label_internal3")
    message = "Artist: {0}".format(artist_name)
    artist_label["message"].setValue(message)
    if shot.get("sg_lens_focal_length"):
        message = "LENS: {0}mm".format(shot["sg_lens_focal_length"])
        lens_label = burn.node("client").node("label_lens1")
        lens_label["message"].setValue(message)
    ext_first = shot['sg_external_in_frame']
    first_frame = shot['sg_head_in']
    last_frame = shot['sg_tail_out']
    dif = last_frame - first_frame + 1
    ext_last = ext_first + dif
    handles = first_frame - 1000 -1
    # burn.node('client').node('label_framerange1').knob('message').setValue('FRAME RANGE: {0}-{1}'.format(ext_first + handles, ext_last+ handles -1))
    task_name = 'TASK: COMP'
    # burn.node('client').node('task_name1').knob('message').setValue('{0}'.format(task_name))
    shot_label["message"].setValue(str(external_name_file))
    burn.node("client").node("label_script_name2")['message'].setValue(name_file)
    burn.node("client").node("label_sequence2")['message'].setValue('Sequence: ' + str(shot['sg_sequence.Sequence.code']))
    date_label = burn.node("client").node("label_date_time3")
    date = "{0}".format(datetime.date.today().strftime('%m-%d-%Y'))
    date_label["message"].setValue(date)
    comment_label = burn.node("client").node("label_comments2")
    if shot.get("sg_resx") and shot.get("sg_resy"):
        formato = '{0} {1} CLAW_A'.format(shot['sg_resx'], shot['sg_resy'])
        nuke.addFormat(formato)
        burn.node("client").node("Reformat2")['format'].setValue('CLAW_A')
        print 'RESolution: {0} {1}'.format(shot['sg_resx'], shot['sg_resy'])

        message = "Version 0"
        comment_label["message"].setValue(message)




def mov_publish_file_and_version(shot, out_name, SHOT, mov_path,
                                 scan_path, usuario, first_frame, last_frame,
                                 total_f, rate, client_pix_jpg_path):
    if 'PIX' in mov_path:
        out_name = out_name + '_pix_v0'
    else:
        out_name = out_name + '_v0'
    rango = '{0}-{1}'.format(SHOT['sg_head_in'], SHOT['sg_tail_out'])
    data = {'project': shot['project'],
            'code': out_name,
            'sg_version_type': 'Client',
            'sg_status_list': 'na',
            'entity': SHOT,
            'sg_path_to_movie': mov_path,
            'sg_path_to_frames': scan_path,
            'user': usuario,
            'created_by': usuario,
            'sg_first_frame': first_frame,
            'sg_last_frame': last_frame,
            'frame_count': total_f,
            'sg_uploaded_movie_frame_rate': float(rate),
            'frame_range': rango,
            'sg_movie_has_slate': True,
            'sg_vendor': 'ollinvfx',
            'sg_version_file_type': 'MOV',
            'client_code': out_name.replace('comp', 'COMP'),
            'sg_submission_notes': 'v0'
            }
    filters = [['code', 'is', out_name], ['project', 'is', shot['project']]]
    version_sg = engine.shotgun.find_one('Version', filters)
    if not version_sg:
        version_sg = engine.shotgun.create('Version', data)
    else:
        engine.shotgun.delete('Version', version_sg['id'])
        version_sg = engine.shotgun.create('Version', data)
    try:
        engine.shotgun.upload('Version', version_sg['id'], mov_path, 'sg_uploaded_movie')
    except:
        print 'NO SE PUDO SUBIR EL VIDEO'

    thumbpath = client_pix_jpg_path.replace('%04d', str(first_frame))

    file_to_publish = mov_path
    name = os.path.basename(file_to_publish)
    name = name.replace('_' + name.split('_')[-1], '')
    if 'PIX' in mov_path:
        name = name + '_pix_v0'
    else:
        name = name + '_v0'
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['code', 'is', os.path.basename(file_to_publish)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    sgtk.util.register_publish(
      tk,
      ctx,
      file_to_publish, 
      name,
      published_file_type="Movie",
      version_number=shot['version'],
      description='Client ersion 0',
      version_entity=version_sg)

def mov_publish_file_and_version_exr(shot, out_name, SHOT, mov_path,
                                 client_exr_path, usuario, first_frame, last_frame,
                                 total_f, rate, client_pix_jpg_path):

    out_name = out_name + '_v0[{0}-{1}]'.format(first_frame, last_frame)
    rango = '{0}-{1}'.format(SHOT['sg_head_in'], SHOT['sg_tail_out'])
    data = {'project': shot['project'],
            'code': out_name,
            'sg_version_type': 'Client Delivery',
            'sg_status_list': 'na',
            'entity': SHOT,
            'sg_path_to_frames': client_exr_path,
            'user': usuario,
            'created_by': usuario,
            'sg_first_frame': first_frame,
            'sg_last_frame': last_frame,
            'frame_count': total_f,
            'client_code': out_name.replace('comp', 'COMP'),
            'sg_uploaded_movie_frame_rate': float(rate),
            'frame_range': rango,
            'sg_movie_has_slate': True,
            'sg_vendor': 'ollinvfx',
            'sg_version_file_type': 'EXR',
            'sg_submission_notes': 'v0'
            }
    filters = [['code', 'is', out_name], ['project', 'is', shot['project']]]
    version_sg = engine.shotgun.find_one('Version', filters)
    if not version_sg:
        version_sg = engine.shotgun.create('Version', data)
    else:
        engine.shotgun.delete('Version', version_sg['id'])
        version_sg = engine.shotgun.create('Version', data)
    try:
        engine.shotgun.upload('Version', version_sg['id'], mov_path, 'sg_uploaded_movie')
    except:
        print 'NO SE PUDO SUBIR EL VIDEO'

    #thumbpath = client_pix_jpg_path.replace('%04d', str(first_frame))

    file_to_publish = mov_path
    name = os.path.basename(file_to_publish)
    name = name.replace('_' + name.split('_')[-1], '')

    name = name + '_v0[{0}-{1}]'.format(first_frame, last_frame)
    p_file = sg.find_one('PublishedFile', [['project', 'is', shot['project']], ['code', 'is', os.path.basename(file_to_publish)]])
    if not p_file is None:
        sg.delete("PublishedFile", p_file['id'])
    sgtk.util.register_publish(
      tk,
      ctx,
      file_to_publish, 
      name,
      published_file_type="Image",
      version_number=shot['version'],
      description='Client ersion 0',
      version_entity=version_sg)




# read dictionary from hiero
file = open(shot_dict_txt, 'r')
SHOTLIST = file.read()
file.close()
shot = ast.literal_eval(SHOTLIST)
seq_name = shot['seq']
work_path = shot['clip_plate_path'].split('/' + seq_name + '/')[0]
engine, tk, ctx, project_frame_rate = start_engine(shot, work_path)
sg_project = shot['project']
tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
conf_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')

specs = get_settings(conf_path)
pro_code = shot['project']['code']
shot_name = shot['clip_name']
scan_path = shot['clip_plate_path']

formato = specs['main_format']
burnin_path = '{0}/resources/ovfx_BSHI_Burnin.nk'.format(conf_path)
print 'BURNING: ', burnin_path
print 'SCAN PATH: ', scan_path
print 'MAIN FORMAT: ', formato
print 'ENGINE = ', engine
gizmo_path = specs['version_zero_nuke_gizmo_path']
sgtk.util.append_path_to_env_var("NUKE_PATH", gizmo_path)
sg = engine.shotgun
usuario = sg.find_one('HumanUser', [['login', 'is', shot['artist']]],['name'])
artist = usuario["name"]
plate_template = engine.get_template_by_name(specs['scan_template'])
client_edit_template = engine.get_template_by_name("nuke_shot_render_prores")
client_exr_template = engine.get_template_by_name("nuke_shot_delivery_exr")
internal_jpgs_template = engine.get_template_by_name("nuke_shot_render_jpg_out_v0")
internal_rev_template = engine.get_template_by_name("nuke_shot_render_movie_v0")
internal_pub_template = engine.get_template_by_name("nuke_shot_render_pub")
cdl_template = engine.get_template_by_name("cdl_shot")
SHOT = sg.find_one('Shot', [['code', 'is', shot_name],['project', 'is', shot['project']]], ['code', 'sg_head_in','sg_tail_out', 'sg_lens_focal_length','sg_sequence.Sequence.code',
                                                                                            'sg_external_id', 'sg_in_timecode', 'sg_camera', 'sg_sequence',
                                                                                            'sg_offset_frame', 'sg_resx', 'sg_resy', 'sg_without_lut',
                                                                                            'sg_str_timecode', 'sg_external_in_frame', 'sg_lut'])
nuke.root()['colorManagement'].setValue('OCIO')
nuke.root()['OCIO_config'].setValue('aces_1.0.3')
nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
nuke.root()['int16Lut'].setValue('ACES - ACEScc')
nuke.root()['logLut'].setValue('Input - ADX - ADX10')
nuke.root()['floatLut'].setValue('ACES - ACES2065-1')

if SHOT:
    fields = shot['fields']
    fields['project_code'] = pro_code
    fields['task_name'] = 'edt'
    fields['Step'] = 'editorial'
    fields['Step_name'] = 'comp'
    fields['name'] = 'master'
    fields['external_id'] = SHOT['sg_external_id']
    VERSION = shot['version']
    str_v = '{0:03d}'.format(int(VERSION))
    SEQ = sg.find_one('Sequence', [['id', 'is', SHOT['sg_sequence']['id']]], ['sg_lut'])
    plate_path = scan_path

    cdl_path = cdl_template.apply_fields(fields)
    internal_jpgs_path = internal_jpgs_template.apply_fields(fields)
    internal_rev_path = internal_rev_template.apply_fields(fields)
    internal_jpgs_path = internal_jpgs_path.replace('master', fields['elementName'])
    internal_rev_path = internal_rev_path.replace('master', fields['elementName'])
    internal_pub_path = internal_pub_template.apply_fields(fields)

    fields['version_dlv'] = 0
    fields['version'] = 0
    client_edit_path = client_edit_template.apply_fields(fields)
    client_exr_path = client_exr_template.apply_fields(fields)
    date_aux = strftime("%m-%d-%Y", gmtime()).split("-")
    date = date_aux[0] + " / " + date_aux[1] + " / " + date_aux[2]
    first_frame = SHOT['sg_head_in']
    last_frame = SHOT['sg_tail_out']
    images = os.listdir(os.path.dirname(plate_path))
    images.sort()
    total_f = int(last_frame) - int(first_frame) + 1
    out_name = os.path.basename(client_edit_path).split('.')[0]
    engine.ensure_folder_exists(os.path.dirname(client_edit_path))
    engine.ensure_folder_exists(os.path.dirname(client_exr_path))

    read = nuke.nodes.Read(name="read_source", file=scan_path, first=first_frame, last=last_frame )
    task_name = 'TASK: COMP'
    lens = 'LENS: ' + str(SHOT['sg_lens_focal_length'])
    slate_name = out_name
    submission_notes = 'Version 0'
    ext_first = SHOT['sg_external_in_frame']
    dif = last_frame - first_frame + 1
    ext_last = ext_first + dif
    burn = nuke.nodePaste(burnin_path)
    read.knob('colorspace').setValue('ACES - ACEScg')
    burn.setInput(0, read)
    nuke.root()['last_frame'].setValue(last_frame)
    nuke.root()['first_frame'].setValue(first_frame)
    burn.node('client').node("Switch")["which"].setValueAt(0, first_frame - 1)
    burn.node('client').node("Switch")["which"].setValueAt(1, first_frame)
    burn.node('client').node("Switch2")["which"].setValueAt(0, first_frame - 1)
    burn.node('client').node("Switch2")["which"].setValueAt(1, first_frame)
    if SHOT['id'] in [12986]:
        burn.node('client').node("OCIOColorSpace2")['disable'].setValue(False)
        read['raw'].setValue(True)
        burn.node('client').node("OCIOColorSpace2")['disable'].setValue(False)
        burn.node('client').node('write_avid1')['raw'].setValue(True)
    burn.node('client').node('label_comments').knob('message').setValue('{0}'.format('Version 0'))
    burn.node('client').node('label_date_time').knob('message').setValue('{0}'.format(date))
    burn.node('client').node('label_script_name').knob('message').setValue('Script: {0}'.format(out_name + '.nk'))
    burn.node('client').node('label_sequence').knob('message').setValue('')
    burn.node('client').node('label_internal').knob('message').setValue('{0}'.format(SHOT['sg_external_id']))
    burn.node('client').node('label_artist').knob('message').setValue('Artist: {0}'.format(artist))
    burn.node('client').node('label_frames').knob('message').setValue('{0}'.format(total_f))
    burn.node('client').node('label_date_times1').knob('message').setValue('{0}'.format(date))
    burn.node('client').node('label_version1').knob('message').setValue('{0}'.format(out_name))
    burn.node('client').node('label_artistname').knob('message').setValue('{0}'.format(artist))
    burn.node('client').node('label_project').knob('message').setValue('BSHI')
    burn.node('client').node('label_project2').knob('message').setValue('BSHI')
    handles = first_frame - 1000 

    timeco = SHOT['sg_str_timecode']
    print timeco
    set_time = burn.node("client").node("AddTimeCode2")
    set_time['startcode'].setValue(timeco)
    set_time['frame'].setValue(first_frame - 1)

    burn.node('client').node('write_avid1').knob('file').setValue(client_edit_path)

    prefixx = specs['tmp_folder']
    dictPath = prefixx + "Reformat_publish_v000_client_{0}_".format(shot_name) + str(uuid.uuid4()) +'.nk'
    nuke.nuke.scriptSaveAs(dictPath)
    print dictPath

    nuke.executeMultiple([burn.node('client').node('write_avid1')], ([first_frame - 1, last_frame, 1],),[nuke.views()[0]])

    nuke.frame(first_frame)
    switch = burn.node("client").node("Switch2")
    switch_which= switch['which']
    switch_which.setAnimated()

    _fill_slate_labels(burn, SHOT, out_name + '.nk', SHOT['sg_external_id'], artist)

    write_n2 = burn.node('client').node('PublisherEXT')
    write_n2['file'].setValue(client_exr_path)
    handle = first_frame - 1000
    clif = int(SHOT['sg_external_in_frame']) + handle - 1
    dur = last_frame - first_frame + 1
    ex_last = clif + dur
    read['frame_mode'].setValue('start at')
    read['frame'].setValue(str(clif))
    switch_which.setValueAt(0, clif - 1)
    switch_which.setValueAt(1, clif)
    set_time = burn.node("client").node("AddTimeCode3")
    set_time['startcode'].setValue(timeco)
    set_time['frame'].setValue(int(SHOT['sg_external_in_frame']) - 1)

    nuke.executeMultiple([write_n2], ([clif - 1, ex_last, 1],), [nuke.views()[0]])
    try:
        if shot['framerate']:
            rate = shot['framerate']
        else:
            rate = project_frame_rate
    except:
        rate = project_frame_rate

    mov_publish_file_and_version(shot, out_name, SHOT, client_edit_path,
                                 scan_path, usuario, first_frame, last_frame,
                                 total_f, rate, client_edit_path
                                )


    mov_publish_file_and_version_exr(shot, out_name, SHOT, client_edit_path,
                                 client_exr_path, usuario, first_frame, last_frame,
                                 total_f, rate, client_edit_path
                                )
    

    nuke.delete(burn)
    nuke.delete(read)
