import sys
import os
import tempfile
import ast
import nuke
import shutil
import subprocess
import tempfile

shot_dict_txt = nuke.rawArgs[3]
config_path = nuke.rawArgs[4]

sys.path.append('{0}/install/core/python'.format(config_path))
sys.path.append("/nfs/ovfxToolkit/Resources/site-packages")
import shotgun_api3
import yaml
import sgtk

file = open(shot_dict_txt,'r')
SHOTLIST = file.read() 
file.close()
shot = ast.literal_eval(SHOTLIST)
print shot

def get_settings(config_path):

    #config_path = engine.context.tank.pipeline_configuration.get_path()
    p_specs_path = os.path.join(config_path, "resources", "project",
    "project_specs.yml")
    stream = open(p_specs_path, "r")
    settings = dict(yaml.load(stream))
    return settings

def connect_to_shotgun(proj_settings):
  api_namekey = proj_settings.get("ingesta_editorial_apikeyname")
  sg_connect = dbConnect.sgConnect()
  sg = sg_connect.connect(api_namekey)
  print 'sg: ', sg
  return sg, sg_connect

def set_ACES_colorspace():
    nuke.root()['colorManagement'].setValue('OCIO')
    nuke.root()['OCIO_config'].setValue('aces_0.1.1')

def create_folders(path):
    if not os.path.exists(path):
        try:
            chmod = '2775'
            ovfxPath=path.split("/")
            ovfxFolder=""
            for folder in ovfxPath:
                ovfxFolder = ovfxFolder + folder + "/" 
                if not os.path.exists(ovfxFolder):
                    os.system("sudo /usr/bin/mkdir %s"%(ovfxFolder))
                    os.system("sudo /usr/bin/chmod  %s %s "%(chmod,ovfxFolder))
                    os.system("sudo /usr/bin/chown %s %s"%('core', ovfxFolder))
                    if 'editorial' in ovfxFolder:
                        os.system("sudo /usr/bin/chgrp %s %s"%('editorial', ovfxFolder))
                    else:
                        os.system("sudo /usr/bin/chgrp %s %s"%('ovfx', ovfxFolder))
        except Exception as e:
          raise e




try:
    sg_project = shot['project']
    tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
    config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
    settings = get_settings(config_path)

    sys.path.append(settings.get("python_utils_path"))
    from pythonTools.shotgunUtils import dbConnect
    sg, sg_connect = connect_to_shotgun(settings)

    fields = ['sg_cut_in', 'sg_cut_out', 'sg_color_space',  'sg_sequence','sg_ref_dnxhd']
    SHOT = sg.find_one('Shot', [['project', 'is', sg_project],['code', 'is', shot['clip_name']]],fields)

    colorspace = SHOT['sg_color_space']
    print 'Shot Color space shotgun: ', colorspace

    # Validation colospace setting for shot (first level)
    if colorspace in [None, '', 'Default']:
        # If there is a default color space value or none
        # Then we should check sequence color space
        seq_id = SHOT.get('sg_sequence').get('id')
        filters = [
            ['project', 'is', sg_project],
            ['id', 'is', seq_id]
            ]
        fields = ['code', 'sg_color_space']
        seq_sg = sg.find_one('Sequence', filters, fields)
        colorspace = seq_sg['sg_color_space']
        print 'Sequence Color Space shotgun: ', colorspace
        # Validation colospace setting for SEQUENCE (2TH   level)
        if colorspace in [None, '', 'Default']:
            # If sequence has no color space override
            project = sg.find_one('Project',[['id', 'is', sg_project['id']]],['sg_color_space'])
            colorspace = project.get('sg_color_space')
            # Validation colospace setting for PROJECT (3TH   level)
            if colorspace in [None, '']:
                colorspace = 'sRGB'

    print 'colorspace: ', colorspace

    # Set nuke project colorspace to ACES
    if colorspace == 'aces':
        if SHOT['sg_ref_dnxhd']:
            set_ACES_colorspace()

    has_cut = shot['has_cut']
    if not SHOT == None:
        # folders_command = 'cd {0}; ./tank Shot {1} folders'.format(config_path, SHOT['id'])
        sys.path.append('{0}/install/core/python'.format(config_path))
        import sgtk
        try:
              # os.system(folders_command)
              sgtk.create_filesystem_structure('Shot', SHOT['id'], engine=None)
        except Exception, e:
          print e


        if not os.path.exists('{0}'.format(os.path.dirname(shot['clip_mov']))):
            create_folders('{0}'.format(os.path.dirname(shot['clip_mov'])))
        if not os.path.exists('{0}'.format(os.path.dirname(shot['clip_jpg']))):
            create_folders('{0}'.format(os.path.dirname(shot['clip_jpg'])))
        else:
            shutil.rmtree(os.path.dirname(shot['clip_jpg']))
            create_folders('{0}'.format(os.path.dirname(shot['clip_jpg'])))

        sg_project = shot['project']
        artist = sg.find_one('HumanUser', [['login', 'is', shot['artist']]])
        first_frame = shot['clip_first']
        nuke.nodePaste(config_path + '/resources/Hiero/hiero_export.nk')
        # nuke.nodePaste(config_path + settings["nuke_reference_script"])

        last_frame = shot['clip_last']
        orig_path = shot['cut_origin_path']
        read = nuke.nodes.Read(name="SCANS", file=orig_path, first=first_frame, last=last_frame )
        if colorspace == 'aces':
            if SHOT['sg_ref_dnxhd']:
                read['colorspace'].setValue('aces')


        nuke.root()['first_frame'].setValue(first_frame)
        nuke.root()['last_frame'].setValue(last_frame)
        write_images = nuke.toNode('write_images')
        write_images['file'].setValue(shot['clip_jpg'])

        mov =nuke.toNode('mov')
        lab_name = nuke.toNode('lab_name')
        lab_frame = nuke.toNode('lab_frame')

        #lab_frame.setInput(0, read)
        croper = nuke.toNode('Crop1')
        croper.setInput(0, read)
        lab_name['message'].setValue(shot['clip_name'])
        write_images.setInput(0, lab_name)
        mov.setInput(0, lab_name)
        mov['file'].setValue(shot['clip_mov'])
        mov['mov64_fps'].setValue(float(shot['framerate']))
        if SHOT['sg_ref_dnxhd']:
            mov['colorspace'].setValue(colorspace)

        nuke.executeMultiple([mov], ([first_frame , last_frame, 1],), [nuke.views()[0]])
        read['frame_mode'].setValue('start at')
        read['frame'].setValue(str(SHOT['sg_cut_in']))
        read['raw'].setValue(True)
        nuke.root()['first_frame'].setValue(SHOT['sg_cut_in'])
        nuke.root()['last_frame'].setValue(SHOT['sg_cut_out'])
        nuke.executeMultiple([write_images], ([SHOT['sg_cut_in'], SHOT['sg_cut_out'] , 1],),
                                           [nuke.views()[0]])
        nuke.root()['first_frame'].setValue(first_frame)
        nuke.root()['last_frame'].setValue(last_frame)
        data = {'sg_first_frame': SHOT['sg_cut_in'],
                    'sg_last_frame': SHOT['sg_cut_out'],
                    'frame_count': shot['clip_duration'],
                    'code': str(sg_project['code']) + '_{0}_edt_{2}_v{1}'.format(shot['clip_name'],shot['v_str'], shot['t_name']),
                    'frame_range': '{0}-{1}'.format(SHOT['sg_cut_in'],SHOT['sg_cut_out']),
                    'sg_uploaded_movie_frame_rate':float(shot['framerate']),
                    'entity': SHOT,
                    'sg_path_to_movie': shot['clip_mov'],
                    'sg_path_to_frames': shot['clip_jpg'],
                    'description': 'Editorial Cut Reference',
                    'sg_status_list': 'na',
                    'user': artist,
                    'sg_version_type': 'Reference',
                    'project': sg_project }

        vers_sg = sg.find_one('Version',[['project', 'is', sg_project],['code', 'is', str(sg_project['code']) + '_{0}_edt_{2}_v{1}'.format(shot['clip_name'],shot['v_str'],shot['t_name'])]])
        if not vers_sg:
            data['created_by'] = artist
            vers_sg = sg.create('Version',data)
        else:
            # data['updated_by'] = artist
            vers_sg = sg.update('Version', vers_sg['id'], data)
        if has_cut:
            cut_items = sg.find('CutItem',[['shot', 'is', SHOT]],['version'])
            for cut_it in cut_items:
                data_cut = {'version': vers_sg}
                sg.update('CutItem',cut_it['id'],data_cut)
        #sg.upload('Version', vers_sg['id'], shot['clip_mov'],'sg_uploaded_movie')

        file_to_publish = shot['clip_mov']

        from tank_vendor.shotgun_authentication import ShotgunAuthenticator

        api_namekey = settings.get("ingesta_editorial_apikeyname")
        sg_auth = sg_connect.authenticateuser_from_apiname(api_namekey, sgtk, ShotgunAuthenticator)

        name = os.path.basename(file_to_publish)
        ver_t = name.split('_')[-1].split('.')[0]
        print 'HERE:', ver_t
        name = name.replace('_' + name.split('_')[-1], '')
        tk = sgtk.sgtk_from_path(file_to_publish)
        ctx = tk.context_from_entity("Shot", SHOT['id'])
        path = shot['cut_origin_path']
        mid_frame = int(int(shot['clip_last'] - shot['clip_first'])/2)
        mid_frame = int(shot['clip_first'] + mid_frame)
        print 'NK FOR RENDER:' + config_path + settings["nuke_reference_script"]
        # nuke.nodePaste(config_path + settings["nuke_reference_script"])
        nuke.nodePaste(config_path + '/resources/Hiero/hiero_export.nk')
        prefixx = settings["tmp_folder"]
        (file, thumbpath) = tempfile.mkstemp(prefix=prefixx + "ver_thumb",suffix='.jpg')
        print thumbpath
        write_images = nuke.toNode('write_images')
        write_images['file'].setValue(thumbpath)
        if colorspace == 'aces':
            write_images['colorspace'].setValue('aces')
        read_th = nuke.nodes.Read(name="THUMB", file=path, first=mid_frame, last=mid_frame )
        read_th['raw'].setValue(True)
        if colorspace == 'aces':
            read_th['colorspace'].setValue('aces')
        write_images.setInput(0, read_th)
        reformat = nuke.toNode('Reformat1')
        # write_images.setInput(0, reformat)

        nuke.executeMultiple([write_images], ([mid_frame , mid_frame , 1],),
                                           [nuke.views()[0]])
        fields = ['sg_sequence.Sequence.code']
        SEQ = sg.find_one('Shot', [['project', 'is', sg_project],['code', 'is', shot['clip_name']]],fields)['sg_sequence.Sequence.code']
        sequence = sg.find_one('Sequence', [['project', 'is', sg_project],['code', 'is', SEQ]])
        filters = [['project', 'is', sg_project],['entity', 'is', sequence]]
        Versions = sg.find('Version',filters,['published_files', 'code'])
        if has_cut:
            for ver in Versions:
              print ver['code']
              if ver_t in ver['code'] and '_ref_' in ver['code']:
                upstream_v = ver
            upstr = upstream_v['published_files'][0]
            print upstr



        p_file = sg.find_one('PublishedFile',[['project', 'is', sg_project],['code', 'is', os.path.basename(file_to_publish)]])
        if not p_file == None:
          sg.delete("PublishedFile", p_file['id'])
        if has_cut:
            sgtk.util.register_publish(
              tk,
              ctx,
              file_to_publish,
              name,
              published_file_type="Movie",
              thumbnail_path = thumbpath,
              sg_fields = {'upstream_published_files' : [upstr]},
              description = 'Editorial Cut Reference',
              version_number = shot['version'],
              version_entity = vers_sg)
        else:
            sgtk.util.register_publish(
              tk,
              ctx,
              file_to_publish,
              name,
              published_file_type="Movie",
              thumbnail_path = thumbpath,
              description = 'Editorial Cut Reference',
              version_number = shot['version'],
              version_entity = vers_sg)

        file_to_publish = shot['clip_jpg']
        name = os.path.basename(file_to_publish)
        name = name.replace('_' + name.split('_')[-1], '')
        p_file = sg.find_one('PublishedFile',[['project', 'is', sg_project],['code', 'is', os.path.basename(file_to_publish)]])
        if not p_file == None:
          sg.delete("PublishedFile", p_file['id'])

        if has_cut:
            sgtk.util.register_publish(
              tk,
              ctx,
              file_to_publish,
              name,
              published_file_type="Image",
              thumbnail_path = thumbpath,
              version_number = shot['version'],
              sg_fields = {'upstream_published_files' : [upstr]},
              description = 'Editorial Cut Reference',
              version_entity = vers_sg)
        else:
            sgtk.util.register_publish(
              tk,
              ctx,
              file_to_publish,
              name,
              published_file_type="Image",
              thumbnail_path = thumbpath,
              version_number = shot['version'],
              description = 'Editorial Cut Reference',
              version_entity = vers_sg)
        prefixx = '/nfs/ovfxToolkit/TMP/'
        script = '''
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
from shotgun_api3 import Shotgun
SG_URL = 'https://0vfx.shotgunstudio.com'
TANK_SCRIPT_NAME = "Toolkit"
TANK_SCRIPT_KEY = "b20e30ae264400f844d4197b5805d1105ba6ddf06fd733b51a74b8fa290cbc73"
sg = Shotgun(SG_URL, 
             TANK_SCRIPT_NAME, 
             TANK_SCRIPT_KEY)
print 'UPLOADING ....'
sg.upload("Version", {0}, '{1}', "sg_uploaded_movie")
'''.format(vers_sg["id"], shot['clip_mov'])
        (file_, dictPath) = tempfile.mkstemp(prefix=prefixx + "upload_job_submitter",
                                             suffix='.py')
        submitter_py = open(dictPath, "w")
        submitter_py.write(script)
        submitter_py.close()

        batchName = os.path.basename(shot['clip_mov']).split('.')[0]
        nuke_cmd = " -SubmitCommandLineJob -executable"
        nuke_cmd += " python -arguments "
        nuke_cmd += ' " {0}" '.format(dictPath)
        nuke_cmd += ' -name "{0}" -pool publish'.format('ref_uploader')
        nuke_cmd += " -prop BatchName={0}".format(batchName)
        subprocess.call('chmod -R 777 {0}'.format(dictPath), shell=True)
        deadline = "/opt/Thinkbox/Deadline10/bin/deadlinecommand"
        subprocess.Popen(deadline + " " + nuke_cmd, shell=True)
        print deadline + " " + nuke_cmd
        print 'Deadline send!!! \n\n'
        print vers_sg
except:
    import traceback
    print 'ERROR: ', traceback.format_exc()
    raise ValueError('Error executing python code!!!.')