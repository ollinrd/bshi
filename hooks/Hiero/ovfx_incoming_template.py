import sys
import os
import tempfile
import ast
import nuke
import shutil
shot_dict_txt = nuke.rawArgs[3]
config_path = nuke.rawArgs[4]
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
sys.path.append('{0}/install/core/python'.format(config_path))
print '{0}/install/core/python'.format(config_path)
import shotgun_api3
import sgtk
import yaml

def get_camera_for_ingest(camera_name, width, height, project_entity, sgDb):
  # This method will search for camera name in shotgun db, if camera is not found 
  # then evaluate resolution to find camera type
  camera = sgDb.find_one('Camera', [['code', 'is', camera_name], ['project', 'is', project_entity]], ['code'])
  # if camera not found then try to obtain by resolution
  if not camera:
      camera = sgDb.find_one('Camera', [['sg_width_resolution', 'is', width],
        ['sg_height_resolution', 'is', height]], ['code'])
  return camera

file = open(shot_dict_txt,'r')
SHOTLIST = file.read()
file.close()
shot = ast.literal_eval(SHOTLIST)
print shot

def create_folders(path):
    if not os.path.exists(path):
        try:
            chmod = '2775'
            ovfxPath=path.split("/")
            ovfxFolder=""
            for folder in ovfxPath:
                ovfxFolder = ovfxFolder + folder + "/" 
                if not os.path.exists(ovfxFolder):
                    os.system("sudo /usr/bin/mkdir %s"%(ovfxFolder))
                    os.system("sudo /usr/bin/chmod  %s %s "%(chmod,ovfxFolder))
                    os.system("sudo /usr/bin/chown %s %s"%('core', ovfxFolder))
                    if 'editorial' in ovfxFolder:
                        os.system("sudo /usr/bin/chgrp %s %s"%('editorial', ovfxFolder))
                    else:
                        os.system("sudo /usr/bin/chgrp %s %s"%('ovfx', ovfxFolder))
        except Exception as e:
          raise e


def wb_config(sg, master_dict):
    """
    master_dict = {
            'source_file': source_file,
            'destination_path': destin_file,
            'wb_path': shot['WB_Path'],
            'extesion': ext,
            'first_frame': min_f,
            'last_frame': max_f
        }
    """
    source_path = master_dict['source_file']
    destination_path = master_dict['destination_path']
    wb_path = master_dict['wb_path']
    first_f = master_dict['first_frame']
    last_f = master_dict['last_frame']

    header_str = 30 * '*'
    header_str += '\n' + header_str + '\n'
    end_str = header_str
    header_str += ' -- RnD --'
    # Crear nodo read
    applied = False

    if os.path.exists(wb_path):
        files = []
        for r, d, f in os.walk(wb_path):
            for file in f:
                files.append(os.path.join(r, file))

        if len(files) > 1:
            print header_str
            print 'Error:  More than one file found'
            for f in files:
                print '\t', f
            print end_str
        elif not len(files):
            print header_str
            print 'Error:  Empty folder\n\t', wb_path
            print end_str
        else:
            file_path = files[0]
            nuke.root()['first_frame'].setValue(first_f)
            nuke.root()['last_frame'].setValue(last_f)

            read_node = nuke.nodes.Read(name="Read_master", file=source_path,
                                        first=first_f, last=last_f)
            read_node['raw'].setValue(True)

            read_node['frame_mode'].setValue('start at')
            read_node['frame'].setValue('1001')

            n_ocio = nuke.createNode('OCIOCDLTransform')
            n_ocio.showControlPanel()
            n_ocio['read_from_file'].setValue(True)
            n_ocio.knob('file').setValue(file_path)
            n_ocio['reload'].execute()
            n_ocio.hideControlPanel()
            n_ocio.setInput(0, read_node)

            write_images = nuke.createNode('Write')
            write_images['file'].setValue(destination_path)
            #write_images['frame_mode'].setValue('start at')
            #write_images['frame'].setValue('1001')
            write_images.setInput(0, n_ocio)

            if 'metadata' in write_images.knobs():
                write_images['metadata'].setValue('all metadata')

            nuke.executeMultiple([write_images], ([first_f, first_f+5, 1],),
                                 [nuke.views()[0]])

            nuke.executeMultiple([write_images], ([first_f, last_f, 1],),
                                 [nuke.views()[0]])

            print ' >> >> White balance applied'

            applied = True
    else:
        print header_str
        print 'Error:  Missing white balance file'
        print 'folder missing: ', wb_path
        print end_str
    return applied


def get_settings(config_path):

    #config_path = engine.context.tank.pipeline_configuration.get_path()
    p_specs_path = os.path.join(config_path, "resources", "project",
    "project_specs.yml")
    stream = open(p_specs_path, "r")
    settings = dict(yaml.load(stream))
    return settings


def connect_to_shotgun(api_namekey, dbConnect):
  
  sg_connection = dbConnect.sgConnect()
  sg = sg_connection.connect(api_namekey)
  print 'sg: ', sg, sg_connection

  return sg , sg_connection
sg_project = shot['project']
tk = sgtk.sgtk_from_entity('Project', sg_project['id'])
config_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
settings = get_settings(config_path)
api_namekey = settings.get("ingesta_editorial_apikeyname")
sys.path.append(settings.get("python_utils_path"))
from pythonTools.shotgunUtils import dbConnect
sg, sg_connect = connect_to_shotgun(api_namekey, dbConnect)
from tank_vendor.shotgun_authentication import ShotgunAuthenticator
sg_auth = sg_connect.authenticateuser_from_apiname(api_namekey, sgtk, ShotgunAuthenticator)

sg_project = shot['project']
has_ccc = False
SHOT = sg.find_one('Shot', [['project', 'is', sg_project], ['code', 'is', shot['clip_name']]],['sg_cut_in', 'sg_cut_out', ])
if not SHOT == None:
    sys.path.append('{0}/install/core/python'.format(config_path))
    import sgtk
    try:
          sgtk.synchronize_filesystem_structure()
          sgtk.create_filesystem_structure('Shot', SHOT['id'], engine=None)
    except Exception, e:
      print e
    if not os.path.exists('{0}'.format(os.path.dirname(shot['clip_plate_path']))):
        create_folders('{0}'.format(os.path.dirname(shot['clip_plate_path'])))
    artist = sg.find_one('HumanUser', [['login', 'is', shot['artist']]])
    orig_path = shot['cut_origin_path']
    origin_files = os.listdir(os.path.dirname(orig_path))
    origin_files.sort()
    new_path = shot['clip_plate_path'].split('.')[0]
    frm = 1001
    EXTENSION = ''
    conta = 1000
    if shot['WB']:
        print 30*'*'
        print 'White balance config'
        ext = orig_path.split('.')[-1]
        EXTENSION = '.' + orig_path.split('.')[-1]
        min_f = None
        max_f = None
        for o_file in origin_files:
            if o_file.endswith(tuple(['.ccc', 'cc', 'cube'])):
                lut_extension = o_file.split('.')[-1]
                if not os.path.exists('{0}'.format(os.path.dirname(shot['clip_cdl_path']))):
                    create_folders('{0}'.format(os.path.dirname(shot['clip_cdl_path'])))
                has_ccc = True
                print 'lut_extension:', lut_extension
                f = open(os.path.dirname(orig_path) + '/' + o_file,"r")
                contenido = f.read()
                contenido = contenido.replace(o_file.split('.')[0], os.path.basename(shot['clip_cdl_path']).split('.')[0])
                f.close()
                try:
                    shot['clip_cdl_path'] = shot['clip_cdl_path'].replace('ccc', lut_extension)
                except:
                    pass
                print shot['clip_cdl_path'], o_file
                new_cdl = open(shot['clip_cdl_path'],"w")
                new_cdl.write(contenido)
                new_cdl.close()
                lut = os.path.basename(shot['clip_cdl_path'].split('.')[0])
                print shot['clip_cdl_path'] 
            else:
                tmp_ = o_file.split('.')
                actual_frame = tmp_[1]
                ORIG_BASENAME = o_file.split('.')[0] + '.%04d.' + o_file.split('.')[-1]
                
                try:
                    actual_frame = int(actual_frame)
                except:
                    actual_frame = None

                if actual_frame is not None:
                    if min_f is None:
                        min_f = actual_frame
                        max_f = actual_frame
                    else:
                        if actual_frame < min_f:
                            min_f = actual_frame
                        if actual_frame > max_f:
                            max_f = actual_frame
        destin_file = new_path + '.####.' + ext
        master_dict = {
            'source_file': orig_path,
            'destination_path': destin_file,
            'wb_path': shot['WB_Path'],
            'extesion': ext,
            'first_frame': min_f,
            'last_frame': max_f}
        applied = wb_config(sg, master_dict)
        if not applied:
            for o_file in origin_files:
                ORIG_BASENAME = o_file.split('.')[0] + '.%04d.' + o_file.split('.')[-1]
                val1 = o_file.endswith(tuple(['.ccc', 'cc', 'cube']))
                val2 = orig_path.split('.')[-1] in o_file
                if not val1 and val2:
                    conta += 1
                    # shutil.copy(os.path.dirname(orig_path) + '/' + o_file, new_path + '.' + str(frm) + '.' + orig_path.split('.')[-1])
                    copied_file = open(os.path.dirname(orig_path) + '/' + o_file).read()
                    source_file = os.path.dirname(orig_path) + '/' + o_file
                    destin_file = new_path + '.' + '{0:04d}'.format(conta) + '.' + orig_path.split('.')[-1]

                    command = 'ln -s {0} {1}'.format(source_file, destin_file)
                    os.system(command)

                    frm = int(o_file.split('.')[-2])
                    frm = conta
    else:
        for o_file in origin_files:
          if o_file.endswith(tuple(['.ccc', 'cc', 'cube'])):
            lut_extension = o_file.split('.')[-1]
            if not os.path.exists('{0}'.format(os.path.dirname(shot['clip_cdl_path']))):
                create_folders('{0}'.format(os.path.dirname(shot['clip_cdl_path'])))
            has_ccc = True
            print 'lut_extension:', lut_extension
            f = open(os.path.dirname(orig_path) + '/' + o_file,"r")
            contenido = f.read()
            contenido = contenido.replace(o_file.split('.')[0], os.path.basename(shot['clip_cdl_path']).split('.')[0])
            f.close()
            try:
                shot['clip_cdl_path'] = shot['clip_cdl_path'].replace('ccc', lut_extension)
            except:
                pass
            print shot['clip_cdl_path'], o_file
            new_cdl = open(shot['clip_cdl_path'],"w")
            new_cdl.write(contenido)
            new_cdl.close()
            lut = os.path.basename(shot['clip_cdl_path'].split('.')[0])
            print shot['clip_cdl_path'] 
          if orig_path.split('.')[-1] in o_file:
            conta += 1
            print os.path.dirname(orig_path) + '/' + o_file, new_path + '.' + '{0:07d}'.format(int(o_file.split('.')[-2])) + '.' + orig_path.split('.')[-1]
            # shutil.copy(os.path.dirname(orig_path) + '/' + o_file, new_path + '.' + str(frm) + '.' + orig_path.split('.')[-1])
            ORIG_BASENAME = o_file.split('.')[0] + '.%04d.' + o_file.split('.')[-1]
            if conta == 1001:
              orig_name = o_file
              if shot['is_cdl']:
                  sg.update('Shot', SHOT['id'], {'sg_external_in_frame_name': orig_name})
            EXTENSION = '.' + orig_path.split('.')[-1]
            copied_file = open(os.path.dirname(orig_path) + '/' + o_file).read()
            source_file = os.path.dirname(orig_path) + '/' + o_file
            destin_file = new_path + '.' + '{0:04d}'.format(conta) + '.' + orig_path.split('.')[-1]

            
            command = 'ln -s {0} {1}'.format(source_file, destin_file)
            # white balance applied
            os.system(command)
            #with open(new_path + '.' + '{0:04d}'.format(int(o_file.split('.')[-2])) + '.' + orig_path.split('.')[-1], "wb") as f:
                #f.write(copied_file)

            frm = int(o_file.split('.')[-2])
            frm = conta

    nuke.nodePaste(config_path + '/resources/Hiero/hiero_export.nk')
    prefixx = '/tmp/'
    (file, thumbpath) = tempfile.mkstemp(prefix=prefixx + "inc_thumb",suffix='.jpg')
    print thumbpath
    write_images = nuke.toNode('write_images')
    write_images['file'].setValue(thumbpath)
    read_th = nuke.nodes.Read(name="THUMB", file=shot['clip_plate_path'].replace('.dpx', EXTENSION), first=int(frm), last=int(frm) )
    read_th['raw'].setValue(True)

    write_images.setInput(0, read_th)

    nuke.executeMultiple([write_images], ([int(frm), int(frm), 1],),
                         [nuke.views()[0]])

    try:
        width = read_th.width()
        height = read_th.height()
        if read_th.metadata().get('exr/CameraFormat'):
            cam = read_th.metadata()['exr/CameraFormat']
        data = {'sg_resy': height, 'sg_resx': width}
        print 'DATA TO UPDATE:' , data
        if shot['is_cdl']:
            sg.update('Shot', SHOT['id'], data)
            data = {'sg_pixel_aspect': int(shot['aspect'])}
            sg.update('Shot', SHOT['id'], data)
    except Exception as e:
        print str(e)


    if has_ccc:
      nuke.nodePaste(config_path + '/resources/Hiero/hiero_thumbnails.nk')
      prefixx = '/tmp/'
      (file, thumbpath_ccc) = tempfile.mkstemp(prefix=prefixx + "ccc_thumb",suffix='.png')
      write_images = nuke.toNode('ccc_write')
      write_images['file'].setValue(thumbpath_ccc)
      lut_path = shot['clip_cdl_path']
      #lut_node = nuke.toNode('LUT')
      #lut_node['vfield_file'].setValue(lut_path)
      nuke.executeMultiple([write_images], ([int(frm - 1 ), int(frm-1) , 1],),[nuke.views()[0]])

    print sgtk, "sgtk Shot:",  SHOT['id']
    file_to_publish = shot['clip_plate_path']
    print file_to_publish ,file_to_publish.split('editorial')[0]
    tk = sgtk.sgtk_from_entity("Shot", SHOT['id'])
    ctx = tk.context_from_entity("Shot", SHOT['id'])
    name = os.path.basename(file_to_publish)
    name = name.replace('_' + name.split('_')[-1], '')
    p_file = sg.find_one('PublishedFile',[['project', 'is', sg_project],['code', 'is', os.path.basename(file_to_publish)],['version_number', 'is', shot['version']]])
    if not p_file == None:
      sg.delete("PublishedFile", p_file['id'])
    sgtk.util.register_publish(
      tk,
      ctx,
      file_to_publish, 
      name, 
      published_file_type="Image",
      thumbnail_path = thumbpath,
      version_number = shot['version'],
      sg_fields = {'sg_client_name' : ORIG_BASENAME },
      description = 'Plate')
    if not shot['mp_name'] == '':
        sh_dat = {'sg_main_plate_name': shot['mp_name']}
        sg.update('Shot', SHOT['id'], sh_dat)
    if has_ccc:
      name = os.path.basename(shot['clip_cdl_path']).split('.')[0]
      file_to_publish = shot['clip_cdl_path']
      p_file = sg.find_one('PublishedFile',[['project', 'is', sg_project],['code', 'is', name],['version_number', 'is', shot['version']]])
      if not p_file == None:
        sg.delete("PublishedFile", p_file['id'])
      sgtk.util.register_publish(
        tk, 
        ctx, 
        file_to_publish, 
        name, 
        published_file_type="Color Decision List",
        thumbnail_path = thumbpath_ccc,
        version_number = shot['version'],
        description = 'CDL')
