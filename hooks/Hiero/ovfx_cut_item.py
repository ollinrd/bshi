import sys
import os
import tempfile
import ast
import nuke
import shutil
import subprocess


shot_dict_txt = nuke.rawArgs[3]
config_path = nuke.rawArgs[4]
bndl_path = nuke.rawArgs[5]
sys.path.append('{0}/install/core/python'.format(config_path))
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
import yaml

import shotgun_api3
import tank as sgtk


file = open(shot_dict_txt,'r')
SHOTLIST = file.read()
file.close()
SHOTS = ast.literal_eval(SHOTLIST)


def get_settings(config_path):
  settings_path = config_path + "/resources/project/project_specs.yml"    
  stream = open(settings_path, "r")
  settings = dict(yaml.load(stream))    
  return settings

project_settings = get_settings(bndl_path)
sys.path.append(project_settings.get("python_utils_path"))
from pythonTools.shotgunUtils import dbConnect

def connect_to_shotgun(proj_settings):

  api_namekey = proj_settings.get("ingesta_editorial_apikeyname")
  sg_connect = dbConnect.sgConnect()
  sg = sg_connect.connect(api_namekey)
  art1 = sg.find_one('HumanUser', [['login', 'is', 'ecampos']])
  print 'user: ', art1
  
  return sg, sg_connect

def create_cut_version(duration, code, framerate, sequence, path_source, artist, sg_project, orig_name):
    data_version = { 'sg_first_frame': 1,
                   'sg_last_frame': 1 + duration,
                   'frame_count': duration + 1,
                   'code': os.path.basename(path_source).replace('.mov', ''),
                   'frame_range': '1-{0}'.format(0 + duration),
                   'sg_uploaded_movie_frame_rate': framerate,
                   'entity': sequence,
                   'sg_path_to_movie': path_source,
                   'sg_status_list': 'na',
                   'user': artist,
                   'sg_version_type': 'Reference',
                   'description': 'Editorial Cut Reference',
                   'sg_external_name': orig_name,
                   'project': sg_project}
    version_cut = sg.find_one('Version', [['code','is', os.path.basename(path_source).replace('.mov', '')]])
    if not version_cut:
        version_cut = sg.create('Version', data_version)
    else:
        version_cut = sg.update('Version', version_cut['id'], data_version)
    print 'VERSION:', version_cut
    return version_cut

def create_part_version(duration, framerate, sequence, path_source, artist, sg_project, orig_name, config_path, first, last, sh_ver):
    data_version = { 'sg_first_frame': 1,
                   'sg_last_frame': 1 + duration,
                   'frame_count': duration + 1,
                   'code': os.path.basename(path_source).replace('.mov', ''),
                   'frame_range': '1-{0}'.format(0 + duration),
                   'sg_uploaded_movie_frame_rate': framerate,
                   'entity': sequence,
                   'sg_path_to_movie': path_source,
                   'sg_status_list': 'na',
                   'sg_version_type': 'Reference',
                   'user': artist,
                   'description': 'Editorial part of cut reference',
                   'sg_external_name': orig_name,
                   'project': sg_project}
    version_cut = sg.find_one('Version', [['code','is', os.path.basename(path_source).replace('.mov', '')]])
    if not version_cut:
        version_cut = sg.create('Version', data_version)
    else:
        version_cut = sg.update('Version', version_cut['id'], data_version)
    print 'VERSION:', version_cut
    try:
        sg.upload('Version', version_cut['id'], path_source, 'sg_uploaded_movie')
    except:
      pass

    mid_frame = int(int(last - first) / 2)
    mid_frame = int(first + mid_frame)
    prefixx = '/tmp/'
    (file, thumbpath) = tempfile.mkstemp(prefix=prefixx + "cut_item",suffix='.jpg')
    write_images = nuke.toNode('write_images')
    write_images['file'].setValue(thumbpath)
    read_th = nuke.nodes.Read(name="THUMB", file=path_source, first=mid_frame, last=mid_frame )
    read_th['raw'].setValue(True)
    write_images.setInput(0, read_th)
    nuke.executeMultiple([write_images], ([mid_frame , mid_frame , 1],),
                                         [nuke.views()[0]])


    #sys.path.append('{0}/install/core/python'.format(config_path))
    #import tank as sgtk
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator

    api_namekey = settings.get("ingesta_editorial_apikeyname")        
    sg_auth = sg_connect.authenticateuser_from_apiname(api_namekey, sgtk, ShotgunAuthenticator)


    file_to_publish = path_source
    name = os.path.basename(file_to_publish)
    name = name.replace('_' + name.split('_')[-1], '')
    tk = sgtk.sgtk_from_entity("Sequence", sequence['id'])
    ctx = tk.context_from_entity("Sequence", sequence['id'])
    p_file = sgtk.util.register_publish(
        tk,
        ctx,
        file_to_publish,
        name,
        published_file_type="Movie",
        version_number= int(sh_ver),
        thumbnail_path=thumbpath,
        created_by=artist,
        version_entity=version_cut)
    return p_file

multiple = False

#print 'videoooossssosss', SHOTS[0]['video_parts']
def create_folders(path):
    if not os.path.exists(path):
        try:
            chmod = '2775'
            ovfxPath=path.split("/")
            ovfxFolder=""
            for folder in ovfxPath:
                ovfxFolder = ovfxFolder + folder + "/" 
                if not os.path.exists(ovfxFolder):
                    os.system("sudo /usr/bin/mkdir %s"%(ovfxFolder))
                    os.system("sudo /usr/bin/chmod  %s %s "%(chmod,ovfxFolder))
                    os.system("sudo /usr/bin/chown %s %s"%('core', ovfxFolder))
                    if 'editorial' in ovfxFolder:
                        os.system("sudo /usr/bin/chgrp %s %s"%('editorial', ovfxFolder))
                    else:
                        os.system("sudo /usr/bin/chgrp %s %s"%('ovfx', ovfxFolder))
        except Exception as e:
          raise e

def change_format(proj_settings, reformat_node, txt):
    width = proj_settings['thumbnail_format'][0]
    height = proj_settings['thumbnail_format'][1]

    # Crating custom format
    f_name = 'custom_format'
    f_new = nuke.addFormat('{0} {1} 1 {2}'.format(width, height, f_name))

    # Apply new info to nodes
    reformat_node['format'].setValue(f_new)



sg, sg_connect = connect_to_shotgun(project_settings)
downstreams = []
print SHOTS
if len(SHOTS[0]['video_parts']) > 1:
    nuke.nodePaste('{0}/resources/Hiero/hiero_export.nk'.format(bndl_path))

    total_duration = SHOTS[0]['total_duration']
    print total_duration
    ind = 0
    for pth in SHOTS[0]['video_parts']:
        # copy sub videos
        sub_path = SHOTS[0]['out_ref'].replace('/mov/', '/segments/')
        arr = os.path.basename(sub_path).split('_')
        arr[1] = SHOTS[0]['seq']
        new_name_part = '_'.join(arr).replace('.mov', '_part{0:02d}.mov'.format(ind + 1))
        sub_path = os.path.dirname(sub_path) + '/' + new_name_part
        try:
          create_folders(os.path.dirname(SHOTS[0]['out_ref']))
        except:
          pass
        try:
          create_folders(os.path.dirname(sub_path))
        except:
          pass

        print 'VIDEO PART = rsync -avh {0} {1}'.format(pth, sub_path)
        p = subprocess.Popen('rsync -avh {0} {1}'.format(pth, sub_path), stdout=subprocess.PIPE, shell=True)
        p_status = p.wait()
        # create reads of new videos
        readNode = nuke.createNode('Read')
        out_ref = SHOTS[0]['out_ref']
        readNode.knob('file').fromUserText(sub_path)
        appen = nuke.toNode('AppendClip1')
        appen.setInput(ind, readNode)
        p_first = readNode['first'].value()
        p_last = readNode['last'].value()
        dur = p_last - p_first + 1
        artist = sg.find_one('HumanUser', [['login', 'is', SHOTS[0]['artist']]])
        sequence = sg.find_one('Sequence', [['project', 'is', SHOTS[0]['project']],['code', 'is',SHOTS[0]['seq']]])
        pf_part_version = create_part_version(dur, float(project_settings['frame_rate']), sequence, sub_path, artist, SHOTS[0]['project'], 
                                           os.path.basename(pth),bndl_path, p_first, p_last, SHOTS[0]['version'])
        downstreams.append(pf_part_version)
        ind += 1
    w_f = nuke.toNode('Fusion')
    w_f['file'].setValue(out_ref)
    w_f['mov64_fps']
    print sub_path
    nuke.executeMultiple([w_f], ([1, int(total_duration), 1],),
                                         [nuke.views()[0]])
    origin_path = sub_path
    multiple = True
    nuke.scriptClear()

for shot in SHOTS:
    print shot
    artist = sg.find_one('HumanUser', [['login', 'is', shot['artist']]])
    sg_project = shot['project']
    cut_name = shot['cut_name']
    Cut = sg.find_one('Cut', [['code', 'is', cut_name], ['project', 'is', sg_project]],['version'])
    framerate = float(shot['framerate'])
    first = shot['cut_first']
    duration = shot['cut_duration']
    last = shot['cut_last']
    if multiple == False:
        origin_path = shot['cut_origin_path']
    origin_video_name = os.path.basename(shot['cut_origin_path']).split('.')[0]
    out_ref = shot['out_ref']
    timecode_in = shot['cut_timecode_in']
    timecode_out = shot['cut_timecode_in']
    sequence = sg.find_one('Sequence', [['project', 'is', sg_project],['code', 'is', shot['seq']]])
    print 'CUT', Cut
    if not Cut:
        print 'no existe el corte, se creara....'
        orig_name = os.path.basename(origin_path)
        data_cut = {'code': cut_name,
                    'fps': framerate,
                    'created_by': artist,
                    'duration': duration,
                    'timecode_start_text': timecode_in,
                    'timecode_end_text': timecode_out,
                    'project': sg_project,
                    'entity': sequence,
                    'revision_number': int(shot['version']),
                    }
        #CUT = sg.create('Cut', data_cut)
        if multiple == False:
            print 'rsync -avh {0} {1}'.format(origin_path, out_ref)
            orig_name = os.path.basename(origin_path)
            # shutil.copy(origin_path, out_ref)
            p = subprocess.Popen('rsync -avh {0} {1}'.format(origin_path, out_ref), stdout=subprocess.PIPE, shell=True)
            p_status = p.wait()
            p2 = subprocess.Popen('ffmpeg {0} {1}'.format(out_ref, out_ref.replace('.mov', '.wav')), stdout=subprocess.PIPE, shell=True)
            p2_status = p.wait()
        path_source = out_ref
        version_cut = create_cut_version(duration, cut_name, framerate, sequence, path_source, artist, sg_project, orig_name)
      # publishfile
        print version_cut
        path = path_source
        mid_frame = int(int(last - first) / 2)
        mid_frame = int(first + mid_frame)        
        nuke.nodePaste('{0}{1}'.format(bndl_path, '/resources/Hiero/hiero_export.nk'))

        prefixx = '/tmp/'
        (file, thumbpath) = tempfile.mkstemp(prefix=prefixx + "cut_item",suffix='.jpg')
        print thumbpath
        write_images = nuke.toNode('write_images')
        write_images['file'].setValue(thumbpath)
        read_th = nuke.nodes.Read(name="THUMB", file=out_ref, first=mid_frame, last=mid_frame )
        read_th['raw'].setValue(True)
        write_images.setInput(0, read_th)

        nuke.executeMultiple([write_images], ([mid_frame , mid_frame , 1],),
                                             [nuke.views()[0]])
        nuke.delete(read_th)
        nuke.delete(write_images)
        data_cut['image'] = thumbpath

        #import tank as sgtk
        from tank_vendor.shotgun_authentication import ShotgunAuthenticator
        api_namekey = project_settings.get("ingesta_editorial_apikeyname")        
        sg_auth = sg_connect.authenticateuser_from_apiname(api_namekey, sgtk, ShotgunAuthenticator)

        file_to_publish = path_source
        name = os.path.basename(file_to_publish)
        name = name.replace('_' + name.split('_')[-1], '')
        tk = sgtk.sgtk_from_entity("Sequence", sequence['id'])
        ctx = tk.context_from_entity("Sequence", sequence['id'])
        pub_cut = sgtk.util.register_publish(
            tk,
            ctx,
            file_to_publish,
            name,
            published_file_type="Movie",
            version_number= int(shot['version']),
            thumbnail_path=thumbpath,
            created_by=artist,
            downstream_published_files=downstreams,
            sg_fields={'downstream_published_files': downstreams},
            version_entity=version_cut)
        print pub_cut
        try:
            sg.upload('Version', version_cut['id'], path_source, 'sg_uploaded_movie')
        except:
            pass
        CUT = sg.create('Cut', data_cut)
        Cut = CUT
        sg.update('Cut', CUT['id'], {'version': version_cut})
        SHOT = sg.find_one('Shot', [['project', 'is', sg_project],['code', 'is', shot['clip_name']]],['sg_cut_in','code','sg_cut_out'])

        prefixx = '/tmp/'
        (file, thumbpath) = tempfile.mkstemp(prefix=prefixx + "cut_item",suffix='.jpg')
        print thumbpath
        nuke.nodePaste('{0}{1}'.format(bndl_path, '/resources/Hiero/hiero_export.nk'))
        write_images = nuke.toNode('write_images')
        write_images['file'].setValue(thumbpath)
        mid_frame = int(int(shot['clip_last'] - shot['clip_first']) / 2)
        mid_frame = int(shot['clip_first'] + mid_frame)

        read_th = nuke.nodes.Read(name="THUMB", file=out_ref, first=mid_frame, last=mid_frame )
        read_th['raw'].setValue(True)
        reformat = nuke.toNode('Reformat1')
        txt = nuke.toNode('Text1')
        print 'VALIDANDO: ', origin_video_name, shot['clip_name']
        if not origin_video_name == shot['clip_name'].replace(' ',''):
            txt['message'].setValue(shot['clip_name'])
            txt['disable'].setValue(False)
        else:
            txt['disable'].setValue(True)
            print 'NO ENTRA'

        change_format(project_settings, reformat, txt)
        # /////////////////////////////////////////////////////////////////////////////////////////////
        #txt.setInput(0, read_th)
        reformat.setInput(0, read_th)

        #write_images.setInput(0, reformat)
        write_images.setInput(0, txt)

        #nuke.scriptSaveAs("/home/fmoreno/Desktop/Tests/holissss.nk")

        nuke.executeMultiple([write_images], ([mid_frame , mid_frame , 1],),
                            [nuke.views()[0]])
        nuke.delete(write_images)
        nuke.delete(read_th)

        data_ci1 = {'edit_in': shot['clip_first'],
                'edit_out': shot['clip_last'],
                'code': shot['clip_name'],
                'timecode_edit_in_text': shot['clip_timecode_in'],
                'timecode_edit_out_text': shot['clip_timecode_out'],
                'cut_order': shot['indice'],
                'image': thumbpath,
                'created_by': artist,
                'cut_item_duration': shot['clip_duration'],
                'project': sg_project,
                'cut': Cut
            }

        data_ci2 = {'edit_in': shot['clip_first'],
                'edit_out': shot['clip_last'],
                'code': shot['clip_name'],
                'timecode_edit_in_text': shot['clip_timecode_in'],
                'timecode_edit_out_text': shot['clip_timecode_out'],
                'cut_order': shot['indice'],
                'image': thumbpath,
                'cut_item_duration': shot['clip_duration'],
                'project': sg_project,
                'cut': Cut
            }

        if not SHOT == None:
            if SHOT.get('sg_cut_in'):
                data_ci1['cut_item_in'] = SHOT['sg_cut_in']
                data_ci1['cut_item_out'] = SHOT['sg_cut_out']
            else:
                data_ci1['cut_item_in'] = shot['clip_first']
                data_ci1['cut_item_out'] = shot['clip_last']
        else:
            data_ci1['cut_item_in'] = shot['clip_first']
            data_ci1['cut_item_out'] = shot['clip_last']

        if not SHOT == None:
            if SHOT.get('sg_cut_in'):
                data_ci2['cut_item_in'] = SHOT['sg_cut_in']
                data_ci2['cut_item_out'] = SHOT['sg_cut_out']
            else:
                data_ci2['cut_item_in'] = shot['clip_first']
                data_ci2['cut_item_out'] = shot['clip_last']
        else:
            data_ci2['cut_item_in'] = shot['clip_first']
            data_ci2['cut_item_out'] = shot['clip_last']
        cutitem = sg.find_one('CutItem',[['project', 'is', sg_project],['code', 'is', str(shot['clip_name'])],['cut', 'is', Cut],['cut_order', 'is', shot['indice']]])
        if not cutitem:
            cutitem = sg.create('CutItem', data_ci1)
        else:
            cutitem = sg.update('CutItem', cutitem['id'],data_ci2)
        if not SHOT:
            pass
        else:
            cutitem = sg.update('CutItem', cutitem['id'], {'shot': SHOT})
            shot_versions = sg.find('Version', [['entity', 'is', SHOT]], ['code'])
            found_vrefs = []
            # REFERENCE VERSION LINK
            for s_v in shot_versions:
                if 'cut_ref' in s_v['code']:
                    found_vrefs.append(s_v['code'])
            found_vrefs.sort()
            if found_vrefs:
                ad_v = sg.find_one('Version', [['entity', 'is', SHOT],['code', 'is', found_vrefs[-1]]])
                cutitem = sg.update('CutItem', cutitem['id'], {'version': ad_v})
            found_v0s = []
            # VERSION ZERO LINK
            for s_v in shot_versions:
                if 'version0' in s_v['code'] and 'mp01' in s_v['code']:
                    found_v0s.append(s_v['code'])
            found_v0s.sort()

            if found_v0s:
                print 'VEEEEERRRSIOON ENCONTRADA:', found_v0s[-1]
                ad_v = sg.find_one('Version', [['entity', 'is', SHOT],['code', 'is', found_v0s[-1]]])
                cutitem = sg.update('CutItem', cutitem['id'], {'version': ad_v})

        print 'curtiem',str(shot['clip_name']), shot,cutitem

    else:
        SHOT = sg.find_one('Shot', [['project', 'is', sg_project],['code', 'is', shot['clip_name']]],['sg_cut_in','code','sg_cut_out'])
        prefixx = '/tmp/'
        (file, thumbpath) = tempfile.mkstemp(prefix=prefixx + "cut_item",suffix='.jpg')
        print thumbpath
        nuke.nodePaste('{0}{1}'.format(bndl_path, '/resources/Hiero/hiero_export.nk'))
        write_images = nuke.toNode('write_images')
        write_images['file'].setValue(thumbpath)
        mid_frame = int(int(shot['clip_last'] - shot['clip_first']) / 2)
        mid_frame = int(shot['clip_first'] + mid_frame)

        read_th = nuke.nodes.Read(name="THUMB", file=out_ref, first=mid_frame, last=mid_frame )
        read_th['raw'].setValue(True)
        reformat = nuke.toNode('Reformat1')
        txt = nuke.toNode('Text1')
        print 'VALIDANDO: ', origin_video_name, shot['clip_name']
        if not origin_video_name == shot['clip_name'].replace(' ',''):
            txt['message'].setValue(shot['clip_name']) 
            txt['disable'].setValue(False)
        else:
            txt['disable'].setValue(True)
            print 'NO ENTRA'

        change_format(project_settings, reformat, txt)

        # /////////////////////////////////////////////////////////////////////////
        #txt.setInput(0, read_th)
        reformat.setInput(0, read_th)

        #write_images.setInput(0, reformat)
        write_images.setInput(0, txt)

        nuke.executeMultiple([write_images], ([mid_frame , mid_frame , 1],),
                                             [nuke.views()[0]])
        nuke.delete(write_images)
        nuke.delete(read_th)

        data_ci1 = {'edit_in': shot['clip_first'],
              'edit_out': shot['clip_last'],
              'code': shot['clip_name'],
              'timecode_edit_in_text': shot['clip_timecode_in'],
              'timecode_edit_out_text': shot['clip_timecode_out'],
              'cut_order': shot['indice'],
              'image': thumbpath,
              'created_by': artist,
              'cut_item_duration': shot['clip_duration'],
              'project': sg_project,
              'cut': Cut
              }

        data_ci2 = {'edit_in': shot['clip_first'],
              'edit_out': shot['clip_last'],
              'code': shot['clip_name'],
              'timecode_edit_in_text': shot['clip_timecode_in'],
              'timecode_edit_out_text': shot['clip_timecode_out'],
              'cut_order': shot['indice'],
              'image': thumbpath,
              'cut_item_duration': shot['clip_duration'],
              'project': sg_project,
              'cut': Cut
              }

        if not SHOT == None:
            if SHOT.get('sg_cut_in'):
                data_ci1['cut_item_in'] = SHOT['sg_cut_in']
                data_ci1['cut_item_out'] = SHOT['sg_cut_out']
            else:
                data_ci1['cut_item_in'] = shot['clip_first']
                data_ci1['cut_item_out'] = shot['clip_last']
        else:
            data_ci1['cut_item_in'] = shot['clip_first']
            data_ci1['cut_item_out'] = shot['clip_last']
        
        if not SHOT == None:
            if SHOT.get('sg_cut_in'):
                data_ci2['cut_item_in'] = SHOT['sg_cut_in']
                data_ci2['cut_item_out'] = SHOT['sg_cut_out']
            else:
                data_ci2['cut_item_in'] = shot['clip_first']
                data_ci2['cut_item_out'] = shot['clip_last']
        else:
            data_ci2['cut_item_in'] = shot['clip_first']
            data_ci2['cut_item_out'] = shot['clip_last']
        cutitem = sg.find_one('CutItem',[['project', 'is', sg_project],['code', 'is', str(shot['clip_name'])],['cut', 'is', Cut],['cut_order', 'is', shot['indice']]])
        if not cutitem:
            cutitem = sg.create('CutItem', data_ci1)
        else:
            cutitem = sg.update('CutItem', cutitem['id'],data_ci2)
        if not SHOT:
            pass
        else:
            cutitem = sg.update('CutItem', cutitem['id'], {'shot': SHOT})
        print 'curtiem',str(shot['clip_name']), shot,cutitem
        found_v0s = []
        if not SHOT == None:
            shot_versions = sg.find('Version', [['entity', 'is', SHOT]], ['code'])
            for s_v in shot_versions:
                if 'version0' in s_v['code'] and 'mp01' in s_v['code']:
                    found_v0s.append(s_v['code'])
            found_v0s.sort()
            if found_v0s:
                print 'VEEEEERRRSIOON ENCONTRADA:', found_v0s[-1]
                ad_v = sg.find_one('Version', [['entity', 'is', SHOT],['code', 'is', found_v0s[-1]]])
                cutitem = sg.update('CutItem', cutitem['id'], {'version': ad_v})




