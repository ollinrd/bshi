# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os

import tank
import sgtk
import datetime
import nuke
import threading

from subprocess import Popen, PIPE
from tank import Hook
from tank import TankError
from sgtk.platform.qt import QtCore
import subprocess
# slack settings
import sys
sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
sys.path.append('/usr/lib/python2.7/site-packages')
from slacker import Slacker
from slackclient import SlackClient
slack_token = "xoxb-287815793574-kBLNIATP5yHULcL6kATYfZF4"
sc = SlackClient(slack_token)


class SecondaryPublishHook(Hook):
    """
    Single hook that implements publish of the primary task
    """
    def execute(self, progress_cb, **kwargs):
        """
         :param progress_cb:     Function
                                A progress callback to log progress during pre-publish.  Call:

                                    progress_cb(percentage, msg)

                                to report progress to the UI
        """
        print 'executing client hook'
        config_path = self.parent.engine.context.tank.pipeline_configuration.get_path()
        entity_type = self.parent.version["entity"]["type"]
        filters = [['id', 'is', self.parent.version['id']]]
        from shotgun_api3 import Shotgun
        SG_URL = 'https://0vfx.shotgunstudio.com'
        TANK_SCRIPT_NAME="Toolkit"
        TANK_SCRIPT_KEY="b20e30ae264400f844d4197b5805d1105ba6ddf06fd733b51a74b8fa290cbc73"
        shgn = Shotgun(SG_URL, 
                       TANK_SCRIPT_NAME, 
                       TANK_SCRIPT_KEY)
        self.int_version = shgn.find_one('Version', filters, ['sg_client_task_name', 'created_by'])

        if not self.int_version['sg_client_task_name']:
            self.int_version['sg_client_task_name'] = 'COMP'

        if "Shot" in entity_type:
            sg_project = self.parent.version['project']
            tk = sgtk.platform.current_engine().sgtk
            conf_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
            self._burnin_nk = '{0}/resources/ovfx_BSHI_Burnin.nk'.format(conf_path)
            return self._render_movie_in_nuke(progress_cb)

    def _render_movie_in_nuke(self, progress_cb=None):
        tk_nuke = sgtk.platform.current_engine()
        version = self.parent.version
        shot_n = tk_nuke.context.entity["name"]
        project = self.parent.shotgun.find_one("Project",
                                               [["id", "is",
                                                 tk_nuke.context.project["id"]]],
                                               ["code"])
        shot_fields = ["sg_head_in", 'sg_tail_out', 'sg_offset_frame', 'sg_in_timecode','sg_camera',
                       'sg_resx', 'sg_resy', 'tags', 'sg_external_in_frame', 'sg_str_timecode', 'sg_lut',
                       'code', 'sg_sequence', 'sg_colorspace', 'sg_sequence.Sequence.code', 'sg_external_id',
                       'sg_full_cg', 'sg_custom_work_range']
        ENTITY = self.parent.shotgun.find_one("Shot",
                                              [["project", "is", project],
                                               ['code', 'is', shot_n]],
                                              shot_fields)
        self.INTERNAL_V = self.parent.shotgun.find_one('Version',[['id', 'is',self.parent.version['id']]],['sg_client_task_name', 'created_by',
                                                                                                           'sg_first_frame','sg_last_frame'] )

        if not ENTITY['sg_custom_work_range']:
            first_frame = ENTITY['sg_head_in']
            last_frame = ENTITY['sg_tail_out']
        else:
            first_frame = self.INTERNAL_V['sg_first_frame']
            last_frame = self.INTERNAL_V['sg_last_frame']
        timecode2 = ENTITY['sg_str_timecode']
        group = nuke.nodes.Group()
        # now operate inside this group
        group.begin()
        nuke.root()['proxy'].setValue(False)
        checkboxes_values = self.parent.checkboxes_values
        script_path = self.parent.checkboxes_values["script_path"]
        entity, version, fields, path = self._get_settings_info(tk_nuke)
        context = self.parent.context
        print context, "---CONTEXT"
        self.init_aces_project()
        task = self.parent.shotgun.find_one("Task",
                                            [["id", "is", version["sg_task"]["id"]]],
                                            ["task_assignees", "step"])
        user = task["task_assignees"][0]
        nk_name = os.path.basename(script_path)
        read = nuke.nodes.Read(name="source",
                               file=path.replace(os.sep, "/"))
        # now create the slate/burnin node
        burn = nuke.nodePaste(self._burnin_nk)
        burn.setInput(0, read)
        nuke.root()["first_frame"].setValue(first_frame)
        nuke.root()["last_frame"].setValue(last_frame)
        date = "{0}".format(datetime.date.today().strftime('%Y / %m / %d'))
        seq_name = ENTITY['sg_sequence.Sequence.code']
        version_data = self.parent.shotgun.find_one("Version", 
                                                    [['id', 'is', version['id']]],
                                                    ['sg_for_trailer', 'sg_vendor_element_filename',
                                                     'sg_submitted_for', 'sg_submission_notes'])
        self.version_data = version_data
        self._init_read_node(read, path, first_frame, last_frame, ENTITY['sg_resx'], ENTITY['sg_resy'])
        # Render process!!!!
        fields = self._define_fields(ENTITY)
        edit_mov_path, fields = self._define_publish_paths(tk_nuke, version, ENTITY,
                                                           fields, checkboxes_values,
                                                           task)
        external_edit_filename = os.path.basename(edit_mov_path)
        # Set the values to the burnin labels
        client_node = burn.node("client")
        client_node.node("Switch")["which"].setValueAt(0, first_frame - 1)
        client_node.node("Switch")["which"].setValueAt(1, first_frame)
        artist_name = self._create_artist_name(user)
        nuke.root()['last_frame'].setValue(last_frame)
        nuke.root()['first_frame'].setValue(first_frame)
        client_node.node('label_date_times1').knob('message').setValue('{0}'.format(date))
        client_node.node('label_date_time').knob('message').setValue('{0}'.format(date))
        client_node.node('label_script_name').knob('message').setValue('Script: {0}'.format(os.path.basename(edit_mov_path).split('.')[0] + '.nk'))
        client_node.node('label_version1').knob('message').setValue('{0}'.format(os.path.basename(edit_mov_path).split('.')[0]))
        client_node.node('label_comments').knob('message').setValue(version_data['sg_submission_notes'])
        client_node.node('label_sequence').knob('message').setValue('Sequence: {0}'.format(seq_name))
        client_node.node('label_artist').knob('message').setValue('Artist: {0}'.format(artist_name))
        client_node.node('label_artistname').knob('message').setValue('Artist: {0}'.format(artist_name))
        client_node.node('label_internal').knob('message').setValue('Shot: {0}'.format(ENTITY['sg_external_id']))
        
        client_node.node('label_project').knob('message').setValue('BSHI')
        if ENTITY['id'] in [12986]:
            client_node.node("OCIOColorSpace2")['disable'].setValue(False)
            read['raw'].setValue(True)
            client_node.node("OCIOColorSpace2")['disable'].setValue(False)
            client_node.node('write_avid1')['raw'].setValue(True)
        thumb_path = self._rendering(burn, edit_mov_path, first_frame, last_frame,
                                     ENTITY, timecode2)

        external_name_file = os.path.basename(edit_mov_path.split('.')[0])
        group.end()
        # Cleanup after ourselves
        nuke.delete(group)
        self._update_version(version, external_name_file, fields["version"])
        external_edit_filename = os.path.basename(edit_mov_path)
        edit_version = self._publish_versions(version, external_edit_filename, edit_mov_path)

        data = {'sg_vendor': 'OLN', 'sg_version_file_type': 'MOV',
                'sg_submitted_for': self.version_data['sg_submitted_for']}
        self.parent.shotgun.update('Version', edit_version['id'], data)
        self.crete_video_publish_file(thumb_path, edit_mov_path,
                                      edit_version, tk_nuke, path,
                                      fields["version"])
        try:
            version = self.parent.shotgun.find_one('Version', 
                                                   [['id', 'is', version['id']]],
                                                   ['sg_clientversionlink'])
            related = version['sg_clientversionlink']
            related.append(edit_version)
            data = {'sg_clientversionlink': related}
            self.parent.shotgun.update('Version', version['id'], data)
        except Exception as e:
            print e


    def crete_video_publish_file(self, thumb_path, pub_path, version_sg, tk_nuke, work_path,v_number):
        file_to_publish = pub_path
        name = os.path.basename(file_to_publish)
        name = name.replace('_' + name.split('_')[-1], '')
        timestap = os.path.basename(file_to_publish).split('_')[-1]
        pub_name = os.path.basename(file_to_publish).replace('_' + timestap,'.mov')
        p_file = self.parent.shotgun.find_one('PublishedFile',[['project', 'is', tk_nuke.context.project],['name', 'is', pub_name]])
        if p_file:
            self.parent.shotgun.delete("PublishedFile", p_file['id'])
        tk = tk_nuke.sgtk
        sgtk.util.register_publish(
            tk,
            tk_nuke.context,
            file_to_publish,
            name,
            published_file_type="Movie",
            thumbnail_path=thumb_path,
            version_number=v_number,
            description='Client version',
            version_entity=version_sg)

    def _publish_versions(self, version, external_name_file, render_path):
        version_client = self.parent.shotgun.find_one("Version",
                                                      [["code", "is", external_name_file]])
        print 'external_name_file'
        if not version_client:
            version_client = self._register_version(version,
                                                    external_name_file,
                                                    render_path)
        else:
            self.parent.shotgun.delete('Version', version_client['id'])
            version_client = self._register_version(version,
                                                    external_name_file,
                                                    render_path)
        self._upload_files(version_client, render_path)
        return version_client

    def _upload_files(self, sg_version, path_to_movie):
        """
        """
        # Upload in a new thread and make our own event loop to wait for the
        # thread to finish.
        event_loop = QtCore.QEventLoop()
        thread = UploaderThread(self.parent, sg_version, path_to_movie)
        thread.finished.connect(event_loop.quit)
        thread.start()
        event_loop.exec_()
        # log any errors generated in the thread
        for e in thread.get_errors():
            self.parent.log_error(e)

    def _register_version(self, version, external_str, version_path):
        data = version.copy()
        data.pop("type")
        data.pop("id")
        if data.get("published_files"):
            data.pop("published_files")
        data.pop("sg_path_to_frames")
        project = sgtk.platform.current_engine().context.project
        filters = [['id', 'is', self.parent.version['id']], 
                   ['project', 'is', project]]
        version_sn = self.parent.shotgun.find_one('Version', filters,
                                                  ['sg_submission_notes',
                                                   'sg_client_version_number',
                                                   'sg_task'])
        data['sg_task'] = None
        data["code"] = external_str
        data["client_code"] = external_str.split('.')[0]
        try:
            data.pop("vtype")
            data["user"] = self.int_version["created_by"]
        except:
            pass
        data["user"] = self.int_version["created_by"] 
        data["sg_status_list"] = "na"
        data["sg_version_type"] = "Client"
        data['project'] = sgtk.platform.current_engine().context.project
        if version_path.endswith(".mov"):
            data["sg_path_to_movie"] = version_path
        else:
            data["sg_path_to_frames"] = version_path
        print '---------------------------------------'
        print data
        print '---------------------------------------'
        versss = self.parent.shotgun.create("Version", data)
        urld = 'https://0vfx.shotgunstudio.com/page/11501#Version_{0}'.format(versss['id'])
        mensaje = "Review version: {0}\nPath:{1}\nURL: {2}".format(data['code'], version_path, urld)
        return versss

    def _update_version(self, version, name_file, version_number):
        self.parent.shotgun.update("Version", version["id"],
                                   {"client_code": name_file,
                                    "sg_client_version_number": version_number,
                                    "sg_submission_notes": version["sg_submission_notes"]})

    def _init_read_node(self, read, path, first_frame, last_frame, X , Y):
        read["file"].setValue(path.replace(os.sep, "/"))
        read["on_error"].setValue("black")
        read["first"].setValue(first_frame)
        read["last"].setValue(last_frame)
        read['reload'].execute()
        self.width_ = X
        self.height_ = Y
        read['colorspace'].setValue('ACES - ACEScg')


    def _get_settings_info(self, tk_nuke):
        version = self.parent.version
        path = version["sg_path_to_frames"]
        entity_type = self.parent.version["entity"]["type"]
        if "Shot" in entity_type:
            etype = "Shot"
        else:
            etype = "Asset"
        entity = self.parent.shotgun.find_one(etype,
                             [["id", "is", version["entity"]["id"]]],
                             ["sg_external_id", "sg_sequence", "code",
                              "sg_submission_notes", "sg_offset_frame",
                              "sg_lens_focal_length", "sg_in_timecode"])

        fields = self._define_fields(entity)
        return (entity, version, fields, path)

    def _create_artist_name(self, user):
        return str(user['id'])

    def _define_publish_paths(self, tk_nuke, version, entity, fields,
                              checkboxes_values, task):
        self.parent.log_debug("Defining paths to publish")
        entity_type = self.parent.version["entity"]["type"]
        if "Shot" in entity_type:
            EDIT_mov_template = tk_nuke.get_template_by_name("nuke_shot_render_prores")
        if checkboxes_values["version_to_publish"]:
            fields["version"] = checkboxes_values["version_to_publish"]
        else:
            fields["version"] = self._compute_client_version(EDIT_mov_template,
                                                             fields)
        fields["external_id"] = entity["sg_external_id"]
        if self.int_version['sg_client_task_name']:
            if 'ANIM' in self.int_version['sg_client_task_name']:
                anm_pub_template = tk_nuke.get_template_by_name('maya_publish_shot_playblast_jpg')
                fields["Step_name"] = self.int_version['sg_client_task_name']
                anim_pub_path = anm_pub_template.apply_fields(fields)
                self.parent.ensure_folder_exists(os.path.dirname(anim_pub_path))
                os.system('rsync -avh {0}/* {1}'.format(os.path.dirname(version['sg_path_to_frames']),
                                                        os.path.dirname(anim_pub_path)))
            Step_name = self.int_version['sg_client_task_name']
        else:
            Step_name = 'COMP'
        fields["Step_name"] = Step_name
        fields['Step'] = Step_name
        edit_mov_path = EDIT_mov_template.apply_fields(fields)
        return edit_mov_path, fields

    def _define_fields(self, entity):
        # get scene path
        tk_nuke = sgtk.platform.current_engine()
        script_path = self.parent.checkboxes_values["script_path"]
        if script_path == "Root":
            script_path = ""
        script_path = os.path.abspath(script_path)
        fields = {}
        entity_type = self.parent.version["entity"]["type"]
        if "/nfs/ollinvfx/Project/" in script_path:
            if "Shot" in entity_type:
                work_template = tk_nuke.get_template_by_name("nuke_shot_work")
            else:
                work_template = tk_nuke.get_template_by_name("nuke_asset_work")
            if 'ANIM' in self.int_version['sg_client_task_name']:
                nk_template = tk_nuke.get_template_by_name("maya_work_shot_playblast_jpg")
                fields = nk_template.get_fields(script_path)
            else:
                fields = work_template.get_fields(script_path)
            print 'FIELDS: ', fields, entity_type
        return fields

    def _rendering(self, burn, edit_mov_path, first_frame, last_frame,
                   ENTITY, timeco):
        #  vfx_path, avid_path, avid_path_jpg
        print 'RENDERING ::::', edit_mov_path
        edit_mov = burn.node("client").node("write_avid1")
        edit_mov["file"].setValue(edit_mov_path)
        output_folder_edit_mov = os.path.dirname(edit_mov_path)
        self.parent.ensure_folder_exists(output_folder_edit_mov)
        # THUMBNAIL
        thumb = burn.node('client').node("jpg")
        thumb_path = '/nfs/ovfxToolkit/TMP/clientV_{0}.jpg'.format(ENTITY['code'])
        thumb['file'].setValue(thumb_path)
        nuke.execute(thumb, first_frame, first_frame, 1,
                     [nuke.views()[0]])
        print 'thumb rendered'
        set_time = burn.node("client").node("AddTimeCode2")
        set_time['startcode'].setValue(timeco)
        set_time['frame'].setValue(first_frame - 1)
        print "executing the write node: EDIT mov ", first_frame - 1, first_frame + 7
        nuke.execute(edit_mov, first_frame - 1, first_frame + 6, 1,
                     [nuke.views()[0]])
        nuke.execute(edit_mov, first_frame - 1, last_frame, 1,
                     [nuke.views()[0]])
        # for pix encode
        # cmd = "ffmpeg -start_number {0} -f image2 -r 23.976 -i ".format(first_frame -1) + pix_jpg_path +" -c:v libx264 -profile high -r 23.976 -pix_fmt yuv420p -b:v 3200000 -timecode "+ timeco + " -s 1280x720 -movflags faststart  -y " + pix_mov_path
        return thumb_path

    def _compute_client_version(self, template, fields):
        existing_versions = self.parent.tank.paths_from_template(template,
                                                                 fields,
                                                                 ["version", "Step"])

        version_numbers = [template.get_fields(v).get("version") for v in existing_versions]

        if version_numbers:
            return max(version_numbers) + 1

        return 1

    def init_aces_project(self):
        nuke.root()['colorManagement'].setValue('OCIO')
        nuke.root()['OCIO_config'].setValue('aces_1.0.3')
        nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
        # -------------------------------------------------
        nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
        nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
        nuke.root()['int16Lut'].setValue('ACES - ACEScc')
        nuke.root()['logLut'].setValue('Input - ADX - ADX10')
        nuke.root()['floatLut'].setValue('ACES - ACES2065-1')


class UploaderThread(QtCore.QThread):
    """
    Simple worker thread that encapsulates uploading to shotgun.
    Broken out of the main loop so that the UI can remain responsive
    even though an upload is happening
    """
    def __init__(self, app, version, path_to_movie):
        QtCore.QThread.__init__(self)
        self._app = app
        self._version = version
        self._path_to_movie = path_to_movie
        self._errors = []

    def get_errors(self):
        """
        can be called after execution to retrieve a list of errors
        """
        return self._errors

    def run(self):
        """
        Thread loop
        """
        try:
            if self._path_to_movie.endswith(".mov"):
                self._app.sgtk.shotgun.upload("Version", self._version["id"],
                                              self._path_to_movie,
                                              "sg_uploaded_movie")
            else:
                import glob
                files = glob.glob(os.path.dirname(self._path_to_movie) + "/*")
        except Exception, e:
            self._errors.append("Movie upload to Shotgun failed: %s" % e)
