# Copyright (c) 2013 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import sgtk
import pprint
import datetime
import nuke

from subprocess import Popen, PIPE
from tank import Hook
from tank import TankError
from sgtk.platform.qt import QtCore

# slack settings
import sys

sys.path.append('/nfs/ovfxToolkit/Resources/site-packages')
sys.path.append('/usr/lib/python2.7/site-packages')


class PrimaryPublishHook(Hook):
    """
    Single hook that implements publish of the primary task
    """

    def execute(self, progress_cb, **kwargs):
        tk = self.parent.engine.sgtk
        conf_path = str(tk.configuration_descriptor.get_config_folder()).replace('\\', '/')
        self._burnin_nk = '{0}/resources/ovfx_BSHI_Burnin.nk'.format(conf_path)
        tk_nuke = self.parent.engine
        if tk_nuke.context.entity["type"] == 'Shot':
            self._render_in_nuke(progress_cb)

    def _render_in_nuke(self, progress_cb=None):
        # Render output is the same as render publish, no render needed just copy file
        #
        tk_nuke = sgtk.platform.current_engine()
        group = nuke.nodes.Group()
        # now operate inside this group
        group.begin()
        first_frame, last_frame = self.parent.checkboxes_values["frame_range"]
        tk_nuke = sgtk.platform.current_engine()
        shot_n = tk_nuke.context.entity["name"]
        project = self.parent.shotgun.find_one("Project",
                                               [["id", "is",
                                                 tk_nuke.context.project["id"]]],
                                               ["code", 'sg_color_space'])
        fields = ["sg_output_alpha", "sg_head_in", 'sg_tail_out', 'sg_without_lut', 'external_id', 'code', 'sg_camera', 'sg_external_in_frame_name', 'sg_custom_work_range',
                  'sg_resx', 'sg_resy', 'sg_main_plate_name', 'sg_sequence.Sequence.code', 'code', 'sg_color_space', 'sg_sequence', 'sg_external_in_frame']
        ENTITY = self.parent.shotgun.find_one("Shot",[["project", "is", project], ['code', 'is', shot_n]],fields)
        print 'ENTITY:', ENTITY
        self.INTERNAL_V = self.parent.shotgun.find_one('Version',[['id', 'is',self.parent.version['id']]],['sg_client_task_name', 'created_by',
                                                                                                           'sg_first_frame','sg_last_frame'] )
        if not ENTITY['sg_custom_work_range']:
            first_frame = ENTITY['sg_head_in']
            last_frame = ENTITY['sg_tail_out']
        else:
            first_frame = self.INTERNAL_V['sg_first_frame']
            last_frame = self.INTERNAL_V['sg_last_frame']
        # Config project specs in case of aces profile
        # Color space level 1 by shot

        checkboxes_values = self.parent.checkboxes_values
        script_path = self.parent.checkboxes_values["script_path"]
        shot, version, fields, path = self._get_settings_info(tk_nuke, script_path)
        nuke.root()["first_frame"].setValue(first_frame)
        nuke.root()["last_frame"].setValue(last_frame)
        nuke.root()['colorManagement'].setValue('OCIO')
        nuke.root()['OCIO_config'].setValue('aces_1.0.3')
        nuke.root()['workingSpaceLUT'].setValue('ACES - ACEScg')
        nuke.root()['monitorLut'].setValue('ACES/Rec.709 D60 sim.')
        nuke.root()['int8Lut'].setValue('Utility - sRGB - Texture')
        nuke.root()['int16Lut'].setValue('ACES - ACEScc')
        nuke.root()['logLut'].setValue('Input - ADX - ADX10')
        nuke.root()['floatLut'].setValue('ACES - ACES2065-1')
        read = nuke.nodes.Read(name="source", file=path.replace(os.sep, "/"))
        # now create the slate/burnin node
        print self._burnin_nk

        burn = nuke.nodePaste(self._burnin_nk)
        burn.setInput(0, read)
        # set burn options for shot version trailer option if available
        version_data = self.parent.shotgun.find_one("Version", [['id', 'is', version['id']]],
                                                    ['sg_for_trailer', 'entity', 'sg_vendor_element_filename'])
        self.version_data = version_data
        self.int_ver_id = version['id']
        # Set default values to false
        task = self.parent.shotgun.find_one("Task",
                                            [["id", "is", version["sg_task"]["id"]]],
                                            ["task_assignees", "step"])
        user = task["task_assignees"][0]
        self._init_read_node(read, path, first_frame, last_frame)
        self.original = path
        rnd_path, FIRST_F = self._define_publish_paths(tk_nuke, version, shot, fields,
                                                       checkboxes_values, task)
        clif = FIRST_F
        dur = last_frame - first_frame + 1
        ex_last = clif + dur
        # all_ch = str(read.metadata()['exr/channels']).split(',')
        self.extra_channels = ''
        read['frame_mode'].setValue('start at')
        read['frame'].setValue(str(FIRST_F))
        if progress_cb:
            progress_cb(35.0, "Waiting the render to finish")
        name_file = os.path.basename(os.path.splitext(script_path)[0])
        name_file_raw = os.path.basename(rnd_path)
        external_name_file = name_file_raw.split('.')[0]
        print 'EXTERNAL NAME: ', external_name_file
        self._fill_slate_labels(burn, shot, version, checkboxes_values, name_file,
                                external_name_file, user, first_frame, last_frame)
        # THUMBNAIL
        thumb = burn.node("client").node("jpg")
        thumb_path = '/nfs/ovfxToolkit/TMP/clientV_exr_{0}_thumbnail.jpg'.format(ENTITY['code'])
        os.system('chmod 777 {0}'.format(thumb_path))
        thumb['file'].setValue(thumb_path)
        thumb['raw'].setValue(True)
        nuke.execute(thumb, FIRST_F, FIRST_F, 1, [nuke.views()[0]])
        external_name_file, version_client = self._rendering(burn, user, rnd_path, first_frame, last_frame, progress_cb, thumb_path)# FIRST_F, ex_last, progress_cb, thumb_path)
        group.end()
        # Cleanup after ourselves
        nuke.delete(group)
        self._update_version(version, external_name_file, fields["version"])
        self._upload_files(version_client, rnd_path)
        self.crete_video_publish_file(thumb_path, rnd_path, version_client, tk_nuke, path, fields["version"])
        try:
            version = self.parent.shotgun.find_one('Version', [['id', 'is', version['id']],['entity', 'is', version_data['entity']]], ['sg_clientversionlink'])
            related = version['sg_clientversionlink']
            related.append(version_client)
            data = {'sg_clientversionlink': related}
            self.parent.shotgun.update('Version', version['id'], data)
        except Exception as e:
            print e



    def crete_video_publish_file(self, thumb_path, pub_path, version_sg, tk_nuke, work_path, v_number):
        file_to_publish = pub_path
        name = os.path.basename(file_to_publish)
        name = name.replace('_' + name.split('_')[-1], '')
        p_file = self.parent.shotgun.find_one('PublishedFile', [['project', 'is', tk_nuke.context.project],
                                                                ['code', 'is', os.path.basename(file_to_publish)]])
        if not p_file is None:
            self.parent.shotgun.delete("PublishedFile", p_file['id'])
        tk = self.parent.engine.sgtk
        sgtk.util.register_publish(
            tk,
            tk_nuke.context,
            file_to_publish,
            name,
            published_file_type="Image",
            thumbnail_path=thumb_path,
            version_number=v_number,
            description='Client version',
            version_entity=version_sg)

    def _update_version(self, version, name_file, version_number):
        self.parent.shotgun.update("Version",
                                   version["id"],
                                   {"client_code": name_file,
                                    "sg_client_version_number": version_number,
                                    })

    def _upload_files(self, sg_version, path_to_movie):
        # Upload in a new thread and make our own event loop to wait for the
        # thread to finish.
        event_loop = QtCore.QEventLoop()
        thread = UploaderThread(self.parent, sg_version, path_to_movie)
        thread.finished.connect(event_loop.quit)
        thread.start()
        event_loop.exec_()
        # log any errors generated in the thread
        for e in thread.get_errors():
            self.parent.log_error(e)

    def _register_version(self, version, external_str, version_path):
        data = version.copy()
        data.pop("type")
        data.pop("id")
        if data.get("published_files"):
            data.pop("published_files")
        data.pop("sg_path_to_frames")
        data["code"] = external_str
        data["sg_version_type"] = "Client"
        data["user"] = version["created_by"]
        data["sg_status_list"] = "na"
        data.pop("vtype")
        pprint.pprint(data, ), "new Version"
        return self.parent.shotgun.create("Version", data)

    def _init_read_node(self, read, path, first_frame, last_frame, color_space=None):
        tk_nuke = sgtk.platform.current_engine()
        shot_n = tk_nuke.context.entity["name"]
        project = self.parent.shotgun.find_one("Project",
                                               [["id", "is",
                                                 tk_nuke.context.project["id"]]],
                                               ["code"])
        ENTITY = self.parent.shotgun.find_one("Shot",
                                              [["project", "is",
                                                project], ['code', 'is', shot_n]],
                                              ["sg_head_in", 'sg_tail_out'])
        read["file"].setValue(path.replace(os.sep, "/"))
        self.orig = path.replace(os.sep, "/")
        read["on_error"].setValue("black")
        read["first"].setValue(first_frame)
        read["last"].setValue(last_frame)
        # self.height_ = read.metadata()['input/height']
        # self.width_ = read.metadata()['input/width']
        read["colorspace"].setValue('ACES - ACEScg')


    def _register_delivery(self, version, user, external, video_path):
        delivery_type = "Client Delivery"
        target_folder = os.path.dirname(os.path.dirname(video_path))
        folder_link = {'local_path': target_folder,
                       'content_type': None,
                       'link_type': 'local',
                       'name': delivery_type}
        context = self.parent.context
        title = "{0} submitted a publish {1} for {2}"
        title = title.format(user["name"], delivery_type,
                             context.entity["name"])
        contents = "The publish {0} is {1}\n".format(delivery_type,
                                                     external)
        contents += "The internal version is {0}".format(version["code"])

        delivery = self.parent.shotgun.find_one("Delivery",
                                                [["description", "is",
                                                  contents]])
        if not delivery:
            addressings_cc = self.parent.shotgun.find_one("Group",
                                                          [["id", "is", 6]],
                                                          ["users"])["users"]
            project = self.parent.shotgun.find_one("Project",
                                                   [["id", "is", context.project["id"]]],
                                                   ["sg_coordinator"])
            addressings_cc.extend(project["sg_coordinator"])
            data = {'project': version["project"],
                    'title': title,
                    'description': contents,
                    'sg_version': version,
                    "sg_external_id": external,
                    "sg_folder": folder_link,
                    "created_by": user,
                    "addressings_cc": addressings_cc,
                    "sg_path_to_file": target_folder,
                    }
            print "this is the data"
            pprint.pprint(data, )
            delivery = self.parent.shotgun.create('Delivery', data)
            self.parent.shotgun.share_thumbnail(entities=[delivery],
                                                source_entity=version)

    def _get_settings_info(self, tk_nuke, script_path):
        version = self.parent.version
        path = version["sg_path_to_frames"]
        self._path_to_frames = version["sg_path_to_frames"]
        if version["entity"]["type"] == 'Shot':
            shot = self.parent.shotgun.find_one("Shot",
                                                [["id", "is",
                                                  version["entity"]["id"]]],
                                                ["sg_external_id",
                                                 "sg_offset_frame", "sg_cut_in",
                                                 'sg_output_alpha',
                                                 "sg_sequence", "code",
                                                 "sg_lens_focal_length",
                                                 "sg_submission_notes",
                                                 "sg_resx", "sg_resy",
                                                 'sg_external_in_frame_name',
                                                 'sg_head_in', 'sg_tail_out',
                                                 'sg_sequence.Sequence.code',
                                                 'sg_external_in_frame'])
            fields = self._define_fields(shot, script_path)
            self.submitted_for = \
            self.parent.shotgun.find_one('Version', [['id', 'is', version['id']]], ['sg_submitted_for'])['sg_submitted_for']
            return (shot, version, fields, path)
        else:
            asset = self.parent.shotgun.find_one('Asset', [["id", "is",
                                                            version["entity"]["id"]]],
                                                           ['code'])
            fields = self._define_fields(asset, script_path)
            self.parent.shotgun.find_one('Version', [['id', 'is', version['id']]], ['sg_submitted_for'])['sg_submitted_for']
            return (asset, version, fields, path)

    def _fill_slate_labels(self, burn, shot, version, checkboxes_values,
                           name_file, external_name_file, user, first_frame, last_frame):
        artist_name = self._create_artist_name(user)
        artist_label = burn.node("client").node("label_artist3")
        shot_label = burn.node("client").node("label_internal3")
        message = "Artist: {0}".format(artist_name)
        artist_label["message"].setValue(message)
        
        rango = str(last_frame - first_frame + 1)
        burn.node("client").node("label_frames3")['message'].setValue(rango)
        if shot.get("sg_lens_focal_length"):
            message = "LENS: {0}mm".format(shot["sg_lens_focal_length"])

        shot_label["message"].setValue('Shot:' + str(external_name_file))
        burn.node("client").node("label_script_name2")['message'].setValue(name_file)
        burn.node("client").node("label_sequence2")['message'].setValue('Secuence: ' + str(shot['sg_sequence.Sequence.code']))
        date_label = burn.node("client").node("label_date_time3")
        date = "{0}".format(datetime.date.today().strftime('%Y / %m / %d'))
        date_label["message"].setValue(date)
        burn.node('client').node('label_project').knob('message').setValue('BSHI')
        burn.node('client').node('label_project2').knob('message').setValue('BSHI')
        comment_label = burn.node("client").node("label_comments2")
        if shot.get("sg_resx") and shot.get("sg_resy"):
            formato = '{0} {1} BSHI_A'.format(shot['sg_resx'], shot['sg_resy'])
            nuke.addFormat(formato)
            burn.node("client").node("Reformat2")['format'].setValue('BSHI_A')
            print 'RESolution: {0} {1}'.format(shot['sg_resx'], shot['sg_resy'])
        if version.get("sg_submission_notes"):
            message = "Notes: {0}".format(version["sg_submission_notes"])
            comment_label["message"].setValue(message)
        elif checkboxes_values.get("description"):
            message = "{0}".format(checkboxes_values["description"])
            comment_label["message"].setValue(message)
        else:
            raise TankError(
                "There is no value for the submission notes.\n Please let the coordination team know about this")

    def _create_artist_name(self, user):
        if user['id'] == 89:
            return str(user['id'])
        return "{0}. {1}".format(user["name"].split(" ")[0][0],
                                 user["name"].split(" ")[-1])

    def _define_publish_paths(self, tk_nuke, version, shot, fields, checkboxes_values, task):

        self.parent.log_debug("Defining paths to publish")
        editorial_template = tk_nuke.get_template_by_name("nuke_delivery_editorial_mov")
        if not shot['sg_output_alpha']:
            render_template = tk_nuke.get_template_by_name("nuke_shot_delivery_exr")
        else:
            render_template = tk_nuke.get_template_by_name("nuke_shot_delivery_PNG")

        fields["external_id"] = shot["sg_external_id"]
        fields['external_base_name'] = shot['sg_external_in_frame_name']
        if self.INTERNAL_V['sg_client_task_name']:
            fields['Step'] = self.INTERNAL_V['sg_client_task_name']
        else:
            fields['Step'] = 'comp'
        if checkboxes_values["version_to_publish"]:
            fields["version"] = checkboxes_values["version_to_publish"]
        else:
            fields["version"] = self._compute_client_version(editorial_template,
                                                             fields)
        render_path = render_template.apply_fields(fields)
        ifn = shot['sg_external_in_frame_name']
        BASE_EXR_PATH = os.path.dirname(render_path)
        if ifn:
            if len(ifn.split('.')) == 2:
                if not shot['sg_output_alpha']:
                    BASE_EXR_PATH + '/' + ifn.replace(ifn.split('_')[-1],'%04d.exr')
                else:
                    BASE_EXR_PATH + '/' + ifn.replace(ifn.split('_')[-1],'%04d.exr')
        FIRST_F = shot['sg_external_in_frame']

        self.FIRST_F = FIRST_F
        return render_path, FIRST_F

    def _define_fields(self, entity, script_path):
        # get scene path
        tk_nuke = sgtk.platform.current_engine()
        if entity['type'] == 'Shot':
            shot_n = tk_nuke.context.entity["name"]
            project = self.parent.shotgun.find_one("Project",
                                                   [["id", "is",
                                                     tk_nuke.context.project["id"]]],
                                                   ["code"])
            ENTITY = self.parent.shotgun.find_one("Shot",
                                                  [["project", "is",
                                                    project], ['code', 'is', shot_n]],
                                                  ["sg_head_in", 'sg_tail_out', ])
            fields = {}
            work_template = tk_nuke.get_template_by_name("nuke_shot_work")
            fields = work_template.get_fields(script_path)
            return fields
        else:
            asset_n = tk_nuke.context.entity["name"]
            project = self.parent.shotgun.find_one("Project",
                                                   [["id", "is",
                                                     tk_nuke.context.project["id"]]],
                                                   ["code"])
            ENTITY = self.parent.shotgun.find_one("Asset",
                                                  [["project", "is",
                                                    project], ['code', 'is', asset_n]],
                                                  ["sg_head_in", 'sg_tail_out', ])
            work_template = tk_nuke.get_template_by_name("nuke_asset_work")
            fields = work_template.get_fields(script_path)
            return fields

    def _compute_client_version(self, template, fields):
        existing_versions = self.parent.tank.paths_from_template(template,
                                                                 fields,
                                                                 ["version"])
        version_numbers = [template.get_fields(v).get("version") for v in existing_versions]
        if version_numbers:
            return max(version_numbers)
        return 1

    def similynks_copy(self, rnd_path, entity, first_frame, last_frame, padding):
        clif = first_frame
        print 'RNDR PATH:', rnd_path
        cuadro = first_frame
        for cont in range(first_frame, last_frame):
            out_pdd = '{{0:0{0}d}}'.format(padding)
            old_p = '%0{0}d'.format(padding)
            command_sim = 'ln -s {0} {1}'.format(self.original.replace('%04d', str(cuadro)),
                                                 rnd_path.replace(old_p, str(out_pdd.format(clif))))
            print command_sim
            os.system(command_sim)
            clif += 1
            cuadro += 1

    def render_slate(self, first_frame, burn, comments, sequence, artist, ENTITY, dpx_path):

        # Slate burnins labels
        # comments
        burn.node("client").node("label_comments2")['message'].setValue('Comments: {0}'.format(comments))
        # secuence
        burn.node("client").node("label_sequence2")['message'].setValue(sequence)
        # artist
        artist = str(artist['id'])
        print 'ARTIST:', artist
        burn.node("client").node("AddTimeCode3")['startcode'].setValue(ENTITY['sg_str_timecode'])
        burn.node("client").node("AddTimeCode3")['frame'].setValue(1000)
        burn.node("client").node("label_artist3")['message'].setValue(artist)
        if not ENTITY['sg_output_alpha']:
            write_exr = burn.node("client").node("PublisherEXT")
        else:
            write_exr = burn.node("client").node("PublisherPNG")
        out_pdd = '{{0:0{0}d}}'.format(4)
        old_p = '%0{0}d'.format(4)
        if not int(self.FIRST_F) == ENTITY['sg_cut_in'] - 1001:
            write_exr['file'].setValue(dpx_path.replace(old_p, out_pdd.format(int(first_frame-1))))
            nuke.root()['fps'].setValue(24)
            nuke.execute(write_exr, first_frame - 1, first_frame - 1, 1, [nuke.views()[0]])
            # nuke.execute(write_exr, self.FIRST_F - 1, self.FIRST_F - 1, 1, [nuke.views()[0]])

    def _rendering(self, burn, sg_user, dpx_path, first_frame, last_frame, progress_cb, thumb_path):

        tk_nuke = sgtk.platform.current_engine()
        shot_n = tk_nuke.context.entity["name"]

        project = self.parent.shotgun.find_one("Project",
                                               [["id", "is",
                                                 tk_nuke.context.project["id"]]],
                                               ["code"])
        ENTITY = self.parent.shotgun.find_one("Shot",
                                              [["project", "is",
                                                project], ['code', 'is', shot_n]],
                                              ["sg_output_alpha", "sg_head_in", 'sg_tail_out', 'sg_without_lut',
                                               'external_id', 'code', 'sg_camera', 'sg_resx', 'sg_resy',
                                               'sg_main_plate_name', 'sg_sequence.Sequence.code', 'sg_external_in_frame',
                                               'sg_str_timecode', 'sg_cut_in'])
        # first_frame = ENTITY['sg_head_in']
        # last_frame = ENTITY['sg_tail_out']

        if progress_cb:
            progress_cb(35.0, "Rendering the jpg")
        nuke.Root()['first_frame'].setValue(first_frame - 1)
        external_name_file = os.path.basename(dpx_path).split('.')[0]

        tk_nuke = sgtk.platform.current_engine()
        cdl_template = tk_nuke.get_template_by_name("cdl_shot")
        client_cdl_template = tk_nuke.get_template_by_name("nuke_shot_delivery_cdl")
        if not ENTITY['sg_output_alpha']:
            vid_template = tk_nuke.get_template_by_name("nuke_shot_delivery_exr")
        else:
            vid_template = tk_nuke.get_template_by_name("nuke_shot_delivery_PNG")
        fields = vid_template.get_fields(dpx_path)

        # Get the current version used for delivery
        version_deliver = fields["version"]

        SHOT = ENTITY['code']
        SEQ = ENTITY['code'].split('_')[0]
        fields = {
            'project': 'BSHI',
            'Shot': SHOT,
            'Sequence': SEQ,
        }

        # Make sure the output folder exists
        output_folder = os.path.dirname(dpx_path)
        self.parent.ensure_folder_exists(output_folder)


        filters = [['id', 'is', self.parent.version['id']]]
        sg_version = self.parent.shotgun.find_one('Version', filters,
                                                  ['sg_submission_notes', 'sg_client_version_number', 'sg_task', 'entity'])

        # Create EXR salte
        self.render_slate(first_frame, burn, sg_version['sg_submission_notes'], ENTITY['sg_sequence.Sequence.code'], sg_user, ENTITY, dpx_path)
        # The is no render, published files must be copied from publish to client delivery path
        self.similynks_copy(dpx_path, ENTITY, first_frame, last_frame, 4)

        dpx_path.replace('DPX', 'EXR').replace('.dpx', '.exr')
        if not ENTITY['sg_output_alpha']:
            image = dpx_path.split('.')[0] + '.' + str(first_frame) + '.exr'
            file_type = 'EXR'
        else:
            image = dpx_path.split('.')[0] + '.' + str(first_frame) + '.png'
            file_type = 'PNG'


        try:


            filters = [['id', 'is', self.parent.version['id']], ['project', 'is', project]]
            version_sn = self.parent.shotgun.find_one('Version', filters,
                                                      ['sg_submission_notes', 'sg_client_version_number', 'sg_task'])

            try:
                submission_notes = version_sn['sg_submission_notes']
            except:
                submission_notes = ''
            try:
                sg_client_version = version_sn['sg_client_version_number']
            except:
                sg_client_version = 1

            print 'external_name_file EXR: ', external_name_file
            print 'exr_path: ', dpx_path


            ex_nF = external_name_file + '.[{0}-{1}]'.format(first_frame - 1, last_frame)

            filters = [['code', 'is', ex_nF], ['project', 'is', project],
                       ['sg_client_version_number', 'is', sg_client_version],['entity', 'is',sg_version['entity']]]
            version_sg = self.parent.shotgun.find_one('Version', filters)
            os.system("USER=$(whoami)")
            coord = os.environ["USER"]
            coord = self.parent.shotgun.find_one('HumanUser', [['login', 'is', str(coord)]])
            data = {'project': project,
                    'code': ex_nF,
                    'sg_version_type': 'Client Delivery',
                    'sg_status_list': 'na',
                    'image': thumb_path,
                    'entity': ENTITY,
                    'sg_client_version_number': sg_client_version,
                    'sg_submission_notes': submission_notes,
                    'sg_path_to_frames': dpx_path,
                    'sg_first_frame': first_frame,
                    'sg_last_frame': last_frame,
                    'frame_count': last_frame - (first_frame - 1),
                    'sg_uploaded_movie_frame_rate': float(24),
                    'frame_range': '{0}-{1}'.format(first_frame, last_frame),
                    'sg_movie_has_slate': True,
                    'sg_vendor': 'ollinvfx',
                    'sg_version_file_type': file_type,
                    'sg_submitted_for': self.submitted_for,
                    'client_code': external_name_file,
                    'sg_client_task_name': self.INTERNAL_V['sg_client_task_name'],
                    'user': self.INTERNAL_V['created_by'],
                    'created_by': coord
                    }

            if not version_sg:
                version_sg = self.parent.shotgun.create('Version', data)
                print 'CREATED', version_sg
            else:
                # self.parent.shotgun.delete('Version', version_sg['id'])
                version_sg = self.parent.shotgun.update('Version', version_sg['id'], data)
                print 'UPDATED', version_sg
        except Exception, e:
            print 'NO SE PUDO CREAR LA VERSION DE DPX', e
        nuke.Root()['first_frame'].setValue(first_frame)
        return external_name_file, version_sg

class UploaderThread(QtCore.QThread):
    def __init__(self, app, version, path_to_movie):
        QtCore.QThread.__init__(self)
        self._app = app
        self._version = version
        self._path_to_movie = path_to_movie
        self._errors = []

    def get_errors(self):
        return self._errors

    def run(self):
        try:
            import glob
            files = glob.glob(os.path.dirname(self._path_to_movie) + "/*")
        except Exception, e:
            self._errors.append("Movie upload to Shotgun failed: %s" % e)
